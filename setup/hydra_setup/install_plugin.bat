set BASEDIR=%~dp0
cd C:\Program Files\QGIS*
set QGISDIR=%cd%
cd Apps\Python27\Lib
set LIB2DIR=%cd%
cd C:\edb\languagepack-9.6\x64\Python-3.3\
set P3DIR=%cd%
cd Lib
set LIB3DIR=%cd%
set PLUGINDIR=%UserProfile%\.qgis2\python\plugins\
cd %BASEDIR%

echo Install GMSG
7za x "gmsh-2.12.0-Windows.zip" -o"C:\Program Files\" -y
if %errorlevel% NEQ 0 goto handleerror1orhigher

echo Extract python 2.7 librairies... please wait...
7za x "Lib_2.7.zip" -o"%LIB2DIR%" -y
if %errorlevel% NEQ 0 goto handleerror1orhigher

echo Extract python 3.3 librairies... please wait...
7za x "Lib_3.3.zip" -o"%LIB3DIR%" -y
if %errorlevel% NEQ 0 goto handleerror1orhigher

echo Install GDAL
msiexec.exe /i "gdal-201-1800-x64-core.msi" /passive /L*V "gdalcore_install.log"
if %errorlevel% NEQ 0 goto handleerror1orhigher
msiexec.exe /i "GDAL-2.1.2.win-amd64-py3.3.msi" /passive /L*V "gdal_install.log" TARGETDIR="%P3DIR%" TARGETDIRX="%P3DIR%"
if %errorlevel% NEQ 0 goto handleerror1orhigher

echo Extract hydra plugin
7za x hydra.zip -o"%PLUGINDIR%" -y
if %errorlevel% NEQ 0 goto handleerror1orhigher

echo Configuring PostgreSQL service...
sc config "postgresql-x64-9.6" obj= LocalSystem password= "" type= interact type= own
if %errorlevel% NEQ 0 goto handleerror1orhigher
net stop "postgresql-x64-9.6"
if %errorlevel% NEQ 0 goto handleerror1orhigher
net start "postgresql-x64-9.6"
if %errorlevel% NEQ 0 goto handleerror1orhigher

exit 0
:handleerror1orhigher
exit 1


