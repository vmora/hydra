echo off
set BASEDIR=%~dp0

cd C:\Program Files\QGIS*
set ROOT=%cd%

set PYTHONPATH=%ROOT%\apps\Python27;%ROOT%\apps\qgis-ltr\python
set PATH=%ROOT%\bin;%ROOT%\apps\qgis-ltr\bin;%ROOT%\apps\Python27\Scripts;c:\Program Files\PostgreSQL\9.6\bin;c:\Program Files (x86)\Graphviz2.38\bin;C:\Program Files\gmsh-2.12.0-Windows;%PATH%
set PYTHONHOME=%ROOT%\apps\Python27
set PGUSER=postgres

cd %BASEDIR%


echo on
set PYTHONPATH
set PATH
