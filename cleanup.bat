del /s *.pyc

FOR /d /r . %%d IN (__pycache__,*.egg-info,*.zip) DO @IF EXIST "%%d" rd /s /q "%%d"
FOR /d /r . %%d IN (hydra/docs/source/api/,hydra/docs/build) DO @IF EXIST "%%d" rd /s /q "%%d"
FOR /d /r . %%d IN (hydra/server/dist,hydra/server/build) DO @IF EXIST "%%d" rd /s /q "%%d"