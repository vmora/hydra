# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
install hydra extention

USAGE

   install.py <postgresql_extension_directory>

"""

import os
import shutil
import sys
import json

__currendir = os.path.dirname(__file__)
__version__ =  json.load(open(os.path.join(__currendir, '..', 'versions', 'version_history.json')))["current_version"]

if __name__=='__main__':
    from hydra import build
    assert os.path.isdir(sys.argv[1])
    build(sys.argv[1], __version__)

