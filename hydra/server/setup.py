# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from setuptools import setup
import re
import os
import sys
import json

if sys.version_info < (3,0):
    sys.exit('Sorry, Python < 3 is not supported')

__currendir = os.path.dirname(__file__)
__version__ =  json.load(open(os.path.join(__currendir, '..', 'versions', 'version_history.json')))["current_version"]

if os.name == 'posix':
    from subprocess import Popen
    Popen(['make', '-C', 'hydra/topo']).communicate()

hydra_module = 'hydra_'+'_'.join(__version__.split('.'))
hydra_topo_module = hydra_module+'.topo'
setup(name='hydra_'+'_'.join(__version__.split('.')),
      version=__version__,
      description='hydrology model, postgres server module',
      author='oslandia',
      author_email='info@oslandia.com',
      url='https://git.oslandia.net/Client-projects/1508_03_hydratec_qgis',
      packages=[hydra_module, hydra_topo_module],
      package_dir={hydra_module: 'hydra', hydra_topo_module: 'hydra/topo'},
      license='MIT',
      package_data={hydra_module: ['*.sql', '*.json'],
          hydra_topo_module:['topo', 'topo.exe', 'libgcc_s_dw2-1.dll', 'libgfortran-3.dll', 'libquadmath-0.dll']},
      include_package_data=True,
      zip_safe=False
     )
