/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*              Scenario management               */
/* ********************************************** */

create table project.scenario(
    id serial primary key,
    name varchar(24) not null default project.unique_name('SCN_'),
    comment varchar(256),
    comput_mode hydra_computation_mode not null default 'hydrology_and_hydraulics',
    dry_inflow integer references project.dry_inflow_scenario(id),
    rainfall integer references project._rainfall(id),
    dt_hydrol_mn real not null default 5,
    soil_moisture_coef real default 0 check(soil_moisture_coef >= 0 and soil_moisture_coef <=1),
    runoff_adjust_coef real default 1 check(runoff_adjust_coef >= 0),
    option_dim_hydrol_network bool not null default false,
    date0 timestamp not null default '2000-01-01 00:00:00',
    tfin_hr real not null default 12,
    tini_hydrau_hr real not null default 12,
    dtmin_hydrau_hr real not null default 0.005,
    dtmax_hydrau_hr real not null default 0.1,
    dzmin_hydrau real not null default 0.05,
    dzmax_hydrau real not null default 0.1,
    flag_save bool not null default 'f',
    tsave_hr real default 0,
    flag_rstart bool not null default 'f',
    flag_gfx_control bool not null default 'f',
    scenario_rstart integer references project.scenario(id),
    flag_hydrology_rstart bool not null default 'f',
    scenario_hydrology_rstart integer references project.scenario(id),
    trstart_hr real default 0,
    graphic_control bool not null default 'f',
    model_connect_settings hydra_model_connection_settings not null default 'cascade',
    tbegin_output_hr real not null default 0,
    tend_output_hr real not null default 12,
    dt_output_hr real not null default .1,
    c_affin_param bool not null default 'f',
    t_affin_start_hr real not null default 0,
    t_affin_min_surf_ddomains real not null default 0,
    strickler_param bool not null default 'f',
    option_file bool not null default 'f',
    option_file_path varchar(256),
    output_option bool not null default 'f',
    transport_type hydra_transport_type not null default 'no_transport',
    iflag_mes bool not null default 'f',
    iflag_dbo5 bool not null default 'f',
    iflag_dco bool not null default 'f',
    iflag_ntk bool not null default 'f',
    quality_type hydra_quality_type not null default 'physico_chemical',
    diffusion bool not null default 'f',
    dispersion_coef real not null default 20,
    longit_diffusion_coef real not null default 41,
    lateral_diff_coef real not null default .45,
    water_temperature_c real not null default 20,
    min_o2_mgl real not null default 1.5,
    kdbo5_j_minus_1 real not null default 0.2,
    koxygen_j_minus_1 real not null default 0.45,
    knr_j_minus_1 real not null default 0.09,
    kn_j_minus_1 real not null default 0.035,
    bdf_mgl real not null default 1.5,
    o2sat_mgl real not null default 9.07,
    iflag_ad1 bool not null default 'f',
    iflag_ad2 bool not null default 'f',
    iflag_ad3 bool not null default 'f',
    iflag_ad4 bool not null default 'f',
    ad1_decay_rate_jminus_one real not null default 0,
    ad2_decay_rate_jminus_one real not null default 0,
    ad3_decay_rate_jminus_one real not null default 0,
    ad4_decay_rate_jminus_one real not null default 0,
    sst_mgl real not null default 0,
    sediment_file varchar(256) not null default '',
    scenario_ref integer references project.scenario(id)
)
;;

create table project.mixed(
    ord integer not null,
    scenario integer not null references project.scenario(id) on delete cascade,
    grp integer not null references project.grp(id),
        unique(scenario, grp),
        unique(scenario, ord)
)
;;

create table project.config_scenario(
    id serial primary key,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    configuration integer not null,
    priority integer not null default currval('project.config_scenario_id_seq')
)
;;

create table project.param_hydrograph(
    id serial primary key,
    param_scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    hydrograph_from varchar(24) not null,
    hydrograph_to varchar(256) not null
)
;;

create table project.param_external_hydrograph(
    id serial primary key,
    param_scenario integer not null references project.scenario(id) on delete cascade,
    external_file varchar(256) not null
)
;;

create table project.param_regulation(
    id serial primary key,
    param_scenario integer not null references project.scenario(id) on delete cascade,
    control_file varchar(256) not null
)
;;

create table project.param_measure(
    id serial primary key,
    param_scenario integer not null references project.scenario(id) on delete cascade,
    measure_file varchar(256) not null
)
;;

create table project.inflow_rerouting(
    id serial primary key,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    hydrograph_from integer not null,
    hydrograph_to integer,
    option hydra_rerouting_option not null default 'fraction',
    parameter real not null default 0,
        unique(scenario, model, hydrograph_from, hydrograph_to)
)
;;

create table project.c_affin_param(
    id serial primary key,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    type_domain varchar(24) not null check(type_domain in ('reach', 'domain_2d', 'branch')),
    element varchar(24) not null,
        unique(scenario, model, type_domain, element)
)
;;

create table project.strickler_param(
    id serial primary key ,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    type_domain varchar(24) not null check(type_domain in ('reach', 'domain_2d', 'branch')),
    element varchar(24) not null,
    rkmin real not null default 0,
    rkmaj real not null default 0,
    pk1 real not null default 0,
    pk2 real not null default 0,
    unique(scenario, model, type_domain, element)
)
;;

create table project.output_option(
    id serial primary key ,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    type_element varchar(48) not null,
    element varchar(24) not null
)
;;

/* ********************************************** */
/*              Grouping                          */
/* ********************************************** */

create table project.scenario_group(
    id serial primary key,
    name varchar(24) unique not null default 'GROUP_'||currval('project.scenario_group_id_seq')::varchar
)
;;

create table project.param_scenario_group(
    scenario integer not null references project.scenario(id) on delete cascade,
    grp integer not null references project.scenario_group(id) on delete cascade,
        unique(scenario, grp)
)
;;

/* ********************************************** */
/*              External files                    */
/* ********************************************** */

create table project.bc_tz_data(
    id serial primary key,
    filename varchar(256) not null
)
;;

