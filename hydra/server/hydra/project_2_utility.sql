/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*                      Models                    */
/* ********************************************** */

create table project.model_config(
    id serial primary key,
    name varchar(16) not null,
    config integer not null default 1,
        unique(name, config),
    cascade_order integer not null default currval('project.model_config_id_seq'),
    comment varchar(256)
)
;;

create table project.grp(
    id serial primary key,
    name varchar(256) unique not null default 'GROUP_'||currval('project.grp_id_seq')::varchar
)
;;

create table project.grp_model(
    grp integer not null references project.grp(id) on delete cascade,
    model integer not null references project.model_config(id),
        unique(grp, model)
)
;;

/* ********************************************** */
/*              Terrain & altitude                */
/* ********************************************** */

-- Source files

create table project.dem(
    id serial primary key,
    priority integer not null default currval('project.dem_id_seq'),
    source varchar(256) unique not null
)
;;

create table project.points_xyz(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('P_XYZ_'),
    source varchar(24) default null,
    comment varchar(256) default null,
    geom geometry('POINTZ',$srid) not null,
    z_ground real not null
)
;;

-- Altitude

create or replace function project.altitude(x real, y real)
returns real
language plpython3u immutable
as
$$$$
    from $hydra import altitude
    import plpy
    workspace = plpy.execute("select workspace from hydra.metadata")[0]['workspace']
    sources = [res['source'].replace('\\','\\\\').replace('$workspace', workspace) for res in plpy.execute("select source from project.dem order by priority asc")]
    return altitude(sources, x, y)
$$$$
;;

create function project.altitude(point geometry)
returns real
language plpgsql immutable
as
$$$$
    begin
        return project.altitude(st_x(point)::real, st_y(point)::real);
    end;
$$$$
;;

create or replace function project.set_altitude(geom geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import set_altitude
    import plpy
    workspace = plpy.execute("select workspace from hydra.metadata")[0]['workspace']
    sources = [res['source'].replace('\\','\\\\').replace('$workspace', workspace) for res in plpy.execute("select source from project.dem order by priority asc")]
    return set_altitude(sources, geom, $srid)
$$$$
;;

-- Filling curves

create or replace function project._str_filling_curve(polygon geometry)
returns varchar
language plpython3u immutable
as
$$$$
    from $hydra import filling_curve
    import plpy
    workspace = plpy.execute("select workspace from hydra.metadata")[0]['workspace']
    sources = [res['source'].replace('\\','\\\\').replace('$workspace', workspace) for res in plpy.execute("select source from project.dem order by priority asc")]
    return filling_curve(sources, polygon)
$$$$
;;

create function project.filling_curve(polygon geometry)
returns real[][]
language plpgsql immutable
as
$$$$
    begin
        return project._str_filling_curve(polygon)::real[][];
    end;
$$$$
;;

-- Geometry tools

create or replace function project.crossed_border(line geometry, polygon geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import crossed_border
    return crossed_border(line, polygon)
$$$$
;;

create or replace function project.crest_line(line geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import crest_line
    import plpy
    workspace = plpy.execute("select workspace from hydra.metadata")[0]['workspace']
    sources = [res['source'].replace('\\','\\\\').replace('$workspace', workspace) for res in plpy.execute("select source from project.dem order by priority asc")]
    return crest_line(sources, line, $srid)
$$$$
;;

create or replace function project.discretize_line(geom geometry, elem_length float)
returns geometry
language plpython3u
as $$$$
    from $hydra import discretize_line
    return discretize_line(geom, elem_length)
$$$$
;;

/* ********************************************** */
/*              Utilities                         */
/* ********************************************** */

create table project.extract(
    id serial primary key,
    ctrl_file varchar(256) not null default ''
)
;;

create table project.animeau(
    id serial primary key,
    param_file varchar(256) not null default ''
)
;;

create table project.crgeng(
    id serial primary key,
    ctrl_file varchar(256) not null default ''
)
;;

/* ********************************************** */
/*              Land use                          */
/* ********************************************** */

create table project.pollution_land_accumulation(
    id serial primary key,
    sds_stockk_gha real[4][4] not null check(array_length(sds_stockk_gha, 1)=4 and array_length(sds_stockk_gha, 2)=4),
    cds_stock_kgha real[4][4] not null check(array_length(sds_stockk_gha, 1)=4 and array_length(sds_stockk_gha, 2)=4)
)
;;

create table project.land_use(
    id serial primary key,
    geom geometry('POLYGON',$srid) not null check(ST_IsValid(geom)),
    land_use hydra_land_use_type not null
)
;;

/* ********************************************** */
/*              Results                           */
/* ********************************************** */

create or replace function project.create_result(scenario varchar, model varchar)
returns bool
language plpython3u
as $$$$
    import plpy
    from $hydra import create_result
    workspace = plpy.execute("select workspace from hydra.metadata")[0]["workspace"]
    for statement in create_result(workspace, scenario, model, $srid):
        plpy.execute(statement)
    return True
$$$$
;;

/* ********************************************** */
/*              Transcription                     */
/* ********************************************** */

create or replace function hydra_to_json(tabl varchar, geom varchar, variadic cols varchar[])
returns varchar
language plpgsql
as $$$$
    declare
        res varchar;
        ct integer;
    begin
        execute 'select count(1) from '||tabl into ct;

        if ct>0 and geom is null then
            execute 'select row_to_json(everything)
            from
                (select ''FeatureCollection'' as type,
                        array_to_json(array_agg(feat)) as features
                 from (select ''Feature'' as type,
                           null As geometry,
                           row_to_json((select l from (select '||(select array_to_string(cols, ','))||') as l)) as properties
                       from '||tabl||'
                      ) as feat
                 ) as everything;' into res;
        elsif ct>0 then
            execute 'select row_to_json(everything)
            from
                (select ''FeatureCollection'' as type,
                        (select row_to_json(l) from (select ''name'' as type,
                            (select row_to_json(m)
                                from (select ''urn:ogc:def:crs:EPSG::''||(select ST_SRID('||geom||') from '||tabl ||' limit 1)::varchar as name) as m)
                            as properties) as l)
                        as crs,
                        array_to_json(array_agg(feat)) as features
                 from (select ''Feature'' as type,
                           ST_AsGeoJSON('||geom||')::json As geometry,
                           row_to_json((select l from (select '||(select array_to_string(cols, ','))||') as l)) as properties
                       from '||tabl||'
                      ) as feat
                 ) as everything;' into res;
        else
            select '' into res;
        end if;
        return res;
    end;
$$$$
;;

create or replace function wkb_loads(geom varchar)
returns varchar
language plpython3u
as
$$$$
    from $hydra import wkb_loads
    return wkb_loads(geom)
$$$$
;;