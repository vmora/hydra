/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*                Flood areas                      */
/* ********************************************** */

create table $model.urban_flood_risk_area(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('UFRA_'),
    geom geometry('POLYGONZ',$srid)
)
;;

create table $model.bank(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('BANK_'),
    geom geometry('LINESTRINGZ',$srid) not null
)
;;

/* ********************************************** */
/*                Catchment                       */
/* ********************************************** */

create table $model.catchment(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('CATCHMENT_'),
    geom geometry('POLYGON',$srid) not null check(ST_IsValid(geom))
)
;;

/* add rule affect catchemnt to catchment_node at creation */
create function ${model}.catchment_after_fct()
returns trigger
language plpgsql
as $$$$
    begin
        if tg_op = 'INSERT' then
            update $model.catchment_node set contour=new.id where ST_Intersects($model.catchment_node.geom, new.geom);
            return new;
        else
            if tg_op = 'UPDATE' then
                update $model.catchment_node set contour=null where contour=old.id;
                update $model.catchment_node set contour=new.id where ST_Intersects($model.catchment_node.geom, new.geom);
                return new;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_catchment_after_trig
    after insert or update on $model.catchment
       for each row execute procedure ${model}.catchment_after_fct()
;;

/* ********************************************** */
/*                  Constrain lines               */
/* ********************************************** */

create table $model._constrain(
    id serial primary key,
    name varchar(16) unique not null,
    geom geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom)),
    discretized geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom)),
    elem_length real not null default 100,
    constrain_type hydra_constrain_type,
    link_attributes json
)
;;

create view $model.constrain as
select id, name, geom, discretized, elem_length, constrain_type, link_attributes
from $model._constrain
;;

create or replace view $model.close_point as
    with allpoints as (
        select id, (st_dumppoints(geom)).geom as geom from $model.constrain
    ),
    ends as (
        select id, st_startpoint(geom) as geom from $model.constrain
        union
        select id, st_endpoint(geom) as geom from $model.constrain
    ),
    cpoints as (
        select id, st_collect(geom) as geom from allpoints group by id
    ),
    clust as (
        select
          sqrt(ST_Area(ST_MinimumBoundingCircle(gc)) / pi()) as radius,
          ST_NumGeometries(gc) as nb,
          ST_Centroid(gc) as geom
        from (
          select unnest(ST_ClusterWithin(geom, 5)) gc
          from allpoints
        ) f
        union
        select
            1 as radius,
            0 as nb,
            st_force2d(e.geom) as geom
        from $model.constrain as c, cpoints as cp, ends as e
        where st_dwithin(c.geom, e.geom, 1)
        and not st_intersects(cp.geom, e.geom)
        and e.id != c.id
        and c.id = cp.id
    )
select row_number() over () AS id, radius, nb, geom::geometry('POINT', $srid) from clust where radius > 0
;;

-- Note that the discretized is not editable
create function ${model}.constrain_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        prec_ real;
        snapped_ geometry;
        discretized_ geometry;
    begin
        if tg_op = 'INSERT' then
            select precision from hydra.metadata into prec_;
            select st_snaptogrid(new.geom, prec_) into snapped_;
            select st_snaptogrid(project.discretize_line(new.geom, coalesce(new.elem_length, 100)), prec_) into discretized_;
            insert into $model._constrain(name, geom, discretized, elem_length, constrain_type, link_attributes)
            values (coalesce(new.name, 'define_later'), snapped_, discretized_, coalesce(new.elem_length, 100), new.constrain_type, new.link_attributes)
            returning id, elem_length into new.id, new.elem_length;
            update $model._constrain set name = 'CONSTR'||new.id::varchar where name = 'define_later' and id = new.id
            returning name into new.name;
            return new;
        elsif tg_op = 'UPDATE' then
            select precision from hydra.metadata into prec_;
            select st_snaptogrid(new.geom, prec_) into snapped_;
            select st_snaptogrid(project.discretize_line(new.geom, new.elem_length), prec_) into discretized_;
            update $model._constrain set name=new.name, geom=snapped_, discretized=discretized_,
                elem_length=new.elem_length, constrain_type=new.constrain_type, link_attributes=new.link_attributes
                where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._constrain where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_constrain_trig
    instead of insert or update or delete on $model.constrain
       for each row execute procedure ${model}.constrain_fct()
;;

create function ${model}.constrain_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        cov integer;
    begin
        if (select trigger_coverage from $model.metadata) then
            select $model.coverage_update() into cov;
        end if;

        if tg_op = 'INSERT' or tg_op = 'UPDATE'  then
            return new;
        else
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_constrain_after_trig
    after insert or update of geom, elem_length or delete on $model._constrain
       execute procedure ${model}.constrain_after_fct()
;;

/* ********************************************** */
/*                    Coverages                   */
/* ********************************************** */

create table $model.coverage(
    id serial primary key,
    domain_type hydra_coverage_type,
    geom geometry('POLYGONZ',$srid) not null check(ST_IsValid(geom))
)
;;

create function ${model}.coverage_after_fct()
returns trigger
language plpgsql
as $$$$
    begin
        if tg_op = 'INSERT' then
            update $model.storage_node set
                contour=new.id
                where ST_Intersects($model.storage_node.geom, new.geom);
            return new;
        else
            if tg_op = 'UPDATE' then
                update $model.storage_node set contour=null where contour=old.id;

                update $model.storage_node set
                    contour=new.id
                    where ST_Intersects($model.storage_node.geom, new.geom);
                    return new;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_coverage_after_trig
    after insert or update of geom on $model.coverage
       for each row execute procedure ${model}.coverage_after_fct()
;;

create table $model.coverage_marker(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('COV_NULL_'),
    comment varchar(256),
    geom geometry('POINTZ',$srid) not null
)
;;

create function ${model}.mkc_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        cov_ integer;
    begin
        if (select trigger_coverage from $model.metadata) then
            select $model.coverage_update() into cov_;
        end if;

        if tg_op = 'INSERT' or tg_op = 'UPDATE'  then
            return new;
        else
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_mkc_trig
    after insert or update of geom or delete on $model.coverage_marker
       for each row execute procedure ${model}.mkc_after_fct()
;;

/* ********************************************** */
/*                 2D domain                      */
/* ********************************************** */

create table $model.domain_2d(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('DOMAIN_2D_') check (name <> 'default_domain'),
    geom geometry('POLYGONZ',$srid),
    comment varchar(256)
)
;;

create table $model.domain_2d_marker(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('DOMAIN_MARKER_'),
    domain_2d integer references $model.domain_2d(id),
    geom geometry('POINTZ',$srid) not null,
    comment varchar(256)
)
;;

-- Add rule to update elem 2d after mkd modification
create function ${model}.mkd_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        old_cov integer;
        new_cov integer;
    begin
        if tg_op = 'INSERT' then
            select id from $model.coverage as c where st_intersects(c.geom, new.geom) into new_cov;
            update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = new_cov;
            return new;
        else
            if tg_op = 'UPDATE' then
                select id from $model.coverage as c where st_intersects(c.geom, old.geom) into old_cov;
                update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = old_cov;
                select id from $model.coverage as c where st_intersects(c.geom, new.geom) into new_cov;
                update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = new_cov;
                return new;
            else
                select id from $model.coverage as c where st_intersects(c.geom, old.geom) into old_cov;
                update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = old_cov;
                return old;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_mkd_after_trig
    after insert or update of geom, domain_2d or delete on $model.domain_2d_marker
       for each row execute procedure ${model}.mkd_after_fct()
;;

/* ********************************************** */
/*                     Reach                      */
/* ********************************************** */

create table $model.reach(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('REACH_'),
    pk0_km real not null default 0,
    dx real not null default 50,
    geom geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom))
)
;;

create index ${model}_reach_geom_idx on $model.reach using gist(geom)
;;

/* add rule to create end points when reach is created, the pk are set automagically */
create function ${model}.reach_after_insert_fct()
returns trigger
language plpgsql
as $$$$
    declare
        _up boolean;
        _down boolean;
    begin
        select exists(select 1 from $model.river_node as n where ST_Intersects(n.geom, ST_Buffer(ST_StartPoint(new.geom), 0.1))) into _up;
        select exists(select 1 from $model.river_node as n where ST_Intersects(n.geom, ST_Buffer(ST_EndPoint(new.geom), 0.1))) into _down;

        if not _up then
            insert into $model.river_node(geom) values(ST_StartPoint(new.geom));
        end if;
        if not _down then
            insert into $model.river_node(geom) values(ST_EndPoint(new.geom));
        end if;
        return new;
    end;
$$$$
;;

create trigger ${model}_reach_after_insert_trig
    after insert on $model.reach
       for each row execute procedure ${model}.reach_after_insert_fct()
;;

/* add trigger on reach geom update to recompute the position */
/* shall we update the pk according to the new position ? */
create function ${model}.reach_after_update_fct()
returns trigger
language plpgsql
as $$$$
    begin
        update $model.river_node set
                -- il faut valider cette mise à jour du pk
                geom = (select ST_LineInterpolatePoint(new.geom, ST_LineLocatePoint(new.geom, geom)))
                where reach=new.id
                    and id not in (select id from $model.river_node as n where ST_Intersects(n.geom, ST_LineInterpolatePoint(new.geom, ST_LineLocatePoint(new.geom, n.geom))));
        return new;
    end;
$$$$
;;

create trigger ${model}_reach_after_update_trig
    after update on $model.reach
    for each row execute procedure ${model}.reach_after_update_fct()
;;

/* add trigger on reach to update singularities validities */
create function ${model}.reach_after_fct()
returns trigger
language plpgsql
as $$$$
    begin
        if tg_op = 'INSERT' then
            update $model.bradley_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.bridge_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.hydraulic_cut_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.zregul_weir_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.param_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.regul_sluice_gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.borda_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            return new;
        else
            if tg_op = 'UPDATE' then
                update $model.bradley_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.bridge_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return new;
            else
                update $model.bradley_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.bridge_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return old;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_reach_trig
    after insert or update or delete on $model.reach
       for each row execute procedure ${model}.reach_after_fct()
;;

/* ********************************************** */
/*                     Branch                     */
/* ********************************************** */

create table $model.branch(
    id integer primary key,
    name varchar(24) unique,
    pk0_km real not null default 0,
    dx real not null default 50,
    geom geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom))
)
;;

create function $model.branch_update_fct()
returns boolean
language plpgsql
as $$$$
    begin
        update $model._pipe_link set branch = null;
        delete from $model.branch;

        -- count up and down node connectivity on pipes
        -- select pass_through manhole nodes that have one up and one down
        -- line merge pipes that have pass_through at both ends
        -- add pipes that are touching

        with up as (
            select n.id, count(1) as ct, n.geom
            from $model.pipe_link as l join $model.manhole_node as n on n.id=l.up
            group by n.id, n.geom
        ),
        down as (
            select n.id, count(1) as ct
            from $model.pipe_link as l join $model.manhole_node as n on n.id=l.down
            group by n.id
        ),
        blade as (
            select st_collect(n.geom) as geom
            from $model.manhole_node as n
            where id in (
            select id from $model.manhole_node
            except
            select up.id
            from up join down on up.id=down.id
            where up.ct = 1
            and down.ct = 1)
        ),
        branch as (
            select (st_dump(st_split(st_linemerge(st_collect(l.geom)), b.geom))).geom as geom
            from $model.pipe_link as l, blade as b
            where l.up_type='manhole'
            group by b.geom
        ),
        oriented as (
            select (st_dump(st_linemerge(st_collect(p.geom)))).geom as geom
            from branch as b,  $model.pipe_link as p
            where st_covers(b.geom, p.geom)
            group by b.geom
        )
        insert into $model.branch(id, name, geom)
        select row_number() over(), 'BRANCH_'||(row_number() over())::varchar, geom
        from oriented
        ;

        update $model.branch as b set name=m.name, dx=m.dx, pk0_km=m.pk0_km
        from $model.pipe_branch_marker_singularity as m where st_intersects(m.geom, b.geom);

        update $model._pipe_link as p set branch = b.id
        from $model._link as l, $model.branch as b
        where p.id = l.id and st_covers(b.geom, l.geom);

        return 't';
    end;
$$$$
;;

/* add trigger on branch to update singularities validities */
create function ${model}.branch_after_fct()
returns trigger
language plpgsql
as $$$$
    begin
        if tg_op = 'INSERT' then
            update $model.hydraulic_cut_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.zregul_weir_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.param_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.regul_sluice_gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.borda_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            return new;
        else
            if tg_op = 'UPDATE' then
                update $model.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return new;
            else
                update $model.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return old;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_branch_trig
    after insert or update or delete on $model.branch
       for each row execute procedure ${model}.branch_after_fct()
;;

/* ********************************************** */
/*                      Street                    */
/* ********************************************** */

create table $model.street(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('STREET_'),
    width real not null default 0,
    elem_length real default 100,
    rk real not null default 40.,
    geom geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom))
)
;;

create index ${model}_street_geom_idx on $model.street using gist(geom)
;;

create function ${model}.street_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        res integer;
    begin
        if new.width != 0 then
            if tg_op = 'INSERT' or (tg_op = 'UPDATE' and old.width=0) then
                perform $model.gen_cst_street(new.id);
            end if;
        end if;

        perform $model.crossroad_update();

        return new;
    end;
$$$$
;;

create trigger ${model}_street_after_trig
    after insert or update of width, geom on $model.street
       for each row execute procedure ${model}.street_after_fct();
;;

/* ********************************************** */
/*                      Station                   */
/* ********************************************** */

create table $model.station(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('STATION_'),
    geom geometry('POLYGONZ',$srid) not null check(ST_IsValid(geom))
)
;;

create function ${model}.station_after_fct()
returns trigger
language plpgsql
as $$$$
    begin
        if tg_op = 'INSERT' then
            update $model.river_node
                set geom=geom
                where st_intersects(geom, new.geom);
            update $model.manhole_node
                set geom=geom
                where st_intersects(geom, new.geom);
            return new;
        else
            if tg_op = 'UPDATE' then
                update $model.river_node
                    set geom=geom
                    where st_intersects(geom, old.geom) or st_intersects(geom, new.geom);
                update $model.manhole_node
                    set geom=geom
                    where st_intersects(geom, old.geom) or st_intersects(geom, new.geom);
                return new;
            else
                update $model.river_node
                    set geom=geom
                    where st_intersects(geom, old.geom);
                update $model.manhole_node
                    set geom=geom
                    where st_intersects(geom, old.geom);
                return old;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_station_trig
    after insert or update or delete on $model.station
       for each row execute procedure ${model}.station_after_fct()
;;
