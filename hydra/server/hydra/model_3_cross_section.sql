/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create table $model.closed_parametric_geometry(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('CP_'),
    zbmin_array real[20][2] check(array_length(zbmin_array, 1)<=20 and array_length(zbmin_array, 1)>=1 and array_length(zbmin_array, 2)=2)
)
;;

create function $model.closed_parametric_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._link
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', cp_geom}')::text[], 'null'::jsonb))::json
            where link_type='pipe' and (configuration->config.name->'cp_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_cp_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_cp_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_cp_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_cp_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

create trigger ${model}_closed_parametric_geometry_trig
    before delete on $model.closed_parametric_geometry
       for each row execute procedure ${model}.closed_parametric_geometry_before_del_fct()
;;

create table $model.open_parametric_geometry(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('OP_'),
    zbmin_array real[20][2] check(array_length(zbmin_array, 1)<=20 and array_length(zbmin_array, 1)>=1 and array_length(zbmin_array, 2)=2)
)
;;

create function $model.open_parametric_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._link
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', op_geom}')::text[], 'null'::jsonb))::json
            where link_type='pipe' and (configuration->config.name->'op_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_op_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_op_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_op_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_op_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

create trigger ${model}_open_parametric_geometry_trig
    before delete on $model.open_parametric_geometry
       for each row execute procedure ${model}.open_parametric_geometry_before_del_fct()
;;

create table $model.valley_cross_section_geometry(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('VA_'),
    zbmin_array real[6][2] not null check(array_length(zbmin_array, 1)<=6 and array_length(zbmin_array, 1)>=1 and array_length(zbmin_array, 2)=2),
    zbmaj_lbank_array real[4][2] check(array_length(zbmaj_lbank_array, 1)<=4 and array_length(zbmaj_lbank_array, 1)>=1 and array_length(zbmaj_lbank_array, 2)=2),
    zbmaj_rbank_array real[4][2] check(array_length(zbmaj_rbank_array, 1)<=4 and array_length(zbmaj_rbank_array, 1)>=1 and array_length(zbmaj_rbank_array, 2)=2),
    rlambda real not null default 1 check(rlambda > 0 and rlambda <= 1),
    rmu1 real not null default -999,
    rmu2 real not null default -999,
    zlevee_lb real not null default -999,
    zlevee_rb real not null default -999,
    transect integer references $model._constrain(id) default null,
    t_ignore_pt boolean not null default False,
    t_discret int not null default 150,
    t_distance_pt int not null default 10
)
;;

create function $model.valley_cross_section_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_vcs_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_vcs_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_vcs_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_vcs_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

create trigger ${model}_valley_cross_section_geometry_trig
    before delete on $model.valley_cross_section_geometry
       for each row execute procedure ${model}.valley_cross_section_geometry_before_del_fct()
;;

create table $model.valley_cross_section_topo_geometry(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('VAT_'),
    xz_array real[150][2] check(array_length(xz_array, 1)<=150 and array_length(xz_array, 1)>=1 and array_length(xz_array, 2)=2)
)
;;

create function $model.valley_cross_section_topo_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_vcs_topo_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_vcs_topo_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_vcs_topo_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_vcs_topo_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

create trigger ${model}_valley_cross_section_topo_geometry_trig
    before delete on $model.valley_cross_section_topo_geometry
       for each row execute procedure ${model}.valley_cross_section_topo_geometry_before_del_fct()
;;
