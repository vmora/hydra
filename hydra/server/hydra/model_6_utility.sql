/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

-- FOR EVERYTHING THAT HAS TO BE DONE LAST AT MODEL CREATION

create index ${model}_elem_2d_node_contour_idx on $model._elem_2d_node using gist(contour)
;;
create index ${model}_constrain_geom_idx on $model._constrain using gist(geom)
;;
create index ${model}_constrain_discretized_geom_idx on $model._constrain using gist(discretized)
;;
create index ${model}_coverage_geom_idx on $model.coverage using gist(geom)
;;

/* ********************************************** */
/*              Validity handling                 */
/* ********************************************** */

create view $model.invalid as
    with
        node_invalidity_reason as ($node_invalidity_reason),
        link_invalidity_reason as ($link_invalidity_reason),
        singularity_invalidity_reason as ($singularity_invalidity_reason)
    select CONCAT('node:', n.id) as
        id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        reason from $model._node as n join node_invalidity_reason as r on r.id=n.id
        where validity=FALSE
    union
    select CONCAT('link:', l.id) as
        id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        reason from $model._link as l join link_invalidity_reason as r on  r.id=l.id
        where validity=FALSE
    union
    select CONCAT('singularity:', s.id) as
        id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        reason from $model._singularity as s join $model._node as n on s.node = n.id join singularity_invalidity_reason as r on r.id=s.id
        where s.validity=FALSE
    union
    select CONCAT('profile:', p.id) as
        id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
;;

/* ********************************************** */
/*          Configuration handling                */
/* ********************************************** */

create or replace view $model.configured as
    select CONCAT('node:', n.id) as id,
        configuration,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom
        from $model._node as n
        where configuration is not null
    union all
    select CONCAT('link:', l.id) as id,
        configuration,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom
        from $model._link as l
        where configuration is not null
    union all
    select CONCAT('singularity:', s.id) as id,
        s.configuration,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from $model._singularity as s join $model._node as n on s.node = n.id
        where s.configuration is not null
    union all
    select CONCAT('river_cross_section_profile:', p.id) as id,
        p.configuration,
        p.name,
        'profile'::varchar as type,
        'river_cross_section'::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.configuration is not null;
;;

create or replace view $model.configured_current as
    with config as (select c.name from $model.configuration as c, $model.metadata as m where m.configuration=c.id)
    select CONCAT('node:', n.id) as id,
        configuration,
        n.name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom
        from $model._node as n, config as c
        where exists (select 1 from json_object_keys(n.configuration) where json_object_keys = c.name limit 1) and c.name != 'default'
    union all
    select CONCAT('link:', l.id) as id,
        configuration,
        l.name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom
        from $model._link as l, config as c
        where exists (select 1 from json_object_keys(l.configuration) where json_object_keys = c.name limit 1) and c.name != 'default'
    union all
    select CONCAT('singularity:', s.id) as id,
        s.configuration,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from config as c, $model._singularity as s join $model._node as n on s.node = n.id
        where exists (select 1 from json_object_keys(s.configuration) where json_object_keys = c.name limit 1) and c.name != 'default'
    union all
    select CONCAT('river_cross_section_profile:', p.id) as id,
        p.configuration,
        p.name,
        'profile'::varchar as type,
        'river_cross_section'::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from config as c, $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where exists (select 1 from json_object_keys(p.configuration) where json_object_keys = c.name limit 1) and c.name != 'default';
;;

/* ********************************************** */
/*          Results and carto generation          */
/* ********************************************** */

create or replace function ${model}.geom_dat(type_ varchar, id integer)
returns varchar
language plpython3u
as
$$$$
    import plpy
    from $hydra import geom_dat
    return geom_dat(plpy, '$model', type_, id)
$$$$
;;

create or replace function ${model}.ctrz_dat()
returns varchar
language plpython3u
as
$$$$
    import plpy
    from $hydra import ctrz_dat
    return ctrz_dat(plpy, '$model')
$$$$
;;

create view ${model}.l2d
as
with link as (
    select l.name, project.crossed_border(l.geom, n.contour) as geom
    from ${model}._link as l, ${model}.elem_2d_node as n
    where (n.id = l.up or n.id=l.down)
    and l.link_type!='mesh_2d'
    and l.link_type!='network_overflow'
    and st_intersects(l.geom, st_exteriorring(n.contour)))
select name, st_x(st_startpoint(geom)) as xb, st_y(st_startpoint(geom)) as yb, st_x(st_endpoint(geom)) as xa, st_y(st_endpoint(geom)) as ya from link
;;

/* ********************************************** */
/*                  Reach types                   */
/* ********************************************** */

create view ${model}.open_reach
as with
    ends as (
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'valley' and p.type_cross_section_down != 'valley' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up != 'valley' and p.type_cross_section_down = 'valley' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'valley' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) > .99
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_down = 'valley' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) < .01
    )
    ,
    bounds as (
        select FIRST_VALUE(reach) over (partition by reach order by alpha), typ, LAG(alpha) over (partition by reach order by alpha) as start, alpha as end, id, reach from ends
    )
    select row_number() over() as id, st_linesubstring(r.geom, b.start, b.end) as geom, r.id as reach from bounds as b join ${model}.reach as r on r.id=b.reach where b.typ = 'down'
;;

create view ${model}.channel_reach
as with
    ends as (
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and p.type_cross_section_down != 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up != 'channel' and p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) > .99
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) < .01
    )
    ,
    bounds as (
        select FIRST_VALUE(reach) over (partition by reach order by alpha), typ, LAG(alpha) over (partition by reach order by alpha) as start, alpha as end, id, reach from ends
    )
    select row_number() over() as id, st_linesubstring(r.geom, b.start, b.end) as geom, r.id as reach from bounds as b join ${model}.reach as r on r.id=b.reach where b.typ = 'down'
;;

create function $model.downstream_valley_section(transect geometry)
returns integer
language plpgsql immutable
as
$$$$
    begin
        return (
        with reach as (
            select r.id, st_intersection(r.geom, transect) as inter, geom, pk0_km
            from ${model}.reach as r
            where st_intersects(r.geom, transect)
        ),
        pk as (
            select st_linelocatepoint(geom, inter)*st_length(geom)/1000+pk0_km as pk_km from reach
        ),
        down_reach as (
            select ndown.reach as id
            from ${model}.river_node as ndown
            join ${model}.connector_link as l on l.down=ndown.id
            join ${model}.river_node as nup on nup.id=l.up
            where l.main_branch
            and nup.reach = (select id from reach)
        ),
        extended_reach_node as (
            select n.id, n.pk_km
            from ${model}.river_node as n, pk
            where n.reach in (select id from reach union select id from down_reach)
            and n.pk_km >= pk.pk_km
        ),
        section_down as (
            select c.id
            from extended_reach_node as n
            join ${model}.river_cross_section_profile as c on c.id = n.id,
            pk
            where ((c.type_cross_section_down='valley' and c.down_vcs_geom is not null)
                or (c.type_cross_section_up='valley' and c.up_vcs_geom is not null))
            order by n.pk_km asc
            limit 1
        )
        select id from section_down);
    end;
$$$$
;;

/* ********************************************** */
/*            View for results display            */
/* ********************************************** */

-- Pipes
create or replace view ${model}.results_pipe as
    select id, name, geom from ${model}.pipe_link
    where up_type='manhole' and down_type='manhole'
;;

-- Hydrol pipes
create or replace view ${model}.results_pipe_hydrol as
    select id, name, geom from ${model}.pipe_link
    where up_type='manhole_hydrology' and down_type='manhole_hydrology'
;;

-- Catchments
create or replace view ${model}.results_catchment as
    select id, name, geom from ${model}.catchment_node
;;

-- Links
create or replace view ${model}.results_link as
    select id, name, geom from ${model}.gate_link
        union
    select id, name, geom from ${model}.regul_gate_link
        union
    select id, name, geom from ${model}.weir_link
        union
    select id, name, geom from ${model}.borda_headloss_link
        union
    select id, name, geom from ${model}.pump_link
        union
    select id, name, geom from ${model}.deriv_pump_link
        union
    select id, name, geom from ${model}.fuse_spillway_link
        union
    select id, name, geom from ${model}.connector_link
        union
    select id, name, geom from ${model}.strickler_link
        union
    select id, name, geom from ${model}.porous_link
        union
    select id, name, geom from ${model}.overflow_link
        union
    select id, name, geom from ${model}.network_overflow_link
        union
    select id, name, geom from ${model}.mesh_2d_link
        union
    select id, name, geom from ${model}.street_link
;;

-- Surface elements
create or replace view ${model}.results_surface as
    with
        dump as (
            select st_collect(n.geom) as points, s.geom as envelope, s.id
            from ${model}.coverage as s, ${model}.crossroad_node as n
            where s.domain_type='street' and st_intersects(s.geom, n.geom)
            group by s.id
        ),
        voronoi as (
            select st_intersection(st_setsrid((st_dump(st_VoronoiPolygons(dump.points, 0, dump.envelope))).geom, ${srid}), dump.envelope) as geom
            from dump
        )
    select e.id as id, e.name as name, e.contour as geom from ${model}.elem_2d_node as e
        union
    select n.id as id, n.name as name, c.geom as geom from ${model}.storage_node as n join ${model}.coverage as c on n.contour=c.id
        union
    select n.id as id, n.name as name, voronoi.geom as geom from voronoi, ${model}.crossroad_node as n where st_intersects(voronoi.geom, n.geom)
        union
    select n.id as id, n.name as name, c.geom as geom from ${model}.crossroad_node as n, ${model}.coverage as c where c.domain_type='crossroad' and st_intersects(n.geom, c.geom)
    order by id asc;
;;

/* ********************************************** */
/*                  Pipe capacity                 */
/* ********************************************** */

create function $model.geometric_calc_s_fct(real[])
returns double precision
language plpgsql immutable
as
$$$$
    declare
        z_array alias for $$1;
        x real[];
        s double precision := 0.0;
        last_h double precision := 0.0;
        last_w double precision := 0.0;
    begin
        foreach x slice 1 in array z_array
        loop
            s:=s+0.5*(x[2]+last_w)*(x[1]-last_h);
            last_h:=x[1];
            last_w:=x[2];
        end loop;
        return s;
    end;
$$$$
;;

create function ${model}.cp_geometric_calc_p_fct(real[])
returns double precision
language plpgsql immutable
as
$$$$
    declare
        z_array alias for $$1;
        x real[];
        s double precision := 0.0;
        last_h double precision := 0.0;
        last_w double precision := 0.0;
        w1 double precision := 0.0;
    begin
        foreach x slice 1 in array z_array
        loop
            s:=s+2*sqrt(pow(x[2]-last_w,2)+pow(x[1]-last_h,2));
            last_h:=x[1];
            last_w:=x[2];
        end loop;
        s:=s+z_array[1][2]+z_array[array_length(z_array, 1)][2];
        return s;
    end;
$$$$
;;

create function ${model}.op_geometric_calc_p_fct(real[])
returns double precision
language plpgsql immutable
as
$$$$
    declare
        z_array alias for $$1;
        x real[];
        s double precision := 0.0;
        last_h double precision := 0.0;
        last_w double precision := 0.0;
        w1 double precision := 0.0;
    begin
        foreach x slice 1 in array z_array
        loop
            s:=s+2*sqrt(pow(x[2]-last_w,2)+pow(x[1]-last_h,2));
            last_h:=x[1];
            last_w:=x[2];
        end loop;
        s:=s+z_array[1][2];
        return s;
    end;
$$$$
;;

create or replace view ${model}.pipe_link as
 select p.id,
    p.name,
    p.up,
    p.down,
    c.comment,
    c.z_invert_up,
    c.z_invert_down,
    c.cross_section_type,
    c.h_sable,
    c.branch,
    c.rk,
    c.custom_length,
    c.circular_diameter,
    c.ovoid_height,
    c.ovoid_top_diameter,
    c.ovoid_invert_diameter,
    c.cp_geom,
    c.op_geom,
    p.geom,
    p.configuration::character varying as configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration as configuration_json,
    (select case
    when (c.z_invert_up-c.z_invert_down)*st_length(p.geom)>0
    and st_length(p.geom)<>0
    then
        (select case when c.cross_section_type='circular' then
            (select case when c.circular_diameter>0 then
                (with n as (select
                    (pi()/4*pow(c.circular_diameter, 2.0)) as s,
                    (pi()*c.circular_diameter) as p,
                    c.rk as k)
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n))
            else -999
            end)
        when c.cross_section_type='ovoid' then
            (with n as (select
                (pi()/8*(c.ovoid_top_diameter*c.ovoid_top_diameter+c.ovoid_invert_diameter*c.ovoid_invert_diameter)+0.5*(c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))*(c.ovoid_top_diameter+c.ovoid_invert_diameter)) as s,
                (pi()/2*(c.ovoid_top_diameter+c.ovoid_invert_diameter)+2*(c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))) as p,
                c.rk as k)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        when c.cross_section_type='pipe' then
            (with n as (select ${model}.geometric_calc_s_fct(cpg.zbmin_array) as s,
                    ${model}.cp_geometric_calc_p_fct(cpg.zbmin_array) as p,
                    c.rk as k
                from ${model}.closed_parametric_geometry cpg
                where c.cp_geom = cpg.id)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        when c.cross_section_type='channel' then
            (with n as (select ${model}.geometric_calc_s_fct(opg.zbmin_array) as s,
                    ${model}.op_geometric_calc_p_fct(opg.zbmin_array) as p,
                    c.rk as k
                from ${model}.open_parametric_geometry opg
                where c.op_geom = opg.id)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        else 0.0
        end)
    else -999
    end) as qcap,
    (select case when c.cross_section_type='circular' then
        (select (c.z_invert_up+c.circular_diameter))
    when c.cross_section_type='ovoid' then
        (select (c.z_invert_up+c.ovoid_height))
    when c.cross_section_type='pipe' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.closed_parametric_geometry cpg
            where c.cp_geom = cpg.id)
        (select (c.z_invert_up + n.height) from n))
    when c.cross_section_type='channel' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.open_parametric_geometry opg
            where c.op_geom = opg.id)
        (select (c.z_invert_up + n.height) from n))
    else null
    end) as z_vault_up,
    (select case when c.cross_section_type='circular' then
        (select (c.z_invert_down+c.circular_diameter))
    when c.cross_section_type='ovoid' then
        (select (c.z_invert_down+c.ovoid_height))
    when c.cross_section_type='pipe' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.closed_parametric_geometry cpg
            where c.cp_geom = cpg.id)
        (select (c.z_invert_down + n.height) from n))
    when c.cross_section_type='channel' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.open_parametric_geometry opg
            where c.op_geom = opg.id)
        (select (c.z_invert_down + n.height) from n))
    else null
    end) as z_vault_down,
    (select case when c.cross_section_type='channel' then
        (select (c.z_invert_up))::double precision
    else (select z_ground from ${model}.manhole_node where p.up = id)::double precision
    end) as z_tn_up,
    (select case when c.cross_section_type='channel' then
        (select (c.z_invert_down))::double precision
    else (select z_ground from ${model}.manhole_node where p.down = id)::double precision
    end) as z_tn_down

   from ${model}._pipe_link c,
    ${model}._link p
 where p.id = c.id
 ;;
