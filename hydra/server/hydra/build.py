# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
database project and models creation functions

USAGE

    python3 -m hydra.build project srid

    python3 -m hydra.build model srid version

    prints corresponding sql statements on stdout. This is usfull for maintenance of an
    old database, or to experiement while developping.

"""

import os
import json
import re
import string
import glob

__currendir = os.path.dirname(__file__)
instances = json.load(open(os.path.join(__currendir, "hydra.instances.json")))

def build(directory, version):
    """save extension hydra--<version>.sql in directory along with associated control file"""
    statements = []

    # special case of migration from 0.0.18: the hydra schema is unchanged,
    # we want to be able to create the extension without touching that

    statements.append("create schema hydra")
    statements.append("create schema work")

    for table in instances.keys():
        # create associated enum
        statements.append("create type hydra_%s as enum (%s)"%(table, ', '.join(["'%s'"%(k) for k in instances[table].keys()])))
        first = next(iter(instances[table].values()))
        if "nb_param" in first:
            statements.append("create table hydra.{table}("
                    """name hydra_{table} primary key,
                    id integer unique,
                    nb_param integer not null,
                        unique(name, nb_param),
                    description varchar \
                    )""".format(table=table))
        else:
            statements.append("""create table hydra.{table}(
                    name hydra_{table} primary key,
                    id integer unique,
                    description varchar{complement}
                    )""".format(table=table, complement=
                        (",\n       abbreviation varchar unique" if "abbreviation" in first else "")+
                        (",\n       el_hydra_no integer" if "el_hydra_no" in first else "")+
                        (",\n       li_hydra_number integer" if "li_hydra_number" in first else "")
                    ))

        for name, instance in instances[table].items():
            instance["table"] = table
            instance["name"] = name
            def quote(s):
                return "'%("+s+")s'" if s in ['name', 'description', 'abbreviation'] else "%("+s+")d"
            cols = ['name'] + [key for key in list(instance.keys()) \
                    if key in ["id",
                               "abbreviation",
                               "description",
                               "el_hydra_no",
                               "li_hydra_number",
                               "nb_param"]]

            statements.append((("insert into hydra.%(table)s("+",".join(cols)+") " \
                    "values ("+",".join([quote(c) for c in cols ])+")")%instance))

    statements.append("""create table hydra.metadata(
        creation_date timestamp not null default current_date,
        srid integer references spatial_ref_sys(srid),
        precision real not null default .01,
        workspace varchar(256) not null default '',
        version varchar(24) not null default '{}',
        id integer unique not null default(1) check (id=1) -- only one row
        )""".format(version))

    # indenting
    statements = [re.sub(r' {5,}', '    ', str(s)) for s in statements]

    statements.append("""create function hydra.metadata_after_insert_fct()
        returns trigger
        language plpython3u
        as $$
            from {hydra} import create_project
            import plpy
            for statement in create_project(TD['new']['srid'], '{version}'):
                plpy.execute(statement)
        $$""".format(
            hydra='hydra_'+'_'.join(version.split('.')),
            version=version
            ))

    statements.append("""create trigger hydra_metadata_after_insert_trig
        after insert on hydra.metadata
        for each row execute procedure hydra.metadata_after_insert_fct()
        """)



    with open(os.path.join(directory, "hydra--{}.sql".format(version)), 'w') as sql:
        for statement in statements:
            #print statement
            sql.write(statement+';\n')

    for upgrade_script in glob.glob(os.path.join(__currendir, "hydra--*--*.sql")):
        with open(os.path.join(directory, os.path.split(upgrade_script)[1]), 'w') as sql:
            sql.write(open(upgrade_script).read())


    open(os.path.join(directory, "hydra.control"), 'w').write(
            "# hydra extension\n"
            "comment = 'Hydraulic model for hydra QGIS plugin'\n"
            "default_version = '{}'\n"
            "requires = 'postgis, plpython3u'\n"
            "relocatable = true\n".format(version))

def create_project(srid, version):

    statements = []

    statements.append("""create schema project""")
    statements.append("""create sequence project.unique_name_seq""")
    statements.append("""create function project.unique_name(base_name varchar)
        returns varchar
        language plpgsql
        as
        $$
            begin
                return base_name||nextval('project.unique_name_seq')::varchar;
            end;
        $$
        """)

    statements.append("""create view project.model as
        select table_schema as name from information_schema.tables where table_name='_singularity'
        """)

    statements.append("""create function project.model_instead_fct()
        returns trigger
        language plpython3u
        as $$
            from {hydra} import create_model
            import plpy
            for statement in create_model(TD['new']['name'], {srid}, '{version}'):
                plpy.execute(statement)
        $$""".format(
            hydra='hydra_'+'_'.join(version.split('.')),
            srid=srid,
            version=version
            ))

    statements.append("""create trigger model_intead_trig
        instead of insert on project.model
        for each row execute procedure project.model_instead_fct()
        """)

    statements += _create_statements("project_1_inflow.sql", {"srid":srid})
    statements += _create_statements("project_2_utility.sql", {"srid":srid, "hydra":'hydra_'+'_'.join(version.split('.')), "workspace":'$workspace'})
    statements += _create_statements("project_3_scenario.sql", {"srid":srid})

    substitutions = {name+"_rainfall_type" : "'%s'"%(name) for name, instance in instances["rainfall_type"].items()}

    template_statements = open(os.path.join(__currendir,
        "project_4_rainfall.template.sql")).read().split(";;")[:-1]

    for name, instance in instances["rainfall_type"].items():
        instance["name"] = name
        for statement in template_statements:
            subs = _define_subst(substitutions, instance)
            subs["tablename"] = "%s_%s"%(name, "rainfall")
            statement = string.Template(statement
                    ).substitute(subs).replace(", , ",", ").replace(", )", ")")
            statements.append(statement)

    statements += _create_statements("project_5_gage_rainfall.sql", {"srid":srid})

    return statements

def create_result(workspace, scenario, model, srid):
    substitutions = {"w15":os.path.join(workspace, scenario, 'hydraulique', scenario+'_'+model+'.w15'), "model":model, "srid":str(srid), "scenario":scenario, "result":'result_'+scenario.lower()+'_'+model.lower()}
    return _create_statements("result.sql", substitutions)

def create_model(model, srid, version):
    "create model schema and related tables"

    statements = []

    substitutions = {"model":model, "srid":str(srid), "version":version, "hydra":'hydra_'+'_'.join(version.split('.'))}
    instances = json.load(open(os.path.join(__currendir, "hydra.instances.json")))

    # define type to id map
    for abstraction in ["node", "cross_section", "singularity", "link"]:
        for name, instance in instances[abstraction+"_type"].items():
            substitutions[name+"_"+abstraction+"_type"] = "'%s'"%(name)

    statements += _create_statements("model_1.sql", substitutions)

    statements += _create_statements("model_2_domain.sql", substitutions)


    #define nodes
    for abstraction in ["node"]:
        template_statements = open(os.path.join(__currendir,
            "model_%s.template.sql"%abstraction)).read().split(";;")[:-1]
        for name, instance in instances[abstraction+"_type"].items():
            instance['name'] = name
            #if "decl" not in instance:
            #    continue
            for statement in template_statements:
                subs = _define_subst(substitutions, instance)
                subs["tablename"] = "%s_%s"%(name, abstraction)
                statement = string.Template(statement
                        ).substitute(subs).replace(", , ",", ").replace(", )", ")")
                statements.append(statement)

    statements += _create_statements("model_3_cross_section.sql", substitutions)
    statements += _create_statements("model_4_singularity.river_cross_section_profile.sql", substitutions)

    for abstraction in ["singularity", "link"]:
        template_statements = open(os.path.join(__currendir,
            "model_%s.template.sql"%abstraction)).read().split(";;")[:-1]
        for name, instance in instances[abstraction+"_type"].items():
            instance['name'] = name
            #if "decl" not in instance:
            #    continue
            for statement in template_statements:
                subs = _define_subst(substitutions, instance)
                subs["tablename"] = "%s_%s"%(name, abstraction)
                statement = string.Template(statement
                        ).substitute(subs).replace(", , ",", ").replace(", )", ")")
                statements.append(statement)

    statements += _create_statements("model_5_functions.sql", substitutions)

    link_constrains = " or ".join(["(link_type=$"+name+"_link_type"\
            " and ("+instance["link_check"]+"))" \
            for name, instance in instances["link_type"].items()])
    statements.append("alter table "+model+"._link \
            add constraint "+model+"_link_connectivity_check_cstr \
            check("+string.Template(link_constrains).substitute(substitutions)+")")

    singularity_constrains = " or ".join(["(singularity_type=$"+name+"_singularity_type"\
            " and ("+instance["node_check"]+"))" \
            for name, instance in instances["singularity_type"].items()])
    statements.append("alter table "+model+"._singularity \
            add constraint "+model+"_singularity_connectivity_check_cstr \
            check("+string.Template(singularity_constrains).substitute(substitutions)+")")


    for type_ in ["node", "link", "singularity"]:
        addition = [
                "up is not null",
                "down is not null",
                "up_type is not null",
                "down_type is not null"] if type_ == "link" else []

        invalidity_reason = "\nunion\n".join(["select id, "+
            "||".join(
                ["case when {cond} then '' else '   {quoted}   ' end".format(
                    cond=cond, quoted=cond.replace("'", "''"))
                    for cond in def_["validity"]+addition]) +
            " as reason from $model.{}_{} as new where not validity".format(subtype, type_)
            for subtype, def_ in instances[type_+"_type"].items()
                if "validity" in def_ and len(def_["validity"])])
        substitutions[type_+"_invalidity_reason"] = string.Template(invalidity_reason).substitute(substitutions)

    statements += _create_statements("model_6_utility.sql", substitutions)
    return statements

def _define_subst(subs, instance):
    ret = subs
    for d in instance["decl"]:
        assert(len(d.split()) >= 2)
    ret["tabledeclare"] = "" if not len(instance["decl"]) else (
            ",\n    "+",\n    ".join([
                string.Template(d).substitute(subs) for d in instance["decl"]]))
    attributes_type = [d.split()[1] for d in instance["decl"] if d[0]!=" "]
    attributes = [d.split()[0] for d in instance["decl"] if d[0]!=" "]
    no_config = instance["not_configurable"] if "not_configurable" in instance.keys() else []
    ret["tableview"] = ", ".join(["c."+a+('::varchar' if t=='json' else '') for a,t in zip(attributes, attributes_type)] +\
            ([string.Template(d).substitute(subs) for d in instance["view"]] if "view" in instance else [])
          + ["c."+a+" as "+a+"_json" for a,t in zip(attributes, attributes_type) if t=='json'])

    ret["tableinsert"] = ", ".join(
        [("new."+a+('::json' if t=='json' else '') if a not in instance["default"]\
                   else "coalesce(new."+a+('::json' if t=='json' else '')+", "+string.Template(instance["default"][a]).substitute(subs)+")")
         for a,t in zip(attributes, attributes_type)])
    ret["tableupdate"] = ", ".join([a+"=new."+a+('::json' if t=='json' else '') for a,t in zip(attributes, attributes_type)])
    ret["oldconfigfields"] = ", ".join(["old."+a for a,t in zip(attributes, attributes_type) if a not in no_config])
    if ret["oldconfigfields"]=='':
        ret["oldconfigfields"]="'no_column'"
    ret["newconfigfields"] = ", ".join(["new."+a for a,t in zip(attributes, attributes_type) if a not in no_config])
    if ret["newconfigfields"]=='':
        ret["newconfigfields"]="'no_column'"
    ret["commentupdate"] = "" if len(attributes) else "--"
    ret["tableid"] = "'%s'"%(instance["name"])
    ret["constraingeom"] = string.Template(instance["constraingeom"]).substitute(subs) if "constraingeom" in instance else "new.geom"
    ret["validity"] = string.Template(" and ".join(["(%s)"%(rule)
                for rule in instance["validity"]])).substitute(ret) \
                        if "validity" in instance and len(instance["validity"]) else "'t'::boolean"
    return ret

def _create_statements(fileName, substitutions={}):
    return [string.Template(statement).substitute(substitutions)
        for statement in open(os.path.join(__currendir, fileName)
            ).read().split(";;")[:-1]
        ]

if __name__ == "__main__":

    import sys

    if sys.argv[1] == "project":
        for statement in create_project(int(sys.argv[2]), sys.argv[3]):
            print(statement)

    if sys.argv[1] == "model":
        for statement in create_model(sys.argv[2], int(sys.argv[3]), sys.argv[4] if len(sys.argv) == 5 else 'dev'):
            print(statement)