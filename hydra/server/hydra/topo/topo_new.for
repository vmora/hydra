
c
c--- programme toponew : lecture des donnees de fichier topo et construction des donnees de sections geometriques 
c    au format hydra, 
c
c      include "device.ncl"
      include "topo_new.ncl"
      logical lexist
      character cname*24
      character ligne*256
      dimension idvprf(4)
      dimension z1(6),b1(6),z2(6),b2(6)
c      
      read(5,*) s_rg, s_rd, np
      do n=1,np
          read(5,*) s(n),z(n)
      end do
      iend=0
      do while(iend.eq.0)
          read(5,"(a)",iostat=io) ligne
          ideb = verify(ligne," ")
          if(io.ne.0)then
              write(0,*)" stop : probleme de lecture dans le&
     & fichier topo_in.dat"
              stop
          end if
          if(ideb.eq.0)cycle
          read(ligne,*)mode
          if(mode.ne.0)then
              read(5,*) pk1,pk,pk2
              read(5,*)np1
              do n=1,np1
                  read(5,*)z1(n),b1(n)
              end do
              do n=np1+1,6
                  z1(n)=z1(n-1)
                  b1(n)=b1(n-1)
              end do          
              read(5,*)np2
              do n=1,np2
                  read(5,*)z2(n),b2(n)
              end do
              do n=np2+1,6
                  z2(n)=z2(n-1)
                  b2(n)=b2(n-1)
              end do
          end if
          iend=1
      end do
          
      np1=0
      np2=0
      iflag=0
      do n=1,np
          if(s(n).ge.s_rg .and. iflag.eq.0)then
              np1=n
              iflag=1
          end if
          if(s(n).ge.s_rd)then
            np2=n
            exit          
          end if
      end do
      if(np1.eq.0 .or. np1.ge.np  .or. np2.eq.0  .or. np2.ge.np)then
          write(0,*) "** pb detecte : abscisse des berges  en dehors de&
     & l'intervalle des abscisses du profil"
          stop
      end if
      nn=0
      do n=1,np1
          nn=nn+1
          sx(nn)=s(n)
          zx(nn)=z(n)
          ind(nn)=1
      end do
      nn=nn+1
      sx(nn)=sx(nn-1)+0.01
      zx(nn)=zx(nn-1)
      ind(nn)=2
      do n=np1+1,np2
          nn=nn+1
          sx(nn)=s(n)
          zx(nn)=z(n)
          ind(nn)=2
      end do
      nn=nn+1
      sx(nn)=sx(nn-1)+0.01
      zx(nn)=zx(nn-1)
      ind(nn)=3
      do n=np2+1,np
          nn=nn+1
          sx(nn)=s(n)
          zx(nn)=z(n)
          ind(nn)=3
      end do
          
      
      long=np+2
      long_sav=long
      hmin=0.5   
      longh1=6
      longh2=4
      longh3=4
      idvtmp=-1

      call TopSec_Traitement_SectionComplete(sx,xdecal,zx,ind,long,
     & cname, hmin,zhydra,bhydra,LongH1,LongH2,LongH3,
     & istat)   
      if(istat.ne.0)then
        write(0,*)"erreur detectee dans routine &
     & TopSec_Traitement_SectionComplete"
        stop
      end if
      
      if(mode.ne.0)then
c         calcul geom lit mineur par interpolation avec donnees des sections adjacentes
          b=s_rd-s_rg
          call geom_lmin(pk1,pk2,pk,b,z1,b1,z2,b2,zhydra,bhydra)
      end if
      zberge=amin1(zhydra(7),zhydra(11))
      zdecal=amax1(zhydra(6)-zberge, 0.)
c      write(0,*) zdecal
c      write(0,*) zhydra
c      write(0,*) bhydra
c      write(0,*) 'end debug'

      write(6,'(6(f7.2,1x))') (zhydra(i) - zdecal, i=1,6)
      write(6,'(6(f7.2,1x))') (bhydra(i), i=1,6)
      write(6,'(4(f7.2,1x))') (zhydra(i), i=7,10)
      write(6,'(4(f7.2,1x))') (bhydra(i), i=7,10)
      write(6,'(4(f7.2,1x))') (zhydra(i), i=11,14)
      write(6,'(4(f7.2,1x))') (bhydra(i), i=11,14)

      stop
      end




      subroutine geom_lmin(pk1,pk2,pk,b0,z01,b01,z02,b02,z,b)
      dimension z01(*),b01(*),z02(*),b02(*),z(*),b(*)
      dimension z1(6),b1(6),z2(6),b2(6)
      r1=(pk-pk1)/(pk2-pk1)
      dz1=(z01(6)-z01(1))/5
      do i=1,6
          z1(i)=z01(1)+ float(i-1)*dz1
      end do
      dz2=(z02(6)-z02(1))/5
      do i=1,6
          z2(i)=z02(1)+ float(i-1)*dz2
      end do
      call nterpn(z01,b01,6,z1,b1,6)
      call nterpn(z02,b02,6,z2,b2,6)
          
      r=(pk-pk1)/(pk2-pk1)
      do i=1,6
          z(i)=r*z2(i)+ (1.-r)*z1(i)
          b(i)=r*b2(i)+ (1.-r)*b1(i)
      end do
      coef=b0/b(6)
      do i=1,6
          b(i)=coef*b(i)
      end do
      return
      end

