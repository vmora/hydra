      subroutine nterp(x0,y0,n,x,y)
      dimension x0(*),y0(*)
      if(x.lt.x0(1).or.x.gt.x0(n))call er_nterp(x0(1),x0(n),x)
      doi=2,n
      if(x.le.x0(i))then
      r=(x-x0(i-1))/(x0(i)-x0(i-1))
      y=y0(i-1)+r*(y0(i)-y0(i-1))
      return
      end if
      end do
      write(0,201)
  201 format('erreur de coherence ds p. nterp')
      stop
      end
c
c
      subroutine nterpn(x0,y0,n0,x,y,np)  
      dimension x0(*),y0(*),x(*),y(*)
c      
c      if(n0.le.np)then !---------  v2.12', 17/02/2011
c        do k=1,n0
c          x(k)=x0(k)
c          y(k)=y0(k)
c        end do
c        do k=n0+1,np
c          x(k)=x0(n0)
c          y(k)=y0(n0)
c        end do
c
c      else            
        do n=1,np
          if(x(n).lt.x0(1))then
            y(n)=y0(1)
          else if(x(n).ge.x0(n0))then
            y(n)=y0(n0)
          else
            i=2
            do while(x(n).ge.x0(i))
              i=i+1
            end do
            r=(x(n)-x0(i-1))/(x0(i)-x0(i-1))
            y(n)=y0(i-1)+r*(y0(i)-y0(i-1))
          end if
        end do
c      end if
      return  
      end
c
c
c
      subroutine nterpb(x0,y0,n,x,y,j)
      dimension x0(*),y0(*)
      if(x.lt.x0(1).or.x.ge.x0(n))call er_nterp(x0(1),x0(n),x)
      do i=2,n
          if(x.le.x0(i))then
              r=(x-x0(i-1))/(x0(i)-x0(i-1))
              y=y0(i-1)+r*(y0(i)-y0(i-1))
              j=i-1
              return
          end if
      end do
      write(0,201)
  201 format('erreur de coherence ds p. nterp')
      stop
      end
c
c
c
      subroutine er_nterp(x1,x2,x)
      write(0,201)x,x1,x2
  201 format(' ** erreur ds p. nterp **'/
     x       '    valeur a interpoler en dehors des limites',
     x       ' d interpolation'/
     x       'x,x1,x2 : ',3g10.3/)
      stop
      end
c
c
c
* * * * * * * * * * * * * * * * * * *
      subroutine xtoy(z1,q1,n,z,f,kk)
      dimension z1(*),q1(*)
      if(z.le.z1(1)) go to 6
      if(z.ge.z1(n))go to 7
      do 2 i=2,n
      if(z1(i).lt.z)goto2
      f=q1(i-1)+(z-z1(i-1))*(q1(i)-q1(i-1))/(z1(i)-z1(i-1))
      kk=i-1
      return
    2 continue
    7 kk=n
      f=q1(n)
c      write(iout,999)(z1(l),q1(l),l=1,n),z
c      if(iclra.gt.0)write(ioprnt,999)(z1(l),q1(l),l=1,n),z
      return
    6 kk=0
      f=q1(1)
c      write(iout,999)(z1(l),q1(l),l=1,n),z
c      if(iclra.gt.0)write(ioprnt,999)(z1(l),q1(l),l=1,n),z
      return
c  999 format(' attention dans p. xtoy '/(2g12.3))
      end
c
c
c

      subroutine upcase(nom,n)
      character*1 nom(*)
      integer upper
      do 10 i=1,n
          if (lge(nom(i), 'a') .and. lle(nom(i), 'z')) then
              upper = ichar(nom(i)) - 32
              nom(i) = char(upper)
          endif
10    continue
      return
      end
c
c
c
      subroutine slec_star(idv,c08)
      character*8 c8,c1*1,c08
      equivalence(c1,c8)
    1 continue
      read(idv,'(a)',end=100)c8
      if(c1.eq.'*')then
        if(c8.eq.c08)return
      end if
      go to 1
  100 continue
      write(0,201)
  201 format( ' p de lecture de donnees : mot cle non trouve : ',a)
      stop
      end
c
c
c
      subroutine lecstar_next_cmd(cid0,iflag) 
c
c--- recherche d un mot cle ds le fichier idvscen
c
      character*8 cid0,cid,c1(80)*1
      equivalence (c1(1),cid)
      iflag=0
      rewind(idvcmd)
    1 continue
  101 format(80a1)    
      read(idvcmd,101,end=500)(c1(k),k=1,80)
      if(c1(1).eq.'*')then
         k=1
         don=2,80
         if(c1(n).ne.' ')then
            c1(k)=c1(n)
            c1(n)=' '
            k=k+1
         endif
         end do
         call upcase(cid,8)
         if(cid.eq.cid0)then
            iflag=1
            return
         endif
      endif
      goto1
  500 return
      end
c
c
c  
c
      subroutine indx_dclassement(x,n,iordre,indx)
c      implicit real*8 (a-h,o-z)
      dimension x(n),indx(n)
      logical b
      character*20 x,petit
! -- classement de x(n) par ordre decroissant
!      la routine renvoie dans indx(n) les indices i de x(i),
!      par x(i) croissant si iordre=1
!      par x(i) decroissant si iordre=-1
!    le tableau x(n) reste tel quel
      do j=1,n
        indx(j)=-1
      end do
      do j=1,n
        k=1
        do while(indx(k).ge.1)
          k=k+1
        enddo
        petit=x(k)
        npetit=k
        do i=k+1,n
          if (iordre.eq. 1) b=x(i).lt.petit
          if (iordre.eq.-1) b=x(i).gt.petit
          if (b.and.indx(i).le.0) then
            petit=x(i)
            npetit=i
          endif
        enddo
        indx(j)=(npetit+1)*indx(j)
        indx(npetit)=abs(indx(npetit))
      enddo
      do j=1,n
        indx(j)=indx(j)-1
      enddo
      return
      end
c
c
c
      subroutine stri(x,ntot,irang)
c
c--- classement du tableau x(n) par ordre decroissant de valeurs
c    le rang de chaque element est calcule dans irang()
c
      dimension x(*),irang(*)
      character*20 x
      irang(1)=1
      do n=2,ntot
      call stri1(x,irang,n-1,x(n),k)
      if(k.eq.0)then
        do i=n,2,-1
      	irang(i)=irang(i-1)
        end do
        irang(1)=n
      elseif(k.eq.n-1)then
        irang(n)=n
      else
        do i=n,k+2,-1
      	irang(i)=irang(i-1)
        end do
        irang(k+1)=n
      end if
      end do
      return
      end
c
c
c
      subroutine stri1(x,irang,n,y,k)
      dimension x(n),irang(n)
      character*20 x,y
      if(y.ge.x(irang(1)))then
        k=0
        return
      elseif(y.le.x(irang(n)))then
        k=n
        return
      else
        i1=1
        i2=n
    1   i=(i1+i2)/2
        if(x(irang(i)).gt.y)then
          i1=i
        else
          i2=i
        end if
        if((i2-i1).le.1)then
          k=i1
          return
        else
          goto1
        end if
      end if
      return
      end
c
c
      function ifunc_rec(cidx,cid_obj,irang,numnp)
c
c--- recherche du rang de cidx dans le tableau cid_obj()
c
      dimension cid_obj(*),irang(*)
      character*20 cidx,cid_obj,c20*20
c
c--- initialisation
c
      ifunc_rec=0
      tmp=numnp
      i1=1
      i2=numnp
      nc=0
      ncmax=int(2.*alog(tmp)/alog(2.))
c
      if(cidx.eq.cid_obj(irang(i1)))then
        ifunc_rec=irang(i1)
        return
      elseif(cidx.eq.cid_obj(irang(i2)))then
        ifunc_rec=irang(i2)
        return
      end if
c
    1 continue
      if(nc.ge.ncmax)go to 500
      i=(i1+i2)/2
      if(cidx.eq.cid_obj(irang(i)))then
        ifunc_rec=irang(i)
        return
      else
      if(i2-i1.le.1)go to 500
        if(cidx.gt.cid_obj(irang(i)))then
c           i1=i
           i2=i
           nc=nc+1
           go to 1
        elseif(cidx.lt.cid_obj(irang(i)))then
c           i2=i
           i1=i
           nc=nc+1
           go to 1
        end if
      end if
  500 continue
c
c--- recherche a echoue
c
      return
      end
c
c
c
