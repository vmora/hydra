!----------------------------------------------------------------------------------------------------------------------------
!
!   Transcription d'une section topographique au format CSV vers le format HYDRA
!   V.Ch - novmenbre 2011
!
!----------------------------------------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------------------------------------
!     Traitement d'un profil en travers complet
!----------------------------------------------------------------------------------------------------------------------------

subroutine TopSec_Traitement_SectionComplete(x,xdecal,y,ind,long,cname,hmin,zhydra,bhydra,LongH1,LongH2,LongH3,istat)
!
!--- parametres & tableaux re�us
!        
      character*(*) cname        ! identifiant du profil topo
      integer long               ! nombre de points d�crivant le profil (longueur des tableaux x y ind)
      dimension x(long),y(long)  ! tableau distances cumul�es - altitudes du profil
      dimension ind(long)        ! indicateur de point de lit mineur (2), lit maj RG (2), lit maj RD (3)
      !    LONG peut �tre augment� de 2 => x y et ind doivent disposer d'au moins deux mots libres au del� du rang LONG (avec valeur re�ue)
      dimension zhydra(14),bhydra(14) ! tableaux de destination de la section "Hydra"
      integer LongH1,LongH2,LongH3 ! longueurs repectives lit mineur hydra (max 6), l maj RG et lmaj RD (max 4)
      !    les lits majeurs RG et RD commencent toujours � zhydra(7) et zhydra(11)
      real hmin         ! profondeur minimale requise pour le lit mineur
      integer istat     ! statut d'erreur, 0=pas d'erreur , <>0 si erreur
!
!--- tableaux locaux de longueur fixe
!        
      integer,parameter :: nprfmax=1000 ! nombre max de sous-sections (profils partiels monotones dans le tableau x-y)
      dimension indxprf(nprfmax)        ! indice de d�part du sous-profil i dans le tableau X-Y
      dimension longprf(nprfmax)        ! longueur du profil i dans le tableau x-y
      dimension indxprf2(nprfmax)       ! indice de d�part correspondant dans le tableau Z-B
      dimension alpha(nprfmax),tho(nprfmax) ! utilis�s par spr opera
      dimension indxberg(2)             ! indice des 1ers et derniers points du lit mineur
      dimension indxLmin(2)             ! indice des 1ers et derniers points du lit mineur correspondant a la berge la + basse. L'un est egal a infxberg(i)
!
!--- tableaux locaux de longueur variable
!        
      real,allocatable :: z(:),b(:),s(:)
!         z(longmax), b(longmax) : tableaux locaux allou�s pour les lois altitudes - largeurs
!         s(longmax)             : tableau local allou� pour la section mouill�e en fonction de l'atitude z(longmax)
!
!--- allocation de m�moire
!
      longmax=long*2+nprfmax+20
      allocate(z(longmax),b(longmax),s(longmax),stat=istat) ; if (istat.ne.0) goto 91
!
!--- rep�rage des changements lit mineur / lit majeur
!
      call TopSec_ReperageIndxberg(x,y,ind,long,indxberg)
      call TopSec_Recalage_PointsDebordement(x,y,long,indxberg,indxLmin,&
     & cname,hmin,istat)

      xdecal=0.5*(x(indxLmin(1))+x(indxLmin(2)))
!        
!--- traitement du lit mineur
!
!        longLmin=indxberg(2)-indxberg(1)+1
      longLmin=indxlmin(2)-indxlmin(1)+1
!        write(*,'(2a,i5)') 'profil: ',trim(cname),longlmin
      call TopSec_Traitement_Section(cname,2,x(indxlmin(1)),y(indxlmin(1)),&
     & longLmin,z,b,s,longmax,nprf,indxprf,indxprf2,longprf,nprfmax,&
     & zhydra(1),bhydra(1),LongH1,6,istat)
!
!--- traitement du lit majeur RG
!
      longLmajrg0=indxberg(1)-1
      if (indxberg(1).eq.indxlmin(1).and.indxberg(1).ne.1) then
         longLmajrg=indxberg(1)-1
         indxfin=indxberg(1)-1
      else
         longLmajrg=indxberg(1)-1+1
         indxfin=indxberg(1)
      endif
      call TopSec_AjustementLMaj_SurCoteDebo(x(1),y(1),longLmajRG,zhydra(longH1))  ! pas de cote < au dernier point l.min
      call TopSec_AjustementLMaj_SurCoteDebo(x(1),y(1),longLmajRG,y(longLmajRG))   ! pas de cote < au 1er point l.maj
      call TopSec_Traitement_Section(cname,1,x(1),y(1),longLmajRG,z,b,s,&
     & longmax,nprf,indxprf,indxprf2,longprf,nprfmax,zhydra(7),bhydra(7),&
     & LongH2,4,istat)
      !--- ajustement par rapport au 1er point lit mineur
        dx=x(indxLmin(1))-x(indxfin)
        do i=7,6+longh2
          bhydra(i)=bhydra(i)+dx
          if (zhydra(i).lt.zhydra(longh1)) zhydra(i)=zhydra(longh1)
        enddo
!
!--- traitement du lit majeur RD
!
      longLmajrd0=long-indxberg(2)
      if (indxberg(2).eq.indxlmin(2).and.indxberg(2).ne.long) then
        longLmajrd=long-indxberg(2)
        indxdeb=indxberg(2)+1
      else
        longLmajrd=long-indxberg(2)+1
        indxdeb=indxberg(2)
      endif
      call TopSec_AjustementLMaj_SurCoteDebo(x(indxberg(2)+1),y(indxberg(2)+1),&
     & longLmajRD0,zhydra(longH1))   ! pas de cote < au dernier point l.min
      call TopSec_AjustementLMaj_SurCoteDebo(x(indxberg(2)+1),y(indxberg(2)+1),&
     & longLmajRD0,y(indxdeb))       ! pas de cote < au 1er point l.maj
      call TopSec_Traitement_Section(cname,3,x(indxdeb),y(indxdeb),&
     & longLmajRD,z,b,s,longmax,nprf,indxprf,indxprf2,longprf,nprfmax,&
     & zhydra(11),bhydra(11),LongH3,4,istat)
      !--- ajustement par rapport au dernier point lit mineur
        dx=x(indxdeb)-x(indxlmin(2))
        do i=11,10+longh3
          bhydra(i)=bhydra(i)+dx
          if (zhydra(i).lt.zhydra(longh1)) zhydra(i)=zhydra(longh1)
        enddo
      
      deallocate(z,b,s,stat=istat)  ; if (istat.ne.0) goto 92
      istat=0
      return

   91   istat=1
      write(0,*) 'erreur allocate dans spr TopSec_Traitement_SectionComplete'
      stop
   92   istat=1
      write(0,*) 'erreur deallocate dans spr TopSec_Traitement_SectionComplete'
      stop
      end



!----------------------------------------------------------------------------------------------------------------------------
!     Traitement d'un lit mineur seul ou d'un lit majeur seul
!----------------------------------------------------------------------------------------------------------------------------

subroutine TopSec_Traitement_Section(cname,indic,x,y,long,z,b,s,longmax,nprf,indxprf,indxprf2,&
     & longprf,nprfmax,zhydra,bhydra,LongHydra,LongObj,istat)
!      x(long) y(long) : donn�e brute issue du fichier topo csv
!
!  Le profil est d'abord subdivis� en sous-sections tel que Y(X) soit monotone 
!
!   indic      : indicateur lit majeur RG (1), lit mineur (2), lit majeur RD (3)
!   nprf       : nombre de sous-sections (profils partiels monotones dans le tableau x-y)
!   nprfmax    : nombre maximal de sous-sections (dimension de tableaux)
!   indxprf(i) : indice de d�part du sous-profil i dans le tableau X-Y
!   longprf(i) : longueur du profil i dans le tableau x-y
!
!   z(*)    b(*)    : transcriptions compl�tes en tableaux Cote-Largeur des diff�rentes sous-sections
!   s(*)            : sections cumul�es: int�grale du tableau z-b
!   indxprf2(i): indice de d�part correspondant dans le tableau Z-B
!   longprf(i) : longueur du profil i dans le tableau z-b, identique � celle dans le tableau x-y
!   
!   indxprf2(nprf+1): indice de d�part dans le tableau Z-B du profil r�sultant de la sommation des diff�rents sous-profils
!                      (repr�sente profil de d�part sym�tris�, sans perte d'aire)
!   longprf(nprf+1): longueur de ce profil r�sultant de la sommation, dans le tableau Z-B
!   => il faut veiller a NPRF+1 <= NPRFMAX
!
!   ixnew = indxprf2(nprf+1) + longprf(nprf+1) : indice de d�part du tableau Z-B pour le profil simplifi� Hydra
!   longobj                                    : longueur objecfif pour le profil simplifi� (6 pour lit mineur, ...)
!   longnew                                    : longueur obtenue pour le profil simplifi� (< ou = � longobj)
!
!   0    : journal, suppos� ouvert
!   idvprf(4) : sorties de controles, non utilis�es si idvprf(1) < ou = 0. - idvprf(4)=fichier expdess
!

!
!--- declaration des tableaux re�us
!      
      dimension x(long),y(long),z(longmax),b(longmax),s(longmax)
      dimension indxprf(nprfmax),longprf(nprfmax)
      dimension indxprf2(nprfmax)
      dimension zhydra(LongObj),bhydra(LongObj)
!
!--- declaration des tableaux locaux
!      
      dimension alpha(nprfmax),tho(nprfmax)
      character*(*) cname

      TolEcart=0.1

!      write(0,'(2a,i5)') '+Profil ',cname,indic
!      if (cname.eq.'K89B_110') then
!        continue
!      endif
      istat=1
      if (long.eq.1) then ! cas avec un seul point (lit majeur RG ou RD notamment)
        LongHydra=1
        zhydra(1)=y(1)
        bhydra(1)=0.
        if (indic.ne.2) istat=0
        return
      elseif (long.le.0) then
        LongHydra=0
        return
      endif
      
      !- d�composition en sous profils monotones
      call TopSec_DecompProfilXY(indic,x,y,long,nprf,indxprf,longprf,nprfmax,istat)
      if (istat.eq.1) then ! (nprf.ge.nprfmax) then
          write(0,'(a,i1)') 'erreur dans spr TopSec_Traitement_Section sur profil '//trim(cname)//' - ',indic
          write(0,'(a)') 'limite atteinte pour les tableaux de longueur NPRFMAX'
          write(0,*) 'istat:',istat
          stop
!           remarque: indice nprf+1 utilis� par spr opera. il faut donc nprf+1 <= nprfmax
      endif

      !- conversion des sous-profils X-Y en tableaux Z-B
      do i=1,nprf
        if (indxprf(i).le.0.or.indxprf(i).ge.longmax) then
            write(0,*) 'erreur sur tableau indxprf'
            stop
        end if
        if (i.eq.1) then
          indxprf2(i)=1
        else
          indxprf2(i)=indxprf2(i-1)+longprf(i-1)
        endif
        call TopSec_TableauXZ_to_TableauZB(x(indxprf(i)),y(indxprf(i)),longprf(i),z(indxprf2(i)),b(indxprf2(i)),istat)
        if (istat.ne.0) then
          write(0,'(a,i1)') 'erreur dans spr TableauXZ_to_TableauZB sur profil '//trim(cname)//' - ',indic
          write(0,'(a,i4)') 'sous section n�:',i
          write(0,*) 'istat:',istat
          stop
        endif
      enddo

      !- sommation des tableaux Z-B des sous-profils
      do i=1,nprf
        alpha(i)=1.
        tho(i)=0.
      enddo
      if (nprf.eq.1) then
        indxprf2(nprf+1)=indxprf2(1)
        longprf(nprf+1)=longprf(1)
      else
        call opera(z,b,indxprf2,longprf,alpha,tho,nprf,longmax,idvout,istat)
      endif
      ix0=indxprf2(nprf+1)
      long0=longprf(nprf+1)
    
      !- �criture du tableau obtenu sous la forme X-Y pour contr�le.

      If (long0.GT.Longobj) then
      
        !- recherche loi en 6 points
        ixnew=indxprf2(nprf+1)+long0
        if (ixnew+longObj.gt.LONGMAX) then ! if (ixnew+long0.gt.LONGMAX) then
          write(*,*) 'ixnew,long0,LONGMAX',ixnew,long0,LONGMAX
          stop
        endif

        call TopSec_Reduction(z(ix0),b(ix0),long0,z(ixnew),b(ixnew),longnew,LongObj,TolEcart,indic,cname)

      else
        !- si le profil de base a d�j� moins de 6 points: pas de traitement
        ixnew=indxprf2(nprf+1)
        longnew=longprf(nprf+1)

      endif

        !- si lit mineur: ajustement pour conserver la section de plein bord
        if (indic.eq.2) then
          call Integrale(long0,z(ix0),b(ix0),1.,s(ix0))
          call Integrale(longnew,z(ixnew),b(ixnew),1.,s(ixnew))
          call TopSec_Ajustement_sur_sections(z(ix0),s(ix0),long0,z(ixnew),b(ixnew),s(ixnew),longnew)
          call Integre(longnew,z(ixnew),b(ixnew),1.,stmp)
          if (abs((stmp-s(ix0-1+long0))/stmp).gt.1.E-3) then
            write(0,801) trim(cname),stmp,s(ix0-1+long0),stmp-s(ix0-1+long0)
            write(*,801) trim(cname),stmp,s(ix0-1+long0),stmp-s(ix0-1+long0),(stmp-s(ix0-1+long0))*100/s(ix0-1+long0)
          endif
        endif

      !- transfert dans le tableau de retour
      LongHydra=longobj ! longnew
      do i=1,longnew
        zhydra(i)=z(ixnew-1+i)
        bhydra(i)=b(ixnew-1+i)
      enddo
      do i=longnew+1,Longobj
        zhydra(i)=zhydra(longnew)
        bhydra(i)=bhydra(longnew)
      enddo
      
        istat=0
      return

  801   format(' profil ',a,' : imprecision ou incoherence lors du controle section de plein bord',3f10.4,f6.1,'%')

end

!----------------------------------------------------------------------------------------------------------------------------

subroutine TopSec_AjustementLMaj_SurCoteDebo(x,y,longLmaj,zhydra6)
      dimension x(longLmaj),y(longLmaj)
      do i=1,longLmaj
      if (y(i).lt.zhydra6) y(i)=zhydra6
      enddo
      return
end

!----------------------------------------------------------------------------------------------------------------------------

subroutine TopSec_ReperageIndxberg(x,y,ind,long,indxberg)
! reperage des 1er et dernier point du lit mineur
!    indxberg(2) ! rangs des points de berge (1er et dernier point du lit mineur avec ityp=2)
      dimension x(long),y(long),ind(long),indxberg(2)
      indxberg(1)=0
      indxberg(2)=0
      if (ind(1).eq.2) indxberg(1)=1
      do i=2,long
        if (ind(i-1).eq.1.and.ind(i).eq.2) then
          indxberg(1)=i
        elseif (ind(i-1).eq.2.and.ind(i).eq.3) then
          indxberg(2)=i-1
        endif
      enddo
      if (ind(long).eq.2) indxberg(2)=long
      return
end

!----------------------------------------------------------------------------------------------------------------------------

      subroutine TopSec_Recalage_PointsDebordement(x,y,long,indxberg,indxLmin,cname,hmin0,istat)
    implicit none
      character*(*) cname
      integer long,indxberg,indxfond,indxLmin,istat,i,j,longold
      real x,y,yhautrg,yhautrd,xx,ymin,hmin0,hmin,ymax,ymaxold
      dimension x(long+3),y(long+3),indxberg(2),indxLmin(2)

      integer indxhaut(2)

!
!  indxhaut(2) : points les + hauts sur chaque berge (peuvent �tre en retrait des points indxberg)
!  indxLmin(1 ou 2): extr�mit�s du lit mineur, apr�s recalage sur le point de d�bordement le + bas
!                   l'un au moins des indxLmin(k) est �gal � indxhaut(k)

      longold=long
      if (cname.eq.'K137C_47') then
        continue
      endif

      indxhaut(1)=indxberg(1)
      yhautRG=y(indxberg(1))
      
      indxhaut(2)=indxberg(2)
      yhautRD=y(indxberg(2))
      
    indxfond=indxberg(1)
      ymin=y(indxberg(1))
      ymax=y(indxberg(1))
      do i=indxberg(1)+1,indxberg(2)
       if (y(i).lt.ymin) then
         indxfond=i
       ymin=y(i)
       endif
       if (y(i).gt.ymax) then
       ymaxold=ymax
       ymax=y(i)
       endif
      enddo
    hmin=amin1(ymax-ymin+0.05,hmin0)
      if (abs(amax1(yhautRG,yhautRD)-ymax).lt.0.01.and.abs(amin1(yhautRG,yhautRD)-ymaxold).lt.0.01) then
      ! cas o� les 2 berges constituent les 2 points les + hauts: pas de correction
      hmin=0. ! amin1(ymaxold-ymin+0.05,hmin0)
      endif
!
!--- la profondeur minimale hmin est-elle respect�e sur chaque berge ?      
!
      if (yhautRG-ymin.lt.hmin) then
        do j=long,indxberg(1),-1
        x(j+1)=x(j)
        y(j+1)=y(j)
      enddo
        long=long+1
      y(indxberg(1))=ymin+hmin
      yhautRG=ymin+hmin
      endif
      if (yhautRD-ymin.lt.hmin) then
        do j=long,indxberg(2)+1,-1
        x(j+1)=x(j)
        y(j+1)=y(j)
      enddo
        long=long+1
      indxberg(2)=indxberg(2)+1
      indxhaut(2)=indxhaut(2)+1
      x(indxberg(2))=x(indxberg(2)-1)
      y(indxberg(2))=ymin+hmin
      yhautRD=ymin+hmin
      endif
!        
!--- calculs des indxLmin (ajustemement sur berge la plus basse)
!
      if (yhautrg.le.yhautrd) then
      !- point de debordement le + bas: rive gauche
      indxLmin(1)=indxhaut(1)
      
      !- ajuster la limite lit mineur rive droite
      do i=indxhaut(2),indxfond,-1
        if (y(i).le.yhautrg+0.0001) exit
      enddo
      if (i.eq.indxfond-1) goto 90
      if (abs(yhautrg-y(i)).lt.0.01) then ! cote yhautrg intercept�e entre les points i et i+1
        indxLmin(2)=i
      else
        !- ajout du point de fin du lit mineur cot� RD
      xx=x(i)+(x(i+1)-x(i))/(y(i+1)-y(i))*(yhautrg-y(i))
        do j=long,i+1,-1
        x(j+1)=x(j)
        y(j+1)=y(j)
      enddo
        long=long+1
      indxberg(2)=indxberg(2)+1
      x(i+1)=xx         ! erreur corrig�e le 19/4/2012:
      y(i+1)=yhautrg    ! r�affectation de x(i+1) et y(i+1),  et non x(i) et y(i)
        indxLmin(2)=i+1   ! ... au lieu de =i
      endif


      else
      !- point de debordement le + bas: rive droite
      indxLmin(2)=indxhaut(2)
      !- ajuster la limite lit mineur rive gauche
      do i=indxhaut(1),indxfond
        if (y(i).le.yhautrd) exit
      enddo
      if (i.eq.indxfond+1) goto 90
      if (abs(yhautrd-y(i)).lt.0.01) then ! cote yhautrd intercept�e entre les points i-1 et i
        indxLmin(1)=i
      else
        !- ajout du point de fin du lit mineur cot� RG
      xx=x(i-1)+(x(i)-x(i-1))/(y(i)-y(i-1))*(yhautrd-y(i-1))
        do j=long,i,-1
        x(j+1)=x(j)
        y(j+1)=y(j)
      enddo
        long=long+1
      indxfond=indxfond+1
      indxberg(2)=indxberg(2)+1
      indxlmin(2)=indxlmin(2)+1
      x(i)=xx
      y(i)=yhautrd
        indxLmin(1)=i
      endif
      endif
      if (long-longold.ge.3) write(*,'(2a,i5)') 'long-longold.ge.3 pour ',trim(cname),long-longold

      return
 90   write(0,*) 'incoherence ds spr Recalage_PointsDebordement'
      stop
      end

!----------------------------------------------------------------------------------------------------------------------------

subroutine TopSec_DecompProfilXY(indic,x,y,long,nprf,indxprf,longprf,nprfmax,istat)
!
! d�composition d'un profil XY en tableaux XY partiels; tels que Y soit monotone.
!  indic: indicateur lit majeur RG (1), lit mineur (2), lit majeur RD (3)
!  nprf : nombre de profils partiels obtenuus
!  indxprf(i) : indice de d�part du profil i
!  longprf(i) : longueur du profil i
! le dernier point du profil i est aussi le 1er du profil i+1
!
      dimension x(long),y(long),indxprf(nprfmax),longprf(nprfmax)
      logical bool,bYcroissant
      
! controle croissance des X
      call ctlCroissance(1,x,long,'DecompProfilXY')

      nprf=1
      indxprf(1)=1
      bYcroissant=y(2).gt.y(1)

      longprf(1)=0
    do i=indxprf(1),long-1
      if (.not.bYcroissant .and. mod(nprf,2).eq.1) bool=y(i+1).le.y(i)
      if (.not.bYcroissant .and. mod(nprf,2).eq.0) bool=y(i+1).ge.y(i)
        if (  bYcroissant    .and. mod(nprf,2).eq.1) bool=y(i+1).ge.y(i)
        if (  bYcroissant    .and. mod(nprf,2).eq.0) bool=y(i+1).le.y(i)
      if (bool) then
        longprf(nprf)=longprf(nprf)+1 ! comptage du point i
      else
        longprf(nprf)=longprf(nprf)+1 ! comptage du point i dans le profil qui se termine
      nprf=nprf+1                   ! demarrage nouveau profil
      if (nprf.gt.nprfmax) then
        if (0.gt.0) write(0,*) 'erreur ds spr DecompProfilXY: depassement de tableaux'
        istat=1
        return ! call sstop() !  'erreur ds spr DecompProfilXY: depassement de tableaux'
      endif
      indxprf(nprf)=i
      longprf(nprf)=1               ! comptage du point i dans le profil qui commence
      endif
      if (i.eq.long-1) longprf(nprf)=longprf(nprf)+1 ! comptage du dernier point
      enddo

      istat=0
      return
end

!----------------------------------------------------------------------------------------------------------------------------

subroutine TopSec_TableauXZ_to_TableauZB(x,y,long,z,b,istat)
!
! transforme un tableau abscisse-altitude en altitude-largeur
! s'applique � une portion de profil (demi-profil) tel que y soit croissant
! k: indicateur si profil d�croissant, type rive gauche (1) ou croissant, type rive droite (2)
!
      dimension x(long),y(long)
      dimension z(long),b(long)
      logical bool
!
!--- controle des conditions d'execution
!
    if (long.lt.2) goto 91
      if (y(1).gt.y(long)) then
      k=1
      else
      k=2
      endif
      do i=2,long
      if (x(i).lt.x(i-1)) goto 93
      enddo
      do i=2,long
      if (k.eq.1) bool=y(i).le.y(i-1)
      if (k.eq.2) bool=y(i).ge.y(i-1)
      if (.not.bool) goto 92
      enddo
!
!--- production Z-B
!      
    if (k.eq.1) then
      do i=1,long
        j=long-i+1
      z(i)=y(j)
        b(i)=abs(x(j)-x(long))
      enddo
      else
      do i=1,long
        z(i)=y(i)
        b(i)=abs(x(i)-x(1))
      enddo
      endif
      istat=0
      return

 91  istat=11; return ! moins de 2 points
 92  istat=12;        ! croissance ou d�croissance y(x) selon "rive" non respect�e
     write(0,'(a,i4)') 'Erreur dans spr TableauXZ_to_TableauZB: croissance ou decroissance y(x) non respect�e au rang',i
       write(0,'(a)') 'Tableau X-Y:'
       write(0,'(2f10.3)') (x(i),y(i),i=1,long)
       return
 93  istat=13; call ctlCroissance(1,x,long,'TableauXZ_to_TableauZB') ; return ! x pas croissant
      end

!----------------------------------------------------------------------------------------------------------------------------

      subroutine TopSec_Reduction(z,b,long,znew,bnew,longnew,longObj,TolEcart,it,cname)
!
!      z(long)    b(long)    : tableau a reduire
!      znew(long) bnew(long) : zone m�moire pour r�sultats intermediaires et d�finitifs
!      longobj: objectif de longueur (pour lit mineur: 6)
!
        dimension z(long),b(long),znew(long),bnew(long)
        character*(*) cname

!          fx=0.0125 ! facteur de r�duction d'�chelle des X pour le spr de r�duction cpqhyd5

        !- facteur de r�duction d'echelle des B: = plage Z / plage B
        if (b(long)-b(1).gt.1..and.z(long)-z(1).gt.1.) then
          fx=(z(long)-z(1))/(b(long)-b(1))
        else
          fx=1.
        endif

        call cpqhyd5(it,long,z,b,znew,bnew,longnew,longobj,fx,istat)
        return

      end

!----------------------------------------------------------------------------------------------------------------------------


subroutine TopSec_Ajustement_sur_sections(z,s,long,znew,bnew,snew,longnew)
      dimension z(long),s(long),znew(longnew),bnew(longnew),snew(longnew)

    ! delta: correction � apporter sur les bnew(k) sauf dernier, et sauf premier si correction n�gative.
      ! scorr: correction sur la section globale

      ! premiere valeur de delta, minorante
      delta=2.*(s(long)-snew(longnew))/((znew(longnew)-znew(1)))
      if (abs(delta)/bnew(longnew).lt.0.001) then
      return
      elseif (delta.gt.0.) then
      sign=1.
      delta1=0.1*delta
      delta2=10.*delta
      else
      sign=-1.
      delta1=10.*delta
      delta2=0.1*delta
    endif
      
      ! dichotomie
      do while (abs(delta2-delta1)/bnew(longnew).gt.0.0001)
      delta=0.5*(delta1+delta2)
      if (sign.gt.0.) then
        scorr=2.*delta*(znew(2)-znew(1))/2.
      else
        scorr=1.*delta*(znew(2)-znew(1))/2.
      endif
      do k=2,longnew-2
        scorr=scorr+2.*delta*(znew(k+1)-znew(k))/2.
      enddo
      scorr=scorr+1.*delta*(znew(longnew)-znew(longnew-1))/2.
      if (scorr*sign.lt.0.) then
        write(*,*) 'pb signe'
        stop
        return
      endif
      if (snew(longnew)+scorr.gt.s(long)) then
        delta2=delta
      else
        delta1=delta
      endif
      enddo

      ! correction des largeurs
    do k=longnew-1,2,-1
      if (bnew(k)+delta.lt.bnew(k+1)) bnew(k)=bnew(k)+delta
      enddo
      if (sign.gt.0..and.bnew(1)+delta.lt.bnew(2)) bnew(1)=bnew(1)+delta

      return
end

!----------------------------------------------------------------------------------------------------------------------------

subroutine swapr(a,b)
  c=a
  a=b
  b=c
  return
end



subroutine Integrale(N,t,q,f,v)
      dimension t(n),q(n),v(n)
      v(1)=0.
      do i=2,n
      v(i) = v(i-1) + 0.5*(q(i)+q(i-1))*(t(i)-t(i-1))
      enddo
      v=v*f
      return
      end


subroutine Integre(N,t,q,f,v)
      dimension t(n),q(n)
      v=0.
      do i=2,n
      v = v + 0.5*(q(i)+q(i-1))*(t(i)-t(i-1))
      enddo
      v=v*f
      return
      end

!----------------------------------------------------------------------------------------------------------------------------

      subroutine cpqhyd5(imode,nlong,xold,yold,xnew,ynew,nnew,nobjectif,fy,istat)
!
! imode : si=2 : indique un lit mineur de section en travers
! fy: facteur de r�duction d'�chelle pour les Y (ou amplification si > 1), c'est � dire les LARGEURS de section si appel par prgm CsvToSec
! xold(nlong)    ,yold(nlong)     : hydrogramme � d�graisser, input (ou si section topo: xold=altitudes, yold=largeurs)
! xnew(nobjectif),ynew(nobjectif) : hydrogramme d�graiss� (output)
! nobjectif : longueur objectif recherch�e
! nnew      : longueur effective de xnew/ynew renvoy�, < ou = � nobjectif
! k(nlong)  : indicateur d'�limination du point: 0=�limin�, 1=conserv� - tableau local
! 0 : idv fichier journal. si 0: ecritures a l'�cran, si <0: pas d'�critures.
! istat  : statut d'erreur. =0 si pas d'erreur, <>0 si erreur

      implicit none
      integer k,n,n1,i,j,i1,i2,ii,nlong,nnew,nobjectif,nmin,imode,istat
      real xold,yold,xnew,ynew,yy,xtmp,ytmp,fy,aire,airep,h1,h2,h,x3,dymin
      dimension xold(nlong),yold(nlong),xnew(nlong),ynew(nlong)
      dimension k(nlong),yy(nlong),xtmp(2),ytmp(2)
      logical b
      real*4 a

! variables de controles      
      integer i1dmp,i2dmp

! controle croissance du tableau xold
   91 format('Erreur dans spr cpqhyd5: le tableau d''entree XOLD n''est pas croissant:',/,'Valeur decroissante au rang',i8,2f15.8)
   92 format('Tableau XOLD re�u:')
   93 format(10f8.3)
      istat=1
      do n=1,nlong-1
        if (xold(n+1).lt.xold(n)) then
          if (0.gt.0) write(0,91) n+1,xold(n),xold(n+1)
          if (0.gt.0) write(0,92)
          if (0.gt.0) write(0,93) xold
          write(0,91) n+1,xold(n),xold(n+1)
          write(*,91) n+1,xold(n),xold(n+1)
          stop
        endif
      enddo

      nnew=nlong
      do n=1,nlong
        k(n)=1
      enddo
      
      ! fond plat �ventuel au d�part (lit mineur uniquement)
      n1=1
      if (imode.eq.2) then
          do n=2,nlong-1
            if (xold(n)-xold(1).lt.0.03) then
                k(n-1)=0
                nnew=nnew-1
              n1=n
            else
                exit
            endif
          enddo
      endif
      
      do while (nnew.gt.nobjectif)
        nmin=0
!       write(0,'(a)')'    n   i1   i2   xtmp1   xtmp2 xold(n)   ytmp1   ytmp2 yold(n)   yy(n)      aire         a         h'
        do n=n1+1,nlong-1
            ! si point d�j� �limin�: suivant
            if (k(n).eq.0) cycle
          ! point pr�c�dent le + proche non �limin�
          do i1=n-1,1,-1
              if (k(i1).eq.1) exit
            enddo
          ! point suivant le + proche non �limin�
          do i2=n+1,nlong
              if (k(i2).eq.1) exit
          enddo
          ! projections verticales sur le segment i1 - i2
          xtmp(1)=xold(i1) ; ytmp(1)=yold(i1)
          xtmp(2)=xold(i2) ; ytmp(2)=yold(i2)
          do i=i1,i2
            call xtoy_LE(xtmp,ytmp,2,xold(i),yy(i),ii)
          enddo
          ! aire
          aire=0.
          do i=i1+1,i2
            h1=yy(i-1)-yold(i-1)
            h2=yy(i)-yold(i)
            if (i2-i1.eq.2.or.h1*h2.ge.0.) then
              airep=0.5*abs(h1+h2)*(xold(i)-xold(i-1))
            else
              h1=abs(h1)
              h2=abs(h2)
              x3=(xold(i)*h1+xold(i-1)*h2)/(h1+h2)
              if (.not.(x3.ge.xold(i-1)-1.e-4.and.x3.le.xold(i)+1.e-4)) then
                write(0,'(a,i4,3f8.3)') 'i,xold(i-1),xold(i),x3',i,xold(i-1),xold(i),x3
                write(0,*) 'incoherence x3'
                write(*,'(a,i4,3f8.3)') 'i,xold(i-1),xold(i),x3',i,xold(i-1),xold(i),x3
                stop
              endif
              airep=(x3-xold(i-1))*h1/2.+(xold(i)-x3)*h2/2.
            endif
            aire=aire+airep
          enddo
          aire=aire*fy

          a=((xold(i2)-xold(i1))**2+(yold(i2)*fy-yold(i1)*fy)**2)
          if (a.lt.0.000001) then
            h=0.
          else
            a=sqrt(a)
            h=abs(2.*aire/a)
          endif

          if (nmin.eq.0) then
            nmin=n
            dymin=h
            i1dmp=i1
            i2dmp=i2
          elseif (h.lt.dymin) then
            nmin=n
            dymin=h
            i1dmp=i1
            i2dmp=i2
          endif
        enddo

        !- elimination du point nmin, ou sortie si nmin=0
        if (nmin.gt.0) then
          k(nmin)=0
          nnew=nnew-1
!            write(0,'(a)') '------------------------------------------------------------------------------------'
!            write(0,'(a,3i4)') '==> point rejet�: rang et encadrants:',nmin,i1dmp,i2dmp
!            write(0,'(a/)') '------------------------------------------------------------------------------------'
        else
          write(0,*) 'incoherence ds cpqhyd5'
          stop
          exit
        endif
          
      enddo

      j=0
      do n=1,nlong
        if (k(n).eq.1) then
          j=j+1
        if (j.eq.1) then
          xnew(j)=xold(1)
        else
          xnew(j)=xold(n)
          endif
        ynew(j)=yold(n)
        endif
      enddo
      if (j.ne.nnew) then
        write(0,*)  'incoherence j/nnew ds cpqhyd5',j,nnew
        stop
      endif
      istat=0
      return
      end



!----------------------------------------------------------------------------------------------------------------------------

