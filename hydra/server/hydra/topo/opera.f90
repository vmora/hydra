      subroutine opera(t,q,iptr,long,alpha,tho,ncb,nptotmax,idvout,istat)
!
!  Operation alg�biques sur NCB hydrogrammes (ou courbes)
!     T()  Q()  : tableaux des hydrogrammes op�rants, successifs, hydrogramme r�sultant stock� � la suite
!     nptotmax  : longueur maximal de ces tableaux, hydrogramme r�sultant INCLUS
!     iptr(k) k=1,ncb : pointeurs des hydrogrammes op�rants
!     iptr(ncb+1)     : pointeur de l'hydrogramme r�sultant
!     long(k) k=1,ncb : longueurs des hydrogrammes op�rants
!     long(ncb+1)     : longueur de l'hydrogramme r�sultant
!     alpha(k) k=1,ncb : facteurs multiplicatifs par hydrogramme op�rant
!     tho(k)   k=1,ncb : pr�vu pour d�calage temporels par hydrogramme op�rant
!                        n'est pas utilis� actuellement dans cette routine.
!              Dans le programme OPERA.EXE, les d�calages temporels sont r�alis�s par le spr appelant.
!     j(k)   k=1,ncb : indice d'avancement ds chaque hydrogramme op�rant (tableau local)
!     idvout : idv fichier journal. si 0: ecritures a l'�cran, si <0: pas d'�critures.
!     istat : statut d'erreur. =0 si pas d'erreur, <>0 si erreur
!
! 
      dimension t(NPTOTMAX),q(NPTOTMAX),iptr(ncb+1),long(ncb+1),j(ncb),alpha(ncb),tho(ncb)
      logical lolo
!
!---controle croissance des T
!
      do i=1,ncb
        call ctlCroissance(i,T(iptr(i)),long(i),'opera')
      enddo
!
!---premier point
!
      tmin=t(1)
      qcib=0.
      imin=1
      do i=1,ncb
         j(i)=1
         if (t(iptr(i)).lt.tmin) then
           tmin=t(iptr(i))
           imin=i
         endif
         qcib=qcib+alpha(i)*q(iptr(i))
      enddo
!      write(idvcib,104)  tmin,qcib
      iptr(ncb+1)=iptr(ncb)+long(ncb)
      long(ncb+1)=0
      call AddToTab(tmin,qcib)
      
      qcibmin=qcib
      qcibmax=qcib
      do i=1,ncb
        if (t(iptr(i)).eq.tmin) j(i)=j(i)+1
      enddo

      lolo=.true.
      DO WHILE (lolo)
!         write(*,'(20i5)') (j(i),long(i),i=1,ncb)
         do i=1,ncb
           if (j(i).le.long(i)) then
             tmin=t(iptr(i)-1+j(i))
             imin=i
             exit
           endif
         enddo
         if (i.eq.ncb+1) then
           if (idvout.ge.0) write(idvout,*) 'pb ds prgm opera,mode 2'
           istat=23
           return
         endif
         qcib=0.
         lolo=.false.
         do i=1,ncb
           if (t(iptr(i)-1+j(i)).lt.tmin.and.j(i).le.long(i)) then
             tmin=t(iptr(i)-1+j(i))
             imin=i
           endif
         enddo
         do i=1,ncb
           call xtoy_LE(t(iptr(i)),q(iptr(i)),long(i),tmin,qtmp,kk)
           if (t(iptr(i)-1+j(i)).eq. tmin) j(i)=j(i)+1
           qcib=qcib+alpha(i)*qtmp
           lolo=lolo.or.j(i).le.long(i)
         enddo
!         write(idvcib,104)  tmin,qcib
         call AddToTab(tmin,qcib)
         qcibmin=amin1(qcibmin,qcib)
         qcibmax=amax1(qcibmax,qcib)
      ENDDO

      istat=0
      return
      
      CONTAINS
      
      subroutine AddToTab(x,y)
        long(ncb+1)=long(ncb+1)+1
        ix=iptr(ncb+1)-1+long(ncb+1)
        if (ix.gt.NPTOTMAX) then
          istat=1
          write(0,*) 'Erreur: d�passement des tableaux T-Q'
          stop
        endif
        t(ix)=x
        q(ix)=y
        return
      endsubroutine

      END

      SUBROUTINE XTOY_LE(Z1,Q1,N,Z,F,KK)
      DIMENSION Z1(n),Q1(n)
      IF(Z.LT.Z1(1)) GO TO 6
      IF(Z.GT.Z1(N))GO TO 7
      DO 2 I=2,N
      IF(Z1(I).Le.Z) GOTO 2   ! .LT. remplac� par .LE. le 26/10/2011 (pour spr opera)
      F=Q1(I-1)+(Z-Z1(I-1))*(Q1(I)-Q1(I-1))/(Z1(I)-Z1(I-1))
      KK=I-1
      RETURN
    2 CONTINUE
    7 KK=N
      F=Q1(N)
      RETURN
    6 KK=0
      F=Q1(1)
      RETURN
      END
      
      subroutine ctlCroissance(icb,x,n,cspr)
        character*(*) cspr
        dimension x(n)
        do i=1,n-1
          if (x(i+1).lt.x(i)) then
            write(0,91) cspr,icb,i+1,x(i),x(i+1)
            write(0,'("Tableau X re�u:")')
            write(0,'(10f8.3)') x
            stop
          endif
        enddo
        return
   91 format("Erreur dans spr ",a,": le tableau d'entree T numero",i4,&
     & " n'est pas croissant: Valeur decroissante au rang",i8,2f15.8)
      end
      



