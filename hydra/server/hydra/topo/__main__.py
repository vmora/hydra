# coding = utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from . import zb
import sys

spl = sys.stdin.readline().split()
s_left_bank, s_right_bank, nbpt = float(spl[0]), float(spl[1]), int(spl[2])
topo_profile = []
for i in range(nbpt):
    topo_profile.append(tuple((float(c) for c in sys.stdin.readline().split()[:2])))
while True:
    try:
        mode=int(sys.stdin.readline())
        break
    except:
        continue

if mode==1:
    pk_up, pk, pk_down = [float(p) for p in sys.stdin.readline().split()]
    zb_up, zb_down = [], []
    for t in [zb_up, zb_down]:
        nbpt = int(sys.stdin.readline())
        for i in range(nbpt):
            t.append(tuple((float(c) for c in sys.stdin.readline().split()[:2])))
    zb_ = zb(s_left_bank, s_right_bank, topo_profile, pk, pk_up, pk_down, zb_up, zb_down)
else:
    zb_ = zb(s_left_bank, s_right_bank, topo_profile)

sys.stdout.write("%7.2f %7.2f %7.2f %7.2f %7.2f %7.2f\n"%tuple(p[0] for p in zb_[4:10]))
sys.stdout.write("%7.2f %7.2f %7.2f %7.2f %7.2f %7.2f\n"%tuple(p[1] for p in zb_[4:10]))
sys.stdout.write("%7.2f %7.2f %7.2f %7.2f\n"%tuple(p[0] for p in zb_[:4]))
sys.stdout.write("%7.2f %7.2f %7.2f %7.2f\n"%tuple(p[1] for p in zb_[:4]))
sys.stdout.write("%7.2f %7.2f %7.2f %7.2f\n"%tuple(p[0] for p in zb_[10:]))
sys.stdout.write("%7.2f %7.2f %7.2f %7.2f\n"%tuple(p[1] for p in zb_[10:]))

