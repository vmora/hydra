# coding = utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import numpy
import tempfile
from subprocess import Popen, PIPE
from io import StringIO
from shapely import wkb
from shapely.geometry import Point, LineString
from math import sqrt

__tempdir = tempfile.gettempdir()

topo = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'topo.exe' if os.name != 'posix' else 'topo')

def zb(plpy,s_left_bank, s_right_bank, topo_profile, pk=None, pk_up=None, pk_down=None, zb_up=None, zb_down=None):
    """returns [zb_left]+[zb_min]+[zb_right] array (list of 4+6+4 2-tuple)
    for the given topographic profile
    s_right_bank and s_right_bank are location of the bank along the profile
    given in in km
    pk, pk_up, pk_down are the location of current, up and down cross-sections
    zb_up, zb_down are the zb arrays for the up and down cross-sections
    """

    in_ = "{s_left_bank} {s_right_bank} {nbpt}\n{topo_profile}\n\n{mode}\n".format(
            s_left_bank=s_left_bank,
            s_right_bank=s_right_bank,
            nbpt=len(topo_profile),
            topo_profile="\n".join(["{} {}".format(p[0], p[1]) for p in topo_profile]),
            mode=0 if pk is None else 1
            )

    if pk:
        in_ += "{pk_up} {pk} {pk_down}\n{nbptl}\n{ptl}\n{nbptr}\n{ptr}\n\n".format(
                pk_up=pk_up,
                pk=pk,
                pk_down=pk_down,
                nbptl=len(zb_up),
                ptl="\n".join(["{} {}".format(p[0], p[1]) for p in zb_up]),
                nbptr=len(zb_down),
                ptr="\n".join(["{} {}".format(p[0], p[1]) for p in zb_down])
                )

    # plpy.warning(s_left_bank, s_right_bank, topo_profile, pk, pk_up, pk_down, zb_up, zb_down)
    # plpy.warning(in_)

    with open(os.path.join(__tempdir, 'topo_in.dat'), 'w') as f:
        f.write(in_)

    out, err = Popen([topo], stdout=PIPE, stdin=PIPE, stderr=PIPE).communicate(bytes(in_, 'utf-8'))
    if err:
        raise Exception(err)

    lines = out.split(b'\n')
    zb_min = [(float(z), float(b)) for z, b in zip(lines[0].split(), lines[1].split())]
    zb_majl = [(float(z), float(b)) for z, b in zip(lines[2].split(), lines[3].split())]
    zb_majr = [(float(z), float(b)) for z, b in zip(lines[4].split(), lines[5].split())]
    return zb_majl+zb_min+zb_majr

def pg_array_to_list(arr):
    return [[float(v) for v in coord.split(',')] for coord in arr.replace('{{','').replace('}}','').split('},{')]

def profile_from_transect(plpy, transect, model, srid, interpolate=False, discretization=150, ignore_points=False, distance_point=100):
    """returns a string representation of the array,
    the riverbed is taken fron the two transect points closest to the reach intersection
    a single point of intersection is assumed"""

    #@todo find upstream and downstream profiles in this query
    inter = plpy.execute("""
        with reach as (
            select r.id, st_intersection(r.geom, 'SRID={srid};{transect}'::geometry) as inter, geom, pk0_km
            from {model}.reach as r
            where st_intersects(r.geom, 'SRID={srid};{transect}'::geometry)
        ),
        pk as (
            select st_linelocatepoint(geom, inter)*st_length(geom)/1000+pk0_km as pk from reach
        ),
        up_ as (
            select coalesce((select zbmin_array from {model}.valley_cross_section_geometry as g where g.id = p.down_vcs_geom),
                            (select zbmin_array from {model}.valley_cross_section_geometry as g where g.id = p.up_vcs_geom)) as zbmin,
                   n.pk_km
            from {model}.river_cross_section_profile as p
            join {model}.river_node as n on p.id=n.id
            where p.id = {model}.upstream_valley_section('SRID={srid};{transect}'::geometry)
        ),
        down_ as (
            select coalesce((select zbmin_array from {model}.valley_cross_section_geometry as g where g.id = p.up_vcs_geom),
                            (select zbmin_array from {model}.valley_cross_section_geometry as g where g.id = p.down_vcs_geom)) as zbmin,
                   n.pk_km
            from {model}.river_cross_section_profile as p
            join {model}.river_node as n on p.id=n.id
            where p.id = {model}.downstream_valley_section('SRID={srid};{transect}'::geometry)
        )
        select r.id as reach, pk.pk, r.inter, (select zbmin from up_)::varchar as su,  (select pk_km from up_) as pku,  (select zbmin from down_)::varchar as sd, (select pk_km from down_) as pkd
        from reach as r, pk
        """.format(
            model=model,
            transect=transect,
            discretiz=str(discretization),
            srid=srid
            ))
    if not inter:
        return None
    inter_reach = wkb.loads(inter[0]['inter'], True)
    transect_geom = wkb.loads(transect, True)

    if len(transect_geom.coords)==2:
        coords = transect_geom.coords
        new_coords_start, new_coords_end = __calc_new_start_end(coords[0],coords[1])
        geom=[new_coords_start]
        geom.extend(coords)
        geom.append(new_coords_end)
        transect_geom = LineString(geom)
        left_bank_pt = 2

    if len(transect_geom.coords)>=3:
        s_center = transect_geom.project(inter_reach)
        s = numpy.array([transect_geom.project(Point(p)) for p in transect_geom.coords])
        left_bank_pt = len(s[s < s_center])
        rigth_bank_pt = len(s[s > s_center])
        coords = transect_geom.coords
        if left_bank_pt==1:
            new_coords_start, new_coords_end = __calc_new_start_end(coords[0],coords[1])
            geom=[new_coords_start]
            geom.extend(coords)
            transect_geom = LineString(geom)
            left_bank_pt = 2
        if rigth_bank_pt==1:
            new_coords_start, new_coords_end = __calc_new_start_end(coords[left_bank_pt-1],coords[left_bank_pt])
            geom=[]
            geom.extend(coords)
            geom.append(new_coords_end)
            transect_geom = LineString(geom)

    s_center = transect_geom.project(inter_reach)
    s = numpy.array([transect_geom.project(Point(p)) for p in transect_geom.coords])
    s_left_bank = s[left_bank_pt-1]
    s_right_bank = s[left_bank_pt]
    # plpy.warning("geom: ", " ".join(["({} {} {})".format(p[0], p[1], p[2]) for p in transect_geom.coords]))
    # plpy.warning("s: ", s)
    # plpy.warning("s_left_bank: ", s_left_bank)
    # plpy.warning("s_right_bank: ", s_right_bank)

    topo_transect = plpy.execute("""select project.set_altitude(project.discretize_line('SRID={srid};{transect}'::geometry, st_length('SRID={srid};{transect}'::geometry)/{discretiz})) as geom""".format(
            transect=transect_geom,
            discretiz=str(discretization), srid=srid))

    topo_profile = [(transect_geom.project(Point(p)), p[2]) for p in wkb.loads(topo_transect[0]['geom'], True).coords]

    if ignore_points or interpolate:
        topo_final=topo_profile
    else:
        points_profile_geom = plpy.execute("""
                        select p.id,
                        p.z_ground as z,
                        st_linelocatepoint('SRID={srid};{transect}'::geometry, p.geom)*st_length('SRID={srid};{transect}'::geometry) as pk
                        from project.points_xyz as p
                        where st_intersects(ST_Buffer('SRID={srid};{transect}'::geometry, {distance}, 'endcap=flat join=round'),p.geom )=true
                        or st_startpoint('SRID={srid};{transect}'::geometry)=p.geom
                        or st_endpoint('SRID={srid};{transect}'::geometry)=p.geom
                        order by pk""".format(model=model, transect=transect_geom, distance=str(distance_point), srid=srid))


        if len(points_profile_geom)>1:
            points_profile = numpy.array([(p['pk'], p['z']) for p in points_profile_geom])
            start = points_profile[0][0]
            end = points_profile[-1][0]
            topo_final = [(p[0],p[1]) for p in topo_profile if p[0]<start]
            if len(topo_final)>1:
                topo_final=numpy.concatenate((topo_final, points_profile))
            else:
                topo_final=points_profile
            for p in topo_profile:
                if p[0]>end:
                    topo_final=numpy.concatenate((topo_final,[p]))
        else:
            topo_final=topo_profile
    #plpy.warning("intersection at ", s_center, s, s_left_bank, s_right_bank, inter[0]['su'], inter[0]['sd'])

    if interpolate and inter[0]['su'] is not None and inter[0]['sd'] is not None:
        pk, pk_up, pk_down =  inter[0]['pk'], inter[0]['pku'], inter[0]['pkd']
        zb_up, zb_down = pg_array_to_list(inter[0]['su']), pg_array_to_list(inter[0]['sd'])
        return str(zb(plpy,s_left_bank, s_right_bank, topo_final,
            pk, pk_up, pk_down, zb_up, zb_down)).replace('[', '{').replace('(', '{').replace(']', '}').replace(')', '}')
    else:
        return str(zb(plpy,s_left_bank, s_right_bank, topo_final)).replace('[', '{').replace('(', '{').replace(']', '}').replace(')', '}')

def __calc_new_start_end(coordstart, coordend):
    inc_x = (coordend[0] - coordstart[0])/(coordend[1]-coordstart[1])
    inc_y = (coordend[1]-coordstart[1])/(coordend[0] - coordstart[0])
    dist = sqrt((coordend[0] - coordstart[0])**2 + (coordend[1]-coordstart[1])**2)
    inc_x = (coordend[0] - coordstart[0])/dist
    inc_y = (coordend[1]-coordstart[1])/dist

    new_coords_start = (coordstart[0]-inc_x, coordstart[1]-inc_y, coordstart[2])
    new_coords_end = (coordend[0]+inc_x, coordend[1]+inc_y, coordend[2])
    return new_coords_start, new_coords_end