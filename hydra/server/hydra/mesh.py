# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
hydra server-side module mesh specific functions

"""

from subprocess import Popen, PIPE
import os
import re
import numpy
import tempfile
from shapely import wkt
from shapely import wkb
import time
import json
from shapely.geometry import MultiLineString, Point, LineString

from shapely import geos
geos.WKBWriter.defaults['include_srid'] = True

from .altitude import altitude, altitudes

# Auto generation implemented:
#   At mesh, generate links:
#       2d node -> immersed channel reach
#       2d node -> valley reach
#       2d node -> reach end
#       2d node -> storage
#       2d node -> street
#   Create links available for link generation:
#       reach   -> reach
#       reach   -> street
#       reach   -> storage
#       storage -> storage
#       cst line-> station node

def mesh(plpy, coverage_id, model, srid, triangles=False, generation_step=None):
    #try:
    start = time.time()

    plpy.notice("start mesh {}".format(time.time()-start))

    workspace = plpy.execute("select workspace from hydra.metadata")[0]['workspace']
    sources = [res['source'].replace('\\','\\\\').replace('$workspace', workspace) for res in plpy.execute("select source from project.dem order by priority asc")]

    points = tuple()
    constrains = plpy.execute("""
        with dr as (
            select (st_dumprings(geom)).geom as geom
            from {model}.coverage
            where id={coverage_id}
        ),
        rings as (
            select st_collect(st_exteriorring(geom)) as geom from dr
        )
        select c.id, (st_dump(st_collectionextract(st_intersection(c.discretized, p.geom), 2))).geom as geom
        from {model}.constrain as c, {model}.coverage as p, rings as r
        where st_length(st_collectionextract(st_intersection(c.discretized, p.geom), 2)) > 0
        and not st_covers(r.geom, st_collectionextract(st_intersection(c.discretized, p.geom), 2))
        and p.id={coverage_id}
        and p.domain_type='2d'
        """.format(
            model=model,
            coverage_id=coverage_id
            ))
    lines = [wkb.loads(r['geom'], True) for r in constrains]

    polygons = [wkb.loads(r['geom'], True) for r in plpy.execute("""
        select c.geom as geom
        from {model}.coverage as c
        where c.id = {coverage_id}
        and c.domain_type='2d'
        """.format(
            model=model,
            coverage_id=coverage_id
            ))]
    plpy.notice("load polygons {}".format(time.time()-start))

    if not len(polygons):
        return 0

    generation_step = generation_step or plpy.execute("""
        insert into {model}.generation_step default values returning id
        """.format(
            model=model
            ))[0]['id']
    plpy.notice("update gen step {}".format(time.time()-start))
    precision = plpy.execute("""
        select precision from hydra.metadata
        """)[0]['precision']
    plan = plpy.prepare("""
        insert into {model}.elem_2d_node(contour, generated)
        values (st_snaptogrid(project.set_altitude('SRID={srid};'||$1)::geometry, {precision}), {generation_step})
        """.format(
            model=model,
            srid=srid,
            generation_step=generation_step,
            precision=precision
        ), [ "varchar"])

    node_map = {}
    current_id = 0
    tempdir = tempfile.mkdtemp() # used to be '/tmp' or tempfile.mkdtemp()
    os.chmod(tempdir, 0o777)
    tmp_in_file = os.path.join(tempdir, 'tmp_mesh.geo')
    with open(tmp_in_file, "w") as geo:
        for polygon in polygons:
            surface_idx = []
            for ring in [polygon.exterior] + list(polygon.interiors):
                ring_idx = []
                for coord in ring.coords:
                    coord = (coord[0], coord[1])
                    if coord in node_map:
                        ring_idx.append(node_map[coord])
                    else:
                        current_id += 1
                        ring_idx.append(current_id)
                        node_map[coord] = current_id
                        geo.write("Point(%d) = {%f, %f, 0, 999999};\n"%(current_id, coord[0], coord[1]))
                loop_idx = []
                for i, j in zip(ring_idx[:-1], ring_idx[1:]):
                    current_id += 1
                    geo.write("Line(%d) = {%d, %d};\n"%(current_id, i, j))
                    loop_idx.append(current_id)
                current_id += 1
                geo.write("Line Loop(%d) = {%s};\n"%(current_id, ", ".join((str(i) for i in loop_idx))))
                surface_idx.append(current_id)
            current_id += 1
            geo.write("Plane Surface(%d) = {%s};\n"%(current_id, ", ".join((str(i) for i in surface_idx))))
            if not triangles:
                geo.write("Recombine Surface {%s};\n"%(current_id))

            current_surface = current_id
            for line in lines:
                if polygon.covers(line):
                    line_idx = []
                    for coord in line.coords:
                        coord = (coord[0], coord[1])
                        if coord in node_map:
                            line_idx.append(node_map[coord])
                        else:
                            current_id += 1
                            line_idx.append(current_id)
                            node_map[coord] = current_id
                            geo.write("Point(%d) = {%f, %f, 0, 999999};\n"%(current_id, coord[0], coord[1]))
                    for i, j in zip(line_idx[:-1], line_idx[1:]):
                        current_id += 1
                        geo.write("Line(%d) = {%d, %d};\n"%(current_id, i, j))
                        geo.write("Line {%d} In Surface {%d};\n"%(current_id, current_surface))
            for point in points:
                if polygon.covers(point):
                    if point.coords[0] in node_map:
                        geo.write("Point {%d} In Surface {%d};\n"%(node_map[point.coords[0]], current_surface))
                    else:
                        current_id += 1
                        node_map[point.coords[0]] = current_id
                        geo.write("Point(%d) = {%f, %f, 0, 999999};\n"%(current_id, point.coords[0][0], point.coords[0][1]))
                        geo.write("Point {%d} In Surface {%d};\n"%(current_id, current_surface))

    plpy.notice("mesh {}".format(time.time()-start))

    #os.chmod(tmp_in_file, 0o666)
    # run gmsh
    tmp_out_file = os.path.join(tempdir, 'tmp_mesh.msh')
    cmd = ['gmsh', '-2', tmp_in_file, '-o', tmp_out_file ]
    plpy.notice("running "+' '.join(cmd))
    if os.name == 'posix':
        out, err = Popen(cmd, stdout=PIPE, stderr=PIPE).communicate()
    else:
        out, err = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()

    for e in err.split(b"\n"):
        plpy.notice(str(e))

    #os.chmod(tmp_out_file, 0o666)
    plpy.notice("parse {}".format(time.time()-start))

    # load results
    with open(tmp_out_file) as out:
        while not re.match("^\$Nodes", out.readline()):
            pass
        nb_nodes = int(out.readline())
        nodes = numpy.zeros((nb_nodes, 3), dtype=numpy.float64)
        for i in range(nb_nodes):
            id_, x, y, z = out.readline().split()
            nodes[int(id_)-1, :] = float(x), float(y), float(z)
        assert re.match("^\$EndNodes", out.readline())
        assert re.match("^\$Elements", out.readline())
        elements = []
        nb_elem = int(out.readline())
        for i in range(nb_elem):
            spl = out.readline().split()
            if spl[1] == '2':
                elements.append([int(n) - 1 for n in spl[-3:]])
                wkt = "POLYGON Z(("+",".join(
                    ["{} {} {}".format(coord[0], coord[1], altitude(sources, coord[0], coord[1]))
                        for coord in reversed(nodes[elements[-1]+[elements[-1][0]]])]
                    )+"))"
                plpy.execute(plan, [wkt])
            elif spl[1] == '3':
                elements.append([int(n) - 1 for n in spl[-4:]])
                wkt = "POLYGON Z(("+",".join(
                    ["{} {} {}".format(coord[0], coord[1], altitude(sources, coord[0], coord[1]))
                        for coord in reversed(nodes[elements[-1]+[elements[-1][0]]])]
                    )+"))"
                plpy.execute(plan, [wkt])
    plpy.notice("insert elem {}".format(time.time()-start))

    # Saves current config and switches to default config
    cfg = plpy.execute("""select configuration from {model}.metadata""".format(model=model))[0]['configuration']
    if cfg != 1:
        plpy.execute("""update {model}.metadata set configuration=1""".format(model=model))

    # set domain_2d
    plpy.execute("""
        update {model}.elem_2d_node as n
            set domain_2d=(
                    select m.domain_2d
                    from {model}.domain_2d_marker as m, {model}.coverage as c
                    where st_intersects(c.geom, m.geom) and c.id={cov_id}
                    order by m.id asc
                    limit 1
                        )
            where generated={generation_step}
        """.format(
            model=model,
            cov_id=coverage_id,
            generation_step=generation_step
        ))

    # add links
    plpy.execute("""
        select {model}.create_elem2d_links(n1.id, (st_dump(st_collectionextract(st_intersection(n1.contour, n2.contour), 2))).geom, {generation_step})
        from {model}.elem_2d_node as n1, {model}.elem_2d_node as n2
        where st_intersects(n1.contour, n2.contour)
        and st_length(st_collectionextract(st_intersection(n1.contour, n2.contour), 2)) > 0
        and n1.id > n2.id
        and n1.generated = {generation_step}
        and n2.generated = {generation_step}
        """.format(
            model=model,
            srid=srid,
            generation_step=generation_step,
            coverage_id=coverage_id
        ))
    plpy.notice("insert link {}".format(time.time()-start))

    # create links on boundary
    plpy.execute("""
        with dr as (
            select (st_dumprings(geom)).geom as geom
            from {model}.coverage
            where id={coverage_id}
        ),
        border as(
            select st_collect(st_exteriorring(geom)) as geom from dr
        )
        select {model}.create_elem2d_links(e.id, (st_dump(st_collectionextract(st_intersection(e.contour, b.geom), 2))).geom, {generation_step})
        from {model}.elem_2d_node as e, border as b
        where e.generated={generation_step}
        and st_intersects(e.contour, b.geom)
        and st_length(st_collectionextract(st_intersection(e.contour, b.geom), 2)) > 0
        """.format(
            model=model,
            generation_step=generation_step,
            coverage_id=coverage_id
        ))

    # create links with immersed channel reach
    plpy.execute("""
                with segments as (
                    select (st_dump(st_intersection(e.contour, c.geom))).geom as geom, e.id as elem_id
                    from {model}.elem_2d_node as e, {model}.channel_reach as c
                    where st_intersects(e.contour, c.geom) and e.generated = {generation_step}
                ),
                river_node as (
                    select ST_LineInterpolatePoint(s.geom, .5) as geom
                    from segments as s
                    where (select count(1) from {model}.river_node as r where st_dwithin(s.geom, r.geom, 0.01)) = 0
                ),
                new_node as (
                    select project.set_altitude(st_force3d(geom)) as geom
                    from river_node as n
                ),
                ins_node as (
                    insert into {model}.river_node(geom, z_ground, generated)
                    select geom, st_z(geom), {generation_step}
                    from new_node
                    returning id, geom
                ),
                links_geom as (
                    select n.geom as g1, e.geom as g2, e.id
                        from {model}.river_node as n, {model}.elem_2d_node as e
                        where st_contains(e.contour, n.geom) and e.generated={generation_step}
                    union
                    select i.geom as g1, e.geom, e.id as g2
                        from ins_node as i , {model}.elem_2d_node as e
                        where st_contains(e.contour, i.geom) and e.generated={generation_step}
                )
                insert into {model}.overflow_link(geom, generated, z_crest1, width1, z_crest2, width2)
                    select st_makeline(ARRAY[l.g1, st_centroid(st_union(l.g1, l.g2))]),{generation_step},e.zb, 0, e.zb, st_length(s.geom)*2
                    from links_geom as l, {model}.elem_2d_node as e, segments as s
                    where l.id=e.id
                        and l.id = s.elem_id
                        and e.generated={generation_step}
            """.format(
                model=model,
                generation_step=generation_step
            ))

    # update streets
    plpy.execute("""DO
                    $do$
                        begin
                            if (select trigger_street_link from {model}.metadata) then
                                perform {model}.update_street_links();
                            end if;
                        end
                    $do$""".format(model=model))

    # Restores initial configuration
    if cfg != 1:
        plpy.execute("""update {m}.metadata set configuration={c}""".format(m=model,c=cfg))

    res =  plpy.execute("""
        select count(1) as ct
        from {model}.elem_2d_node
        where generated={generation_step}
        """.format(
            model=model,
            generation_step=generation_step))[0]['ct']

    return res

def create_elem2d_links(plpy, elem_id, border, model, srid, generation_step):
    # find the domains that borders the element
    res = plpy.execute("""
        select c.domain_type, c.id
        from {model}.coverage as c
        where st_intersects(c.geom, '{border}'::geometry)
        and st_length(st_collectionextract(st_intersection(c.geom, '{border}'::geometry), 2)) > 0
        """.format(
            model=model,
            border=border
        ))
    domains = {r['domain_type']: r['id'] for r in res}
    cst = plpy.execute("""
        select c.constrain_type, c.link_attributes, c.elem_length, c.geom
        from {model}.constrain as c
        where st_covers(c.discretized, '{border}'::geometry)
        """.format(
            model=model,
            border=border
        ))


    link_type = (cst[0]['constrain_type']) if cst else None
    link_attributes = json.loads(cst[0]['link_attributes']) if cst and cst[0]['link_attributes'] is not None else {}

    #plpy.warning(domains, link_type)
    if "reach" in domains:
        if link_type == 'flood_plain_transect':
            plpy.execute("""
                with river_node as (
                    select st_closestpoint(r.geom, st_centroid(st_intersection(
                            st_intersection(
                                c.geom,
                                st_buffer('{transect}'::geometry, {elem_length}::real/2)),
                            r.geom))) as geom,
                        st_distance(e.geom, r.geom)/10 as dist
                    from {model}.reach as r, {model}.coverage as c, {model}.elem_2d_node as e
                    where st_intersects(r.geom, c.geom)
                    and c.id={coverage_id}
                    and e.id={elem_id}
                ),
                new_node as (
                    select project.set_altitude(st_force3d(geom)) as geom
                    from river_node as n
                    where (select count(1) from {model}.river_node as r where st_dwithin(r.geom, n.geom, n.dist)) = 0
                ),
                ins_node as (
                    insert into {model}.river_node(geom, z_ground, generated)
                    select geom, st_z(geom), {generation_step}
                    from new_node
                    returning id, geom
                ),
                ins_link as (
                    insert into {model}.{link_type}_link(geom, border, generated{attribute_colums})
                    select st_makeline(ARRAY[(
                         select geom from (
                         select n.geom
                         from {model}.river_node as n join {model}.coverage as c on st_intersects(c.geom, n.geom)
                         where c.id={coverage_id}
                         union
                         select geom from ins_node
                         ) as t order by st_distance(geom, {model}.interpolate_transect_at(st_lineinterpolatepoint('{border}'::geometry, .5))) limit 1),
                         st_lineinterpolatepoint('{border}'::geometry, .5),
                         st_centroid(st_union(e.geom, (st_lineinterpolatepoint('{border}'::geometry, .5))))]), '{border}'::geometry, {generation_step}{attribute_values}
                    from {model}.elem_2d_node as e, river_node as tg
                    where e.id={elem_id}
                    returning id
                )
                select {model}.set_link_z_and_altitude(id) from ins_link
            """.format(
                model=model,
                border=border,
                elem_id=elem_id,
                transect=cst[0]['geom'],
                elem_length=cst[0]['elem_length'],
                coverage_id=domains["reach"],
                generation_step=generation_step,
                attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                link_type = 'overflow'
            ))

        else:
            plpy.execute("""
                with river_node as (
                    select st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, st_centroid(st_intersection(r.geom, {model}.interpolate_transect_at(st_lineinterpolatepoint('{border}'::geometry, .5)))))) as geom,
                    st_distance(e.geom, r.geom)/10 as dist
                    from {model}.reach as r, {model}.coverage as c, {model}.elem_2d_node as e
                    where st_intersects(r.geom, c.geom)
                    and c.id={coverage_id}
                    and e.id={elem_id}
                ),
                new_node as (
                    select project.set_altitude(st_force3d(geom)) as geom
                    from river_node as n
                    where (select count(1) from {model}.river_node as r where st_dwithin(r.geom, n.geom, n.dist)) = 0
                ),
                ins_node as (
                    insert into {model}.river_node(geom, z_ground, generated)
                    select geom, st_z(geom), {generation_step}
                    from new_node
                    returning id, geom
                ),
                ins_link as (
                    insert into {model}.{link_type}_link(geom, border, generated{attribute_colums})
                    select st_makeline(ARRAY[(
                         select geom from (
                         select n.geom
                         from {model}.river_node as n join {model}.coverage as c on st_intersects(c.geom, n.geom)
                         where c.id={coverage_id}
                         union
                         select geom from ins_node
                         ) as t order by st_distance(geom, {model}.interpolate_transect_at(st_lineinterpolatepoint('{border}'::geometry, .5))) limit 1),
                         st_lineinterpolatepoint('{border}'::geometry, .5),
                         st_centroid(st_union(e.geom, (st_lineinterpolatepoint('{border}'::geometry, .5))))]), '{border}'::geometry, {generation_step}{attribute_values}
                    from {model}.elem_2d_node as e
                    where e.id={elem_id}
                    returning id
                )
                select {model}.set_link_z_and_altitude(id) from ins_link
            """.format(
                model=model,
                border=border,
                elem_id=elem_id,
                coverage_id=domains["reach"],
                generation_step=generation_step,
                attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                link_type = link_type or 'overflow'
            ))
    elif "reach_end" in domains:
        plpy.execute("""
            with reach_node as (
                select n.geom
                from (select st_startpoint(geom) as geom from {model}.open_reach
                      union
                      select st_endpoint(geom) as geom from {model}.open_reach) as n, {model}.coverage as c
                where st_intersects(c.geom, n.geom)
                and c.id={coverage_id}
                order by n.geom <-> '{border}'::geometry limit 1
            ),
            ins_link as (
                insert into {model}.{link_type}_link(geom, border, generated{attribute_colums})
                select st_makeline(ARRAY[
                     st_centroid(st_union(e.geom, (st_lineinterpolatepoint('{border}'::geometry, .5)))),
                     st_lineinterpolatepoint('{border}'::geometry, .5),
                     (select geom from reach_node)]) , '{border}'::geometry, {generation_step}{attribute_values}
                from {model}.elem_2d_node as e
                where e.id={elem_id}
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            border=border,
            elem_id=elem_id,
            coverage_id=domains["reach_end"],
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type if link_type and link_type != 'flood_plain_transect' else 'overflow'
        ))

    elif "storage" in domains:
        plpy.execute("""
            with storage_node as (
                select n.geom
                from {model}.storage_node as n, {model}.coverage as c
                where st_intersects(c.geom, n.geom)
                and c.id={coverage_id}
            ),
            ins_link as (
                insert into {model}.{link_type}_link(geom, border, generated{attribute_colums})
                select st_makeline(ARRAY[
                     st_centroid(st_union(e.geom, (st_lineinterpolatepoint('{border}'::geometry, .5)))),
                     st_lineinterpolatepoint('{border}'::geometry, .5),
                     (st_centroid(st_union((select geom from {model}.storage_node as t where contour = {coverage_id}), st_lineinterpolatepoint('{border}'::geometry, .5))))
                     ]), '{border}'::geometry, {generation_step}{attribute_values}
                from {model}.elem_2d_node as e
                where e.id={elem_id}
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            border=border,
            elem_id=elem_id,
            coverage_id=domains["storage"],
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type or 'overflow'
        ))

    elif "street" in domains:
        plpy.execute("""
            with street_node as (
                select st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, st_lineinterpolatepoint('{border}'::geometry, .5))) as geom,
                st_distance(e.geom, r.geom)/3 as dist
                from {model}.street as r, {model}.coverage as c, {model}.elem_2d_node as e
                where st_intersects(r.geom, c.geom)
                and c.id={coverage_id}
                and e.id={elem_id}
            ),
            new_node as (
                select project.set_altitude(st_force3d(geom)) as geom
                from street_node as n
                where (select count(1) from {model}.crossroad_node as r where st_dwithin(r.geom, n.geom, n.dist)) = 0
            ),
            ins_node as (
                insert into {model}.crossroad_node(geom, z_ground, area, generated)
                select geom, st_z(geom), 1, {generation_step}
                from new_node
                returning id, geom
            ),
            ins_link as (
                insert into {model}.overflow_link(geom, border, generated{attribute_colums})
                select st_makeline(ARRAY[(
                     select geom from (
                     select n.geom
                     from {model}.crossroad_node as n join {model}.coverage as c on st_intersects(c.geom, n.geom)
                         where c.id={coverage_id}
                         union
                     select geom from ins_node
                     ) as t order by geom <-> st_lineinterpolatepoint('{border}'::geometry, .5) limit 1),
                     st_lineinterpolatepoint('{border}'::geometry, .5),
                     st_centroid(st_union(e.geom, (st_lineinterpolatepoint('{border}'::geometry, .5))))]) , '{border}'::geometry, {generation_step}{attribute_values}
                from {model}.elem_2d_node as e
                where e.id={elem_id}
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            border=border,
            elem_id=elem_id,
            coverage_id=domains["street"],
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type or 'overflow'
        ))

    elif "crossroad" in domains or None in domains:
        pass
    else: # 2D/2D
        assert(len(domains) == 1)
        plpy.execute("""
            with ins_link as (
                insert into {model}.{link_type}_link(geom, border, generated{attribute_colums})
                select st_makeline(e1.geom, e2.geom), '{border}'::geometry, {generation_step}{attribute_values}
                from {model}.elem_2d_node as e1, {model}.elem_2d_node as e2
                where e1.id={elem_id}
                and e2.id!=e1.id
                and st_covers(e2.contour, '{border}'::geometry)
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            border=border,
            elem_id=elem_id,
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type if link_type is not None and link_type not in ('flood_plain_transect', 'boundary') else 'mesh_2d'
        ))



    return 1

def create_links(plpy, up_domain_id, down_domain_id, model, srid):

    res = plpy.execute("""
        select du.geom as ugeom, dd.geom as dgeom, du.domain_type as utype, dd.domain_type as dtype, st_linemerge(st_intersection(du.geom, dd.geom)) as border
        from {model}.coverage as du, {model}.coverage as dd
        where dd.id={down_domain_id}
        and du.id={up_domain_id}
        and st_length(st_collectionextract(st_intersection(du.geom, dd.geom), 2)) > 0
        """.format(
        down_domain_id=down_domain_id,
        up_domain_id=up_domain_id,
        model=model
    ))

    if not res:
        plpy.warning("domains {} and {} have no common boundary".format(up_domain_id, down_domain_id))
        return 0

    domains, = res

    generation_step = plpy.execute("""
        insert into {model}.generation_step default values returning id
        """.format(
            model=model
            ))[0]['id']

    cst = plpy.execute("""
        select c.constrain_type, c.link_attributes
        from {model}.constrain as c
        where st_covers(c.discretized, '{border}'::geometry)
        """.format(
            model=model,
            border=domains['border']
        ))
    link_type = (cst[0]['constrain_type']) if cst else None
    has_border = link_type == 'overflow'
    link_attributes = json.loads(cst[0]['link_attributes']) if cst and cst[0]['link_attributes'] is not None else {}

    # Saves current config and switches to default config
    cfg = plpy.execute("""select configuration from {model}.metadata""".format(model=model))[0]['configuration']
    if cfg != 1:
        plpy.execute("""update {model}.metadata set configuration=1""".format(model=model))

    if domains['utype'] == 'reach' and domains['dtype'] == 'reach':
        res = plpy.execute("""
            with blade as
            (
                select (st_dumppoints('{border}'::geometry)).geom as geom
            ),
            border as (
                select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                from (select st_collect(geom) as geom from blade) as b
            ),
            up_river_node as (
                select st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, st_centroid(st_intersection(r.geom, {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom,.5), {uid}))))) as geom,
                st_distance(st_lineinterpolatepoint(b.geom, .5), r.geom)/10 as dist
                from {model}.reach as r, {model}.coverage as c, border as b
                where st_intersects(r.geom, c.geom)
                and c.id={uid}
            ),
            down_river_node as (
                select st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, st_centroid(st_intersection(r.geom, {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom, .5), {did}))))) as geom,
                st_distance(st_lineinterpolatepoint(b.geom, .5), r.geom)/10 as dist
                from {model}.reach as r, {model}.coverage as c, border as b
                where st_intersects(r.geom, c.geom)
                and c.id={did}
            ),
            new_node as (
                select project.set_altitude(st_force3d(geom)) as geom
                from (select geom, dist from up_river_node union select geom, dist from down_river_node) as n
                where not exists (select 1 from {model}.river_node as r where st_dwithin(r.geom, n.geom, n.dist))
            ),
            ins_node as (
                insert into {model}.river_node(geom, z_ground, generated)
                select geom, st_z(geom), {generation_step}
                from new_node
                returning id, geom
            ),
            ins_link as (
                insert into {model}.{link_type}_link(geom, border,generated{attribute_colums})
                select st_makeline(ARRAY[
                     (select geom from (select n.geom from {model}.river_node as n
                                            join {model}.coverage as c on st_intersects(c.geom, n.geom)
                                            where c.id={uid}
                                            union
                                        select geom from ins_node) as t
                     order by geom <-> {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom, .5), {uid}) limit 1),
                     st_lineinterpolatepoint(b.geom, .5),
                     (select geom from (select n.geom from {model}.river_node as n
                                            join {model}.coverage as c on st_intersects(c.geom, n.geom)
                                            where c.id={did}
                                            union
                                        select geom from ins_node) as t
                     order by geom <-> {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom, .5), {did}) limit 1)
                     ]), b.geom, {generation_step}{attribute_values}
                from border as b
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            uid=up_domain_id,
            did=down_domain_id,
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type or 'overflow',
            border=domains['border']
        ))
        # Restores initial configuration
        if cfg != 1:
            plpy.execute("""update {m}.metadata set configuration={c}""".format(m=model,c=cfg))
        return 1

    elif domains['utype'] == 'reach' and domains['dtype'] == 'street':
        plpy.execute("""
            with blade as
            (
                select (st_dumppoints('{border}'::geometry)).geom as geom
            ),
            border as (
                select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                from (select st_collect(geom) as geom from blade) as b
            ),
            river_node as (
                select st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, st_centroid(st_intersection(r.geom, {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom,.5), {uid}))))) as geom,
                st_distance(st_lineinterpolatepoint(b.geom, .5), r.geom)/10 as dist
                from {model}.reach as r, {model}.coverage as c, border as b
                where st_intersects(r.geom, c.geom)
                and c.id={uid}
            ),
            street_node as (
                select st_lineinterpolatepoint(s.geom, st_linelocatepoint(s.geom, st_lineinterpolatepoint(b.geom, .5))) as geom,
                st_distance(st_lineinterpolatepoint(b.geom, .5), s.geom)/3 as dist
                from {model}.street as s, {model}.coverage as c, border as b
                where st_intersects(s.geom, c.geom)
                and c.id={did}
            ),
            new_river_node as (
                select project.set_altitude(st_force3d(geom)) as geom
                from river_node as n
                where not exists (select 1 from {model}.river_node as r where st_dwithin(r.geom, n.geom, n.dist))
            ),
            new_street_node as (
                select project.set_altitude(st_force3d(geom)) as geom
                from street_node as n
                where not exists (select 1 from {model}.crossroad_node as s where st_dwithin(s.geom, n.geom, n.dist))
            ),
            ins_river_node as (
                insert into {model}.river_node(geom, z_ground, generated)
                select geom, st_z(geom), {generation_step}
                from new_river_node
                returning id, geom
            ),
            ins_street_node as (
                insert into {model}.crossroad_node(geom, z_ground, area, generated)
                select geom, st_z(geom), 1, {generation_step}
                from new_street_node
                returning id, geom
            ),
            ins_link as (
                insert into {model}.{link_type}_link(geom, border,generated{attribute_colums})
                select st_makeline(ARRAY[
                     (select geom from (select n.geom from {model}.river_node as n
                                            join {model}.coverage as c on st_intersects(c.geom, n.geom)
                                            where c.id={uid}
                                            union
                                        select geom from ins_river_node) as t
                     order by geom <-> {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom, .5), {uid}) limit 1),
                     st_lineinterpolatepoint(b.geom, .5),
                     (select geom from (select n.geom from {model}.crossroad_node as n
                                            join {model}.coverage as c on st_intersects(c.geom, n.geom)
                                            where c.id={did}
                                            union
                                        select geom from ins_street_node) as t
                     order by geom <-> st_lineinterpolatepoint(b.geom, .5) limit 1)
                     ]), b.geom, {generation_step}{attribute_values}
                from border as b
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            uid=up_domain_id,
            did=down_domain_id,
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type or 'overflow',
            border=domains['border']
        ))
        plpy.execute("""DO
                    $do$
                        begin
                            if (select trigger_street_link from {model}.metadata) then
                                perform {model}.update_street_links();
                            end if;
                        end
                    $do$""".format(model=model))
        # Restores initial configuration
        if cfg != 1:
            plpy.execute("""update {m}.metadata set configuration={c}""".format(m=model,c=cfg))
        return 1

    elif domains['utype'] == 'reach' and domains['dtype'] == 'storage':
        plpy.execute("""
            with blade as
            (
                select (st_dumppoints('{border}'::geometry)).geom as geom
            ),
            border as (
                select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                from (select st_collect(geom) as geom from blade) as b
            ),
            river_node as (
                select st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, st_centroid(st_intersection(r.geom, {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom,.5), {uid}))))) as geom,
                st_distance(st_lineinterpolatepoint(b.geom, .5), r.geom)/10 as dist
                from {model}.reach as r, {model}.coverage as c, border as b
                where st_intersects(r.geom, c.geom)
                and c.id={uid}
            ),
            new_river_node as (
                select project.set_altitude(st_force3d(geom)) as geom
                from river_node as n
                where not exists (select 1 from {model}.river_node as r where st_dwithin(r.geom, n.geom, n.dist))
            ),
            ins_river_node as (
                insert into {model}.river_node(geom, z_ground, generated)
                select geom, st_z(geom), {generation_step}
                from new_river_node
                returning id, geom
            ),
            ins_link as (
                insert into {model}.{link_type}_link(geom, border,generated{attribute_colums})
                select st_makeline(ARRAY[
                     (select geom from (select n.geom from {model}.river_node as n
                                            join {model}.coverage as c on st_intersects(c.geom, n.geom)
                                            where c.id={uid}
                                            union
                                        select geom from ins_river_node) as t
                     order by geom <-> st_lineinterpolatepoint(b.geom, .5) limit 1),
                     st_lineinterpolatepoint(b.geom, .5),
                     (st_centroid(st_union((select geom from {model}.storage_node as t where contour = {did}), st_lineinterpolatepoint(b.geom, .5))))
                     ]), b.geom, {generation_step}{attribute_values}
                from border as b
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            uid=up_domain_id,
            did=down_domain_id,
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type or 'overflow',
            border=domains['border']
        ))
        # Restores initial configuration
        if cfg != 1:
            plpy.execute("""update {m}.metadata set configuration={c}""".format(m=model,c=cfg))
        return 1

    elif domains['utype'] == 'street' and domains['dtype'] == 'storage':
        plpy.execute("""
            with blade as
            (
                select (st_dumppoints('{border}'::geometry)).geom as geom
            ),
            border as (
                select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                from (select st_collect(geom) as geom from blade) as b
            ),
            street_node as (
                select st_lineinterpolatepoint(s.geom, st_linelocatepoint(s.geom, st_lineinterpolatepoint(b.geom, .5))) as geom,
                st_distance(st_lineinterpolatepoint(b.geom, .5), s.geom)/3 as dist
                from {model}.street as s, {model}.coverage as c, border as b
                where st_intersects(s.geom, c.geom)
                and c.id={uid}
            ),
            new_street_node as (
                select project.set_altitude(st_force3d(geom)) as geom
                from street_node as n
                where not exists (select 1 from {model}.crossroad_node as s where st_dwithin(s.geom, n.geom, n.dist))
            ),
            ins_street_node as (
                insert into {model}.crossroad_node(geom, z_ground, area, generated)
                select geom, st_z(geom), 1, {generation_step}
                from new_street_node
                returning id, geom
            ),
            ins_link as (
                insert into {model}.{link_type}_link(geom, border,generated{attribute_colums})
                select st_makeline(ARRAY[
                     (select geom from (select n.geom from {model}.crossroad_node as n
                                            join {model}.coverage as c on st_intersects(c.geom, n.geom)
                                            where c.id={uid}
                                            union
                                        select geom from ins_street_node) as t
                     order by geom <-> st_lineinterpolatepoint(b.geom, .5) limit 1),
                     st_lineinterpolatepoint(b.geom, .5),
                     (st_centroid(st_union((select geom from {model}.storage_node as t where contour = {did}), st_lineinterpolatepoint(b.geom, .5))))
                     ]), b.geom, {generation_step}{attribute_values}
                from border as b
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            uid=up_domain_id,
            did=down_domain_id,
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type or 'overflow',
            border=domains['border']
        ))
        plpy.execute("""DO
                    $do$
                        begin
                            if (select trigger_street_link from {model}.metadata) then
                                perform {model}.update_street_links();
                            end if;
                        end
                    $do$""".format(model=model))
        # Restores initial configuration
        if cfg != 1:
            plpy.execute("""update {m}.metadata set configuration={c}""".format(m=model,c=cfg))
        return 1

    elif domains['utype'] == 'storage' and domains['dtype'] == 'storage':
        plpy.execute("""
            with blade as
            (
                select (st_dumppoints('{border}'::geometry)).geom as geom
            ),
            border as (
                select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                from (select st_collect(geom) as geom from blade) as b
            ),
            ins_link as (
                insert into {model}.{link_type}_link(geom, border,generated{attribute_colums})
                select st_makeline(ARRAY[
                     (st_centroid(st_union((select geom from {model}.storage_node as t where contour = {uid}), st_lineinterpolatepoint(b.geom, .5)))),
                     st_lineinterpolatepoint(b.geom, .5),
                     (st_centroid(st_union((select geom from {model}.storage_node as t where contour = {did}), st_lineinterpolatepoint(b.geom, .5))))
                     ]), b.geom, {generation_step}{attribute_values}
                from border as b
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            uid=up_domain_id,
            did=down_domain_id,
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type or 'overflow',
            border=domains['border']
        ))
        # Restores initial configuration
        if cfg != 1:
            plpy.execute("""update {m}.metadata set configuration={c}""".format(m=model,c=cfg))
        return 1

    else:
        # Restores initial configuration
        if cfg != 1:
            plpy.execute("""update {m}.metadata set configuration={c}""".format(m=model,c=cfg))
        plpy.warning("link generation between {} and {} domains is not implemented".format(domains['utype'], domains['dtype']))
        return 0


def create_boundary_links(plpy, constrain_id, station_node_id, model, srid):

    generation_step = plpy.execute("""
        insert into {model}.generation_step default values returning id
        """.format(
            model=model
            ))[0]['id']

    res = plpy.execute("""
            with mesh_node as (
                select ST_Force2D(n.geom) as node, ST_Centroid(ST_Intersection(n.contour, ST_Buffer(c.geom, 0.1))) as border
                from {model}.elem_2d_node as n, {model}.constrain as c
                where c.id={cid} and ST_DWithin(n.contour, c.geom, 0.1)
            )
            insert into {model}.connector_link(geom, generated)
                select ST_MakeLine(ARRAY[ST_Centroid(ST_Collect(m.node, m.border)), m.border, n.geom]), {generation_step}
                from mesh_node as m, {model}.station_node as n
                where n.id={nid}
        """.format(
            model=model,
            cid=constrain_id,
            nid=station_node_id,
            generation_step=generation_step
        ))

def create_network_overflow_links(plpy, domain_id, model, srid):
    res = plpy.execute("""
        select d.geom as geom, d.domain_type as type
        from {model}.coverage as d
        where d.id={domain_id}
        """.format(
        domain_id=domain_id,
        model=model
    ))

    if not res:
        plpy.warning("error identifying domain {}".format(domain_id))
        return 0

    domain, = res

    # delete existing network overflows
    plpy.execute("""
        delete from {model}.network_overflow_link as l
        where ST_Contains('{geom}', l.geom)
        """.format(
            model=model,
            geom=domain['geom'])
        )

    generation_step = plpy.execute("""
        insert into {model}.generation_step default values returning id
        """.format(
            model=model
            ))[0]['id']

    if domain['type'] in ['street', 'crossroad']:
        plpy.execute("""
            with manholes as (
                select n.geom
                from {model}.manhole_node as n, {model}.coverage as c
                where ST_Contains(c.geom, n.geom)
                and c.id={id}
            ),
            crossroads as (
                select c.geom, c.z_ground
                from {model}.crossroad_node as c, {model}.coverage as cov
                where ST_Contains(cov.geom, c.geom)
                and cov.id={id}
            ),
            nodes as (
                select m.geom as up_geom,
                      (select c.geom from crossroads as c order by m.geom <-> c.geom limit 1) as down_geom,
                      (select c.z_ground from crossroads as c order by m.geom <-> c.geom limit 1) as z
                from manholes as m
            )
            insert into {model}.network_overflow_link(geom, z_overflow, generated)
            select st_makeline(ARRAY[n.up_geom,n.down_geom]), n.z, {generation_step}
            from nodes as n
        """.format(
            model=model,
            id=domain_id,
            generation_step=generation_step
            ))
        return 1

    elif domain['type'] == 'storage':
        plpy.execute("""
            with manholes as (
                select n.geom
                from {model}.manhole_node as n, {model}.coverage as c
                where ST_Contains(c.geom, n.geom)
                and c.id={id}
            ),
            nodes as (
                select m.geom as up_geom, s.geom as down_geom, s.zs_array[0][0] as z
                from manholes as m, {model}.storage_node as s
                where s.contour={id}
            )
            insert into {model}.network_overflow_link(geom, z_overflow, generated)
            select st_makeline(ARRAY[n.up_geom,st_centroid(st_union(n.up_geom, n.down_geom))]), n.z, {generation_step}
            from nodes as n
        """.format(
            model=model,
            id=domain_id,
            generation_step=generation_step
            ))
        return 1

    elif domain['type'] == '2d':
        plpy.execute("""
            with manholes as (
                select n.geom
                from {model}.manhole_node as n, {model}.coverage as c
                where ST_Contains(c.geom, n.geom)
                and c.id={id}
            ),
            nodes as (
                select m.geom as up_geom, e.geom as down_geom, e.zb as z
                from manholes as m
                join {model}.elem_2d_node as e on ST_contains(e.contour, m.geom)
            )
            insert into {model}.network_overflow_link(geom, z_overflow, generated)
            select st_makeline(ARRAY[n.up_geom,st_centroid(st_union(n.up_geom, n.down_geom))]), n.z, {generation_step}
            from nodes as n
        """.format(
            model=model,
            id=domain_id,
            generation_step=generation_step
            ))
        return 1

    else:
        plpy.warning("network overflow generation on {} is not implemented".format(domain['type']))
        return 0


def discretize_line(geom, elem_length):
    """split line if needed"""
    from shapely.geometry import LineString, Point
    from shapely import wkb
    from math import ceil

    def split_line(l, nb_elem):
        return LineString((l.interpolate(x, True).coords[0] for x in (float(i)/nb_elem for i in range(nb_elem+1))))

    line = wkb.loads(geom, True)
    points = [line.coords[0]]
    for point in line.coords[1:]:
        d = Point(points[-1]).distance(Point(point))
        if d > elem_length:
            points += list(split_line(LineString([points[-1], point]), int(ceil(d/elem_length))).coords[1:])
        else:
            points.append(point)
    out_line = LineString(points)
    geos.lgeos.GEOSSetSRID(out_line._geom, geos.lgeos.GEOSGetSRID(line._geom))
    return out_line.wkb_hex

def crossed_border(line, polygon):
    """return border (segment of exterior ring) crossed by link
    the returned segment direction makes a positive angle with tthe link lines
    """
    link = wkb.loads(line, True)
    contour = wkb.loads(polygon, True).exterior
    # find intersecting edge
    border = [LineString([n1, n2])
            for n1, n2 in zip(contour.coords[:-1], contour.coords[1:])
            if link.intersects(LineString([n1, n2]))]
    if not len(border):
        return None
    border = border[0]

    # compute angle
    u = numpy.array(contour.coords[0]) - numpy.array(contour.coords[1])
    v = numpy.array(border.coords[0]) - numpy.array(border.coords[1])
    if u[0]*v[1] - u[1]*v[0] < 0:
        border = LineString(reversed(border.coords))

    geos.lgeos.GEOSSetSRID(border._geom, geos.lgeos.GEOSGetSRID(link._geom))

    return border.wkb_hex