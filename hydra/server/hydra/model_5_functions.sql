/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/*
@todo créer un trigger de vérification spécifique:

Restriction supplémentaire pour les objets RK, ZQ, ZT, ZF : s’ils sont placés sur une
branche filaire (reach ou branche de collecteur) ils doivent être connectés au nœud down de
la branche, car ils sont interprétés comme des conditions down.
*/

/* ********************************************** */
/*              Connectivity  checks              */
/* ********************************************** */

create function ${model}.check_on_branch_or_reach_endpoint(geom geometry)
returns boolean
language plpgsql
as $$$$
    declare
        res_ boolean;
    begin
        with forbidden as (select st_startpoint(reach.geom) as point from $model.reach
                               union
                               select st_endpoint(reach.geom) as point from $model.reach
                               union
                               select st_startpoint(branch.geom) as point from $model.branch
                               union
                               select st_endpoint(branch.geom) as point from $model.branch
                               )
        select exists(select 1 from forbidden where st_intersects(geom, forbidden.point)) into res_;
        return res_;
    end;
$$$$
;;

create function ${model}.check_connected_pipes(id_ integer)
returns boolean
language plpgsql
as $$$$
    declare
        up_pipes_ integer;
        down_pipes_ integer;
    begin
        select count(*) from $model.pipe_link where down=id_ into up_pipes_;
        select count(*) from $model.pipe_link where up=id_ into down_pipes_;
        if (up_pipes_=1 or down_pipes_=1) then
            return true;
        else
            return false;
        end if;
    end;
$$$$
;;

create view $model.connectivity_manhole as
    /*nb up is the count ok link for wich the node is down*/
    with up as (select n.id as amid, sum((r.down=n.id)::integer) as nb_up
        from $model._link as r, $model._node as n
        where r.link_type=$pipe_link_type
        group by n.id
    ),
    down as (select n.id as avid, sum((r.up=n.id)::integer) as nb_down
        from $model._link as r, $model._node as n
        where r.link_type=$pipe_link_type
        group by n.id
    )
    select n.id, n.geom, nb_up, nb_down
        from $model._node as n, up, down
        where n.id=amid and n.id=avid and n.node_type=$manhole_node_type
;;

create view $model.connectivity_manhole_hydrology as
    /*nb up is the count ok link for wich the node is down*/
    with up as (select n.id as amid, sum((r.down=n.id)::integer) as nb_up
        from $model._link as r, $model._node as n
        where r.link_type=$pipe_link_type
        group by n.id
    ),
    down as (select n.id as avid, sum((r.up=n.id)::integer) as nb_down
        from $model._link as r, $model._node as n
        where r.link_type=$pipe_link_type
        group by n.id
    )
    select n.id, n.geom, nb_up, nb_down
        from $model._node as n, up, down
        where n.id=amid and n.id=avid and n.node_type=$manhole_hydrology_node_type
;;

create function ${model}.check_extremities_nodes(geom_ geometry)
returns boolean
language plpgsql
as $$$$
    declare
        up_ integer;
        down_ integer;
    begin
        select id from $model.river_node as n where st_intersects(geom, st_startpoint(geom_)) into up_;
        select id from $model.river_node as n where st_intersects(geom, st_endpoint(geom_)) into down_;

        if (up_ is not null and down_ is not null) then
            return true;
        else
            return false;
        end if;
    end;
$$$$
;;

/* ********************************************** */
/*            Constrain line spliting             */
/* ********************************************** */

create or replace function $model.split_constrain(constrain_id integer, point geometry('POINTZ', $srid))
returns boolean
language plpgsql
as $$$$
    declare
        _geom geometry('LINESTRINGZ', $srid);
        _elem_length real;
        _constrain_type hydra_constrain_type ;
        _link_attributes json;
    begin
        update $model.metadata set trigger_coverage=False;

        select geom, elem_length, constrain_type, link_attributes from $model.constrain where id=constrain_id into _geom, _elem_length, _constrain_type, _link_attributes;

        delete from $model.constrain where id=constrain_id;

        insert into $model.constrain(geom, elem_length, constrain_type, link_attributes)
            values (
                    ST_SetSRID((
                        ST_Dump(
                            ST_Split(
                                ST_SetSRID(ST_Snap(_geom, point,0.0001), $srid),
                                ST_SetSRID(point, $srid)
                                )
                            )
                        ).geom, $srid),
                    _elem_length,
                    _constrain_type,
                    _link_attributes
                    );

        update $model.metadata set trigger_coverage=True;
        perform $model.coverage_update();
    return true;
    end;
$$$$
;;

/* ********************************************** */
/*            Coverage regeneration               */
/* ********************************************** */

create function $model.coverage_update()
returns integer
language plpgsql
as $$$$
    declare
        res_ integer;
    begin
        delete from $model.coverage ; -- where domain_2d is null;
        alter sequence $model.coverage_id_seq restart;

        with cst as (
            select
                    (st_dump(
                        coalesce(
                            st_split(
                                a.discretized,
                                (select st_collect(discretized)
                                    from $model.constrain as b
                                    where a.id!=b.id
                                    and st_intersects(a.discretized, b.discretized)
                                    )
                                ),
                            a.discretized)
                    )).geom as geom
            from $model.constrain as a

        )
        -- polygonize
        insert into $model.coverage(geom, domain_type)
        select (st_dump(st_polygonize(geom))).geom, '2d' from cst;


        with oned as (
            select c.id, count(1) as ct
            from $model.coverage as c, $model.street as d
            where st_intersects(c.geom, d.geom)
            group by c.id
        )
        update $model.coverage set domain_type='street' where id in (select oned.id from oned where ct=1);

        with oned as (
            select c.id, count(1) as ct
            from $model.coverage as c, $model.street as d
            where st_intersects(c.geom, d.geom)
            group by c.id
        )
        update $model.coverage set domain_type='crossroad' where id in (select oned.id from oned where ct>1);

        update $model.coverage as c set domain_type='reach'
        where exists (select 1 from $model.open_reach as r
        where st_intersects(c.geom, r.geom)
        and st_length(st_collectionextract(st_intersection(c.geom, r.geom), 2)) > 0);

        update $model.coverage as c set domain_type='reach_end'
        where exists (select 1 from $model.open_reach as r
            where (st_intersects(c.geom, st_startpoint(r.geom)) and st_distance(st_startpoint(r.geom), st_exteriorring(c.geom)) > 5)
            or (st_intersects(c.geom,  st_endpoint(r.geom))) and st_distance(st_endpoint(r.geom), st_exteriorring(c.geom)) > 5);

        with oned as (
            select distinct c.id
            from $model.coverage as c, $model.storage_node as d
            where st_intersects(c.geom, d.geom)
        )
        update $model.coverage set domain_type='storage' where id in (select oned.id from oned);

        with oned as (
            select c.id, count(1) as ct
            from $model.coverage as c, $model.coverage_marker as mkc
            where st_intersects(c.geom, mkc.geom)
            group by c.id
        )
        update $model.coverage set domain_type=null where id in (select oned.id from oned where ct=1);

        select count(*) from $model.coverage into res_;
        return res_;
    end;
$$$$
;;

/* ********************************************** */
/*              Street regeneration               */
/* ********************************************** */

create function ${model}.crossroad_update()
returns integer
language plpgsql
as $$$$
    declare
        res_ integer;
    begin
        with nodes as (
            insert into $model.crossroad_node(geom)
                (select distinct s.geom
                    from (select (st_dumppoints(geom)).geom as geom from $model.street) as s
                    left join (select geom from $model.crossroad_node) as c
                    on s.geom=c.geom
                    where c.geom is null)
            returning id
        )
        select count(1) from nodes into res_;
        return res_;
    end;
$$$$
;;


create function ${model}.update_street_links()
returns integer
language plpgsql
as $$$$
    declare
        prec_ real;
        res_ integer;
    begin
        select precision from hydra.metadata into prec_;

        delete from $model.street_link
        where generated is not null;

        with ins as (
            select rk, width, st_makeline(
                lag(n.geom)
                over (partition by s.id order by st_linelocatepoint(s.geom, n.geom)),
                n.geom) as geom, n.generated
            from $model.street as s, $model.crossroad_node as n
            where st_dwithin(s.geom, n.geom, prec_)
        ),
        gen as (
            insert into $model.generation_step default values returning id
        ),
        links as (
            insert into $model.street_link(rk, width, geom, generated)
            select rk, width, geom, gen.id from ins, gen where geom is not null
            returning id
        )
        select count(1) from links into res_;
        return res_;
    end;
$$$$
;;

create function ${model}.gen_cst_street(street_id integer)
returns integer
language plpgsql
as $$$$
    declare
        _street_geom geometry;
        _street_width real;
        _street_el real;
    begin
        select geom, width, elem_length
        from $model.street
        where id=street_id
        into _street_geom, _street_width, _street_el;

        with buf as (
                select st_buffer(st_force2d(_street_geom), _street_width/2, 'quad_segs=1 join=mitre mitre_limit='||(_street_width*2)::varchar) as geom
            ),
            cst as (
                select st_exteriorring(geom) as geom from buf
            ),
            snp as (
                update $model.constrain set geom = st_snap(geom, _street_geom, 1)
                where ST_DWithin(geom, _street_geom, 1)
            ),
            spl as (
                select coalesce(
                    st_split(cst.geom, (select st_collect(st_force2d(c.geom))
                            from $model.constrain as c
                            where st_intersects(c.geom, cst.geom)
                            )), cst.geom) as geom
                from cst
            ),
            spld as (
                select st_force3d((st_dump(geom)).geom) as geom from spl
            ),
            diff as (
                select id, (st_dump(st_collectionextract(st_split(c.geom, b.geom), 2))).geom as geom
                from $model.constrain as c, cst as b
                where st_intersects(b.geom, c.geom)
            ),
            ins as (
                insert into $model.constrain(geom, elem_length, constrain_type)
                select d.geom, c.elem_length, c.constrain_type
                from $model.constrain as c, diff as d
                where c.id = d.id
                union
                select geom, _street_el, 'overflow'
                from spld
                returning id
            )
            delete from $model.constrain where id in (select id from diff);
            return 1;
    end;
$$$$
;;

/* ********************************************** */
/*                      Mesh tools                */
/* ********************************************** */

create or replace function ${model}.mesh(domain_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import mesh
    return mesh(plpy, domain_id, '$model', $srid)
$$$$
;;

create or replace function ${model}.create_links(up_domain_id integer, down_domain_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_links
    return create_links(plpy, up_domain_id, down_domain_id, '$model', $srid)
$$$$
;;

create or replace function ${model}.create_boundary_links(constrain_id integer, station_node_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_boundary_links
    return create_boundary_links(plpy, constrain_id, station_node_id, '$model', $srid)
$$$$
;;

create or replace function ${model}.create_elem2d_links(elem_id integer, border geometry, generation_step integer default null)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_elem2d_links
    return create_elem2d_links(plpy, elem_id, border, '$model', $srid, generation_step)
$$$$
;;

create or replace function ${model}.create_network_overflow_links(domain_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_network_overflow_links
    return create_network_overflow_links(plpy, domain_id, '$model', $srid)
$$$$
;;



/* ********************************************** */
/*                Hydrology checks                */
/* ********************************************** */

create function ${model}.check_hydrogramme_apport_on_down_route_fct()
returns boolean
language plpgsql
as $$$$
    declare
        res boolean;
    begin
        /* apres: routing with hydrograms at down */
        with routing_with_hydrogramme_apport_at_down as (
            select r.id as rid from $model._link as r, $model._hydrograph_bc_singularity as a
            where r.down=a.id and r.link_type=$routing_hydrology_link_type
        )
        select count(1)=0 from $model._link as r
        where r.down_type!=$manhole_hydrology_node_type and r.link_type=$routing_hydrology_link_type
        and r.id not in (select rid from routing_with_hydrogramme_apport_at_down) into res;
        return res;
    end;
$$$$
;;

create function ${model}.check_catchment_has_one_route_fct()
returns boolean
language plpgsql
as $$$$
    declare
        res boolean;
    begin
        with link as (
            select count(1) as count from $model.catchment_node as bv, $model._link as r
            where bv.id=r.up and r.link_type=$routing_hydrology_link_type
            group by r.id)
        select max(count)=min(count) and min(count)=1 from link into res;
        return res;
    end;
$$$$
;;

-- create function ${model}.check_clavh_is_down_fct()
-- returns boolean
-- language plpgsql
-- as $$$$
--     declare
--         res boolean;
--     begin
--         select coalesce(max(nb_down)=0, True) from $model.clavh_singularity as s, $model.connectivity_manhole_hydrology as c where c.id=s.id into res;
--         return res;
--     end;
-- $$$$

/* ********************************************** */
/*                    Profile                     */
/* ********************************************** */

create function ${model}.update_river_cross_section_pl1d(coverage_id integer)
returns integer
language plpgsql
as $$$$
    declare
        prec_ real;
        res_ integer;
        reach_id_ integer;
    begin
        select precision from hydra.metadata into prec_;

        select r.id
        from $model.coverage as c, $model.reach as r
        where st_intersects(r.geom, c.geom)
        and c.id=coverage_id
        and st_intersects(r.geom, c.geom)
        limit 1
        into reach_id_;

        delete from $model.river_cross_section_pl1d
        where generated is not null;

        -- create banks
        drop table if exists banks;
        create  table banks
        as
        with res as (
            select (st_dump(st_linemerge((st_collectionextract(st_intersection(cs.discretized, cv.geom), 2))))).geom as geom
            from $model.coverage as cv, $model.constrain as cs, $model.reach as r
            where st_intersects(r.geom, cv.geom)
            and cv.id=coverage_id
            and r.id = reach_id_
            and not st_intersects(r.geom, cs.geom)
        )
        select row_number() over() as id, geom::geometry('LINESTRINGZ', $srid) from res
        ;


        -- river points that form the bedline are the one
        -- which are the closest point of their projection and the banks
        drop table if exists bedline;
        create  table bedline
        as
        with projected as (
            select b.id as bank, st_lineinterpolatepoint(b.geom, st_linelocatepoint(b.geom, r.geom)) as geom
            from (
                select (st_dumppoints(geom)).geom as geom
                from $model.reach where id=reach_id_
                union
                select n.geom
                from $model.river_node as n
                where n.reach=reach_id_
                ) as r, banks as b
            union
            select b.id as bank, (st_dumppoints(b.geom)).geom as geom
            from banks as b
        ),
        closest as (
            select b.bank, st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, b.geom)) as geom
            from $model.reach as r, projected as b
            where r.id=reach_id_
        )
        select row_number() over() as id, c.bank, st_makeline(c.geom order by st_linelocatepoint(b.geom, c.geom)) as geom
        from closest as c, banks as b
        where c.bank=b.id
        group by c.bank
        ;

        drop table if exists midline;
        create  table midline
        as
        select row_number() over() as id, st_makeline(st_centroid(st_makeline(p.geom, st_closestpoint(b.geom, p.geom)))) as geom, p.bank
        from banks as b, (select (st_dumppoints(geom)).geom as geom, bank from bedline) as p
        where b.id=p.bank
        group by p.bank
        ;

        drop table if exists pl1d;
        create  table pl1d
        as
        select  row_number() over() as id,
            st_makeline(st_lineinterpolatepoint((select geom from banks where id = 1), i::real/10),
                        st_lineinterpolatepoint((select geom from banks where id = 2), i::real/10)) as geom,
                    1 as node
        from generate_series(1, 10) as i
        ;
        return 1;
    end;
$$$$
;;

-- plpython3u doesn't support multidim arrays yet, so we pass it as string and cast hence the use of _street_after_insert_trig , ignore_points=False, distance_point=100, discretization=150)
create or replace function ${model}._str_profile_from_transect(flood_plain_transect geometry, interpolate boolean default 'f'::boolean, discretization integer default '150'::integer, ignore_points boolean default 'f'::boolean, distance_point integer default '100'::integer)
returns varchar
language plpython3u immutable
as
$$$$
    import plpy
    from $hydra import profile_from_transect
    return profile_from_transect(plpy, flood_plain_transect, '$model', $srid, interpolate, discretization, ignore_points, distance_point)
$$$$
;;

create function ${model}.profile_from_transect(flood_plain_transect geometry, interpolate boolean default 'f'::boolean, discretization integer default '150'::integer, ignore_points boolean default 'f'::boolean, distance_point integer default '100'::integer)
returns real[][]
language plpgsql immutable
as
$$$$
    begin
        return $model._str_profile_from_transect(flood_plain_transect, interpolate, discretization, ignore_points, distance_point)::real[][];
    end;
$$$$
;;

-- closest section on the same reach
create function $model.upstream_valley_section(transect geometry)
returns integer
language plpgsql immutable
as
$$$$
    begin
        return (
        with reach as (
            select r.id, st_intersection(r.geom, transect) as inter, geom, pk0_km
            from ${model}.reach as r
            where st_intersects(r.geom, transect)
        ),
        pk as (
            select st_linelocatepoint(geom, inter)*st_length(geom)/1000+pk0_km as pk_km from reach
        ),
        up_reach as (
            select nup.reach as id
            from ${model}.river_node as nup
            join ${model}.connector_link as l on l.up=nup.id
            join ${model}.river_node as ndown on ndown.id=l.down
            where l.main_branch
            and ndown.reach = (select id from reach)
        ),
        extended_reach_node as (
            select n.id, n.pk_km
            from ${model}.river_node as n, pk
            where n.reach in (select id from reach union select id from up_reach)
            and n.pk_km <= pk.pk_km
        ),
        section_up as (
            select c.id
            from extended_reach_node as n
            join ${model}.river_cross_section_profile as c on c.id = n.id,
            pk
            where ((c.type_cross_section_up='valley' and c.up_vcs_geom is not null)
                or (c.type_cross_section_down='valley' and c.down_vcs_geom is not null))
            order by n.pk_km desc
            limit 1
        )
        select id from section_up);
    end;
$$$$
;;

/* ********************************************** */
/*                Altitude handling               */
/* ********************************************** */

create or replace function ${model}.set_link_z_and_altitude(link_id integer)
returns integer
language plpgsql
as $$$$
    declare
        _res integer;
    begin
        perform ${model}.set_link_ext_z(link_id);

        select ${model}.set_link_altitude(link_id) into _res;

        return _res;
    end;
$$$$
;;

create or replace function ${model}.set_link_altitude(link_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import set_link_altitude
    return set_link_altitude(plpy, link_id, '$model', $srid)
$$$$
;;

create or replace function ${model}.set_link_ext_z(link_id integer)
returns boolean
language plpgsql
as $$$$
    begin

        update ${model}._link as l
        set geom=ST_SetPoint(
                    ST_SetPoint(
                        l.geom,
                        0,
                        ST_MakePoint(
                            ST_X(ST_StartPoint(l.geom)),
                            ST_Y(ST_StartPoint(l.geom)),
                            ST_Z(nup.geom)
                            )
                        ),
                    -1,
                    ST_MakePoint(
                        ST_X(ST_EndPoint(l.geom)),
                        ST_Y(ST_EndPoint(l.geom)),
                        ST_Z(ndown.geom)
                        )
                    )
        from ${model}._node as nup, ${model}._node as ndown
        where l.id=link_id and nup.id=l.up and ndown.id=l.down;

        return 't';
    end;
$$$$
;;

create function ${model}.line_set_z(
    in line geometry('LINESTRINGZ', $srid),
    in z_up real,
    in z_down real)
returns geometry('LINESTRINGZ', $srid)
language plpgsql
as $$$$
    declare
        geom geometry('LINESTRINGZ', $srid);
    begin
        with d as (select (ST_DumpPoints(line)).geom as geom),
        p as (select ST_X(d.geom) as x, ST_Y(d.geom) as y,
            ST_LineLocatePoint(line, d.geom) as a
            from d),
        i as (select x, y, (1-a)*z_up + a*z_down as z, a from p)
        select ST_SetSRID(ST_Makeline(ST_MakePoint(x, y, z) order by a), $srid) as geom from i into geom;
        return geom;
    end;
$$$$
;;

-- for any point in reach coverage, give the transect interpolated at this point
-- for points on the border bewteen two reach coverage, you can specify the coverage id
create function ${model}.interpolate_transect_at(point geometry, coverage_id integer default null)
returns geometry('LINESTRINGZ', $srid)
language plpgsql
as $$$$
    declare
        l_ real;
    begin
        l_ = 10000;
        --raise warning 'interpolate_transect_at % %', point, coverage_id;
        return (
            with reach as (
                select r.id, r.geom, c.geom as flood_plain
                from $model.coverage as c, $model.reach as r
                where st_intersects(r.geom, c.geom)
                and domain_type='reach'
                and coalesce(c.id = coverage_id, 't'::boolean)
                order by c.geom <-> point
                limit 1
            ),
            transect_piece as (
                select st_linemerge(st_intersection(c.discretized, r.flood_plain)) as geom
                from $model.constrain as c, reach as r
                where st_intersects(r.flood_plain, c.discretized)
                and st_intersects(r.geom, c.geom)
                and c.constrain_type='flood_plain_transect'
                and st_length(st_collectionextract(st_intersection(c.discretized, r.flood_plain), 2)) > 0
            ),
            transect as (
                select st_makeline(
                    st_translate(st_startpoint(c.geom), -l_*sin(2*pi()-st_azimuth(st_startpoint(c.geom), st_endpoint(c.geom))), l_*cos(2*pi()-st_azimuth(st_startpoint(c.geom), st_endpoint(c.geom)))),
                    st_translate(st_startpoint(c.geom), l_*sin(2*pi()-st_azimuth(st_startpoint(c.geom), st_endpoint(c.geom))), -l_*cos(2*pi()-st_azimuth(st_startpoint(c.geom), st_endpoint(c.geom))))
                ) as geom, st_azimuth(st_startpoint(c.geom), st_endpoint(c.geom)) as azi
                from transect_piece as c
            ),
            dist as (
                select (case
                            when
                            abs((select azi from transect order by azi desc limit 1)-(select azi from transect order by azi asc limit 1)) > pi()
                                then case
                                when azi = (select azi from transect order by azi desc limit 1)
                                    then azi-pi()
                                    else azi+pi() end
                                else azi end)
                            as azi, st_distance(point, geom) as d from transect
            ),
            ct as  (
                select count(1) as n from dist
            ),
            disttot as (
                select sum(d) as sumd from dist
            ),
            interp as (
                select case when ct.n=1 then (
                    select azi from dist
                )
                when ct.n=2 then (
                    select sum(azi*(1-d/sumd)) as azi
                    from dist, disttot
                )
                end as azi
                from ct
            )
            select st_intersection(st_makeline(
                st_translate(point, l_*sin(2*pi()-azi), -l_*cos(2*pi()-azi)),
                st_translate(point, -l_*sin(2*pi()-azi), l_*cos(2*pi()-azi))), flood_plain)
            from interp, reach
        );
    end;
$$$$
;;