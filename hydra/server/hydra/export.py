# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from shapely import wkb
from shapely.geometry import Point, LineString

def reach_dat(plpy, model, id_):
    out = "*reach\n"
    res = plpy.execute("""
        select name, geom from {model}.reach where id={id}
        """.format(model=model, id=id_))[0]
    out += res['name']+"\n"
    out += '\n'.join(['{} {}'.format(c[0], c[1]) for c in wkb.loads(res['geom'], True).coords])
    out += '\n\n'

    res = plpy.execute("""
        with stuf as (
            select st_exteriorring(c.geom) as geom, st_collect(st_linemerge(st_intersection(c.geom, cs.discretized))) as transect, r.geom as reach
            from {model}.coverage as c, {model}.open_reach as r , {model}.constrain as cs
            where st_intersects(r.geom, c.geom)
            and r.reach={id}
            and cs.constrain_type='flood_plain_transect'
            and st_dimension(st_intersection(c.geom, cs.discretized))=1
            group by c.geom, r.geom
        )
        select * from stuf where st_numgeometries(transect) = 2
        """.format(model=model, id=id_))
    for r in res:
        transects = wkb.loads(r['transect'], True)
        reach = wkb.loads(r['reach'], True)
        banks = wkb.loads(r['geom'], True).difference(transects)
        if isinstance(transects, LineString) or isinstance(banks, LineString) or len(banks.geoms) != 2:
            continue
        up = 0 if reach.project(transects[0].interpolate(.5, normalized=True)) < reach.project(transects[1].interpolate(.5, normalized=True)) else 1
        down = (up+1)%2

        left = 0 if banks[0].intersects(Point(transects[up].coords[0])) else 1
        right = (left+1)%2
        for l in [transects[up], banks[right], transects[down], banks[left]]:
            out += "{}\n".format(len(list(l.coords)))
            out += '\n'.join(['{} {}'.format(c[0], c[1]) for c in l.coords])
            out += "\n"
        out += "\n"

    return out

def branch_dat(plpy, model, id_):
    out = ""
    res = plpy.execute("""
        with points as (
            select distinct on (geom) b.id as branch, (St_DumpPoints(p.geom)).geom as geom
            from {model}.pipe_link as p
            inner join {model}.branch as b on b.id=p.branch
            where b.id={id}
            )
        select b.name as b_name, St_X(p.geom) as x, St_Y(p.geom) as y
        from points as p
        inner join {model}.branch as b on b.id=p.branch
        where b.id={id}
        order by ST_LineLocatePoint(b.geom, St_closestPoint(b.geom, (p.geom))) asc
        """.format(model=model, id=id_))
    if res:
        out = "*branch\n"
        out += res[0]['b_name']+"\n"
        out += '\n'.join(['{} {}'.format(p['x'], p['y']) for p in res])
        out += '\n\n'
    return out

def _table_dat(plpy, model, id_, table):
    out = ""
    res = plpy.execute("""
        select e.name, st_x(e.geom) as x, st_y(e.geom) as y, st_z(e.geom) as z
        from {model}.{table} as e, {model}.coverage as c
        where c.id={id}
        and st_intersects(e.geom, c.geom)
        """.format(model=model, id=id_, table=table))
    if res:
        out += '\n'.join(['{} {} {} {}'.format(r['name'], r['x'], r['y'], r['z']) for r in res])
        out += '\n\n'

    res = plpy.execute("""
        select st_exteriorring(geom) as geom from {model}.coverage where id={id}
        """.format(model=model, id=id_))[0]
    out += '\n'.join(['{} {}'.format(c[0], c[1]) for c in wkb.loads(res['geom'], True).coords])
    out += '\n\n'

    return out

def twod_dat(plpy, model, id_):
    return  "*2d\n"+_table_dat(plpy, model, id_, 'elem_2d_node')

def street_dat(plpy, model, id_):
    return  "*street\n"+_table_dat(plpy, model, id_, 'crossroad_node')

def crossroad_dat(plpy, model, id_):
    return  "*crossroad\n"+_table_dat(plpy, model, id_, 'crossroad_node')

def storage_dat(plpy, model, id_):
    out = "*storage\n"
    res = plpy.execute("""
        select e.name, e.zs_array[1][1] as z
        from {model}.storage_node as e, {model}.coverage as c
        where c.id={id}
        and st_intersects(e.geom, c.geom)
        """.format(model=model, id=id_))
    if res:
        out += '\n'.join(['{} {}'.format(r['name'], r['z']) for r in res])
        out += '\n\n'

    res = plpy.execute("""
        select st_exteriorring(geom) as geom from {model}.coverage where id={id}
        """.format(model=model, id=id_))[0]
    out += '\n'.join(['{} {}'.format(c[0], c[1]) for c in wkb.loads(res['geom'], True).coords])
    out += '\n\n'
    return out

def geom_dat(plpy, model, type_, id_):
    if type_ == 'reach':
        return reach_dat(plpy, model, id_)
    elif type_ == 'branch':
        return branch_dat(plpy, model, id_)
    elif type_ == '2d':
        return twod_dat(plpy, model, id_)
    elif type_ == 'street':
        return street_dat(plpy, model, id_)
    elif type_ == 'storage':
        return storage_dat(plpy, model, id_)
    elif type_ == 'crossroad':
        return crossroad_dat(plpy, model, id_)

    return None


def ctrz_dat(plpy, model):
    if plpy.execute("""
                select count(1) as ct from {model}.coverage
                """.format(model=model))[0]['ct'] == 0:
        return ""

    union = wkb.loads(
            plpy.execute("""
                select st_geometryn(st_union(geom), 1) as geom from {model}.coverage
                """.format(model=model))[0]['geom'], True)
    return '\n\n'.join(['\n'.join(['{} {}'.format(c[0], c[1]) for c in r.coords]) for r in [union.exterior] + list(union.interiors)])
