# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
packaging script for the hydra documentation

USAGE
    build.py [-h]

OPTIONS
    -h, --help
        print this help

    -o, --open
        open index of generated doc
"""

import os
import getopt
import sys
import shutil
import shlex
from subprocess import Popen, PIPE
from  ..utility import package
from  ..database.database_doc import DatabaseDocumentation

def create_package(build_dir):
    zip_filename = os.path.join(build_dir, "hydra.zip")
    package.zip_(zip_filename)
    package.install(build_dir, zip_filename)

if __name__ == "__main__":

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "ho",
                ["help", "help"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    current_dir = os.path.abspath(os.path.dirname(__file__))
    build_dir = os.path.join(current_dir, "build")
    html_dir = os.path.join(build_dir, "html")
    source_dir =  os.path.join(current_dir, "source")
    doctrees_dir = os.path.join(build_dir, "doctrees")
    module_dir = os.path.join(build_dir, "hydra")
    api_dir = os.path.join(source_dir, "api")
    static_dir = os.path.join(source_dir, "_static")

    def exec_cmd(cmd):
        print " ".join(cmd)
        out, err = Popen(cmd, stderr=PIPE, stdout=PIPE).communicate()
        if err :
            sys.stderr.write("\n".join(["failed: %s\n"%(e) for e in err.split('\n') if len(e)]))
            exit(1)

    if not os.path.isdir(build_dir):
        os.mkdir(build_dir)

    if not os.path.isdir(static_dir):
        os.mkdir(static_dir)

    if os.path.isdir(api_dir):
        shutil.rmtree(api_dir)
    os.mkdir(api_dir)

    if not os.path.isdir(html_dir):
        os.mkdir(html_dir)

    if not os.path.isdir(os.path.join(html_dir, 'model')):
        os.mkdir(os.path.join(html_dir, 'model'))
        shutil.copyfile(os.path.join(source_dir, 'model', 'vue_ensemble.svg'), os.path.join(html_dir, 'model', 'vue_ensemble.svg'))

    create_package(build_dir)
    shutil.rmtree(os.path.join(module_dir, 'server'))

    open(os.path.join(source_dir, "generated_er_graph.rst"), "w").write(
            "ER Graph\n########\n\n.. graphviz::\n\n"+DatabaseDocumentation().dot())

    exec_cmd(shlex.split("sphinx-apidoc -e -d2 -T -o")+[api_dir, module_dir, "**/*_test.py", "**/*_demo.py"])
    exec_cmd(shlex.split("sphinx-build -E -b html -d")+[doctrees_dir, source_dir, html_dir])

    print "generated in {}".format(html_dir)

    if "-o" in optlist or "--open" in optlist:
        os.system(os.path.abspath(os.path.join(html_dir, "index.html")))

    print 'ok'

