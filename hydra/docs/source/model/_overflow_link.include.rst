_overflow_link
--------------
type=21


=========================  ==============================  ============  ============  ================
Attribut                   Type                            Défaut        Contrainte    Clef Etrangère
=========================  ==============================  ============  ============  ================
z_crest1*                  real
width1*                    real
z_crest2*                  real                            new.z_crest1
width2*                    real                            0
cc*                        real                            .6
lateral_contraction_coef*  real                            1
break_mode*                hydra_fuse_spillway_break_mode  'none'
z_break*                   real
t_break*                   real
z_invert*                  real
width_breach*              real
grp*                       integer                         1
dt_fracw_array*            real[10][2]
border*                    LINESTRINGZ
=========================  ==============================  ============  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        up_type in
        ('river', 'storage', 'elem_2d', 'crossroad')
        and down_type in
        ('river', 'storage', 'elem_2d', 'crossroad')

Validité:

    .. code-block:: sql

        z_crest1 is not null
        width1 is not null
        width1>=0
        z_crest2 is not null
        z_crest2>=z_crest1
        width2 is not null
        width2>=0
        cc is not null
        cc<=1
        cc>=0
        lateral_contraction_coef is not null
        lateral_contraction_coef<=1
        lateral_contraction_coef>=0
        break_mode is not null
        break_mode!='zw_critical' or z_break is not null
        break_mode!='time_critical' or t_break is not null
        break_mode='none' or width_breach is not null
        break_mode='none' or z_invert is not null
        break_mode='none' or grp is not null
        break_mode='none' or grp>0
        break_mode='none' or grp<100
        break_mode='none' or dt_fracw_array is not null
        break_mode='none' or array_length(dt_fracw_array, 1)<=10
        break_mode='none' or array_length(dt_fracw_array, 1)>=1
        break_mode='none' or array_length(dt_fracw_array, 2)=2

