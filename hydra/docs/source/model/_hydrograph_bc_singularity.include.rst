_hydrograph_bc_singularity
--------------------------
type=17


========================  ============================  ========  ==================  =========================================================================================
Attribut                  Type                          Défaut    Contrainte          Clef Etrangère
========================  ============================  ========  ==================  =========================================================================================
storage_area*             real
tq_array*                 real[20][2]
constant_dry_flow*        real                          0
distrib_coef*             real                          1
lag_time_hr*              real                          0
sector*                   integer                                 on delete set null  project.dry_inflow_sector(id) :ref:`🔗 <project-dry-inflow-sector>`
hourly_modulation*        integer                                                     project.dry_inflow_hourly_modulation(id) :ref:`🔗 <project-dry-inflow-hourly-modulation>`
pollution_quality_mode*   hydra_pollution_quality_mode
pollution_quality_param*  json
external_file_data        boolean                       'f'
========================  ============================  ========  ==================  =========================================================================================

Contraintes de connectivité:

    .. code-block:: sql

        node_type!='catchment'
        and node_type!='manhole_hydrology'

Validité:

    .. code-block:: sql

        storage_area is not null
        storage_area>=0
        constant_dry_flow is not null
        constant_dry_flow>=0
        distrib_coef is null or distrib_coef>=0
        lag_time_hr is null or (lag_time_hr>=0)
        sector is null or (distrib_coef is not null and lag_time_hr is not null)
        external_file_data or tq_array is not null
        external_file_data or array_length(tq_array, 1)<=10
        external_file_data or array_length(tq_array, 1)>=1
        external_file_data or array_length(tq_array, 2)=2

