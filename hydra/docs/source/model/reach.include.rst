reach
-----



==========  ===========  ========  ================  ================
Attribut    Type         Défaut    Contrainte        Clef Etrangère
==========  ===========  ========  ================  ================
id          serial                 primary key
name        varchar(16)  calculé   unique
pk0_km      real         0
dx          real         50
geom        LINESTRINGZ            ST_IsValid(geom)
==========  ===========  ========  ================  ================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('REACH_')
