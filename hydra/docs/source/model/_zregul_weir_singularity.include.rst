_zregul_weir_singularity
------------------------
type=5


====================================  =====================  ===========  ============  ================
Attribut                              Type                   Défaut       Contrainte    Clef Etrangère
====================================  =====================  ===========  ============  ================
z_invert*                             real
z_regul*                              real
width*                                real
cc*                                   real                   .6
mode_regul*                           hydra_weir_mode_regul  'elevation'
reoxy_law*                            hydra_weir_mode_reoxy  'gameson'
reoxy_param*                          json
full_section_discharge_for_headloss*  boolean                't'
====================================  =====================  ===========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type in
        ('river', 'manhole')

Validité:

    .. code-block:: sql

        z_invert is not null 
        z_regul is not null 
        width is not null
        width>=0
        cc is not null
        cc<=1
        cc>=0
        mode_regul is not null
        reoxy_law is not null
        reoxy_param is not null
        not model.check_on_branch_or_reach_endpoint(new.geom)

