_fuse_spillway_link
-------------------
type=15


===============  ==============================  ========  ============  ================
Attribut         Type                            Défaut    Contrainte    Clef Etrangère
===============  ==============================  ========  ============  ================
z_invert*        real
width*           real
cc*              real                            .6
break_mode*      hydra_fuse_spillway_break_mode  'none'
z_break*         real
t_break*         real
grp*             integer                         1
dt_fracw_array*  real[10][2]
===============  ==============================  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        up_type not in
        ('catchment', 'manhole_hydrology')
        and down_type not in
        ('catchment', 'manhole_hydrology')

Validité:

    .. code-block:: sql

        z_invert is not null
        width is not null
        width>=0.
        cc is not null
        cc<=1
        cc>=0.
        break_mode is not null
        break_mode!='zw_critical' or z_break is not null
        break_mode!='time_critical' or t_break is not null
        break_mode='none' or grp is not null
        break_mode='none' or grp>0
        break_mode='none' or grp<100
        break_mode='none' or dt_fracw_array is not null
        break_mode='none' or array_length(dt_fracw_array, 1)<=10
        break_mode='none' or array_length(dt_fracw_array, 1)>=1
        break_mode='none' or array_length(dt_fracw_array, 2)=2

