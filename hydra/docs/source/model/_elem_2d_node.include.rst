_elem_2d_node
-------------
type=5


==========  ========  ========  ===============  ==============================================
Attribut    Type      Défaut    Contrainte       Clef Etrangère
==========  ========  ========  ===============  ==============================================
area*       real      calculé
zb*         real      calculé
rk*         real      12
domain_2d*  integer             voir ci-dessous  model.domain_2d(id) :ref:`🔗 <model-domain-2d>`
contour     POLYGONZ            voir ci-dessous
==========  ========  ========  ===============  ==============================================

Champs calculés:

    zb

    .. code-block:: sql

        select ST_Z(ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), srid), (select precision
        from hydra.metadata)))

    area

    .. code-block:: sql

        ST_Area(new.contour)

Contraintes:

    .. code-block:: sql

        on delete cascade on update cascade

    .. code-block:: sql

        ST_IsValid(contour)
        and ST_NPoints(contour) in (4,5)

Validité:

    .. code-block:: sql

        area is not null
        area>0
        zb is not null
        rk is not null
        rk>0

