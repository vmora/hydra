coverage
--------



============  ===================  ========  ================  ================
Attribut      Type                 Défaut    Contrainte        Clef Etrangère
============  ===================  ========  ================  ================
id            serial                         primary key
domain_type*  hydra_coverage_type
geom          POLYGONZ                       ST_IsValid(geom)
============  ===================  ========  ================  ================
