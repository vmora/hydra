valley_cross_section_topo_geometry
----------------------------------



==========  ============  ========  ===============  ================
Attribut    Type          Défaut    Contrainte       Clef Etrangère
==========  ============  ========  ===============  ================
id          serial                  primary key
name        varchar(24)   calculé   unique
xz_array*   real[150][2]            voir ci-dessous
==========  ============  ========  ===============  ================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('VAT_')

Contraintes:

    .. code-block:: sql

        array_length(xz_array, 1)<=150
        and array_length(xz_array, 1)>=1
        and array_length(xz_array, 2)=2
