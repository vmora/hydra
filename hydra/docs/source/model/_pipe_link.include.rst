_pipe_link
----------
type=20


======================  ========================  ==========  ==================  =================================================================================
Attribut                Type                      Défaut      Contrainte          Clef Etrangère
======================  ========================  ==========  ==================  =================================================================================
comment*                varchar(256)
z_invert_up*            real
z_invert_down*          real
cross_section_type*     hydra_cross_section_type  'circular'
h_sable*                real                      0
branch*                 integer                               on delete set null  model.branch(id) :ref:`🔗 <model-branch>`
rk*                     real
custom_length*          real                      null
circular_diameter*      real
ovoid_height*           real
ovoid_top_diameter*     real
ovoid_invert_diameter*  real
cp_geom*                integer                                                   model.closed_parametric_geometry(id) :ref:`🔗 <model-closed-parametric-geometry>`
op_geom*                integer                                                   model.open_parametric_geometry(id) :ref:`🔗 <model-open-parametric-geometry>`
======================  ========================  ==========  ==================  =================================================================================

Contraintes de connectivité:

    .. code-block:: sql

        (up_type='manhole_hydrology'
        and down_type='manhole_hydrology')
        or (up_type='manhole'
        and down_type='manhole')

Validité:

    .. code-block:: sql

        z_invert_up is not null
        z_invert_down is not null
        h_sable is not null
        cross_section_type is not null
        cross_section_type not in ('valley')
        rk is not null
        rk >0
        cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)
        cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)
        cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)
        cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )
        cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)
        cross_section_type!='pipe' or cp_geom is not null
        cross_section_type!='channel' or op_geom is not null

