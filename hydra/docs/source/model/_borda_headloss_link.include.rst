_borda_headloss_link
--------------------
type=10


==========  ==============================  ========  ============  ================
Attribut    Type                            Défaut    Contrainte    Clef Etrangère
==========  ==============================  ========  ============  ================
law_type*   hydra_borda_headloss_link_type
param*      json
==========  ==============================  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        up_type not in
        ('catchment', 'manhole_hydrology')
        and down_type not in
        ('catchment', 'manhole_hydrology')

Validité:

    .. code-block:: sql

        law_type is not null
        param is not null

