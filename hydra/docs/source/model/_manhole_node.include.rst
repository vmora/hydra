_manhole_node
-------------
type=1


==========  ======  ========  ============  ================
Attribut    Type    Défaut    Contrainte    Clef Etrangère
==========  ======  ========  ============  ================
area*       real    1
z_ground*   real    calculé
==========  ======  ========  ============  ================

Champs calculés:

    z_ground

    .. code-block:: sql

        select project.altitude(new.geom)

Validité:

    .. code-block:: sql

        area is not null
        area>0
        z_ground is not null
        (select not exists(select 1 from model.station where st_intersects(geom, new.geom)))
        model.check_connected_pipes(new.id)

