_singularity
------------



================  ======================  ========  ============  ================
Attribut          Type                    Défaut    Contrainte    Clef Etrangère
================  ======================  ========  ============  ================
id                serial                            primary key
singularity_type  hydra_singularity_type
name              varchar(24)                       unique
node              integer                           unique
node_type         hydra_node_type
configuration*    json
validity*         boolean
================  ======================  ========  ============  ================

Contraintes:

    .. code-block:: sql

        unique(id, singularity_type)

    .. code-block:: sql

        foreign key(node, node_type)
        references model._node(id, node_type)
