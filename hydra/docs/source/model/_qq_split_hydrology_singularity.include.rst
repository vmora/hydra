_qq_split_hydrology_singularity
-------------------------------
type=20


================  ===============  ========  ============  ================
Attribut          Type             Défaut    Contrainte    Clef Etrangère
================  ===============  ========  ============  ================
qq_array*         real[10][3]
downstream*       integer
downstream_type*  hydra_link_type  calculé
split1*           integer
split1_type*      hydra_link_type  calculé
split2*           integer
split2_type*      hydra_link_type  calculé
================  ===============  ========  ============  ================

Champs calculés:

    split2_type

    .. code-block:: sql

        select link_type
        from model._link
        where id=new.split2

    downstream_type

    .. code-block:: sql

        select link_type
        from model._link
        where id=new.downstream

    split1_type

    .. code-block:: sql

        select link_type
        from model._link
        where id=new.split1

Contraintes:

    .. code-block:: sql

        foreign key(downstream, downstream_type)
        references model._link(id, link_type)

    .. code-block:: sql

        downstream_type='pipe' or downstream_type='connector_hydrology'

    .. code-block:: sql

        foreign key(split1, split1_type)
        references model._link(id, link_type)

    .. code-block:: sql

        split1_type='pipe' or split1_type='connector_hydrology'

    .. code-block:: sql

        foreign key(split2, split2_type)
        references model._link(id, link_type)

    .. code-block:: sql

        split2 is null or split2_type='pipe' or split2_type='connector_hydrology'

Contraintes de connectivité:

    .. code-block:: sql

        node_type='manhole_hydrology'

Validité:

    .. code-block:: sql

        qq_array is not null
        downstream is not null
        split1 is not null
        array_length(qq_array, 1)<=10
        array_length(qq_array, 1)>=1
        (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)

