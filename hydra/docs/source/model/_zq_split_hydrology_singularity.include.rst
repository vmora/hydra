_zq_split_hydrology_singularity
-------------------------------
type=21


=================  ====================  ========  ============  ================
Attribut           Type                  Défaut    Contrainte    Clef Etrangère
=================  ====================  ========  ============  ================
downstream*        integer
downstream_type*   hydra_link_type       calculé
split1*            integer
split1_type*       hydra_link_type       calculé
split2*            integer
split2_type*       hydra_link_type       calculé
downstream_law     hydra_split_law_type  'weir'
downstream_param*  json
split1_law*        hydra_split_law_type
split1_param*      json
split2_law*        hydra_split_law_type
split2_param*      json
=================  ====================  ========  ============  ================

Champs calculés:

    split2_type

    .. code-block:: sql

        select link_type
        from model._link
        where id=new.split2

    downstream_type

    .. code-block:: sql

        select link_type
        from model._link
        where id=new.downstream

    split1_type

    .. code-block:: sql

        select link_type
        from model._link
        where id=new.split1

Contraintes:

    .. code-block:: sql

        foreign key (downstream, downstream_type)
        references model._link(id, link_type)

    .. code-block:: sql

        downstream_type='pipe' or downstream_type='connector_hydrology'

    .. code-block:: sql

        foreign key (split1, split1_type)
        references model._link(id, link_type)

    .. code-block:: sql

        split1_type='pipe' or split1_type='connector_hydrology'

    .. code-block:: sql

        foreign key (split2, split2_type)
        references model._link(id, link_type)

    .. code-block:: sql

        split2 is null or split2_type='pipe' or split2_type='connector_hydrology'

Contraintes de connectivité:

    .. code-block:: sql

        node_type='manhole_hydrology'

Validité:

    .. code-block:: sql

        downstream is not null
        split1 is not null
        downstream_param is not null
        split1_law is not null
        split1_param is not null
        split2 is null or split2_law is not null
        split2 is null or split2_param is not null

