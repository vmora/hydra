_bridge_headloss_singularity
----------------------------
type=25


====================================  ===========  ========  ============  ================
Attribut                              Type         Défaut    Contrainte    Clef Etrangère
====================================  ===========  ========  ============  ================
l_road*                               real
z_road*                               real
zw_array*                             real[10][2]
full_section_discharge_for_headloss*  boolean      't'
====================================  ===========  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type='river'

Validité:

    .. code-block:: sql

        l_road is not null
        l_road>=0
        z_road is not null
        zw_array is not null 
        array_length(zw_array, 1)<=10
        array_length(zw_array, 1)>=1
        array_length(zw_array, 2)=2
        not model.check_on_branch_or_reach_endpoint(new.geom)

