_street_link
------------
type=16


==========  ======  ===================  ============  ================
Attribut    Type    Défaut               Contrainte    Clef Etrangère
==========  ======  ===================  ============  ================
width*      real
length*     real    ST_Length(new.geom)
rk*         real    30.
==========  ======  ===================  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        up_type='crossroad'
        and down_type='crossroad'

Validité:

    .. code-block:: sql

        width is not null
        width>=0
        length is not null
        length>=0
        rk is not null
        rk>0

