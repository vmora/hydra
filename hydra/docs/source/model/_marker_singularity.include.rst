_marker_singularity
-------------------
type=7


==========  ============  ========  ============  ================
Attribut    Type          Défaut    Contrainte    Clef Etrangère
==========  ============  ========  ============  ================
comment*    varchar(256)
==========  ============  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type in
        ('river', 'manhole')
