_porous_link
------------
type=13


==============  ===========  ========  ============  ================
Attribut        Type         Défaut    Contrainte    Clef Etrangère
==============  ===========  ========  ============  ================
z_invert*       real
width*          real
transmitivity*  real
border*         LINESTRINGZ
==============  ===========  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        up_type not in
        ('catchment', 'manhole_hydrology')
        and down_type not in
        ('catchment', 'manhole_hydrology')

Validité:

    .. code-block:: sql

        z_invert is not null
        width is not null
        width>=0
        transmitivity is not null
        transmitivity>=0

