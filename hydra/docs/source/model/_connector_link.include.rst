_connector_link
---------------
type=9


============  =======  ========  ============  ================
Attribut      Type     Défaut    Contrainte    Clef Etrangère
============  =======  ========  ============  ================
main_branch*  boolean  'f'
============  =======  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        up_type not in
        ('catchment', 'manhole_hydrology')
        and down_type not in
        ('catchment', 'manhole_hydrology')
