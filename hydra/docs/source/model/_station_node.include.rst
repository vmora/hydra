_station_node
-------------
type=3


==========  =======  ========  ============  ===========================================
Attribut    Type     Défaut    Contrainte    Clef Etrangère
==========  =======  ========  ============  ===========================================
area*       real     1
z_invert*   real     calculé
station     integer  calculé                 model.station(id) :ref:`🔗 <model-station>`
==========  =======  ========  ============  ===========================================

Champs calculés:

    z_invert

    .. code-block:: sql

        select project.altitude(new.geom)

    station

    .. code-block:: sql

        select id
        from model.station
        where ST_Intersects(new.geom, geom)

Validité:

    .. code-block:: sql

        area is not null
        area>0
        z_invert is not null
        station is not null 
        (select st_intersects(new.geom, geom) from model.station where id=new.station)

