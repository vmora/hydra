domain_2d
---------



==========  ============  ========  ===============  ================
Attribut    Type          Défaut    Contrainte       Clef Etrangère
==========  ============  ========  ===============  ================
id          serial                  primary key
name        varchar(16)   calculé   voir ci-dessous
geom*       POLYGONZ
comment*    varchar(256)
==========  ============  ========  ===============  ================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('DOMAIN_2D_')

Contraintes:

    .. code-block:: sql

        unique   check (name <> 'default_domain')
