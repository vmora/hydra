_zq_bc_singularity
------------------
type=13


==========  ===========  ========  ============  ================
Attribut    Type         Défaut    Contrainte    Clef Etrangère
==========  ===========  ========  ============  ================
zq_array*   real[10][2]
==========  ===========  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type!='catchment'
        and node_type!='manhole_hydrology'

Validité:

    .. code-block:: sql

        zq_array is not null
        array_length(zq_array, 1)<=10
        array_length(zq_array, 1)>=1
        array_length(zq_array, 2)=2

