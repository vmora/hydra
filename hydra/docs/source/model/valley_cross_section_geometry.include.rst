valley_cross_section_geometry
-----------------------------



==================  ===========  ========  ===============  ================================================
Attribut            Type         Défaut    Contrainte       Clef Etrangère
==================  ===========  ========  ===============  ================================================
id                  serial                 primary key
name                varchar(24)  calculé   unique
zbmin_array         real[6][2]             voir ci-dessous
zbmaj_lbank_array*  real[4][2]             voir ci-dessous
zbmaj_rbank_array*  real[4][2]             voir ci-dessous
rlambda             real         1         voir ci-dessous
rmu1                real         -999
rmu2                real         -999
zlevee_lb           real         -999
zlevee_rb           real         -999
transect*           integer      null                       model._constrain(id) :ref:`🔗 <model-constrain>`
t_ignore_pt         boolean      False
t_discret           int          150
t_distance_pt       int          10
==================  ===========  ========  ===============  ================================================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('VA_')

Contraintes:

    .. code-block:: sql

        array_length(zbmin_array, 1)<=6
        and array_length(zbmin_array, 1)>=1
        and array_length(zbmin_array, 2)=2

    .. code-block:: sql

        array_length(zbmaj_lbank_array, 1)<=4
        and array_length(zbmaj_lbank_array, 1)>=1
        and array_length(zbmaj_lbank_array, 2)=2

    .. code-block:: sql

        array_length(zbmaj_rbank_array, 1)<=4
        and array_length(zbmaj_rbank_array, 1)>=1
        and array_length(zbmaj_rbank_array, 2)=2

    .. code-block:: sql

        rlambda > 0
        and rlambda <= 1
