catchment
---------



==========  ===========  ========  ================  ================
Attribut    Type         Défaut    Contrainte        Clef Etrangère
==========  ===========  ========  ================  ================
id          serial                 primary key
name        varchar(16)  calculé   unique
geom        POLYGON                ST_IsValid(geom)
==========  ===========  ========  ================  ================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('CATCHMENT_')
