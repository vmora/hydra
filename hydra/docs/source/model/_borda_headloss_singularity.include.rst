_borda_headloss_singularity
---------------------------
type=3


====================================  =====================================  ========  ============  ================
Attribut                              Type                                   Défaut    Contrainte    Clef Etrangère
====================================  =====================================  ========  ============  ================
law_type*                             hydra_borda_headloss_singularity_type
param*                                json
full_section_discharge_for_headloss*  boolean                                't'
====================================  =====================================  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type in
        ('river', 'manhole')

Validité:

    .. code-block:: sql

        law_type is not null
        param is not null
        not model.check_on_branch_or_reach_endpoint(new.geom)

