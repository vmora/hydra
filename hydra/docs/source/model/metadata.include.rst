metadata
--------



===================  ===========  ============  ===============  =======================================================
Attribut             Type         Défaut        Contrainte       Clef Etrangère
===================  ===========  ============  ===============  =======================================================
id                   integer      1             voir ci-dessous
version              varchar(24)  '1.3'
creation_date        timestamp    current_date
trigger_coverage     boolean      't'
trigger_branch       boolean      't'
trigger_street_link  boolean      't'
configuration        integer                                     model.configuration(id) :ref:`🔗 <model-configuration>`
is_switching*        boolean      'f'
===================  ===========  ============  ===============  =======================================================

Contraintes:

    .. code-block:: sql

        unique check (id=1)
