_reservoir_rsp_hydrology_singularity
------------------------------------
type=23


==================  ==============================  ========  ============  ================
Attribut            Type                            Défaut    Contrainte    Clef Etrangère
==================  ==============================  ========  ============  ================
drainage*           integer
drainage_type*      hydra_link_type                 calculé
overflow*           integer
overflow_type*      hydra_link_type                 calculé
z_ini*              real                            calculé
zr_sr_qf_qs_array*  real[10][4]
treatment_mode*     hydra_pollution_treatment_mode  calculé
treatment_param*    json
==================  ==============================  ========  ============  ================

Champs calculés:

    treatment_mode

    .. code-block:: sql

        'residual_concentration'

    z_ini

    .. code-block:: sql

        new.zr_sr_qf_qs_array[1][1]

    overflow_type

    .. code-block:: sql

        select link_type
        from model._link
        where id=new.overflow

    drainage_type

    .. code-block:: sql

        select link_type
        from model._link
        where id=new.drainage

Contraintes:

    .. code-block:: sql

        foreign key(drainage, drainage_type)
        references model._link(id, link_type)

    .. code-block:: sql

        drainage_type='pipe' or drainage_type='connector_hydrology'

    .. code-block:: sql

        foreign key(overflow, overflow_type)
        references model._link(id, link_type)

    .. code-block:: sql

        overflow_type='pipe' or overflow_type='connector_hydrology'

Contraintes de connectivité:

    .. code-block:: sql

        node_type='manhole_hydrology'

Validité:

    .. code-block:: sql

        drainage is not null
        overflow is not null
        z_ini is not null
        zr_sr_qf_qs_array is not null 
        treatment_mode is not null
        treatment_param is not null
        array_length(zr_sr_qf_qs_array, 1)<=10
        array_length(zr_sr_qf_qs_array, 1)>=1
        array_length(zr_sr_qf_qs_array, 2)=4

