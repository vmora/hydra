_gate_link
----------
type=5


=================  ======================  ================  ============  ================
Attribut           Type                    Défaut            Contrainte    Clef Etrangère
=================  ======================  ================  ============  ================
z_invert*          real
z_ceiling*         real
width*             real
cc*                real                    .6
action_gate_type*  hydra_action_gate_type  'upward_opening'
mode_valve*        hydra_valve_mode        'no_valve'
z_gate*            real                    new.z_ceiling
v_max_cms*         real                    .2
=================  ======================  ================  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        up_type not in
        ('catchment', 'manhole_hydrology')
        and down_type not in
        ('catchment', 'manhole_hydrology')

Validité:

    .. code-block:: sql

        z_invert is not null
        z_ceiling is not null
        z_ceiling>z_invert
        width is not null
        cc is not null
        cc <=1
        cc >=0
        action_gate_type is not null
        mode_valve is not null
        z_gate is not null
        z_gate>=z_invert
        z_gate<=z_ceiling
        v_max_cms is not null

