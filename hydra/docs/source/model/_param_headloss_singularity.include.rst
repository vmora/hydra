_param_headloss_singularity
---------------------------
type=4


====================================  ===========  ========  ============  ================
Attribut                              Type         Défaut    Contrainte    Clef Etrangère
====================================  ===========  ========  ============  ================
q_dz_array*                           real[10][2]
full_section_discharge_for_headloss*  boolean      't'
====================================  ===========  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type in
        ('river', 'manhole')

Validité:

    .. code-block:: sql

        q_dz_array is not null
        array_length(q_dz_array, 1)<=10
        array_length(q_dz_array, 1)>=1
        array_length(q_dz_array, 2)=2
        not model.check_on_branch_or_reach_endpoint(new.geom)

