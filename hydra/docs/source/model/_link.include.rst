_link
-----



==============  ===============  ========  =================  ==========================================================
Attribut        Type             Défaut    Contrainte         Clef Etrangère
==============  ===============  ========  =================  ==========================================================
id              serial                     primary key
link_type       hydra_link_type
name            varchar(24)                unique
generated*      integer                    on delete cascade  model.generation_step(id) :ref:`🔗 <model-generation-step>`
up*             integer
up_type*        hydra_node_type
down*           integer
down_type*      hydra_node_type
geom            LINESTRINGZ                ST_IsValid(geom)
configuration*  json
validity*       boolean
==============  ===============  ========  =================  ==========================================================

Contraintes:

    .. code-block:: sql

        unique (id, link_type)

    .. code-block:: sql

        foreign key (up, up_type)
        references model._node(id, node_type) on delete set null

    .. code-block:: sql

        foreign key (down, down_type)
        references model._node(id, node_type) on delete set null

    .. code-block:: sql

        down_type != catchment_node_type

    .. code-block:: sql

        up is not null or up_type = elem_2d_node_type

    .. code-block:: sql

        down is not null or down_type = elem_2d_node_type
