_pipe_branch_marker_singularity
-------------------------------
type=9


==========  ======  ========  ============  ================
Attribut    Type    Défaut    Contrainte    Clef Etrangère
==========  ======  ========  ============  ================
pk0_km*     real    0
dx*         real    50
==========  ======  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type='manhole'

Validité:

    .. code-block:: sql

        pk0_km is not null
        dx is not null
        dx>=0.1

