_catchment_node
---------------
type=9


===================================  ==============================  ========  ==================  ===============================================
Attribut                             Type                            Défaut    Contrainte          Clef Etrangère
===================================  ==============================  ========  ==================  ===============================================
area_ha*                             real
rl*                                  real
slope*                               real
c_imp*                               real
netflow_type*                        hydra_netflow_type
constant_runoff*                     real
horner_ini_loss_coef*                real                            0.6
horner_recharge_coef*                real                            0.12
holtan_sat_inf_rate_mmh*             real                            2
holtan_dry_inf_rate_mmh*             real                            10
holtan_soil_storage_cap_mm*          real                            50
scs_j_mm*                            real                            150
scs_soil_drainage_time_day*          real                            10
scs_rfu_mm*                          real                            50
permeable_soil_j_mm*                 real                            150
permeable_soil_rfu_mm*               real                            50
permeable_soil_ground_max_inf_rate*  real                            1
permeable_soil_ground_lag_time_day*  real                            1
permeable_soil_coef_river_exchange*  real                            1
runoff_type*                         hydra_runoff_type
socose_tc_mn*                        real
socose_shape_param_beta*             real                            4
define_k_mn*                         real
q_limit*                             real                            9999
q0*                                  real                            0
contour*                             integer                         calculé   on delete set null  model.catchment(id) :ref:`🔗 <model-catchment>`
catchment_pollution_mode*            hydra_catchment_pollution_mode
catchment_pollution_param*           json
===================================  ==============================  ========  ==================  ===============================================

Champs calculés:

    contour

    .. code-block:: sql

        select id
        from model.catchment
        where st_intersects(geom, new.geom)

Validité:

    .. code-block:: sql

        area_ha is not null
        area_ha>0
        rl is not null
        rl>0
        slope is not null
        c_imp is not null
        c_imp>=0
        c_imp<=1
        netflow_type is not null
        netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)
        netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)
        netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)
        netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)
        netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)
        netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)
        netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)
        netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)
        netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)
        netflow_type!='permeable_soil' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)
        netflow_type!='permeable_soil' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)
        netflow_type!='permeable_soil' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)
        netflow_type!='permeable_soil' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)
        netflow_type!='permeable_soil' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)
        runoff_type is not null
        runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0)
        runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)
        runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0)
        q_limit is not null
        q0 is not null

