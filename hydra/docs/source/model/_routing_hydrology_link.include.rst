_routing_hydrology_link
-----------------------
type=18


==============  =======  ========  ============  ================================================================================
Attribut        Type     Défaut    Contrainte    Clef Etrangère
==============  =======  ========  ============  ================================================================================
cross_section*  real     1
length*         real     0.1
slope*          real     .01
hydrograph*     integer  calculé                 model._hydrograph_bc_singularity(id) :ref:`🔗 <model-hydrograph-bc-singularity>`
==============  =======  ========  ============  ================================================================================

Champs calculés:

    hydrograph

    .. code-block:: sql

        select hbc.id
        from model.hydrograph_bc_singularity as hbc
        where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1)

Contraintes de connectivité:

    .. code-block:: sql

        up_type='catchment'
        and down_type in
        ('manhole_hydrology', 'manhole', 'river', 'station', 'storage')

Validité:

    .. code-block:: sql

        cross_section is not null
        cross_section>=0
        length is not null
        length>=0
        slope is not null
        down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))

