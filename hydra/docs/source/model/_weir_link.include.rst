_weir_link
----------
type=4


==========  ======  ========  ============  ================
Attribut    Type    Défaut    Contrainte    Clef Etrangère
==========  ======  ========  ============  ================
z_invert*   real
width*      real
cc*         real    .6
z_weir*     real
v_max_cms   real    .5
==========  ======  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        up_type not in
        ('catchment', 'manhole_hydrology')
        and down_type not in
        ('catchment', 'manhole_hydrology')

Validité:

    .. code-block:: sql

        z_invert is not null 
        width is not null
        width>=0
        cc is not null
        cc<=1
        cc>=0
        v_max_cms is not null
        v_max_cms>=0

