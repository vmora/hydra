_reservoir_rs_hydrology_singularity
-----------------------------------
type=22


================  ==============================  ==================  ============  ================
Attribut          Type                            Défaut              Contrainte    Clef Etrangère
================  ==============================  ==================  ============  ================
drainage*         integer
drainage_type*    hydra_link_type                 calculé
overflow*         integer
overflow_type*    hydra_link_type                 calculé
q_drainage*       real
z_ini*            real                            new.zs_array[1][1]
zs_array*         real[10][2]
treatment_mode*   hydra_pollution_treatment_mode  calculé
treatment_param*  json
================  ==============================  ==================  ============  ================

Champs calculés:

    treatment_mode

    .. code-block:: sql

        'residual_concentration'

    overflow_type

    .. code-block:: sql

        select link_type
        from model._link
        where id=new.overflow

    drainage_type

    .. code-block:: sql

        select link_type
        from model._link
        where id=new.drainage

Contraintes:

    .. code-block:: sql

        foreign key(drainage, drainage_type)
        references model._link(id, link_type)

    .. code-block:: sql

        drainage_type='pipe' or drainage_type='connector_hydrology'

    .. code-block:: sql

        foreign key(overflow, overflow_type)
        references model._link(id, link_type)

    .. code-block:: sql

        overflow_type='pipe' or overflow_type='connector_hydrology'

Contraintes de connectivité:

    .. code-block:: sql

        node_type='manhole_hydrology'

Validité:

    .. code-block:: sql

        drainage is not null
        overflow is not null
        q_drainage is not null
        q_drainage>=0
        z_ini is not null
        zs_array is not null 
        treatment_mode is not null
        array_length(zs_array, 1)<=10
        array_length(zs_array, 1)>=1
        array_length(zs_array, 2)=2

