Model
#####

.. raw:: html

    <object data="vue_ensemble.svg" type="image/svg+xml"></object>


Entites de configuration
========================

La gestion des configurations s'appuie sur les tables ``configuration`` et ``metadata`` du modèle.

.. _model-configuration:
.. include:: configuration.include.rst

.. _model-metadata:
.. include:: metadata.include.rst

La création du modèle entraine l'insertion d'une configuration intitiale ``default`` dans la table ``configuration``, cette dernière est référencée par ``metadata`` (qui ne peut contenir qu'un enregistrement).

Lors de l'ajout d'un élément configuré (champ ``configuration`` non null), une entrée est insérée dans la table ``configuration`` si c'est nécessaire (si une configuration du même nom n'existe pas encore). Voici un exemple de valeur pour le champ ``configuration`` dans le cas d'un :ref:`model-manhole-node`:

.. code-block:: json

    {
        "default": {
                "area": 1, 
                "z_ground": 999
        }, 
        "test_config": {
                "area": 2, 
                "z_ground": 888
        }
    }


La configuration ``default`` est obligatoire, ses valeurs sont prises dans le cas où la configuration courante n'est pas disponible pour l'élément configuré. De plus, tous les champs spécifiques aux types dérivés, excepté les champs de type ``geometry``, doivent être présent (dans ce cas ``area`` et ``z_ground``).


Generation de noeuds et liaisons par lots
=========================================

La table ``generation_step`` liste les étapes de génération d'éléments, typiquement associé au maillage d'un domaine 2D. La suppression d'enregistrement dans cette table permet de supprimer (en cascade) les éléments générés à cette étape.

.. _model-generation-step:
.. include:: generation_step.include.rst


Gereneration des sections en assainisement
==========================================

Lors de l'insertion d'un `pipe_link`, soit un `river_cross_section_profile`, un enregistrement coreespondant est ajouté aux quatres tables:
- circular_cross_section
- ovoid_cross_section
- channel_cross_section
- pipe_cross_section

Dans ces tables un et un seul des champs `id_pipe_link`, `id_river_cross_section_profile_up` et `id_river_cross_section_profile_down` est non null.

La section active est renseignée par le champ `cross_section_type` (`_up` ou `_down` pour le profiles de rivière)



Domain
======

.. _model-catchment:
.. include:: catchment.include.rst
.. _model-coverage:
.. include:: coverage.include.rst
.. _model-reach:
.. include:: reach.include.rst
.. _model-station:
.. include:: station.include.rst
.. _model-domain-2d:
.. include:: domain_2d.include.rst
.. _model-branch:
.. include:: branch.include.rst

Utility
=======

.. _model-constrain:
.. include:: _constrain.include.rst

The `bottom`, `finish`, and `top` values are curvilinear coordinates on the polygon exterior ring. In case of triangle `finish=top`.

Types abstraits
===============

.. graphviz::

   digraph foo {
   }

.. _model-node:
.. include:: _node.include.rst
.. _model-link:
.. include:: _link.include.rst
.. _model-singularity:
.. include:: _singularity.include.rst

Entités de type _node
=====================

.. _model-manhole-node:
.. include:: _manhole_node.include.rst
.. _model-river-node:
.. include:: _river_node.include.rst
.. _model-station-node:
.. include:: _station_node.include.rst
.. _model-storage-node:
.. include:: _storage_node.include.rst
.. _model-elem-2d-node:
.. include:: _elem_2d_node.include.rst
.. _model-crossroad-node:
.. include:: _crossroad_node.include.rst
.. _model-manhole-hydrology-node:
.. include:: _manhole_hydrology_node.include.rst
.. _model-catchment-node:
.. include:: _catchment_node.include.rst

Entités de type _link
=====================

.. _model-deriv-pump-link:
.. include:: _deriv_pump_link.include.rst
.. _model-weir-link:
.. include:: _weir_link.include.rst
.. _model-gate-link:
.. include:: _gate_link.include.rst
.. _model-pump-link:
.. include:: _pump_link.include.rst
.. _model-regul-gate-link:
.. include:: _regul_gate_link.include.rst
.. _model-connector-link:
.. include:: _connector_link.include.rst
.. _model-borda-headloss-link:
.. include:: _borda_headloss_link.include.rst
.. _model-mesh-2d-link:
.. include:: _mesh_2d_link.include.rst
.. _model-strickler-link:
.. include:: _strickler_link.include.rst
.. _model-porous-link:
.. include:: _porous_link.include.rst
.. _model-fuse-spillway-link:
.. include:: _fuse_spillway_link.include.rst
.. _model-street-link:
.. include:: _street_link.include.rst
.. _model-routing-hydrology-link:
.. include:: _routing_hydrology_link.include.rst
.. _model-connector-hydrology-link:
.. include:: _connector_hydrology_link.include.rst
.. _model-pipe-link:
.. include:: _pipe_link.include.rst

Entités de type _singularity
============================

.. _model-gate-singularity:
.. include:: _gate_singularity.include.rst
.. _model-hydraulic-cut-singularity:
.. include:: _hydraulic_cut_singularity.include.rst
.. _model-borda-headloss-singularity:
.. include:: _borda_headloss_singularity.include.rst
.. _model-param-headloss-singularity:
.. include:: _param_headloss_singularity.include.rst
.. _model-zregul-weir-singularity:
.. include:: _zregul_weir_singularity.include.rst
.. _model-regul-sluice-gate-singularity:
.. include:: _regul_sluice_gate_singularity.include.rst
.. _model-marker-singularity:
.. include:: _marker_singularity.include.rst
.. _model-bradley-headloss-singularity:
.. include:: _bradley_headloss_singularity.include.rst
.. _model-pipe-branch-marker-singularity:
.. include:: _pipe_branch_marker_singularity.include.rst
.. _model-weir-bc-singularity:
.. include:: _weir_bc_singularity.include.rst
.. _model-strickler-bc-singularity:
.. include:: _strickler_bc_singularity.include.rst
.. _model-zq-bc-singularity:
.. include:: _zq_bc_singularity.include.rst
.. _model-tz-bc-singularity:
.. include:: _tz_bc_singularity.include.rst
.. _model-froude-bc-singularity:
.. include:: _froude_bc_singularity.include.rst
.. _model-tank-bc-singularity:
.. include:: _tank_bc_singularity.include.rst
.. _model-hydrograph-bc-singularity:
.. include:: _hydrograph_bc_singularity.include.rst
.. _model-constant-inflow-bc-singularity:
.. include:: _constant_inflow_bc_singularity.include.rst
.. _model-model-connect-bc-singularity:
.. include:: _model_connect_bc_singularity.include.rst
.. _model-qq-split-hydrology-singularity:
.. include:: _qq_split_hydrology_singularity.include.rst
.. _model-zq-split-hydrology-singularity:
.. include:: _zq_split_hydrology_singularity.include.rst
.. _model-reservoir-rs-hydrology-singularity:
.. include:: _reservoir_rs_hydrology_singularity.include.rst
.. _model-reservoir-rsp-hydrology-singularity:
.. include:: _reservoir_rsp_hydrology_singularity.include.rst
.. _model-hydrology-bc-singularity:
.. include:: _hydrology_bc_singularity.include.rst
.. _model-river-cross-section-profile:
.. include:: _river_cross_section_profile.include.rst
.. _model-river-cross-section-pl1d:
.. include:: _river_cross_section_pl1d.include.rst

Entités de type _cross_section
==============================


.. _model-valley-cross-section-geometry:
.. include:: valley_cross_section_geometry.include.rst
.. _model-closed-parametric-geometry:
.. include:: closed_parametric_geometry.include.rst
.. _model-open-parametric-geometry:
.. include:: open_parametric_geometry.include.rst
.. _model-valley-cross-section-topo-geometry:
.. include:: valley_cross_section_topo_geometry.include.rst
