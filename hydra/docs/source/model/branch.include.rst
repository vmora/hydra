branch
------



==========  ===========  ========  ================  ================
Attribut    Type         Défaut    Contrainte        Clef Etrangère
==========  ===========  ========  ================  ================
id          integer                primary key
name*       varchar(24)            unique
pk0_km      real         0
dx          real         50
geom        LINESTRINGZ            ST_IsValid(geom)
==========  ===========  ========  ================  ================
