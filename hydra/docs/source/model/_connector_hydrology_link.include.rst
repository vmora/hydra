_connector_hydrology_link
-------------------------
type=19


===========  =======  ========  ============  ================================================================================
Attribut     Type     Défaut    Contrainte    Clef Etrangère
===========  =======  ========  ============  ================================================================================
hydrograph*  integer  calculé                 model._hydrograph_bc_singularity(id) :ref:`🔗 <model-hydrograph-bc-singularity>`
===========  =======  ========  ============  ================================================================================

Champs calculés:

    hydrograph

    .. code-block:: sql

        select hbc.id
        from model.hydrograph_bc_singularity as hbc
        where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1)

Contraintes de connectivité:

    .. code-block:: sql

        up_type='manhole_hydrology'
        and down_type in
        ('manhole_hydrology', 'manhole', 'river', 'station')

Validité:

    .. code-block:: sql

        down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))

