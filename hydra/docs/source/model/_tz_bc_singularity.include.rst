_tz_bc_singularity
------------------
type=14


==================  ===========  ========  ============  ================
Attribut            Type         Défaut    Contrainte    Clef Etrangère
==================  ===========  ========  ============  ================
tz_array*           real[10][2]
cyclic              boolean      'f'
external_file_data  boolean      'f'
==================  ===========  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type!='catchment'
        and node_type!='manhole_hydrology'

Validité:

    .. code-block:: sql

        external_file_data or tz_array is not null
        external_file_data or array_length(tz_array, 1)<=10
        external_file_data or array_length(tz_array, 1)>=1
        external_file_data or array_length(tz_array, 2)=2

