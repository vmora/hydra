_river_cross_section_profile
----------------------------



===========================  ========================  ========  ============  =================================================================================================
Attribut                     Type                      Défaut    Contrainte    Clef Etrangère
===========================  ========================  ========  ============  =================================================================================================
id                           integer                             unique        model._river_node(id) :ref:`🔗 <model-river-node>`
name                         varchar(24)                         unique
z_invert_up*                 real
z_invert_down*               real
type_cross_section_up*       hydra_cross_section_type  'valley'
type_cross_section_down*     hydra_cross_section_type  'valley'
up_rk*                       real
up_rk_maj*                   real
up_sinuosity*                real
up_circular_diameter*        real
up_ovoid_height*             real
up_ovoid_top_diameter*       real
up_ovoid_invert_diameter*    real
up_cp_geom*                  integer                                           model.closed_parametric_geometry(id) :ref:`🔗 <model-closed-parametric-geometry>`
up_op_geom*                  integer                                           model.open_parametric_geometry(id) :ref:`🔗 <model-open-parametric-geometry>`
up_vcs_geom*                 integer                                           model.valley_cross_section_geometry(id) :ref:`🔗 <model-valley-cross-section-geometry>`
up_vcs_topo_geom*            integer                                           model.valley_cross_section_topo_geometry(id) :ref:`🔗 <model-valley-cross-section-topo-geometry>`
down_rk*                     real
down_rk_maj*                 real
down_sinuosity*              real
down_circular_diameter*      real
down_ovoid_height*           real
down_ovoid_top_diameter*     real
down_ovoid_invert_diameter*  real
down_cp_geom*                integer                                           model.closed_parametric_geometry(id) :ref:`🔗 <model-closed-parametric-geometry>`
down_op_geom*                integer                                           model.open_parametric_geometry(id) :ref:`🔗 <model-open-parametric-geometry>`
down_vcs_geom*               integer                                           model.valley_cross_section_geometry(id) :ref:`🔗 <model-valley-cross-section-geometry>`
down_vcs_topo_geom*          integer                                           model.valley_cross_section_topo_geometry(id) :ref:`🔗 <model-valley-cross-section-topo-geometry>`
configuration*               json
validity*                    bool
===========================  ========================  ========  ============  =================================================================================================
