_model_connect_bc_singularity
-----------------------------
type=19


=============  ========================  ============  ============  ================
Attribut       Type                      Défaut        Contrainte    Clef Etrangère
=============  ========================  ============  ============  ================
cascade_mode*  hydra_model_connect_mode  'hydrograph'
zq_array*      real[100][2]
=============  ========================  ============  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type!='catchment'
        and node_type!='manhole_hydrology'

Validité:

    .. code-block:: sql

        cascade_mode is not null
        cascade_mode='hydrograph' or zq_array is not null
        array_length(zq_array, 1)<=100
        array_length(zq_array, 1)>=1
        array_length(zq_array, 2)=2

