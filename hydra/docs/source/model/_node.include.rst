_node
-----



==============  ===============  ========  =================  ==========================================================
Attribut        Type             Défaut    Contrainte         Clef Etrangère
==============  ===============  ========  =================  ==========================================================
id              serial                     primary key
node_type       hydra_node_type
name            varchar(24)                unique
geom            POINTZ                     ST_IsValid(geom)
generated*      integer                    on delete cascade  model.generation_step(id) :ref:`🔗 <model-generation-step>`
configuration*  json
validity*       boolean
==============  ===============  ========  =================  ==========================================================

Contraintes:

    .. code-block:: sql

        unique (id, node_type)
