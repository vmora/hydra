_strickler_link
---------------
type=12


==========  ===========  ===================  ============  ================
Attribut    Type         Défaut               Contrainte    Clef Etrangère
==========  ===========  ===================  ============  ================
z_crest1*   real
width1*     real
length*     real         ST_Length(new.geom)
rk*         real         12
z_crest2*   real         new.z_crest1+0.001
width2*     real         new.width1+0.001
border*     LINESTRINGZ
==========  ===========  ===================  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        up_type not in
        ('catchment', 'manhole_hydrology')
        and down_type not in
        ('catchment', 'manhole_hydrology')

Validité:

    .. code-block:: sql

        z_crest1 is not null
        width1 is not null
        width1>0
        length is not null
        length>0
        rk is not null
        rk>=0
        z_crest2 is not null
        z_crest2>z_crest1
        width2 is not null
        width2>width1

