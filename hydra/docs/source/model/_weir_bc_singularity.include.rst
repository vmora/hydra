_weir_bc_singularity
--------------------
type=11


==========  ======  ========  ============  ================
Attribut    Type    Défaut    Contrainte    Clef Etrangère
==========  ======  ========  ============  ================
z_weir*     real
width*      real
cc*         real    .6
==========  ======  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type!='catchment'
        and node_type!='manhole_hydrology'

Validité:

    .. code-block:: sql

        z_weir is not null
        width is not null
        width>=0
        cc is not null
        cc<=1
        cc>=0.

