_manhole_hydrology_node
-----------------------
type=8


==========  ======  ========  ============  ================
Attribut    Type    Défaut    Contrainte    Clef Etrangère
==========  ======  ========  ============  ================
area*       real    1
z_ground*   real    calculé
==========  ======  ========  ============  ================

Champs calculés:

    z_ground

    .. code-block:: sql

        select project.altitude(new.geom)

Validité:

    .. code-block:: sql

        area is not null
        (area > 0)
        z_ground is not null

