_river_cross_section_pl1d
-------------------------



==========  ===========  ========  =================  ==========================================================
Attribut    Type         Défaut    Contrainte         Clef Etrangère
==========  ===========  ========  =================  ==========================================================
id          integer                unique             model._river_node(id) :ref:`🔗 <model-river-node>`
name        varchar(24)            unique
profile     LINESTRINGZ            voir ci-dessous
generated*  integer                on delete cascade  model.generation_step(id) :ref:`🔗 <model-generation-step>`
==========  ===========  ========  =================  ==========================================================

Contraintes:

    .. code-block:: sql

        ST_NPoints(profile)=4
        and ST_IsValid(profile)
