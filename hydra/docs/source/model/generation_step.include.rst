generation_step
---------------



============  ===========  ========  ============  ================
Attribut      Type         Défaut    Contrainte    Clef Etrangère
============  ===========  ========  ============  ================
id            serial                 primary key
name          varchar(24)  calculé   unique
description*  varchar
============  ===========  ========  ============  ================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('GEN_STEP_')
