_crossroad_node
---------------
type=6


==========  ======  ========  ============  ================
Attribut    Type    Défaut    Contrainte    Clef Etrangère
==========  ======  ========  ============  ================
area*       real    1
z_ground*   real    calculé
h*          real    0
==========  ======  ========  ============  ================

Champs calculés:

    z_ground

    .. code-block:: sql

        select project.altitude(new.geom)

Validité:

    .. code-block:: sql

        area is not null
        (area > 0)
        z_ground is not null
        h is not null
        h>=0

