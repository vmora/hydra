_mesh_2d_link
-------------
type=11


=========================  ===========  ========  ============  ================
Attribut                   Type         Défaut    Contrainte    Clef Etrangère
=========================  ===========  ========  ============  ================
z_invert*                  real
lateral_contraction_coef*  real         1
border*                    LINESTRINGZ  calculé
=========================  ===========  ========  ============  ================

Champs calculés:

    border

    .. code-block:: sql

        model.inter_pave_border_fct(up_, down_)

Contraintes de connectivité:

    .. code-block:: sql

        up_type='elem_2d'
        and down_type='elem_2d'

Validité:

    .. code-block:: sql

        z_invert is not null
        lateral_contraction_coef is not null
        lateral_contraction_coef<=1
        lateral_contraction_coef>=0
        ST_NPoints(border)=2
        ST_IsValid(border)

