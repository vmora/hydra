_regul_sluice_gate_singularity
------------------------------
type=6


====================================  ======================  ===================  ==================  ======================================
Attribut                              Type                    Défaut               Contrainte          Clef Etrangère
====================================  ======================  ===================  ==================  ======================================
z_invert*                             real
z_ceiling*                            real
width*                                real
cc*                                   real                    .6
action_gate_type*                     hydra_action_gate_type  'upward_opening'
z_invert_stop*                        real                    new.z_invert
z_ceiling_stop*                       real                    new.z_ceiling
v_max_cms*                            real                    .5
dt_regul_hr*                          real                    0
mode_regul*                           hydra_gate_mode_regul   'elevation'
z_control_node*                       integer                                      on delete set null  model._node(id) :ref:`🔗 <model-node>`
z_pid_array*                          real[3]                 '{1, 0, 0}'::real[]
z_tz_array*                           real[10][2]
q_z_crit*                             real
q_tq_array*                           real[10][2]
nr_z_gate*                            real                    new.z_ceiling
full_section_discharge_for_headloss*  boolean                 't'
====================================  ======================  ===================  ==================  ======================================

Contraintes de connectivité:

    .. code-block:: sql

        node_type in
        ('river', 'manhole')

Validité:

    .. code-block:: sql

        z_invert is not null
        z_ceiling is not null
        width is not null
        width>=0
        cc is not null
        cc<=1
        cc>=0
        action_gate_type is not null
        z_invert_stop is not null
        z_ceiling_stop is not null
        v_max_cms is not null
        v_max_cms>=0
        dt_regul_hr is not null
        mode_regul!='elevation' or z_control_node is not null
        mode_regul!='elevation' or z_pid_array is not null
        mode_regul!='elevation' or z_tz_array is not null
        mode_regul!='elevation' or array_length(z_tz_array, 1)<=10
        mode_regul!='elevation' or array_length(z_tz_array, 1)>=1
        mode_regul!='elevation' or array_length(z_tz_array, 2)=2
        mode_regul!='discharge' or q_z_crit is not null
        mode_regul!='discharge' or array_length(q_tq_array, 1)<=10
        mode_regul!='discharge' or array_length(q_tq_array, 1)>=1
        mode_regul!='discharge' or array_length(q_tq_array, 2)=2
        mode_regul!='no_regulation' or nr_z_gate is not null

