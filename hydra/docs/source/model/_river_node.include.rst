_river_node
-----------
type=2


==========  =======  ========  ============  =======================================
Attribut    Type     Défaut    Contrainte    Clef Etrangère
==========  =======  ========  ============  =======================================
reach*      integer  calculé                 model.reach(id) :ref:`🔗 <model-reach>`
z_ground*   real     calculé
area*       real     1
==========  =======  ========  ============  =======================================

Champs calculés:

    z_ground

    .. code-block:: sql

        select project.altitude(new.geom)

    reach

    .. code-block:: sql

        select id
        from model.reach
        where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) asc limit 1

Validité:

    .. code-block:: sql

        z_ground is not null
        area is not null
        area>0
        reach is not null
        (select not exists(select 1 from model.station where st_intersects(geom, new.geom)))

