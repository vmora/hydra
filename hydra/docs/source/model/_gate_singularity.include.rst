_gate_singularity
-----------------
type=1


====================================  ======================  ================  ============  ================
Attribut                              Type                    Défaut            Contrainte    Clef Etrangère
====================================  ======================  ================  ============  ================
z_invert*                             real
z_ceiling*                            real
width*                                real
cc*                                   real                    .6
action_gate_type*                     hydra_action_gate_type  'upward_opening'
mode_valve*                           hydra_valve_mode        'no_valve'
z_gate*                               real                    new.z_ceiling
v_max_cms*                            real                    .2
full_section_discharge_for_headloss*  boolean                 't'
====================================  ======================  ================  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type in
        ('river', 'manhole')

Validité:

    .. code-block:: sql

        z_invert is not null
        z_ceiling is not null
        z_ceiling>z_invert
        width is not null
        cc is not null
        cc <=1
        cc >=0
        action_gate_type is not null
        mode_valve is not null
        z_gate is not null
        z_gate>=z_invert
        z_gate<=z_ceiling
        v_max_cms is not null
        not model.check_on_branch_or_reach_endpoint(new.geom)

