_network_overflow_link
----------------------
type=22


===========  ======  ========  ============  ================
Attribut     Type    Défaut    Contrainte    Clef Etrangère
===========  ======  ========  ============  ================
z_overflow*  real    calculé
area*        real    1
===========  ======  ========  ============  ================

Champs calculés:

    z_overflow

    .. code-block:: sql

        st_z(st_endpoint(new.geom))

Contraintes de connectivité:

    .. code-block:: sql

        up_type='manhole'
        and down_type in
        ('crossroad', 'elem_2d', 'storage', 'river')

Validité:

    .. code-block:: sql

        z_overflow is not null
        area is not null
        area>=0

