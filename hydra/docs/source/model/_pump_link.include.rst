_pump_link
----------
type=6


=============  ===============  ========  ============  ================
Attribut       Type             Défaut    Contrainte    Clef Etrangère
=============  ===============  ========  ============  ================
npump*         integer
zregul_array*  real[10][2]
hq_array*      real[10][10][2]
=============  ===============  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        up_type not in
        ('catchment', 'manhole_hydrology')
        and down_type not in
        ('catchment', 'manhole_hydrology')

Validité:

    .. code-block:: sql

        npump is not null
        npump<=10
        npump>=1
        zregul_array is not null
        hq_array is not null
        array_length(zregul_array, 1)=npump
        array_length(zregul_array, 2)=2
        array_length(hq_array, 1)=npump
        array_length(hq_array, 2)<=10
        array_length(hq_array, 2)>=1
        array_length(hq_array, 3)=2

