_deriv_pump_link
----------------
type=3


==========  ===========  ========  ============  ================
Attribut    Type         Défaut    Contrainte    Clef Etrangère
==========  ===========  ========  ============  ================
q_pump*     real
qz_array*   real[10][2]
==========  ===========  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        up_type not in
        ('catchment', 'manhole_hydrology')
        and down_type not in
        ('catchment', 'manhole_hydrology')

Validité:

    .. code-block:: sql

        q_pump is not null
        q_pump>= 0
        qz_array is not null
        array_length(qz_array, 1)<=10
        array_length(qz_array, 1)>=1
        array_length(qz_array, 2)=2

