_tank_bc_singularity
--------------------
type=16


==========  ===========  ========  ============  ================
Attribut    Type         Défaut    Contrainte    Clef Etrangère
==========  ===========  ========  ============  ================
zs_array*   real[10][2]
zini*       real
==========  ===========  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type!='catchment'
        and node_type!='manhole_hydrology'

Validité:

    .. code-block:: sql

        zs_array is not null 
        zini is not null
        array_length(zs_array, 1)<=10
        array_length(zs_array, 1)>=1
        array_length(zs_array, 2)=2

