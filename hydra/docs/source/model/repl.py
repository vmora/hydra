# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import re
import sys

for file_ in sys.argv[1:]:
    lines = open(file_).readlines()
    lenmax = max([len(l) for l in lines if l[-1]!='='])
    for i in range(len(lines)):
        m = re.match(r'^[= ]+$', lines[i])
        if m:
            lines[i] = lines[i].rstrip()+'='*(lenmax - len(lines[i]) - 1)+'\n'
        #m = re.match(r'.*(<[^>]+>)', lines[i])
        #if m:
        #    newref = re.sub(r'[\._]+', '-', m.group(1))
        #    lines[i] = lines[i].replace(m.group(1), newref)
        #    print lines[i]
    #print "".join(lines)
    open(file_, 'w').write("".join(lines))


