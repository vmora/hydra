_constrain
----------



================  ====================  ========  ================  ================
Attribut          Type                  Défaut    Contrainte        Clef Etrangère
================  ====================  ========  ================  ================
id                serial                          primary key
name              varchar(16)                     unique
geom              LINESTRINGZ                     ST_IsValid(geom)
discretized       LINESTRINGZ                     ST_IsValid(geom)
elem_length       real                  100
constrain_type*   hydra_constrain_type
link_attributes*  json
================  ====================  ========  ================  ================
