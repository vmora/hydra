_bradley_headloss_singularity
-----------------------------
type=8


==============  ===================  ==========  ============  ================
Attribut        Type                 Défaut      Contrainte    Clef Etrangère
==============  ===================  ==========  ============  ================
d_abutment_l*   real
d_abutment_r*   real
abutment_type*  hydra_abutment_type  'angle_90'
zw_array*       real[10][2]
z_ceiling*      real                 calculé
==============  ===================  ==========  ============  ================

Champs calculés:

    z_ceiling

    .. code-block:: sql

        new.zw_array[array_length(new.zw_array, 1)][1]

Contraintes de connectivité:

    .. code-block:: sql

        node_type='river'

Validité:

    .. code-block:: sql

        d_abutment_l is not null
        d_abutment_r is not null
        abutment_type is not null
        zw_array is not null 
        z_ceiling is not null
        array_length(zw_array, 1)<=10
        array_length(zw_array, 1)>=1
        array_length(zw_array, 2)=2
        not model.check_on_branch_or_reach_endpoint(new.geom)

