_storage_node
-------------
type=4


==========  ===========  ========  ===============  =============================================
Attribut    Type         Défaut    Contrainte       Clef Etrangère
==========  ===========  ========  ===============  =============================================
zs_array*   real[10][2]
zini*       real
contour*    integer      calculé   voir ci-dessous  model.coverage(id) :ref:`🔗 <model-coverage>`
==========  ===========  ========  ===============  =============================================

Champs calculés:

    contour

    .. code-block:: sql

        select id
        from model.coverage
        where st_intersects(geom, new.geom)

Contraintes:

    .. code-block:: sql

        unique  on delete set null

Validité:

    .. code-block:: sql

        zs_array is not null 
        zini is not null
        array_length(zs_array, 1)<=10 
        array_length(zs_array, 1) >=1
        array_length(zs_array, 2)=2

