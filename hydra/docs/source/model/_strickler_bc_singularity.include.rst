_strickler_bc_singularity
-------------------------
type=12


==========  ======  ========  ============  ================
Attribut    Type    Défaut    Contrainte    Clef Etrangère
==========  ======  ========  ============  ================
slope*      real
k*          real
width*      real
==========  ======  ========  ============  ================

Contraintes de connectivité:

    .. code-block:: sql

        node_type!='catchment'
        and node_type!='manhole_hydrology'

Validité:

    .. code-block:: sql

        slope is not null
        k is not null
        k>0
        width is not null
        width>=0

