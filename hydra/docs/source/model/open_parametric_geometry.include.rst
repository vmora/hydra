open_parametric_geometry
------------------------



============  ===========  ========  ===============  ================
Attribut      Type         Défaut    Contrainte       Clef Etrangère
============  ===========  ========  ===============  ================
id            serial                 primary key
name          varchar(24)  calculé   unique
zbmin_array*  real[20][2]            voir ci-dessous
============  ===========  ========  ===============  ================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('OP_')

Contraintes:

    .. code-block:: sql

        array_length(zbmin_array, 1)<=20
        and array_length(zbmin_array, 1)>=1
        and array_length(zbmin_array, 2)=2
