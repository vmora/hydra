fuse_spillway_break_mode
------------------------



=============  ====  ====================
name           id    description
=============  ====  ====================
time_critical  2     Break on time
none           0     No break
zw_critical    1     Break on water level
=============  ====  ====================
