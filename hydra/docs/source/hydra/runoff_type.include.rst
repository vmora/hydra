runoff_type
-----------



===========  ====  =================================
name         id    description
===========  ====  =================================
k_desbordes  1     k-Desbordes  and linear reservoir
passini      2     k-Passini  and linear reservoir
giandotti    3     k-Giandotti and linear reservoir
socose       4     Tc imposed and Socose
define_k     5     Defined K and linear reservoir
===========  ====  =================================
