netflow_type
------------



===============  ====  ===============
name             id    description
===============  ====  ===============
horner           2     Horner
constant_runoff  1     Constant runoff
holtan           3     Holtan
permeable_soil   5     Permeable soil
scs              4     SCS
===============  ====  ===============
