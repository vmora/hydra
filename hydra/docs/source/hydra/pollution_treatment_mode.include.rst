pollution_treatment_mode
------------------------



======================  ====  ======================
name                    id    description
======================  ====  ======================
residual_concentration  1     Residual concentration
efficiency              2     Efficiency
======================  ====  ======================

residual_concentration (pollution_treatment_mode)
-------------------------------------------------

=============  ======  =========  ===========  =======
name           type    default    constrain    place
=============  ======  =========  ===========  =======
mes_conc_mgl   real    0                       1
dbo5_conc_mgl  real    0                       2
ntk_conc_mgl   real    0                       4
dco_conc_mgl   real    0                       3
=============  ======  =========  ===========  =======

efficiency (pollution_treatment_mode)
-------------------------------------

======================  ======  =========  ===========  =======
name                    type    default    constrain    place
======================  ======  =========  ===========  =======
mes_efficiency_factor   real    0                       1
dbo5_efficiency_factor  real    0                       2
dco_efficiency_factor   real    0                       3
ntk_efficiency_factor   real    0                       4
======================  ======  =========  ===========  =======
