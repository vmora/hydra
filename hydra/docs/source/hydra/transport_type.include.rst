transport_type
--------------



============  ====  ==========================================
name          id    description
============  ====  ==========================================
no_transport  1     None
quality       3     Quality
sediment      4     Sediment transport
pollution     2     Pollution generation and network transport
============  ====  ==========================================
