model_connect_mode
------------------



=======================  ====  ==========================
name                     id    description
=======================  ====  ==========================
hydrograph               1     Hydrograph
zq_downstream_condition  2     Downstream condition: Q(Z)
=======================  ====  ==========================
