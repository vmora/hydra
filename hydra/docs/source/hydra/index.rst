Hydra
#####

Des types énumérés postgres sont définis pour les types listés, des attributs suppélementaires sont disponibles (fichier hydra.instances.json) ils sont listés ici.

.. _hydra-node-type:
.. include:: node_type.include.rst
.. _hydra-cross-section-type:
.. include:: cross_section_type.include.rst
.. _hydra-link-type:
.. include:: link_type.include.rst
.. _hydra-singularity-type:
.. include:: singularity_type.include.rst
.. _hydra-netflow-type:
.. include:: netflow_type.include.rst
.. _hydra-runoff-type:
.. include:: runoff_type.include.rst
.. _hydra-valve-mode:
.. include:: valve_mode.include.rst
.. _hydra-action-gate-type:
.. include:: action_gate_type.include.rst
.. _hydra-borda-headloss-link-type:
.. include:: borda_headloss_link_type.include.rst
.. _hydra-fuse-spillway-break-mode:
.. include:: fuse_spillway_break_mode.include.rst
.. _hydra-borda-headloss-singularity-type:
.. include:: borda_headloss_singularity_type.include.rst
.. _hydra-weir-mode-regul:
.. include:: weir_mode_regul.include.rst
.. _hydra-weir-mode-reoxy:
.. include:: weir_mode_reoxy.include.rst
.. _hydra-abutment-type:
.. include:: abutment_type.include.rst
.. _hydra-pollution-quality-mode:
.. include:: pollution_quality_mode.include.rst
.. _hydra-model-connect-mode:
.. include:: model_connect_mode.include.rst
.. _hydra-split-law-type:
.. include:: split_law_type.include.rst
.. _hydra-pollution-treatment-mode:
.. include:: pollution_treatment_mode.include.rst
.. _hydra-rainfall-type:
.. include:: rainfall_type.include.rst
.. _hydra-land-use-type:
.. include:: land_use_type.include.rst
.. _hydra-computation-mode:
.. include:: computation_mode.include.rst
.. _hydra-model-connection-settings:
.. include:: model_connection_settings.include.rst
.. _hydra-transport-type:
.. include:: transport_type.include.rst
.. _hydra-type-sor1:
.. include:: type_sor1.include.rst
.. _hydra-quality-type:
.. include:: quality_type.include.rst
.. _hydra-rerouting-option:
.. include:: rerouting_option.include.rst
.. _hydra-type-domain:
.. include:: type_domain.include.rst
