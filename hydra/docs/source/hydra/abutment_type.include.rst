abutment_type
-------------



========  ====  =============
name      id    description
========  ====  =============
rounded   3     Rounded edge
angle_45  2     Angle 45
angle_90  1     Angle 90
========  ====  =============
