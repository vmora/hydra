borda_headloss_singularity_type
-------------------------------



===========================  ====  ===========================
name                         id    description
===========================  ====  ===========================
enlargment_with_transition   11    Enlargment with transition
screen_normal_to_flow        14    Screen normal to flow
sharp_angle_bend             13    Sharp angle bend
circular_bend                12    Circular bend
q                            1     Law dz=KQ2
contraction_with_transition  10    Contraction with transition
friction                     16    Friction
v                            2     Law dE=(K/2g).(Q2/S2)
sharp_transition_geometry    9     Sharp geometry transition
parametric                   17    Parametric
screen_oblique_to_flow       15    Screen oblique to flow
sharp_bend_rectangular       18    Sharp bend rectangular
===========================  ====  ===========================

enlargment_with_transition (borda_headloss_singularity_type)
------------------------------------------------------------

=================  ==================================================================  =========  ===========  =======
name               type                                                                default    constrain    place
=================  ==================================================================  =========  ===========  =======
half_angle         real                                                                                        2
transition_option  enum('conic', 'placeholder_do_not_display', 'rectangular', 'flat')                          1
=================  ==================================================================  =========  ===========  =======

screen_normal_to_flow (borda_headloss_singularity_type)
-------------------------------------------------------

=================  ===============================================================================================================================================================================  =========  ===========  =======
name               type                                                                                                                                                                             default    constrain    place
=================  ===============================================================================================================================================================================  =========  ===========  =======
grid_shape         enum('rectangular_edge', 'upstream_round_edge', 'up_and_downstream_round_edge', 'sharp_large_downstream_edge', 'sharp_narrow_downstream_edge', 'pointed_edge', 'circular_edge')                          4
blocage_coef       real                                                                                                                                                                                                     1
v_angle            real                                                                                                                                                                                                     2
deposition_option  enum('modern_automatic_grid', 'old_automatic', 'manual')                                                                                                                                                 3
=================  ===============================================================================================================================================================================  =========  ===========  =======

sharp_angle_bend (borda_headloss_singularity_type)
--------------------------------------------------

====================  =======================  =========  ===========  =======
name                  type                     default    constrain    place
====================  =======================  =========  ===========  =======
wall_friction_option  enum('smooth', 'rough')                          4
angle                 real                                             1
connector_no          integer                                          2
ls                    real                                             3
====================  =======================  =========  ===========  =======

circular_bend (borda_headloss_singularity_type)
-----------------------------------------------

====================  =======================  =========  ===========  =======
name                  type                     default    constrain    place
====================  =======================  =========  ===========  =======
curve_radius          real                                             1
curve_angle           real                                             2
wall_friction_option  enum('smooth', 'rough')                          3
====================  =======================  =========  ===========  =======

q (borda_headloss_singularity_type)
-----------------------------------

========  ======  =========  ===========  =======
name      type    default    constrain    place
========  ======  =========  ===========  =======
coef_kq2  real                            1
========  ======  =========  ===========  =======

contraction_with_transition (borda_headloss_singularity_type)
-------------------------------------------------------------

=================  ====================================  =========  ===========  =======
name               type                                  default    constrain    place
=================  ====================================  =========  ===========  =======
half_angle         real                                                          2
transition_option  enum('conic', 'rectangular', 'flat')                          1
=================  ====================================  =========  ===========  =======

friction (borda_headloss_singularity_type)
------------------------------------------

======  ======  =========  ===========  =======
name    type    default    constrain    place
======  ======  =========  ===========  =======
ks      real                            2
length  real                            1
======  ======  =========  ===========  =======

v (borda_headloss_singularity_type)
-----------------------------------

============  ==============================  =========  ===========  =======
name          type                            default    constrain    place
============  ==============================  =========  ===========  =======
reference_cs  enum('upstream', 'downstream')                          1
coef_kv_12    real                                                    2
coef_kv_21    real                                                    3
============  ==============================  =========  ===========  =======

sharp_transition_geometry (borda_headloss_singularity_type)
-----------------------------------------------------------

==============  =================  =========  ===========  =======
name            type               default    constrain    place
==============  =================  =========  ===========  =======
middle_option   enum('no', 'yes')                          1
middle_cs_area  real                                       2
==============  =================  =========  ===========  =======

parametric (borda_headloss_singularity_type)
--------------------------------------------

========  ===========  =========  ===========  =======
name      type         default    constrain    place
========  ===========  =========  ===========  =======
qh_array  real[10][2]                          1
========  ===========  =========  ===========  =======

screen_oblique_to_flow (borda_headloss_singularity_type)
--------------------------------------------------------

==================  ======================================================================================================================================================================================================================================================================================================  =========  ===========  =======
name                type                                                                                                                                                                                                                                                                                                    default    constrain    place
==================  ======================================================================================================================================================================================================================================================================================================  =========  ===========  =======
grid_shape_oblique  enum('rectangular_edge', 'upstream_round_edge', 'up_and_downstream_round_edge', 'sharp_large_downstream_edge', 'sharp_narrow_downstream_edge', 'pointed_edge', 'placeholder_do_not_display', 'thick_rectangular_edge', 'thin_rectangular_edge', 'long_rectangular_edge', 'very_long_rectangular_edge')                          3
blocage_coef        real                                                                                                                                                                                                                                                                                                                            1
v_angle             real                                                                                                                                                                                                                                                                                                                            2
==================  ======================================================================================================================================================================================================================================================================================================  =========  ===========  =======

sharp_bend_rectangular (borda_headloss_singularity_type)
--------------------------------------------------------

==========  ===========================================================================  =========  ===========  =======
name        type                                                                         default    constrain    place
==========  ===========================================================================  =========  ===========  =======
bend_shape  enum('elbow_90_deg', 'z_shaped_elbow', 'u_shaped_elbow', 'two_elbows_in_d')                          1
param4      real                                                                                                 5
param3      real                                                                                                 4
param2      real                                                                                                 3
param1      real                                                                                                 2
==========  ===========================================================================  =========  ===========  =======
