link_type
---------



===================  ====  ==============  =============  ================
name                 id    abbreviation    class_hydra    group_hydra_no
===================  ====  ==============  =============  ================
pipe                 20    TRC             HYDROL         5
strickler            12    LSTK            LI             1
fuse_spillway        15    LDVF            LI             7
routing_hydrology    18    ROUT            HYDROL         3
deriv_pump           3     QDP             LI             13
porous               13    LPOR            LI             2
weir                 4     LDEV            LI             3
borda_headloss       10    QMK             LI             20
connector            9     QMTR            LI             19
pump                 6     QMP             LI             10
street               16    LRUE            LI             21
mesh_2d              11    LPAV            LI             5
gate                 5     LORF            LI             4
overflow             21    OFL             LI             32
regul_gate           7     RG              LI             8
connector_hydrology  19    TRH             HYDROL         4
network_overflow     22    NOV             LI             33
===================  ====  ==============  =============  ================
