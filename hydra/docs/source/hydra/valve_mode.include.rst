valve_mode
----------



==================  ====  =================================
name                id    description
==================  ====  =================================
one_way_upstream    3     one way valve (toward upstream)
one_way_downstream  2     one way valve (toward downstream)
no_valve            1     no valve
==================  ====  =================================
