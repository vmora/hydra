weir_mode_reoxy
---------------



=======  ====  =============
name     id    description
=======  ====  =============
r15      2     R15 law
gameson  1     Gameson law
=======  ====  =============

r15 (weir_mode_reoxy)
---------------------

========================  ======  =========  ===========  =======
name                      type    default    constrain    place
========================  ======  =========  ===========  =======
reoxygen_coef             real                            1
temperature_correct_coef  real                            2
========================  ======  =========  ===========  =======

gameson (weir_mode_reoxy)
-------------------------

==============  ======  =========  ===========  =======
name            type    default    constrain    place
==============  ======  =========  ===========  =======
waterfall_coef  real    1                       1
adjust_coef     real    1                       2
pollution_coef  real    1                       3
==============  ======  =========  ===========  =======
