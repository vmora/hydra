quality_type
------------



============================  ====  =======================================================================
name                          id    description
============================  ====  =======================================================================
physico_chemical              1     Physico chemical: 4 inter related parameters: DBO, NH4, NO3, O2
quality_parameter_selection   2     Quality parameter selection: no chemical interaction betwwen parameters
suspended sediment transport  3     Suspended sediment transport
============================  ====  =======================================================================
