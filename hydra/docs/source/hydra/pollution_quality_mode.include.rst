pollution_quality_mode
----------------------



=========  ====
name       id
=========  ====
quality1   2
quality3   4
quality2   3
pollution  1
=========  ====

quality1 (pollution_quality_mode)
---------------------------------

====================  ======  =========  ===========  =======
name                  type    default    constrain    place
====================  ======  =========  ===========  =======
runoff_flow_dbo5_mgl  real    0                       5
dry_flow_o2_mgl       real    0                       4
dbo5_dcant_fraction   real    0                       10
dry_flow_no3_mgl      real    0                       3
dry_flow_dbo5_mgl     real    0                       1
dbou_dbo5_ratio       real    1.2                     9
dry_flow_nh4_mgl      real    0                       2
runoff_flow_o2_mgl    real    0                       8
runoff_flow_no3_mgl   real    0                       7
runoff_flow_nh4_mgl   real    0                       6
====================  ======  =========  ===========  =======

quality3 (pollution_quality_mode)
---------------------------------

======  ======  =========  ===========  =======
name    type    default    constrain    place
======  ======  =========  ===========  =======
dmes    real    0                       1
======  ======  =========  ===========  =======

quality2 (pollution_quality_mode)
---------------------------------

===================  ======  =========  ===========  =======
name                 type    default    constrain    place
===================  ======  =========  ===========  =======
dry_flow_ad4_mgl     real    0                       4
dry_flow_ad1_mgl     real    0                       1
runoff_flow_ad3_mgl  real    0                       7
runoff_flow_ad2_mgl  real    0                       6
dry_flow_ad3_mgl     real    0                       3
dry_flow_ad2_mgl     real    0                       2
runoff_ad1_mgl       real    0                       5
runoff_flow_ad4_mgl  real    0                       8
===================  ======  =========  ===========  =======

pollution (pollution_quality_mode)
----------------------------------

====================  ======  =========  ===========  =======
name                  type    default    constrain    place
====================  ======  =========  ===========  =======
runoff_flow_dbo5_mgl  real    0                       6
dry_flow_dbo5_mgl     real    0                       2
dry_flow_mes_mgl      real    0                       1
dry_flow_ntk_mgl      real    0                       4
dry_flow_dco_mgl      real    0                       3
runoff_mes_mgl        real    0                       5
runoff_flow_ntk_mgl   real    0                       8
runoff_flow_dco_mgl   real    0                       7
====================  ======  =========  ===========  =======
