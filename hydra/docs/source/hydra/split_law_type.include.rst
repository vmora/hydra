split_law_type
--------------



========  ====
name      id
========  ====
stickler  3
gate      2
zq        4
weir      1
========  ====

stickler (split_law_type)
-------------------------

======  ======  =========  ===========  =======
name    type    default    constrain    place
======  ======  =========  ===========  =======
======  ======  =========  ===========  =======

gate (split_law_type)
---------------------

=============  ======  =========  ===========  =======
name           type    default    constrain    place
=============  ======  =========  ===========  =======
gate_diameter  real                            2
gate_z_invert  real                            1
gate_cc        real                            3
=============  ======  =========  ===========  =======

zq (split_law_type)
-------------------

========  ========  =========  ===========  =======
name      type      default    constrain    place
========  ========  =========  ===========  =======
zq_array  real[][]  [[0, 0]]                1
========  ========  =========  ===========  =======

weir (split_law_type)
---------------------

=============  ======  =========  ===========  =======
name           type    default    constrain    place
=============  ======  =========  ===========  =======
weir_z_invert  real                            1
weir_cc        real                            3
weir_width     real                            2
=============  ======  =========  ===========  =======
