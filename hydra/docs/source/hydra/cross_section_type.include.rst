cross_section_type
------------------



========  ====  ==============  ================
name      id    abbreviation    group_hydra_no
========  ====  ==============  ================
pipe      3     PF              3
ovoid     5     OV              5
valley    2     VL              2
channel   4     PO              4
circular  6     CI              6
========  ====  ==============  ================
