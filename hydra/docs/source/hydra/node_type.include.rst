node_type
---------



=================  ====  ==============  =============  ================
name               id    abbreviation    class_hydra    group_hydra_no
=================  ====  ==============  =============  ================
river              2     NODR            LI             29
manhole_hydrology  8     NODH            HYDROL         1
storage            4     CAS             EL             37
catchment          9     BC              HYDROL         2
station            3     NODS            EL             35
manhole            1     NODA            LI             28
elem_2d            5     PAV             EL             36
crossroad          6     CAR             EL             38
=================  ====  ==============  =============  ================
