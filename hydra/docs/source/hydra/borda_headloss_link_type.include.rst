borda_headloss_link_type
------------------------



===========================  ====  ===========================
name                         id    description
===========================  ====  ===========================
enlargment_with_transition   11    Enlargment with transition
screen_normal_to_flow        14    Screen normal to flow
sharp_angle_bend             13    Sharp angle bend
junction                     6     Junction
lateral_outlet               3     Lateral outlet
t_shape_junction             20    T shape junction
t_shape_bifurcation          19    T shape bifurcation
friction                     16    Friction
bifurcation                  5     Bifurcation
circular_bend                12    Circular bend
q                            1     Law dz=KQ2
transition_edge_inlet        7     Transition edge inlet
diffuser                     8     Diffuser
lateral_inlet                4     Lateral inlet
v                            2     Law dE=(K/2g).(Q2/S2)
sharp_transition_geometry    9     Sharp geometry transition
contraction_with_transition  10    Contraction with transition
parametric                   17    Parametric
screen_oblique_to_flow       15    Screen oblique to flow
sharp_bend_rectangular       18    Sharp rectangular bend
===========================  ====  ===========================

enlargment_with_transition (borda_headloss_link_type)
-----------------------------------------------------

=================  ==================================================================  =========  ===========  =======
name               type                                                                default    constrain    place
=================  ==================================================================  =========  ===========  =======
up_cs_area         real                                                                                        2
down_cs_zinvert    real                                                                                        6
down_geom          enum('circular', 'canal', 'computed')                                                       4
up_geom            enum('circular', 'canal', 'computed')                                                       1
half_angle         real                                                                                        8
up_cs_zinvert      real                                                                                        3
down_cs_area       real                                                                                        5
transition_option  enum('conic', 'placeholder_do_not_display', 'rectangular', 'flat')                          7
=================  ==================================================================  =========  ===========  =======

screen_normal_to_flow (borda_headloss_link_type)
------------------------------------------------

=================  ===============================================================================================================================================================================  =========  ===========  =======
name               type                                                                                                                                                                             default    constrain    place
=================  ===============================================================================================================================================================================  =========  ===========  =======
up_cs_area         real                                                                                                                                                                                                     2
obstruction_rate   real                                                                                                                                                                                                     4
up_geom            enum('circular', 'canal', 'computed')                                                                                                                                                                    1
v_angle            real                                                                                                                                                                                                     5
grid_shape         enum('rectangular_edge', 'upstream_round_edge', 'up_and_downstream_round_edge', 'sharp_large_downstream_edge', 'sharp_narrow_downstream_edge', 'pointed_edge', 'circular_edge')                          7
up_cs_zinvert      real                                                                                                                                                                                                     3
deposition_option  enum('modern_automatic_grid', 'used_automatic', 'manual')                                                                                                                                                6
=================  ===============================================================================================================================================================================  =========  ===========  =======

sharp_angle_bend (borda_headloss_link_type)
-------------------------------------------

====================  =====================================  =========  ===========  =======
name                  type                                   default    constrain    place
====================  =====================================  =========  ===========  =======
up_cs_area            real                                                           2
angle                 real                                                           4
up_geom               enum('circular', 'canal', 'computed')                          1
up_cs_zinvert         real                                                           3
ls                    real                                                           6
wall_friction_option  enum('smooth', 'rough')                                        7
connector_no          integer                                                        5
====================  =====================================  =========  ===========  =======

junction (borda_headloss_link_type)
-----------------------------------

===============  =====================================  =========  ===========  =======
name             type                                   default    constrain    place
===============  =====================================  =========  ===========  =======
up_cs_area       real                                                           2
angle            real                                                           7
down_cs_zinvert  real                                                           6
down_geom        enum('circular', 'canal', 'computed')                          4
up_geom          enum('circular', 'canal', 'computed')                          1
up_cs_zinvert    real                                                           3
down_cs_area     real                                                           5
===============  =====================================  =========  ===========  =======

lateral_outlet (borda_headloss_link_type)
-----------------------------------------

==============  =====================================  =========  ===========  =======
name            type                                   default    constrain    place
==============  =====================================  =========  ===========  =======
up_cs_zinvert   real                                                           3
outlet_shape    enum('sharp', 'rounded')                                       5
up_cs_area      real                                                           2
outlet_cs_area  real                                                           4
up_geom         enum('circular', 'canal', 'computed')                          1
==============  =====================================  =========  ===========  =======

t_shape_junction (borda_headloss_link_type)
-------------------------------------------

===============  =====================================  =========  ===========  =======
name             type                                   default    constrain    place
===============  =====================================  =========  ===========  =======
up_cs_area       real                                                           2
down_cs_zinvert  real                                                           6
down_geom        enum('circular', 'canal', 'computed')                          4
up_geom          enum('circular', 'canal', 'computed')                          1
up_cs_zinvert    real                                                           3
down_cs_area     real                                                           5
===============  =====================================  =========  ===========  =======

t_shape_bifurcation (borda_headloss_link_type)
----------------------------------------------

===============  =====================================  =========  ===========  =======
name             type                                   default    constrain    place
===============  =====================================  =========  ===========  =======
up_cs_area       real                                                           2
down_cs_zinvert  real                                                           6
down_geom        enum('circular', 'canal', 'computed')                          4
up_geom          enum('circular', 'canal', 'computed')                          1
up_cs_zinvert    real                                                           3
down_cs_area     real                                                           5
===============  =====================================  =========  ===========  =======

friction (borda_headloss_link_type)
-----------------------------------

=============  =====================================  =========  ===========  =======
name           type                                   default    constrain    place
=============  =====================================  =========  ===========  =======
up_cs_zinvert  real                                                           3
length         real                                                           4
up_cs_area     real                                                           2
up_geom        enum('circular', 'canal', 'computed')                          1
ks             real                                                           5
=============  =====================================  =========  ===========  =======

bifurcation (borda_headloss_link_type)
--------------------------------------

===============  =====================================  =========  ===========  =======
name             type                                   default    constrain    place
===============  =====================================  =========  ===========  =======
up_cs_area       real                                                           2
angle            real                                                           7
down_cs_zinvert  real                                                           6
down_geom        enum('circular', 'canal', 'computed')                          4
up_geom          enum('circular', 'canal', 'computed')                          1
up_cs_zinvert    real                                                           3
down_cs_area     real                                                           5
===============  =====================================  =========  ===========  =======

circular_bend (borda_headloss_link_type)
----------------------------------------

====================  =====================================  =========  ===========  =======
name                  type                                   default    constrain    place
====================  =====================================  =========  ===========  =======
up_cs_area            real                                                           2
curve_angle           real                                                           5
up_geom               enum('circular', 'canal', 'computed')                          1
up_cs_zinvert         real                                                           3
curve_radius          real                                                           4
wall_friction_option  enum('smooth', 'rough')                                        6
====================  =====================================  =========  ===========  =======

q (borda_headloss_link_type)
----------------------------

========  ======  =========  ===========  =======
name      type    default    constrain    place
========  ======  =========  ===========  =======
coef_kq2  real                            1
========  ======  =========  ===========  =======

transition_edge_inlet (borda_headloss_link_type)
------------------------------------------------

===============  =====================================  =========  ===========  =======
name             type                                   default    constrain    place
===============  =====================================  =========  ===========  =======
down_geom        enum('circular', 'canal', 'computed')                          1
entrance_shape   enum('sharp', 'rounded')                                       5
angle            real                                                           4
down_cs_zinvert  real                                                           3
down_cs_area     real                                                           2
===============  =====================================  =========  ===========  =======

diffuser (borda_headloss_link_type)
-----------------------------------

=============  =====================================  =========  ===========  =======
name           type                                   default    constrain    place
=============  =====================================  =========  ===========  =======
up_cs_area     real                                                           2
length         real                                                           4
up_geom        enum('circular', 'canal', 'computed')                          1
half_angle     real                                                           5
up_cs_zinvert  real                                                           3
diffus_geom    enum('circular', 'square', 'plane')                            6
=============  =====================================  =========  ===========  =======

lateral_inlet (borda_headloss_link_type)
----------------------------------------

===============  =====================================  =========  ===========  =======
name             type                                   default    constrain    place
===============  =====================================  =========  ===========  =======
inlet_cs_area    real                                                           4
down_geom        enum('circular', 'canal', 'computed')                          1
inlet_shape      enum('sharp', 'rounded')                                       5
down_cs_zinvert  real                                                           3
down_cs_area     real                                                           2
===============  =====================================  =========  ===========  =======

v (borda_headloss_link_type)
----------------------------

===============  =====================================  =========  ===========  =======
name             type                                   default    constrain    place
===============  =====================================  =========  ===========  =======
reference_cs     enum('upstream', 'downstream')                                 7
up_cs_area       real                                                           2
down_cs_zinvert  real                                                           6
down_geom        enum('circular', 'canal', 'computed')                          4
up_geom          enum('circular', 'canal', 'computed')                          1
up_cs_zinvert    real                                                           3
coef_kv_12       real                                                           8
coef_kv_21       real                                                           9
down_cs_area     real                                                           5
===============  =====================================  =========  ===========  =======

sharp_transition_geometry (borda_headloss_link_type)
----------------------------------------------------

===============  =====================================  =========  ===========  =======
name             type                                   default    constrain    place
===============  =====================================  =========  ===========  =======
up_cs_area       real                                                           2
down_cs_zinvert  real                                                           6
down_geom        enum('circular', 'canal', 'computed')                          4
middle_option    enum('no', 'yes')                                              7
up_geom          enum('circular', 'canal', 'computed')                          1
up_cs_zinvert    real                                                           3
down_cs_area     real                                                           5
middle_cs_area   real                                                           8
===============  =====================================  =========  ===========  =======

contraction_with_transition (borda_headloss_link_type)
------------------------------------------------------

=================  =====================================  =========  ===========  =======
name               type                                   default    constrain    place
=================  =====================================  =========  ===========  =======
up_cs_area         real                                                           2
down_cs_zinvert    real                                                           6
down_geom          enum('circular', 'canal', 'computed')                          4
up_geom            enum('circular', 'canal', 'computed')                          1
half_angle         real                                                           8
up_cs_zinvert      real                                                           3
down_cs_area       real                                                           5
transition_option  enum('conic', 'rectangular', 'flat')                           7
=================  =====================================  =========  ===========  =======

parametric (borda_headloss_link_type)
-------------------------------------

========  ===========  =========  ===========  =======
name      type         default    constrain    place
========  ===========  =========  ===========  =======
qh_array  real[10][2]                          1
========  ===========  =========  ===========  =======

screen_oblique_to_flow (borda_headloss_link_type)
-------------------------------------------------

==================  ======================================================================================================================================================================================================================================================================================================  =========  ===========  =======
name                type                                                                                                                                                                                                                                                                                                    default    constrain    place
==================  ======================================================================================================================================================================================================================================================================================================  =========  ===========  =======
grid_shape_oblique  enum('rectangular_edge', 'upstream_round_edge', 'up_and_downstream_round_edge', 'sharp_large_downstream_edge', 'sharp_narrow_downstream_edge', 'pointed_edge', 'placeholder_do_not_display', 'thick_rectangular_edge', 'thin_rectangular_edge', 'long_rectangular_edge', 'very_long_rectangular_edge')                          6
up_cs_area          real                                                                                                                                                                                                                                                                                                                            2
up_geom             enum('circular', 'canal', 'computed')                                                                                                                                                                                                                                                                                           1
v_angle             real                                                                                                                                                                                                                                                                                                                            5
up_cs_zinvert       real                                                                                                                                                                                                                                                                                                                            3
blocage_coef        real                                                                                                                                                                                                                                                                                                                            4
==================  ======================================================================================================================================================================================================================================================================================================  =========  ===========  =======

sharp_bend_rectangular (borda_headloss_link_type)
-------------------------------------------------

===============  ========================================================================================  =========  ===========  =======
name             type                                                                                      default    constrain    place
===============  ========================================================================================  =========  ===========  =======
up_cs_area       real                                                                                                              2
down_cs_zinvert  real                                                                                                              6
down_geom        enum('circular', 'canal', 'computed')                                                                             4
bend_shape       enum(elbow_90_deg', 'z_shaped_elbow', 'u_shaped_elbow', 'two_elbows_in_distinct_planes')                          7
up_geom          enum('circular', 'canal', 'computed')                                                                             1
up_cs_zinvert    real                                                                                                              3
param4           real                                                                                                              11
param3           real                                                                                                              10
param2           real                                                                                                              9
param1           real                                                                                                              8
down_cs_area     real                                                                                                              5
===============  ========================================================================================  =========  ===========  =======
