singularity_type
----------------



=======================  ====  ==============  ================
name                     id    abbreviation    group_hydra_no
=======================  ====  ==============  ================
zq_bc                    13    ZQ              15
zq_split_hydrology       21    DZH             7
pipe_branch_marker       9     MRKB            34
strickler_bc             12    RK              11
marker                   7     MRKP            35
model_connect_bc         19    RACC            28
bradley_headloss         8     BRD             33
hydraulic_cut            2     CPR             23
reservoir_rsp_hydrology  23    RSPH            9
hydrograph_bc            17    HY              17
zregul_weir              5     DE              5
bridge_headloss          25    BRDG            34
qq_split_hydrology       20    DQH             6
gate                     1     VA              25
reservoir_rs_hydrology   22    RSH             8
param_headloss           4     DH              10
froude_bc                15    ZF              25
tz_bc                    14    ZT              16
hydrology_bc             24    CLH             10
regul_sluice_gate        6     ACTA            31
tank_bc                  16    BO              18
borda_headloss           3     SMK             32
constant_inflow_bc       18    HC              27
weir_bc                  11    DL              26
=======================  ====  ==============  ================
