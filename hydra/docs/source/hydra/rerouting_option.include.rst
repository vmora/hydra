rerouting_option
----------------



========  ====  =========================
name      id    description
========  ====  =========================
qlimit    2     Q limit (m3/s)
fraction  1     Fraction (between 0 et 1)
========  ====  =========================
