.. _meshing:

Principes de génération du maillage 2D 
######################################

Les evolutions 
==============

Dans la version actuelle d’Hydrariv le maillage 2D est géré par l’outil Hydramap, les objets de 
maillage générés sont ensuite importés dans l’IHM d’Hydrariv. 

Dans la nouvelle version toutes les opérations de maillage 2D sont gérées directement par 
l’IHM. 

Plusieurs évolutions importantes sont à noter: 
 - Les règles de génération sont désormais beaucoup plus strictes: le maillage doit respecter le principe de connectivité entre mailles, à savoir que les sommets de mailles adjacentes doivent coïncider (à une tolérance de proximité près). La configuration suivante n’est plus acceptée, du moins pour les nouveaux maillages: 

.. image:: figure1.svg

..

 
 - Il est désormais possible d’ajuster automatiquement le maillage à des lignes de contraintes traversant un bloc. Cette possibilité ouvre le champ à de nombreuses fonctionnalités non disponibles dans Hydramap. 

 - On distingue désormais les opérations géométriques de génération/redécoupage du maillage des opérations de calcul de cotes topographiques affectées aux champs des objets générés: mailles et liaisons: 
 - Les opérations de maillage reposent sur des commandes SQL de la BD PostGré/PostGis et sur des utilitaires de calculs spécifiques appelés par QGIS, 
 - Les calculs topo font appel à la bibliothèque GDAL et au MNT sous-jacent. Ils sont exécutés après les opérations de génération du maillage. 

 
La présente note décrit les principes de maillage retenu, ainsi que les différentes fonctionnalités 
sous-jacentes. 
 
 
Les principes 
=============

Le bloc 
-------

**Table bloc_2d et tables liées**

Le bloc est un polygone formé de 3 ou 4 polylignes. Il sert d’enveloppe pour générer un maillage triangulaire ou quadrangulaire. C’est l’objet pivot dans le processus de création d’un maillage 2D. 
Du point de vue du MCD Les objets « blocs », « polylignes », « maillages élémentaires », et « domaines 2D » appartiennent à des tables liées par des relations bien précises, définies ci-dessous: 
 
.. image:: figure2.svg

..
 

Il faut respecter le phasage de création suivant: 
 - création d’un nom de domaine dans la table « domain_2d », 
 - création de polylignes dans la table « polyligne », 
 - création d’un bloc dans la table « bloc_2d, 
 - génération du maillage ; les objets générés sont stockées dans « domain_2d_node » et dans la table « mesh_2d_link » ( ou autres tables « link » dans le cas de liaisons particulières). 

**Propriétés d’un bloc et maillage de référence**

Un bloc peut être de deux types: 
 - Forme quadrangulaire: un bloc quadrangulaire permet de générer des mailles quadrangulaires, chacune étant repérée par les indices (i,j) qui définissent sa position relative à l’intérieur du bloc. Le nombre de segments est le même sur les deux contours opposés de chaque bloc: 

.. image:: figure3.svg

..


 - Forme triangualaire: un bloc triangulaire permet de générer des mailles triangulaires. Le nombre de segments est le même sur chacun des trois contours (Contrairement au maillage quadrangulaire il n’y a pas de repérage relatif d’une maille triangulaire dans le bloc):


.. image:: figure4.svg

..


Le style d’affichage des LF_2D, avec des extrémités de polylignes sous forme de flèches, permet 
de visualiser clairement les limites de chacun des côtés constituant le bloc_2D. 


**Formulaire de saisie d’un bloc**

Il se présente comme suit: 

.. image:: figure5.svg

..


La boite de dialogue de paramétrage du bloc s’ouvre. L’utilisateur paramètre le bloc. Toutes les manipulations ultérieures sur ce bloc se font via l’objet bloc ; l’utilisateur n’a pas d’accès en écriture directe sur la couche LF_2D. Il est possible de supprimer la totalité des mailles d’un bloc donné et de les régénérer en modifiant par exemple le nombre de segments dans un sens. 
 

 

 

**Génération du maillage**

Cette opération est faite automatiquement par sélection du bloc et appel à une fonction 
spécifique (anciennement fonction bloc_2D dans HydraMap). 

*Note sur l'algorithme de génération des maillages quadrangulaires*

L'algorithme prend en entrées 4 linestrings qui deux à deux comportent le même nombre de segment, leur orientation et leur appellation sont définies de la manière suivante:

.. image:: figure_algo1.svg

L'algorithme commence par un changement de représentation des linestrings `start` et `end` en:
  - extrémités
  - écart par rapport à la ligne droite (`offsets`)

.. image:: figure_algo2.svg

L'algorithme construit les lignes intermédiaires qui sont définies de la manière suivant:
  - leurs extrémités sont les points des linestrings `bottom` et `top`
  - leurs `offsets` sont interpolés linéairement entre les `offsets` de `start` et ceux de `end`

.. image:: figure_algo3.svg

Une fois les lignes intermédiaires construites, on peut créer les quadrangles.


*Note sur l'algorithme de génération des maillages triangulaires*

L'algorithme prend en entrées 3 linestrings les deux premières comportant le même nombre de segments, leur orientation et leur appellation sont définies de la manière suivante:

.. image:: figure_algo4.svg

Le nombre de segment de `bottom` n'est pas pris en compte dans l'algorithme.

La première étape est de discrétiser `bottom` en `n` segments, on calcul ensuite les `offsets` pour `bottom`:

.. image:: figure_algo5.svg

On crée ensuite les lignes intermédiaire à partir de `bottom` en interpolant les `offsets` de la façon suivante:

.. image:: figure_algo6.svg

On créer finalement les triangles:

.. image:: figure_algo7.svg


Principe de remaillage a l’interieur d’un bloc 
----------------------------------------------

Deux remarques préliminaires importantes: 
- Le remaillage n’est possible qu’à l’intérieur d’un bloc quadrangulaire, 
- Une maille rectangulaire, une fois remaillée, ne peut être découpée une seconde fois. Les conséquences sur la gestion des lignes de contraintes sont discutées plus loin. 

Le problème posé est le suivant: un bloc quadrangulaire vient d’être maillé. On veut appuyer le maillage de ce bloc sur une ligne de contrainte traversant le bloc, et l’affiner suffisamment pour épouser le mieux possible les contours de la ligne de contrainte. 

Il faut au départ sélectionner un facteur de remaillage autour de la ligne de contrainte: 1, 2 ou 4. Chaque maille traversée par la ligne de contrainte est subdivisé en 1, 4, ou 16 mailles élémentaires comme suit: 
 

.. image:: figure6.svg
 
..


Les principes de remaillage sont les mêmes pour tous les facteurs de remaillage, mais les modalités diffèrent. Elles sont examinées ci-après pour chacun de ces facteurs. 

**Remaillage de facteur 1**

C’est un cas particulièrement simple, puisque le domaine d’influence d’une maille traversée se limite à cette maille: 

.. image:: figure7.svg
 
..

 
Il faut tout d’abord déformer la ligne de contrainte et venir l’appuyer sur les sommets les plus proches des mailles traversées: 

 
.. image:: figure8.svg
 
..


La dernière étape consiste à générer les liaisons à travers la ligne de contraintes. Les liaisons LPAV ne sont pas modifiées dans cette configuration. 

**Alternative**

Plutôt que de "snapper" aux coins de la grille et être limité à des angles de O°, 45° et 90°, on peut bénéficier des décomposition en triangles et avoir en plus des angles de 22.5° et 67.5° en ajoutant les milieux des segments du contour des quadrangles et "snappant" les nœuds de la ligne sur le nœuds le plus proche:

.. image:: figure8a.svg

On a alors le maillage suivant:

.. image:: figure8b.svg
 
Pour les intersections de lignes de contraintes cette approche eut présenter un problème au niveau de certaines zones:

.. image:: figure8c.svg

**Remaillage de facteur 2**

*Etape 1: sélection des mailles du bloc susceptibles d’être remaillées*

.. image:: figure9.svg
 
..

 
Dans le cas général d’un nombre multiple de mailles traversées par la ligne de contrainte il faut sélectionner l’union de toutes les mailles sous influence, comme illustré ci-dessous dans le cas d’un facteur 2 de remaillage: 

.. image:: figure10.svg
 
..

 
On sélectionne toutes les mailles de couleur bleue (traversées par la polyligne) et verte (mailles sous influence) 
 
*Etape 2: Remaillage des mailles sélectionnées*

C’est l’étape la plus compliquée en termes algorithmiques: Il faut tout d’abord déformer la ligne de contrainte et venir l’appuyer sur les sommets les plus proches des mailles traversées , lesquelles sont subdivisées en sous mailles: 

 
.. image:: figure11.svg
 
..


Il faut ensuite remailler les mailles adjacentes de façon à respecter la règle de connectivité. La règle adoptée pour le découpage est la suivante: 
 
 - Cas d’une maille adjacente à une maillé bordée par deux segments sur un seul côté: on sous découpe comme suit: 

.. image:: figure12.svg
 
..


 - Cas d’une maille adjacente à une maille bordée par deux segments sur deux cotés adjacents: on sous découpe comme suit: 

.. image:: figure13.svg
 
..

 
 - Cas d’une maille adjacente à une maille bordée par deux segments sur deux côtés opposés: on sous découpe comme suit: 

.. image:: figure14.svg
 
..
 

 - Cas d’une maille adjacente à une maille bordée par deux segments sur trois côtés: on sous découpe comme suit: 

.. image:: figure15.svg
 
..
 

 - Cas d’une maille adjacente à une maille bordée par deux segments sur les quatre côtés: on sous découpe comme suit: 

.. image:: figure16.svg
 
..
 
 
L’application de ces principes conduit au remaillage suivant dans notre exemple ci-dessus: 

.. image:: figure17.svg
 
..
 
 
 

 
 
*Etape 3: élimination des mailles originelles lorsqu’elles sont remaillées, élimination des liaisons afférentes et génération des liaisons supplémentaires.*

Des liaisons de nature différentes des LPAV peuvent également être générées de part et d’autre 
de la ligne de contrainte, selon la nature de l’opération. 
 
**Remaillage de facteur 4**

*Etape 1: sélection des mailles du bloc susceptibles d’être remaillées*

Le domaine d’influence d’une maille affectée d’un facteur de remaillage égal à 4 est délimité ci-dessous: 

.. image:: figure18.svg
 
..

 
Dans le cas général d’un nombre multiple de mailles traversées par la ligne de contrainte il faut sélectionner l’union de toutes les mailles sous influence, comme illustré ci-dessous dans le cas d’un facteur 4 de remaillage: 
 
.. image:: figure19.svg
 
..

 
Il faut ensuite remailler les mailles adjacentes de façon à respecter la règle de connectivité. Le nombre de cas élémentaires à considérer est assez important dans le cas du facteur 4 de remaillage, il faut tous les recenser pour les besoins de la programmation. 


On commence par subdiviser en 4 mailles toutes les mailles connexes aux mailles traversées par la polyligne et on sous maille en fonction du nombre de segments de bordure des mailles adjacentes: 
 
.. image:: figure20.svg
 
..


On traite ensuite les mailles externes sous influence et on sous maille en fonction du nombre de 
segments de bordure des mailles adjacentes: 

.. image:: figure21.svg
 
..
 
 
**Etape 3: élimination des mailles originelles lorsqu’elles sont remaillées, élimination des liaisons afférentes et génération des liaisons supplémentaires.**

Des liaisons de nature différentes des LPAV peuvent également être générées de part et d’autre de la ligne de contrainte, selon la nature de l’opération. 
 
*Commentaire important:* 
Il est nécessaire, dans le cas de remaillages successifs à l’intérieur d’un bloc, de connaitre le statut d’une maille originelle. On munit pour cela la table « bloc » d’un tableu boléan: tbolean ( ni, numj) , prenant deux valeurs: 

  - .true. si la maille originelle est toujours présente dans la table « domain_2d_node » 
  - .false. si la maille a été supprimée: remplacement par deux mailles plus petites ou éliminée suite à une troncature de bloc (cf. §3.1.2).  


Cas de lignes de contraintes multiples 
--------------------------------------

Les lignes de contraintes sont traitées en séquence et non simultanément. 

Le principe retenu est le suivant: si une ligne de contrainte intersecte une maille déjà décomposée en sous mailles, le programme ne recrée pas de mailles supplémentaires, mais il déforme le contour de la ligne de contrainte pour lui faire emprunter les arêtes d’une maille existante. De façon plus systématique le programme examine pour chaque maille intersectée le statut des mailles sous influence de la maille intersectée. Si une de ces mailles a le statut tabolean(i,j)=.false., alors aucun remaillage n’est effectué à l’intérieur et autour de cette maille. 

L’exemple suivant illustre la méthode / on reprend le maillage précédent pour lequel une première ligne de contrainte a été introduite: 

.. image:: figure22.svg
 
..
 
 
Les mailles coloriées en bleu foncé sont celles pour lesquels un remaillage est possible. Les mailles en vert foncé sont les mailles attenantes, également remaillées.  Le long du tronçon AB aucun remaillage n’est possible: la ligne de contrainte est ajustée le long des arêtes des maillées déjà créées. On peut affiner localement dans le cas de mailles rectangulaires traversées de part en part, comme illustré ci-dessous: 

 
.. image:: figure23.svg
 
..
 

Cette méthode est très générale et peut être codée sans trop de difficultés. Elle montre par ailleurs le rôle de contrôle joué par la table tbolean(). 

*Remarque:* le fait de traiter les lignes de contrainte en séquence ne leur donne pas un rôle symétrique: la première ligne de contrainte traitée est la mieux servie car elle dispose de possibilités de remaillage de l’ensemble des mailles du bloc. Les lignes de contraintes suivantes doivent se « contenter » des mailles originelles restées libres pour bénéficier d’un maillage affiné. 
 
*Cas des blocs triangulaires:* Le maillage dans ces blocs est considéré comme décomposée: une ligne de contrainte traversant le bloc est obligatoirement réajustée sur les arêtes des mailles triangulaires à 
l’intérieur du bloc. 

Jointure entre deux blocs contigus 
----------------------------------

Cette opération doit être faite préalablement à l’introduction de lignes de contraintes, afin d’assurer la continuité et la connectivité du maillage si une ligne de contrainte traverse plus d’un bloc. 

L’opération de jointure entre deux blocs contigus nécessite d’ajuster localement le maillage. Les algorithmes complets restent à développer mais le principe retenu est illustré par les exemples de configurations suivantes: 

**Blocs quadrangulaires de taille comparable** 

.. image:: figure24.svg
 
..
 
 
La méthode consiste à générer des mailles triangulaires le long d’une rangée de bloc et d’ajuster les sommets sur ceux des mailles de l’autre bloc: 

.. image:: figure25.svg
 
..


**Blocs quadrangulaires de tailles différentes**

.. image:: figure26.svg
 
..

 
On remaille d’un facteur 2 ( ou 4 si nécessaire) la rangée du bloc inférieur. On est alors ramené au cas précédent: 
 

.. image:: figure27.svg
 
..


**Un bloc triangulaire et un bloc quadrangulaire**

.. image:: figure28.svg
 
..

 
Le principe est le même: on triangule la dernière rangée du bloc quadrangulaire: 

 
.. image:: figure29.svg
 
..


Application: les fonctionnalites reposant sur les operations de remaillage 
===========================================================================

Les fonctionnalités ci-après font appel à un bloc donné. En cas de singularités traversant plusieurs blocs il faut recommencer l’opération autant de fois qu’il y a de blocs traversés. 

On suppose dans ce qui suit que le bloc est déjà maillé. 

On décrit ci-après les traitements sous-jacents à chaque fonctionnalité, sans préjuger à ce stade des outils les plus adaptés pour réaliser ces traitements: requêtes SQL, programmation sous Python, modules externe en C ou Fortran. 

Création d’un talus 
-------------------

.. image:: figure30.svg
 
..

 
Cette opération correspond à la création d’une ligne de contrainte. 

*Données utilisateur:* 
  - Identifiant du bloc 
  - Identifiant de la polyligne 
  - facteur de remaillage souhaité.

Traitements réalisés: 
  - Sélection des mailles intersectées par la polyligne (pas besoin de trier) 
  - Sélection des mailles sous influence. 
  - Création des nouvelles mailles issues du remaillage. 
  - Création des liaisons issues du remaillage en distinguant: 
    - Les liaisons intersectant le talus, 
    - Les autres liaisons. 
  - Création de la polyligne modifiée. 
  - Suppression des mailles intersectées par la polyligne, des mailles sous influence et des liaisons connectées à ces mailles. 
  - Actualisation du tableau tbolean(). 

Les données attributaires des mailles et liaisons créées sont calculées dans une seconde étape. 


Rognure d’une région à l’extérieur d’un polygone 
------------------------------------------------

On souhaite éliminer toute la région en couleur jaune qui est extérieure au polygone délimité en rouge: 

.. image:: figure31.svg
 
..


*Données utilisateur:*
  - Identifiant du bloc 
  - Identifiant du pllygone 

*Traitements réalisés:*

Le travail de traitement est le même que dans le cas précédent.

Il faudra en plus supprimer toutes les mailles extérieures au polygone en fin de traitement. 
 
Cette fonctionnalité est très utile dans le cas de domaine avec une bordure externe compliquée, comme par exemple une région estuarienne, illustrée comme suit: 

.. image:: figure32.svg
 
..

 
On commence par mailler un bloc rectangulaire qui englobe les contours de l’estuaire et de la zone côtière: 
 
 
.. image:: figure33.svg
 
..
 

On peut ensuite générer des maillages triangulaires pour modéliser l’océan: 
 
.. image:: figure34.svg
 
..
 

Il faut enfin jointer les blocs entre eux selon la méthode décrite au § 2.4. 
 

Suppression d’une région à l’intérieur d’un polygone 
----------------------------------------------------

Cette fonctionnalité est symétrique de la précédente: 

.. image:: figure35.svg
 
..
 

En fin de traitement il faut supprimer les mailles à l’intérieur du contour polygonal. 
 

Resserrement d’un maillage à l’intérieur d’un bloc 
--------------------------------------------------


.. image:: figure36.svg
 
..
 

Cette opération n’est possible que si les mailles concernées par le remaillage sont toutes d’origine: ( Tbolean (i,j)=.true). 

*Données utilisateurs:*
  - Identificateur du bloc 
  - Indice des mailles extrémités du quadrangle, 
  - facteur de remaillage: 2 ou 4 

Cette fonctionnalité est intéressante pour simuler les perturbations apportées par un obstacle de petites dimensions à l’écoulement. 


Immersion d’un lit mineur de faible largeur dans un bloc 
--------------------------------------------------------

.. image:: figure37.svg
 
..
 

Cette configuration est similaire à celle d’une ligne de contrainte, mais en ajoutant les opérations suivantes: 
  - Créations de nœuds « river node » au centroide de chaque maille intersectée, 
  - Création de liaisons de type LRC connectant le nœud à chacune des mailles de part et d’autre du bief. 

 
.. image:: figure38.svg
 
..
 

Découpage d’un lit mineur avec berges s’appuyant sur le maillage 
----------------------------------------------------------------

.. image:: figure39.svg
 
..
 

*Données d’entrée:*
  - Identificateur du bloc, 
  - Id du bief fialire 
  - Polylignes représentant les berges RG et RD, 
  - Facteur de maillage. 

 
*Traitements réalisés:*

On commence par générer un maillage découpé par le polygone formé par les berges et les extrémités du bloc intersectées par les berges: 

.. image:: figure40.svg
 
..
 

On supprime ensuite les mailles à l’intérieur du polygone et on vérifie que la polyligne du bief reste à l’intérieur du polygone évidé. On corrige ponctuellement le tracé du bief si nécessaire ; 

.. image:: figure41.svg
 
..
 
 
La dernière opération consiste à générer des nœuds le long du bief filaire et générer des liaisons LRC vers les mailles bordant les berges. Ce travail présente des difficultés d’automatisation car le découpage des mailles n’est pas régulier. Une création manuelle ou semi manuelle des liaisons apparait être la solution la plus appropriée. Ce point reste à approfondir.  

