scenario
--------



==========================  ===============================  ==================  ===============  =======================================================================
Attribut                    Type                             Défaut              Contrainte       Clef Etrangère
==========================  ===============================  ==================  ===============  =======================================================================
id                          serial                                               primary key
name                        varchar(24)                      calculé
comment*                    varchar(256)
comput_mode                 hydra_computation_mode           calculé
dry_inflow*                 integer                                                               project.dry_inflow_scenario(id) :ref:`🔗 <project-dry-inflow-scenario>`
rainfall*                   integer                                                               project._rainfall(id) :ref:`🔗 <project-rainfall>`
dt_hydrol_mn                real                             5
soil_moisture_coef*         real                             0                   voir ci-dessous
runoff_adjust_coef*         real                             1                   voir ci-dessous
option_dim_hydrol_network   bool                             false
date0                       timestamp                        '2000-01-01         00:00:00'
tfin_hr                     real                             12
tini_hydrau_hr              real                             12
dtmin_hydrau_hr             real                             0.005
dtmax_hydrau_hr             real                             0.1
dzmin_hydrau                real                             0.05
dzmax_hydrau                real                             0.1
flag_save                   bool                             'f'
tsave_hr*                   real                             0
flag_rstart                 bool                             'f'
flag_gfx_control            bool                             'f'
scenario_rstart*            integer                                                               project.scenario(id) :ref:`🔗 <project-scenario>`
flag_hydrology_rstart       bool                             'f'
scenario_hydrology_rstart*  integer                                                               project.scenario(id) :ref:`🔗 <project-scenario>`
trstart_hr*                 real                             0
graphic_control             bool                             'f'
model_connect_settings      hydra_model_connection_settings  'cascade'
tbegin_output_hr            real                             0
tend_output_hr              real                             12
dt_output_hr                real                             .1
c_affin_param               bool                             'f'
t_affin_start_hr            real                             0
t_affin_min_surf_ddomains   real                             0
strickler_param             bool                             'f'
option_file                 bool                             'f'
option_file_path*           varchar(256)
output_option               bool                             'f'
transport_type              hydra_transport_type             'no_transport'
iflag_mes                   bool                             'f'
iflag_dbo5                  bool                             'f'
iflag_dco                   bool                             'f'
iflag_ntk                   bool                             'f'
quality_type                hydra_quality_type               'physico_chemical'
diffusion                   bool                             'f'
dispersion_coef             real                             20
longit_diffusion_coef       real                             41
lateral_diff_coef           real                             .45
water_temperature_c         real                             20
min_o2_mgl                  real                             1.5
kdbo5_j_minus_1             real                             0.2
koxygen_j_minus_1           real                             0.45
knr_j_minus_1               real                             0.09
kn_j_minus_1                real                             0.035
bdf_mgl                     real                             1.5
o2sat_mgl                   real                             9.07
iflag_ad1                   bool                             'f'
iflag_ad2                   bool                             'f'
iflag_ad3                   bool                             'f'
iflag_ad4                   bool                             'f'
ad1_decay_rate_jminus_one   real                             0
ad2_decay_rate_jminus_one   real                             0
ad3_decay_rate_jminus_one   real                             0
ad4_decay_rate_jminus_one   real                             0
sst_mgl                     real                             0
sediment_file               varchar(256)                     ''
scenario_ref*               integer                                                               project.scenario(id) :ref:`🔗 <project-scenario>`
==========================  ===============================  ==================  ===============  =======================================================================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('SCN_')

    comput_mode

    .. code-block:: sql

        'hydrology_and_hydraulics'

Contraintes:

    .. code-block:: sql

        soil_moisture_coef >= 0
        and soil_moisture_coef <=1

    .. code-block:: sql

        runoff_adjust_coef >= 0
