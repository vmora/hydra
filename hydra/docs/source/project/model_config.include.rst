model_config
------------



=============  ============  ========  ============  ================
Attribut       Type          Défaut    Contrainte    Clef Etrangère
=============  ============  ========  ============  ================
id             serial                  primary key
name           varchar(16)
config         integer       1
cascade_order  integer       calculé
comment*       varchar(256)
=============  ============  ========  ============  ================

Champs calculés:

    cascade_order

    .. code-block:: sql

        currval('project.model_config_id_seq')

Contraintes:

    .. code-block:: sql

        unique(name, config)
