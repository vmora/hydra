param_external_hydrograph
-------------------------



==============  ============  ========  =================  =================================================
Attribut        Type          Défaut    Contrainte         Clef Etrangère
==============  ============  ========  =================  =================================================
id              serial                  primary key
param_scenario  integer                 on delete cascade  project.scenario(id) :ref:`🔗 <project-scenario>`
external_file   varchar(256)
==============  ============  ========  =================  =================================================
