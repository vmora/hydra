_gage_rainfall
--------------
type=5


======================  =================================  ========  ============  ================
Attribut                Type                               Défaut    Contrainte    Clef Etrangère
======================  =================================  ========  ============  ================
cbv_grid_connect_file*  varchar(256)
interpolation*          hydra_rainfall_interpolation_type
======================  =================================  ========  ============  ================

Validité:

    .. code-block:: sql

        interpolation is not null

