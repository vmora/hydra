_double_triangular_rainfall
---------------------------
type=3


==================  =======  ========  ============  ========================================================
Attribut            Type     Défaut    Contrainte    Clef Etrangère
==================  =======  ========  ============  ========================================================
montana_total*      integer                          project.coef_montana(id) :ref:`🔗 <project-coef-montana>`
montana_peak*       integer                          project.coef_montana(id) :ref:`🔗 <project-coef-montana>`
total_duration_mn*  real
peak_duration_mn*   real
peak_time_mn*       real
==================  =======  ========  ============  ========================================================

Validité:

    .. code-block:: sql

        montana_total is not null
        montana_peak is not null 
        total_duration_mn is not null
        total_duration_mn>0
        peak_duration_mn is not null
        peak_duration_mn>0
        peak_duration_mn<total_duration_mn
        peak_time_mn is not null
        peak_time_mn>.5*peak_duration_mn
        peak_time_mn<(total_duration_mn - .5*peak_duration_mn)

