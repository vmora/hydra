gage_rainfall_data
------------------



=============  ============  ========  =================  ============================================================
Attribut       Type          Défaut    Contrainte         Clef Etrangère
=============  ============  ========  =================  ============================================================
rainfall       integer                 on delete cascade  project._gage_rainfall(id) :ref:`🔗 <project-gage-rainfall>`
rain_gage      integer                 on delete cascade  project.rain_gage(id) :ref:`🔗 <project-rain-gage>`
is_active*     boolean
t_mn_hcum_mm*  real[100][2]            voir ci-dessous
constraint     pk_id                   voir ci-dessous
=============  ============  ========  =================  ============================================================

Contraintes:

    .. code-block:: sql

        array_length(t_mn_hcum_mm, 1) <=1000
        and array_length(t_mn_hcum_mm, 1)>=1
        and array_length(t_mn_hcum_mm, 2)=2

    .. code-block:: sql

        primary key (rainfall, rain_gage)
