coef_montana
------------



================  ============  ========  ============  ================
Attribut          Type          Défaut    Contrainte    Clef Etrangère
================  ============  ========  ============  ================
id                serial                  primary key
name              varchar(24)   calculé
comment*          varchar(256)
return_period_yr  real
coef_a            real                    coef_a > 0
coef_b            real                    coef_b < 0
================  ============  ========  ============  ================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('MONTANA_')
