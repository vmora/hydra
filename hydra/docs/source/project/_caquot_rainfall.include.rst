_caquot_rainfall
----------------
type=1


==========  =======  ========  ============  ========================================================
Attribut    Type     Défaut    Contrainte    Clef Etrangère
==========  =======  ========  ============  ========================================================
montana*    integer                          project.coef_montana(id) :ref:`🔗 <project-coef-montana>`
==========  =======  ========  ============  ========================================================

Validité:

    .. code-block:: sql

        montana is not null

