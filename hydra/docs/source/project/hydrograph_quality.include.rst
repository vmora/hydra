hydrograph_quality
------------------



====================  ======  ========  ============  ================
Attribut              Type    Défaut    Contrainte    Clef Etrangère
====================  ======  ========  ============  ================
id                    serial            primary key
dry_flow_dbo5_mgl     real
dry_flow_nh4_mgl      real
dry_flow_no3_mgl      real
dry_flow_o2_mgl       real
dry_flow_ad_mgl       real
dry_flow_mes_mgl      real
runoff_flow_dbo5_mgl  real
runoff_flow_nh4_mgl   real
runoff_flow_no3_mgl   real
runoff_flow_ad_mgl    real
runoff_flow_mes_mgl   real
dbou_dbo5_ratio       real    1.2
dbo5_dcant_fraction   real    0
mes_decant_fraction   real    0
====================  ======  ========  ============  ================
