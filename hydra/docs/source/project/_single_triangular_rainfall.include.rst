_single_triangular_rainfall
---------------------------
type=2


==================  =======  ========  ============  ========================================================
Attribut            Type     Défaut    Contrainte    Clef Etrangère
==================  =======  ========  ============  ========================================================
montana*            integer                          project.coef_montana(id) :ref:`🔗 <project-coef-montana>`
total_duration_mn*  real
peak_time_mn*       real
==================  =======  ========  ============  ========================================================

Validité:

    .. code-block:: sql

        montana is not null 
        total_duration_mn is not null
        total_duration_mn>0
        peak_time_mn is not null
        peak_time_mn>0
        peak_time_mn<=total_duration_mn

