pollution_land_accumulation
---------------------------



==============  ==========  ========  ===============  ================
Attribut        Type        Défaut    Contrainte       Clef Etrangère
==============  ==========  ========  ===============  ================
id              serial                primary key
sds_stockk_gha  real[4][4]            voir ci-dessous
cds_stock_kgha  real[4][4]            voir ci-dessous
==============  ==========  ========  ===============  ================

Contraintes:

    .. code-block:: sql

        array_length(sds_stockk_gha, 1)=4
        and array_length(sds_stockk_gha, 2)=4

    .. code-block:: sql

        array_length(sds_stockk_gha, 1)=4
        and array_length(sds_stockk_gha, 2)=4
