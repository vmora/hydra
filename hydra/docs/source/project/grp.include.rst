grp
---



==========  ============  ========  ============  ================
Attribut    Type          Défaut    Contrainte    Clef Etrangère
==========  ============  ========  ============  ================
id          serial                  primary key
name        varchar(256)  calculé   unique
==========  ============  ========  ============  ================

Champs calculés:

    name

    .. code-block:: sql

        'GROUP_'||currval('project.grp_id_seq')::varchar
