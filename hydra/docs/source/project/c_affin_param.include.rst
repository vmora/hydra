c_affin_param
-------------



===========  ===========  ========  =================  =================================================
Attribut     Type         Défaut    Contrainte         Clef Etrangère
===========  ===========  ========  =================  =================================================
id           serial                 primary key
scenario     integer                on delete cascade  project.scenario(id) :ref:`🔗 <project-scenario>`
model        varchar(24)
type_domain  varchar(24)            voir ci-dessous
element      varchar(24)
===========  ===========  ========  =================  =================================================

Contraintes:

    .. code-block:: sql

        unique(scenario, model, type_domain, element)

    .. code-block:: sql

        type_domain in ('reach', 'domain_2d', 'branch')
