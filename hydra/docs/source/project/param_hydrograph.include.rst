param_hydrograph
----------------



===============  ============  ========  =================  =================================================
Attribut         Type          Défaut    Contrainte         Clef Etrangère
===============  ============  ========  =================  =================================================
id               serial                  primary key
param_scenario   integer                 on delete cascade  project.scenario(id) :ref:`🔗 <project-scenario>`
model            varchar(24)
hydrograph_from  varchar(24)
hydrograph_to    varchar(256)
===============  ============  ========  =================  =================================================
