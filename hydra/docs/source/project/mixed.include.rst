mixed
-----



==========  =======  ========  =================  =================================================
Attribut    Type     Défaut    Contrainte         Clef Etrangère
==========  =======  ========  =================  =================================================
ord         integer
scenario    integer            on delete cascade  project.scenario(id) :ref:`🔗 <project-scenario>`
grp         integer                               project.grp(id) :ref:`🔗 <project-grp>`
==========  =======  ========  =================  =================================================

Contraintes:

    .. code-block:: sql

        unique(scenario, grp)

    .. code-block:: sql

        unique(scenario, ord)
