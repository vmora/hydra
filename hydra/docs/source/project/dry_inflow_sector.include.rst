dry_inflow_sector
-----------------



==========  ============  ========  ================  ================
Attribut    Type          Défaut    Contrainte        Clef Etrangère
==========  ============  ========  ================  ================
id          serial                  primary key
name        varchar(24)   calculé
comment*    varchar(256)
geom        POLYGON                 ST_IsValid(geom)
==========  ============  ========  ================  ================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('SECT_')
