dry_inflow_hourly_modulation
----------------------------



==========  ============  ========  ===============  ================
Attribut    Type          Défaut    Contrainte       Clef Etrangère
==========  ============  ========  ===============  ================
id          serial                  primary key
name        varchar(24)   calculé
comment*    varchar(256)
hv_array*   real[][]                voir ci-dessous
==========  ============  ========  ===============  ================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('MODUL_')

Contraintes:

    .. code-block:: sql

        array_length(hv_array, 2)=2
