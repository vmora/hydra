land_use
--------



==========  ===================  ========  ================  ================
Attribut    Type                 Défaut    Contrainte        Clef Etrangère
==========  ===================  ========  ================  ================
id          serial                         primary key
geom        POLYGON                        ST_IsValid(geom)
land_use    hydra_land_use_type
==========  ===================  ========  ================  ================
