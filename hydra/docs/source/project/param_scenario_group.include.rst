param_scenario_group
--------------------



==========  =======  ========  =================  =============================================================
Attribut    Type     Défaut    Contrainte         Clef Etrangère
==========  =======  ========  =================  =============================================================
scenario    integer            on delete cascade  project.scenario(id) :ref:`🔗 <project-scenario>`
grp         integer            on delete cascade  project.scenario_group(id) :ref:`🔗 <project-scenario-group>`
==========  =======  ========  =================  =============================================================

Contraintes:

    .. code-block:: sql

        unique(scenario, grp)
