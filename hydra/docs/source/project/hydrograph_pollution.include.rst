hydrograph_pollution
--------------------



====================  ======  ========  ============  ================
Attribut              Type    Défaut    Contrainte    Clef Etrangère
====================  ======  ========  ============  ================
id                    serial            primary key
dry_flow_mes_mgl      real
dry_flow_dbo5_mgl     real
dry_flow_dco_mgl      real
dry_flow_ntk_mgl      real
runoff_mes_mgl        real
runoff_flow_dbo5_mgl  real
runoff_flow_dco_mgl   real
runoff_flow_ntk_mgl   real
====================  ======  ========  ============  ================
