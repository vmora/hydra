inflow_rerouting
----------------



===============  ======================  ==========  =================  =================================================
Attribut         Type                    Défaut      Contrainte         Clef Etrangère
===============  ======================  ==========  =================  =================================================
id               serial                              primary key
scenario         integer                             on delete cascade  project.scenario(id) :ref:`🔗 <project-scenario>`
model            varchar(24)
hydrograph_from  integer
hydrograph_to*   integer
option           hydra_rerouting_option  'fraction'
parameter        real                    0
===============  ======================  ==========  =================  =================================================

Contraintes:

    .. code-block:: sql

        unique(scenario, model, hydrograph_from, hydrograph_to)
