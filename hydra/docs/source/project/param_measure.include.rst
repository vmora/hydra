param_measure
-------------



==============  ============  ========  =================  =================================================
Attribut        Type          Défaut    Contrainte         Clef Etrangère
==============  ============  ========  =================  =================================================
id              serial                  primary key
param_scenario  integer                 on delete cascade  project.scenario(id) :ref:`🔗 <project-scenario>`
measure_file    varchar(256)
==============  ============  ========  =================  =================================================
