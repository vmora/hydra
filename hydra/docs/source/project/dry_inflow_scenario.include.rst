dry_inflow_scenario
-------------------



==========  ============  ========  ============  ================
Attribut    Type          Défaut    Contrainte    Clef Etrangère
==========  ============  ========  ============  ================
id          serial                  primary key
name        varchar(24)   calculé
comment*    varchar(256)
==========  ============  ========  ============  ================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('DRYSCN_')
