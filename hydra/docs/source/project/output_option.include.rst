output_option
-------------



============  ===========  ========  =================  =================================================
Attribut      Type         Défaut    Contrainte         Clef Etrangère
============  ===========  ========  =================  =================================================
id            serial                 primary key
scenario      integer                on delete cascade  project.scenario(id) :ref:`🔗 <project-scenario>`
model         varchar(24)
type_element  varchar(48)
element       varchar(24)
============  ===========  ========  =================  =================================================
