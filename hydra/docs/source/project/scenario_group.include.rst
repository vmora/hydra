scenario_group
--------------



==========  ===========  ========  ============  ================
Attribut    Type         Défaut    Contrainte    Clef Etrangère
==========  ===========  ========  ============  ================
id          serial                 primary key
name        varchar(24)  calculé   unique
==========  ===========  ========  ============  ================

Champs calculés:

    name

    .. code-block:: sql

        'GROUP_'||currval('project.scenario_group_id_seq')::varchar
