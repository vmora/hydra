hydrograph_variable_dry_flow
----------------------------



============================  =======  ========  ============  ========================================================================================
Attribut                      Type     Défaut    Contrainte    Clef Etrangère
============================  =======  ========  ============  ========================================================================================
id                            serial             primary key
sector                        integer                          project.dry_inflow_sector(id) :ref:`🔗 <project-dry-inflow-sector>`
distrib_coef                  real
lag_time_hr                   real     0
dry_inflow_hourly_modulation  integer                          project.dry_inflow_hourly_modulation(id) :ref:`🔗 <project-dry-inflow-hourly-modulation>`
============================  =======  ========  ============  ========================================================================================
