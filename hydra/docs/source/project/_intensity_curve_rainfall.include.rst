_intensity_curve_rainfall
-------------------------
type=4


===================  ============  ========  ============  ================
Attribut             Type          Défaut    Contrainte    Clef Etrangère
===================  ============  ========  ============  ================
t_mn_intensity_mmh*  real[100][2]
===================  ============  ========  ============  ================

Validité:

    .. code-block:: sql

        t_mn_intensity_mmh is not null
        array_length(t_mn_intensity_mmh, 1)<=100 and array_length(t_mn_intensity_mmh, 1)>=1 and array_length(t_mn_intensity_mmh, 2)=2

