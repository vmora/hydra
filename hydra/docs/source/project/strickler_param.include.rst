strickler_param
---------------



=================  ===========  ========  =================  =================================================
Attribut           Type         Défaut    Contrainte         Clef Etrangère
=================  ===========  ========  =================  =================================================
id                 serial                 primary key
scenario           integer                on delete cascade  project.scenario(id) :ref:`🔗 <project-scenario>`
model              varchar(24)
type_domain        varchar(24)            voir ci-dessous
element            varchar(24)
rkmin              real         0
rkmaj              real         0
pk1                real         0
pk2                real         0
unique(scenario,*  model,                 voir ci-dessous
=================  ===========  ========  =================  =================================================

Contraintes:

    .. code-block:: sql

        type_domain in ('reach', 'domain_2d', 'branch')

    .. code-block:: sql

        type_domain, element)
