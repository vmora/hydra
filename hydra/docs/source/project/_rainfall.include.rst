_rainfall
---------



=============  ===================  ========  ============  ================
Attribut       Type                 Défaut    Contrainte    Clef Etrangère
=============  ===================  ========  ============  ================
id             serial                         primary key
rainfall_type  hydra_rainfall_type
name           varchar(24)
comment*       varchar(256)
validity*      boolean
=============  ===================  ========  ============  ================

Contraintes:

    .. code-block:: sql

        unique(id, rainfall_type)
