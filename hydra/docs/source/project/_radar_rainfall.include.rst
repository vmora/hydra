_radar_rainfall
---------------
type=6


====================  ============  ========  ============  ================
Attribut              Type          Défaut    Contrainte    Clef Etrangère
====================  ============  ========  ============  ================
file                  varchar(256)
unit_correction_coef  real          1
====================  ============  ========  ============  ================

Validité:

    .. code-block:: sql

        file is not null

