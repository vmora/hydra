grp_model
---------



==========  =======  ========  =================  ========================================================
Attribut    Type     Défaut    Contrainte         Clef Etrangère
==========  =======  ========  =================  ========================================================
grp         integer            on delete cascade  project.grp(id) :ref:`🔗 <project-grp>`
model       integer                               project.model_config(id) :ref:`🔗 <project-model-config>`
==========  =======  ========  =================  ========================================================

Contraintes:

    .. code-block:: sql

        unique(grp, model)
