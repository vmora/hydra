rain_gage
---------



==========  ============  ========  ============  ================
Attribut    Type          Défaut    Contrainte    Clef Etrangère
==========  ============  ========  ============  ================
id          serial                  primary key
name        varchar(24)   calculé   unique
comment*    varchar(256)
geom        POINTZ
==========  ============  ========  ============  ================

Champs calculés:

    name

    .. code-block:: sql

        project.unique_name('RG_')
