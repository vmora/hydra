.. _constrain-lines:

Lignes de contraintes
#####################

Les lignes de contraintes sont matérialisées par des polylignes créées par l'utilisateur; leurs fonctions sont multiples :
  - délimitation des frontières extérieures des domaines 2D et 1D,
  - identification de lignes de contraintes pour la génération du maillage 2D, et du type de liaisons hydrauliques associées (i.e. reliant les mailles situées de part et d'autre de cette ligne de contrainte),
  - génération de liaisons en lot.

Les méthodes de :ref:`génération du maillage <meshing>` et des :ref:`liaisons par lot <generate-links>` sont décrites dans des chapitres spécifiques. Seuls les paramètres à renseigner par l'utilisateur sont précisés ici.

Paramètres associés aux lignes de contraintes
=============================================
**Type de ligne**
  - Frontière extérieure de domaine (1D ou 2D)
  - Ligne de contrainte pour maillage 2D

**Liaisons attaché à cette ligne**
  - Type de liaison :

    - 2D
    - Weir
    - Strickler
    - Fuse spillway
    - Porous
    - River to 2D

  - Paramètres associés à ces liaisons (cf. tables spécifiques à ces éléments).
  - Critère de distance autour de la ligne de contrainte pour la recherche des cotes topographiques nécessaires à la génération des liaisons.

**Options maillage 2D**
  - taille des mailles
  - distance d'influence autour de la ligne sur la taille des mailles
  - Option snap (imposée pour les lignes frontières)

MCD
====

Le MCD doit être modifié pour intégrer ce nouvel élément :

  - Ajout d'une table constraint_line

========================  ===========  =========  =====================  ======================================================
Attribut                  Type         Défaut     Contrainte             Clef Etrangère
========================  ===========  =========  =====================  ======================================================
id                        serial                  primary key
line_type                 integer                                        hydra._line_type(id)
link_type                 integer                                        hydra._volatile_link_type(id)
2D_mesh_size              real
2D_mesh_width_adjustment  real         mesh_size
2d_mesh_snap              boolean                 1 si _line_type(id)=2
========================  ===========  =========  =====================  ======================================================

  - Ajout d'une table hydra._volatile_link_type

====  ==============  =============  ==========================
id    abbreviation    li_hydra_no    name
====  ==============  =============  ==========================
1     LDEV            3              weir
2     LPAV            5              mesh_2d
3     LSTK            1              strickler
4     LPOR            2              porous
5     LRC             6              river_to_2d
6     LDVF            7              fuse_spillway
====  ==============  =============  ==========================

  - Ajout d'une table hydra._line_type

====  ===================
id    name 
====  =================== 
1     2d_mesh_constrainst
2     external_border
====  ===================

	
  - Ajout de tables spécifiques à chaque type de liaisons associées permettant d'enregistrer les paramètres correspondant à chaque ligne de contrainte :

. mesh_2D_constraint_link

===========  ==========  ========  ====================  ================
Attribut     Type        Défaut    Contrainte            Clef Etrangère
===========  ==========  ========  ====================  ================
comput_mode  boolean     'f'
===========  ==========  ========  ====================  ================
	
. weir_constraint_link

==========  ======  ============  ============  ================
Attribut    Type    Défaut        Contrainte    Clef Etrangère
==========  ======  ============  ============  ================
cc          real    .6
v_max_cms   real    .5
==========  ======  ============  ============  ================

. strickler_constraint_link

==========  ======  ========  ============  ================
Attribut    Type    Défaut    Contrainte    Clef Etrangère
==========  ======  ========  ============  ================
rk          real    10
==========  ======  ========  ============  ================
	
. fuse_spillway_constraint_link

==============  ===========  ========  ========================  =============================================================================
Attribut        Type         Défaut    Contrainte                Clef Etrangère
==============  ===========  ========  ========================  =============================================================================
    cc              real         .6
break_mode      integer                                          hydra._fuse_spillway_break_mode(id) :ref:`🔗 <hydra-fuse-spillway-break-mode>`
z_break         real
t_break         real
grp*            integer                                          
dt_fracw_array  real[10][2]            cf. _fuse_spillway_link
==============  ===========  ========  ========================  =============================================================================
	
. porous_constraint_link

=============  ======  ========  ============  ================
Attribut       Type    Défaut    Contrainte    Clef Etrangère
=============  ======  ========  ============  ================
transmitivity  real    0
=============  ======  ========  ============  ================	
	
. river_to_2D_constraint_link

==========  ======  ========  ============  ================
Attribut    Type    Défaut    Contrainte    Clef Etrangère
==========  ======  ========  ============  ================
cc          real    .2
==========  ======  ========  ============  ================
	

Modalités de création des lignes de contraintes
===============================================

Différents boutons seront créés, pour chaque type de ligne de contrainte :
  - délimitation des frontières extérieures des domaines 2D et 1D,
  - lignes de contraintes pour la génération du maillage 2D, et du type de liaisons hydrauliques associées (i.e. reliant les mailles situées de part et d'autre de cette ligne de contrainte),
  - génération de liaisons en lot.


	
	
