Définition de la géométrie des biefs filaires 
#############################################

Objet 
=====
Un domaine filaire de cours d’eau est défini par son bief, c’est-à-dire une poyligne sur laquelle sont posés des nœuds de calcul.

Cette entité suffit en toute rigueur pour réaliser des calculs hydrauliques, mais elle reste incomplète pour cartographier des taches d’inondation associées à ces domaines filaires.

Il s'agit donc de définir les modalités :

  - d'extrapolation des cotes d'eau calculées sur chaque noeud de calcul (soit sur un point situé sur le bief matérialisant l'axe du lit mineur) sur le lit majeur,
  - d'interpolation de ces cotes d'eau entre deux noeuds de calcul.

Pour ce faire, Hydra s'appuie sur les objets PL1D, dont une descrition ainsi que les modalités de création sont précisées ci-après.

Les PL1D
========
Les PL1D sont des polylignes permettant de définir pour un noeud de calcul donné :
  - les lignes d'interpolation des cotes calculées sur les deux rives,
  - les limites du lit mineur et des lits majeur rives gauche et droite.

.. image:: PL1D_1.png
..

La délimitation des berges et des lignes de lit majeur à partir de profils transversaux est assurée par les polylignes de type PL1D qui peuvent compter 2, 3 ou 4 sommets selon ce que l’on souhaite modéliser. Le type de PL1D doit être identique tout le long d'un bief donné.

.. image:: PL1D_2.png
..

Les PL1D découpent des pseudo mailles qui sont générées comme suit pour les besoins des sorties cartographiques :

.. image:: PL1D_3.png
..


Création des PL1D
=================
Les principes de création des PL1D sont les suivants :
  - l'utilisateur :

    - crée un bief filaire 
    - dessine des lignes de contraintes matérialisant les limites extérieures du domaine filaire considéré sur chacune des deux rives
    - crée manuellement les PL1D structurants de chacun des biefs entre les lignes de contraintes

  - l'application génère automatiquement des PL1D interpolés entre les PL1D utilisateur pour affiner la discrétisation du domaine lors des restitutions carographiques.

*Quand créer ces PL1D générés? Bouton spécifique,  lors de la génération des calculs, lors de la modification ou l'ajout d'un PL1D par l'utilisateur?*
*Algoritme d'interpolation à créer. Voir si il ne sera pas nécessaire d'ajouter une polyligne matérialisant l'axe général de la vallée.*


**Création des PL1D utilisateurs**
La première étape consiste à dessiner le bief et les lignes de contraintes matérialisant les limites extérieures du domaine sur chacune des deux rives.

L'utilisateur dessine ensuite la polyligne matérialisant le PL1D (2, 3 ou 4 sommets); les extrémités doivent être extérieures aux lignes de contrainte matérialisant les frontières extérieures du domaine 1D.

L'application "ajuste" les extrémités de la polyligne sur les lignes de contrainte intersectées.

.. image:: PL1D_4.png
..

.. image:: PL1D_5.png
..

Un noeud est créé au point d'intersection entre les PL1D et le bief. Si un noeud existe à proximité (<1m) de ce point d'intersection, le PL1D est attaché à ce noeud.

Pour associer le PL1D à un ``noeud existant`` (notamment à l'extrémité des biefs), l'utilisateur sélectionne le noeud concerné puis crée le PL1D sur le même principe que précédemment.

*``MCD`` : Un flag permet d'identifier les PL1D "utilisateur" dans la table PL1D.*

*Création de PL1D intermédiaires*
L'applicatiuon génère des PL1D intermédiaires ``sur chaque noeud de calcul``; un flag permet d'identifier les PL1D "générés" dans la table PL1D.

.. image:: PL1D_6.png
..




