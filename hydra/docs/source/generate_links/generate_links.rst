.. _generate-links:

Génération des liaisons
#######################

Introduction
============
Deux types de liaisons peuvent être distingués :
- Celles qui peuvent être générées en lot : liaisons dont les caractéristiques sont uniquement liées à la topographie et à la géométrie des objets auxquels elles sont rattachées : liaisons de déversement sur des digues, berges ou infrastructures en remblai s’appuyant au moins sur une maille 2D ou reliant deux biefs fialaires,
- Celles dont les caractéristiques ne peuvent être déduites de la seule topographie : ouvrages de décharge, stations de relevage, … et liaisons de déversement entre  pour lesquelles les largeurs de déversement ne peuvent être déduites des caractéristiques des objets de modélisation qui les portent ou définies globalement par ailleurs.

La structure de la base de données actuelle impose que les liaisons binodales soient rattachées à deux nœuds de calcul pour être valides.
Ceci implique que la suppression d’un des nœuds attachés à une liaison entraine la suppression de cette liaison ; la localisation et les caractéristiques de cette liaison sont alors perdues.


Génération des liaisons en lot
==============================
**Principes généraux**
Les liaisons pouvant être générées en lot sont celles dont les caractéristiques sont uniquement liées à la topographie et à la géométrie des objets auxquels elles sont rattachées; elles concernent les types de liaisons suivants :
- 2D
- Overflow
- Strickler
- Porous

L’utilisateur défini une polyligne localisant la ligne de fracture, dénommée ``ligne de contrainte``. Lors de la génération de la liaison, l’application détermine les caractéristiques des liaisons par analyse des points hauts situés autour de la ligne de contrainte au droit de la liaison (cf. ci-après).
Le type de liaison généré est paramétré dans la ligne de contrainte.

**Liaisons 2D/2D**
Les domaines 2D sont délimités par un polygone à l’intérieur duquel le maillage est généré (domaine 2D); les modalités de génération du maillage ne sont pas détaillées ici.
Au droit des lignes de contraintes, situées à l’intérieur d’un domaine ou à la frontière ente deux domaines, l’application génère des liaisons sur le même principe que pour les mesh2D_link (cf. ci-après).
Ces liaisons sont créées automatiquement lors de la génération du maillage.

**Liaisons 2D/casiers**
Le principe est le même que pour les liaisons 2D/2D. La largeur de la liaison est déduite de la largeur de la maille au droit de son intersection avec la liaison considérée.
L’utilisateur sélectionne la ligne de contrainte considérée, et lance la génération des liaisons via un bouton dédié.

**Liaisons 1D/2D**
La difficulté réside dans l’orientation des liaisons générées, d’autant plus difficile à déterminer automatiquement que le domaine 2D est éloigné du lit mineur ou que le lit mineur présente une sinuosité marquée par rapport aux frontières du domaine 2D. L'application s'appuie alors sur les PL1D préalablement définis par l'utilisateur.
Les liaisons 1D/2D sont générées parallèlement aux PL1D après sélection de la ligne de contrainte et activation d’un bouton dédié.

.. image:: 1D-2D_1.png
..

**Liaisons 1D/2D, 1D immergé dans un maillage 2D**
On s’intéresse ici au cas d’un cours définit uniquement par son bief 1d. Le cours d’eau serpente dans le lit majeur d’un domaine 2D.
Le maillage est calculé simplement en partitionnant le bief filaire en tronçons de longueur dx. Le programme crée un noeud au centroide de chaque tronçon, recherche la maille contenant le nœud et génère une liaison de type Overflow comme suit :
.. image:: 1D-2D_2.png
..

**Liaisons 1D /1D**
La génération se fait sur le même principe que pour les liaisons 1D / 2D, la ligne de contrainte marquant la frontière entre les deux domaines 1D.
A la différence des liaisons 1D / 2D, l’application ne peut automatiquement définir la position des liaisons. L’utilisateur affecte donc à la ligne de contrainte un espacement moyen entre les liaisons (dx).

.. image:: 1D-1D_1.png
..

Les liaisons 1D/1D sent générées après sélection de la ligne de contrainte et activation d’un bouton dédié. Les PL1D ne présentant pas forcément la même orientation sur les deux biefs, l’orientation des liaisons est déterminée par une moyenne de ces deux orientations.

.. image:: 1D-1D_2.png
..

Création de liaisons individuelles
==================================
Les liaisons peuvent être créées manuellement via un bouton spécifique à chaque type de liaison disponible.
Les liaisons peuvent être créées avant la création des noeuds qui les porteront; elles devront cependant être toutes raccordées à un noeud lors de la génération du calcul (un contrôle de cohérence est prévu lors de la génération du modèle préalable au lancement du calcul).  
Cette relaxation de la contrainte de connectivité des liaison permet :
- de positionner des liaisons et de renseigner leurs caractéristiques avant que le schéma de modélisation soit totalement défini (lors du recensement des ouvrages sur le terrain ou de la préparation des levés topographiques par exemple),
- de conserver leurs caractéristiques si le noeud qui les porte devait être supprimé (actualisation du maillage 2D, modification de l'agancement de carrefours, ...).
Les liaisons non raccordées à un noeud pourront être mises en surbrillance.


Calcul des paramètres géométriques des liaisons
===============================================
Recherche des points hauts autour des lignes de contraintes
-----------------------------------------------------------
Lorsque la liaison générée s'appuie sur une ligne de contrainte, cette dernière est expoitée pour rechercher la cote de déversement de la liaison. L'application recherche les cotes altimétriques les plus hautes dans un buffer autour de la polyligne considérée dans un rayon défini par l'utilisateur lors du paramétrage de la ligne de contrainte; deux cas de figure se présentent :
- la donnée topographique est totalement définie dans le MNT; la recherche des cotes hautes se fait à partir du MNT, 
- des données topographiques vectorielles (semis de points) sont présentes dans le buffer; la recherche des cotes hautes se fait uniquement sur ces points.


Paramètres associés aux différentes liaions
-------------------------------------------
Ce chapitre précise les modalités de calcul des caractéristiques géométriques des liaisons lorsque celles-ci peuvent être définies (ou approchées) automatiquement, qu'elles soient géénrées en lot ou individuellement; il s'agit des laiisons suivantes :
    - 2D
    - Overflow
    - Strickler
    - Porous

Pour ce qui est des autres laisons, leurs paramètres ne peuvent être définis automatiquement à partir du MNT ou de semis de points (radier et ouverture de vannes ou de ponceaux de décharge, stations de pompage, ...). L'ensemble des paramètres est alors saisi directement par l'utilisateur via les interfaces dédiées ouvertes après création de l'élément.

La génération des liaisons par lot s'appuie automatiquement sur des ``lignes de contraintes`` permettant d'extraire les données topographiques nécessaires au calage altimétrique.
Les liaisons individuelles peuvent s'appuyer sur une ligne de contrainte (la ligne matérialisant la liaison coupe la ligne de contrainte considérée), mais ce n'est pas une obligation. Deux modes de calcul sont ainsi définis, suivant que l'on s'appuie ou non sur une ligne de contrainte.

Le tableau ci-après présente les différentes configurations de liaisons envisageables en fonction des éléments amont et aval qu'elles relient.
Pour chaque cas, se référer à la description correspondante ci-après.

+-------------------+--------------+---------------+--------------------+---------------+
|                   | Bief 1D  (1) | Maille 2D (2) | Casier ou Ilot (3) | Carrefour (4) |
+===================+==============+===============+====================+===============+
| Bief 1D  (1)      |    11        |     12        |        13          |     14        |
+-------------------+--------------+---------------+--------------------+---------------+
| Maille 2D (2)     |              |     22        |        23          |     24        |
+-------------------+--------------+---------------+--------------------+---------------+
| Casier ou Ilot (3)|              |               |        33          |     34        |
+-------------------+--------------+---------------+--------------------+---------------+
| Carrefour (4)     |              |               |                    |     44        |
+-------------------+--------------+---------------+--------------------+---------------+


**Type 11 : bief 1D - bief 1D**

.. image:: Link-11.png

*Commentaire :* dx est défini par la ligne de contrainte si générée en lot. Sinon, à renseigner manuellement par l'utilisateur ``avant`` le calcul des paramètres associés à la liaison.*

+--------------------+----------------------------------------------------------+
| Link type          | Geometric caracterisitcs                                 |
|                    +----------+-----------------------+-----------------------+
|                    | Param.   | Avec ligne contrainte | Sans ligne contrainte |
+====================+==========+=======================+=======================+
| mesh_2d_link       | z_invert | *-*                   | *-*                   |
|                    +----------+-----------------------+-----------------------+
|                    | width    | *-*                   | *-*                   |
+--------------------+----------------------------------+-----------------------+
| ``strickler_link`` | z_crest1 | min(Z{AB})            | *-*                   |
|                    +----------+-----------------------+-----------------------+
|                    | width1   | .1*dx                 | *-*                   |
|                    +----------+-----------------------+-----------------------+
|                    | z_crest2 | max(Z{AB})            | *-*                   |
|                    +----------+-----------------------+-----------------------+
|                    | width2   | dX                    | *-*                   |
|                    +----------+-----------------------+-----------------------+
|                    | length   | [ij]                  | *-*                   |
+--------------------+----------+-----------------------+-----------------------+
| porous_link        | z_invert | moy(Z{AB})            | *-*                   |
|                    +----------+-----------------------+-----------------------+
|                    | width    | dX                    | *-*                   |
+--------------------+----------+-----------------------+-----------------------+
| Overflow_link      | z_crest1 | min(Z{AB})            | *-*                   |
|                    +----------+-----------------------+-----------------------+
|                    | width1   | 0                     | *-*                   |
|                    +----------+-----------------------+-----------------------+
|                    | z_crest2 | moy(Z{AB})            | *-*                   |
|                    +----------+-----------------------+-----------------------+
|                    | width2   | dX                    | *-*                   |
+--------------------+----------+-----------------------+-----------------------+


**Type 12 : bief 1D - maille 2D (ou 21)**

.. image:: Link-12.png

*Commentaire :* 

+--------------------+--------------------------------------------------------------+
| Link type          | Geometric caracterisitcs                                     |
|                    +----------+-------------------------+-------------------------+
|                    | Param.   | Avec ligne contrainte   | Sans ligne contrainte   |
+====================+==========+=========================+=========================+
| mesh_2d_link       | z_invert | *-*                     | *-*                     |
|                    +----------+-------------------------+-------------------------+
|                    | width =  | *-*                     | *-*                     |
+--------------------+----------+-------------------------+-------------------------+
| strickler_link     | z_crest1 | max(zb(j); min(Z{A'B'}) | max(zb(j); min(Z{AB})   |
|                    +----------+-------------------------+-------------------------+
|                    | width1 = | .1*[AB]                 | .1*[AB]                 |
|                    +----------+-------------------------+-------------------------+
|                    | z_crest2 | max(zb(j); max(Z{A'B'}) | max(zb(j); max(Z{AB})   |
|                    +----------+-------------------------+-------------------------+
|                    | width2   | [AB]                    | [AB]                    |
|                    +----------+-------------------------+-------------------------+
|                    | length   | [ij]                    | [ij]                    |
+--------------------+----------+-------------------------+-------------------------+
| porous_link        | z_invert | min(zb(j); moy(Z{A'B'}) | min(zb(j); moy(Z{AB})   |
|                    +----------+-------------------------+-------------------------+
|                    | width =  | [AB]                    | [AB]                    |
+--------------------+----------+-------------------------+-------------------------+
| ``Overflow_link``  | z_crest1 | max(zb(j); min(Z{A'B'}) | max(zb(j); min(Z{AB})   |
|                    +----------+-------------------------+-------------------------+
|                    | width1   | 0                       | 0                       |
|                    +----------+-------------------------+-------------------------+
|                    | z_crest2 | max(zb(j); moy(Z{A'B'}) | max(zb(j); moy(Z{AB})   |
|                    +----------+-------------------------+-------------------------+
|                    | width2   | [AB]                    | [AB]                    |
+--------------------+----------+-------------------------+-------------------------+


**Type 13 : bief 1D - casier ou ilot (ou 31)**

.. image:: Link-13.png

*Commentaire :* la largeur de la liaison peut être définie à partir des sommets du polygone matérialisant le casier encadrant la liaison ou par l'utilisateur (dx). Option à choisir lors de la création de la liaison, avant le calcul de ses paramètres.

+--------------------+--------------------------------------------------------------+
| Link type          | Geometric caracterisitcs                                     |
|                    +----------+-------------------------+-------------------------+
|                    | Param.   | Avec ligne contrainte   | Sans ligne contrainte   |
+====================+==========+=========================+=========================+
| mesh_2d_link       | z_invert | *-*                     | *-*                     |
|                    +----------+-------------------------+-------------------------+
|                    | width =  | *-*                     | *-*                     |
+--------------------+----------+-------------------------+-------------------------+
| strickler_link     | z_crest1 | min(Z{A'B'})            | min(Z{AB})              |
|                    +----------+-------------------------+-------------------------+
|                    | width1 = | .1*[AB]                 | .1*[AB]                 |
|                    +----------+-------------------------+-------------------------+
|                    | z_crest2 |  max(Z{A'B'})           | max(Z{AB})              |
|                    +----------+-------------------------+-------------------------+
|                    | width2   | [AB] ou dx              | [AB] ou dx              |
|                    +----------+-------------------------+-------------------------+
|                    | length   | [ij]                    | [ij]                    |
+--------------------+----------+-------------------------+-------------------------+
| porous_link        | z_invert | moy(Z{A'B'})            | moy(Z{AB})              |
|                    +----------+-------------------------+-------------------------+
|                    | width =  | [AB] ou dx              | [AB] ou dx              |
+--------------------+----------+-------------------------+-------------------------+
| ``Overflow_link``  | z_crest1 | min(Z{A'B'})            | min(Z{AB})              |
|                    +----------+-------------------------+-------------------------+
|                    | width1   | 0                       | 0                       |
|                    +----------+-------------------------+-------------------------+
|                    | z_crest2 | max(zb(j); moy(Z{A'B'}) | max(zb(j); moy(Z{AB})   |
|                    +----------+-------------------------+-------------------------+
|                    | width2   | [AB] ou dx              | [AB] ou dx              |
+--------------------+----------+-------------------------+-------------------------+


*Nota : si la liaison est calculée sur une largeur dx, la position des points A, B, A' et B' est définie par l'application à dx/2 de part et d'autre du point d'intersection de la liaison et de la frontière du casier.*


**Type 14 : bief 1D - carrefour (ou 41)**

.. image:: Link-14.png

+--------------------+--------------------------------------------------------------------------+
| Link type          | Geometric caracterisitcs                                                 |
|                    +----------+-------------------------------+-------------------------------+
|                    | Param.   | Avec ligne contrainte         | Sans ligne contrainte         |
+====================+==========+===============================+===============================+
| mesh_2d_link       | z_invert | *-*                           | *-*                           |
|                    +----------+-------------------------------+-------------------------------+
|                    | width =  | *-*                           | *-*                           |
+--------------------+----------+-------------------------------+-------------------------------+
| strickler_link     | z_crest1 | max(z_ground(j); min(Z{A'B'}) | max(z_ground(j); min(Z{AB})   |
|                    +----------+-------------------------------+-------------------------------+
|                    | width1 = | .1*[A'B'] (=.1*dx/2)          | .1*[A'B'] (=.1*dx/2)          |
|                    +----------+-------------------------------+-------------------------------+
|                    | z_crest2 | max(z_ground(j); max(Z{A'B'}) | max(z_ground(j); max(Z{AB})   |
|                    +----------+-------------------------------+-------------------------------+
|                    | width2   | [A'B'] (=dx/2)                | [A'B'] (=dx/2)                |
|                    +----------+-------------------------------+-------------------------------+
|                    | length   | [ij]                          | [ij]                          |
+--------------------+----------+-------------------------------+-------------------------------+
| porous_link        | z_invert | min(z_ground(j); moy(Z{A'B'}) | min(z_ground(j); moy(Z{AB})   |
|                    +----------+-------------------------------+-------------------------------+
|                    | width =  | [A'B'] (=dx/2)                | [A'B'] (=dx/2)                |
+--------------------+----------+-------------------------------+-------------------------------+
| ``Overflow_link``  | z_crest1 | max(z_ground(j); min(Z{A'B'}) | max(z_ground(j); min(Z{AB})   |
|                    +----------+-------------------------------+-------------------------------+
|                    | width1   | 0                             | 0                             |
|                    +----------+-------------------------------+-------------------------------+
|                    | z_crest2 | max(z_ground(j); moy(Z{A'B'}) | max(z_ground(j); moy(Z{AB})   |
|                    +----------+-------------------------------+-------------------------------+
|                    | width2   | [A'B'] (=dx/2)                | [A'B'] (=dx/2)                |
+--------------------+----------+-------------------------------+-------------------------------+


*Nota : dx correspond à la longueur des deux rues attachées au carrefour. Si une seule rue est attaché, ne considérer que la longeuur de cette rue. A' et B' correspondent à la projection du point milieu de chacune de ces deux rues sur la ligne de contrainte.*


**Type 22 : maille 2D - maille 2D**

.. image:: Link-22.png

+--------------------+--------------------------------------------------------------------------------------+
| Link type          | Geometric caracterisitcs                                                             |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | Param.   | Avec ligne contrainte               | Sans ligne contrainte               |
+====================+==========+=====================================+=====================================+
| ``mesh_2d_link``   | z_invert | min(Z{AB})                          | min(Z{AB})                          |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width =  | [AB]                                | [AB]                                |
+--------------------+----------+-------------------------------------+-------------------------------------+
| strickler_link     | z_crest1 | *-*                                 | *-*                                 |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width1 = | *-*                                 | *-*                                 |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | z_crest2 | *-*                                 | *-*                                 |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width2   | *-*                                 | *-*                                 |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | length   | *-*                                 | *-*                                 |
+--------------------+----------+-------------------------------------+-------------------------------------+
| porous_link        | z_invert | max(zb(i); zb(j))                   | max(zb(i); zb(j))                   |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width =  | [AB]                                | [AB]                                |
+--------------------+----------+-------------------------------------+-------------------------------------+
| Overflow_link      | z_crest1 | max(max(zb(i); zb(j)); min(Z{A'B'}) | max(max(zb(i); zb(j)); min(Z{AB})   |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width1   | 0                                   | 0                                   |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | z_crest2 | max(max(zb(i); zb(j)); moy(Z{A'B'}) | max(max(zb(i); zb(j)); moy(Z{AB})   |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width2   | [AB]                                | [AB]                                |
+--------------------+----------+-------------------------------------+-------------------------------------+


*Nota : la ligne de contrainte ne suit pas forcément les limites des mailles.*


**Type 23 : maille 2D - casier ou ilot (ou 32)**

.. image:: Link-23.png

+--------------------+--------------------------------------------------------------------------------------+
| Link type          | Geometric caracterisitcs                                                             |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | Param.   | Avec ligne contrainte               | Sans ligne contrainte               |
+====================+==========+=====================================+=====================================+
| mesh_2d_link       | z_invert | *-*                                 | *-*                                 |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width =  | *-*                                 | *-*                                 |
+--------------------+----------+-------------------------------------+-------------------------------------+
| strickler_link     | z_crest1 | zb(i)                               | zb(i)                               |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width1 = | .1*[AB]                             | .1*[AB]                             |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | z_crest2 | max(zb(i); max(Z{A'B'})             | max(zb(i); max(Z{A'B'})             |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width2   | [AB]                                | [AB]                                |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | length   | [ij]                                | [ij]                                |
+--------------------+----------+-------------------------------------+-------------------------------------+
| porous_link        | z_invert | zb(i)                               | zb(i)                               |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width =  | [AB]                                | [AB]                                |
+--------------------+----------+-------------------------------------+-------------------------------------+
| ``Overflow_link``  | z_crest1 | zb(i)                               | zb(i)                               |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width1 = | 0                                   | 0                                   |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | z_crest2 | max(zb(i); moy(Z{A'B'})             | max(zb(i); max(Z{AB})               |
|                    +----------+-------------------------------------+-------------------------------------+
|                    | width2   | [AB]                                | [AB]                                |
+--------------------+----------+-------------------------------------+-------------------------------------+


*Nota : pour la génération en lots, les sommets des mailles 2D doivent ête fusionnés avec ceux de la ligne de contraintes.*


**Type 24 : maille 2D - carrefour (ou 42)**

.. image:: Link-24.png

+--------------------+--------------------------------------------------------------------------------------------------+
| Link type          | Geometric caracterisitcs                                                                         |
|                    +----------+--------------------------------------------+------------------------------------------+
|                    | Param.   | Avec ligne contrainte                      | Sans ligne contrainte                    |
+====================+==========+============================================+==========================================+
| mesh_2d_link       | z_invert | *-*                                        | *-*                                      |
|                    +----------+--------------------------------------------+------------------------------------------+
|                    | width =  | *-*                                        | *-*                                      |
+--------------------+----------+--------------------------------------------+------------------------------------------+
| strickler_link     | z_crest1 | max(max(zb(i); z_ground(j)); min(Z{A'B'})) | max(max(zb(i); z_ground(j)); min(Z{AB})) |
|                    +----------+--------------------------------------------+------------------------------------------+
|                    | width1 = | .1*[AB]                                    | .1*[AB]                                  |
|                    +----------+--------------------------------------------+------------------------------------------+
|                    | z_crest2 | max(max(zb(i); z_ground(j)); max(Z{A'B'})) | max(max(zb(i); z_ground(j)); max(Z{AB})) |
|                    +----------+--------------------------------------------+------------------------------------------+
|                    | width2   | [AB]                                       | [AB]                                     |
|                    +----------+--------------------------------------------+------------------------------------------+
|                    | length   | [ij]                                       | [ij]                                     |
+--------------------+----------+--------------------------------------------+------------------------------------------+
| porous_link        | z_invert | zb(i)                                      | zb(i)                                    |
|                    +----------+--------------------------------------------+------------------------------------------+
|                    | width =  | [AB]                                       | [AB]                                     |
+--------------------+----------+--------------------------------------------+------------------------------------------+
| ``Overflow_link``  | z_crest1 | max(max(zb(i); z_ground(j)); min(Z{A'B'})) | max(max(zb(i); z_ground(j)); min(Z{AB})) |
|                    +----------+--------------------------------------------+------------------------------------------+
|                    | width1 = | 0                                          | 0                                        |
|                    +----------+--------------------------------------------+------------------------------------------+
|                    | z_crest2 | max(max(zb(i); z_ground(j)); moy(Z{A'B'})) | max(max(zb(i); z_ground(j)); max(Z{AB})) |
|                    +----------+--------------------------------------------+------------------------------------------+
|                    | width2   | [AB]                                       | [AB]                                     |
+--------------------+----------+--------------------------------------------+------------------------------------------+


**Type 33 : casier ou ilot - casier ou ilot**

.. image:: Link-33.png

*Commentaire :* la largeur de la liaison peut être définie à partir de la longueur commune aux deux casiers (sommets fusionnés) ou par l'utilisateur (dx). Option à choisir lors de la création de la liaison.
Si les sommets des casiers ne sont pas fusionnés, la présence d'une ligne de contrainte est exigée.

+--------------------+--------------------------------------------------------------+
| Link type          | Geometric caracterisitcs                                     |
|                    +----------+-------------------------+-------------------------+
|                    | Param.   | Avec ligne contrainte   | Sans ligne contrainte   |
+====================+==========+=========================+=========================+
| mesh_2d_link       | z_invert | *-*                     | *-*                     |
|                    +----------+-------------------------+-------------------------+
|                    | width =  | *-*                     | *-*                     |
+--------------------+----------+-------------------------+-------------------------+
| strickler_link     | z_crest1 | min(Z{A'B'})            | min(Z{AB})              |
|                    +----------+-------------------------+-------------------------+
|                    | width1 = | .1*[AB]                 | .1*[AB]                 |
|                    +----------+-------------------------+-------------------------+
|                    | z_crest2 | max(Z{A'B'})            | max(Z{AB})              |
|                    +----------+-------------------------+-------------------------+
|                    | width2   | [AB] ou dx              | [AB] ou dx              |
|                    +----------+-------------------------+-------------------------+
|                    | length   | [ij]                    | [ij]                    |
+--------------------+----------+-------------------------+-------------------------+
| porous_link        | z_invert | moy(Z{A'B'})            | moy(Z{AB})              |
|                    +----------+-------------------------+-------------------------+
|                    | width =  | [AB] ou dx              | [AB] ou dx              |
+--------------------+----------+-------------------------+-------------------------+
| ``Overflow_link``  | z_crest1 | min(Z{A'B'})            | min(Z{AB})              |
|                    +----------+-------------------------+-------------------------+
|                    | width1 = | 0                       | 0                       |
|                    +----------+-------------------------+-------------------------+
|                    | z_crest2 | moy(Z{A'B'})            | max(Z{AB})              |
|                    +----------+-------------------------+-------------------------+
|                    | width2   | [AB] ou dx              | [AB] ou dx              |
+--------------------+----------+-------------------------+-------------------------+


*Nota : si la liaison est calculée sur une largeur dx, la position des points A' et B' est définie par l'application à dx/2 de part et d'autre du point d'intersection de la liaison et de la ligne de contrainte.*


**Type 34 : casier ou ilot - carrefour (ou 43)**

.. image:: Link-34.png

+--------------------+--------------------------------------------------------------------------+
| Link type          | Geometric caracterisitcs                                                 |
|                    +----------+--------------------------------+------------------------------+
|                    | Param.   | Avec ligne contrainte          | Sans ligne contrainte        |
+====================+==========+================================+==============================+
| mesh_2d_link       | z_invert | *-*                            | *-*                          |
|                    +----------+--------------------------------+------------------------------+
|                    | width =  | *-*                            | *-*                          |
+--------------------+----------+--------------------------------+------------------------------+
| strickler_link     | z_crest1 | max(z_ground(j); min(Z{A'B'})) | max(z_ground(j); min(Z{AB})) |
|                    +----------+--------------------------------+------------------------------+
|                    | width1 = | .1*dx                          | .1*dx                        |
|                    +----------+--------------------------------+------------------------------+
|                    | z_crest2 | max(z_ground(j); max(Z{A'B'})) | max(z_ground(j); max(Z{AB})) |
|                    +----------+--------------------------------+------------------------------+
|                    | width2   | dx                             | dx                           |
|                    +----------+--------------------------------+------------------------------+
|                    | length   | [ij]                           | [ij]                         |
+--------------------+----------+--------------------------------+------------------------------+
| porous_link        | z_invert | moy(Z{A'B'})                   | moy(Z{AB})                   |
|                    +----------+--------------------------------+------------------------------+
|                    | width =  | dx                             | dx                           |
+--------------------+----------+--------------------------------+------------------------------+
| ``Overflow_link``  | z_crest1 | max(z_ground(j); min(Z{A'B'})) | max(z_ground(j); min(Z{AB})) |
|                    +----------+--------------------------------+------------------------------+
|                    | width1 = | 0                              | 0                            |
|                    +----------+--------------------------------+------------------------------+
|                    | z_crest2 | max(z_ground(j); moy(Z{A'B'})) | max(z_ground(j); max(Z{AB})) |
|                    +----------+--------------------------------+------------------------------+
|                    | width2   | dx                             | dx                           |
+--------------------+----------+--------------------------------+------------------------------+


*Nota : dx correspond à la longueur des deux rues attachées au carrefour. Si une seule rue est attaché, ne considérer que la longeuur de cette rue. A' et B' correspondent à la projection du point milieu de chacune de ces deux rues sur la ligne de contrainte.*
*En l'absence de lignes de contraintes, les cotes sont recherchées sur la frontière du casier délimitée par les points A et B correspondant à la projection de dx sur cette frontière.*


**Type 44 : casier ou ilot - casier ou ilot**

.. image:: Link-34.png

+--------------------+--------------------------------------------------------------------------------------------------------------+
| Link type          | Geometric caracterisitcs                                                                                     |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | Param.   | Avec ligne contrainte                            | Sans ligne contrainte                          |
+====================+==========+==================================================+================================================+
| mesh_2d_link       | z_invert | *-*                                              | *-*                                            |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | width =  | *-*                                              | *-*                                            |
+--------------------+----------+--------------------------------------------------+------------------------------------------------+
|  Overflow_link     | z_invert | max(z_ground(i); z_ground(j))                    | max(z_ground(i); z_ground(j))                  |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | z_weir   | max(max(z_ground(i); z_ground(j)); moy(Z{A'B'})) | max(z_ground(i); z_ground(j))                  |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | width    | dx                                               | dx                                             |
+--------------------+----------+--------------------------------------------------+------------------------------------------------+
| strickler_link     | z_crest1 | max(max(z_ground(i); z_ground(j)); min(Z{A'B'})) | max(z_ground(i); z_ground(j))                  |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | width1 = | .1*dx                                            | dx                                             |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | z_crest2 | max(max(z_ground(i); z_ground(j)); max(Z{A'B'})) | max(z_ground(i); z_ground(j))                  |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | width2   | dx                                               | dx                                             |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | length   | [ij]                                             | [ij]                                           |
+--------------------+----------+--------------------------------------------------+------------------------------------------------+
| porous_link        | z_invert | moy(Z{A'B'})                                     | moy(Z{AB})                                     |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | width =  | dx                                               | dx                                             |
+--------------------+----------+--------------------------------------------------+------------------------------------------------+
| ``Overflow_link``  | z_crest1 | max(max(z_ground(i); z_ground(j)); min(Z{A'B'})) | max(z_ground(i); z_ground(j))                  |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | width1 = | 0                                                | 0                                              |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | z_crest2 | max(max(z_ground(i); z_ground(j)); moy(Z{A'B'})) | max(z_ground(i); z_ground(j))                  |
|                    +----------+--------------------------------------------------+------------------------------------------------+
|                    | width2   | dx                                               | dx                                             |
+--------------------+----------+--------------------------------------------------+------------------------------------------------+


*Nota : dx correspond à la plus petite des longueurs cumulées des deux rues attachées à chacun des deux carrefours. Si une seule rue est attachée à l'un des deux carrefours, ne considérer que la longeuur de cette rue dans le calcul. A' et B' correspondent à la projection du point milieu de chacune des deux rues présentant la plus petite des longueurs cumulées sur la ligne de contrainte.*


Gestion des liaisons lors de la suppression des noeuds qui les portent
======================================================================
Il s'agit ici de préciser le comportement de l'application quand aux liaisons binodales lors de la suppression d'un ou des noeuds qui la portent. Deux cas de figure sont envisageables :
- suppression en cascade de tous les éléments attachés au noeud supprimé,
- conservation des éléments, qui ne sont alors attachés à aucun noeud ou un seul noeud.

Il est proposé :
- de supprimer en cascade les éléments générés en lot (automatiquement),
- de conserver les éléments créés manuellement.

Pour ce faire, une table recense les éléments générés en lot.
 
 **Cas particulier des liaisons 1D/2D**
 La génération automatique des liaisons 1D/2D entraine la création de nombreux noeuds sur la branche filaire concernée. Afin de ne pas multiplier inutilement ces noeuds, ces derniers seront supprimés lors de la suppression des laisisons 1D/2D qu'ils portent si ils ne portent pas d'autres éléments (singularité, PT, PL1D ou liaisons).
 
