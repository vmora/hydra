Data Model
==========

This is an overview of the data model used by hyra. For a complete understanding of the implementation, please refer to :doc:`model/index`
