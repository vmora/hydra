Analyses cartographiques résultats rivière et assainissement 
############################################################

Objet 
=====
Différents types de sorties cartograpiques sont nécessaires pour exploiter les résultats de calcul dan l'inerface :

- En assainissement :

  - Identification des regards sur lesquels un débordement de surface est calculé (cote d'eau supérieure à la cote TN du regard)

  - Identification des tronçons de collecteurs en charge. Un collecteur peut être en charge sur toute sa longueur ou partiellement.

- En rivière :

  - Analyses thématiques sur les hauteurs d'eau aux noeuds de calcul : mailles 2D, casiers, profils en travers 1D, rues, ilots.

  - Restitution continue des hauteurs d'eau, par croisement d'une lignes d'eau interpolée entre les noeuds de calcul (triangulation) :

    - à l'échelle des objets de modélisation : les cotes TN sont extraites des inforamtions contenues dans les objets de modélisation (cote moyenne des mailles, ...)

    - à une échelle plus fine, par croisement de la ligne d'eau avec le MNT.

  - courbes iso-cotes d'eau

  - Champs de vitesses

  - Croisements hauteurs et vitesses (alea) suivant une grille définie par l'utilisateur

  - Comparaison de différents scénarios de calcul : écarts entre les hauteurs d'eau triangulées ou les vitesses

Ces éléments doivent également pouvoir ête exportés (shape) pour établir des cartographies spécifiques.

Les méthodes de production et d'export de ces analyses ne sont à ce jour pas toutes arrêtées :

- certaines analyses sont vectorielles (assainissement, hauteurs d'eau aux noeuds de calcul, champs de vitesses, courbes iso-cotes d'eau), d'autres peuvent être faites et exportées sous forme vectorielles ou raster (iso-hauteurs d'eau, isi-vitesses)

- Pour ce qui est des sorties vectorielles, plusieurs solutions sont envisageables :

  - analyses thématiques sur les objets de modélisation (outils QGIS), via l'ajout de champs supplémentaires stockant les résultats au pas de temps appelé par l'utilisateur (ou valeur max). L'application devant permettre de restituer des animations dynamique, ces champs doivent être actualisés à chaque pas de temps ou contenir la totalité des résultats disponibles.

  - création de tables temporaires, agrégeant le cas échéant les différents éléments nécessaires à l'nalyse ( mailles 2D, casiers, profils en travers 1D, rues, ilots); les résultats peuvent alors également être stockés dans ces tables.

 
Interface
=========

Les analyses analyses cartographiques sont paramétrées dans un panneau spécifique docké sur le gestionnaire de couches. Ce panneau permet :
  
- de sélectionner et paramétrer les analyses souhaitées

- d'exporter ces analyses (shape)


Assainissement
==============

- Débordements sur chaussée : mise en surbrillance des regards pour lesquels la ligne d'eau est au dessus de manhole(z_ground)

- Mise en charge des collecteurs : mise en surbrillance des tronçons en charge ligne d'eau supérieure à z_invert_up et / ou à z_invert_down :

  - soit couleurs spécifiques si totalement en charge ou en charge par l'amont ou en charge par l'aval --> possibilité d'analyse thématique sur l'objet pipe_link

  - soit analyse plus détaillée, identifiant la part du collecteur en charge --> nécessite de créer de nouveaux objets (sous partie des collecteurs partiellement en charge)

Rivière
=======

Les analyses sur les hauteurs d'eau s'appuient pour certaines d'entre elles sur la ``ligne d'eau triangulée`` par le programme Fortran ``triangulZ.exe`` (évolution du programme actuellement utilisé pour le traitement cartographique des résultats d'hydrariv crgenG.exe.

Le programme traingulZ.exe est lancé automatiquement par défaut à l'issue du calcul; une option permet de ne pas le lancer (gestionnaire de scénario).

Ce programme nécessite en entrée, en compléments des résultats de calcul :

- d'un ``fichier de géométrie``, remplaçant les fichiers actuellement utilisés par crgenG.exe SF1, SF2 et SF3; ce fichier est généré depuis la base de données. Le programme s'arrêtera si aucun élément n'est contenu dans ce fichier (cas de modèles d'assainissement),

- d'un fichier délimitant le ``contour`` sur lequel est réalisé le traitement. Il est généré automatiquement à partir de ``l'enveloppe convexe`` de l'ensemble des objets de modélisation exploités par triangulZ.exe : mailles 2D, casiers, PL1D, rues, ilots.

- ``Hauteurs d'eau``
    
  - ``Résultats bruts`` : l'analyse est effectuée sur les objets de modélisation (mailles 2D, casiers, PL1D, rues, ilots); la hauteur d'eau est calculée par différence entre la cote d'eau calculée et la cote de fond associée à l'objet

    - Cas des objets de type Node :

        - elem_2d_node : zb --> hauteur d'eau appliquée au contour de chaque elem_2d_node

        - storage_node : zs_array[1][1] --> hauteur d'eau appliquée au contour de chaque storage_node
        
        - urban_storage_node : zs_array[1][1] --> hauteur d'eau appliquée au contour de chaque urban_storage_node

    - Cas des autres objets : il s'agit d'objet ne présentant pas de contours surfaciques (lignes ou polylignes), pour lesquels la restitution d'une hauteur moyenne sur chacun d'entre eux est insuffisante (variation des hauteurs d'eau le long de l'onjet). 

        - river_cross_section_pl1d : la hauteur d'eau varie en fonction de la géométrie de la section,
        
        - street_link : la hauteur d'eau varie entre l'amont et l'aval (pente de la ligne d'eau différente de celle de la rue).
    
        Il apparait ici nécessaire de produire une ``couche géométrique`` consitutée d'un ``agrégat`` des objets de modélisations, sur lesquels pourront être réalisées des analyses thématiques, en lien avec les cotes d'eau calculées :

        - mailles 2D, casiers, ilots
        
        - éléments surfaciques créés à partir des:

            - street_link : rectangle de longueur égale à length et de largeure égale à width. L'objet peut être découpé en sous éléments de taille fixe (10% de la longueur totale par exemple), auxquels sont affectées des cotes de fond moyennes, 

            - river_cross_section_pl1d : rectangle de largeur fixe (2m par exemple). L'objet peut être découpé en sous éléments de taille fixe (10% de la longueur totale de chaque "lit",mineur, majeur rivve gauche et majeur rive droite,par exemple), auxquels sont affectées des cotes de fond moyennes. Celà nécessite de disposer de la section interpollée au droit du noeud portant le PL1D.
    
    Les ``exports`` peuvent se faire à partir de cette couche géométrique (Shape).
    
    - ``Iso-hauteurs`` : les iso-hauteurs sont restituées à partir de la ``ligne d'eau triangulée`` restituée par le programme ``triangulZ.exe``

        - ``Simplifiées`` : sorites brutes du programme triangulZ.exe, affichés via ``mesh_layer``. A priori pas d'export pour cette sortie, principalement utile pour une analyse rapide des résultats.

        - ``Détaillées`` : croisement de la ligne d'eau triangulée avec le MNT. Ces analyses peuvent être réalisées à partir de raster ou de vecteurs; à définir / objectif de flouiditré des animations dynamiques.
    
    Les ``exports`` doivent pouvoir être faits sous forme de ``raster et de vecteurs`` (polygones iso-coutours "découpés").

- ``Iso-cotes`` : polylignes iso-cotes de la ligne d'eau.

    Les ``exports`` sont des polylignes. 


- ``Vitesses``
    - ``Champs de vitesses`` :` vecteurs vitesses au centroïde des éléments de modélisation (mailles 2D, casiers, PL1D, rues, ilots). Ils peuvent être créés par le biais de symboles "fléches" et d'une analyse thématique sur :

        - l'angle de la vitesse (cf. sorties de calcul),

        - L'intensité de la vitesse (taille du symbole et couleurs)
        
    Les ``exports`` peuvent se faire à partir de cette couche géométrique (Shape).
    
    - ``Iso-vitesses`` : générées par le programme Fortran ``triangulV.exe``. Les résultats sont affichés via ``mesh_layer``.
    
    Les ``exports`` doivent pouvoir être faits sous forme de ``raster et de vecteurs`` (polygones iso-coutours "découpés").

- ``Liaisons actives`` : mise en surbrillance des liaisons en eau au pas de temps sélectionné.
    
    Les ``exports`` devront intégrer l'ensemble des liaisons; il peut être là aussi envisageable de passer par une table d'agrégats (géométrie de l'ensemble des liaisons).

- ``Aleas``
