Formation sql spatial
#####################


Premiers pas en sql
===================

Le SQL (Structured Query Language: langage de requête structurée) est un langage servant à exploiter des bases de données relationnelles.

.. image:: client_serveur.svg

Le SQL est envoyé au serveur par un client pour ajouter, modifier ou récupérer des données. Parmis les clients de PostgreSQL on trouve:

pgadmin3
  interface graphique, utile notament pour la présentation des plans de requêtes

psql
  ligne de commande, léger

QGIS
  pour les couches postgis, mais aussi pour des commandes SQL grace au plugin DB-Manager

On notera avant de commencer QuE SQl n'esT Pas sensiBLE à la CasE. On se restreindra donc ici à l'utilisation de minuscules même si traditionnellement les mots clefs du SQL sont écrits en majuscule.

Documentation
-------------

La documentation de référence pour PostgreSQL se trouve `ici <http://www.postgresql.org/docs/9.1/static/reference.html>`_ (google: postgresql reference)

Les fonctions spatiales fournies par PostGis sont quand a elle documentée `là <http://postgis.net/docs/reference.html>`_ (google: postgis reference)


Schémas, tables et champs
-------------------------

Afin de structurer nos base de donnée, on regroupera le tables dans des schémas. On évitera ainsi de polluer le schéma ``public`` qui est le schéma par défaut. 

Pour créer un schéma:

.. code-block:: sql

    create schema drh;

On notera au passage la présence d'un **point-virgule** ``;`` à la fin de l'instruction.

Une table est un ensemble **non-ordonné** d'enregistrement ayant la même structures, c'est à dire les même champs, par exemple tous les employés ont un ``nom``, un ``prenom`` et un salaire, la DRH pourra donc "ranger" dans la table ``employe`` des enregistrements contenant chacun ces champs.

Pour créer la table ``employe`` dans le schéma ``drh`` et définir sa structure (ses champs):

.. code-block:: sql

    create table drh.employe(
        nom varchar,
        prenom varchar,
        salaire integer
    );

Pour **ajouter** les employé Pierre Dupont et André Dubois à la table, on utilise un requête ``insert``:

.. code-block:: sql

    insert into drh.employe(prenom, nom) values('Pierre', 'Dupont');

    insert into drh.employe(prenom, nom) values('André', 'Dubois');

Pour **lister** les données dans la tables, on utilise une requête ``select``:

.. code-block:: sql

    select nom, prenom, salaire from drh.employe;

On notera au passage que nos employé n'ont pas de salaire (puisque nous ne l'avons pas spécifié lors de leur insertion). Lorsque la valeur d'un champ est absente, elle a une valeur spéciale ``null``.

Remarquons aussi que rien ne nous empêche de rajouter un autre Pierre Dupont, qui sera alors difficilement différentiable du premier:

.. code-block:: sql

    insert into drh.employe(prenom, nom) values('Pierre', 'Dupont');

    select nom, prenom, salaire from drh.employe;


Contraintes
-----------

Pour éviter que les utilisateurs de notre base de donnée n'entrent des données erronées, ou prêtant à confusion, on rajoute des **contraintes** à notre table. Par exemple si l'on veut interdire la possibilité qu'un employé soit sans salaire, il faut modifier la table avec une requête ```alter``:

.. code-block:: sql

    alter table drh.employe alter salaire set not null;

Ce qui sera refusé tant que les données ne seront pas valide, ce qui est le cas ici. Il nous faut donc **modifier les données** avec une requête ``update``:

.. code-block:: sql

    update drh.employe set salaire = 0;

Cette requête met à zéro le salaire de tous les employés. Attention **zéro n'est pas null**.

On peut vérifier nos donnés:

.. code-block:: sql

    select * from drh.employe

Le ``*`` est utile lorsqu'on veut lister tous les champs disponible (à éviter cependant lorsqu'on utilise la base à partir d'un programme).

Les salaires n'étant plus null, on peut rajouter la contrainte:

.. code-block:: sql

    alter table drh.employe alter salaire set not null;

On peut tester son efficacité en tentant:

.. code-block:: sql

    update drh.employe set salaire = NULL;

Qui sera refusé par PostgreSQL avec le message d'erreur approprié ``ERROR:  null value in column "salaire" violates not-null constraint``

L'ajout de contraintes à postériori est fastidieuse, on aurait évité cette situation inconfortable en créant les contraintes au moment de la création de la table ``employe``.

.. code-block:: sql

    drop table drh.employe;

    create table drh.employe(
        nom varchar not null,
        prenom varchar not null,
        salaire integer not null check (salaire >=0),
        unique(nom, prenom) -- évite d'avoir deux employés indifférentiables l'un de l'autre
    );

    insert into drh.employe(prenom, nom, salaire) values('Pierre', 'Dupont', 50000);

    insert into drh.employe(prenom, nom, salaire) values('André', 'Dubois', 60000);

    insert into drh.employe(prenom, nom, salaire) values('Alain', 'Dubois', 25000);

    insert into drh.employe(prenom) values('Gérard'); -- échoue

    insert into drh.employe(prenom, nom, salaire) values('Odette', 'Déficit', -60000); -- échoue

    insert into drh.employe(prenom, nom, salaire) values('André', 'Dubois', 60000); -- échoue

Notons:
  - la façon de supprimer une table (et toutes ses précieuses données)
  - la présence de la contrainte d'unicité qui assure qu'un même couple ``(nom, prenom)`` n'est présent qu'une seule fois dans la table,
  - la vérification (``check``)que les salaires ne sont pas négatifs,
  - une syntaxe pour les **commentaires** (partent d'un ``--`` et vont jusqu'au bout de la ligne).

Clefs étrangères et jointures
-----------------------------

Supposons que chaque employés est assigné à un département. Pour modéliser ce type de relation dans une BDDR, on utilise un **clef étrangère**:


.. code-block:: sql

    drop table drh.employe;

    create table drh.departement(
        id serial primary key,
        nom varchar unique not null check (nom in ('production', 'administration'))
    );

    create table drh.employe(
        nom varchar not null,
        prenom varchar not null,
        salaire integer not null check (salaire >=0),
        unique(nom, prenom),
        dept integer references drh.departement(id) -- un departement avec cet valeur d'id doit exister
    );

Le type ``serial`` est un entier auto-incrémenté.  La contrainte ``primary key`` est équivalente à ``unique not null``.

.. code-block:: sql

    insert into drh.departement(nom) values('production');

    insert into drh.departement(nom) values('administration');

    insert into drh.employe(prenom, nom, salaire, dept) values('Pierre', 'Dupont', 50000, 1);

    insert into drh.employe(prenom, nom, salaire, dept) values('André', 'Dubois', 60000, 1);

    insert into drh.employe(prenom, nom, salaire, dept) values('Alain', 'Dubois', 25000, 2);

    insert into drh.employe(prenom, nom, salaire, dept) values('Raimond', 'Dupain', 15000, 3); -- échoue

Si l'on désire connaître le nom du département d'affectation d'Alain Dubois, on peut commencer par chercher le numéro du département. On limite les employés listés à l'aide d'une clause ``where``:

.. code-block:: sql
 
    select dept from drh.employe where nom='Dubois' and prenom='Alain';

On trouve que c'est le département dont l'``id`` est 2, on cherche ensuite le nom de ce département:

.. code-block:: sql
 
    select nom from drh.departement where id=2;

On peut utiliser une **sous-requête** (un ``select`` entre parenthèse) pour le faire en une seule fois:

.. code-block:: sql
 
    select nom from drh.departement where id=(select dept from drh.employe where nom='Dubois' and prenom='Alain');


Pour associer les employés à leur département d'affectation, on habituellement utilise une **jointure**:

.. code-block:: sql

    select e.nom, e.prenom, d.nom 
    from drh.employe as e 
    join drh.departement as d 
    on e.dept=d.id;

Que l'on peut aussi écrire avec une clause ``where``:

.. code-block:: sql

    select e.nom, e.prenom, d.nom  as departement 
    from drh.employe as e, drh.departement as d 
    where e.dept=d.id;

Comme cette requête est un peu longue a taper, on peut **aliaser** cette requête en créant une **vue**:

.. code-block:: sql

    create view drh.affectation as
    select e.nom, e.prenom, d.nom as departement from drh.employe as e, drh.departement as d where e.dept=d.id;

La nécessité de jointure est alors caché à l'utilisateur, et la requête s'écrit simplement:

.. code-block:: sql

    select * from drh.affectation;

Et pour le nom du département d'affectation d'Alain Dubois:

.. code-block:: sql

    select departement from drh.affectation where nom='Dubois' and prenom='Alain';


Exercice
--------

On souhaite réaliser la base de données suivante pour la gestion des stocks et le suivi des commandes:

.. image:: db1.svg

Notes: 
  - les cercles dénotent qu'un champ peut être null.
  - les losanges dénotent des clef primaires

Questions:
  - que doit-on modifier pour s'assurer qu'un client a forcément une adresse ?
  - quelle contrainte permet d'assurer qu'un client n'a qu'une seule adresse ?
  - quelle relation est établie par la table commande entre les clients et les produits ? 


Testez la base avec les requêtes suivantes:

.. code-block:: sql

    insert into adresse(code_postal) values('38000');

    insert into adresse(code_postal) values('75000');

    insert into client(prenom, nom) values('Pierre', 'Dupont');

    insert into client(prenom, nom) values('André', 'Dubois');

    insert into client(prenom, nom) values('Alain', 'Dubois');

    insert into client(prenom, nom) values('Alain', 'Dubois'); -- doit échouer

    insert into client(prenom, nom) values('Odette', 'Déficit');

    insert into telephone(client, numero) values(1, '0789012345');

    insert into telephone(numero) values('0123456789'); -- doit échouer

    insert into produit(nom, stock) values('tuyau', 200);

    insert into produit(nom, stock) values('robinets', 50);

    insert into produit(nom, stock) values('racords 90', 150);

    insert into commande(client, produit) values(1, 1);

    insert into commande(client, produit) values(1, 2);

    insert into commande(client, produit) values(2, 2);



SQL spatial avec PostGIS
========================

autre doc

Vocabulaire SIG
---------------

SRID
----

Géométries
----------

BBOX et index spatial
---------------------

Jointures spatiales
-------------------


