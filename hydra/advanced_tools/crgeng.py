# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
from subprocess import Popen, PIPE
from PyQt4 import uic, QtGui, QtCore
from PyQt4.QtGui import QTableWidgetItem, QFileDialog, QMessageBox
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.base_dialog import BaseDialog, tr
import hydra.utility.string as string

class Crgeng(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        self.current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(self.current_dir, "crgeng.ui"), self)

        self.table_control_files = HydraTableWidget(project,
            ("id", "ctrl_file"),
            (tr("Id"), tr("Control File")),
            "project", "crgeng",
            (self.new_control_file, self.delete_control_file), "", "id",
            self.table_control_files_placeholder, file_columns=[1])
        self.table_control_files.set_editable(False)

        self.go.clicked.connect(self.run)
        self.buttonBox.accepted.connect(self.save)

    def delete_control_file(self):
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_control_files.table.row(items[0])
            id = int(self.table_control_files.table.item(selected_row,0).text())
            self.project.execute("""delete from project.crgeng where id={};""".format(id))
            self.table_control_files.update_data()

    def new_control_file(self):
        file_url = QFileDialog.getOpenFileName(self, tr('Select crgeng control file'), os.path.join(self.project.get_workspace(), self.project.name))
        if file_url:
            self.project.execute("""insert into project.crgeng(ctrl_file) values('{}');""".format(self.project.pack_path(file_url)))
            self.table_control_files.update_data()

    def run(self):
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_control_files.table.row(items[0])
            file = self.table_control_files.table.item(selected_row,1).text()
            self.__exec("Crgeng_V8.exe", [file], os.path.dirname(file))
            with open(os.path.join(os.path.dirname(file), 'crgeng.log')) as f:
                log = f.readlines()
            with open(os.path.join(os.path.dirname(file), 'crgeng.err')) as f:
                err = int(f.readlines()[0])
            if err==0:
                self.project.log.notice("Crgeng successful:\n{}\n".format('\n'.join(log)))
                QMessageBox.information(self, tr('Crgeng successful'), tr("""Crgeng run successfully with control file {}.""").format(file), QMessageBox.Ok)
            else:
                self.project.log.error("Crgeng failed", '\n'.join(log))
                QMessageBox.warning(self, tr('Crgeng error'), tr("""An error occured during crgeng with file {}.\nSee hydra.log file for crgeng logs.\n""").format(file), QMessageBox.Ok)

    def __exec(self, binary, args, directory):
        args.insert(0, os.path.abspath(os.path.join(self.current_dir, binary)))
        Popen(args, cwd=directory, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True).communicate()

    def save(self):
        BaseDialog.save(self)
        self.close()