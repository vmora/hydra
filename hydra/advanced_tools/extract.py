# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
from subprocess import Popen, PIPE
from PyQt4 import uic, QtGui, QtCore
from PyQt4.QtGui import QTableWidgetItem, QFileDialog, QMessageBox
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.base_dialog import BaseDialog, tr
import hydra.utility.string as string

class Extract(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        self.current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(self.current_dir, "extract.ui"), self)

        self.table_control_files = HydraTableWidget(project,
            ("id", "ctrl_file"),
            (tr("Id"), tr("Control File")),
            "project", "extract",
            (self.new_control_file, self.delete_control_file), "", "id",
            self.table_control_files_placeholder, file_columns=[1])
        self.table_control_files.set_editable(False)
        self.table_control_files.table.itemSelectionChanged.connect(self.refresh_wexpdess)

        self.refresh_wexpdess()

        self.go_extract.clicked.connect(self.run_extract)
        self.go_wexpdess.clicked.connect(self.run_wexpdess)
        self.buttonBox.accepted.connect(self.save)

    def delete_control_file(self):
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_control_files.table.row(items[0])
            id = int(self.table_control_files.table.item(selected_row,0).text())
            self.project.execute("""delete from project.extract where id={};""".format(id))
            self.table_control_files.update_data()

    def new_control_file(self):
        file_url = QFileDialog.getOpenFileName(self, tr('Select extract control file'), os.path.join(self.project.get_workspace(), self.project.name))
        if file_url:
            self.project.execute("""insert into project.extract(ctrl_file) values('{}');""".format(self.project.pack_path(file_url)))
            self.table_control_files.update_data()

    def refresh_wexpdess(self):
        self.go_wexpdess.setEnabled(False)
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_control_files.table.row(items[0])
            file = self.table_control_files.table.item(selected_row,1).text()
            extract_res_file = os.path.splitext(file)[0]+'.mst'
            if os.path.isfile(extract_res_file):
                self.go_wexpdess.setEnabled(True)

    def run_extract(self):
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_control_files.table.row(items[0])
            file = self.table_control_files.table.item(selected_row,1).text()
            self.__exec("extract_V9.exe", [file], os.path.dirname(file))
            with open(os.path.join(os.path.dirname(file), 'extract.log')) as f:
                log = f.readlines()
            with open(os.path.join(os.path.dirname(file), 'extract.err')) as f:
                err = int(f.readlines()[0])
            if err==0:
                self.project.log.notice("Extract successful:\n{}\n".format('\n'.join(log)))
                QMessageBox.information(self, tr('Extract successful'), tr("""Extract run successfully with control file {}.""").format(file), QMessageBox.Ok)
            else:
                self.project.log.error("Extract failed", '\n'.join(log))
                QMessageBox.warning(self, tr('Extract error'), tr("""An error occured during extract with file {}.\nSee hydra.log file for extract logs.\n""").format(file), QMessageBox.Ok)
        self.refresh_wexpdess()

    def run_wexpdess(self):
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_control_files.table.row(items[0])
            file = self.table_control_files.table.item(selected_row,1).text()
            self.__exec("wexpdess.exe", [os.path.splitext(file)[0]+'.mst'], os.path.dirname(file))
            with open(os.path.join(os.path.dirname(file), 'expdess.log')) as f:
                log = f.readlines()
            self.project.log.notice("Expdess result:\n{}\n".format('\n'.join(log)))

    def __exec(self, binary, args, directory):
        args.insert(0, os.path.abspath(os.path.join(self.current_dir, binary)))
        process = Popen(args, cwd=directory, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
        process.communicate()

    def save(self):
        BaseDialog.save(self)
        self.close()