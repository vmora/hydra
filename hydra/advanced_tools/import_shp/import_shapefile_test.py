
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
run test to check if shp import in database

USAGE

   python -m hydra.utile.import_shapefile_test [-dhs] [-g project_name shapefile_name]

OPTIONS

   -h, --help
        print this help

   -g  --gui project_name shapefile_name
        run in gui mode

   -k  --keep
        keep the database and the project after test

"""


from __future__ import absolute_import # important to read the doc !

import sip
import sys
import os
import getopt
from subprocess import Popen, PIPE

for cls in [u'QDate', u'QDateTime', u'QString', u'QTextStream', u'QTime', u'QUrl', u'QVariant']:
    sip.setapi(cls, 2)

from PyQt4.QtGui import QApplication
from PyQt4.QtCore import QCoreApplication, QTranslator
from hydra.project import Project
from hydra.database.database import TestProject, remove_project, project_exists
from hydra.advanced_tools.import_shp.import_shapefile import ImportShapefile
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.string import normalized_name, normalized_model_name


def get_parameters():
        return SettingsProperties.get_path('postgre'), SettingsProperties.get_postgre_settings()

def gui_mode(project_name, shapefile):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project = Project.load_project(project_name)
    filepath = os.path.abspath(shapefile)
    filename = os.path.basename(shapefile).split('.')[0]

    # Load shape in schema work
    install_env = os.environ.copy()
    install_env['PGPASSWORD'] = SettingsProperties.get_postgre_settings()['password']
    postgrepath, pg = get_parameters()
    pg_shp2pgsql = os.path.join(postgrepath, 'bin', 'shp2pgsql')
    psql = os.path.join(postgrepath, 'bin', 'psql')
    argslist = [pg_shp2pgsql,'-I','-s',str(project.srid),'-W','latin1',filepath,'work.'+filename,'|',psql,'-U',pg['user'],'-d',str(project.name)]
    execute(argslist,install_env)

    test_dialog = ImportShapefile(project,filename)
    test_dialog.exec_()


def execute(argslist,install_env) :
    process = Popen(argslist, stdin=PIPE, stdout=PIPE, stderr=PIPE, env=install_env, shell=True)
    output, error = process.communicate()
    return

def test():

    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.street import StreetEditor
    from hydra.utility.string import normalized_name, normalized_model_name
    import sys

    log_manager = LogManager(ConsoleLogger(), "Hydra")

    current_dir = os.path.abspath(os.path.dirname(__file__))
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "shapefile_test"
    project_name = normalized_name(project_name)
    if project_exists(project_name):
        remove_project(project_name)
    project = Project.create_new_project("/$user/.hydra",project_name,'2154')
    project.add_new_model("modeltest")
    project.set_current_model("modeltest")

    install_env = os.environ.copy()
    install_env['PGPASSWORD'] = SettingsProperties.get_postgre_settings()['password']
    postgrepath, pg = get_parameters()
    pg_shp2pgsql = os.path.join(postgrepath, 'bin', 'shp2pgsql')
    psql = os.path.join(postgrepath, 'bin', 'psql')

    filepath = current_dir + "/test_data/manholetest.shp"
    filepath = os.path.abspath(filepath)
    argslist = [pg_shp2pgsql,'-I','-s',str(project.srid),'-W','latin1',filepath,'work.worktestmanhole','|',psql,'-U',pg['user'],'-d',str(project.name)]
    execute(argslist,install_env)

    filepath = current_dir + "/test_data/pipetest.shp"
    argslist = [pg_shp2pgsql,'-I','-s',str(project.srid),'-W','latin1',filepath,'work.worktestpipe','|',psql,'-U',pg['user'],'-d',str(project.name)]
    execute(argslist,install_env)

    filepath = current_dir + "/test_data/pointxyztest.shp"
    argslist = [pg_shp2pgsql,'-I','-s',str(project.srid),'-W','latin1',filepath,'work.workpointerrain','|',psql,'-U',pg['user'],'-d',str(project.name)]
    execute(argslist,install_env)

    worktabletest = 'worktestmanhole'

    test_dialog = ImportShapefile(project,worktabletest)
    test_dialog.comboSchema.setCurrentIndex(test_dialog.comboSchema.findText("modeltest"))
    test_dialog.comboTable.setCurrentIndex(test_dialog.comboTable.findText("manhole"))
    test_dialog.ImportTable.cellWidget(1,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(1,1).layout().itemAt(0).widget().findText("z_ground"))
    test_dialog.ImportTable.cellWidget(2,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(2,1).layout().itemAt(0).widget().findText("name"))
    test_dialog.ImportTable.cellWidget(3,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(3,1).layout().itemAt(0).widget().findText("area"))
    test_dialog.save()

    worktabletest = "worktestpipe"
    test_dialog = ImportShapefile(project,worktabletest)


    test_dialog.comboTable.setCurrentIndex(test_dialog.comboTable.findText("pipe"))
    test_dialog.ImportTable.cellWidget(1,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(1,1).layout().itemAt(0).widget().findText("comment"))
    test_dialog.ImportTable.cellWidget(2,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(2,1).layout().itemAt(0).widget().findText("name"))
    test_dialog.ImportTable.cellWidget(3,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(3,1).layout().itemAt(0).widget().findText("circular_d"))
    test_dialog.ImportTable.cellWidget(4,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(4,1).layout().itemAt(0).widget().findText("ovoid_inve"))
    test_dialog.ImportTable.cellWidget(6,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(6,1).layout().itemAt(0).widget().findText("ovoid_heig"))
    test_dialog.ImportTable.cellWidget(7,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(7,1).layout().itemAt(0).widget().findText("h_sable"))
    test_dialog.ImportTable.cellWidget(8,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(8,1).layout().itemAt(0).widget().findText("z_invert_u"))
    test_dialog.ImportTable.cellWidget(10,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(10,1).layout().itemAt(0).widget().findText("ovoid_top_"))
    test_dialog.ImportTable.cellWidget(11,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(11,1).layout().itemAt(0).widget().findText("z_invert_d"))
    test_dialog.ImportTable.cellWidget(12,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(12,1).layout().itemAt(0).widget().findText("rk"))

    test_dialog.ImportTable.cellWidget(5,3).activate_checkbox.setCheckState(True)
    test_dialog.ImportTable.cellWidget(5,3).default_value.setText('42')

    test_dialog.save()

    worktabletest = "workpointerrain"
    test_dialog = ImportShapefile(project,worktabletest)

    test_dialog.comboSchema.setCurrentIndex(test_dialog.comboSchema.findText("project"))

    test_dialog.comboTable.setCurrentIndex(test_dialog.comboTable.findText("points_xyz"))

    test_dialog.ImportTable.cellWidget(1,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(1,1).layout().itemAt(0).widget().findText("comment"))
    test_dialog.ImportTable.cellWidget(3,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(3,1).layout().itemAt(0).widget().findText("z_ground"))
    test_dialog.ImportTable.cellWidget(4,1).layout().itemAt(0).widget().setCurrentIndex(test_dialog.ImportTable.cellWidget(4,1).layout().itemAt(0).widget().findText("name"))

    test_dialog.ImportTable.cellWidget(2,3).activate_checkbox.setCheckState(True)
    test_dialog.ImportTable.cellWidget(2,3).default_value.setText('Nocifer')

    test_dialog.save()

    manhole = project.execute("""select area,z_ground from modeltest.manhole_node where name='NODA5018';""").fetchone()
    assert float(manhole[0])==15
    assert float(manhole[1])==700
    manhole2 = project.execute("""select area,z_ground from modeltest.manhole_node where name='NODA5023';""").fetchone()
    assert float(manhole2[0])==1.0
    assert float(manhole2[1])==9999
    pipe = project.execute("""select comment,h_sable,rk,custom_length from modeltest.pipe_link where z_invert_down=20 and z_invert_up=25;""").fetchone()
    assert pipe[0] == 'honor'
    assert pipe[1] == 1.2
    assert pipe[2] == 70
    assert pipe[3] == 42
    pointxyz = project.execute("""select name,comment,source from project.points_xyz where name= 'P_XYZ_5537';""").fetchone()
    assert pointxyz[0] == 'P_XYZ_5537'
    assert pointxyz[1] == 'vador'
    assert pointxyz[2] == 'Nocifer'
    numberpipe = project.execute("""select COUNT(*) from modeltest.pipe_link""").fetchone()[0]
    assert numberpipe == 6
    numberpointxyz = project.execute("""select COUNT(*) from project.points_xyz""").fetchone()[0]
    assert numberpointxyz == 4
    #check trigger status
    trigger = project.execute("""select trigger_branch from modeltest.metadata""").fetchone()[0]
    assert trigger == True

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgk",
            ["help", "debug", "gui","keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    shapefile_name = args[1]
    gui_mode(project_name, shapefile_name)
    exit(0)

test()

keep = False
if "-k" in optlist or "--keep" in optlist :
    keep = True

if project_exists("shapefile_test") and keep == False:
        remove_project("shapefile_test")

print "ok"