# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import json
import os
import re
from functools import partial
from collections import OrderedDict
from PyQt4 import uic
from PyQt4.QtCore import QCoreApplication, Qt
from PyQt4.QtGui import QMenu, QMessageBox, QWidget, QCursor, QTableWidgetItem, QComboBox, QLabel, QVBoxLayout, QDialogButtonBox
from hydra.utility.tables_properties import TablesProperties
from hydra.advanced_tools.import_shp.widget.default_value_widget import DefaultValueWidget
from hydra.gui.base_dialog import BaseDialog

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

__json_keys__ = json.load(open(os.path.join(os.path.dirname(__file__), "import_shapefile.json")))

class ImportShapefile(BaseDialog):
    def __init__ (self, project, source, parent=None):
        BaseDialog.__init__(self,project,parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "import_shapefile.ui"), self)

        self.table_source=source
        self.buttonBox.rejected.connect(self.close)
        self.buttonBox.accepted.connect(self.save)
        self.label_workname.setText("work.{}".format(self.table_source))

        # Combo for project/model choice
        self.comboSchema.addItem("project")
        models = self.project.get_models()
        for model in models :
            self.comboSchema.addItem(model)
        current_model = self.project.get_current_model()
        if current_model:
            self.comboSchema.setCurrentIndex(self.comboSchema.findText(self.project.get_current_model().name))

        self.comboSchema.currentIndexChanged.connect(self.__schema_changed)

        # Table
        self.ImportTable.setColumnCount(4)
        headerH=["Target column","Source column","Data type","Default value ?"]
        self.ImportTable.setHorizontalHeaderLabels(headerH)
        typeimportlist = [ str(item) for item in __json_keys__]
        if self.table_source is not None and self.table_source != ""  :
            self.geomtype = self.project.execute( "select type from geometry_columns where f_table_name = '{}' ".format(self.table_source)).fetchone()[0]
        else :
            self.geomtype = ""

        self.comboTable.currentIndexChanged.connect(self.__table_changed)

        self.__schema_changed()

    def __schema_changed(self):
        schema = self.comboSchema.currentText() if self.comboSchema.currentText() =='project' else 'model'
        self.comboTable.clear()
        for item in __json_keys__ :
            if str(self.geomtype) in str(__json_keys__[item]["geom"]["typeimport"]) and schema==str(__json_keys__[item]["schema"]):
                self.comboTable.addItem(str(item))
        self.__table_changed()

    def __table_changed(self):
        table = self.comboTable.currentText()
        self.ImportTable.setRowCount(0)
        if table:
            self.ImportTable.setRowCount(len(__json_keys__[table]["columns"])+1)
            list_column_tab = [__json_keys__[table]["geom"]["namecolumn"]] + [ item for item in __json_keys__[table]["columns"]]
            headerV = [__json_keys__[table]["geom"]["namecolumn"]]

            headerV = headerV + [s.replace('_',' ').capitalize() for s in __json_keys__[table]["columns"]]

            columnlistrequest = self.project.execute( "select column_name, data_type from information_schema.columns WHERE table_name = '{}'".format(self.table_source)).fetchall()
            columnlist=[]
            columnlisttype=[]

            for column in columnlistrequest:
                columnlist.append(str(column[0]))
                columnlisttype.append(str(column[1]))

            listtype = []
            for i in range (len(list_column_tab)):
                if list_column_tab[i] != __json_keys__[table]["geom"]["namecolumn"] :
                    listtype.append(__json_keys__[table]["columns"][list_column_tab[i]]["type"])
                else :
                    listtype.append(__json_keys__[table]["geom"]["type"])

            for i in range (self.ImportTable.rowCount()):
                #Target column
                label = QTableWidgetItem()
                label.setText(headerV[i])
                label.setTextAlignment(Qt.AlignCenter)
                self.ImportTable.setItem(i,0,label)

                # Source column , combo box
                if self.ImportTable.item(i,0).text() != __json_keys__[table]["geom"]["namecolumn"] :
                    type = __json_keys__[table]["columns"][list_column_tab[i]]["type"]
                    combo = self.get_combo(columnlist,columnlisttype,type,True)
                    vlayout = QVBoxLayout()
                    vlayout.addWidget(combo)
                    widget = QWidget()
                    widget.setLayout(vlayout)
                    self.ImportTable.setCellWidget(i,1,widget)

                #Source column geom, combo box
                if self.ImportTable.item(i,0).text() == __json_keys__[table]["geom"]["namecolumn"] :
                    columnlistgeom=[]
                    columnlisttypegeom=[]
                    columnlistrequestgeom = self.project.execute( "select column_name, data_type from information_schema.columns WHERE table_name = '{t}' and column_name IN (SELECT f_geometry_column FROM public.geometry_columns WHERE f_table_name = '{t}')".format(t=self.table_source)).fetchall()
                    for column in columnlistrequestgeom:
                        columnlistgeom.append(str(column[0]))
                        columnlisttypegeom.append(str(column[1]))

                    type = __json_keys__[table]["geom"]["type"]
                    combo = self.get_combo(columnlistgeom,columnlisttypegeom,type)
                    vlayoutgeom = QVBoxLayout()
                    vlayoutgeom.addWidget(combo)
                    widgetgeom = QWidget()
                    widgetgeom.setLayout(vlayoutgeom)
                    self.ImportTable.setCellWidget(i,1,widgetgeom)

                #data type column
                label2 = QTableWidgetItem()
                label2.setText(listtype[i])
                label2.setTextAlignment(Qt.AlignCenter)
                self.ImportTable.setItem(i,2,label2)

                #default value column
                if list_column_tab[i] != __json_keys__[table]["geom"]["namecolumn"]:
                    if __json_keys__[table]["columns"][list_column_tab[i]]["default"] == "True":
                        defaultwidget = DefaultValueWidget(self.ImportTable)
                        self.ImportTable.setCellWidget(i,3,defaultwidget)

            self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)
        else:
            self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

    def get_combo(self, list, listtype, type, nullable=False):
        combobox = QComboBox()
        if nullable == True :
            combobox.addItem("")
        for i in range(0, len(list)):
            if type == "real" :
                if listtype[i] == "numeric" or listtype[i] == "integer" :
                    temp = tr(list[i])
                    combobox.addItem("{}".format(temp))
            elif type == "string" :
                if listtype[i] == "character varying" :
                    temp = tr(list[i])
                    combobox.addItem("{}".format(temp))
            else :
                temp = tr(list[i])
                combobox.addItem("{}".format(temp))
        if nullable == False :
            combobox.setCurrentIndex(combobox.findText("geom"))

        return combobox

    def requete_insert(self, schema, table):
        statement = ""
        statementinsert = []
        statementselect = []
        statement += "insert into {}.{}".format(schema,table)
        if __json_keys__[table]["properties"]["type"] != "" :
            statement += "_{}".format(__json_keys__[table]["properties"]["type"])
        statement += " ("
        for i in range(self.ImportTable.rowCount()):
            default = self.ImportTable.cellWidget(i,3).get_default_value() if self.ImportTable.cellWidget(i,3) else False
            table_source_column = self.ImportTable.cellWidget(i,1).layout().itemAt(0).widget().currentText()
            table_target_column = self.ImportTable.item(i,0).text().lower().replace(' ','_')

            if (table_target_column == __json_keys__[table]["geom"]["namecolumn"]):
                # geometry part of query
                if __json_keys__[table]['geom']['typeimport'] == 'ZPOINT' :
                        geomstatement = "ST_SetSRID(ST_MakePoint(ST_X({point}),ST_Y({point}),ST_Z({point})),ST_SRID({point}))".format(point = self.ImportTable.cellWidget(0,1).layout().itemAt(0).widget().currentText())
                elif __json_keys__[table]['geom']['typeimport'] == 'MULTILINESTRINGZ' :
                        geomstatement = "ST_Force_3DZ((ST_Dump("+ self.ImportTable.cellWidget(0,1).layout().itemAt(0).widget().currentText() +")).geom)"
                if geomstatement:
                    statementinsert.append(table_target_column)
                    statementselect.append(geomstatement)
            elif default:
                statementinsert.append(table_target_column)
                statementselect.append("'" + default + "'" )
            elif table_source_column != '':
                statementinsert.append(table_target_column)
                statementselect.append(table_source_column)

        statement += (', ').join(statementinsert)
        statement += ") select "
        statement += (', ').join(statementselect)
        statement += " from {}.work.{}".format(self.project.get_current_project(),self.table_source)

        return statement

    def save(self):
        schema = self.comboSchema.currentText()
        table = self.comboTable.currentText()
        query = self.requete_insert(schema, table)

        if table == 'pipe' :
        # prevent update trigger for pipe to fire
            triggerstatement = "select trigger_branch from {}.metadata;".format(schema)
            bool_state = self.project.execute(triggerstatement).fetchone()[0]
            if bool_state == True :
                self.project.execute("update {}.metadata set trigger_branch = False;".format(schema))

        self.project.execute(query)

        # reinit trigger
        if table == 'pipe' and bool_state == True :
            self.project.execute("update {}.metadata set trigger_branch = True;".format(schema))
            self.project.execute("""select {}.branch_update_fct();""".format(schema))
        self.project.log.notice(tr("Imported data from schema work into table {}.{};".format(schema, table)))
        BaseDialog.save(self)
        self.close()