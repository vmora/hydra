# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
handles the maj on databases from version to version
"""

from __future__ import absolute_import # important to read the doc !
from PyQt4.QtCore import QCoreApplication, Qt
from PyQt4.QtGui import QMessageBox, QProgressDialog
import os
import tempfile
import string
import json
import math
import string
from hydra.database import database as dbhydra
from hydra.versions.dump_restore import temp_dump_project
from hydra.utility.sql_json import instances

__currendir = os.path.dirname(__file__)
__history = json.load(open(os.path.join(__currendir, "version_history.json")))

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

def update_project(project):
    save_file, exit_code, output, error = temp_dump_project(project.name)

    results = []
    while project.get_version() != dbhydra.data_version() :
        check = __check_version(project)

        res = one_step_update(project)
        results.append(res)

    res = 1 if 1 in results else 0
    return res

def one_step_update(project):
    current_version = project.get_version()
    target_version = project.get_version_to_update()

    srid = project.srid
    substitutions = {"srid":str(srid), "version":target_version, "hydra":'hydra_'+'_'.join(target_version.split('.')), "workspace":'$workspace'}

    project_file = __history[current_version]["scripts"]["prj"]
    model_file = __history[current_version]["scripts"]["model"]

    statements = []
    statements += __create_statements(project_file, substitutions)

    project_funcs, model_funcs = __get_hydra_sql_func()

    statements += [string.Template(s).substitute(substitutions) for s in project_funcs]

    models = project.get_models()
    for model in models:
        substitutions["model"] = model
        for type_ in ["node", "link", "singularity"]:
            addition = ["up is not null",
                        "down is not null",
                        "up_type is not null",
                        "down_type is not null"] if type_ == "link" else []
            invalidity_reason = "\nunion\n".join(["select id, "+
                "||".join(
                    ["case when {cond} then '' else '   {quoted}   ' end".format(
                        cond=cond, quoted=cond.replace("'", "''"))
                        for cond in def_["validity"]+addition]) +
                " as reason from $model.{}_{} as new where not validity".format(subtype, type_)
                for subtype, def_ in instances[type_+"_type"].items()
                    if "validity" in def_ and len(def_["validity"])])
            substitutions[type_+"_invalidity_reason"] = string.Template(invalidity_reason).substitute(substitutions)
        statements += __create_statements(model_file, substitutions)
        statements += [string.Template(s).substitute(substitutions) for s in model_funcs]

    res = __execute_statements(project, statements)
    project.log.notice("Updated project {} from version {} to version {}".format(project.name, current_version, target_version))
    return res

def __execute_statements(project, statements):
    res = 0
    # rearrange queries to run isolated ones first as they need specific transaction blocs and commits
    statements.sort(key=lambda x:(0 if '--isolated--' in x else 1))

    for i in range(len(statements)):
        try:
            if "--isolated--" in statements[i]:
                # carefull with use of this feature (commits previous queries. must be harmless for DB !)
                project.commit()
                project.execute_isolated(statements[i])
                project.commit()
            else:
                project.execute(statements[i])
        except Exception, error:
            project.log.error("Failed update query:", repr(error))
            res=1

    if res == 0:
        project.commit()
    else:
        project.rollback()
    return res

def __get_hydra_sql_func():
    project_funcs = []
    model_funcs = []
    for file in os.listdir(os.path.join(__currendir, "..", "server", "hydra")):
        if file.endswith(".sql"):
            for statement in open(os.path.join(__currendir, "..", "server", "hydra", file)).read().split(";;")[:-1]:
                if "from $hydra import" in statement:
                    if "project" in file:
                        project_funcs.append(statement)
                    elif "model" in file:
                        model_funcs.append(statement)
    return project_funcs, model_funcs

def __create_statements(file, subs={}):
    return [string.Template(s).substitute(subs) for s in open(os.path.join(__currendir, file)).read().split(";;")[:-1]]


def __check_version(project):
    if project.get_version() == dbhydra.data_version():
        project.log.error(tr("""Selected project is in version {}.\nThis is already the last available data model version.\nNo update is needed.""").format(project.get_version()))
    elif project.get_version_to_update() == "null":
        project.log.error(tr("""Selected project is in version {}.\nThis version does not support automatic update.\nPlease contact Hydra support.""").format(project.get_version()))
    elif project.get_version_to_update() == 'unsupported':
        project.log.error(tr("""Selected project seems to be in version {}.\nThis version is not compatible with automatic update.\nPlease contact Hydra support.""").format(project.get_version()))

