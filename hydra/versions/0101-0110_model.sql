/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Update model                                  */
/* ********************************************** */

alter table ${model}.metadata
    add column if not exists switching boolean default 'f';
;;


create table $model.config_switch(
    id integer not null default 1 unique check (id=1),
    is_switching boolean default 'f'
)
;;

insert into $model.config_switch default values;
;;

-- remove ueless trigger
drop trigger if exists ${model}_metadata_configuration_after_update_trig on $model.metadata;
;;

-- remove useless associated func
drop function if exists ${model}.metadata_configuration_after_update_fct();
;;

create or replace function ${model}.metadata_configuration_after_update_fct()
returns trigger
language plpgsql
as $$$$
    declare
        node record;
        singularity record;
        link record;
        update_fields varchar;
        update_arrays varchar;
        json_fields varchar;
        config_name varchar;
    begin
        select name from $model.configuration where id=new.configuration into config_name;
        update $model.config_switch set is_switching=true;

        -- Update nodes that have a configuration
        for node in select * from $model._node where configuration is not null loop
            if config_name not in (select k from json_object_keys(node.configuration) as k) then
                select 'default' into config_name;
            end if;

            -- json switch must be done first to handle trigger on other fields with correct values
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type = 'json'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'configuration')
            into json_fields;

            if json_fields is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||json_fields||' where id='||node.id::varchar||';';
            end if;

            -- user defined types
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type = 'USER-DEFINED'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||update_fields||' where id='||node.id::varchar||';';
            end if;

            -- common fields
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type != 'USER-DEFINED'
            and data_type != 'ARRAY'
            and data_type != 'json'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||update_fields||' where id='||node.id::varchar||';';
            end if;

            -- assume that all arrays are real[] (it is the case in v1.0.0)
            select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type = 'ARRAY'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'configuration')
            into update_arrays;

            if update_arrays is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||update_arrays||' where id='||node.id::varchar||';';
            end if;
        end loop;

        -- Update singularities that have a configuration
        for singularity in select * from $model._singularity where configuration is not null loop
            if config_name not in (select k from json_object_keys(singularity.configuration) as k) then
                select 'default' into config_name;
            end if;

            -- json switch must be done first to handle trigger on other fields with correct values
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type = 'json'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'configuration')
            into json_fields;

            if json_fields is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||json_fields||' where id='||singularity.id::varchar||';';
            end if;

            -- user defined types
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type = 'USER-DEFINED'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||update_fields||' where id='||singularity.id::varchar||';';
            end if;

            -- common fields
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type != 'USER-DEFINED'
            and data_type != 'ARRAY'
            and data_type != 'json'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||update_fields||' where id='||singularity.id::varchar||';';
            end if;

            -- assume that all arrays are real[] (it is the case in v1.0.0)
            select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type = 'ARRAY'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'configuration')
            into update_arrays;

            if update_arrays is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||update_arrays||' where id='||singularity.id::varchar||';';
            end if;
        end loop;

        -- Update links that have a configuration
        for link in select * from $model._link where configuration is not null loop
            if config_name not in (select k from json_object_keys(link.configuration) as k) then
                select 'default' into config_name;
            end if;

            -- json switch must be done first to handle trigger on other fields with correct values
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type = 'json'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'configuration', 'branch')
            into json_fields;

            if json_fields is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||json_fields||' where id='||node.id::varchar||';';
            end if;

            -- user defined types
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type = 'USER-DEFINED'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'configuration', 'branch')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||update_fields||' where id='||link.id::varchar||';';
            end if;

            -- common fields
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type != 'USER-DEFINED'
            and data_type != 'ARRAY'
            and data_type != 'json'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'configuration', 'branch')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||update_fields||' where id='||link.id::varchar||';';
            end if;

            -- assume that all arrays are real[] (it is the case in v1.0.0)
            select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type = 'ARRAY'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'configuration', 'branch')
            into update_arrays;

            if update_arrays is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||update_arrays||' where id='||link.id::varchar||';';
            end if;
        end loop;

        update $model.config_switch set is_switching=false;
        return new;
    end;
$$$$
;;

create trigger ${model}_metadata_configuration_after_update_trig
    before update of configuration on $model.metadata
       for each row execute procedure ${model}.metadata_configuration_after_update_fct()
;;

/* ***************************************************************** */
/* Following code is autogenerated from templates with the command:  */
/* build.py model $model $srid $version >update.sql              */
/* then replace $srid by $srid in generated code and clean up    */
/* ***************************************************************** */

/* ******* */
/*  NODES  */
/* ******* */
create or replace function ${model}.river_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('river', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'river') where name = 'define_later' and id = id_;
            insert into $model._river_node
                values (id_, 'river', coalesce(new.reach, (select id from $model.reach where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) asc limit 1)), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.area, 1));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'river_node');

            -- Lines to update specific nodes that works with associated contours
            if 'river' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'river' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) from  $model._river_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.reach, old.z_ground, old.area) as o, (select new.reach, new.z_ground, new.area) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.reach, new.z_ground, new.area) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._river_node set reach=new.reach, z_ground=new.z_ground, area=new.area where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'river_node');

            -- Lines to update specific nodes that works with associated contours
            if 'river' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'river' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'river' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) from  $model._river_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'river' = 'storage' then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'river' = 'crossroad' then
                perform $model.update_street_links();
            end if;

            delete from $model._river_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;

$$$$
;;

create or replace function ${model}.manhole_hydrology_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('manhole_hydrology', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'manhole_hydrology') where name = 'define_later' and id = id_;
            insert into $model._manhole_hydrology_node
                values (id_, 'manhole_hydrology', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'manhole_hydrology_node');

            -- Lines to update specific nodes that works with associated contours
            if 'manhole_hydrology' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'manhole_hydrology' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) from  $model._manhole_hydrology_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground) as o, (select new.area, new.z_ground) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._manhole_hydrology_node set area=new.area, z_ground=new.z_ground where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'manhole_hydrology_node');

            -- Lines to update specific nodes that works with associated contours
            if 'manhole_hydrology' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'manhole_hydrology' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'manhole_hydrology' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) from  $model._manhole_hydrology_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'manhole_hydrology' = 'storage' then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'manhole_hydrology' = 'crossroad' then
                perform $model.update_street_links();
            end if;

            delete from $model._manhole_hydrology_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;

$$$$
;;

create or replace function ${model}.storage_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('storage', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'storage') where name = 'define_later' and id = id_;
            insert into $model._storage_node
                values (id_, 'storage', new.zs_array, new.zini, coalesce(new.contour, (select id from $model.coverage where st_intersects(geom, new.geom))));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'storage' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10 ) and (array_length(zs_array, 1) >=1) and (array_length(zs_array, 2)=2) from  $model._storage_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.zs_array, old.zini, old.contour) as o, (select new.zs_array, new.zini, new.contour) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.zs_array, new.zini, new.contour) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._storage_node set zs_array=new.zs_array, zini=new.zini, contour=new.contour where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'storage' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10 ) and (array_length(zs_array, 1) >=1) and (array_length(zs_array, 2)=2) from  $model._storage_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'storage' = 'crossroad' then
                perform $model.update_street_links();
            end if;

            delete from $model._storage_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;

$$$$
;;

create or replace function ${model}.catchment_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('catchment', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'catchment') where name = 'define_later' and id = id_;
            insert into $model._catchment_node
                values (id_, 'catchment', new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.netflow_param::json, new.runoff_type, new.runoff_param::json, coalesce(new.q_limit, 9999), coalesce(new.q0, 0), coalesce(new.contour, (select id from $model.catchment where st_intersects(geom, new.geom))), new.catchment_pollution_mode, new.catchment_pollution_param::json);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'catchment' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_param is not null) and (runoff_type is not null) and (runoff_param is not null) and (q_limit is not null) and (q0 is not null) from  $model._catchment_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.netflow_param, old.runoff_type, old.runoff_param, old.q_limit, old.q0, old.contour, old.catchment_pollution_mode, old.catchment_pollution_param) as o, (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.netflow_param, new.runoff_type, new.runoff_param, new.q_limit, new.q0, new.contour, new.catchment_pollution_mode, new.catchment_pollution_param) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.netflow_param, new.runoff_type, new.runoff_param, new.q_limit, new.q0, new.contour, new.catchment_pollution_mode, new.catchment_pollution_param) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._catchment_node set area_ha=new.area_ha, rl=new.rl, slope=new.slope, c_imp=new.c_imp, netflow_type=new.netflow_type, netflow_param=new.netflow_param::json, runoff_type=new.runoff_type, runoff_param=new.runoff_param::json, q_limit=new.q_limit, q0=new.q0, contour=new.contour, catchment_pollution_mode=new.catchment_pollution_mode, catchment_pollution_param=new.catchment_pollution_param::json where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'catchment' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'catchment' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_param is not null) and (runoff_type is not null) and (runoff_param is not null) and (q_limit is not null) and (q0 is not null) from  $model._catchment_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'storage' then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'catchment' = 'crossroad' then
                perform $model.update_street_links();
            end if;

            delete from $model._catchment_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;

$$$$
;;

create or replace function ${model}.station_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('station', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'station') where name = 'define_later' and id = id_;
            insert into $model._station_node
                values (id_, 'station', coalesce(new.area, 1), coalesce(new.z_invert, (select project.altitude(new.geom))), coalesce(new.station, (select id from $model.station where ST_Intersects(new.geom, geom))));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'station_node');

            -- Lines to update specific nodes that works with associated contours
            if 'station' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'station' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (z_invert is not null) and (station is not null ) and ((select st_intersects(new.geom, geom) from $model.station where id=new.station)) from  $model._station_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_invert, old.station) as o, (select new.area, new.z_invert, new.station) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_invert, new.station) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._station_node set area=new.area, z_invert=new.z_invert, station=new.station where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'station_node');

            -- Lines to update specific nodes that works with associated contours
            if 'station' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'station' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'station' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (z_invert is not null) and (station is not null ) and ((select st_intersects(new.geom, geom) from $model.station where id=new.station)) from  $model._station_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'station' = 'storage' then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'station' = 'crossroad' then
                perform $model.update_street_links();
            end if;

            delete from $model._station_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;

$$$$
;;

create or replace function ${model}.manhole_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('manhole', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'manhole') where name = 'define_later' and id = id_;
            insert into $model._manhole_node
                values (id_, 'manhole', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'manhole_node');

            -- Lines to update specific nodes that works with associated contours
            if 'manhole' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'manhole' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) and ($model.check_connected_pipes(new.id)) from  $model._manhole_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground) as o, (select new.area, new.z_ground) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._manhole_node set area=new.area, z_ground=new.z_ground where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'manhole_node');

            -- Lines to update specific nodes that works with associated contours
            if 'manhole' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'manhole' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'manhole' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) and ($model.check_connected_pipes(new.id)) from  $model._manhole_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'manhole' = 'storage' then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'manhole' = 'crossroad' then
                perform $model.update_street_links();
            end if;

            delete from $model._manhole_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;

$$$$
;;

create or replace function ${model}.elem_2d_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('elem_2d', coalesce(new.name, 'define_later'), ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata)), new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'elem_2d') where name = 'define_later' and id = id_;
            insert into $model._elem_2d_node
                values (id_, 'elem_2d', coalesce(new.area, ST_Area(new.contour)), coalesce(new.zb, (select ST_Z(ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata))))), coalesce(new.rk, 12), new.domain_2d, new.contour);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'elem_2d_node');

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'elem_2d' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.zb, old.rk, old.domain_2d, old.contour) as o, (select new.area, new.zb, new.rk, new.domain_2d, new.contour) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.zb, new.rk, new.domain_2d, new.contour) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata)), generated=new.generated where id=old.id;
            update $model._elem_2d_node set area=new.area, zb=new.zb, rk=new.rk, domain_2d=new.domain_2d, contour=new.contour where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'elem_2d_node');

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'elem_2d' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'elem_2d' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'storage' then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'elem_2d' = 'crossroad' then
                perform $model.update_street_links();
            end if;

            delete from $model._elem_2d_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;

$$$$
;;

create or replace function ${model}.crossroad_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('crossroad', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'crossroad') where name = 'define_later' and id = id_;
            insert into $model._crossroad_node
                values (id_, 'crossroad', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.h, 0));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'crossroad_node');

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'crossroad' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground, old.h) as o, (select new.area, new.z_ground, new.h) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground, new.h) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._crossroad_node set area=new.area, z_ground=new.z_ground, h=new.h where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'crossroad_node');

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'crossroad' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'crossroad' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'storage' then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'crossroad' = 'crossroad' then
                perform $model.update_street_links();
            end if;

            delete from $model._crossroad_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;

$$$$
;;

/* ************* */
/*  SINGULARITY  */
/* ************* */

create or replace function ${model}.zq_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'zq_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'zq_bc') where name = 'define_later' and id = id_;

            insert into $model._zq_bc_singularity
                values (id_, 'zq_bc', new.zq_array);
            if 'zq_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'zq_bc_singularity');
            update $model._singularity set validity = (select (zq_array is not null) and (array_length(zq_array, 1)<=10) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) from  $model._zq_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.zq_array) as o, (select new.zq_array) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.zq_array) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._zq_bc_singularity set zq_array=new.zq_array where id=old.id;
            if 'zq_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'zq_bc_singularity');
            update $model._singularity set validity = (select (zq_array is not null) and (array_length(zq_array, 1)<=10) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) from  $model._zq_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._zq_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'zq_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.zq_split_hydrology_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'zq_split_hydrology', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'zq_split_hydrology') where name = 'define_later' and id = id_;

            insert into $model._zq_split_hydrology_singularity
                values (id_, 'zq_split_hydrology', new.downstream, coalesce(new.downstream_type, (select link_type from $model._link where id=new.downstream)), new.split1, coalesce(new.split1_type, (select link_type from $model._link where id=new.split1)), new.split2, coalesce(new.split2_type, (select link_type from $model._link where id=new.split2)), coalesce(new.downstream_law, 'weir'), new.downstream_param::json, new.split1_law, new.split1_param::json, new.split2_law, new.split2_param::json);
            if 'zq_split_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'zq_split_hydrology_singularity');
            update $model._singularity set validity = (select (downstream is not null) and (split1 is not null) and (downstream_param is not null) and (split1_law is not null) and (split1_param is not null) and (split2 is null or split2_law is not null) and (split2 is null or split2_param is not null) from  $model._zq_split_hydrology_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.downstream, old.downstream_type, old.split1, old.split1_type, old.split2, old.split2_type, old.downstream_law, old.downstream_param, old.split1_law, old.split1_param, old.split2_law, old.split2_param) as o, (select new.downstream, new.downstream_type, new.split1, new.split1_type, new.split2, new.split2_type, new.downstream_law, new.downstream_param, new.split1_law, new.split1_param, new.split2_law, new.split2_param) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.downstream, new.downstream_type, new.split1, new.split1_type, new.split2, new.split2_type, new.downstream_law, new.downstream_param, new.split1_law, new.split1_param, new.split2_law, new.split2_param) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._zq_split_hydrology_singularity set downstream=new.downstream, downstream_type=new.downstream_type, split1=new.split1, split1_type=new.split1_type, split2=new.split2, split2_type=new.split2_type, downstream_law=new.downstream_law, downstream_param=new.downstream_param::json, split1_law=new.split1_law, split1_param=new.split1_param::json, split2_law=new.split2_law, split2_param=new.split2_param::json where id=old.id;
            if 'zq_split_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'zq_split_hydrology_singularity');
            update $model._singularity set validity = (select (downstream is not null) and (split1 is not null) and (downstream_param is not null) and (split1_law is not null) and (split1_param is not null) and (split2 is null or split2_law is not null) and (split2 is null or split2_param is not null) from  $model._zq_split_hydrology_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._zq_split_hydrology_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'zq_split_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;
create or replace function ${model}.pipe_branch_marker_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'pipe_branch_marker', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'pipe_branch_marker') where name = 'define_later' and id = id_;

            insert into $model._pipe_branch_marker_singularity
                values (id_, 'pipe_branch_marker', coalesce(new.pk0_km, 0), coalesce(new.dx, 50));
            if 'pipe_branch_marker' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'pipe_branch_marker_singularity');
            update $model._singularity set validity = (select (pk0_km is not null) and (dx is not null) and (dx>=0.1) from  $model._pipe_branch_marker_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.pk0_km, old.dx) as o, (select new.pk0_km, new.dx) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.pk0_km, new.dx) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._pipe_branch_marker_singularity set pk0_km=new.pk0_km, dx=new.dx where id=old.id;
            if 'pipe_branch_marker' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'pipe_branch_marker_singularity');
            update $model._singularity set validity = (select (pk0_km is not null) and (dx is not null) and (dx>=0.1) from  $model._pipe_branch_marker_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._pipe_branch_marker_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'pipe_branch_marker' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.strickler_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'strickler_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'strickler_bc') where name = 'define_later' and id = id_;

            insert into $model._strickler_bc_singularity
                values (id_, 'strickler_bc', new.slope, new.k, new.width);
            if 'strickler_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'strickler_bc_singularity');
            update $model._singularity set validity = (select (slope is not null) and (k is not null) and (k>0) and (width is not null) and (width>=0) from  $model._strickler_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.slope, old.k, old.width) as o, (select new.slope, new.k, new.width) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.slope, new.k, new.width) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._strickler_bc_singularity set slope=new.slope, k=new.k, width=new.width where id=old.id;
            if 'strickler_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'strickler_bc_singularity');
            update $model._singularity set validity = (select (slope is not null) and (k is not null) and (k>0) and (width is not null) and (width>=0) from  $model._strickler_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._strickler_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'strickler_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.marker_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'marker', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'marker') where name = 'define_later' and id = id_;

            insert into $model._marker_singularity
                values (id_, 'marker', new.comment);
            if 'marker' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'marker_singularity');
            update $model._singularity set validity = (select 't'::boolean from  $model._marker_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment) as o, (select new.comment) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._marker_singularity set comment=new.comment where id=old.id;
            if 'marker' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'marker_singularity');
            update $model._singularity set validity = (select 't'::boolean from  $model._marker_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._marker_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'marker' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.model_connect_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'model_connect_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'model_connect_bc') where name = 'define_later' and id = id_;

            insert into $model._model_connect_bc_singularity
                values (id_, 'model_connect_bc', coalesce(new.cascade_mode, 'hydrograph'), new.zq_array);
            if 'model_connect_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'model_connect_bc_singularity');
            update $model._singularity set validity = (select (cascade_mode is not null) and (cascade_mode='hydrograph' or zq_array is not null) and (array_length(zq_array, 1)<=100) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) from  $model._model_connect_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.cascade_mode, old.zq_array) as o, (select new.cascade_mode, new.zq_array) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.cascade_mode, new.zq_array) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._model_connect_bc_singularity set cascade_mode=new.cascade_mode, zq_array=new.zq_array where id=old.id;
            if 'model_connect_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'model_connect_bc_singularity');
            update $model._singularity set validity = (select (cascade_mode is not null) and (cascade_mode='hydrograph' or zq_array is not null) and (array_length(zq_array, 1)<=100) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) from  $model._model_connect_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._model_connect_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'model_connect_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.bradley_headloss_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'bradley_headloss', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'bradley_headloss') where name = 'define_later' and id = id_;

            insert into $model._bradley_headloss_singularity
                values (id_, 'bradley_headloss', new.d_abutment_l, new.d_abutment_r, coalesce(new.abutment_type, 'angle_90'), new.zw_array, coalesce(new.z_ceiling, new.zw_array[array_length(new.zw_array, 1)][1]));
            if 'bradley_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'bradley_headloss_singularity');
            update $model._singularity set validity = (select (d_abutment_l is not null) and (d_abutment_r is not null) and (abutment_type is not null) and (zw_array is not null ) and (z_ceiling is not null) and (array_length(zw_array, 1)<=10) and (array_length(zw_array, 1)>=1) and (array_length(zw_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._bradley_headloss_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.d_abutment_l, old.d_abutment_r, old.abutment_type, old.zw_array, old.z_ceiling) as o, (select new.d_abutment_l, new.d_abutment_r, new.abutment_type, new.zw_array, new.z_ceiling) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.d_abutment_l, new.d_abutment_r, new.abutment_type, new.zw_array, new.z_ceiling) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._bradley_headloss_singularity set d_abutment_l=new.d_abutment_l, d_abutment_r=new.d_abutment_r, abutment_type=new.abutment_type, zw_array=new.zw_array, z_ceiling=new.z_ceiling where id=old.id;
            if 'bradley_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'bradley_headloss_singularity');
            update $model._singularity set validity = (select (d_abutment_l is not null) and (d_abutment_r is not null) and (abutment_type is not null) and (zw_array is not null ) and (z_ceiling is not null) and (array_length(zw_array, 1)<=10) and (array_length(zw_array, 1)>=1) and (array_length(zw_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._bradley_headloss_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._bradley_headloss_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'bradley_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.hydraulic_cut_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'hydraulic_cut', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'hydraulic_cut') where name = 'define_later' and id = id_;

            insert into $model._hydraulic_cut_singularity
                values (id_, 'hydraulic_cut', new.qz_array);
            if 'hydraulic_cut' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'hydraulic_cut_singularity');
            update $model._singularity set validity = (select (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._hydraulic_cut_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.qz_array) as o, (select new.qz_array) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.qz_array) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._hydraulic_cut_singularity set qz_array=new.qz_array where id=old.id;
            if 'hydraulic_cut' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'hydraulic_cut_singularity');
            update $model._singularity set validity = (select (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._hydraulic_cut_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._hydraulic_cut_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'hydraulic_cut' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.reservoir_rsp_hydrology_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'reservoir_rsp_hydrology', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'reservoir_rsp_hydrology') where name = 'define_later' and id = id_;

            insert into $model._reservoir_rsp_hydrology_singularity
                values (id_, 'reservoir_rsp_hydrology', new.drainage, coalesce(new.drainage_type, (select link_type from $model._link where id=new.drainage)), new.overflow, coalesce(new.overflow_type, (select link_type from $model._link where id=new.overflow)), coalesce(new.z_ini, new.zr_sr_qf_qs_array[1][1]), new.zr_sr_qf_qs_array, coalesce(new.treatment_mode, 'residual_concentration'), new.treatment_param::json);
            if 'reservoir_rsp_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'reservoir_rsp_hydrology_singularity');
            update $model._singularity set validity = (select (drainage is not null) and (overflow is not null) and (z_ini is not null) and (zr_sr_qf_qs_array is not null ) and (treatment_mode is not null) and (treatment_param is not null) and (array_length(zr_sr_qf_qs_array, 1)<=10) and (array_length(zr_sr_qf_qs_array, 1)>=1) and (array_length(zr_sr_qf_qs_array, 2)=4) from  $model._reservoir_rsp_hydrology_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.drainage, old.drainage_type, old.overflow, old.overflow_type, old.z_ini, old.zr_sr_qf_qs_array, old.treatment_mode, old.treatment_param) as o, (select new.drainage, new.drainage_type, new.overflow, new.overflow_type, new.z_ini, new.zr_sr_qf_qs_array, new.treatment_mode, new.treatment_param) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.drainage, new.drainage_type, new.overflow, new.overflow_type, new.z_ini, new.zr_sr_qf_qs_array, new.treatment_mode, new.treatment_param) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._reservoir_rsp_hydrology_singularity set drainage=new.drainage, drainage_type=new.drainage_type, overflow=new.overflow, overflow_type=new.overflow_type, z_ini=new.z_ini, zr_sr_qf_qs_array=new.zr_sr_qf_qs_array, treatment_mode=new.treatment_mode, treatment_param=new.treatment_param::json where id=old.id;
            if 'reservoir_rsp_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'reservoir_rsp_hydrology_singularity');
            update $model._singularity set validity = (select (drainage is not null) and (overflow is not null) and (z_ini is not null) and (zr_sr_qf_qs_array is not null ) and (treatment_mode is not null) and (treatment_param is not null) and (array_length(zr_sr_qf_qs_array, 1)<=10) and (array_length(zr_sr_qf_qs_array, 1)>=1) and (array_length(zr_sr_qf_qs_array, 2)=4) from  $model._reservoir_rsp_hydrology_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._reservoir_rsp_hydrology_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'reservoir_rsp_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.hydrograph_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'hydrograph_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'hydrograph_bc') where name = 'define_later' and id = id_;

            insert into $model._hydrograph_bc_singularity
                values (id_, 'hydrograph_bc', new.storage_area, new.tq_array, coalesce(new.constant_dry_flow, 0), coalesce(new.distrib_coef, 1), coalesce(new.lag_time_hr, 0), new.sector, new.hourly_modulation, new.pollution_quality_mode, new.pollution_quality_param::json, coalesce(new.external_file_data, 'f'));
            if 'hydrograph_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'hydrograph_bc_singularity');
            update $model._singularity set validity = (select (storage_area is not null) and (storage_area>=0) and (constant_dry_flow is not null) and (constant_dry_flow>=0) and (distrib_coef is null or (distrib_coef>=0 and distrib_coef<=1)) and (lag_time_hr is null or (lag_time_hr>=0)) and (sector is null or (distrib_coef is not null and lag_time_hr is not null)) and (external_file_data or tq_array is not null) and (external_file_data or array_length(tq_array, 1)<=10) and (external_file_data or array_length(tq_array, 1)>=1) and (external_file_data or array_length(tq_array, 2)=2) from  $model._hydrograph_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.storage_area, old.tq_array, old.constant_dry_flow, old.distrib_coef, old.lag_time_hr, old.sector, old.hourly_modulation, old.pollution_quality_mode, old.pollution_quality_param, old.external_file_data) as o, (select new.storage_area, new.tq_array, new.constant_dry_flow, new.distrib_coef, new.lag_time_hr, new.sector, new.hourly_modulation, new.pollution_quality_mode, new.pollution_quality_param, new.external_file_data) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.storage_area, new.tq_array, new.constant_dry_flow, new.distrib_coef, new.lag_time_hr, new.sector, new.hourly_modulation, new.pollution_quality_mode, new.pollution_quality_param, new.external_file_data) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._hydrograph_bc_singularity set storage_area=new.storage_area, tq_array=new.tq_array, constant_dry_flow=new.constant_dry_flow, distrib_coef=new.distrib_coef, lag_time_hr=new.lag_time_hr, sector=new.sector, hourly_modulation=new.hourly_modulation, pollution_quality_mode=new.pollution_quality_mode, pollution_quality_param=new.pollution_quality_param::json, external_file_data=new.external_file_data where id=old.id;
            if 'hydrograph_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'hydrograph_bc_singularity');
            update $model._singularity set validity = (select (storage_area is not null) and (storage_area>=0) and (constant_dry_flow is not null) and (constant_dry_flow>=0) and (distrib_coef is null or (distrib_coef>=0 and distrib_coef<=1)) and (lag_time_hr is null or (lag_time_hr>=0)) and (sector is null or (distrib_coef is not null and lag_time_hr is not null)) and (external_file_data or tq_array is not null) and (external_file_data or array_length(tq_array, 1)<=10) and (external_file_data or array_length(tq_array, 1)>=1) and (external_file_data or array_length(tq_array, 2)=2) from  $model._hydrograph_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._hydrograph_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'hydrograph_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.zregul_weir_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'zregul_weir', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'zregul_weir') where name = 'define_later' and id = id_;

            insert into $model._zregul_weir_singularity
                values (id_, 'zregul_weir', new.z_invert, new.z_regul, new.width, coalesce(new.cc, .6), coalesce(new.mode_regul, 'elevation'), coalesce(new.reoxy_law, 'gameson'), new.reoxy_param::json);
            if 'zregul_weir' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'zregul_weir_singularity');
            update $model._singularity set validity = (select (z_invert is not null ) and (z_regul is not null ) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (mode_regul is not null) and (reoxy_law is not null) and (reoxy_param is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._zregul_weir_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_regul, old.width, old.cc, old.mode_regul, old.reoxy_law, old.reoxy_param) as o, (select new.z_invert, new.z_regul, new.width, new.cc, new.mode_regul, new.reoxy_law, new.reoxy_param) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_regul, new.width, new.cc, new.mode_regul, new.reoxy_law, new.reoxy_param) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._zregul_weir_singularity set z_invert=new.z_invert, z_regul=new.z_regul, width=new.width, cc=new.cc, mode_regul=new.mode_regul, reoxy_law=new.reoxy_law, reoxy_param=new.reoxy_param::json where id=old.id;
            if 'zregul_weir' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'zregul_weir_singularity');
            update $model._singularity set validity = (select (z_invert is not null ) and (z_regul is not null ) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (mode_regul is not null) and (reoxy_law is not null) and (reoxy_param is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._zregul_weir_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._zregul_weir_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'zregul_weir' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.bridge_headloss_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'bridge_headloss', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'bridge_headloss') where name = 'define_later' and id = id_;

            insert into $model._bridge_headloss_singularity
                values (id_, 'bridge_headloss', new.l_road, new.z_road, new.zw_array);
            if 'bridge_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'bridge_headloss_singularity');
            update $model._singularity set validity = (select (l_road is not null) and (l_road>=0) and (z_road is not null) and (zw_array is not null ) and (array_length(zw_array, 1)<=10) and (array_length(zw_array, 1)>=1) and (array_length(zw_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._bridge_headloss_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.l_road, old.z_road, old.zw_array) as o, (select new.l_road, new.z_road, new.zw_array) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.l_road, new.z_road, new.zw_array) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._bridge_headloss_singularity set l_road=new.l_road, z_road=new.z_road, zw_array=new.zw_array where id=old.id;
            if 'bridge_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'bridge_headloss_singularity');
            update $model._singularity set validity = (select (l_road is not null) and (l_road>=0) and (z_road is not null) and (zw_array is not null ) and (array_length(zw_array, 1)<=10) and (array_length(zw_array, 1)>=1) and (array_length(zw_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._bridge_headloss_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._bridge_headloss_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'bridge_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.qq_split_hydrology_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'qq_split_hydrology', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'qq_split_hydrology') where name = 'define_later' and id = id_;

            insert into $model._qq_split_hydrology_singularity
                values (id_, 'qq_split_hydrology', new.qq_array, new.downstream, coalesce(new.downstream_type, (select link_type from $model._link where id=new.downstream)), new.split1, coalesce(new.split1_type, (select link_type from $model._link where id=new.split1)), new.split2, coalesce(new.split2_type, (select link_type from $model._link where id=new.split2)));
            if 'qq_split_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'qq_split_hydrology_singularity');
            update $model._singularity set validity = (select (qq_array is not null) and (downstream is not null) and (split1 is not null) and (array_length(qq_array, 1)<=10) and (array_length(qq_array, 1)>=1) and ((split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)) from  $model._qq_split_hydrology_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.qq_array, old.downstream, old.downstream_type, old.split1, old.split1_type, old.split2, old.split2_type) as o, (select new.qq_array, new.downstream, new.downstream_type, new.split1, new.split1_type, new.split2, new.split2_type) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.qq_array, new.downstream, new.downstream_type, new.split1, new.split1_type, new.split2, new.split2_type) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._qq_split_hydrology_singularity set qq_array=new.qq_array, downstream=new.downstream, downstream_type=new.downstream_type, split1=new.split1, split1_type=new.split1_type, split2=new.split2, split2_type=new.split2_type where id=old.id;
            if 'qq_split_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'qq_split_hydrology_singularity');
            update $model._singularity set validity = (select (qq_array is not null) and (downstream is not null) and (split1 is not null) and (array_length(qq_array, 1)<=10) and (array_length(qq_array, 1)>=1) and ((split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)) from  $model._qq_split_hydrology_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._qq_split_hydrology_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'qq_split_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.gate_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'gate', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'gate') where name = 'define_later' and id = id_;

            insert into $model._gate_singularity
                values (id_, 'gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.mode_valve, 'no_valve'), coalesce(new.z_gate, new.z_ceiling), coalesce(new.v_max_cms, .2));
            if 'gate' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._gate_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.mode_valve, old.z_gate, old.v_max_cms) as o, (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._gate_singularity set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, mode_valve=new.mode_valve, z_gate=new.z_gate, v_max_cms=new.v_max_cms where id=old.id;
            if 'gate' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._gate_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._gate_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'gate' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.reservoir_rs_hydrology_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'reservoir_rs_hydrology', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'reservoir_rs_hydrology') where name = 'define_later' and id = id_;

            insert into $model._reservoir_rs_hydrology_singularity
                values (id_, 'reservoir_rs_hydrology', new.drainage, coalesce(new.drainage_type, (select link_type from $model._link where id=new.drainage)), new.overflow, coalesce(new.overflow_type, (select link_type from $model._link where id=new.overflow)), new.q_drainage, coalesce(new.z_ini, new.zs_array[1][1]), new.zs_array, coalesce(new.treatment_mode, 'residual_concentration'), new.treatment_param::json);
            if 'reservoir_rs_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'reservoir_rs_hydrology_singularity');
            update $model._singularity set validity = (select (drainage is not null) and (overflow is not null) and (q_drainage is not null) and (q_drainage>=0) and (z_ini is not null) and (zs_array is not null ) and (treatment_mode is not null) and (array_length(zs_array, 1)<=10) and (array_length(zs_array, 1)>=1) and (array_length(zs_array, 2)=2) from  $model._reservoir_rs_hydrology_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.drainage, old.drainage_type, old.overflow, old.overflow_type, old.q_drainage, old.z_ini, old.zs_array, old.treatment_mode, old.treatment_param) as o, (select new.drainage, new.drainage_type, new.overflow, new.overflow_type, new.q_drainage, new.z_ini, new.zs_array, new.treatment_mode, new.treatment_param) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.drainage, new.drainage_type, new.overflow, new.overflow_type, new.q_drainage, new.z_ini, new.zs_array, new.treatment_mode, new.treatment_param) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._reservoir_rs_hydrology_singularity set drainage=new.drainage, drainage_type=new.drainage_type, overflow=new.overflow, overflow_type=new.overflow_type, q_drainage=new.q_drainage, z_ini=new.z_ini, zs_array=new.zs_array, treatment_mode=new.treatment_mode, treatment_param=new.treatment_param::json where id=old.id;
            if 'reservoir_rs_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'reservoir_rs_hydrology_singularity');
            update $model._singularity set validity = (select (drainage is not null) and (overflow is not null) and (q_drainage is not null) and (q_drainage>=0) and (z_ini is not null) and (zs_array is not null ) and (treatment_mode is not null) and (array_length(zs_array, 1)<=10) and (array_length(zs_array, 1)>=1) and (array_length(zs_array, 2)=2) from  $model._reservoir_rs_hydrology_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._reservoir_rs_hydrology_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'reservoir_rs_hydrology' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.param_headloss_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'param_headloss', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'param_headloss') where name = 'define_later' and id = id_;

            insert into $model._param_headloss_singularity
                values (id_, 'param_headloss', new.q_dz_array);
            if 'param_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'param_headloss_singularity');
            update $model._singularity set validity = (select (q_dz_array is not null) and (array_length(q_dz_array, 1)<=10) and (array_length(q_dz_array, 1)>=1) and (array_length(q_dz_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._param_headloss_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.q_dz_array) as o, (select new.q_dz_array) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.q_dz_array) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._param_headloss_singularity set q_dz_array=new.q_dz_array where id=old.id;
            if 'param_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'param_headloss_singularity');
            update $model._singularity set validity = (select (q_dz_array is not null) and (array_length(q_dz_array, 1)<=10) and (array_length(q_dz_array, 1)>=1) and (array_length(q_dz_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._param_headloss_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._param_headloss_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'param_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.froude_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'froude_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'froude_bc') where name = 'define_later' and id = id_;

            insert into $model._froude_bc_singularity
                values (id_, 'froude_bc');
            if 'froude_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'froude_bc_singularity');
            update $model._singularity set validity = (select 't'::boolean from  $model._froude_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select ) as o, (select ) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select ) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            --update $model._froude_bc_singularity set  where id=old.id;
            if 'froude_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'froude_bc_singularity');
            update $model._singularity set validity = (select 't'::boolean from  $model._froude_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._froude_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'froude_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.tz_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'tz_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'tz_bc') where name = 'define_later' and id = id_;

            insert into $model._tz_bc_singularity
                values (id_, 'tz_bc', new.tz_array, coalesce(new.cyclic, 'f'), coalesce(new.external_file_data, 'f'));
            if 'tz_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'tz_bc_singularity');
            update $model._singularity set validity = (select (external_file_data or tz_array is not null) and (external_file_data or array_length(tz_array, 1)<=10) and (external_file_data or array_length(tz_array, 1)>=1) and (external_file_data or array_length(tz_array, 2)=2) from  $model._tz_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.tz_array, old.cyclic, old.external_file_data) as o, (select new.tz_array, new.cyclic, new.external_file_data) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.tz_array, new.cyclic, new.external_file_data) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._tz_bc_singularity set tz_array=new.tz_array, cyclic=new.cyclic, external_file_data=new.external_file_data where id=old.id;
            if 'tz_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'tz_bc_singularity');
            update $model._singularity set validity = (select (external_file_data or tz_array is not null) and (external_file_data or array_length(tz_array, 1)<=10) and (external_file_data or array_length(tz_array, 1)>=1) and (external_file_data or array_length(tz_array, 2)=2) from  $model._tz_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._tz_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'tz_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.hydrology_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'hydrology_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'hydrology_bc') where name = 'define_later' and id = id_;

            insert into $model._hydrology_bc_singularity
                values (id_, 'hydrology_bc');
            if 'hydrology_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'hydrology_bc_singularity');
            update $model._singularity set validity = (select 't'::boolean from  $model._hydrology_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select ) as o, (select ) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select ) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            --update $model._hydrology_bc_singularity set  where id=old.id;
            if 'hydrology_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'hydrology_bc_singularity');
            update $model._singularity set validity = (select 't'::boolean from  $model._hydrology_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._hydrology_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'hydrology_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.regul_sluice_gate_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'regul_sluice_gate', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'regul_sluice_gate') where name = 'define_later' and id = id_;

            insert into $model._regul_sluice_gate_singularity
                values (id_, 'regul_sluice_gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.z_invert_stop, new.z_invert), coalesce(new.z_ceiling_stop, new.z_ceiling), coalesce(new.v_max_cms, .5), coalesce(new.dt_regul_hr, 0), coalesce(new.mode_regul, 'elevation'), new.z_control_node, coalesce(new.z_pid_array, '{1, 0, 0}'::real[]), new.z_tz_array, new.q_z_crit, new.q_tq_array, coalesce(new.nr_z_gate, new.z_ceiling));
            if 'regul_sluice_gate' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'regul_sluice_gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) from  $model._regul_sluice_gate_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.z_invert_stop, old.z_ceiling_stop, old.v_max_cms, old.dt_regul_hr, old.mode_regul, old.z_control_node, old.z_pid_array, old.z_tz_array, old.q_z_crit, old.q_tq_array, old.nr_z_gate) as o, (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._regul_sluice_gate_singularity set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, z_invert_stop=new.z_invert_stop, z_ceiling_stop=new.z_ceiling_stop, v_max_cms=new.v_max_cms, dt_regul_hr=new.dt_regul_hr, mode_regul=new.mode_regul, z_control_node=new.z_control_node, z_pid_array=new.z_pid_array, z_tz_array=new.z_tz_array, q_z_crit=new.q_z_crit, q_tq_array=new.q_tq_array, nr_z_gate=new.nr_z_gate where id=old.id;
            if 'regul_sluice_gate' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'regul_sluice_gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) from  $model._regul_sluice_gate_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._regul_sluice_gate_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'regul_sluice_gate' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.tank_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'tank_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'tank_bc') where name = 'define_later' and id = id_;

            insert into $model._tank_bc_singularity
                values (id_, 'tank_bc', new.zs_array, new.zini);
            if 'tank_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'tank_bc_singularity');
            update $model._singularity set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10) and (array_length(zs_array, 1)>=1) and (array_length(zs_array, 2)=2) from  $model._tank_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.zs_array, old.zini) as o, (select new.zs_array, new.zini) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.zs_array, new.zini) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._tank_bc_singularity set zs_array=new.zs_array, zini=new.zini where id=old.id;
            if 'tank_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'tank_bc_singularity');
            update $model._singularity set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10) and (array_length(zs_array, 1)>=1) and (array_length(zs_array, 2)=2) from  $model._tank_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._tank_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'tank_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.borda_headloss_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'borda_headloss', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'borda_headloss') where name = 'define_later' and id = id_;

            insert into $model._borda_headloss_singularity
                values (id_, 'borda_headloss', new.law_type, new.param::json);
            if 'borda_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'borda_headloss_singularity');
            update $model._singularity set validity = (select (law_type is not null) and (param is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._borda_headloss_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.law_type, old.param) as o, (select new.law_type, new.param) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.law_type, new.param) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._borda_headloss_singularity set law_type=new.law_type, param=new.param::json where id=old.id;
            if 'borda_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'borda_headloss_singularity');
            update $model._singularity set validity = (select (law_type is not null) and (param is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._borda_headloss_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._borda_headloss_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'borda_headloss' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.constant_inflow_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'constant_inflow_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'constant_inflow_bc') where name = 'define_later' and id = id_;

            insert into $model._constant_inflow_bc_singularity
                values (id_, 'constant_inflow_bc', new.q0);
            if 'constant_inflow_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'constant_inflow_bc_singularity');
            update $model._singularity set validity = (select (q0 is not null) from  $model._constant_inflow_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.q0) as o, (select new.q0) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.q0) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._constant_inflow_bc_singularity set q0=new.q0 where id=old.id;
            if 'constant_inflow_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'constant_inflow_bc_singularity');
            update $model._singularity set validity = (select (q0 is not null) from  $model._constant_inflow_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._constant_inflow_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'constant_inflow_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.weir_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'weir_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'weir_bc') where name = 'define_later' and id = id_;

            insert into $model._weir_bc_singularity
                values (id_, 'weir_bc', new.z_weir, new.width, coalesce(new.cc, .6));
            if 'weir_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'weir_bc_singularity');
            update $model._singularity set validity = (select (z_weir is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0.) from  $model._weir_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_weir, old.width, old.cc) as o, (select new.z_weir, new.width, new.cc) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_weir, new.width, new.cc) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._weir_bc_singularity set z_weir=new.z_weir, width=new.width, cc=new.cc where id=old.id;
            if 'weir_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'weir_bc_singularity');
            update $model._singularity set validity = (select (z_weir is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0.) from  $model._weir_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._weir_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'weir_bc' = 'pipe_branch_marker' then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.pipe_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('pipe', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'pipe') where name = 'define_later' and id = id_;
            insert into $model._pipe_link
                values (id_, 'pipe', new.comment, new.z_invert_up, new.z_invert_down, coalesce(new.cross_section_type, 'circular'), coalesce(new.h_sable, 0), new.branch, new.rk, coalesce(new.custom_length, null), new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom);
            if 'pipe' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'pipe_link');
            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment, old.z_invert_up, old.z_invert_down, old.cross_section_type, old.h_sable, old.branch, old.rk, old.custom_length, old.circular_diameter, old.ovoid_height, old.ovoid_top_diameter, old.ovoid_invert_diameter, old.cp_geom, old.op_geom) as o, (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.branch, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.branch, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._pipe_link set comment=new.comment, z_invert_up=new.z_invert_up, z_invert_down=new.z_invert_down, cross_section_type=new.cross_section_type, h_sable=new.h_sable, branch=new.branch, rk=new.rk, custom_length=new.custom_length, circular_diameter=new.circular_diameter, ovoid_height=new.ovoid_height, ovoid_top_diameter=new.ovoid_top_diameter, ovoid_invert_diameter=new.ovoid_invert_diameter, cp_geom=new.cp_geom, op_geom=new.op_geom where id=old.id;
            if 'pipe' = 'pipe' and (st_intersects(st_startpoint(new.geom), st_startpoint(new.geom))=false or st_intersects(st_endpoint(new.geom), st_endpoint(new.geom))=false) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'pipe_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' and (st_intersects(st_startpoint(new.geom), st_startpoint(new.geom))=false or st_intersects(st_endpoint(new.geom), st_endpoint(new.geom))=false) then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._pipe_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'pipe' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'pipe' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.strickler_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('strickler', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'strickler') where name = 'define_later' and id = id_;
            insert into $model._strickler_link
                values (id_, 'strickler', new.z_crest1, new.width1, coalesce(new.length, ST_Length(new.geom)), coalesce(new.rk, 12), coalesce(new.z_crest2, new.z_crest1+0.001), coalesce(new.width2, new.width1+0.001), new.border);
            if 'strickler' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'strickler_link');
            update $model._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>0) and (length is not null) and (length>0) and (rk is not null) and (rk>=0) and (z_crest2 is not null) and (z_crest2>z_crest1) and (width2 is not null) and (width2>width1) from  $model._strickler_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'strickler' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_crest1, old.width1, old.length, old.rk, old.z_crest2, old.width2, old.border) as o, (select new.z_crest1, new.width1, new.length, new.rk, new.z_crest2, new.width2, new.border) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_crest1, new.width1, new.length, new.rk, new.z_crest2, new.width2, new.border) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._strickler_link set z_crest1=new.z_crest1, width1=new.width1, length=new.length, rk=new.rk, z_crest2=new.z_crest2, width2=new.width2, border=new.border where id=old.id;
            if 'strickler' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'strickler_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'strickler' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>0) and (length is not null) and (length>0) and (rk is not null) and (rk>=0) and (z_crest2 is not null) and (z_crest2>z_crest1) and (width2 is not null) and (width2>width1) from  $model._strickler_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._strickler_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'strickler' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'strickler' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.fuse_spillway_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('fuse_spillway', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'fuse_spillway') where name = 'define_later' and id = id_;
            insert into $model._fuse_spillway_link
                values (id_, 'fuse_spillway', new.z_invert, new.width, coalesce(new.cc, .6), coalesce(new.break_mode, 'none'), new.z_break, new.t_break, coalesce(new.grp, 1), new.dt_fracw_array);
            if 'fuse_spillway' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'fuse_spillway_link');
            update $model._link set validity = (select (z_invert is not null) and (width is not null) and (width>=0.) and (cc is not null) and (cc<=1) and (cc>=0.) and (break_mode is not null) and (break_mode!='zw_critical' or z_break is not null) and (break_mode!='time_critical' or t_break is not null) and (break_mode='none' or grp is not null) and (break_mode='none' or grp>0) and (break_mode='none' or grp<100) and (break_mode='none' or dt_fracw_array is not null) and (break_mode='none' or array_length(dt_fracw_array, 1)<=10) and (break_mode='none' or array_length(dt_fracw_array, 1)>=1) and (break_mode='none' or array_length(dt_fracw_array, 2)=2) from  $model._fuse_spillway_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'fuse_spillway' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.width, old.cc, old.break_mode, old.z_break, old.t_break, old.grp, old.dt_fracw_array) as o, (select new.z_invert, new.width, new.cc, new.break_mode, new.z_break, new.t_break, new.grp, new.dt_fracw_array) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.width, new.cc, new.break_mode, new.z_break, new.t_break, new.grp, new.dt_fracw_array) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._fuse_spillway_link set z_invert=new.z_invert, width=new.width, cc=new.cc, break_mode=new.break_mode, z_break=new.z_break, t_break=new.t_break, grp=new.grp, dt_fracw_array=new.dt_fracw_array where id=old.id;
            if 'fuse_spillway' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'fuse_spillway_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'fuse_spillway' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert is not null) and (width is not null) and (width>=0.) and (cc is not null) and (cc<=1) and (cc>=0.) and (break_mode is not null) and (break_mode!='zw_critical' or z_break is not null) and (break_mode!='time_critical' or t_break is not null) and (break_mode='none' or grp is not null) and (break_mode='none' or grp>0) and (break_mode='none' or grp<100) and (break_mode='none' or dt_fracw_array is not null) and (break_mode='none' or array_length(dt_fracw_array, 1)<=10) and (break_mode='none' or array_length(dt_fracw_array, 1)>=1) and (break_mode='none' or array_length(dt_fracw_array, 2)=2) from  $model._fuse_spillway_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._fuse_spillway_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'fuse_spillway' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'fuse_spillway' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.routing_hydrology_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('routing_hydrology', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'routing_hydrology') where name = 'define_later' and id = id_;
            insert into $model._routing_hydrology_link
                values (id_, 'routing_hydrology', coalesce(new.cross_section, 1), coalesce(new.length, 0.1), coalesce(new.slope, .01), coalesce(new.hydrograph, (select hbc.id from $model.hydrograph_bc_singularity as hbc where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1))));
            if 'routing_hydrology' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'routing_hydrology_link');
            update $model._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  $model._routing_hydrology_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'routing_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.cross_section, old.length, old.slope, old.hydrograph) as o, (select new.cross_section, new.length, new.slope, new.hydrograph) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.cross_section, new.length, new.slope, new.hydrograph) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._routing_hydrology_link set cross_section=new.cross_section, length=new.length, slope=new.slope, hydrograph=new.hydrograph where id=old.id;
            if 'routing_hydrology' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'routing_hydrology_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'routing_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  $model._routing_hydrology_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._routing_hydrology_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'routing_hydrology' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'routing_hydrology' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.deriv_pump_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('deriv_pump', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'deriv_pump') where name = 'define_later' and id = id_;
            insert into $model._deriv_pump_link
                values (id_, 'deriv_pump', new.q_pump, new.qz_array);
            if 'deriv_pump' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'deriv_pump_link');
            update $model._link set validity = (select (q_pump is not null) and (q_pump>= 0) and (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) from  $model._deriv_pump_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'deriv_pump' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.q_pump, old.qz_array) as o, (select new.q_pump, new.qz_array) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.q_pump, new.qz_array) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._deriv_pump_link set q_pump=new.q_pump, qz_array=new.qz_array where id=old.id;
            if 'deriv_pump' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'deriv_pump_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'deriv_pump' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (q_pump is not null) and (q_pump>= 0) and (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) from  $model._deriv_pump_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._deriv_pump_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'deriv_pump' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'deriv_pump' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.porous_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('porous', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'porous') where name = 'define_later' and id = id_;
            insert into $model._porous_link
                values (id_, 'porous', new.z_invert, new.width, new.transmitivity, new.border);
            if 'porous' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'porous_link');
            update $model._link set validity = (select (z_invert is not null) and (width is not null) and (width>=0) and (transmitivity is not null) and (transmitivity>=0) from  $model._porous_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'porous' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.width, old.transmitivity, old.border) as o, (select new.z_invert, new.width, new.transmitivity, new.border) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.width, new.transmitivity, new.border) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._porous_link set z_invert=new.z_invert, width=new.width, transmitivity=new.transmitivity, border=new.border where id=old.id;
            if 'porous' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'porous_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'porous' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert is not null) and (width is not null) and (width>=0) and (transmitivity is not null) and (transmitivity>=0) from  $model._porous_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._porous_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'porous' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'porous' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.weir_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('weir', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'weir') where name = 'define_later' and id = id_;
            insert into $model._weir_link
                values (id_, 'weir', new.z_invert, new.width, coalesce(new.cc, .6), new.z_weir, coalesce(new.v_max_cms, .5));
            if 'weir' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'weir_link');
            update $model._link set validity = (select (z_invert is not null ) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (v_max_cms is not null) and (v_max_cms>=0) from  $model._weir_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'weir' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.width, old.cc, old.z_weir, old.v_max_cms) as o, (select new.z_invert, new.width, new.cc, new.z_weir, new.v_max_cms) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.width, new.cc, new.z_weir, new.v_max_cms) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._weir_link set z_invert=new.z_invert, width=new.width, cc=new.cc, z_weir=new.z_weir, v_max_cms=new.v_max_cms where id=old.id;
            if 'weir' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'weir_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'weir' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert is not null ) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (v_max_cms is not null) and (v_max_cms>=0) from  $model._weir_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._weir_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'weir' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'weir' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.borda_headloss_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('borda_headloss', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'borda_headloss') where name = 'define_later' and id = id_;
            insert into $model._borda_headloss_link
                values (id_, 'borda_headloss', new.law_type, new.param::json);
            if 'borda_headloss' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'borda_headloss_link');
            update $model._link set validity = (select (law_type is not null) and (param is not null) from  $model._borda_headloss_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'borda_headloss' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.law_type, old.param) as o, (select new.law_type, new.param) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.law_type, new.param) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._borda_headloss_link set law_type=new.law_type, param=new.param::json where id=old.id;
            if 'borda_headloss' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'borda_headloss_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'borda_headloss' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (law_type is not null) and (param is not null) from  $model._borda_headloss_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._borda_headloss_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'borda_headloss' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'borda_headloss' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.connector_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('connector', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'connector') where name = 'define_later' and id = id_;
            insert into $model._connector_link
                values (id_, 'connector', coalesce(new.main_branch, 'f'));
            if 'connector' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'connector_link');
            update $model._link set validity = (select 't'::boolean from  $model._connector_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'connector' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.main_branch) as o, (select new.main_branch) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.main_branch) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._connector_link set main_branch=new.main_branch where id=old.id;
            if 'connector' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'connector_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'connector' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select 't'::boolean from  $model._connector_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._connector_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'connector' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'connector' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.pump_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('pump', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'pump') where name = 'define_later' and id = id_;
            insert into $model._pump_link
                values (id_, 'pump', new.npump, new.zregul_array, new.hq_array);
            if 'pump' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'pump_link');
            update $model._link set validity = (select (npump is not null) and (npump<=10) and (npump>=1) and (zregul_array is not null) and (hq_array is not null) and (array_length(zregul_array, 1)=npump) and (array_length(zregul_array, 2)=2) and (array_length(hq_array, 1)=npump) and (array_length(hq_array, 2)<=10) and (array_length(hq_array, 2)>=1) and (array_length(hq_array, 3)=2) from  $model._pump_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'pump' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.npump, old.zregul_array, old.hq_array) as o, (select new.npump, new.zregul_array, new.hq_array) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.npump, new.zregul_array, new.hq_array) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._pump_link set npump=new.npump, zregul_array=new.zregul_array, hq_array=new.hq_array where id=old.id;
            if 'pump' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'pump_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'pump' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (npump is not null) and (npump<=10) and (npump>=1) and (zregul_array is not null) and (hq_array is not null) and (array_length(zregul_array, 1)=npump) and (array_length(zregul_array, 2)=2) and (array_length(hq_array, 1)=npump) and (array_length(hq_array, 2)<=10) and (array_length(hq_array, 2)>=1) and (array_length(hq_array, 3)=2) from  $model._pump_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._pump_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'pump' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'pump' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.street_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('street', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'street') where name = 'define_later' and id = id_;
            insert into $model._street_link
                values (id_, 'street', new.width, coalesce(new.length, ST_Length(new.geom)), coalesce(new.rk, 30.));
            if 'street' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'street_link');
            update $model._link set validity = (select (width is not null) and (width>=0) and (length is not null) and (length>=0) and (rk is not null) and (rk>0) from  $model._street_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'street' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.width, old.length, old.rk) as o, (select new.width, new.length, new.rk) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.width, new.length, new.rk) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._street_link set width=new.width, length=new.length, rk=new.rk where id=old.id;
            if 'street' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'street_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'street' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (width is not null) and (width>=0) and (length is not null) and (length>=0) and (rk is not null) and (rk>0) from  $model._street_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._street_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'street' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'street' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.mesh_2d_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('mesh_2d', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'mesh_2d') where name = 'define_later' and id = id_;
            insert into $model._mesh_2d_link
                values (id_, 'mesh_2d', new.z_invert, coalesce(new.lateral_contraction_coef, 1), coalesce(new.border, $model.inter_pave_border_fct(up_, down_)));
            if 'mesh_2d' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'mesh_2d_link');
            update $model._link set validity = (select (z_invert is not null) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (ST_NPoints(border)=2) and (ST_IsValid(border)) from  $model._mesh_2d_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'mesh_2d' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.lateral_contraction_coef, old.border) as o, (select new.z_invert, new.lateral_contraction_coef, new.border) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.lateral_contraction_coef, new.border) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._mesh_2d_link set z_invert=new.z_invert, lateral_contraction_coef=new.lateral_contraction_coef, border=new.border where id=old.id;
            if 'mesh_2d' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'mesh_2d_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'mesh_2d' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert is not null) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (ST_NPoints(border)=2) and (ST_IsValid(border)) from  $model._mesh_2d_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._mesh_2d_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'mesh_2d' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'mesh_2d' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.gate_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('gate', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'gate') where name = 'define_later' and id = id_;
            insert into $model._gate_link
                values (id_, 'gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.mode_valve, 'no_valve'), coalesce(new.z_gate, new.z_ceiling), coalesce(new.v_max_cms, .2));
            if 'gate' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'gate_link');
            update $model._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) from  $model._gate_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'gate' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.mode_valve, old.z_gate, old.v_max_cms) as o, (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._gate_link set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, mode_valve=new.mode_valve, z_gate=new.z_gate, v_max_cms=new.v_max_cms where id=old.id;
            if 'gate' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'gate_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'gate' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) from  $model._gate_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._gate_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'gate' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'gate' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.overflow_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('overflow', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'overflow') where name = 'define_later' and id = id_;
            insert into $model._overflow_link
                values (id_, 'overflow', new.z_crest1, new.width1, coalesce(new.z_crest2, new.z_crest1), coalesce(new.width2, 0), coalesce(new.cc, .6), coalesce(new.lateral_contraction_coef, 1), coalesce(new.break_mode, 'none'), new.z_break, new.t_break, new.z_invert, new.width_breach, coalesce(new.grp, 1), new.dt_fracw_array, new.border);
            if 'overflow' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'overflow_link');
            update $model._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>=0) and (z_crest2 is not null) and (z_crest2>=z_crest1) and (width2 is not null) and (width2>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (break_mode is not null) and (break_mode!='zw_critical' or z_break is not null) and (break_mode!='time_critical' or t_break is not null) and (break_mode='none' or width_breach is not null) and (break_mode='none' or z_invert is not null) and (break_mode='none' or grp is not null) and (break_mode='none' or grp>0) and (break_mode='none' or grp<100) and (break_mode='none' or dt_fracw_array is not null) and (break_mode='none' or array_length(dt_fracw_array, 1)<=10) and (break_mode='none' or array_length(dt_fracw_array, 1)>=1) and (break_mode='none' or array_length(dt_fracw_array, 2)=2) from  $model._overflow_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'overflow' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_crest1, old.width1, old.z_crest2, old.width2, old.cc, old.lateral_contraction_coef, old.break_mode, old.z_break, old.t_break, old.z_invert, old.width_breach, old.grp, old.dt_fracw_array, old.border) as o, (select new.z_crest1, new.width1, new.z_crest2, new.width2, new.cc, new.lateral_contraction_coef, new.break_mode, new.z_break, new.t_break, new.z_invert, new.width_breach, new.grp, new.dt_fracw_array, new.border) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_crest1, new.width1, new.z_crest2, new.width2, new.cc, new.lateral_contraction_coef, new.break_mode, new.z_break, new.t_break, new.z_invert, new.width_breach, new.grp, new.dt_fracw_array, new.border) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._overflow_link set z_crest1=new.z_crest1, width1=new.width1, z_crest2=new.z_crest2, width2=new.width2, cc=new.cc, lateral_contraction_coef=new.lateral_contraction_coef, break_mode=new.break_mode, z_break=new.z_break, t_break=new.t_break, z_invert=new.z_invert, width_breach=new.width_breach, grp=new.grp, dt_fracw_array=new.dt_fracw_array, border=new.border where id=old.id;
            if 'overflow' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'overflow_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'overflow' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>=0) and (z_crest2 is not null) and (z_crest2>=z_crest1) and (width2 is not null) and (width2>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (break_mode is not null) and (break_mode!='zw_critical' or z_break is not null) and (break_mode!='time_critical' or t_break is not null) and (break_mode='none' or width_breach is not null) and (break_mode='none' or z_invert is not null) and (break_mode='none' or grp is not null) and (break_mode='none' or grp>0) and (break_mode='none' or grp<100) and (break_mode='none' or dt_fracw_array is not null) and (break_mode='none' or array_length(dt_fracw_array, 1)<=10) and (break_mode='none' or array_length(dt_fracw_array, 1)>=1) and (break_mode='none' or array_length(dt_fracw_array, 2)=2) from  $model._overflow_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._overflow_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'overflow' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'overflow' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.regul_gate_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('regul_gate', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'regul_gate') where name = 'define_later' and id = id_;
            insert into $model._regul_gate_link
                values (id_, 'regul_gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.z_invert_stop, new.z_invert), coalesce(new.z_ceiling_stop, new.z_ceiling), coalesce(new.v_max_cms, .5), coalesce(new.dt_regul_hr, 0), coalesce(new.mode_regul, 'elevation'), new.z_control_node, coalesce(new.z_pid_array, '{1, 0, 0}'::real[]), new.z_tz_array, new.q_z_crit, new.q_tq_array, coalesce(new.nr_z_gate, new.z_ceiling));
            if 'regul_gate' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'regul_gate_link');
            update $model._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) from  $model._regul_gate_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'regul_gate' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.z_invert_stop, old.z_ceiling_stop, old.v_max_cms, old.dt_regul_hr, old.mode_regul, old.z_control_node, old.z_pid_array, old.z_tz_array, old.q_z_crit, old.q_tq_array, old.nr_z_gate) as o, (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._regul_gate_link set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, z_invert_stop=new.z_invert_stop, z_ceiling_stop=new.z_ceiling_stop, v_max_cms=new.v_max_cms, dt_regul_hr=new.dt_regul_hr, mode_regul=new.mode_regul, z_control_node=new.z_control_node, z_pid_array=new.z_pid_array, z_tz_array=new.z_tz_array, q_z_crit=new.q_z_crit, q_tq_array=new.q_tq_array, nr_z_gate=new.nr_z_gate where id=old.id;
            if 'regul_gate' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'regul_gate_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'regul_gate' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) from  $model._regul_gate_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._regul_gate_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'regul_gate' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'regul_gate' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.connector_hydrology_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('connector_hydrology', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'connector_hydrology') where name = 'define_later' and id = id_;
            insert into $model._connector_hydrology_link
                values (id_, 'connector_hydrology', coalesce(new.hydrograph, (select hbc.id from $model.hydrograph_bc_singularity as hbc where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1))));
            if 'connector_hydrology' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'connector_hydrology_link');
            update $model._link set validity = (select (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  $model._connector_hydrology_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'connector_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.hydrograph) as o, (select new.hydrograph) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.hydrograph) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._connector_hydrology_link set hydrograph=new.hydrograph where id=old.id;
            if 'connector_hydrology' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'connector_hydrology_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'connector_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  $model._connector_hydrology_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._connector_hydrology_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'connector_hydrology' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'connector_hydrology' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace function ${model}.network_overflow_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('network_overflow', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'network_overflow') where name = 'define_later' and id = id_;
            insert into $model._network_overflow_link
                values (id_, 'network_overflow', coalesce(new.z_overflow, st_z(st_endpoint(new.geom))), coalesce(new.area, 1));
            if 'network_overflow' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'network_overflow_link');
            update $model._link set validity = (select (z_overflow is not null) and (area is not null) and (area>=0) from  $model._network_overflow_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'network_overflow' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_overflow, old.area) as o, (select new.z_overflow, new.area) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_overflow, new.area) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._network_overflow_link set z_overflow=new.z_overflow, area=new.area where id=old.id;
            if 'network_overflow' = 'pipe' then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'network_overflow_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'network_overflow' = 'pipe' then
                update $model.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_overflow is not null) and (area is not null) and (area>=0) from  $model._network_overflow_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._network_overflow_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'network_overflow' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'network_overflow' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;

$$$$
;;

create or replace view $model.configured as
    select CONCAT('node:', n.id) as id,
        configuration,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom
        from $model._node as n
        where configuration is not null
    union all
    select CONCAT('link:', l.id) as id,
        configuration,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom
        from $model._link as l
        where configuration is not null
    union all
    select CONCAT('singularity:', s.id) as id,
        s.configuration,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from $model._singularity as s join $model._node as n on s.id = n.id
        where s.configuration is not null
;;

