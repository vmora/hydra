/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Update version                                */
/* ********************************************** */

update ${model}.metadata set version = '1.2.1';
;;

/* ********************************************** */
/*  Update configs                                */
/* ********************************************** */

-- Trigger to remove properly config
create or replace function ${model}.config_before_delete_fct()
returns trigger
language plpgsql
as $$$$
    begin
        update $model._node set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._node set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._link set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._link set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._singularity set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._singularity set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        return old;
    end;
$$$$
;;

create trigger ${model}_config_before_delete_trig
    before delete on $model.configuration
       for each row execute procedure ${model}.config_before_delete_fct()
;;

create or replace function ${model}.metadata_configuration_after_update_fct()
returns trigger
language plpgsql
as $$$$
    declare
        node record;
        singularity record;
        link record;
        update_fields varchar;
        update_arrays varchar;
        json_fields varchar;
        config_name varchar;
    begin
        update $model.config_switch set is_switching=true;

        -- Update nodes that have a configuration
        select name from $model.configuration where id=new.configuration into config_name;
        for node in select * from $model._node where configuration is not null loop
            select name from $model.configuration where id=new.configuration into config_name;
            if config_name not in (select k from json_object_keys(node.configuration) as k) then
                select 'default' into config_name;
            end if;

            -- json switch must be done first to handle trigger on other fields with correct values
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type = 'json'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
            into json_fields;

            if json_fields is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||json_fields||' where id='||node.id::varchar||';';
            end if;

            -- user defined types
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type = 'USER-DEFINED'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||update_fields||' where id='||node.id::varchar||';';
            end if;

            -- common fields
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type != 'USER-DEFINED'
            and data_type != 'ARRAY'
            and data_type != 'json'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||update_fields||' where id='||node.id::varchar||';';
            end if;

            -- assume that all arrays are real[] (it is the case in v1.0.0)
            select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type = 'ARRAY'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
            into update_arrays;

            if update_arrays is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||update_arrays||' where id='||node.id::varchar||';';
            end if;
        end loop;

        -- Update singularities that have a configuration
        for singularity in select * from $model._singularity where configuration is not null loop
            select name from $model.configuration where id=new.configuration into config_name;
            if config_name not in (select k from json_object_keys(singularity.configuration) as k) then
                select 'default' into config_name;
            end if;

            -- json switch must be done first to handle trigger on other fields with correct values
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type = 'json'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
            into json_fields;

            if json_fields is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||json_fields||' where id='||singularity.id::varchar||';';
            end if;

            -- user defined types
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type = 'USER-DEFINED'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||update_fields||' where id='||singularity.id::varchar||';';
            end if;

            -- common fields
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type != 'USER-DEFINED'
            and data_type != 'ARRAY'
            and data_type != 'json'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||update_fields||' where id='||singularity.id::varchar||';';
            end if;

            -- assume that all arrays are real[] (it is the case in v1.0.0)
            select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type = 'ARRAY'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
            into update_arrays;

            if update_arrays is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||update_arrays||' where id='||singularity.id::varchar||';';
            end if;
        end loop;

        -- Update links that have a configuration
        for link in select * from $model._link where configuration is not null loop
            select name from $model.configuration where id=new.configuration into config_name;
            if config_name not in (select k from json_object_keys(link.configuration) as k) then
                select 'default' into config_name;
            end if;

            -- json switch must be done first to handle trigger on other fields with correct values
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type = 'json'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
            into json_fields;

            if json_fields is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||json_fields||' where id='||link.id::varchar||';';
            end if;

            -- user defined types
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type = 'USER-DEFINED'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||update_fields||' where id='||link.id::varchar||';';
            end if;

            -- common fields
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type != 'USER-DEFINED'
            and data_type != 'ARRAY'
            and data_type != 'json'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||update_fields||' where id='||link.id::varchar||';';
            end if;

            -- assume that all arrays are real[] (it is the case in v1.0.0)
            select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type = 'ARRAY'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
            into update_arrays;

            if update_arrays is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||update_arrays||' where id='||link.id::varchar||';';
            end if;
        end loop;

        update $model.config_switch set is_switching=false;
        return new;
    end;
$$$$
;;

create or replace view $model.configured_current as
    with config as (select c.name from $model.configuration as c, $model.metadata as m where m.configuration=c.id)
    select CONCAT('node:', n.id) as id,
        configuration,
        n.name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom
        from $model._node as n, config as c
        where exists (select 1 from json_object_keys(n.configuration) where json_object_keys = c.name limit 1)
    union all
    select CONCAT('link:', l.id) as id,
        configuration,
        l.name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom
        from $model._link as l, config as c
        where exists (select 1 from json_object_keys(l.configuration) where json_object_keys = c.name limit 1)
    union all
    select CONCAT('singularity:', s.id) as id,
        s.configuration,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from config as c, $model._singularity as s join $model._node as n on s.node = n.id
        where exists (select 1 from json_object_keys(s.configuration) where json_object_keys = c.name limit 1);
;;

/* Update view configured current (don't show configured items in default config) */
create or replace view $model.configured_current as
    with config as (select c.name from $model.configuration as c, $model.metadata as m where m.configuration=c.id)
    select CONCAT('node:', n.id) as id,
        configuration,
        n.name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom
        from $model._node as n, config as c
        where exists (select 1 from json_object_keys(n.configuration) where json_object_keys = c.name limit 1) and c.name != 'default'
    union all
    select CONCAT('link:', l.id) as id,
        configuration,
        l.name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom
        from $model._link as l, config as c
        where exists (select 1 from json_object_keys(l.configuration) where json_object_keys = c.name limit 1) and c.name != 'default'
    union all
    select CONCAT('singularity:', s.id) as id,
        s.configuration,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from config as c, $model._singularity as s join $model._node as n on s.node = n.id
        where exists (select 1 from json_object_keys(s.configuration) where json_object_keys = c.name limit 1) and c.name != 'default';
;;


/* ********************************************** */
/*  Bug fix: Catchment node insert                */
/* ********************************************** */

create or replace function ${model}.catchment_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('catchment', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'catchment') where name = 'define_later' and id = id_;
            insert into $model._catchment_node (id, node_type, area_ha, rl, slope, c_imp, netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef, holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm, scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, permeable_soil_j_mm, permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange, runoff_type, socose_tc_mn, socose_shape_param_beta, define_k_mn, q_limit, q0, contour, catchment_pollution_mode, catchment_pollution_param)
                values (id_, 'catchment', new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, coalesce(new.q_limit, 9999), coalesce(new.q0, 0), coalesce(new.contour, (select id from $model.catchment where st_intersects(geom, new.geom))), new.catchment_pollution_mode, new.catchment_pollution_param::json);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'catchment' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)) and (runoff_type is not null) and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0)) and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)) and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0)) and (q_limit is not null) and (q0 is not null) from  $model._catchment_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.catchment_pollution_mode, new.catchment_pollution_param) is distinct from (old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.catchment_pollution_mode, old.catchment_pollution_param)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.catchment_pollution_mode, old.catchment_pollution_param) as o, (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.catchment_pollution_mode, new.catchment_pollution_param) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.catchment_pollution_mode, new.catchment_pollution_param) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._catchment_node set area_ha=new.area_ha, rl=new.rl, slope=new.slope, c_imp=new.c_imp, netflow_type=new.netflow_type, constant_runoff=new.constant_runoff, horner_ini_loss_coef=new.horner_ini_loss_coef, horner_recharge_coef=new.horner_recharge_coef, holtan_sat_inf_rate_mmh=new.holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh=new.holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm=new.holtan_soil_storage_cap_mm, scs_j_mm=new.scs_j_mm, scs_soil_drainage_time_day=new.scs_soil_drainage_time_day, scs_rfu_mm=new.scs_rfu_mm, permeable_soil_j_mm=new.permeable_soil_j_mm, permeable_soil_rfu_mm=new.permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate=new.permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day=new.permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange=new.permeable_soil_coef_river_exchange, runoff_type=new.runoff_type, socose_tc_mn=new.socose_tc_mn, socose_shape_param_beta=new.socose_shape_param_beta, define_k_mn=new.define_k_mn, q_limit=new.q_limit, q0=new.q0, contour=new.contour, catchment_pollution_mode=new.catchment_pollution_mode, catchment_pollution_param=new.catchment_pollution_param::json where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'catchment' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'catchment' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)) and (runoff_type is not null) and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0)) and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)) and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0)) and (q_limit is not null) and (q0 is not null) from  $model._catchment_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'catchment' = 'crossroad' and (select trigger_street_link from $model.metadata) then
                perform $model.update_street_links();
            end if;

            delete from $model._catchment_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

/* ********************************************** */
/*  Bug fix: SRID set to 9999 in V 1.2.0          */
/* ********************************************** */

create or replace function ${model}.elem_2d_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('elem_2d', coalesce(new.name, 'define_later'), ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata)), new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'elem_2d') where name = 'define_later' and id = id_;
            insert into $model._elem_2d_node
                values (id_, 'elem_2d', coalesce(new.area, ST_Area(new.contour)), coalesce(new.zb, (select ST_Z(ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata))))), coalesce(new.rk, 12), new.domain_2d, new.contour);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'elem_2d_node');

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'elem_2d' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.zb, new.rk) is distinct from (old.area, old.zb, old.rk)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.zb, old.rk) as o, (select new.area, new.zb, new.rk) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.zb, new.rk) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata)), generated=new.generated where id=old.id;
            update $model._elem_2d_node set area=new.area, zb=new.zb, rk=new.rk, domain_2d=new.domain_2d, contour=new.contour where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'elem_2d_node');

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'elem_2d' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'elem_2d' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'elem_2d' = 'crossroad' and (select trigger_street_link from $model.metadata) then
                perform $model.update_street_links();
            end if;

            delete from $model._elem_2d_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;
