# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
import tempfile
from subprocess import Popen, PIPE
from hydra.project import Project
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.empty_ui_manager import EmptyUIManager
from hydra.utility.log import LogManager, ConsoleLogger

__tempdir = tempfile.gettempdir()

__sql_dump_header ="""
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;
SET session_replication_role = replica;

"""


def __get_parameters():
    return SettingsProperties.get_path('postgre'), SettingsProperties.get_db(), SettingsProperties.get_postgre_settings()

def __execute(arguments):
    process = Popen(arguments, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
    output, error = process.communicate()
    exit_code = process.returncode
    return exit_code, output, error

def version_from_dump(file_name):
    with open(file_name, 'r') as fil:
        for line in fil:
            match = re.search(r"CREATE EXTENSION IF NOT EXISTS hydra WITH SCHEMA public VERSION '(\w+(\.\d+)*)';", line)
            if match:
                return match.group(1)
    return ''

def srid_from_dump(file_name):
    with open(file_name, 'r') as fil:
        for line in fil:
            match0 = re.search(r"SRID=(\d+)", line)
            if match0:
                return int(match0.group(1))
            match1 = re.search(r"geometry\('POINTZ',(\d+)\)", line)
            if match1:
                return int(match1.group(1))
    return ''

def dump_project(file_name, project_name):
    __postgrepath, __db, __pg = __get_parameters()
    pg_dump = os.path.join(__postgrepath, 'bin', 'pg_dump')
    args = [pg_dump,'-h', __db['host'], '-p', str(__db['port']), '-U', __pg['user'], '-f', file_name, '-d', project_name]
    exit_code, output, error = __execute(args)

    # Version continuity !
    prj = Project(LogManager(ConsoleLogger(), "Hydra"), EmptyUIManager(), project_name)
    version, = prj.execute("""select version from hydra.metadata""").fetchone()
    replacements = {"""CREATE EXTENSION IF NOT EXISTS hydra WITH SCHEMA public;""":
                    """CREATE EXTENSION IF NOT EXISTS hydra WITH SCHEMA public VERSION '{}';""".format(version)}
    __replace_str(file_name, replacements)

    return exit_code, output, error

def temp_dump_project(project_name):
    __postgrepath, __db, __pg = __get_parameters()
    temp_file = os.path.join(__tempdir, "_"+project_name+".save.sql")
    exit_code, output, error = dump_project(temp_file, project_name)
    return temp_file, exit_code, output, error

def restore_project(file_name, project_name):
    __postgrepath, __db, __pg = __get_parameters()
    psql = os.path.join(__postgrepath, 'bin', 'psql')
    args = [psql,'-h', __db['host'], '-p', str(__db['port']), '-U', __pg['user'], '-f', file_name, '-d', project_name]
    exit_code, output, error = __execute(args)
    return exit_code, output, error

def import_model_in_project(project_name, model_name, file_name):
    __postgrepath, __db, __pg = __get_parameters()
    psql = os.path.join(__postgrepath, 'bin', 'psql')
    model_file = __extract_model_dump(file_name, model_name)
    args = [psql,'-h', __db['host'], '-p', str(__db['port']), '-U', __pg['user'], '-d', project_name, '-f', model_file]
    exit_code, output, error = __execute(args)
    return exit_code, output, error

def create_db(db_name):
    __postgrepath, __db, __pg = __get_parameters()
    create = os.path.join(__postgrepath, 'bin', 'createdb')
    args = [create,'-h', __db['host'], '-p', str(__db['port']), '-U', __pg['user'], db_name]
    exit_code, output, error = __execute(args)
    return exit_code, output, error

def copy_project(project_name, new_project_name):
    __postgrepath, __db, __pg = __get_parameters()
    temp_file = os.path.join(__tempdir, "_"+project_name+".save.sql")
    dump_exit_code, dump_output, dump_error = dump_project(temp_file, project_name)
    if dump_exit_code != 0:
        return dump_exit_code, dump_output, dump_error
    else:
        createdb_exit_code, createdb_output, createdb_error = create_db(new_project_name)
        if createdb_exit_code != 0:
            return createdb_exit_code, createdb_output, createdb_error
        else:
            restore_exit_code, restore_output, restore_error = restore_project(temp_file, new_project_name)
            return restore_exit_code, restore_output, restore_error

def __replace_str(file, dic):
    lines = []
    with open(file) as infile:
        for line in infile:
            for src, target in dic.iteritems():
                line = line.replace(src, target)
            lines.append(line)
    with open(file, 'w') as outfile:
        for line in lines:
            outfile.write(line)

def __extract_model_dump(file, model):
    with open(file, 'r') as fil:
        data = fil.read()
        pattern = re.compile(r"(SET search_path = {}, pg_catalog;[- \n]*Data for Name:.*SELECT pg_catalog\.setval\('valley_cross_section_topo_geometry_id_seq', \d*, (true|false)\);)".format(model), re.DOTALL)
        match = re.search(pattern, data)
        if match:
            temp_file = os.path.join(__tempdir, "_"+model+".sql")
            with open(temp_file, 'w')  as f:
                f.write(__sql_dump_header)
                f.write(str(match.group(1)))
                f.write("\n\n")
            return temp_file
    raise HydraFileError("Error with file {}".format(file))

class HydraFileError(Exception):
    pass



