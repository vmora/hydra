/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Update borda sing types                       */
/* ********************************************** */

delete from hydra.borda_headloss_singularity_type where name ='diffuser' or name='transition_edge_inlet';
;;

update hydra.borda_headloss_singularity_type set id=18 where name='sharp_bend_rectangular';
update hydra.borda_headloss_singularity_type set id=17 where name='parametric';
update hydra.borda_headloss_singularity_type set id=16 where name='friction';
update hydra.borda_headloss_singularity_type set id=15 where name='screen_oblique_to_flow';
update hydra.borda_headloss_singularity_type set id=14 where name='screen_normal_to_flow';
update hydra.borda_headloss_singularity_type set id=13 where name='sharp_angle_bend';
update hydra.borda_headloss_singularity_type set id=12 where name='circular_bend';
update hydra.borda_headloss_singularity_type set id=11 where name='enlargment_with_transition';
update hydra.borda_headloss_singularity_type set id=10 where name='contraction_with_transition';
update hydra.borda_headloss_singularity_type set id=9 where name='sharp_transition_geometry';
;;

/* ********************************************** */
/*  Update version                                */
/* ********************************************** */

update hydra.metadata set version = '0.0.29';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;