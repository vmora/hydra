# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run update tests on a project from V0.0.27

Warning : you need to have hydra postgresql in version 0.0.27 installed !

USAGE

   update_test.py [-hk]

OPTIONS

   -h, --help
        print this help

   -k, --keep
        does not delete test database after update

"""

assert __name__ == "__main__"

import sys
import os
import getopt
import psycopg2
from .update import update_project
from .dump_restore import create_db, restore_project
from ..database import database as dbhydra
from ..project import Project
from ..utility.empty_ui_manager import EmptyUIManager
from ..utility.log import LogManager, ConsoleLogger
from ..utility.settings_properties import SettingsProperties

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

db = SettingsProperties.get_db()
pg = SettingsProperties.get_postgre_settings()
log = LogManager(ConsoleLogger(), "Hydra")

__currendir = os.path.dirname(__file__)
dic = {"updt_river_test":[["global"], 27561], "updt_wastewater_test":[["alc", "mrs", "pdc"], 3944]}

for project, properties in dic.iteritems():
    if dbhydra.project_exists(project):
        dbhydra.remove_project(project)
    log.notice("Creating database {} in version 0.0.27".format(project))
    create_db(project)

    log.notice("Uploading data")
    current_dir = os.path.abspath(os.path.dirname(__file__))
    test_data = os.path.join(current_dir, "test_data", project+".sql")
    restore_project(test_data, project)

    conn = psycopg2.connect(database=project, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
    cur = conn.cursor()
    cur.execute("""alter table hydra.metadata disable trigger hydra_metadata_after_insert_trig;
                                       insert into hydra.metadata(srid, workspace) values ({}, '{}');
                                       alter table hydra.metadata enable trigger hydra_metadata_after_insert_trig;""".format(properties[1], os.path.join(os.path.expanduser("~"), ".hydra")))
    conn.commit()
    conn.close()

    test_project = Project(log, EmptyUIManager(), project)

    assert test_project.get_version()=='0.0.27'
    update_project(test_project)
    assert test_project.get_version()==dbhydra.data_version()
    log.notice("Final version: {}".format(test_project.get_version()))

    if not keep:
        dbhydra.remove_project(project)
        sys.stdout.write("    Deleting database {}...\n".format(project))

sys.stdout.write("ok\n")