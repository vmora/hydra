/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*              Street regeneration               */
/* ********************************************** */

create or replace function ${model}.gen_cst_street(street_id integer)
returns integer
language plpgsql
as $$$$
    declare
        _street_geom geometry;
        _street_width real;
        _street_el real;
    begin
        select geom, width, elem_length
        from $model.street
        where id=street_id
        into _street_geom, _street_width, _street_el;

        with buf as (
                select st_buffer(st_force2d(_street_geom), _street_width/2, 'quad_segs=1 join=mitre mitre_limit='||(_street_width*2)::varchar) as geom
            ),
            cst as (
                select st_exteriorring(geom) as geom from buf
            ),
            snp as (
                update $model.constrain set geom = st_snap(geom, _street_geom, 1)
                where ST_DWithin(geom, _street_geom, 1)
            ),
            spl as (
                select coalesce(
                    st_split(cst.geom, (select st_collect(st_force2d(c.geom))
                            from $model.constrain as c
                            where st_intersects(c.geom, cst.geom)
                            )), cst.geom) as geom
                from cst
            ),
            spld as (
                select st_force3d((st_dump(geom)).geom) as geom from spl
            ),
            diff as (
                select id, (st_dump(st_collectionextract(st_split(c.geom, b.geom), 2))).geom as geom
                from $model.constrain as c, cst as b
                where st_intersects(b.geom, c.geom)
            ),
            ins as (
                insert into $model.constrain(geom, elem_length, constrain_type)
                select d.geom, c.elem_length, c.constrain_type
                from $model.constrain as c, diff as d
                where c.id = d.id
                union
                select geom, _street_el, 'overflow'
                from spld
                returning id
            )
            delete from $model.constrain where id in (select id from diff);
            return 1;
    end;
$$$$
;;

/* ********************************************** */
/*  Network overflow link creation                */
/* ********************************************** */

create or replace function ${model}.create_network_overflow_links(domain_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_network_overflow_links
    return create_network_overflow_links(plpy, domain_id, '$model', $srid)
$$$$
;;

/* ********************************************** */
/*  Custom length for pipes                       */
/* ********************************************** */

alter table ${model}._pipe_link add column if not exists custom_length real default null
;;

create or replace view ${model}.pipe_link as
 select p.id,
    p.name,
    p.up,
    p.down,
    c.comment,
    c.z_invert_up,
    c.z_invert_down,
    c.cross_section_type,
    c.h_sable,
    c.branch,
    c.rk,
    c.circular_diameter,
    c.ovoid_height,
    c.ovoid_top_diameter,
    c.ovoid_invert_diameter,
    c.cp_geom,
    c.op_geom,
    p.geom,
    p.configuration::character varying as configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration as configuration_json,
    (select case
    when (c.z_invert_up-c.z_invert_down)*st_length(p.geom)>0
    and st_length(p.geom)<>0
    then
        (select case when c.cross_section_type='circular' then
            (select case when c.circular_diameter>0 then
                (with n as (select
                    (pi()/4*pow(c.circular_diameter, 2.0)) as s,
                    (pi()*c.circular_diameter) as p,
                    c.rk as k)
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n))
            else -999
            end)
        when c.cross_section_type='ovoid' then
            (with n as (select
                (pi()/8*(c.ovoid_top_diameter*c.ovoid_top_diameter+c.ovoid_invert_diameter*c.ovoid_invert_diameter)+0.5*(c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))*(c.ovoid_top_diameter+c.ovoid_invert_diameter)) as s,
                (pi()/2*(c.ovoid_top_diameter+c.ovoid_invert_diameter)+2*(c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))) as p,
                c.rk as k)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        when c.cross_section_type='pipe' then
            (with n as (select ${model}.geometric_calc_s_fct(cpg.zbmin_array) as s,
                    ${model}.cp_geometric_calc_p_fct(cpg.zbmin_array) as p,
                    c.rk as k
                from ${model}.closed_parametric_geometry cpg
                where c.cp_geom = cpg.id)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        when c.cross_section_type='channel' then
            (with n as (select ${model}.geometric_calc_s_fct(opg.zbmin_array) as s,
                    ${model}.op_geometric_calc_p_fct(opg.zbmin_array) as p,
                    c.rk as k
                from ${model}.open_parametric_geometry opg
                where c.op_geom = opg.id)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        else 0.0
        end)
    else -999
    end) as qcap,
    (select case when c.cross_section_type='circular' then
        (select (c.z_invert_up+c.circular_diameter))
    when c.cross_section_type='ovoid' then
        (select (c.z_invert_up+c.ovoid_height))
    when c.cross_section_type='pipe' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.closed_parametric_geometry cpg
            where c.cp_geom = cpg.id)
        (select (c.z_invert_up + n.height) from n))
    when c.cross_section_type='channel' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.open_parametric_geometry opg
            where c.op_geom = opg.id)
        (select (c.z_invert_up + n.height) from n))
    else null
    end) as z_vault_up,
    (select case when c.cross_section_type='circular' then
        (select (c.z_invert_down+c.circular_diameter))
    when c.cross_section_type='ovoid' then
        (select (c.z_invert_down+c.ovoid_height))
    when c.cross_section_type='pipe' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.closed_parametric_geometry cpg
            where c.cp_geom = cpg.id)
        (select (c.z_invert_down + n.height) from n))
    when c.cross_section_type='channel' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.open_parametric_geometry opg
            where c.op_geom = opg.id)
        (select (c.z_invert_down + n.height) from n))
    else null
    end) as z_vault_down,
    (select case when c.cross_section_type='channel' then
        (select (c.z_invert_up))::double precision
    else (select z_ground from ${model}.manhole_node where p.up = id)::double precision
    end) as z_tn_up,
    (select case when c.cross_section_type='channel' then
        (select (c.z_invert_down))::double precision
    else (select z_ground from ${model}.manhole_node where p.down = id)::double precision
    end) as z_tn_down,
    c.custom_length

    from ${model}._pipe_link c,
    ${model}._link p
where p.id = c.id
;;

create or replace function ${model}.pipe_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into ${model}._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('pipe', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update ${model}._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'pipe') where name = 'define_later' and id = id_;
            insert into ${model}._pipe_link
                values (id_, 'pipe', new.comment, new.z_invert_up, new.z_invert_down, coalesce(new.cross_section_type, 'circular'), coalesce(new.h_sable, 0), new.branch, new.rk, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom, new.custom_length);
            if 'pipe' = 'pipe' then
                perform ${model}.branch_update_fct();
            end if;
            --perform ${model}.set_link_altitude(id_);
            perform ${model}.add_configuration_fct(new.configuration::json, id_, 'pipe_link');
            update ${model}._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  ${model}._pipe_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update ${model}._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update ${model}._pipe_link set comment=new.comment, z_invert_up=new.z_invert_up, z_invert_down=new.z_invert_down, cross_section_type=new.cross_section_type, h_sable=new.h_sable, branch=new.branch, rk=new.rk, custom_length=new.custom_length, circular_diameter=new.circular_diameter, ovoid_height=new.ovoid_height, ovoid_top_diameter=new.ovoid_top_diameter, ovoid_invert_diameter=new.ovoid_invert_diameter, cp_geom=new.cp_geom, op_geom=new.op_geom where id=old.id;
            if 'pipe' = 'pipe' then
               perform ${model}.branch_update_fct();
            end if;
            perform ${model}.add_configuration_fct(new.configuration::json, old.id, 'pipe_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update ${model}._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  ${model}._pipe_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from ${model}._pipe_link where id=old.id;
            delete from ${model}._link where id=old.id;
            if 'pipe' = 'pipe' then
               perform ${model}.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$$$;;
