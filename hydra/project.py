# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
hydra module to handle project
"""
import os
import sys
import psycopg2
from PyQt4.QtCore import QCoreApplication
from hydra.database import database as dbhydra
from hydra.model import Model
from hydra.database.terrain import Terrain
from hydra.database.radar_rain import RadarRain
from hydra.utility import sql_json
from hydra.utility import empty_ui_manager
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.utility.settings_properties import SettingsProperties

def tr(msg):
    return unicode(QCoreApplication.translate("Hydra", msg))

class Project(object):
    @staticmethod
    def create_new_project(working_dir, project_name, srid, log_manager = LogManager(ConsoleLogger(), "Hydra"), ui_manager = empty_ui_manager.EmptyUIManager()):
        dbhydra.create_project(working_dir, project_name, srid)
        project = Project(log_manager, ui_manager, project_name)
        log_manager.notice(tr("Project created."))
        return project

    @staticmethod
    def load_project(project_name, log_manager = LogManager(ConsoleLogger(), "Hydra"), ui_manager = empty_ui_manager.EmptyUIManager()):
        project = Project(log_manager, ui_manager, project_name)
        log_manager.notice(tr("Project loaded."))
        return project

    def __getattr__(self, att):
        if att=='name':
            return self.__current_project
        elif att=='srid':
            return self.__srid
        elif att == 'version':
            return self.get_version()
        elif att == 'workspace':
            return self.get_workspace()
        elif att == 'directory':
            return self.get_project_dir()
        else:
            raise AttributeError

    def __init__(self, log_manager, ui_manager, project_name):
        self.log = log_manager
        self.__ui_manager = ui_manager
        self.__current_project =  project_name
        self.__current_model =  None
        self.__current_scenario = None
        db_name = project_name
        db = SettingsProperties.get_db()
        pg = SettingsProperties.get_postgre_settings()
        try:
            self.__conn = psycopg2.connect(database=db_name, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
            self.__cur = self.__conn.cursor()
        except:
            raise Exception(tr('Database ') + project_name + tr(' not found. You should use create_new_projet/open_project instead of constructor.'))

        self.__project_file = self.get_filepath()
        query = self.execute("""
            select srid from geometry_columns
            where f_table_schema='project'
            limit 1
            """).fetchone()

        if query is not None:
            self.__srid = query[0]
        else:
            self.__srid = 0

        if not os.path.exists(self.directory):
            os.mkdir(self.directory)

        self.__terrain = Terrain(os.path.join(self.directory, 'terrain'), self)
        self.__radar_rain = RadarRain(os.path.join(self.directory, 'rain'), self)

    def __qgs_dir(self):
        return os.path.join(dbhydra.project_dir(self.__current_project), self.__current_project+".qgs")

    def database_version(self):
        return self.execute("select version from hydra.metadata").fetchone()[0]

    def get_senario_path(self, id_scenario):
        name_scenario, =  self.execute("""
            select name
            from project.scenario
            where id=%d
            """ %(int(id_scenario))).fetchone()
        return self.get_senario_path_from_name(name_scenario)

    def get_senario_path_from_name(self, name_scenario):
        return os.path.join(self.get_project_dir(), name_scenario)

    def open_in_ui(self, qgs_already_opened = False):
        if self.get_version() != dbhydra.data_version():
            raise RuntimeError(
                    tr('Data model in %s is version %s while the database module is version %s')%(
                    self.__current_project, self.get_version(), dbhydra.data_version()))

        if not(os.path.isfile(self.__project_file)):
            self.log.notice(str(tr("New file created.")))
            self.__ui_manager.new_project()
            self.save()
            for model_name in self.get_models():
                self.__ui_manager.add_vector_layers(self.__current_project, model_name, "layers_model.json")
        if not qgs_already_opened:
            self.__ui_manager.open_project(self.__project_file)
        if not self.__ui_manager.group_project_exists():
            self.__ui_manager.add_vector_layers(self.__current_project, "project", "layers_project.json")
        self.log.notice(str(tr("Project {0} opened.")).format(self.__project_file))

    def build_vrt(self, file, data_srid=None):
        vrt = self.__terrain.build_vrt(file, data_srid)
        return vrt

    def build_rain_multivrt(self, name, file, template, data_srid=None):
        vrt = self.__radar_rain.build_vrt(name, file, template, data_srid)
        return vrt

    def save(self):
        self.__ui_manager.save_project(self.__project_file)
        self.log.notice(str(tr("File {filename} saved.")).format(filename = self.__project_file))

    def close(self):
        self.__conn.close()

    def delete_model(self, model_name):
        self.__ui_manager.remove_group_layers(model_name)
        dbhydra.remove_model(self.__cur, str(model_name))
        self.__conn.commit()

    def add_new_model(self, model_name, empty=False):
        if self.get_current_project() == None:
            raise Exception(tr('Project needed. You should use create_new_projet to create a new project.'))
        else:
            dbhydra.remove_model(self.__cur, str(model_name))
            self.__conn.commit()
            dbhydra.create_model(self.__cur, str(model_name), self.__srid, empty)
            self.__conn.commit()
            self.__ui_manager.add_vector_layers(self.__current_project, model_name, "layers_model.json")
            self.log.notice(str(tr("Model {0} created.")).format(model_name))

    def add_new_scenario(self):
        return dbhydra.create_scenario(self.__cur)

    def reload_project_layers(self):
        self.__ui_manager.remove_group_layers('project')
        self.__ui_manager.add_vector_layers(self.__current_project, "project", "layers_project.json")

    def reload_model_layers(self, model_name, arrows=False):
        if self.get_current_model() != None:
            self.__ui_manager.remove_group_layers(model_name)
            self.__ui_manager.add_vector_layers(self.__current_project, model_name, "layers_model.json", arrows)

    def reload_terrain_layers(self):
        self.__ui_manager.remove_group_layers('terrain')
        terrain_files = self.execute("""select source from project.dem order by priority""").fetchall()
        self.__ui_manager.add_terrain_layers(self.__current_project, [self.unpack_path(f) for f, in terrain_files])

    def reload_rain_layers(self):
        self.__ui_manager.remove_group_layers('rain')
        rain_files = self.execute("""select file from project.radar_rainfall""").fetchall()
        self.__ui_manager.add_rain_layers(self.__current_project, [self.unpack_path(f) for f, in rain_files])

    def load_csv_results_layers(self):
        project_path = self.get_project_dir()
        model_name = self.get_current_model().name
        scn = self.get_current_scenario()
        if scn is not None:
            scn_name = scn[1]
        else:
            scn_name = ''
        self.__ui_manager.add_csv_result_layers(project_path, self.__current_project, model_name, scn_name)

    def unload_csv_results_layers(self):
        self.__ui_manager.remove_group_layers('res: '+self.get_current_scenario()[1])

    def reload_w15_layer(self, w14_result, w15_result):
        self.unload_w15_layer()
        layer = self.__ui_manager.add_w15_layer(self, w14_result, w15_result)
        return layer

    def unload_w15_layer(self):
        self.__ui_manager.remove_w15_layer()

    def get_project_dir(self):
        dir = self.get_workspace()
        if dir is not None:
            return os.path.abspath(os.path.join(dir, self.__current_project))
        else:
            return os.path.abspath(self.__current_project)

    def get_filepath(self):
        return os.path.abspath(os.path.join(self.get_project_dir(), self.__current_project + ".qgs"))

    def get_timestamp(self):
        time = self.execute("""select creation_date from hydra.metadata""").fetchone()
        if time:
            return time[0]
        else:
            return None

    def get_models(self):
        return dbhydra.get_project_models_list(self.__cur)

    def get_current_project(self):
        return self.__current_project

    def get_current_model(self):
        return self.__current_model

    def set_current_model(self, model_name):
        if model_name is not None:
            self.__current_model = Model(self, self.__srid, model_name)
            models = self.get_models()
            # for model in models:
                # self.__ui_manager.set_model_layers_expanded(model, model==model_name)
            # self.__ui_manager.set_model_layers_expanded('project', False)
        else :
            self.__current_model = None

    def get_current_scenario(self):
        return self.__current_scenario

    def get_scenarios(self):
        return self.execute("""select id, name from project.scenario;""").fetchall()

    def set_current_scenario(self, scn_id):
        self.__current_scenario = self.execute("""
                        select id, name from project.scenario where id = {};""".format(scn_id)).fetchone()

    def get_version(self):
        query = self.execute("""select version from hydra.metadata;""").fetchone()
        if query:
            return query[0]
        else:
            self.log.error("No project version found")

    def get_version_to_update(self):
        return dbhydra.target_version(self.get_version())

    def get_workspace(self):
        ws = self.execute("""select workspace from hydra.metadata""").fetchone()
        if ws:
            dir=ws[0]
            if not SettingsProperties.local():
                # Server side project
                return str(os.path.join(os.path.expanduser('~'), '.hydra'))
            else:
                if dir == "/$user/.hydra":
                    return str(os.path.join(os.path.expanduser('~'), '.hydra'))
                # Local project
                else:
                    return str(dir)
        else:
            return ""

    def pack_path(self, path):
        if path is None or path =='':
            return path
        else:
            p = os.path.realpath(path)
            w = os.path.realpath(self.get_workspace())
            if p.startswith(w):
                p = p.replace(w, '$workspace')
            return p

    def unpack_path(self, path):
        if path is None or path =='':
            return path
        else:
            p = path
            w = os.path.realpath(self.get_workspace())
            if p.startswith('$workspace'):
                p = p.replace('$workspace', w)
            return p

    def __sql_wait(self):
        while 1:
            state = self.__conn.poll()
            if state == psycopg2.extensions.POLL_OK:
                break
            elif state == psycopg2.extensions.POLL_WRITE:
                select.select([], [self.__conn.fileno()], [])
            elif state == psycopg2.extensions.POLL_READ:
                select.select([self.__conn.fileno()], [], [])
            else:
                raise psycopg2.OperationalError("poll() returned %s" % state)

    def vacuum_analyse(self, table=''):
        old_isolation_level = self.__conn.isolation_level
        self.__conn.set_isolation_level(0)
        self.__cur.execute("Vacuum analyse "+table)
        self.__conn.set_isolation_level(old_isolation_level)

    def try_execute(self, sql, param=None):
        success=True
        try:
            if param is None:
                self.__cur.execute(sql)
            else:
                self.__cur.execute(sql, param)
        except:
            self.__conn.rollback()
            success=False
        return success, self.__cur

    def execute(self, sql, param=None):
        try:
            if param is None:
                self.__cur.execute(sql)
                #self.log.notice("Query: "+sql)
            else:
                self.__cur.execute(sql, param)
                #self.log.notice("Query: "+sql)
        except Exception, error:
            # Rollback in case there is any error
            self.__conn.rollback()
            self.log.notice('Rollback')
            self.log.error("Error in query: "+sql, error)
        return self.__cur

    def execute_isolated(self, sql, param=None):
        try:
            if param is None:
                old_isolation_level = self.__conn.isolation_level
                self.__conn.set_isolation_level(0)
                self.__cur.execute(sql)
                #self.log.notice("Query: "+sql)
                self.__conn.set_isolation_level(old_isolation_level)
            else:
                old_isolation_level = self.__conn.isolation_level
                self.__conn.set_isolation_level(0)
                self.__cur.execute(sql, param)
                #self.log.notice("Query: "+sql)
                self.__conn.set_isolation_level(old_isolation_level)
        except Exception, error:
            # Rollback in case there is any error
            self.__conn.rollback()
            self.log.notice('Rollback')
            self.log.error("Error in query: "+sql, error)
        return self.__cur

    def executemany(self, sql, values):
        try:
            self.__cur.executemany(sql, values)
        except:
            # Rollback in case there is any error
            self.__conn.rollback()
            self.log.notice('Rollback')
            raise
        return self.__cur

    def commit(self):
        try:
            self.__conn.commit()
            #self.log.notice('Commit')
        except:
            # Rollback in case there is any error
            self.__conn.rollback()
            self.log.notice('Rollback')
            raise

    def rollback(self):
        self.__conn.rollback()
        self.log.notice('Rollback')

    def terrain(self):
        return self.__terrain

    def radar_rain(self):
        return self.__radar_rain