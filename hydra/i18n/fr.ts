<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="ff" sourcelanguage="en">
<context>
    <name>@default</name>
    <message>
        <location filename="hydra.py" line="49"/>
        <source>&amp;Project</source>
        <translation type="unfinished">&amp;Projet</translation>
    </message>
    <message>
        <location filename="hydra.py" line="51"/>
        <source>&amp;Open</source>
        <translation type="unfinished">&amp;Ouvrir</translation>
    </message>
    <message>
        <location filename="hydra.py" line="52"/>
        <source>&amp;New</source>
        <translation type="unfinished">&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="hydra.py" line="71"/>
        <source>&amp;Save As</source>
        <translation type="unfinished">&amp;Enregister sous</translation>
    </message>
    <message>
        <location filename="hydra.py" line="54"/>
        <source>&amp;Import topographic data</source>
        <translation type="unfinished">&amp;Impoter des données topographiques</translation>
    </message>
    <message>
        <location filename="hydra.py" line="55"/>
        <source>&amp;Raster</source>
        <translation type="unfinished">&amp;Raster</translation>
    </message>
    <message>
        <location filename="hydra.py" line="56"/>
        <source>&amp;Points</source>
        <translation type="unfinished">&amp;Points</translation>
    </message>
    <message>
        <location filename="hydra.py" line="57"/>
        <source>&amp;Lines</source>
        <translation type="unfinished">&amp;Lignes</translation>
    </message>
    <message>
        <location filename="hydra.py" line="59"/>
        <source>&amp;Quit</source>
        <translation type="unfinished">&amp;Quitter</translation>
    </message>
    <message>
        <location filename="hydra.py" line="61"/>
        <source>&amp;Model</source>
        <translation type="unfinished">&amp;Modèle</translation>
    </message>
    <message>
        <location filename="hydra.py" line="64"/>
        <source>Model 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hydra.py" line="65"/>
        <source>Model 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hydra.py" line="66"/>
        <source>Model 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hydra.py" line="68"/>
        <source>&amp;Configurations</source>
        <translation type="unfinished">&amp;Configurations</translation>
    </message>
    <message>
        <location filename="hydra.py" line="69"/>
        <source>&amp;Add</source>
        <translation type="unfinished">&amp;Ajouter</translation>
    </message>
    <message>
        <location filename="hydra.py" line="70"/>
        <source>&amp;Import</source>
        <translation type="unfinished">&amp;Importer</translation>
    </message>
    <message>
        <location filename="hydra.py" line="73"/>
        <source>&amp;Hydrology</source>
        <translation type="unfinished">&amp;Hydrologie</translation>
    </message>
    <message>
        <location filename="hydra.py" line="75"/>
        <source>Dry weather apports</source>
        <translation type="unfinished">&amp;Apport de temps sec</translation>
    </message>
    <message>
        <location filename="hydra.py" line="77"/>
        <source>Internal Rain</source>
        <translation type="unfinished">Pluviométrie interne</translation>
    </message>
    <message>
        <location filename="hydra.py" line="78"/>
        <source>External Rain</source>
        <translation type="unfinished">Pluviométrie externe</translation>
    </message>
    <message>
        <location filename="hydra.py" line="79"/>
        <source>Rain list</source>
        <translation type="unfinished">Liste des pluies</translation>
    </message>
    <message>
        <location filename="hydra.py" line="81"/>
        <source>Pollution in waste water...</source>
        <translation type="unfinished">Pollution en assainissement...</translation>
    </message>
    <message>
        <location filename="hydra.py" line="83"/>
        <source>Rain grid...</source>
        <translation type="unfinished">Grille pluvioétrique</translation>
    </message>
    <message>
        <location filename="hydra.py" line="85"/>
        <source>Rain radar...</source>
        <translation type="unfinished">Pluie radar</translation>
    </message>
    <message>
        <location filename="hydra.py" line="87"/>
        <source>&amp;Scenario</source>
        <translation type="unfinished">&amp;Scénario</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="manhole.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="manhole.ui" line="67"/>
        <source>section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="manhole.ui" line="76"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="manhole.ui" line="89"/>
        <source>m²</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="manhole.ui" line="111"/>
        <source>altitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="manhole.ui" line="120"/>
        <source>256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="manhole.ui" line="133"/>
        <source>m</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManholeEditor</name>
    <message>
        <location filename="manhole.py" line="39"/>
        <source>Hello</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
