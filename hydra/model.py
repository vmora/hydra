# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from inspect import getargvalues, currentframe
import hydra.utility.sql_json as sql_json
import string

# @note inspect provide instrospection of function argument names and values
#       for magic to work, function arguments needto have the same name
#       as table fields

def _quote(value):
    return "'"+value+"'" \
            if (isinstance(value, str) or isinstance(value, unicode))\
            and ( value == "" or value[0] != "'") and value != "null" else value

def _encapsulate(value):
    if value is None:
        return 'null'
    elif isinstance(value, basestring):
        return "'{}'".format(value)
    elif isinstance(value, list):
        return "'{}'::real[]".format(str(value).replace(']', '}'
                                              ).replace('[', '{'
                                              ).replace('(', '{'
                                              ).replace(')', '}'
                                              ).replace('None', 'null'))
    else:
        return str(value)

class Model:
    def __init__(self, proj, srid, model_name):
        self.__proj = proj
        self.__srid = srid
        self.name = model_name

    def __getattr__(self, att):
        if att=='srid':
             return self.__srid
        else:
            raise AttributeError

    def make_point(self, point):
        if isinstance(point, str) or isinstance(point, unicode):
            return point
        return "'srid={}; POINTZ({} {} {})'::geometry".format(
                str(self.__srid),
                str(point[0]),
                str(point[1]),
                str(point[2]) if len(point) == 3 else 9999
                ) if point else None

    def make_line(self, geom):
        if isinstance(geom, str) or isinstance(geom, unicode):
            if geom[:10]=="LINESTRING" and geom[-10:]!="::geometry":
                return "'srid={}; {}'::geometry".format(str(self.__srid), geom)
            else:
                return geom
        return "'srid={}; LINESTRINGZ({})'::geometry".format(
                str(self.__srid),
                ",".join([
                    str(p[0])+" "+str(p[1])+" "+(str(p[2]) if len(p) == 3 else "9999")
                    for p in geom])) if geom else None

    def make_polygon(self, geom, has_z=True):
        if isinstance(geom, str) or isinstance(geom, unicode):
            return geom
        return "'srid={}; POLYGON{}(({}))'::geometry".format(
                str(self.__srid),
                "Z" if has_z else "",
                ",".join([str(p[0])+" "+str(p[1])+
                    ((" "+(str(p[2]) if len(p) == 3 else "9999")) if has_z else "")
                    for p in geom[0]])) if geom else None

    def make_real_array(self, array):
        return "'{}'::real[]".format(
                str(array).replace(']', '}'
                         ).replace('[', '{'
                         ).replace('(', '{'
                         ).replace(')', '}'
                         ).replace('None', 'null')) if array else None

    def execute(self, sql):
        """execute statement, the variable $model and $srid in the sql are
        replaced with the appropriate value"""
        return self.__proj.execute(string.Template(sql).substitute(
            {"model": self.name, "srid": self.srid }))

    def get_attrib_from_table_byname(self, table, name):
        return self.__proj.execute("""select * from {}.{} where UPPER(name)=UPPER({})""".format(
            self.name, table, _quote(str(name)))).fetchall()

    def get_Distance_point_to_line_table(self, line_table, id_line, point):
        pstring = 'Point('+ point[0] + ' ' + point[1] + ')'
        lstring= self.__proj.execute("""select st_astext(geom) from {}.{} where  id={}""".format(
                        self.name, line_table,id_line)).fetchall()[0][0]
        res=self.__proj.execute("""select ST_Distance(St_GeomFromText('{}',{}),
                        St_GeomFromText('{}',{}))""".format(pstring, self.__srid, lstring, self.__srid))
        distance = res.fetchall()[0][0]
        return distance

    def __magic_insert(self, table, arg_map):
        argmap = {key: value for key, value in arg_map.iteritems()
            if value is not None and key not in ['table','self']}
        if len(argmap):
            sql = ("""
                    insert into {}.{}({}) values({}) returning id""".format(
                    self.name,
                    table,
                    ",".join(argmap.keys()),
                    ",".join(_quote(str(value)) for value in argmap.values())))
            new_id, = self.__proj.execute(sql).fetchone()
        else:
            new_id, = self.__proj.execute(""" insert into {}.{} default values returning id""".format(
                self.name, table)).fetchone()
        return new_id

    def __magic_update(self, table, id, arg_map):
        values_to_update=[]
        for key, value in arg_map.iteritems():
            if value is not None and key not in ['table','self', 'id']:
                           values_to_update.append("{}={}".format(key, _quote(str(value))))
        if len(values_to_update)>0:
            self.__proj.execute("""update {}.{} set {} where id={}""".format(self.name, table, ", ".join(values_to_update), id))

    def get_current_configuration(self):
        return self.execute("""select configuration from $model.metadata;""").fetchone()[0]

    def get_configurations(self):
        return self.execute("""select id, name from $model.configuration;""").fetchall()

    def set_current_configuration(self, config_id):
        self.execute("""update $model.metadata set configuration={}""".format(config_id))
        self.__proj.commit()

    def get_number_configured(self):
        return self.execute("""select count(1) from $model.configured_current;""").fetchone()[0]

    def drop_config_on_object(self, config_id, obj_table, obj_id):
        type=obj_table.split('_')[-1] if obj_table!='river_cross_section_profile' else obj_table
        obj_json = self.execute("""select configuration from $model.{table} where id={id};""".format(table='_'+type, id=obj_id)).fetchone()[0]
        config = self.execute("""select name from $model.configuration where id={};""".format(config_id)).fetchone()[0]

        if config in obj_json.keys():
            obj_json.pop(config)
            if len(obj_json.keys())==1:
                self.execute("""update $model.{table} set (configuration)=(null) where id={id};""".format(table='_'+type, id=obj_id))
            else:
                self.execute("""update $model.{table} set (configuration)=('{cfg_json}') where id={id};""".format(table='_'+type, id=obj_id, cfg_json=sql_json.pack(obj_json)))

            self.execute("""update $model.{table} set ({keys})=({values}) where id={id};""".format(
                keys=(', ').join(obj_json['default'].keys()), values=(', ').join([_encapsulate(v) for v in obj_json['default'].values()]), table='_'+obj_table, id=obj_id))

    def delete_config(self, config_id):
        assert config_id != self.get_current_configuration()
        config_name, = self.execute("""select name from $model.configuration where id={};""".format(config_id)).fetchone()
        configured_items = self.execute("""select id, configuration, type, subtype from $model.configured;""").fetchall()
        # find items wich are config in this config
        for item in configured_items:
            c_id, obj_json, typ, subtyp = item
            id=c_id.split(':')[1]
            if config_name in obj_json.keys():
                # drop associated config in json dict
                obj_json.pop(config_name)
                if len(obj_json.keys())==1:
                    self.execute("""update $model.{tab} set configuration=null where id={i};""".format(tab='_'+typ, i=id))
                else:
                    self.execute("""update $model.{tab} set configuration='{cfg_json}' where id={i};""".format(tab='_'+typ, i=id, cfg_json=sql_json.pack(obj_json)))
        # drop config
        self.execute("""delete from $model.configuration where id={};""".format(config_id))

    def add_constrain(self, geom, elem_length=None, name=None,
            snap_distance=None, keep_left=None, keep_right=None,
            link_type=None, link_attributes=None ):
        geom = self.make_line(geom)
        return self.__magic_insert('constrain', getargvalues(currentframe()).locals)

    def update_constrain(self, id, geom=None, elem_length=None, name=None,
            snap_distance=None, keep_left=None, keep_right=None,
            link_type=None, link_attributes=None  ):
        geom = self.make_line(geom)
        self.__magic_update('constrain', id, getargvalues(currentframe()).locals)

    def add_domain_2d(self, name=None ):
        #geom = self.make_polygon(geom)
        return self.__magic_insert('domain_2d', getargvalues(currentframe()).locals)

    def update_domain_2d(self, id, name=None ):
        #geom = self.make_polygon(geom)
        self.__magic_update('domain_2d', id, getargvalues(currentframe()).locals)

    def add_simple_singularity(self, table, geom, name=None):
        """ used to insert default singularities into any tables """
        geom = self.make_point(geom)
        return self.__magic_insert(table, getargvalues(currentframe()).locals)

    def update_simple_singularity(self, table, id, geom, name=None):
        """ used to update default singularities into any tables """
        geom = self.make_point(geom)
        self.__magic_update(table, id, getargvalues(currentframe()).locals)

    def add_marker_singularity(self, geom, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('marker_singularity', getargvalues(currentframe()).locals)

    def update_marker_singularity(self, id, geom, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('marker_singularity', id, getargvalues(currentframe()).locals)

    def add_coverage_marker(self, geom, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('coverage_marker', getargvalues(currentframe()).locals)

    def update_coverage_marker(self, id, geom, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('coverage_marker', id, getargvalues(currentframe()).locals)

    def add_2d_domain_marker(self, geom, name=None, domain_2d=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('domain_2d_marker', getargvalues(currentframe()).locals)

    def update_2d_domain_marker(self, id, geom, name=None, domain_2d=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('domain_2d_marker', id, getargvalues(currentframe()).locals)

    def add_ci_singularity(self, geom, q0=0, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('constant_inflow_bc_singularity', getargvalues(currentframe()).locals)

    def update_ci_singularity(self, id, geom, q0, name=None):
        geom = self.make_point(geom)
        self.__magic_update('constant_inflow_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_hy_singularity(self, geom=None, name=None, storage_area=0, tq_array=None, constant_dry_flow=None,
            distrib_coef=None, lag_time_hr=None, sector=None, hourly_modulation=None,
            pollution_quality_mode=None, pollution_quality_param=None, external_file_data=None):
        geom = self.make_point(geom)
        tq_array = self.make_real_array(tq_array)
        return self.__magic_insert('hydrograph_bc_singularity', getargvalues(currentframe()).locals)

    def update_hy_singularity(self, id, geom=None, name=None, storage_area=None, tq_array=None, constant_dry_flow=None,
                                distrib_coef=None, lag_time_hr=None, sector=None, hourly_modulation=None,
                                pollution_quality_mode=None, pollution_quality_param=None, external_file_data=None):
        geom = self.make_point(geom)
        tq_array = self.make_real_array(tq_array)
        self.__magic_update('hydrograph_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_manhole(self, geom, name=None, area=None, z_ground=None):
        geom = self.make_point(geom)
        return self.__magic_insert('manhole_node', getargvalues(currentframe()).locals)

    def update_manhole(self, id, geom=None, name=None, area=None, z_ground=None):
        #delete_row_by_name_from_table('manhole_node', name)
        geom = self.make_point(geom)
        self.__magic_update('manhole_node', id, getargvalues(currentframe()).locals)

    def add_pipe_link(self, geom, z_invert_up=None, z_invert_down=None, cross_section_type=None,
                                    h_sable=None, rk=None, circular_diameter=None, ovoid_height=None, ovoid_top_diameter=None, ovoid_invert_diameter=None,
                                    cp_geom=None, op_geom=None, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('pipe_link', getargvalues(currentframe()).locals)

    def update_pipe_link(self, id, geom, z_invert_up=None, z_invert_down=None, cross_section_type = None, h_sable=None,
                                    rk=None, circular_diameter=None, ovoid_height=None, ovoid_top_diameter=None, ovoid_invert_diameter=None,
                                    cp_geom=None, op_geom=None, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('pipe_link', id, getargvalues(currentframe()).locals)

    def add_link_qmv(self, geom, z_invert=None, z_ceiling=None, width=None, cc=None, mode_valve=None, action_gate_type=None, z_gate=None, v_max_cms=None, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('gate_link', getargvalues(currentframe()).locals)

    def update_link_qmv(self, id, geom, z_invert=None, z_ceiling=None, width=None, cc=None, mode_valve=None, action_gate_type=None, z_gate=None, v_max_cms=None, name=None):
        geom = self.make_line(geom)
        self.__magic_update('gate_link', id, getargvalues(currentframe()).locals)

    def add_link_qms(self, geom, z_invert, width, cc, z_weir, v_max_cms, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('weir_link', getargvalues(currentframe()).locals)

    def update_link_qms(self, id, geom, z_invert, width, cc, z_weir, v_max_cms, name=None):
        geom = self.make_line(geom)
        self.__magic_update('weir_link', id, getargvalues(currentframe()).locals)

    def add_simple_link(self, table, geom, name=None):
        """ used to insert default links into any tables """
        geom = self.make_line(geom)
        return self.__magic_insert(table, getargvalues(currentframe()).locals)

    def update_simple_link(self, table, id, geom, name=None):
        """ used to update default links into any tables """
        geom = self.make_line(geom)
        self.__magic_update(table, id, getargvalues(currentframe()).locals)

    def add_street(self, geom, width=None, rk=None, elem_length=None, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('street', getargvalues(currentframe()).locals)

    def update_street(self, id, geom, width=None, rk=None, elem_length=None, name=None):
        geom = self.make_line(geom)
        self.__magic_update('street', id, getargvalues(currentframe()).locals)

    def add_reachpath(self, geom, dx=None, pk0_km=None, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('reach', getargvalues(currentframe()).locals)

    def update_reachpath(self, id, geom, dx=None, pk0_km=None, name=None):
        geom = self.make_line(geom)
        self.__magic_update('reach', id, getargvalues(currentframe()).locals)

    def try_merge_reach(self, geom):
        res=False
        # reaches endding at the start of the new line
        up_reaches = self.__proj.execute("""select id, name, st_astext(geom)
            from {}.reach
            where ST_Equals(st_endpoint(geom), st_startpoint({}));""".format(self.name, self.make_line(geom))).fetchall()
        down_reaches = self.__proj.execute("""select id, name, st_astext(geom)
            from {}.reach
            where ST_Equals(st_startpoint(geom), st_endpoint({}));""".format(self.name, self.make_line(geom))).fetchall()
        if len(up_reaches)>0 and len(down_reaches)==0:
            self.__proj.execute("""update {}.reach
                set geom=st_makeline(geom,{})
                where id = {};""".format(self.name, self.make_line(geom), up_reaches[0][0]))
            self.__proj.execute("""
                insert into {m}.river_node(geom, z_ground)
                select st_endpoint({l}), {z}
                where not exists(select 1 from {m}.river_node where ST_Intersects(ST_Buffer(geom, 0.1), {l}));
                """.format(m=self.name, l=self.make_line(geom), z=str(geom[-1][2] if len(geom[-1])>2 else 9999)))
            res = True
        # reaches starting at the end of the new line
        elif len(up_reaches)==0 and len(down_reaches)>0:
            self.__proj.execute("""update {}.reach
                set geom=st_makeline({}, geom)
                where id = {};""".format(self.name, self.make_line(geom), down_reaches[0][0]))
            self.__proj.execute("""
                insert into {m}.river_node(geom, z_ground)
                select st_startpoint({l}), {z}
                where not exists(select 1 from {m}.river_node where ST_Intersects(ST_Buffer(geom, 0.1), {l}));
                """.format(m=self.name, l=self.make_line(geom), z=str(geom[0][2] if len(geom[0])>2 else 9999)))
            res = True
        # reaches at both ends
        elif  len(up_reaches)>0 and len(down_reaches)>0:
            id=self.__proj.execute("""insert into {}.reach(geom) values (ST_MakeLine(ST_MakeLine({}, {}), {})) returning id;""".format(self.name, self.make_line(up_reaches[0][2]), self.make_line(geom), self.make_line(down_reaches[0][2]))).fetchone()[0]
            self.__proj.execute("""update {m}.river_node set reach={i} where reach in ({u}, {d});""".format(m=self.name, i=id, u=up_reaches[0][0], d=down_reaches[0][0]))
            self.__proj.execute("""delete from {}.reach where id in ({}, {});""".format(self.name, up_reaches[0][0], down_reaches[0][0]))
            res = True
        return res

    def add_river_cross_section_profile(self, geom, z_invert_up=None, z_invert_down=None,
                                        type_cross_section_up=None,type_cross_section_down=None,
                                        up_rk=None, up_rk_maj=None, up_sinuosity=None, up_circular_diameter=None,
                                        up_ovoid_height=None, up_ovoid_top_diameter=None, up_ovoid_invert_diameter=None,
                                        up_cp_geom=None, up_op_geom=None, up_vcs_geom=None, up_vcs_topo_geom=None,
                                        down_rk=None, down_rk_maj=None, down_sinuosity=None, down_circular_diameter=None,
                                        down_ovoid_height=None, down_ovoid_top_diameter=None, down_ovoid_invert_diameter=None,
                                        down_cp_geom=None, down_op_geom=None, down_vcs_geom=None, down_vcs_topo_geom=None, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('river_cross_section_profile', getargvalues(currentframe()).locals)

    def update_river_cross_section_profile(self, id, geom, z_invert_up=None, z_invert_down=None,
                                        type_cross_section_up=None,type_cross_section_down=None,
                                        up_rk=None, up_rk_maj=None, up_sinuosity=None, up_circular_diameter=None,
                                        up_ovoid_height=None, up_ovoid_top_diameter=None, up_ovoid_invert_diameter=None,
                                        up_cp_geom=None, up_op_geom=None, up_vcs_geom=None, up_vcs_topo_geom=None,
                                        down_rk=None, down_rk_maj=None, down_sinuosity=None, down_circular_diameter=None,
                                        down_ovoid_height=None, down_ovoid_top_diameter=None, down_ovoid_invert_diameter=None,
                                        down_cp_geom=None, down_op_geom=None, down_vcs_geom=None, down_vcs_topo_geom=None, name=None):
        geom = self.make_point(geom)
        self.__magic_update('river_cross_section_profile', id, getargvalues(currentframe()).locals)

    def add_pipe_channel_section_geometry(self, table, zbmin_array, name=None):
        zbmin_array = self.make_real_array(zbmin_array)
        return self.__magic_insert(table, getargvalues(currentframe()).locals)

    def update_pipe_channel_section_geometry(self, table, id, zbmin_array, name=None):
        zbmin_array = self.make_real_array(zbmin_array)
        self.__magic_update(table, id, getargvalues(currentframe()).locals)

    def add_valley_section_geometry(self, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array,
            rlambda, rmu1,rmu2, zlevee_lb, zlevee_rb, name=None):
        zbmin_array = self.make_real_array(zbmin_array)
        zbmaj_lbank_array = self.make_real_array(zbmaj_lbank_array)
        zbmaj_rbank_array = self.make_real_array(zbmaj_rbank_array)
        return self.__magic_insert('valley_cross_section_geometry', getargvalues(currentframe()).locals)

    def update_valley_section_geometry(self, id, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array,
            rlambda, rmu1,rmu2, zlevee_lb, zlevee_rb, name=None, transect=None, t_discret=150, t_ignore_pt=False, t_distance_pt=100):
        zbmin_array = self.make_real_array(zbmin_array)
        zbmaj_lbank_array = self.make_real_array(zbmaj_lbank_array)
        zbmaj_rbank_array = self.make_real_array(zbmaj_rbank_array)
        self.__magic_update('valley_cross_section_geometry', id, getargvalues(currentframe()).locals)

    def add_valley_section_topo_geometry(self, xz_array, name=None):
        xz_array = self.make_real_array(xz_array)
        return self.__magic_insert('valley_cross_section_topo_geometry', getargvalues(currentframe()).locals)

    def update_valley_section_topo_geometry(self, id, xz_array, name=None):
        xz_array = self.make_real_array(xz_array)
        self.__magic_update('valley_cross_section_topo_geometry', id, getargvalues(currentframe()).locals)

    def add_river_node(self, geom, z_ground=None, area=None, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('river_node', getargvalues(currentframe()).locals)

    def update_river_node(self, id, geom, z_ground=None, area=None, name=None):
        geom = self.make_point(geom)
        self.__magic_update('river_node', id, getargvalues(currentframe()).locals)

    def add_singularity_va(self, geom, z_invert=None, z_ceiling=None, width=None, cc=None, mode_valve=None, action_gate_type=None, z_gate=None, v_max_cms=None, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('gate_singularity', getargvalues(currentframe()).locals)

    def update_singularity_va(self, id, geom, z_invert=None, z_ceiling=None, width=None, cc=None, mode_valve=None, action_gate_type=None, z_gate=None, v_max_cms=None, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        self.__magic_update('gate_singularity', id, getargvalues(currentframe()).locals)

    def add_hydraulic_cut_singularity(self, geom, qz_array, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        qz_array = self.make_real_array(qz_array)
        return self.__magic_insert('hydraulic_cut_singularity', getargvalues(currentframe()).locals)

    def update_hydraulic_cut_singularity(self, id, geom, qz_array, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        qz_array = self.make_real_array(qz_array)
        self.__magic_update('hydraulic_cut_singularity', id, getargvalues(currentframe()).locals)

    def add_param_headloss_singularity(self, geom, q_dz_array, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        q_dz_array = self.make_real_array(q_dz_array)
        return self.__magic_insert('param_headloss_singularity', getargvalues(currentframe()).locals)

    def update_param_headloss_singularity(self, id, geom, q_dz_array, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        q_dz_array = self.make_real_array(q_dz_array)
        self.__magic_update('param_headloss_singularity', id, getargvalues(currentframe()).locals)

    def add_zq_bc_singularity(self, geom, zq_array, name=None):
        geom = self.make_point(geom)
        zq_array = self.make_real_array(zq_array)
        return self.__magic_insert('zq_bc_singularity', getargvalues(currentframe()).locals)

    def update_zq_bc_singularity(self, id, geom, zq_array, name=None):
        geom = self.make_point(geom)
        zq_array = self.make_real_array(zq_array)
        self.__magic_update('zq_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_tz_bc_singularity(self, geom, tz_array, cyclic=None, external_file_data=None, name=None):
        geom = self.make_point(geom)
        tz_array = self.make_real_array(tz_array)
        return self.__magic_insert('tz_bc_singularity', getargvalues(currentframe()).locals)

    def update_tz_bc_singularity(self, id, geom, tz_array, cyclic=None, external_file_data=None, name=None):
        geom = self.make_point(geom)
        tz_array = self.make_real_array(tz_array)
        self.__magic_update('tz_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_tank_bc_singularity(self, geom, zs_array, zini, name=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        return self.__magic_insert('tank_bc_singularity', getargvalues(currentframe()).locals)

    def update_tank_bc_singularity(self, id, geom, zs_array, zini, name=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        self.__magic_update('tank_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_smk(self, geom, law_type=None, param=None, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('borda_headloss_singularity', getargvalues(currentframe()).locals)

    def update_singularity_smk(self, id, geom, law_type=None, param=None, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        self.__magic_update('borda_headloss_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_de(self, geom, z_invert, z_regul, width, cc,
                            mode_regul=None, reoxy_law=None, reoxy_param=None, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('zregul_weir_singularity', getargvalues(currentframe()).locals)

    def update_singularity_de(self, id, geom,  z_invert, z_regul, width, cc,
                            mode_regul=None, reoxy_law=None, reoxy_param=None, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        self.__magic_update('zregul_weir_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_acta(self, geom, z_invert=None, z_ceiling=None, width=None, cc=None,
                            action_gate_type=None, z_invert_stop=None, z_ceiling_stop=None,
                            v_max_cms=None, dt_regul_hr=None, mode_regul=None,
                            z_control_node=None, z_pid_array=None, z_tz_array=None,
                            q_z_crit=None, q_tq_array=None, nr_z_gate=None, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        z_pid_array = self.make_real_array(z_pid_array)
        z_tz_array = self.make_real_array(z_tz_array)
        q_tq_array = self.make_real_array(q_tq_array)
        return self.__magic_insert('regul_sluice_gate_singularity', getargvalues(currentframe()).locals)

    def update_singularity_acta(self, id, geom, z_invert=None, z_ceiling=None, width=None, cc=None,
                            action_gate_type=None, z_invert_stop=None, z_ceiling_stop=None,
                            v_max_cms=None, dt_regul_hr=None, mode_regul=None,
                            z_control_node=None, z_pid_array=None, z_tz_array=None,
                            q_z_crit=None, q_tq_array=None, nr_z_gate=None, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        z_pid_array = self.make_real_array(z_pid_array)
        z_tz_array = self.make_real_array(z_tz_array)
        q_tq_array = self.make_real_array(q_tq_array)
        self.__magic_update('regul_sluice_gate_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_brd(self, geom, zw_array, d_abutment_l, d_abutment_r, abutment_type, z_ceiling, name=None):
        geom = self.make_point(geom)
        zw_array = self.make_real_array(zw_array)
        return self.__magic_insert('bradley_headloss_singularity', getargvalues(currentframe()).locals)

    def update_singularity_brd(self, id, geom, zw_array, d_abutment_l, d_abutment_r, abutment_type, z_ceiling, name=None):
        geom = self.make_point(geom)
        zw_array = self.make_real_array(zw_array)
        self.__magic_update('bradley_headloss_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_brdg(self, geom, zw_array, l_road, z_road, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        zw_array = self.make_real_array(zw_array)
        return self.__magic_insert('bridge_headloss_singularity', getargvalues(currentframe()).locals)

    def update_singularity_brdg(self, id, geom, zw_array, l_road, z_road, full_section_discharge_for_headloss=True, name=None):
        geom = self.make_point(geom)
        zw_array = self.make_real_array(zw_array)
        self.__magic_update('bridge_headloss_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_mrkb(self, geom, pk0_km, dx, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('pipe_branch_marker_singularity', getargvalues(currentframe()).locals)

    def update_singularity_mrkb(self,id, geom, pk0_km, dx, name=None):
        geom = self.make_point(geom)
        self.__magic_update('pipe_branch_marker_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_dl(self, geom, z_weir, width, cc, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('weir_bc_singularity', getargvalues(currentframe()).locals)

    def update_singularity_dl(self, id, geom, z_weir, width, cc, name=None):
        geom = self.make_point(geom)
        self.__magic_update('weir_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_rk(self, geom, slope, k, width, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('strickler_bc_singularity', getargvalues(currentframe()).locals)

    def update_singularity_rk(self,id, geom, slope, k, width, name=None):
        geom = self.make_point(geom)
        self.__magic_update('strickler_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_hc(self, geom, q0, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('constant_inflow_bc_singularity', getargvalues(currentframe()).locals)

    def update_singularity_hc(self,id, geom, q0, name=None):
        geom = self.make_point(geom)
        self.__magic_update('constant_inflow_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_racc(self, geom, cascade_mode, zq_array, name=None):
        geom = self.make_point(geom)
        zq_array = self.make_real_array(zq_array)
        return self.__magic_insert('model_connect_bc_singularity', getargvalues(currentframe()).locals)

    def update_singularity_racc(self, id, geom, cascade_mode, zq_array, name=None):
        geom = self.make_point(geom)
        zq_array = self.make_real_array(zq_array)
        self.__magic_update('model_connect_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_dqh(self, geom, qq_array, downstream, split1, split2=None, name=None):
        geom = self.make_point(geom)
        qq_array = self.make_real_array(qq_array)
        # downstream, split1 and split2 are calculated by DB on creation
        return self.__magic_insert('qq_split_hydrology_singularity', getargvalues(currentframe()).locals)

    def update_singularity_dqh(self, id, geom, qq_array, downstream, split1, split2=None, name=None):
        geom = self.make_point(geom)
        qq_array = self.make_real_array(qq_array)
        if downstream:
            downstream_type, = self.__proj.execute("""select link_type from {}._link where id={}""".format(self.name, downstream)).fetchone()
        if split1:
            split1_type, = self.__proj.execute("""select link_type from {}._link where id={}""".format(self.name, split1)).fetchone()
        if split2:
            split2_type, = self.__proj.execute("""select link_type from {}._link where id={}""".format(self.name, split2)).fetchone()
        self.__magic_update('qq_split_hydrology_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_dzh(self, geom, downstream, downstream_law, downstream_param,
                split1, split1_law, split1_param, split2, split2_law, split2_param, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('zq_split_hydrology_singularity', getargvalues(currentframe()).locals)

    def update_singularity_dzh(self, id, geom, downstream, downstream_law, downstream_param,
                split1, split1_law, split1_param, split2, split2_law, split2_param, name=None):
        geom = self.make_point(geom)
        if downstream:
            downstream_type, = self.__proj.execute("""select link_type from {}._link where id={}""".format(self.name, downstream)).fetchone()
        if split1:
            split1_type, = self.__proj.execute("""select link_type from {}._link where id={}""".format(self.name, split1)).fetchone()
        if split2:
            split2_type, = self.__proj.execute("""select link_type from {}._link where id={}""".format(self.name, split2)).fetchone()
        self.__magic_update('zq_split_hydrology_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_rsh(self, geom, drainage, overflow, q_drainage, z_ini, zs_array,
            treatment_mode=None, treatment_param=None, name=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        # drainage and overflow are calculated by DB on creation
        return self.__magic_insert('reservoir_rs_hydrology_singularity', getargvalues(currentframe()).locals)

    def update_singularity_rsh(self, id, geom, drainage, overflow, q_drainage, z_ini, zs_array,
            treatment_mode=None, treatment_param=None, name=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        if drainage:
            drainage_type, = self.__proj.execute("""select link_type from {}._link where id={}""".format(self.name, drainage)).fetchone()
        if overflow:
            overflow_type, = self.__proj.execute("""select link_type from {}._link where id={}""".format(self.name, overflow)).fetchone()
        self.__magic_update('reservoir_rs_hydrology_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_rsph(self, geom, drainage, overflow, z_ini, zr_sr_qf_qs_array,
            treatment_mode=None, treatment_param=None, name=None):
        geom = self.make_point(geom)
        zr_sr_qf_qs_array = self.make_real_array(zr_sr_qf_qs_array)
        # drainage and overflow are calculated by DB on creation
        return self.__magic_insert('reservoir_rsp_hydrology_singularity', getargvalues(currentframe()).locals)

    def update_singularity_rsph(self,id, geom, drainage, overflow, z_ini, zr_sr_qf_qs_array,
            treatment_mode=None, treatment_param=None, name=None):
        geom = self.make_point(geom)
        zr_sr_qf_qs_array = self.make_real_array(zr_sr_qf_qs_array)
        if drainage:
            drainage_type, = self.__proj.execute("""select link_type from {}._link where id={}""".format(self.name, drainage)).fetchone()
        if overflow:
            overflow_type, = self.__proj.execute("""select link_type from {}._link where id={}""".format(self.name, overflow)).fetchone()
        self.__magic_update('reservoir_rsp_hydrology_singularity', id, getargvalues(currentframe()).locals)

    def add_catchment_node(self, geom, name=None, area_ha=None, rl=None, slope=None, c_imp=None,
            netflow_type=None, constant_runoff=None, horner_ini_loss_coef=None, horner_recharge_coef=None,
            holtan_sat_inf_rate_mmh=None, holtan_dry_inf_rate_mmh=None, holtan_soil_storage_cap_mm=None,
            scs_j_mm=None, scs_soil_drainage_time_day=None, scs_rfu_mm=None, permeable_soil_j_mm=None,
            permeable_soil_rfu_mm=None, permeable_soil_ground_max_inf_rate=None, permeable_soil_ground_lag_time_day=None, permeable_soil_coef_river_exchange=None,
            runoff_type=None, socose_tc_mn=None, socose_shape_param_beta=None, define_k_mn=None,
            q_limit=None, q0=None, contour=None, catchment_pollution_mode=None, catchment_pollution_param=None):
        geom = self.make_point(geom)
        return self.__magic_insert("catchment_node", getargvalues(currentframe()).locals)

    def update_catchment_node(self,id, geom=None, name=None, area_ha=None, rl=None, slope=None, c_imp=None,
            netflow_type=None, constant_runoff=None, horner_ini_loss_coef=None, horner_recharge_coef=None,
            holtan_sat_inf_rate_mmh=None, holtan_dry_inf_rate_mmh=None, holtan_soil_storage_cap_mm=None,
            scs_j_mm=None, scs_soil_drainage_time_day=None, scs_rfu_mm=None, permeable_soil_j_mm=None,
            permeable_soil_rfu_mm=None, permeable_soil_ground_max_inf_rate=None, permeable_soil_ground_lag_time_day=None, permeable_soil_coef_river_exchange=None,
            runoff_type=None, socose_tc_mn=None, socose_shape_param_beta=None, define_k_mn=None,
            q_limit=None, q0=None, contour=None, pollution_mode=None, pollution_param=None):
        geom = self.make_point(geom)
        self.__magic_update("catchment_node", id, getargvalues(currentframe()).locals)

    def add_catchment_contour(self, geom, name=None):
        geom = self.make_polygon(geom, False)
        return self.__magic_insert('catchment', getargvalues(currentframe()).locals)

    def update_catchment_contour(self, id, geom, name=None):
        geom = self.make_polygon(geom, False)
        self.__magic_update('catchment', id, getargvalues(currentframe()).locals)

    def add_station(self, geom, name=None):
        geom = self.make_polygon(geom)
        return self.__magic_insert('station', getargvalues(currentframe()).locals)

    def update_station(self, id, geom, name=None):
        geom = self.make_polygon(geom)
        self.__magic_update('station', id, getargvalues(currentframe()).locals)

    def add_station_node(self, geom, z_invert=None, area=None, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('station_node', getargvalues(currentframe()).locals)

    def update_station_node(self, id, geom, z_invert=None, area=None, name=None):
        geom = self.make_point(geom)
        self.__magic_update('station_node', id, getargvalues(currentframe()).locals)

    def add_storage_node(self, geom, zs_array=None, zini=None, name=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        return self.__magic_insert('storage_node', getargvalues(currentframe()).locals)

    def update_storage_node(self, id, geom, zs_array=None, zini=None, name=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        self.__magic_update('storage_node', id, getargvalues(currentframe()).locals)

    def add_elem_2d_node(self, contour, zb=None, rk=None, domain_2d=None, area=None, name=None):
        contour = self.make_polygon(contour)
        return self.__magic_insert('elem_2d_node', getargvalues(currentframe()).locals)

    def update_elem_2d_node(self, id, contour, zb=None, rk=None, domain_2d=None, area=None, name=None):
        contour = self.make_polygon(contour)
        self.__magic_update('elem_2d_node', id, getargvalues(currentframe()).locals)

    def add_river_cross_section_pl1d(self, id, profile, geom=None, name=None):
        geom = self.make_point(geom)
        profile = self.make_line(profile)
        return self.__magic_insert('river_cross_section_pl1d', getargvalues(currentframe()).locals)

    def update_river_cross_section_pl1d(self, id, profile, geom=None, name=None):
        geom = self.make_point(geom)
        profile = self.make_line(profile)
        self.__magic_update('river_cross_section_pl1d', id, getargvalues(currentframe()).locals)

    def add_routing_hydrology_link(self, geom, cross_section, slope=None, length=None, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('routing_hydrology_link', getargvalues(currentframe()).locals)

    def update_routing_hydrology_link(self, id, geom, cross_section, slope, length, name=None):
        geom = self.make_line(geom)
        self.__magic_update('routing_hydrology_link', id, getargvalues(currentframe()).locals)

    def add_mesh_2d_link(self, geom, z_invert=None, lateral_contraction_coef=None, name=None, border=None):
        geom = self.make_line(geom)
        border = self.make_line(border)
        return self.__magic_insert('mesh_2d_link', getargvalues(currentframe()).locals)

    def update_mesh_2d_link(self, id, geom, z_invert=None, lateral_contraction_coef=None, name=None, border=None):
        geom = self.make_line(geom)
        border = self.make_line(border)
        self.__magic_update('mesh_2d_link', id, getargvalues(currentframe()).locals)

    def add_strickler_link(self, geom, z_crest1, width1, rk, z_crest2, width2, length=None, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('strickler_link', getargvalues(currentframe()).locals)

    def update_strickler_link(self, id, geom, z_crest1, width1, rk, z_crest2, width2, length=None, name=None):
        geom = self.make_line(geom)
        self.__magic_update('strickler_link', id, getargvalues(currentframe()).locals)

    def add_weir_link(self, geom, z_invert, width, cc, z_weir, v_max_cms, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('weir_link', getargvalues(currentframe()).locals)

    def update_weir_link(self, id, geom, z_invert, width, cc, z_weir, v_max_cms, name=None):
        geom = self.make_line(geom)
        self.__magic_update('weir_link', id, getargvalues(currentframe()).locals)

    def add_regul_gate_link(self, geom, z_invert=None, z_ceiling=None, width=None, cc=None, action_gate_type=None, z_invert_stop=None, z_ceiling_stop=None,
                            v_max_cms=None, dt_regul_hr=None, mode_regul=None,
                            z_control_node=None, z_pid_array=None, z_tz_array=None,
                            q_z_crit=None, q_tq_array=None, nr_z_gate=None, name=None):
        geom = self.make_line(geom)
        z_pid_array = self.make_real_array(z_pid_array)
        z_tz_array = self.make_real_array(z_tz_array)
        q_tq_array = self.make_real_array(q_tq_array)
        return self.__magic_insert('regul_gate_link', getargvalues(currentframe()).locals)

    def update_regul_gate_link(self, id, geom, z_invert=None, z_ceiling=None, width=None, cc=None, action_gate_type=None, z_invert_stop=None, z_ceiling_stop=None,
                            v_max_cms=None, dt_regul_hr=None, mode_regul=None,
                            z_control_node=None, z_pid_array=None, z_tz_array=None,
                            q_z_crit=None, q_tq_array=None, nr_z_gate=None, name=None):
        geom = self.make_line(geom)
        z_pid_array = self.make_real_array(z_pid_array)
        z_tz_array = self.make_real_array(z_tz_array)
        q_tq_array = self.make_real_array(q_tq_array)
        self.__magic_update('regul_gate_link', id, getargvalues(currentframe()).locals)

    def get_attrib_from_table_byid(self, table, id):
        return self.__proj.execute('select * from {} where id={}'.format(
            table, _quote(str(id)))).fetchall()

    def add_manhole_hydrology(self, geom, name=None, area=None, z_ground=None):
        geom = self.make_point(geom)
        return self.__magic_insert('manhole_hydrology_node', getargvalues(currentframe()).locals)

    def update_manhole_hydrology(self, id, geom=None, name=None, area=None, z_ground=None):
        geom = self.make_point(geom)
        self.__magic_update('manhole_hydrology_node', id, getargvalues(currentframe()).locals)

    def add_strickler_bc_singularity(self, geom, slope,k, width, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('strickler_bc_singularity', getargvalues(currentframe()).locals)

    def update_strickler_bc_singularity(self, id, geom, slope,k, width, name=None):
        geom = self.make_point(geom)
        self.__magic_update('strickler_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_porous_link(self, geom, z_invert, width, transmitivity, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('porous_link', getargvalues(currentframe()).locals)

    def update_porous_link(self, id, geom, z_invert, width, transmitivity, name=None):
        geom = self.make_line(geom)
        self.__magic_update('porous_link', id, getargvalues(currentframe()).locals)

    def add_zfuse_spillway_link(self, geom, z_invert, width, cc, break_mode, z_break, grp, dt_fracw_array, name=None):
        geom = self.make_line(geom)
        dt_fracw_array = self.make_real_array(dt_fracw_array)
        return self.__magic_insert('fuse_spillway_link', getargvalues(currentframe()).locals)

    def update_zfuse_spillway_link(self, id, geom, z_invert, width, cc, break_mode, z_break, grp, dt_fracw_array, name=None):
        geom = self.make_line(geom)
        dt_fracw_array = self.make_real_array(dt_fracw_array)
        self.__magic_update('fuse_spillway_link', id, getargvalues(currentframe()).locals)

    def add_tfuse_spillway_link(self, geom, z_invert, width, cc, break_mode, t_break, grp, dt_fracw_array, name=None):
        geom = self.make_line(geom)
        dt_fracw_array = self.make_real_array(dt_fracw_array)
        return self.__magic_insert('fuse_spillway_link', getargvalues(currentframe()).locals)

    def update_tfuse_spillway_link(self, id, geom, z_invert, width, cc, break_mode, t_break, grp, dt_fracw_array, name=None):
        geom = self.make_line(geom)
        dt_fracw_array = self.make_real_array(dt_fracw_array)
        self.__magic_update('fuse_spillway_link', id, getargvalues(currentframe()).locals)

    def add_borda_link(self, geom, law_type=None, param=None, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('borda_headloss_link', getargvalues(currentframe()).locals)

    def update_borda_link(self, id, geom, law_type=None, param=None, name=None):
        geom = self.make_line(geom)
        self.__magic_update('borda_headloss_link', id, getargvalues(currentframe()).locals)

    def add_qdp_link(self, geom, q_pump, qz_array, name=None):
        geom = self.make_line(geom)
        qz_array = self.make_real_array(qz_array)
        return self.__magic_insert('deriv_pump_link', getargvalues(currentframe()).locals)

    def update_qdp_link(self, id, geom, q_pump, qz_array, name=None):
        geom = self.make_line(geom)
        qz_array = self.make_real_array(qz_array)
        self.__magic_update('deriv_pump_link', id, getargvalues(currentframe()).locals)

    def add_pump_link(self, geom, npump, zregul_array, hq_array, name=None):
        geom = self.make_line(geom)
        zregul_array = self.make_real_array(zregul_array)
        hq_array = self.make_real_array(hq_array)
        return self.__magic_insert('pump_link', getargvalues(currentframe()).locals)

    def update_pump_link(self, id, geom, npump, zregul_array, hq_array, name=None):
        geom = self.make_line(geom)
        zregul_array = self.make_real_array(zregul_array)
        hq_array = self.make_real_array(hq_array)
        self.__magic_update('pump_link', id, getargvalues(currentframe()).locals)

    def add_overflow_link(self, geom, z_crest1=None, z_crest2=None, width1=None, width2=None, cc=None, lateral_contraction_coef=None,
                            break_mode=None, z_break=None, t_break=None, z_invert=None, width_breach=None, grp=None, dt_fracw_array=None, name=None):
        geom = self.make_line(geom)
        dt_fracw_array = self.make_real_array(dt_fracw_array)
        return self.__magic_insert('overflow_link', getargvalues(currentframe()).locals)

    def update_overflow_link(self, id, geom, z_crest1=None, z_crest2=None, width1=None, width2=None, cc=None, lateral_contraction_coef=None,
                            break_mode=None, z_break=None, t_break=None, z_invert=None, width_breach=None, grp=None, dt_fracw_array=None, name=None):
        geom = self.make_line(geom)
        dt_fracw_array = self.make_real_array(dt_fracw_array)
        self.__magic_update('overflow_link', id, getargvalues(currentframe()).locals)

    def add_network_overflow_link(self, geom, z_overflow=None, area=None, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('network_overflow_link', getargvalues(currentframe()).locals)

    def update_network_overflow_link(self, id, geom, z_overflow=None, area=None, name=None):
        geom = self.make_line(geom)
        self.__magic_update('network_overflow_link', id, getargvalues(currentframe()).locals)

    def add_crossroad_node(self, geom, area=None, z_ground=None, h=None, name=None):
        geom = self.make_point(geom)
        return self.__magic_insert('crossroad_node', getargvalues(currentframe()).locals)

    def update_crossroad_node(self, id, geom, area=None, z_ground=None, h=None, name=None):
        geom = self.make_point(geom)
        self.__magic_update('crossroad_node', id, getargvalues(currentframe()).locals)

    def add_street_link(self, geom, length, width, rk, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('street_link', getargvalues(currentframe()).locals)

    def update_street_link(self, id, geom, length, width, rk, name=None):
        geom = self.make_line(geom)
        self.__magic_update('street_link', id, getargvalues(currentframe()).locals)

    def add_connector_link(self, geom, main_branch, name=None):
        geom = self.make_line(geom)
        return self.__magic_insert('connector_link', getargvalues(currentframe()).locals)

    def update_connector_link(self, id, geom,  main_branch, name=None):
        geom = self.make_line(geom)
        self.__magic_update('connector_link', id, getargvalues(currentframe()).locals)

    def insert_node_on_pipe(self, geom, name=None, area=None, z_ground=None):
        geom= self.make_point(geom)
        id_pipe, = self.__proj.execute("""select id from {}.pipe_link
                                                    where ST_DWithin({}, geom, .1)
                                                    """.format(self.name, geom)).fetchone()
        node_new=None
        if id_pipe:
            #collecting pipe informations
            node_up, node_down, z_invert_up, z_invert_down, h_sable, cross_section_type, rk, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom = self.__proj.execute("""
                                            select up, down, z_invert_up, z_invert_down, h_sable, cross_section_type, rk, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom
                                            from {}.pipe_link
                                            where id={} """.format(self.name, id_pipe)).fetchone()
            #insert node
            type, = self.__proj.execute("""select node_type
                                            from {}._node
                                            where id={} """.format(self.name, node_up)).fetchone()

            del_query = """delete from {}.pipe_link where id={} """.format(self.name, id_pipe)  # storing deletion of pipe query
                                                                                                # if type = network, delete before insertion (for branches mechanism to work)
                                                                                                # if type = hydrology, delete at the end (for hydrologysingularities to carry over)
            if type == 'manhole':
                node_new = self.add_manhole(geom, name, area, z_ground)
                self.__proj.execute(del_query)
            elif type == 'manhole_hydrology':
                node_new = self.add_manhole_hydrology(geom, name, area, z_ground)

            geom_up = self.__proj.execute("""select ST_X(geom), ST_Y(geom) from {}.{}_node where id={}""".format(self.name, type, node_up)).fetchone()
            geom_down = self.__proj.execute("""select ST_X(geom), ST_Y(geom) from {}.{}_node where id={}""".format(self.name, type, node_down)).fetchone()
            geom_new = self.__proj.execute("""select ST_X(geom), ST_Y(geom) from {}.{}_node where id={}""".format(self.name, type, node_new)).fetchone()
            # insert pipe #1
            id_pipe_up = self.add_pipe_link([geom_up,geom_new])
            # insert pipe #2
            id_pipe_down = self.add_pipe_link([geom_new,geom_down])
            # updates on Z at node level
            l_up, = self.__proj.execute("""select ST_Length(geom) from {}.pipe_link where id={}""".format(self.name, id_pipe_up)).fetchone()
            l_down, = self.__proj.execute("""select ST_Length(geom) from {}.pipe_link where id={}""".format(self.name, id_pipe_down)).fetchone()
            z_invert_mid = round((l_up*z_invert_down+l_down*z_invert_up)/(l_up+l_down), 3) if (z_invert_down and z_invert_up) else None
            self.update_pipe_link(id_pipe_up, None, z_invert_up, z_invert_mid, cross_section_type, h_sable, rk, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom)
            self.update_pipe_link(id_pipe_down, None, z_invert_mid, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom)

            if type == 'manhole_hydrology':
                # updates on singularities zq_split, qq_split, reservoir_rs and reservoir rsp that might reference the deleted pipe
                tables = {'qq_split_hydrology_singularity':['downstream', 'split1', 'split2'],
                            'zq_split_hydrology_singularity':['downstream', 'split1', 'split2'],
                            'reservoir_rs_hydrology_singularity':['drainage', 'overflow'],
                            'reservoir_rsp_hydrology_singularity':['drainage', 'overflow']}
                for table, columns in tables.iteritems():
                    for column in columns:
                        self.__proj.execute("""update {model}.{table}
                                                set {column}='{new_pipe}'
                                                where {column}={old_pipe} and node={node}
                                                """.format(model=self.name, table=table, column=column, new_pipe=id_pipe_up, old_pipe= id_pipe, node=node_up))
                        self.__proj.execute("""update {model}.{table}
                                                set {column}='{new_pipe}'
                                                where {column}={old_pipe} and node={node}
                                                """.format(model=self.name, table=table, column=column, new_pipe=id_pipe_down, old_pipe= id_pipe, node=node_down))
                #deleting pipe
                self.__proj.execute(del_query)

        return node_new, type

    def delete_node_update_pipe(self, id_node, table):
        from hydra.gui.delete import DeleteTool
        pipe_up, z_invert_up = self.__proj.execute("""select id, z_invert_up
                                                from {}.pipe_link
                                                where down={}""".format(self.name, id_node)).fetchone()
        pipe_down, z_invert_down = self.__proj.execute("""select id, z_invert_down
                                                from {}.pipe_link
                                                where up={}""".format(self.name, id_node)).fetchone()
        # insert new pipe
        node_up, = self.__proj.execute("""select up
                                            from {}.pipe_link
                                            where id={}""".format(self.name, pipe_up)).fetchone()
        node_down, = self.__proj.execute("""select down
                                            from {}.pipe_link
                                            where id={}""".format(self.name, pipe_down)).fetchone()
        geom_up = self.__proj.execute("""select ST_X(geom), ST_Y(geom) from {}.{} where id={}""".format(self.name, table, node_up)).fetchone()
        geom_down = self.__proj.execute("""select ST_X(geom), ST_Y(geom) from {}.{} where id={}""".format(self.name, table, node_down)).fetchone()
        id_pipe = self.add_pipe_link([geom_up,geom_down], z_invert_up, z_invert_down)
        # updates on singularities zq_split, qq_split, reservoir_rs and reservoir rsp that might reference the deleted pipes
        tables = {'qq_split_hydrology_singularity':['downstream', 'split1', 'split2'],
                    'zq_split_hydrology_singularity':['downstream', 'split1', 'split2'],
                    'reservoir_rs_hydrology_singularity':['drainage', 'overflow'],
                    'reservoir_rsp_hydrology_singularity':['drainage', 'overflow']}
        for tab, columns in tables.iteritems():
            for column in columns:
                self.__proj.execute("""update {model}.{tab}
                                        set {column}='{new_pipe}'
                                        where {column}={old_pipe} and node={node}
                                        """.format(model=self.name, tab=tab, column=column, new_pipe=id_pipe, old_pipe= pipe_up, node=node_up))
                self.__proj.execute("""update {model}.{tab}
                                        set {column}='{new_pipe}'
                                        where {column}={old_pipe} and node={node}
                                        """.format(model=self.name, tab=tab, column=column, new_pipe=id_pipe, old_pipe= pipe_down, node=node_down))
        #delete old node
        DeleteTool.delete_node(None, self.__proj, self.name, table, id_node)
        return id_pipe

    def fuse_reach(self, reach1_id, reach2_id):
        # insert new reach
        id_reach = self.__proj.execute("""with r1 as (select geom from {model}.reach where id={id1}),
                                               r2 as (select geom from {model}.reach where id={id2})
                                        insert into {model}.reach(geom) select ST_MakeLine(r1.geom, r2.geom) from r1, r2 limit 1 returning id""".format(model=self.name, id1=reach1_id, id2=reach2_id)).fetchone()[0]
        # update nodes
        self.__proj.execute("""update {}.river_node set reach={} where reach={} or reach={}""".format(self.name, id_reach, reach1_id, reach2_id))
        # delete old reach
        self.__proj.execute("""delete from {}.reach where id={} or id={}""".format(self.name, reach1_id, reach2_id))
        return id_reach

    def split_reach(self, reach_id, pk):
        # get info on reach
        reach_length, reach_geom = self.__proj.execute("""select ST_Length(reach.geom), geom from {}.reach where id={}""".format(self.name,reach_id)).fetchone()
        # insert new river nodes at new reaches extremities
        geom_node_1, = self.__proj.execute("""select ST_LineInterpolatePoint(reach.geom, {}) from {}.reach where id={}""".format((pk*reach_length-5)/reach_length, self.name,reach_id)).fetchone()
        self.add_river_node(geom_node_1)
        geom_node_2, = self.__proj.execute("""select ST_LineInterpolatePoint(reach.geom, {}) from {}.reach where id={}""".format((pk*reach_length+5)/reach_length, self.name,reach_id)).fetchone()
        self.add_river_node(geom_node_2)
        # delete old reach
        self.__proj.execute("""update {}.river_node set reach=null where reach={}""".format(self.name, reach_id))
        self.__proj.execute("""delete from {}.reach where id={}""".format(self.name, reach_id))
        # insert new reaches
        reach_id_1, reach_geom_1 = self.__proj.execute("""insert into {m}.reach(geom) select ST_Line_Substring('{g}', 0, {p}) returning id, geom""".format(m=self.name, g=reach_geom, p=(pk*reach_length-5)/reach_length)).fetchone()
        reach_id_2, reach_geom_2 = self.__proj.execute("""insert into {m}.reach(geom) select ST_Line_Substring('{g}', {p}, 1) returning id, geom""".format(m=self.name, g=reach_geom, p=(pk*reach_length+5)/reach_length)).fetchone()
        # update nodes
        self.__proj.execute("""update {}.river_node set reach={} where ST_Intersects(ST_Buffer(river_node.geom, 0.1), '{}')""".format(self.name, reach_id_1, reach_geom_1))
        self.__proj.execute("""update {}.river_node set reach={} where ST_Intersects(ST_Buffer(river_node.geom, 0.1), '{}')""".format(self.name, reach_id_2, reach_geom_2))


    def reverse_link(self, table, link_id):
        up, down, up_type, down_type = self.__proj.execute("""select up, down, up_type, down_type from {}.{} where id={}""".format(self.name, table, link_id)).fetchone()
        geoms = self.__proj.execute("""with points as (select (st_dumppoints(geom)).geom as pt from {}.{} where id={})
                                                select ST_X(pt), ST_Y(pt) from points""".format(self.name, table, link_id)).fetchall()
        geom = "'srid={}; LINESTRINGZ({})'::geometry".format(
                str(self.__srid),
                ",".join([
                    str(p[0])+" "+str(p[1])+" "+(str(p[2]) if len(p) == 3 else "9999")
                    for p in geoms[::-1]])) if geoms else None
        self.__proj.try_execute("""update {}.{} set geom={}, up={}, down={}, up_type='{}', down_type='{}' where id={}""".format(self.name, table, geom, down, up, down_type, up_type, link_id))