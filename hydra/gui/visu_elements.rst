visu tool elements
------------------


========================================  =====  ======  ==========  ==========  ==========  ==========
table                                     file   abrev   param1      param2      param3      param4 
========================================  =====  ======  ==========  ==========  ==========  ==========
borda_headloss_link                       w14    QMK     ql(m3/s)    zam(m)      zav(m)      dE(m)     
borda_headloss_singularity                w14    SMK     q(m3/s)     zam(m)      zav(m)      dE(m)     
bradley_headloss_singularity              w14    BRD     qam(m3/s)   zam(m/s)    zav(m/s)  
bridge_headloss_singularity               w14    BRDG    qam(m3/s)   zam(m/s)    zav(m/s)  
catchment_node                            w13    BC      int_pluie(mm/h)  qruis(m3/s)
connector_hydrology_link                  w13    TRH     q(m3/s)   
connector_link                            w14    QMTR    ql(m3/s)    zam(m)      zav(m)    
constant_inflow_bc_singularity            w14    HC      q(m3/s)     z(m)      
crossroad_node                            w15    CAR     None        z(m)        ux(m/s)     uy(m/s)   
deriv_pump_link                           w14    QDP     ql(m3/s)    zam(m)      zav(m)    
elem_2d_node                              w15    PAV     None        z(m)        ux(m/s)     uy(m/s)   
froude_bc_singularity                     w14    ZF      q(m3/s)     z(m)      
fuse_spillway_link                        w14    LDVF    ql(m3/s)    zam(m)      zav(m)    
gate_link                                 w14    LORF    ql(m3/s)    zam(m)      zav(m)      zs(m)     
gate_singularity                          w14    VA      qam(m3/s)   zam(m/s)    zav(m/s)    zs(m)     
hydraulic_cut_singularity                 w14    CPR     qam(m3/s)   zam(m/s)    zav(m/s)  
hydrograph_bc_singularity                 w14    HY      qapp(m3/s)  z(m)      
hydrology_bc_singularity                  w13    CLH     q(m3/s)   
manhole_node                              w15    NODA    q(m3/s)     z(m)        vit(m/s)    qoverflow(m3/s)
marker_singularity                        w14    MRKP    qav(m3/s)   z(m)        qap(m3/s) 
mesh_2d_link                              w14    LPAV    ql(m3/s)    zam(m)      zav(m)    
network_overflow_link                     w14    LDEB    ql(m3/s)    zam(m)      zav(m)    
overflow_link                             w14    LOVFL   ql(m3/s)    zam(m)      zav(m)      b_rupt(m) 
param_headloss_singularity                w14    DH      qam(m3/s)   zam(m/s)    zav(m/s)  
pipe_branch_marker_singularity            w14    MRKB    q(m3/s)     z(m)      
porous_link                               w14    LPOR    ql(m3/s)    zam(m)      zav(m)    
pump_link                                 w14    QMP     ql(m3/s)    zam(m)      zav(m)    
qq_split_hydrology_singularity            w13    DQH     qam(m3/s)   qav(m3/s)   qder1(m3/s)  qder2(m3/s)
regul_gate_link                           w14    RG      ql(m3/s)    zam(m)      zav(m)      zs(m)     
regul_sluice_gate_singularity             w14    ACTA    q(m3/s)     zam(m)      zav(m)      zs(m)     
reservoir_rs_hydrology_singularity        w13    RSH     qam(m3/s)   qf(m3/s)    qoverflow(m3/s)  qrestit(m3/s)
reservoir_rsp_hydrology_singularity       w13    RSPH    qam(m3/s)   qf(m3/s)    qoverflow(m3/s)  qrestit(m3/s)
river_node                                w15    NODR    q           z           vit_lm      q_lmaj    
routing_hydrology_link                    w13    ROUT    q(m3/s)   
station_node                              w15    NODS    None        z           area      
storage_node                              w15    CAS     None        z(m)        area(m2)    volm3)    
street_link                               w14    LRUE    ql(m3/s)    zam(m)      zav(m)      vit(m/s)  
strickler_bc_singularity                  w14    RK      q(m3/s)     z(m)      
strickler_link                            w14    LSTK    ql(m3/s)    zam(m)      zav(m)    
tank_bc_singularity                       w14    BO      None        z(m)        vol(m3)   
tz_bc_singularity                         w14    ZT      q(m3/s)     z(m)      
weir_bc_singularity                       w14    QDL     q(m3/s)     zam(m)      zav(m)    
weir_link                                 w14    LDEV    ql(m3/s)    zam(m)      zav(m)      zs(m)     
zq_bc_singularity                         w14    ZQ      q(m3/s)     z(m)      
zq_split_hydrology_singularity            w13    DZH     qam(m3/s)   qav(m3/s)   qder1(m3/s)  qder2(m3/s)
zregul_weir_singularity                   w14    DE      qam(m3/s)   zam(m/s)    zav(m/s)    zs/bs(m)  
========================================  =====  ======  ==========  ==========  ==========  ==========
