# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import io
import os
import numpy
import time
import matplotlib.patheffects as path_effects
from random import randint
from math import sqrt
from PyQt4 import uic
from PyQt4.QtGui import QApplication, QVBoxLayout, QWidget, QImage, QColor
from PyQt4.QtCore import Qt, QCoreApplication, QMargins, pyqtSignal, QObject
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT, FigureCanvasQTAgg as FigureCanvas
from matplotlib.font_manager import FontProperties
from hydra.utility.result_decoder import W16Result
from qgis.gui import QgsRubberBand
from qgis.core import QgsGeometry, QGis

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

_plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
_layer_icons_dir = os.path.join(_plugin_dir, "ressources", "layer_icons")
_icons_dir = os.path.join(_plugin_dir, "ressources", "images")
_color = ['royalblue','coral','aquamarine','powderblue','steelblue','limegreen','crimson','olivedrab','darkcyan','slategray']

def solve_non_continu(x_data_incont,y_data_incont,x_data_max,y_data_max) :
    if x_data_incont is None or y_data_incont is None :
        return(x_data_incont,y_data_incont,x_data_max,y_data_max)

    listsaved = []

    for i in range(len(x_data_incont)-1) :
        if x_data_incont[i] == x_data_incont[i+1]:
            save = [x_data_incont[i],y_data_incont[i]]
            listsaved.append(save)

    interp_y_data_incont = numpy.interp(x_data_max,x_data_incont,y_data_incont)

    if len(x_data_max) == len(interp_y_data_incont) :
        for save in listsaved :
            for i in range (len(x_data_max)) :
                if x_data_max[i] == save[0] :
                    x_data_max = numpy.insert(x_data_max,i,save[0])
                    interp_y_data_incont = numpy.insert(interp_y_data_incont,i,save[1])
                    y_data_max = numpy.insert(y_data_max,i,y_data_max[i])
                    save = [-1,-999]

    return(x_data_max,interp_y_data_incont,x_data_max,y_data_max)

class Highlighter(QObject):

    def __init__(self, map_canvas, line_wkt, pk0=0, parent=None):
        QObject.__init__(self, parent)

        self.__map_canvas = map_canvas
        self.__rb = QgsRubberBand(map_canvas, QGis.Line)
        self.__line = QgsGeometry.fromWkt(line_wkt)
        self.__rb.addGeometry(self.__line, None)
        self.__rb.setWidth(5)
        color = QColor(255, 0, 0, 128)
        self.__rb.setColor(color)
        self.__pk0 = pk0

    def __del__(self):
        self.__map_canvas.scene().removeItem(self.__rb)

    def show(self):
        self.__rb.show()

    def hide(self):
        self.__rb.hide()

    def set_extent_km(self, pk_min, pk_max):
        # section visibility bounds
        self.__rb.reset()
        km_to_m = 1000
        pk0 = self.__pk0
        start = max(0, (pk_min - pk0)*km_to_m)
        end = min(self.__line.length(), (pk_max - pk0)*km_to_m)

        vertices = [self.__line.interpolate(start).asPoint()]
        vertex_count = len(self.__line.asPolyline())
        distance = 0
        for i in range(1, vertex_count):
            vertex_i = self.__line.vertexAt(i)
            distance += sqrt(self.__line.sqrDistToVertexAt(vertex_i, i-1))
            # 2.16 distance = line.distanceToVertex(i)

            if distance <= start:
                pass
            elif distance < end:
                vertices += [vertex_i]
            else:
                break

        vertices += [self.__line.interpolate(end).asPoint()]

        self.__rb.addGeometry(QgsGeometry.fromPolyline(vertices), None)


class LongProfilInterface(QWidget):

    def __getattr__(self, att):
        if att =='display_ground_level':
            return self.__parent.display_ground_level if (self.__parent and hasattr(self.__parent,'display_ground_level')) else self.__display_ground_level
        elif att=='display_base_line':
            return  self.__parent.display_base_line if (self.__parent and hasattr(self.__parent,'display_base_line')) else self.__display_base_line
        elif att=='display_vault_line':
            return  self.__parent.display_vault_line if (self.__parent and hasattr(self.__parent,'display_vault_line')) else self.__display_vault_line
        elif att=='display_r_bank':
            return  self.__parent.display_r_bank if (self.__parent and hasattr(self.__parent,'display_r_bank')) else self.__display_r_bank
        elif att=='display_l_bank':
            return  self.__parent.display_l_bank if (self.__parent and hasattr(self.__parent,'display_l_bank')) else self.__display_l_bank
        elif att=='display_singularities':
            return  self.__parent.display_singularities if (self.__parent and hasattr(self.__parent,'display_singularities')) else self.__display_singularities
        elif att=='display_links':
            return  self.__parent.display_links if (self.__parent and hasattr(self.__parent,'display_links')) else self.__display_links
        elif att=='display_cross_sections':
            return  self.__parent.display_cross_sections if (self.__parent and hasattr(self.__parent,'display_cross_sections')) else self.__display_cross_sections
        elif att=='display_water_line':
            return  self.__parent.display_water_line if (self.__parent and hasattr(self.__parent,'display_water_line')) else self.__display_water_line
        elif att=='display_water_flow':
            return  self.__parent.display_water_flow if (self.__parent and hasattr(self.__parent,'display_water_flow')) else self.__display_water_flow
        elif att=='display_water_minmax':
            return  self.__parent.display_water_minmax if (self.__parent and hasattr(self.__parent,'display_water_minmax')) else self.__display_water_minmax
        elif att=='display_overflow_z':
            return  self.__parent.display_overflow_z if (self.__parent and hasattr(self.__parent,'display_overflow_z')) else self.__display_overflow_z
        elif att=='display_water_speed':
            return  self.__parent.display_water_speed if (self.__parent and hasattr(self.__parent,'display_water_speed')) else self.__display_water_speed
        elif att=='display_water_line_comp':
            return  self.__parent.display_water_line_comp if (self.__parent and hasattr(self.__parent,'display_water_line_comp')) else self.__display_water_line_comp
        elif att=='display_water_flow_comp':
            return  self.__parent.display_water_flow_comp if (self.__parent and hasattr(self.__parent,'display_water_flow_comp')) else self.__display_water_flow_comp
        elif att=='display_water_minmax_comp':
            return  self.__parent.display_water_minmax_comp if (self.__parent and hasattr(self.__parent,'display_water_minmax_comp')) else self.__display_water_minmax_comp
        elif att=='display_water_speed_comp':
            return  self.__parent.display_water_speed_comp if (self.__parent and hasattr(self.__parent,'display_water_speed_comp')) else self.__display_water_speed_comp
        elif att=='branchs':
            return  self.__parent.branchs if (self.__parent and hasattr(self.__parent,'branchs')) else self.__branchs
        elif att=='list_secondary_scenarios':
            return  self.__parent.list_secondary_scenarios if (self.__parent and hasattr(self.__parent,'list_secondary_scenarios')) else self.__list_secondary_scenarios
        else:
            raise AttributeError

    def __setattr__(self, name, value):
        if name=='display_ground_level':
            self.__display_ground_level=value
        elif name=='display_base_line':
            self.__display_base_line=value
        elif name=='display_vault_line':
            self.__display_vault_line=value
        elif name=='display_r_bank':
            self.__display_r_bank=value
        elif name=='display_l_bank':
            self.__display_l_bank=value
        elif name=='display_singularities':
            self.__display_singularities=value
        elif name=='display_links':
            self.__display_links=value
        elif name=='display_cross_sections':
            self.__display_cross_sections=value
        elif name=='display_water_line':
            self.__display_water_line=value
        elif name=='display_water_flow':
            self.__display_water_flow=value
        elif name=='display_water_minmax':
            self.__display_water_minmax=value
        elif name=='display_overflow_z':
            self.__display_overflow_z=value
        elif name=='display_water_speed':
            self.__display_water_speed=value
        elif name=='display_water_line_comp':
            self.__display_water_line_comp=value
        elif name=='display_water_flow_comp':
            self.__display_water_flow_comp=value
        elif name=='display_water_minmax_comp':
            self.__display_water_minmax_comp=value
        elif name=='display_water_speed_comp':
            self.__display_water_speed_comp=value
        elif name=='branchs':
            self.__branchs=value
        elif name=='list_secondary_scenarios':
            self.__list_secondary_scenarios=value
        else:
            super(LongProfilInterface, self).__setattr__(name, value)

    def __init__ (self,iface,project,time_control, parent=None):
        QWidget.__init__(self)

        self.__isreloading = True
        self.setVisible(False)

        self.__iface = iface
        self.__project = project
        self.__time_control = time_control
        self.__time_control.timeChanged.connect(self.time_changed)
        self.__parent = parent

        self.setWindowModality(Qt.NonModal)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.setWindowTitle("Graph window")
        self.resize(900,500)
        self.graph_window = None
        self.graph_window = GraphWindow(self.__project,self)

        self.__display_singularities = False
        self.__display_cross_sections = False
        self.__display_links = False
        self.__display_ground_level = False
        self.__display_base_line = False
        self.__display_vault_line = False
        self.__display_l_bank = False
        self.__display_r_bank = False
        self.__display_water_line = False
        self.__display_water_flow = False
        self.__display_water_minmax = False
        self.__display_overflow_z = False
        self.__display_water_speed = False
        self.__display_water_line_comp = False
        self.__display_water_flow_comp = False
        self.__display_water_minmax_comp = False
        self.__display_water_speed_comp = False

        self.__list_secondary_scenarios = []
        self.__branchs = []

        self.__idx = 0
        self.__show_struct = False
        self.__show_links = False
        self.__show_cross = False
        self.__has_result = False
        self.init_locals()
        self.__isreloading = False

    def init_locals(self):
        self.__values = [[]]
        self.__values_secondary_list=[]
        self.__pks_results=numpy.array([])
        self.__pks_geom=numpy.array([])
        self.__baseline=numpy.array([])
        self.__vaultline=numpy.array([])
        self.__rbankline=numpy.array([])
        self.__lbankline=numpy.array([])
        self.__overflow=numpy.array([])
        self.__topo=numpy.array([])
        self.__waterline_min=numpy.array([])
        self.__waterline_max=numpy.array([])
        self.__speedline=numpy.array([])
        self.__labelshover_point_Z=numpy.array([])
        self.__labelshover_point_pk=numpy.array([])
        self.__labels_marker = []
        self.__labelslink = []
        self.__highlighters = []
        self.__labels = []

        self.__struct_rpr_collection = {'weir':[],'gate':[],'various':[],'tank_bc':[],'zq_bc':[]}

        self.init_collection(['baselinecollection','rbankcollection','lbankcollection','vaultcollection','groundcollection','watercollection','watercollection_comparator'])
        self.init_scn_comparator()

    def init_collection(self,listcollection):
        for c in listcollection:
            if hasattr(self.graph_window, c):
                vars(self.graph_window)[c].reset()
                vars(self.graph_window)[c] = GraphWindow.LineCollection([])
            else:
                vars(self.graph_window)[c] = GraphWindow.LineCollection([])

    def init_scn_comparator(self):
        for scn in self.list_secondary_scenarios:

            if hasattr(self.graph_window, 'watercollection_' + scn[1]):
                vars(self.graph_window)['watercollection_' +scn[1]].reset()
                vars(self.graph_window)['watercollection_' +scn[1]] = GraphWindow.LineCollection([])
            else :
                vars(self.graph_window)['watercollection_' +scn[1]] = GraphWindow.LineCollection([])
            self.graph_window.init_lines_for_scn(scn)

    def display(self):
        self.reload(override = True)
        self.show()

    def copy_image(self):
        self.graph_window.export_image()

    def copy_data(self):
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)
        data_to_export = tr("Geometry") + '\r\n'
        data_to_export = data_to_export + tr("pk (km)")
        data_to_export = data_to_export + '\t' + tr("baseline (m)")
        data_to_export = data_to_export + '\t' + tr("topo (m)")
        data_to_export = data_to_export + '\t' + tr("vault (m)")
        data_to_export = data_to_export + '\t' + tr("left bank (m)")
        data_to_export = data_to_export + '\t' + tr("rigth bank (m)")

        data_to_export = data_to_export + '\r\n'
        for i in range(0, len(self.__pks_geom)):
            data_to_export = data_to_export + str(self.__pks_geom[i])
            data_to_export = data_to_export + '\t' + (str(self.__baseline[i]) if i<len(self.__baseline) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__topo[i]) if i<len(self.__topo) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__vaultline[i]) if i<len(self.__vaultline) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__rbankline[i]) if i<len(self.__rbankline) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__lbankline[i]) if i<len(self.__lbankline) else "-")
            data_to_export = data_to_export + '\r\n'

        data_to_export = data_to_export + '\r\n' + tr("Results") + '\r\n'

        data_to_export = data_to_export + tr("pk (km)")
        data_to_export = data_to_export + '\t' + tr("overflow (m)")
        data_to_export = data_to_export + '\t' + tr("waterline z (m)")
        data_to_export = data_to_export + '\t' + tr("waterline zmin (m)")
        data_to_export = data_to_export + '\t' + tr("waterline zmax (m)")
        data_to_export = data_to_export + '\t' + tr("waterline Q (m3/s)")
        data_to_export = data_to_export + '\t' + tr("waterline Qmin (m3/s)")
        data_to_export = data_to_export + '\t' + tr("waterline Qmax (m3/s)")

        data_to_export = data_to_export + '\r\n'
        for i in range(0, len(self.__pks_results)):
            data_to_export = data_to_export + str(self.__pks_results[i])
            data_to_export = data_to_export + '\t' + (str(self.__overflow[i]) if i<len(self.__overflow) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__values[i][0]) if (i<len(self.__values) and len(self.__values[i])>0) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__waterline_min[i][0]) if (i<len(self.__waterline_min) and len(self.__waterline_min[i])>0) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__waterline_max[i][0]) if (i<len(self.__waterline_max) and len(self.__waterline_max[i])>0) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__values[i][1]) if (i<len(self.__values) and len(self.__values[i])>0) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__waterline_min[i][1]) if (i<len(self.__waterline_min) and len(self.__waterline_min[i])>1) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__waterline_max[i][1]) if (i<len(self.__waterline_max) and len(self.__waterline_max[i])>1) else "-")
            data_to_export = data_to_export + '\r\n'

        cb.setText(data_to_export, mode=cb.Clipboard)

    def remove_secondary_scn (self,scn):
        if hasattr(self.graph_window,'watercollection_' + scn[1]):
            vars(self.graph_window)['watercollection_' + scn[1]].reset()
            vars(self.graph_window)['Qline_' +  scn[1]].reset()
            vars(self.graph_window)['waterline_min_' +  scn[1]].reset()
            vars(self.graph_window)['waterline_max_' +  scn[1]].reset()
            vars(self.graph_window)['speedline_' +  scn[1]].reset()

    def time_changed(self, idx):
        if self.__has_result:
            self.__values = self.result_at_time(idx)
            self.__values_secondary_list = self.result_at_time_collection(idx)
            self.__idx = idx
            color_list = []
            if self.display_water_line == True:
                lenght_collection,format = self.graph_window.watercollection.get_collection_format()
                total_lines = 0
                self.graph_window.watercollection.reset()
                for i in range (lenght_collection):
                    total_lines = total_lines + format[i]
                    self.graph_window.watercollection.append_line(self.graph_window.new_line(1,'royalblue', '-',self.__pks_results[total_lines:(total_lines+format[i+1])],self.__values[:,0][total_lines:(total_lines+format[i+1])]),'waterline')
                    self.graph_window.watercollection.show_collection(True,'waterline')
                for p in self.graph_window.listfillbetween:
                    p.remove()
                self.graph_window.listfillbetween = self.graph_window.watercollection.fill_between_collection(self.graph_window.baselinecollection,'lightblue',maxmode=True,max_secondary_collection=self.graph_window.vaultcollection)

            if self.display_water_flow == True:
                self.graph_window.second_line.set_data(self.__pks_results, self.__values[:,1])

            if self.display_water_speed == True:
                self.graph_window.speedline.set_data(self.__pks_results, self.__values[:,3])

            if self.display_water_line_comp:
                for scn_values in self.__values_secondary_list:
                    lenght_collection,format = self.graph_window.watercollection.get_collection_format()
                    total_lines=0
                    color =(vars(self.graph_window)['watercollection_'+scn_values[1]].get_collection()[0].get_color())
                    vars(self.graph_window)['watercollection_'+scn_values[1]].reset()
                    for i in range (lenght_collection):
                        total_lines = total_lines + format[i]
                        vars(self.graph_window)['watercollection_'+scn_values[1]].append_line(self.graph_window.new_line(1,color, ':',self.__pks_results[total_lines:(total_lines+format[i+1])],scn_values[2][:,0][total_lines:(total_lines+format[i+1])]),scn_values[1]+' waterline')
                        vars(self.graph_window)['watercollection_'+scn_values[1]].show_collection(True,scn_values[1]+' waterline')

            if self.display_water_flow_comp == True:
                for scn_values in self.__values_secondary_list:
                    vars(self.graph_window)['Qline_'+scn_values[1]].set_data(self.__pks_results,scn_values[2][:,1])

            if self.display_water_speed_comp == True:
                for scn_values in self.__values_secondary_list:
                    vars(self.graph_window)['speedline_'+scn_values[1]].set_data(self.__pks_results,scn_values[2][:,3])

            self.graph_window.reload_legend()
            self.__time_control.nextFrame()

    def calcul_max_extent(self) :
        #typevalue : 1 => Q , débit / 3=> V , vitesse
        ymaxQ=0
        ymaxV=0
        if self.__waterline_max is not None and len(self.__pks_results) != 0:
            for i in range(0, len(self.__pks_results)):
                if self.__waterline_max[i][1] > ymaxQ :
                    ymaxQ = self.__waterline_max[i][1]
                #if self.__waterline_max[i][3] > ymaxV :
                    #ymaxV = self.__waterline_max[i][3]
            self.graph_window.ymaxQ = ymaxQ +float(0.1*ymaxQ)
            self.graph_window.xmaxQ = self.__pks_results[-1]
            #self.graph_window.ymaxV = ymaxV +float(0.1*ymaxV)
            #self.graph_window.xmaxV = self.__pks_results[-1]

    def init_profile(self):
        self.graph_window.speedline.reset()
        self.graph_window.waterline_min.reset()
        self.graph_window.waterline_max.reset()
        self.graph_window.overflow.reset()
        self.graph_window.second_line.reset()
        self.graph_window.label_points.reset()
        for scn_values in self.__values_secondary_list:
            self.graph_window.init_lines_for_scn([scn_values[0],scn_values[1]])

        self.graph_window.baselinecollection.show_collection(False)
        self.graph_window.rbankcollection.show_collection(False)
        self.graph_window.lbankcollection.show_collection(False)
        self.graph_window.vaultcollection.show_collection(False)
        self.graph_window.groundcollection.show_collection(False)
        self.graph_window.watercollection.show_collection(False)

        self.graph_window.label_points_collection.reset()
        self.graph_window.label_points_collection.show_collection(False)

        #reset singularities
        for struct in self.graph_window.structure_collection:
            struct.reset()
        self.graph_window.structure_collection = []
        #reset fillbetween
        for j in self.graph_window.listfillbetween:
            j.remove()
        self.graph_window.listfillbetween = []

        for p in self.graph_window.listgroundlevel:
            p.remove()
        self.graph_window.listgroundlevel = []

        if self.display_base_line == True:
            self.graph_window.baselinecollection.show_collection(True,'baseline')
        if self.display_ground_level == True:
            self.graph_window.groundcollection.show_collection(True,'groundline')
        if self.display_vault_line == True:
            self.graph_window.vaultcollection.show_collection(True,'vaultline')
            self.graph_window.listgroundlevel = self.graph_window.groundcollection.fill_between_collection(self.graph_window.vaultcollection,'lightgrey')
        if self.display_r_bank == True:
            self.graph_window.rbankcollection.show_collection(True,'rbank')
        if self.display_l_bank == True:
            self.graph_window.lbankcollection.show_collection(True,'lbank')
        if self.display_links == True:
            self.graph_window.label_points.set_data(self.__labelshover_point_pk,self.__labelshover_point_Z)
            self.graph_window.label_points.set_line_visible(False)
            self.graph_window.labels_link.set_data(self.__labelslink)
            self.make_style_links_labels()
            self.graph_window.label_points_collection.show_collection(True)
        if self.display_singularities == True:
            for elem in self.__struct_rpr_collection['weir']:
                self.graph_window.structure_collection.append(self.graph_window.new_line(1,'black', '-',elem[0],elem[1]))
            for elem in self.__struct_rpr_collection['gate']:
                self.graph_window.structure_collection.append(self.graph_window.new_line(1,'black', '-',elem[0][0],elem[0][1],linewidth=6))
                self.graph_window.structure_collection.append(self.graph_window.new_line(1,'white', '-',elem[1][0],elem[1][1],linewidth=3,patheffect='black'))
                self.graph_window.structure_collection.append(self.graph_window.new_line(1,'black', '-',elem[2][0],elem[2][1],linewidth=6))
            for elem in self.__struct_rpr_collection['various']:
                self.graph_window.structure_collection.append(self.graph_window.new_line(1,'black', '',elem[0],elem[1],marker='*',pointstyle=True))
            for elem in self.__struct_rpr_collection['tank_bc']:
                self.graph_window.structure_collection.append(self.graph_window.new_line(1,'black', '-',elem[0],elem[1]))
            for elem in self.__struct_rpr_collection['zq_bc']:
                self.graph_window.structure_collection.append(self.graph_window.new_line(1,'steelblue', '-',elem[0],elem[1],linewidth=0.25))

        if self.__has_result:
            if self.display_water_minmax == True:
                self.graph_window.waterline_min.set_data(self.__pks_results, self.__waterline_min[:,0])
                self.graph_window.waterline_max.set_data(self.__pks_results, self.__waterline_max[:,0])
            if self.display_water_line == True:
                self.graph_window.watercollection.show_collection(True,'waterline')
                self.graph_window.listfillbetween = self.graph_window.watercollection.fill_between_collection(self.graph_window.baselinecollection,'lightblue',maxmode=True,max_secondary_collection=self.graph_window.vaultcollection)
            if self.display_water_flow == True:
                self.graph_window.second_line.set_data(self.__pks_results, self.__values[:,1])
            if self.display_overflow_z == True:
                self.graph_window.overflow.set_data(self.__pks_results, self.__overflow)
            if self.display_water_speed == True:
                self.graph_window.speedline.set_data(self.__pks_results, self.__values[:,3])
            if self.display_water_line_comp == True:
                for scn_values in self.__values_secondary_list:
                    vars(self.graph_window)['watercollection_'+scn_values[1]].show_collection(True,'waterline_'+scn_values[1])
            if self.display_water_minmax_comp == True:
                for scn_values in self.__values_secondary_list:
                    vars(self.graph_window)['waterline_min_' + scn_values[1]].set_data(self.__pks_results,vars(self)['__waterline_min_' + scn_values[1]][:,0])
                    vars(self.graph_window)['waterline_max_' + scn_values[1]].set_data(self.__pks_results,vars(self)['__waterline_max_'+scn_values[1]][:,0])
                    vars(self.graph_window)['waterline_max_' + scn_values[1]].set_legend('Water max z ' + scn_values[1])
                    vars(self.graph_window)['waterline_min_' + scn_values[1]].set_legend('Water min z ' + scn_values[1])
            if self.display_water_flow_comp == True:
                for scn_values in self.__values_secondary_list:
                    vars(self.graph_window)['Qline_' + scn_values[1]].set_data(self.__pks_results,scn_values[2][:,1])
                    vars(self.graph_window)['Qline_' + scn_values[1]].set_legend('Water Q ' + scn_values[1])
            if self.display_water_speed_comp == True:
                for scn_values in self.__values_secondary_list:
                    vars(self.graph_window)['speedline_' + scn_values[1]].set_data(self.__pks_results,scn_values[2][:,3])
                    vars(self.graph_window)['speedline_' + scn_values[1]].set_legend('Water speed ' + scn_values[1])

        if len(self.__labels)>0:
            self.graph_window.labels.set_data([v[0] for v in self.__labels], numpy.interp(numpy.array([v[0] for v in self.__labels]), self.__pks_geom, self.__baseline) , [v[1] for v in self.__labels])
        else:
            self.graph_window.labels.reset_labels()

        self.calcul_max_extent()
        self.graph_window.set_extent()
        self.graph_window.reload_legend()

    def result_at_time(self, istep, result_path = 'current_scn'):
        values = None
        for branch in self.branchs:
            if result_path == 'current_scn' :
                w16result = W16Result(self.__result_path)
                results = w16result[branch.name][istep][0]
                tmpres = numpy.array([r[1] for r in results])
                values = numpy.concatenate([values,tmpres]) if values is not None else tmpres
            else:
                w16result = W16Result(result_path)
                results = w16result[branch.name][istep][0]
                tmpres = numpy.array([r[1] for r in results])
                values = numpy.concatenate([values,tmpres]) if values is not None else tmpres
        return values

    def result_at_time_collection(self,istep):
        values_collection_comparator = []

        for scn in self.list_secondary_scenarios :
            if scn[2] is not None:
                values_collection_comparator.append([scn[0],scn[1], self.result_at_time(istep, scn[2])])
        return values_collection_comparator

    def proceed_branch(self, tool, labels, linkhover_label, singularities_draw):
        model = self.__project.get_current_model().name
        line_wkt, = self.__project.execute("""
            select ST_AsText(geom) from {model}.{table}
            where id={id}
            """.format(
                model=tool.schema,
                table=tool.table,
                id=tool.id)).fetchone()

        self.__has_result = True
        self.graph_window.has_result = True
        self.__load_from_geom(labels, linkhover_label, singularities_draw, line_wkt, tool)

        if self.__project.get_current_scenario() is None:
            self.__has_result = False
            self.graph_window.has_result = False

        else:
            id_scn, name_scn = self.__project.get_current_scenario()
            self.__result_path = os.path.join(self.__project.get_senario_path(id_scn), "hydraulique", name_scn + "_" + model + ".w16")
            for i in range(len(self.list_secondary_scenarios)):
                scn = self.list_secondary_scenarios[i]
                path = os.path.join(self.__project.get_senario_path(scn[0]), "hydraulique", scn[1] + "_" + model + ".w16")
                self.list_secondary_scenarios[i][2] = path

            if not os.path.exists(self.__result_path):
                self.__has_result = False
                self.graph_window.has_result = False

        for i in range(len(self.list_secondary_scenarios)):
            path = self.list_secondary_scenarios[i][2]
            if not os.path.exists(path):
                self.list_secondary_scenarios[i][2] = None

        if self.__has_result:
                self.__load_from_results(line_wkt, tool)

        self.graph_window.pk_range_changed.connect(self.__highlighters[-1].set_extent_km)
        self.init_profile()
        self.graph_window.plot_graph(self.__has_result,self.display_water_flow,self.display_water_speed)

    def reload(self, override = False):
        if (not self.__isreloading and self.isVisible()) or override:
            self.__isreloading = True
            self.init_locals()
            for branch in self.branchs:
                labels, linkhover_label, singularities_draw = self.reload_new_labels(branch)
                self.proceed_branch(branch, labels, linkhover_label, singularities_draw)
            self.__isreloading = False

    def reload_new_labels(self, tool):

        #declaration des requetes sql

        sql_singularity_label_reach = """select n.pk_km, s.name
                    from {model}._singularity as s join {model}.river_node as n on s.node=n.id
                    where n.reach={reach}"""
        sql_singularity_draw_reach = """SELECT wbs.name as name, n.pk_km,1 as rpr, wbs.z_weir as z1, 0 as z2, 0 as z3
                FROM {model}.weir_bc_singularity as wbs,{model}.river_node as n
                WHERE n.reach={reach} and wbs.node = n.id
                UNION
                SELECT gs.name as name,  n.pk_km,2 as rpr, gs.z_invert as z1, z_ceiling as z2, z_ceiling+0.5 as z3
                FROM {model}.gate_singularity as gs,{model}.river_node as n
                WHERE n.reach={reach} and gs.node = n.id
                UNION
                SELECT hc.name as name,  n.pk_km,3 as rpr, n.z_ground as z1, 0 as z2, 0 as z3
                FROM {model}.hydraulic_cut_singularity as hc,{model}.river_node as n
                WHERE hc.node = n.id and n.reach={reach}
                UNION
                SELECT bh.name as name,  n.pk_km,3 as rpr, n.z_ground as z1, 0 as z2, 0 as z3
                FROM {model}.borda_headloss_singularity as bh,{model}.river_node as n
                WHERE bh.node = n.id and n.reach={reach}
                UNION
                SELECT bh.name as name,  n.pk_km,2 as rpr, zw_array[1][1] as z1, z_ceiling as z2, z_ceiling+0.5 as z3
                FROM {model}.bradley_headloss_singularity as bh,{model}.river_node as n
                WHERE bh.node = n.id and n.reach={reach}
                UNION
                SELECT bh.name as name,  n.pk_km,2 as rpr, zw_array[1][1] as z1, zw_array[array_length(zw_array,1)][1] as z2, z_road as z3
                FROM {model}.bridge_headloss_singularity as bh,{model}.river_node as n
                WHERE bh.node = n.id and n.reach={reach}
                UNION
                SELECT ph.name as name,  n.pk_km,3 as rpr, n.z_ground as z1, 0 as z2, 0 as z3
                FROM {model}.param_headloss_singularity as ph,{model}.river_node as n
                WHERE ph.node = n.id and n.reach={reach}
                UNION
                SELECT ms.name as name,  n.pk_km,3 as rpr, n.z_ground as z1, 0 as z2, 0 as z3
                FROM {model}.marker_singularity as ms,{model}.river_node as n
                WHERE ms.node = n.id and n.reach={reach}
                UNION
                SELECT hbs.name as name,  n.pk_km,3 as rpr, n.z_ground as z1, 0 as z2, 0 as z3
                FROM {model}.hydrograph_bc_singularity as hbs,{model}.river_node as n
                WHERE hbs.node = n.id and n.reach={reach}
                UNION
                SELECT cib.name as name,  n.pk_km,3 as rpr, n.z_ground as z1, 0 as z2, 0 as z3
                FROM {model}.constant_inflow_bc_singularity as cib,{model}.river_node as n
                WHERE cib.node = n.id and n.reach={reach}
                UNION
                SELECT zbs.name as name,  n.pk_km,5 as rpr, zq_array[1][1] as z1, zq_array[array_length(zq_array,1)][1] as z2, 0 as z3
                FROM {model}.zq_bc_singularity as zbs,{model}.river_node as n
                WHERE zbs.node=n.id and n.reach={reach}
                UNION
                SELECT tbs.name as name,  n.pk_km,3 as rpr, n.z_ground as z1, 0 as z2, 0 as z3
                FROM {model}.tz_bc_singularity as tbs,{model}.river_node as n
                WHERE tbs.node = n.id and n.reach={reach}
                UNION
                SELECT tbs.name as name,  n.pk_km,3 as rpr, n.z_ground as z1, 0 as z2, 0 as z3
                FROM {model}.froude_bc_singularity as tbs,{model}.river_node as n
                WHERE tbs.node = n.id and n.reach={reach}
                UNION
                SELECT tbs.name as name,  n.pk_km,3 as rpr, n.z_ground as z1, 0 as z2, 0 as z3
                FROM {model}.strickler_bc_singularity as tbs,{model}.river_node as n
                WHERE tbs.node = n.id and n.reach={reach}
                UNION
                SELECT tbs.name as name,  n.pk_km,4 as rpr, zs_array[1][1] as z1, zs_array[array_length(zs_array,1)][1] as z2, 0 as z3
                FROM {model}.tank_bc_singularity as tbs,{model}.river_node as n
                WHERE tbs.node=n.id and n.reach={reach}
                UNION
                SELECT tbs.name as name,  n.pk_km,3 as rpr, n.z_ground as z1, 0 as z2, 0 as z3
                FROM {model}.model_connect_bc_singularity as tbs,{model}.river_node as n
                WHERE tbs.node = n.id and n.reach={reach}"""

        sql_crosssection_label_reach = """select n.pk_km, s.name
                from {model}.river_cross_section_profile as s join {model}.river_node as n on s.id=n.id
                where n.reach={reach}"""

        sql_linkhover_label_reach = """select n.pk_km, z.name as name, z.z_crest1 as z_print, '. #009e00' as symbol
                from {model}.river_node as n, {model}.overflow_link as z
                where n.reach={reach} and z.up = n.id and z.width1 <> 0
                union
                select n.pk_km, z.name as name, z.z_crest1 as z_print, '. #009e00' as symbol
                from {model}.river_node as n, {model}.overflow_link as z
                where n.reach={reach} and z.down = n.id and z.width1 <> 0
                union
                select n.pk_km, z.name as name, z.z_crest2 as z_print, '. #009e00' as symbol
                from {model}.river_node as n, {model}.overflow_link as z
                where n.reach={reach} and z.up = n.id and z.width1 = 0
                union
                select n.pk_km, z.name as name, z.z_crest2 as z_print, '. #009e00' as symbol
                from {model}.river_node as n, {model}.overflow_link as z
                where n.reach={reach} and z.down = n.id and z.width1 = 0
                union
                select n.pk_km, z.name as name, z.z_crest1 as z_print, '. #ff7f00' as symbol
                from {model}.river_node as n, {model}.strickler_link as z
                where n.reach={reach} and z.up = n.id
                union
                select n.pk_km, z.name as name, z.z_crest1 as z_print, '. #ff7f00' as symbol
                from {model}.river_node as n, {model}.strickler_link as z
                where n.reach={reach} and z.down = n.id
                union
                select n.pk_km, z.name as name, z.z_invert as z_print, 'X black' as symbol
                from {model}.river_node as n, {model}.gate_link as z
                where n.reach={reach} and z.up = n.id
                union
                select n.pk_km, z.name as name, z.z_invert as z_print, 'X black' as symbol
                from {model}.river_node as n, {model}.gate_link as z
                where n.reach={reach} and z.down = n.id
                union
                select n.pk_km,  z.name as name, z.z_invert as z_print, 'X red' as symbol
                from {model}.river_node as n, {model}.regul_gate_link as z
                where n.reach={reach} and z.up = n.id
                union
                select n.pk_km, z.name as name, z.z_invert as z_print, 'X red' as symbol
                from {model}.river_node as n, {model}.regul_gate_link as z
                where n.reach={reach} and z.down = n.id
                union
                select n.pk_km, z.name as name, z.z_invert as z_print, '_ #e31a1c' as symbol
                from {model}.river_node as n, {model}.porous_link as z
                where n.reach={reach} and z.up = n.id
                union
                select n.pk_km,  z.name as name, z.z_invert as z_print, '_ #e31a1c' as symbol
                from {model}.river_node as n, {model}.porous_link as z
                where n.reach={reach} and z.down = n.id
                union
                select n.pk_km, z.name as name, z.z_invert as z_print, '_ black' as symbol
                from {model}.river_node as n, {model}.weir_link as z
                where n.reach={reach} and z.up = n.id
                union
                select n.pk_km, z.name as name, z.z_invert as z_print, '_ black' as symbol
                from {model}.river_node as n, {model}.weir_link as z
                where n.reach={reach} and z.down = n.id"""

        with_sql_branch = """with nodes as (select n.name, n.id, n.z_ground, st_linelocatepoint(b.geom, n.geom)*st_length(b.geom)/1000 as pk_km
                    from {model}.manhole_node as n, {model}.branch as b
                    where st_dwithin(b.geom, n.geom, .1)
                    and b.id={reach})
                    """

        sql_singularity_label_branch = """select nodes.pk_km, s.name
                from {model}._singularity as s join nodes on nodes.id=s.node"""

        sql_linkhover_label_branch = """select nodes.pk_km, nodes.name as name, nodes.z_ground, 'o black' as symbol
                from nodes
                union
                select nodes.pk_km, nodes.name as name, nodes.z_ground, 'o black' as symbol
                from nodes """

        # construction des requetes
        # NB : reach = branch dans la formulation des requêtes pour simplicité

        sql_labels =""
        sql_linkhover_label =""
        sql_singularities_draw =""

        labels=[]
        linkhover_label = []
        singularities_draw = []

        if tool.table == 'reach' :

            if self.display_singularities == True :
                sql_labels += sql_singularity_label_reach
                sql_singularities_draw += sql_singularity_draw_reach
            if self.display_cross_sections == True:
                if not sql_labels=="":
                    sql_labels += """ union """
                sql_labels += sql_crosssection_label_reach
            if self.display_links == True:
                sql_linkhover_label += sql_linkhover_label_reach

        if tool.table == 'branch' :

            if self.display_singularities == True :
                sql_labels = with_sql_branch + sql_singularity_label_branch
            if self.display_links == True :
                sql_linkhover_label = with_sql_branch + sql_linkhover_label_branch

        # execution des requêtes

        if not sql_labels=="":
            sql_labels = sql_labels +""" order by pk_km;"""
            labels = self.__project.execute(sql_labels.format(reach=tool.id, model=tool.schema)).fetchall()
        if not sql_linkhover_label=="":
            sql_linkhover_label = sql_linkhover_label +""" order by pk_km;"""
            linkhover_label = self.__project.execute(sql_linkhover_label.format(reach=tool.id, model=tool.schema)).fetchall()
        if not sql_singularities_draw=="":
            sql_singularities_draw = sql_singularities_draw +""" order by pk_km;"""
            singularities_draw = self.__project.execute(sql_singularities_draw.format(reach=tool.id, model=tool.schema)).fetchall()

        return labels, linkhover_label, singularities_draw

    def __load_from_geom(self, labels, linkhover_label, singularities_draw, line_wkt, tool):
        pk_geom=None
        if tool.table == 'reach':
            baseline = []
            topo_line = []
            z_vault_line = []
            pk_geom = []
            l_bank = []
            r_bank = []
            pk0, = self.__project.execute("""
                    select pk0_km
                    from {model}.reach
                    where id={reach};
                    """.format(model=tool.schema, reach=tool.id)).fetchone()
            for pk_km, z_invert_up, z_invert_down, z_tn_up, z_tn_down, \
                z_lbank_up, z_lbank_down, z_rbank_up, z_rbank_down in \
                self.__project.execute("""
                    select n.pk_km, p.z_invert_up, p.z_invert_down, p.z_tn_up, p.z_tn_down,
                        p.z_lbank_up, p.z_lbank_down, p.z_rbank_up, p.z_rbank_down
                    from {model}.river_cross_section_profile p
                    join {model}.river_node n on n.id=p.id
                    where n.reach={reach}
                    order by n.pk_km
                    """.format(model=tool.schema, reach=tool.id)).fetchall():
                if z_invert_up is not None and z_invert_up!=0:
                    baseline.append(z_invert_up)
                    pk_geom.append(pk_km-pk0)
                    topo_line.append(z_tn_up)
                    r_bank.append(z_rbank_up)
                    l_bank.append(z_lbank_up)
                if z_invert_down is not None and z_invert_down!=0:
                    baseline.append(z_invert_down)
                    pk_geom.append(pk_km-pk0)
                    topo_line.append(z_tn_down)
                    r_bank.append(z_rbank_down)
                    l_bank.append(z_lbank_down)

        elif tool.table == 'branch':
            baseline = []
            topo_line = []
            z_vault_line = []
            pk_geom = []
            l_bank = []
            r_bank = []
            pk0, = self.__project.execute("""
                    select pk0_km
                    from {model}.branch
                    where id={branch};
                    """.format(model=tool.schema, branch=tool.id)).fetchone()
            for pk_km_up, z_invert_up, pk_km_down, z_invert_down, z_tn_up, z_tn_down, z_vault_up, z_vault_down in \
                self.__project.execute("""
                    select
                        st_linelocatepoint(b.geom, st_startpoint(p.geom))*st_length(b.geom)/1000 as pk_km_up,
                        z_invert_up,
                        st_linelocatepoint(b.geom, st_endpoint(p.geom))*st_length(b.geom)/1000 as pk_km_down,
                        z_invert_down,
                        z_tn_up,
                        z_tn_down,
                        z_vault_up,
                        z_vault_down
                    from {model}.pipe_link p
                    join {model}.branch b on b.id=p.branch
                    where b.id={branch}
                    order by pk_km_up
                    """.format(model=tool.schema, branch=tool.id)).fetchall():
                if z_invert_up is not None and z_invert_up!=0:
                    baseline.append(z_invert_up)
                    topo_line.append(z_tn_up)
                    z_vault_line.append(z_vault_up)
                    pk_geom.append(pk_km_up-pk0)
                if z_invert_down is not None and z_invert_down!=0:
                    baseline.append(z_invert_down)
                    topo_line.append(z_tn_down)
                    z_vault_line.append(z_vault_down)
                    pk_geom.append(pk_km_down-pk0)

        assert not pk_geom is None

        pk_offset = 0 if len(self.__pks_geom)==0 else self.__pks_geom[-1]

        linkhover_label_pk = []
        linkhover_label_z = []

        for l in labels:
            if len(l) == 2:
                pkl = [l[0]+pk_offset- pk0,l[1]]
            if len(l) == 3:
                pk1 = [l[0]+pk_offset- pk0,l[1],l[2]]
            self.__labels.append(pkl)

        for l in linkhover_label:
            pk2 = [l[0]+pk_offset- pk0,l[1],l[2]]
            linkhover_label_pk.append(l[0]+pk_offset- pk0)
            linkhover_label_z.append(l[2])
            self.__labels_marker.append(l[3])
            self.__labelslink.append(pk2)

        self.__labelshover_point_Z = numpy.append(self.__labelshover_point_Z,linkhover_label_z)
        self.__labelshover_point_pk = numpy.append(self.__labelshover_point_pk,linkhover_label_pk)

        pks = numpy.array([r + pk_offset for r in pk_geom])

        #singularities_draw : arraylike [name,pk,type representation,z1,z2,z3]
        #Zbaseline is interpolated and may fail !
        offset_sing = 0
        if len(self.__pks_geom)!= 0 :
            offset_sing = self.__pks_geom[-1]

        for l in singularities_draw:
            if l[2] == 1 :
                zbaseline = numpy.interp(offset_sing+l[1],pks,numpy.array(topo_line,dtype='float64'))
                line_rpr1=[[offset_sing+l[1],offset_sing+l[1]],[zbaseline,l[3]]]
                self.__struct_rpr_collection['weir'].append(line_rpr1)
            if l[2] == 2 :
                zbaseline = numpy.interp(offset_sing+l[1],pks,numpy.array(topo_line,dtype='float64'))
                line1_rpr2=[[offset_sing+l[1],offset_sing+l[1]],[zbaseline,l[3]]]
                line2_rpr2=[[offset_sing+l[1],offset_sing+l[1]],[l[3],l[4]]]
                line3_rpr2=[[offset_sing+l[1],offset_sing+l[1]],[l[4],l[5]]]
                self.__struct_rpr_collection['gate'].append([line1_rpr2,line2_rpr2,line3_rpr2])
            if l[2] == 3 :
                point_rpr3 = [[offset_sing+l[1]],[l[3]]]
                self.__struct_rpr_collection['various'].append(point_rpr3)
            if l[2] == 4 :
                line_rpr4=[offset_sing+[l[1],offset_sing+l[1]],[l[3],l[4]]]
                self.__struct_rpr_collection['tank_bc'].append(line_rpr4)
            if l[2] == 5 :
                line_rpr5=[[offset_sing+l[1],offset_sing+l[1]],[l[3],l[4]]]
                self.__struct_rpr_collection['zq_bc'].append(line_rpr5)

        self.__pks_geom = numpy.append(self.__pks_geom, pks)

        baseline = numpy.array(baseline)
        self.__baseline = numpy.append(self.__baseline, baseline) # zf
        self.__topo = numpy.append(self.__topo, topo_line) # z_tn
        self.__vaultline = numpy.append(self.__vaultline, z_vault_line) # z_vault
        self.__rbankline = numpy.append(self.__rbankline, r_bank) # r_bank
        self.__lbankline = numpy.append(self.__lbankline, l_bank) # l_bank
        self.__highlighters.append(Highlighter(self.__iface.mapCanvas(), line_wkt, pk_offset))

        # CollectionLine, append lignes for a branch
        self.graph_window.baselinecollection.append_line(self.graph_window.new_line(1,'grey', '-',pks,baseline),'baseline')
        self.graph_window.rbankcollection.append_line(self.graph_window.new_line(1,'k', '--',pks,r_bank),"rbank")
        self.graph_window.lbankcollection.append_line(self.graph_window.new_line(1,'k', ':',pks,l_bank),"lbank")
        self.graph_window.vaultcollection.append_line(self.graph_window.new_line(1,'k', '-',pks,z_vault_line),"vaultline")
        self.graph_window.groundcollection.append_line(self.graph_window.new_line(1,'#7F7F7F', '-',pks,topo_line),"groundline")

    def __load_from_results(self, line_wkt, tool):
        w16result = W16Result(self.__result_path)
        w16resultcollection = []

        for scn in self.list_secondary_scenarios :
            if scn[2] is not None :
                result = W16Result(scn[2])
                w16resultcollection.append([scn[1],result])

        res_geom = w16result.get_geom(tool.name)
        if tool.table == 'reach':
            pk0, = self.__project.execute("""
                    select pk0_km
                    from {model}.reach
                    where id={reach};
                    """.format(model=tool.schema, reach=tool.id)).fetchone()
        elif tool.table == 'branch':
            pk0, = self.__project.execute("""
                    select pk0_km
                    from {model}.branch
                    where id={branch};
                    """.format(model=tool.schema, branch=tool.id)).fetchone()

        pk_offset = 0 if len(self.__pks_results)==0 else self.__pks_results[-1]

        pks = numpy.array([r[0] - pk0 + pk_offset for r in res_geom])

        self.__values = self.result_at_time(0)

        self.__values_secondary_list = self.result_at_time_collection(0)

        self.graph_window.watercollection.append_line(self.graph_window.new_line(1,'royalblue', '-',pks,self.__values[:,0][len(self.__pks_results):(len(self.__pks_results)+len(pks))]),"waterline")

        compteur_couleur = 0
        for scn_values in self.__values_secondary_list:
            vars(self.graph_window)['watercollection_'+scn_values[1]].append_line(self.graph_window.new_line(1,_color[compteur_couleur], ':',pks,scn_values[2][:,0][len(self.__pks_results):(len(self.__pks_results)+len(pks))]),"waterline_"+scn_values[1])
            compteur_couleur += 1

        self.__pks_results = numpy.append(self.__pks_results, pks)
        self.__overflow = numpy.append(self.__overflow, numpy.array([r[2] for r in res_geom])) # zv/zd
        self.__waterline_min = self.result_at_time(w16result.nstep + 1)
        self.__waterline_max = self.result_at_time(w16result.nstep + 2)

        for scn_result in w16resultcollection:
            vars(self)['__waterline_min_'+scn_result[0]] = self.result_at_time(scn_result[1].nstep +1)
            vars(self)['__waterline_max_'+scn_result[0]] = self.result_at_time(scn_result[1].nstep +2)

        times = w16result.get_step_times()
        self.__time_control.setTimes(["%.2f"%(d) for d in times])
        self.__highlighters.append(Highlighter(self.__iface.mapCanvas(), line_wkt, pk_offset))

    def make_style_links_labels(self):
        liststyle = ['. #009e00','. #ff7f00','X black','X red','. #e31a1c','| black','o black']
        for i in range (len(liststyle)):
            indices = [j for j, mrk in enumerate(self.__labels_marker) if mrk == liststyle[i]]
            labels_z = [self.__labelshover_point_Z[k] for k in indices]
            labels_pk = [self.__labelshover_point_pk[l] for l in indices]
            self.graph_window.label_points_collection.append_line(self.graph_window.new_line(1,liststyle[i][2:],':',labels_pk,labels_z,marker=liststyle[i][0],pointstyle=True), None)

class GraphWindow(QWidget):
    "Graph window Widget"
    class NavigationToolbar(NavigationToolbar2QT):
        # only display the buttons we need
        toolitems = [t for t in NavigationToolbar2QT.toolitems if
                     t[0] in ('Home', 'Back', 'Forward', 'Pan', 'Zoom', 'Subplots')]

    pk_range_changed = pyqtSignal(float, float)

    class Labels(object):
        def __init__(self, canvas, axes, symbol=None):
            self.__data = None
            self.__canvas = canvas
            self.__axes = axes
            self.__labels = []

        def __update(self):
            pass

        def hide_or_show(self,xlim,ylim):
            for l in self.__labels:
                if xlim[0]>l.get_position()[0] or xlim[1]<l.get_position()[0] or ylim[0]>l.get_position()[1] or ylim[1]<l.get_position()[1]:
                    l.set_visible(False)
                else:
                    l.set_visible(True)

        def reset_labels(self):
            for l in self.__labels:
                l.remove()
            self.__labels = []

        def set_data(self, x_data, z_data, labels):
            for l in self.__labels:
                l.remove()
            self.__labels = []
            for i in range(len(x_data)):
                self.__labels.append(
                        self.__axes.text(x_data[i], z_data[i], "--"+labels[i],
                            fontproperties=FontProperties(size=10), rotation=-90,
                            verticalalignment='top', horizontalalignment='center'))
            self.__canvas.draw()

    class HoverLabels(object):
        def __init__(self, canvas, axes, symbol=None):
            self.__data = None
            self.__canvas = canvas
            self.__axes = axes
            self.__labels = []
            self.annot = self.__axes.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"))
            self.annot.set_visible(False)

        def reset_labels(self):
            self.__labels = []

        def set_data(self,labels):
            self.__labels = labels

        def set_annot(self,ind,line):
            x,y = line.get_data()
            self.annot.xy = (x[ind["ind"][0]], y[ind["ind"][0]])
            text = "{}, {}".format([self.__labels[n][1] for n in ind["ind"]][0], [self.__labels[n][2] for n in ind["ind"]][0])
            self.annot.set_text(text)
            self.annot.get_bbox_patch().set_alpha(0.4)

    class LineCollection (object):
        "class to regroup lines for a given data and allow a better control of them"
        def __init__(self, collection ):
            self.__LineCollection = collection

        def reset(self):
            for l in self.__LineCollection:
                l.reset()
            self.__LineCollection = []

        def append_line(self,line,legend):
            if len(self.__LineCollection) == 0:
                line.set_legend(legend)
            line.set_line_visible(False)
            self.__LineCollection.append(line)

        def show_collection(self, bool, legend = None):
            for l in self.__LineCollection:
                l.set_line_visible(bool)
            onelegend = False
            if bool:
                for l in self.__LineCollection:
                    if l.get_line() is not None and onelegend == False:
                        l.set_legend(legend)
                        onelegend = True
            else:
                for l in self.__LineCollection:
                    if l.get_line() is not None and onelegend == False:
                        l.set_legend('_nolegend_')
                        onelegend = True

        def get_collection_format(self):
            listformat = [0]
            for l in self.__LineCollection:
                listformat.append(l.get_len_line())
            return(len(self.__LineCollection),listformat)

        def get_collection(self):
            return(self.__LineCollection)

        def fill_between_collection(self,baselinecollection,color,maxmode=False,max_secondary_collection=None):
            listfillbetween =[]
            xdatabaseline=None
            baseline_collection = baselinecollection.get_collection()
            if max_secondary_collection is not None:
                secondary_collection = max_secondary_collection.get_collection()
            if len(self.__LineCollection) == len(baseline_collection):
                for i in range (len(self.__LineCollection)):
                    if maxmode == True:
                        if secondary_collection[i].get_xline() is not None:
                            xdata = self.__LineCollection[i].get_xline()
                            xdatabaseline = baseline_collection[i].get_xline()
                            zvaultline = secondary_collection[i].get_yline()
                            zwater = self.__LineCollection[i].get_yline()
                            zbaseline = baseline_collection[i].get_yline()
                            zvaultlineinterp = numpy.interp(xdata,xdatabaseline,zvaultline)
                            for l in range(len(zwater)):
                                if zwater[l] > zvaultlineinterp[l]:
                                    zwater[l] = zvaultlineinterp[l]
                            xdatabaseline,zbaseline,xdata,zwater = solve_non_continu(xdatabaseline,zbaseline,xdata,zwater)
                        else :
                            xdata = self.__LineCollection[i].get_xline()
                            xdatabaseline = baseline_collection[i].get_xline()
                            zwater = self.__LineCollection[i].get_yline()
                            zbaseline = baseline_collection[i].get_yline()
                            xdatabaseline,zbaseline,xdata,zwater = solve_non_continu(xdatabaseline,zbaseline,xdata,zwater)
                    else :
                        xdata = self.__LineCollection[i].get_xline()
                        xdatabaseline = baseline_collection[i].get_xline()
                        zwater = self.__LineCollection[i].get_yline()
                        zbaseline = baseline_collection[i].get_yline()
                        xdatabaseline,zbaseline,xdata,zwater = solve_non_continu(xdatabaseline,zbaseline,xdata,zwater)

                    if xdatabaseline is not None:
                        if len(xdatabaseline) == len(zbaseline):

                            listfillbetween.append(self.__LineCollection[i].get_axes().fill_between(xdata,zbaseline,zwater,where = zwater>zbaseline, interpolate=True, facecolor = color))

                    self.__LineCollection[i].get_canvas().draw()
            return listfillbetween

    class Line(object):
        "utility class to encapsulate data and show/hide"
        def __init__(self, canvas, axes, color, style,legend=None,pointstyle=False,marker=None):
            self.__xdata = None
            self.__ydata = None
            self.__canvas = canvas
            self.__axes = axes
            self.__line = None
            self.__color = color
            self.__style= style
            self.__legend = legend
            self.__pointstyle = pointstyle
            self.__marker = marker

        def __update(self):
            "update the line graph (show/hide)"
            self.__remove()
            if self.__pointstyle == False and self.__xdata is not None and self.__ydata is not None :
                self.__line, = self.__axes.plot(
                    self.__xdata,
                    self.__ydata,
                    linestyle=self.__style,
                    marker="",
                    color=self.__color,
                    label=self.__legend)
            elif self.__pointstyle == True and self.__xdata is not None and self.__ydata is not None:
                    self.__line, = self.__axes.plot(
                    self.__xdata,
                    self.__ydata,
                    linestyle='None',
                    marker=self.__marker,
                    markerfacecolor='None',
                    markeredgecolor=self.__color,
                    label=self.__legend)

            self.__axes.legend()
            self.__axes.get_xaxis().get_major_formatter().set_useOffset(False)
            self.__axes.get_yaxis().get_major_formatter().set_useOffset(False)
            self.__canvas.draw()

        def __remove(self):
            if self.__line:
                self.__line.remove()
                self.__line = None

        def reset(self):
            self.set_legend(None)
            self.__remove()
            self.__xdata = None
            self.__ydata = None

        def set_data(self, x_data, y_data):
            if len(x_data)==len(y_data):
                "set/reset line data"
                self.__xdata = x_data
                self.__ydata = y_data
                self.__update()

        def get_canvas(self):
            return self.__canvas

        def get_line(self):
            return self.__line

        def get_color(self):
            return self.__color

        def get_axes(self):
            return self.__axes

        def get_xline(self):
            return self.__xdata

        def get_yline(self):
            return self.__ydata

        def get_len_line(self):
            if self.__line is not None:
                return len(self.__xdata)

        def set_line_visible(self,bool):
            if self.__line is not None:
                self.__line.set_visible(bool)

        def set_legend(self,legend):
            self.__legend = legend
            if self.__line is not None:
                self.__line.set_label(self.__legend)

    def __init__(self, project, parent=None):
        QWidget.__init__(self, parent)

        self.__project = project
        self.__dpi = 75
        self.has_result = False
        self.__fig = Figure(dpi=self.__dpi)
        self.__axes1 = self.__fig.add_subplot(311)
        self.__axes2 = self.__fig.add_subplot(312)
        self.__axes3 = self.__fig.add_subplot(313)
        self.__canvas = FigureCanvas(self.__fig)

        self.vLayout = QVBoxLayout()
        self.vLayout.setContentsMargins(QMargins(0,0,0,0))

        self.vLayout.addWidget(self.__canvas)
        self.setLayout(self.vLayout)
        parent.setLayout(self.vLayout)

        self.__canvas.mpl_connect('scroll_event', self.__on_scroll)
        self.__canvas.mpl_connect('button_press_event', self.__on_press)
        self.__canvas.mpl_connect('motion_notify_event', self.__on_move)
        self.__canvas.mpl_connect('motion_notify_event', self.__hover)

        self.listfillbetween = []
        self.listgroundlevel = []

        self.__start_x = None
        self.__start_xlim = None
        self.__start_ylim = None
        self.__xratio = None

        self.__start_x2 = None
        self.__start_xlim2 = None
        self.__start_ylim2 = None
        self.__xratio2 = None

        self.__start_x3 = None
        self.__start_xlim3 = None
        self.__start_ylim3 = None
        self.__xratio3 = None

        self.ymaxQ = 0
        self.xmaxQ = 0
        self.ymaxV = 0
        self.xmaxV = 0

        self.waterline_min = GraphWindow.Line(self.__canvas, self.__axes1, 'lightgreen', '-',"Min waterline")
        self.waterline_max = GraphWindow.Line(self.__canvas, self.__axes1, 'darkblue', '-',"Max waterline")
        self.overflow = GraphWindow.Line(self.__canvas, self.__axes1, 'red', '-',"Overflow")
        self.second_line = GraphWindow.Line(self.__canvas, self.__axes2, 'm', '-')
        self.label_points = GraphWindow.Line(self.__canvas, self.__axes1, 'purple', None, pointstyle = True, marker ="o")
        self.labels = GraphWindow.Labels(self.__canvas, self.__axes1)
        self.labels_link = GraphWindow.HoverLabels(self.__canvas, self.__axes1)
        self.speedline = GraphWindow.Line(self.__canvas, self.__axes3, 'green', '-',)

        self.label_points_collection = GraphWindow.LineCollection([])
        self.structure_collection = []

        self.plot_graph()

    def reload_legend (self):
        self.__axes1.legend()
        self.__axes2.legend()
        self.__axes3.legend()

    def init_lines_for_scn(self,scn):

        if hasattr(self, 'waterline_min_' + scn[1]):
            vars(self)['waterline_min_'+scn[1]].reset()
            vars(self)['waterline_max_'+scn[1]].reset()
            vars(self)['Qline_'+scn[1]].reset()
            vars(self)['speedline_'+scn[1]].reset()
        else:
            vars(self)['waterline_min_'+scn[1]] = GraphWindow.Line(self.__canvas, self.__axes1, _color[randint(0,len(_color)-1)], ':',"Min waterline "+ scn[1])
            vars(self)['waterline_max_'+scn[1]] = GraphWindow.Line(self.__canvas, self.__axes1, _color[randint(0,len(_color)-1)], ':',"Max waterline "+ scn[1])
            vars(self)['Qline_'+scn[1]] = GraphWindow.Line(self.__canvas, self.__axes2, _color[randint(0,len(_color)-1)], ':',"Q "+ scn[1])
            vars(self)['speedline_'+scn[1]] = GraphWindow.Line(self.__canvas, self.__axes3, _color[randint(0,len(_color)-1)], ':',"Speed "+ scn[1])

    def new_line(self,axes,color,linestyle,xdata,ydata,pointstyle=False,marker=None,linewidth=None,patheffect=None):
        if axes == 1:
            axetemp = self.__axes1
        elif axes == 2:
            axetemp = self.__axes2
        elif axes == 3:
            axetemp = self.__axes3
        Line = GraphWindow.Line(self.__canvas,axetemp,color,linestyle,pointstyle=pointstyle,marker=marker)
        Line.set_data(xdata,ydata)

        if linewidth is not None :
            Line.get_line().set_linewidth(linewidth)
        if patheffect is not None :
            Line.get_line().set_path_effects([path_effects.Stroke(linewidth=6, foreground=patheffect),path_effects.Normal()])

        return Line

    def plot_graph(self,result=False,Qflow=False,speedline=False):

        gs=GridSpec(3,1, height_ratios=[(1+999*int(bool)) for bool in [True, Qflow and result, speedline and result]])

        self.__axes1.set_position(gs[0,0].get_position(self.__fig))
        self.__axes2.set_position(gs[1,0].get_position(self.__fig))
        self.__axes3.set_position(gs[2,0].get_position(self.__fig))

        self.__axes2.set_visible(False)
        self.__axes3.set_visible(False)

        if Qflow and result :
            self.__axes2.set_visible(True)
        if speedline and result:
            self.__axes3.set_visible(True)

        self.__canvas.draw()

    def __responsive_label(self):
        xlim = self.__axes1.get_xlim()
        ylim = self.__axes1.get_ylim()
        self.labels.hide_or_show(xlim,ylim)

    def __emit_range_change(self):
            x_min_graph = 0
            if self.__axes1.get_xlim()[0] > 0 :
                x_min_graph = self.__axes1.get_xlim()[0]
            x_max_graph = self.__x_max_graph
            if self.__axes1.get_xlim()[1] < x_max_graph :
                x_max_graph = self.__axes1.get_xlim()[1]
            self.pk_range_changed.emit(
                    x_min_graph,
                    x_max_graph)

    def __hover(self, event):
        # there is a pb with inaxes if 2 axes overlap
        line = self.label_points.get_line()
        vis = self.labels_link.annot.get_visible()
        if event.inaxes == self.__axes1 and line is not None:
            cont, ind = line.contains(event)
            if cont:
                self.labels_link.set_annot(ind,line)
                self.labels_link.annot.set_visible(True)
                self.__canvas.draw()
            else:
                if vis:
                    self.labels_link.annot.set_visible(False)
                    self.__canvas.draw()

    def __on_press(self, event):
        "initialise data for drag"

        if event.button == 1:
            self.__start_x = event.x
            self.__start_y = event.y
            self.__start_xlim = numpy.array(self.__axes1.get_xlim())
            self.__start_ylim = numpy.array(self.__axes1.get_ylim())
            self.__xratio = (self.__start_xlim[1] -  self.__start_xlim[0])\
                         / self.__canvas.width()
            self.__yratio = (self.__start_ylim[1] -  self.__start_ylim[0])\
                         / self.__canvas.height()
            "second graphique"

        if event.button == 1:
            self.__start_x2 = event.x
            self.__start_y2 = event.y
            self.__start_xlim2 = numpy.array(self.__axes2.get_xlim())
            self.__start_ylim2 = numpy.array(self.__axes2.get_ylim())
            self.__xratio2 = (self.__start_xlim2[1] -  self.__start_xlim2[0])\
                         / self.__canvas.width()
            self.__yratio2 = (self.__start_ylim2[1] -  self.__start_ylim2[0])\
                         / self.__canvas.height()

            "troisieme graphique"

        if event.button == 1:
            self.__start_x3 = event.x
            self.__start_y3 = event.y
            self.__start_xlim3 = numpy.array(self.__axes3.get_xlim())
            self.__start_ylim3 = numpy.array(self.__axes3.get_ylim())
            self.__xratio3 = (self.__start_xlim3[1] -  self.__start_xlim3[0])\
                         / self.__canvas.width()
            self.__yratio3 = (self.__start_ylim3[1] -  self.__start_ylim3[0])\
                         / self.__canvas.height()

    def __on_move(self, event):
        "move plot on drag"
        if event.button == 1:
            if event.x != None:
                self.__axes1.set_xlim(self.__start_xlim + (self.__start_x - event.x)*self.__xratio)
            if event.y != None:
                self.__axes1.set_ylim(self.__start_ylim + (self.__start_y - event.y)*self.__yratio)

        "move second plot on drag"
        if event.button == 1:
            if event.x != None:
                self.__axes2.set_xlim(self.__start_xlim2 + (self.__start_x2 - event.x)*self.__xratio2)
            if event.y != None:
                self.__axes2.set_ylim(self.__start_ylim2 + (self.__start_y2 - event.y)*self.__yratio2)

        "move third plot on drag"
        if event.button == 1:
            if event.x != None:
                self.__axes3.set_xlim(self.__start_xlim3 + (self.__start_x3 - event.x)*self.__xratio3)
            if event.y != None:
                self.__axes3.set_ylim(self.__start_ylim3 + (self.__start_y3 - event.y)*self.__yratio3)

            self.__canvas.draw()
            self.__emit_range_change()

            self.__responsive_label()

    def __on_scroll(self, event):
        "zoom plot on scroll"
        self.__scrolling(event,self.__axes1)
        self.__scrolling(event,self.__axes2)
        self.__scrolling(event,self.__axes3)

        self.__canvas.draw()
        self.__emit_range_change()
        self.__responsive_label()

    def __scrolling(self,event,axes):
        if event.xdata is None:
                y_min, y_max = axes.get_ylim()
                new_height = (y_max - y_min)*(1.1 if event.step < 0 else .9)
                axes.set_ylim(
                        [(y_max+y_min)*.5 - new_height*.5,
                         (y_max+y_min)*.5 + new_height*.5])
        else:
            assert event.step
            x_min, x_max = axes.get_xlim()
            new_width = (x_max - x_min)*(1.1 if event.step < 0 else .9)
            relx = (x_max - event.xdata) / (x_max - x_min)
            axes.set_xlim(
                    [event.xdata - new_width*(1-relx),
                     event.xdata + new_width*(relx)])

    def set_extent(self):
        self.__axes1.get_xaxis().set_visible(True)
        self.__axes1.get_yaxis().set_visible(True)
        self.__axes1.grid(True)
        self.__axes1.set_ylabel(tr('Altitude(Z)(m)'))
        self.__axes1.set_xlabel(tr('pk(km)'))
        self.__axes1.relim()
        self.__axes1.autoscale(enable=True, axis='both', tight=True)
        self.__x_max_graph = self.__axes1.get_xlim()[1]

        self.__axes2.get_xaxis().set_visible(True)
        self.__axes2.get_yaxis().set_visible(True)
        self.__axes2.grid(True)
        self.__axes2.set_ylabel(tr('Debit(Q)(m3/s)'))
        self.__axes2.set_xlabel(tr('pk(km)'))
        self.__axes2.autoscale(enable=True, axis='both', tight=True)
        self.__axes2.set_ylim(0,self.ymaxQ)
        self.__axes2.set_xlim(0,self.xmaxQ)

        self.__axes3.get_xaxis().set_visible(True)
        self.__axes3.get_yaxis().set_visible(True)
        self.__axes3.grid(True)
        self.__axes3.set_ylabel(tr('Vitesse(V)(m/s)'))
        self.__axes3.set_xlabel(tr('pk(km)'))
        self.__axes3.relim()
        self.__axes3.autoscale(enable=True, axis='both', tight=True)
        #self.__axes3.set_ylim(0,self.ymaxV)
        #self.__axes3.set_xlim(0,self.xmaxV)

        self.__canvas.draw()
        self.__emit_range_change()

    def export_image(self):
        buf = io.BytesIO()
        self.__fig.savefig(buf)
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard )
        cb.setImage(QImage.fromData(buf.getvalue()), mode=cb.Clipboard)
        buf.close()
