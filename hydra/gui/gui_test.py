# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run gui tests

USAGE
    python -m hydra.gui.test [-h]

OPTIONS
    -h
        print this help
"""


import subprocess
import os
import re
from subprocess import Popen, PIPE
from tempfile import gettempdir
from time import time
from multiprocessing.pool import ThreadPool
from ..database.database import TestProject, project_exists
import getopt
import sys

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hvj:e:",
            ["help", "verbose"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

start = time()

def list_tests():
    "return module names for tests"
    tests = []
    hydra_gui_dir = os.path.abspath(os.path.dirname(__file__))
    top_dir = os.path.dirname(hydra_gui_dir)
    for root, dirs, files in os.walk(hydra_gui_dir):
        for file_ in files:
            if re.match(r".*_guitest.py$", file_):
                # remove the trailing '.py' (3 characters)
                # replace \ or / by dots
                test = '.'.join(
                            os.path.abspath(
                                os.path.join(root, file_)
                            ).replace(hydra_gui_dir, "hydra.gui").split(os.sep))[:-3]
                tests.append(test)
    return tests

def run(test):
    start = time()
    out, err = subprocess.Popen(["python", "-m", test],
            stderr=PIPE,
            stdout=PIPE).communicate()
    if len(err):
        return 1, str(err), out
    return  0, " ran in %.2f sec"%(time() - start), out


if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")


tests = list_tests()
project_name = "gui_test"
test_project = TestProject(project_name, with_model=True)

for i, test in enumerate(tests):
    print "% 3d/%d %s"%(i+1, len(tests), test),
    rt, msg, out = run(test)
    if "-v" in optlist or "--verbose" in optlist:
        print out
    print msg

    if rt != 0:
        raise Exception(msg)

print "gui tests run in %d sec"%(int(time() - start))
exit(0)
