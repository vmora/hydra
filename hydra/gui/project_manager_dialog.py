# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import psycopg2
import tempfile
import json
import re
import datetime
from urlparse import urlparse
from PyQt4 import uic
from PyQt4.QtCore import QCoreApplication, Qt, QDate
from PyQt4.QtGui import QApplication, QDialog, QFileDialog, QMessageBox, QTableWidgetItem, QAbstractItemView, QTextEdit, QTreeWidgetItem, QColor, QPushButton
import new_project_dialog as npdialog
from hydra.database import database as dbhydra
from hydra.project import Project
from hydra.utility.empty_ui_manager import EmptyUIManager
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.string import isint
from hydra.versions.update import update_project as updt_project
from hydra.versions.dump_restore import dump_project, restore_project, create_db, copy_project, srid_from_dump, version_from_dump


_desktop_dir = os.path.join(os.path.expanduser("~"), "Desktop")

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

def unique_project_name(project_name):
    projects = dbhydra.get_projects_list()
    matches = [prj
                for prj in projects
                if project_name in prj[:len(project_name)]]
    index = [int(name.replace(project_name+'_',''))
                for name in matches
                if isint(name.replace(project_name+'_',''))]
    return str(project_name)+'_'+str(next(i for i, e in enumerate(sorted(index) + [ None ], 1) if i != e))

class ProjectManagerDialog(QDialog):
    def __init__(self, log_manager, ui_manager, qgis_current_project=None, parent=None):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "project_manager_dialog.ui"), self)
        self.parent = parent

        self.tempdir = tempfile.gettempdir()

        self.__log = log_manager
        self.__ui_manager = ui_manager
        self.__empty_ui_manager = EmptyUIManager()
        self.__qgis_current_project = qgis_current_project

        self.__selected_project = None
        self.new_current_project = None
        self.qgis_current_project_deleted = False

        self.table_project.sortItems(0)
        self.btnNewProject.clicked.connect(self.__new_project)
        self.btnOpenProject.clicked.connect(self.__open_project)
        self.btnDeleteProject.clicked.connect(self.__delete_project)
        self.btnDuplicateProject.clicked.connect(self.__copy_project)
        self.btnExport.clicked.connect(self.__export_project)
        self.btnImport.clicked.connect(self.__import_project)
        self.btnUpdate.clicked.connect(self.__update_project)
        self.btnClose.clicked.connect(self.close)

        self.ui_version = dbhydra.data_version()
        self.label_version.setText(self.ui_version+' (Installed versions: {})'.format(', '.join(dbhydra.get_available_hydra_versions())))

        self.data_table=[]

        self.__refresh_list_projects()

    def __project_selected(self):
        if self.table_project.currentRow()>-1:
            index = int(self.table_project.item(self.table_project.currentRow(), 5).text())-1
            self.__select_project(self.data_table[index][0], self.data_table[index][1], self.data_table[index][2])
        else:
            self.__selected_project = None
            self.__selected_version = None
            self.__selected_srid = None
        self.__refresh_ui()

    def __new_project(self):
        new_project_dialog = npdialog.NewProjectDialog()
        if new_project_dialog.exec_():
            project_name, srid, working_dir = new_project_dialog.get_result()
            if not project_name=='':
                self.__create_project(project_name, srid, working_dir)

    def __create_project(self, project_name, srid, working_dir):
        Project.create_new_project(working_dir, project_name, srid, self.__log, self.__empty_ui_manager)
        self.__refresh_list_projects()

    def __select_project(self, project_name, srid, version):
        self.__selected_project = project_name
        self.__selected_version = version
        self.__selected_srid = srid

    def __open_project(self):
        if (self.__selected_project != None):
            project_name = self.__selected_project
            workspace = dbhydra.get_project_metadata(project_name)[2]
            project_dir = os.path.join(workspace, project_name)

            if not os.path.isdir(project_dir):
                msgBox = QMessageBox()
                msgBox.setWindowTitle(tr('Project directory not found'))
                msgBox.setIcon(QMessageBox.Question)
                msgBox.setText(tr('Directory "{p}" not found in {ws}. Select another workspace (directory containing the project folder) or create a new folder for project {p} in {ws}?').format(p=project_name, ws=workspace))
                msgBox.addButton(QPushButton('Select workspace'), QMessageBox.YesRole)
                msgBox.addButton(QPushButton('Create folder'), QMessageBox.NoRole)
                ret = msgBox.exec_()
                if ret == 0: #YesRole
                    dirName = QFileDialog.getExistingDirectory(None, tr('Select directory'), workspace)
                    if dirName:
                        dbhydra.change_project_workspace(project_name, dirName)

            self.new_current_project = Project.load_project(project_name, self.__log, self.__ui_manager)
            self.new_current_project.open_in_ui()
            self.close()

    def __delete_project(self):
        if (self.__selected_project!=None):
            confirm = QMessageBox(QMessageBox.Warning, tr('Delete project'), tr('This will delete project {}. Proceed?').format(self.__selected_project), QMessageBox.Ok | QMessageBox.Cancel).exec_()
            if confirm == QMessageBox.Ok:
                self.__disable_ui()
                dbhydra.remove_project(self.__selected_project)
                self.__log.notice(str(tr("Project {0} deleted.")).format(self.__selected_project))
                if (self.__selected_project == self.__qgis_current_project):
                    self.__ui_manager.new_project()
                    self.qgis_current_project_deleted = True
                self.__refresh_list_projects()

    def __update_project(self):
        if (self.__selected_project != None):
            project = Project(self.__log, self.__empty_ui_manager, self.__selected_project)
            res = updt_project(project)
            if res == 0:
                QMessageBox.information(None, tr('Successful update'), tr("""Project {} updated to version {}.""").format(project.name, project.get_version()), QMessageBox.Ok)
            elif res == 1:
                QMessageBox.warning(None, tr('Error during update'), tr("""An error occured while updating project {}.\nCheck hydra.log file for more informations.""").format(project.name), QMessageBox.Ok)
            self.__refresh_list_projects()

    def __refresh_list_projects(self):
        self.__disable_ui()
        self.__selected_project = None

        self.data_table=[]

        self.table_project.setRowCount(0)
        self.table_project.setSortingEnabled(False)
        self.table_project.setHorizontalHeaderLabels(['Project name', 'SRID', 'Data version', 'Creation date', 'Workspace', 'Id'])

        projects = sorted(dbhydra.get_projects_list())

        for project_name in projects:
            row_position = self.table_project.rowCount()
            self.table_project.insertRow(row_position)

            srid, version, workspace, date = dbhydra.get_project_metadata(project_name)
            self.data_table.append([project_name, srid, version, date, workspace])
            # Column 0: project name
            self.table_project.setItem(row_position, 0, QTableWidgetItem(project_name))
            # Column 1: project SRID
            self.table_project.setItem(row_position, 1, QTableWidgetItem(str(srid)))
            if srid is None or srid == 0:
                self.table_project.item(row_position, 1).setForeground(QColor(255,0,0))
                self.table_project.item(row_position, 1).setToolTip(tr('Bad SRID'))
            # Column 2: project version
            self.table_project.setItem(row_position, 2, QTableWidgetItem(version))
            if version != self.ui_version:
                self.table_project.item(row_position, 2).setForeground(QColor(255,0,0))
                self.table_project.item(row_position, 2).setToolTip(tr('Not current version'))
            # Column 3: project creation date
            if isinstance(date, datetime.date):
                date_item = QDate(date.year, date.month, date.day)
                creation_date = QTableWidgetItem()
                creation_date.setData(0, date_item)
            else:
                creation_date = QTableWidgetItem(tr('No creation date found'))
                creation_date.setForeground(QColor(255,0,0))
            self.table_project.setItem(row_position, 3, creation_date)
            # Column 4: project workspace
            self.table_project.setItem(row_position, 4, QTableWidgetItem(workspace))
            if not os.path.isdir(os.path.join(workspace, project_name)):
                self.table_project.item(row_position, 4).setForeground(QColor(255,0,0))
                self.table_project.item(row_position, 4).setToolTip(tr('Project folder not found in workspace'))
            # Column 5 (hidden): index
            self.table_project.setItem(row_position, 5, QTableWidgetItem(str(row_position+1)))

        self.table_project.itemSelectionChanged.connect(self.__project_selected)
        self.table_project.setColumnHidden(5, True)
        self.table_project.resizeRowsToContents()
        self.table_project.resizeColumnsToContents()
        self.table_project.horizontalHeader().setStretchLastSection(True)
        self.table_project.setSortingEnabled(True)

        self.__refresh_ui()

    def __refresh_ui(self):
        if self.__selected_project!=None:
            self.__disable_ui()
            self.__enable_ui()

    def __export_project(self):
        if self.__selected_project != None:
            title = tr('Export project')
            filter = tr('SQL file(*.sql)')
            file_name = QFileDialog.getSaveFileName (self, title, _desktop_dir, filter)
            if file_name:
                QApplication.setOverrideCursor(Qt.WaitCursor)
                QApplication.processEvents()
                exit_code, output, error = dump_project(file_name, self.__selected_project)
                QApplication.restoreOverrideCursor()
                if exit_code == 0:
                    QMessageBox.information(self, tr('Export successfull'),
                                tr('Project {p} successfully exported to {f}.').format(p=self.__selected_project, f=file_name), QMessageBox.Ok)
                else:
                    QMessageBox.critical(self, tr('Export error'),
                                tr('Error exporting project {p} to {f}. See hydra.log file.').format(p=self.__selected_project, f=file_name), QMessageBox.Ok)
                    self.__log.notice('Export error: {e}'.format(e=error))

    def __import_project(self):
        title = tr('Import project')
        filter = tr('SQL files(*.sql)')
        file_url = QFileDialog.getOpenFileName(self, title, _desktop_dir, filter)
        if file_url:
            file_version = version_from_dump(file_url)
            if file_version not in dbhydra.get_available_hydra_versions():
                QMessageBox.critical(self, tr('Import error'),
                        tr('File {f} data version ({fv}) do not match available versions ({av}).').format(f=file_url, fv=file_version, av=', '.join(dbhydra.get_available_hydra_versions())), QMessageBox.Ok)
                return

            new_project_dialog = npdialog.NewProjectDialog(srid=(srid_from_dump(file_url)))
            if new_project_dialog.exec_():
                name, srid, working_dir = new_project_dialog.get_result()
                if not SettingsProperties.local():
                    working_dir = "/$user/.hydra"
                if name != '':
                    QApplication.setOverrideCursor(Qt.WaitCursor)
                    QApplication.processEvents()
                    create_exit_code, create_output, create_error = create_db(name)
                    QApplication.restoreOverrideCursor()
                    if create_exit_code != 0:
                        QMessageBox.critical(self, tr('Import error'),
                                tr('Error importing project {p} from {f}. See hydra.log file.').format(p=name, f=file_url), QMessageBox.Ok)
                        self.__log.notice('CreateDB error: {e}'.format(e=create_error))
                        return

                    restore_exit_code, restore_output, create_error = restore_project(file_url, name)
                    if restore_exit_code != 0:
                        QMessageBox.critical(self, tr('Import error'),
                                tr('Error importing project {p} from {f}. See hydra.log file.').format(p=name, f=file_url), QMessageBox.Ok)
                        self.__log.notice('RestoreDB error: {e}'.format(e=restore_error))
                        return

                    db = SettingsProperties.get_db()
                    pg = SettingsProperties.get_postgre_settings()

                    conn = psycopg2.connect(database=name, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
                    cur = conn.cursor()
                    cur.execute("""alter table hydra.metadata disable trigger hydra_metadata_after_insert_trig;
                                       insert into hydra.metadata(srid, workspace) values ({}, '{}');
                                       alter table hydra.metadata enable trigger hydra_metadata_after_insert_trig;""".format(srid, working_dir))
                    conn.commit()
                    conn.close()

                    QMessageBox.information(self, tr('Import successfull'),
                                tr('Project {p} successfully imported from {f}.').format(p=name, f=file_url), QMessageBox.Ok)
                    self.__refresh_list_projects()

    def __copy_project(self):
        title = tr('Duplicate project')
        if self.__selected_project != None:
            new_name = unique_project_name(self.__selected_project+'_copy')
            copy_project_dialog = npdialog.NewProjectDialog(project_name=new_name, srid=self.__selected_srid)
            if copy_project_dialog.exec_():
                name, srid, workspace = copy_project_dialog.get_result()
                if not SettingsProperties.local():
                    workspace = "/$user/.hydra"
                if name != '':
                    QApplication.setOverrideCursor(Qt.WaitCursor)
                    QApplication.processEvents()
                    copy_exit_code, copy_output, copy_error = copy_project(self.__selected_project, name)
                    QApplication.restoreOverrideCursor()
                    if copy_exit_code != 0:
                        QMessageBox.critical(self, tr('Import error'),
                                tr('Error while duplicating project {}. See hydra.log file.').format(self.__selected_project), QMessageBox.Ok)
                        self.__log.notice('Copy project error: {e}'.format(e=copy_error))
                        return

                    db = SettingsProperties.get_db()
                    pg = SettingsProperties.get_postgre_settings()

                    conn = psycopg2.connect(database=name, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
                    cur = conn.cursor()
                    cur.execute("""alter table hydra.metadata disable trigger hydra_metadata_after_insert_trig;
                                       insert into hydra.metadata(srid, workspace) values ({}, '{}');
                                       alter table hydra.metadata enable trigger hydra_metadata_after_insert_trig;""".format(srid, workspace))
                    conn.commit()
                    conn.close()

                    QMessageBox.information(self, tr('Import successfull'),
                                tr('Project {} successfully copied into project {}.').format(self.__selected_project, name), QMessageBox.Ok)
                    self.__refresh_list_projects()

    def __browse(self):
        title = tr('Select workspace path:')
        dir_name = QFileDialog.getExistingDirectory(self, title, _desktop_dir)
        if dir_name:
            self.workspace.setText(dir_name)

    def __enable_ui(self):
        self.btnDeleteProject.setEnabled(True)
        self.btnExport.setEnabled(True)
        if self.__selected_version == self.ui_version:
            self.btnOpenProject.setEnabled(True)
        else:
            self.btnUpdate.setEnabled(True)

    def __disable_ui(self):
        self.btnOpenProject.setEnabled(False)
        self.btnDeleteProject.setEnabled(False)
        self.btnExport.setEnabled(False)
        self.btnUpdate.setEnabled(False)