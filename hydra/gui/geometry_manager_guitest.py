# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.geometry_manager_guitest [-dhs] [-g project_name model_name]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name
        run in gui mode

   -s  --solo
        run test in solo mode (create database)
"""

from __future__ import absolute_import # important to read the doc !
import sip
for cls in [u'QDate', u'QDateTime', u'QString', u'QTextStream', u'QTime', u'QUrl', u'QVariant']:
    sip.setapi(cls, 2)

from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt
from hydra.project import Project
from PyQt4.QtGui import QApplication
from PyQt4.QtCore import QCoreApplication, QTranslator
from hydra.gui.geometry_manager import GeometryManager
from ..database.database import TestProject, remove_project, project_exists

def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = GeometryManager(obj_project)
    test_dialog.exec_()


def test():
    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)
    model = obj_project.get_current_model()

    test_dialog = GeometryManager(obj_project)

    test_dialog.new_cp_geom()
    test_dialog.new_op_geom()
    test_dialog.new_vcs_geom()

    test_dialog.delete_cp_geom()
    test_dialog.delete_op_geom()
    test_dialog.delete_vcs_geom()



try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name)
    test()
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    project_name = normalized_name(project_name)
    model_name = normalized_model_name(model_name)
    gui_mode(project_name)
    exit(0)

test()

print "ok"
exit(0)


