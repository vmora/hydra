# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
Settings is a dictionnary in hydra.config

First key : external ressource
Second key : specific value

{'postgre': {'path' : postgrepath}}

"""

import json
import os
import re
from PyQt4 import uic
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QDialog, QFileDialog, QTableWidgetItem, QCheckBox, QPushButton, QColor, QColorDialog, QPalette, QWidget, QVBoxLayout
from base_dialog import tr
from hydra.utility.settings_properties import SettingsProperties
from hydra.gui.widgets.trigger_control_widget import TriggerControlWidget


_desktop_dir = os.path.join(os.path.expanduser("~"), "Desktop")


class HydraSettings(QDialog):
    def __init__(self, parent=None, project=None):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "settings.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.project = project

        self.postgre_btn.clicked.connect(lambda: self.__browse('path_postgre'))
        self.python_btn.clicked.connect(lambda: self.__browse('path_python'))

        self.color_button_0.clicked.connect(lambda: self.__select_color(0))
        self.color_button_1.clicked.connect(lambda: self.__select_color(1))
        self.color_button_2.clicked.connect(lambda: self.__select_color(2))
        self.color_button_3.clicked.connect(lambda: self.__select_color(3))
        self.color_button_4.clicked.connect(lambda: self.__select_color(4))

        self.server_host.setText(str(SettingsProperties.get_server_db('host')))
        self.server_port.setText(str(SettingsProperties.get_server_db('port')))
        self.local_use.setChecked(SettingsProperties.local())
        self.local_port.setText(str(SettingsProperties.get_local_db('port')))
        self.path_postgre.setText(str(SettingsProperties.get_path('postgre')))
        self.path_python.setText(str(SettingsProperties.get_path('python')))
        self.local_use.stateChanged.connect(self.__local_use_check)
        self.__local_use_check()

        self.snap.setText(str(SettingsProperties.get_snap()))

        if self.project is not None and len(self.project.get_models())>0 and self.__trigger_control_columns_exists(self.project):
            self.__refresh_model_trigger_table()
        else:
            self.settings_tab.setTabEnabled(2, False)

        self.timestep.setText(str(SettingsProperties.get_timestep()))
        self.scale.setText(str(SettingsProperties.get_velocity_style('scale')))
        self.__set_velocity_classes(SettingsProperties.get_velocity_style('classes'))
        self.__set_velocity_color(SettingsProperties.get_velocity_style('colors'))

    @staticmethod
    def __trigger_control_columns_exists(project):
        exist = True
        for model in project.get_models():
            for column in ['trigger_coverage', 'trigger_branch', 'trigger_street_link']:
                if not project.execute("""select exists(select 1 from information_schema.columns where table_schema='{}' and table_name='metadata' and column_name='{}');""".format(model, column)).fetchone()[0]:
                    exist = False
        return bool(exist)

    def __browse(self, external):
        title = tr('Select {} path:').format(external)
        dir_name = QFileDialog.getExistingDirectory(None, title, _desktop_dir)
        if dir_name:
            getattr(self, external).setText(dir_name)

    def __local_use_check(self):
        if self.local_use.isChecked() == True:
            self.server_host.setEnabled(False)
            self.server_port.setEnabled(False)

            self.local_port.setEnabled(True)
            self.path_postgre.setEnabled(True)
            self.postgre_btn.setEnabled(True)
            self.path_python.setEnabled(True)
            self.python_btn.setEnabled(True)
        else:
            self.server_host.setEnabled(True)
            self.server_port.setEnabled(True)

            self.local_port.setEnabled(False)
            self.path_postgre.setEnabled(False)
            self.postgre_btn.setEnabled(False)
            self.path_python.setEnabled(False)
            self.python_btn.setEnabled(False)

    def __refresh_model_trigger_table(self):
        self.trigger_control_panel.setRowCount(0)
        self.trigger_control_panel.setSortingEnabled(False)
        self.trigger_control_panel.setHorizontalHeaderLabels(['Model', 'Coverage', 'Branch', 'Street links'])

        models = self.project.get_models()
        for model in models:
            row_position = self.trigger_control_panel.rowCount()
            self.trigger_control_panel.insertRow(row_position)
            self.trigger_control_panel.setItem(row_position, 0, QTableWidgetItem(str(model)))
            self.trigger_control_panel.setCellWidget(row_position, 1, TriggerControlWidget(self.trigger_control_panel, self.project, model, 'coverages'))
            self.trigger_control_panel.setCellWidget(row_position, 2, TriggerControlWidget(self.trigger_control_panel, self.project, model, 'branches'))
            self.trigger_control_panel.setCellWidget(row_position, 3, TriggerControlWidget(self.trigger_control_panel, self.project, model, 'street links'))
            self.trigger_control_panel.setRowHeight(row_position, self.trigger_control_panel.rowHeight(row_position)*2.5)
        self.trigger_control_panel.resizeColumnsToContents()
        self.trigger_control_panel.setSortingEnabled(True)

    def __select_color(self, index):
        in_color = self.__get_color('color_button_{}'.format(index))
        out_color = QColorDialog.getColor(QColor(*in_color))
        if out_color.isValid():
            getattr(self, 'color_button_{}'.format(index)).setStyleSheet("background-color:rgb{}".format(out_color.getRgb()))

    def __set_velocity_classes(self, array):
        for i in range(0,5):
            getattr(self, 'velocity_{}'.format(i)).setText(str(array[i]))

    def __set_velocity_color(self, color_array):
        for i in range(0,5):
            color = QColor(*color_array[i])
            getattr(self, 'color_button_{}'.format(i)).setStyleSheet("background-color:rgb{}".format(color.getRgb()))

    def __get_velocity_classes(self):
        arr=[]
        for i in range(0,5):
            arr.append(getattr(self, 'velocity_{}'.format(i)).text())
        return map(float, arr)

    def __get_color(self, button_name):
        style = getattr(self, button_name).styleSheet()
        color_array = style.split('(')[1].strip(')').split(', ')
        return map(int, color_array)

    def __get_velocity_colors(self):
        colors=[]
        for i in range(0,5):
            colors.append(self.__get_color('color_button_{}'.format(i)))
        return colors

    def save(self):
        if self.project is not None and len(self.project.get_models())>0 and self.__trigger_control_columns_exists(self.project):
            for irow in range(self.trigger_control_panel.rowCount()):
                for icol in range(1,4):
                    self.trigger_control_panel.cellWidget(irow, icol).update_metadata()

        SettingsProperties.write_settings_file(float(self.snap.text()),
                                                int(self.server_port.text()), self.server_host.text(),
                                                self.local_use.isChecked(), int(self.local_port.text()),
                                                self.path_postgre.text(), self.path_python.text(),
                                                float(self.timestep.text()),
                                                float(self.scale.text()), self.__get_velocity_classes(), self.__get_velocity_colors())
        self.close()