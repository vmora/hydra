# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from .base_dialog import BaseDialog, tr
from .widgets.hydra_table_widget import HydraTableWidget
from ..utility.string import normalized_name
from functools import partial
import hydra.utility.string as string
from PyQt4 import uic, QtGui, QtCore
from PyQt4.QtGui import QTableWidgetItem
import os
import re

class DryInflowSectorSettings(BaseDialog):

    def list_to_sql_array(self, py_list):
        return str(map(str, py_list)).replace('[', '{').replace(']', '}').replace('\'', '') if py_list else None

    def float_sql(self, float_value):
        value = string.get_sql_float(float_value)
        if value is None:
            value="0"
        return value

    def nullable_float_sql(self, float_value):
        value = string.get_sql_float(float_value)
        if value is None:
            value="null"
        return value

    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "dry_inflow_sector_settings.ui"), self)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.table_sectors.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.table_sectors.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.active_sector_id = None

        self.table_sectors.sortItems(1)

        sectors = self.project.execute("select id, name, comment from project.dry_inflow_sector").fetchall()

        self.table_sectors.setRowCount(0)
        self.table_sectors.setSortingEnabled(False)

        for sector in sectors:
            row_position = self.table_sectors.rowCount()
            self.table_sectors.insertRow(row_position)
            self.table_sectors.setItem(row_position,0,QTableWidgetItem(string.get_str(sector[0])))
            self.table_sectors.setItem(row_position,1,QTableWidgetItem(string.get_str(sector[1])))
            self.table_sectors.setItem(row_position,2,QTableWidgetItem(string.get_str(sector[2])))

        self.table_sectors.resizeColumnsToContents()
        self.table_sectors.horizontalHeader().setStretchLastSection(True)
        self.table_sectors.itemSelectionChanged.connect(self.set_active_sector)
        self.table_sectors.setSortingEnabled(True)

        self.table_dry_inflow_sector_settings.itemChanged.connect(self.table_dry_inflow_sector_itemChanged)

    def set_active_sector(self):
        items = self.table_sectors.selectedItems()
        if len(items)>0:
            selected_row = self.table_sectors.row(items[0])
            if not self.active_sector_id is None:
                self.save_sector_settings()

            self.active_sector_id = self.table_sectors.item(selected_row,0).text()
            self.sector_name.setText(self.table_sectors.item(selected_row,1).text())
            self.table_dry_inflow_sector_settings.setRowCount(0)
            self.add_model_hydrographs()
            self.set_computed_distribution_coef()

    def add_model_hydrographs(self):
        if self.active_sector_id is not None:
            models = self.project.get_models()
            for model in models:
                hydrographs = self.project.execute("""
                    with n as (select geom from project.dry_inflow_sector where id={})
                    select bc.id, bc.name, bc.distrib_coef, bc.constant_dry_flow, bc.sector,
                    bc.lag_time_hr
                    from {}.hydrograph_bc_singularity as bc, n
                    where st_contains(n.geom,bc.geom);""".format(self.active_sector_id, model)).fetchall()

                n = len(hydrographs)
                previous_row = self.table_dry_inflow_sector_settings.rowCount()
                self.table_dry_inflow_sector_settings.setRowCount(n + previous_row)

                for row in range(0, n):
                    index = row + previous_row
                    self.table_dry_inflow_sector_settings.setItem(index,0,QTableWidgetItem(string.get_str(hydrographs[row][0])))
                    self.table_dry_inflow_sector_settings.setItem(index,1,QTableWidgetItem(model))
                    self.table_dry_inflow_sector_settings.setItem(index,2,QTableWidgetItem(string.get_str(hydrographs[row][1])))

                    self.table_dry_inflow_sector_settings.setItem(index,3,QTableWidgetItem(""))
                    if string.get_sql_float(hydrographs[row][4])==None:
                        self.table_dry_inflow_sector_settings.item(index,3).setCheckState(QtCore.Qt.Unchecked)
                    else:
                        self.table_dry_inflow_sector_settings.item(index,3).setCheckState(QtCore.Qt.Checked)
                    self.table_dry_inflow_sector_settings.item(index,3).setFlags(QtCore.Qt.ItemIsEnabled)

                    self.table_dry_inflow_sector_settings.setItem(index,4,QTableWidgetItem(string.get_str(hydrographs[row][2])))
                    self.table_dry_inflow_sector_settings.setItem(index,6,QTableWidgetItem(string.get_str(hydrographs[row][3])))
                    self.table_dry_inflow_sector_settings.setItem(index,7,QTableWidgetItem(string.get_str(hydrographs[row][5])))

            self.table_dry_inflow_sector_settings.resizeColumnsToContents()
            self.table_dry_inflow_sector_settings.horizontalHeader().setStretchLastSection(True)

    def table_dry_inflow_sector_itemChanged(self, item):
        # compute distrib coefs when value in column 3 (def by sector) or 4 (contribution coef) is changed
        if item.column()==3 or item.column()==4:
            self.set_computed_distribution_coef()

    def set_computed_distribution_coef(self):
        row_count = self.table_dry_inflow_sector_settings.rowCount()
        # sum total active hydrographs contributions
        total_contribution = 0
        for row in range(row_count):
            if self.table_dry_inflow_sector_settings.item(row, 3) is not None and self.table_dry_inflow_sector_settings.item(row, 3).checkState()==QtCore.Qt.Checked \
            and self.table_dry_inflow_sector_settings.item(row, 4) is not None and self.float_sql(self.table_dry_inflow_sector_settings.item(row, 4).text()) >= 0:
                total_contribution += self.float_sql(self.table_dry_inflow_sector_settings.item(row, 4).text())
        # set each hydrograph distribution coef
        for row in range(row_count):
            if self.table_dry_inflow_sector_settings.item(row, 3) is not None and self.table_dry_inflow_sector_settings.item(row, 3).checkState()==QtCore.Qt.Checked \
            and self.table_dry_inflow_sector_settings.item(row, 4) is not None and self.float_sql(self.table_dry_inflow_sector_settings.item(row, 4).text()) >= 0:
                self.table_dry_inflow_sector_settings.setItem(row,5,QTableWidgetItem(string.get_str(round(self.float_sql(self.table_dry_inflow_sector_settings.item(row, 4).text())/total_contribution,3))))
            else:
                self.table_dry_inflow_sector_settings.setItem(row,5,QTableWidgetItem(""))
            self.table_dry_inflow_sector_settings.item(row,5).setFlags(QtCore.Qt.ItemIsEnabled)

    def save_sector_settings(self):
        if self.active_sector_id is None:
            return

        items = self.get_sector_settings_items()

        if len(items)==0:
            return

        values = list()
        for item in items:
            sql= """update {}.hydrograph_bc_singularity
                set constant_dry_flow={}, distrib_coef={},
                lag_time_hr={},
                sector={}
                where id={};
                """.format(item[0], item[1], item[2],item[3],
                item[4],item[5])

            self.project.execute(sql)

    def get_sector_settings_items(self):
        n = self.table_dry_inflow_sector_settings.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            result.append(list())
            result[index_list].append(self.table_dry_inflow_sector_settings.item(i,1).text())
            result[index_list].append(self.nullable_float_sql(self.table_dry_inflow_sector_settings.item(i,6).text()))
            result[index_list].append(self.nullable_float_sql(self.table_dry_inflow_sector_settings.item(i,4).text()))
            result[index_list].append(self.nullable_float_sql(self.table_dry_inflow_sector_settings.item(i,7).text()))
            if self.table_dry_inflow_sector_settings.item(i,3).checkState()==QtCore.Qt.Checked:
                result[index_list].append(self.active_sector_id)
            else:
                result[index_list].append("null")
            result[index_list].append(self.table_dry_inflow_sector_settings.item(i,0).text())

            index_list = index_list + 1
        return result

    def save(self):
        if not self.active_sector_id is None:
            self.save_sector_settings()
        BaseDialog.save(self)
        self.close()