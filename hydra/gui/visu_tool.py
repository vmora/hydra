# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import division
from __future__ import unicode_literals
import json
import numpy
import os
import re
import io
import glob
from functools import partial
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT, FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt4 import uic, QtGui
from PyQt4.QtCore import QCoreApplication, QMargins, Qt
from PyQt4.QtGui import QDialog, QTreeWidgetItem , QApplication, QComboBox, QMenu, QMessageBox, QHBoxLayout, QWidget, QMainWindow, QCursor, QVBoxLayout, QFileDialog, QLabel, QPushButton, QImage, QStatusBar, QInputDialog, QTableWidgetItem
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.utility.result_decoder import HydraResult
from hydra.utility.tables_properties import TablesProperties
from hydra.gui.visu_graph import VisuGraph
from hydra.gui.visu_table import VisuTable

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

elements = json.load(open(os.path.join(os.path.dirname(__file__), "visu.json")))

files_dir = {
    "w15":"hydraulique",
    "w14":"hydraulique",
    "w13":"hydrol"
}

titles = {
    "qam":'Flow(m3/s)',
    "zam":'Level(m)'
}

class VisuMenu(QWidget):
    def __init__(self, project, list_objects, parent=None):
        QWidget.__init__(self, parent)
        self.__currentproject = project
        current_dir = os.path.dirname(__file__)
        menu = QMenu()
        pos = self.mapFromGlobal(QCursor.pos())
        menu.move(pos.x(), pos.y())
        self.__properties = TablesProperties.get_properties()
        self.__action = None
        self.__table = None
        self.__id = None

        for key in list_objects.keys():
            tmp_menu = menu.addMenu(tr(self.__properties[key]['name']))
            for value in list_objects[key][0]:
                tmp_menu.addAction(str(value[0]) + ": " + str(value[1])).triggered.connect(partial(self.__menu_click, elements[key]["table"]))

        menu.exec_()

    def __menu_click(self, table):
        self.__table = table
        self.__id = self.sender().text().split(":",1)[1].strip()

    def get_action(self):
        return (self.__table, self.__id)

# Don't use SelectTool: it need to filter objects before showing menu.

class VisuTool(QWidget):
    def __init__(self, project, point, size, parent=None):
        QWidget.__init__(self, parent)
        self.currentproject = project
        list_objects = self.get_editables_objects(point, size)
        if len(list_objects)==0:
            return
			
    def get_editables_objects(self, point, size):
        x1 = str(point[0]-size)
        x2 = str(point[0]+size)
        y1 = str(point[1]-size)
        y2 = str(point[1]+size)
        polygon =  x1 + " "+ y1 +","+ x1 + " "+ y2 +","+ x2 + " "+ y2 +","+ x2 + " "+ y1 +","+ x1 + " "+ y1

        results = dict()
        self.layermap_process(elements, results, polygon)
        return results

    def layermap_process(self, items, results, polygon):
        for key in items:
            object = items[key]
            if object['type']=="layer":
                sql = """
                        select {}, name from {}.{}
                        where ST_Intersects('srid={}; POLYGON(({}))'::geometry, {})
                        """.format(object['key'], self.currentproject.get_current_model().name,
                        str(object['table']), self.currentproject.srid, polygon, str(object['geom']))
                result = self.currentproject.execute(sql).fetchall()
                if len(result)>0:
                    results[str(object['table'])]= (result, object['key'])

            if object['type']=="group":
                self.__edit_tool_layermap_process(object['objects'], results, polygon)

class VisuToolGraph(VisuTool) :
    def __init__ (self, project, point, size, parent=None) :
        VisuTool.__init__(self, project, point, size, parent)
        list_objects = self.get_editables_objects(point, size)
        self.table, self.objectname = VisuMenu(project, list_objects, None).get_action()
        if self.table is None:
            return
        id_scn, name_scn = self.currentproject.get_current_scenario()
        model_name = self.currentproject.get_current_model().name
        if model_name is None or id_scn is None:
            return
        VisuGraph(project, project.get_current_model().name, self.objectname, self.table, project.get_current_scenario()[0], parent)

class VisuToolTable(VisuTool) :
    def __init__ (self, project, point, size, parent=None) :
        VisuTool.__init__(self, project, point, size, parent)
        list_objects = self.get_editables_objects(point, size)
        self.table, self.objectname = VisuMenu(project,list_objects, None).get_action()
        if self.table is None:
            return
        id_scn, name_scn = self.currentproject.get_current_scenario()
        model_name = self.currentproject.get_current_model().name
        if model_name is None or id_scn is None:
            return
        VisuTable(project, self.objectname, self.table, parent)

if __name__=='__main__':
    from PyQt4.QtGui import QApplication
    from hydra.project import Project
    import sys
    app = QApplication(sys.argv)
    obj_project = Project.load_project("pdemo")
    # current_project, model_name, name_item, table_item, id_scn
    graph = VisuGraph(obj_project, "model1", "NODR909", "river_node", 2)
    graph.show()
    app.exec_()
