# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from hydra.gui.base_dialog import BaseDialog, tr
import hydra.utility.string as string
from PyQt4 import uic, QtGui, QtCore
from PyQt4.QtGui import QTableWidgetItem, QFileDialog, QMessageBox
from qgis.gui import QgsGenericProjectionSelector
import os
import re

class TerrainManager(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "terrain_manager.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.btnAdd.clicked.connect(self.add)
        self.btnDel.clicked.connect(self.delete)
        self.btnUp.clicked.connect(self.up)
        self.btnDown.clicked.connect(self.down)
        self.btnAscToVrt.clicked.connect(self.asc_to_vrt)

        self.table_terrain.resizeColumnsToContents()
        self.table_terrain.horizontalHeader().setStretchLastSection(True);
        self.__refresh_table_terrain()
        self.table_terrain.itemChanged.connect(self.on_data_edited)

    def on_data_edited(self, new_value):
        if not self.__loading:
            self.table_terrain.resizeColumnsToContents()
            self.table_terrain.horizontalHeader().setStretchLastSection(True);
            current_index = self.table_terrain.currentRow()
            self.project.execute("""update project.dem set source='{}' where id={};""".format(
                self.project.pack_path(self.table_terrain.item(current_index,2).text()),
                self.table_terrain.item(current_index,0).text()))
            self.__refresh_table_terrain()

    def __refresh_table_terrain(self):
        self.__loading = True
        terrains = self.project.execute("""
                    select id, priority, source
                    from project.dem
                    order by priority asc
                    """).fetchall()
        self.table_terrain.setRowCount(0)
        for t in terrains:
            rowPosition = self.table_terrain.rowCount()
            self.table_terrain.insertRow(rowPosition)
            self.table_terrain.setItem(rowPosition , 0, QTableWidgetItem(str(t[0])))
            self.table_terrain.setItem(rowPosition , 1, QTableWidgetItem(str(t[1])))
            self.table_terrain.setItem(rowPosition , 2, QTableWidgetItem(self.project.unpack_path(t[2])))
        self.__loading = False

    def up(self):
        current_index = self.table_terrain.currentRow()
        if current_index==-1:
            return
        self.__loading = True
        item1 = self.table_terrain.takeItem(current_index, 0)
        item2 = self.table_terrain.takeItem(current_index, 1)
        item3 = self.table_terrain.takeItem(current_index, 2)
        rowPosition = current_index - 1
        self.table_terrain.insertRow(rowPosition)
        self.table_terrain.setItem(rowPosition , 0, item1)
        self.table_terrain.setItem(rowPosition , 1, item2)
        self.table_terrain.setItem(rowPosition , 2, item3)
        self.table_terrain.removeRow(current_index+1)
        self.update_priorities()
        self.__refresh_table_terrain()

    def down(self):
        current_index = self.table_terrain.currentRow()
        if current_index==-1:
            return
        if current_index==self.table_terrain.rowCount()-1:
            return
        self.__loading = True
        item1 = self.table_terrain.takeItem(current_index, 0)
        item2 = self.table_terrain.takeItem(current_index, 1)
        item3 = self.table_terrain.takeItem(current_index, 2)
        rowPosition = current_index + 2
        self.table_terrain.insertRow(rowPosition)
        self.table_terrain.setItem(rowPosition , 0, item1)
        self.table_terrain.setItem(rowPosition , 1, item2)
        self.table_terrain.setItem(rowPosition , 2, item3)
        self.table_terrain.removeRow(current_index)
        self.update_priorities()
        self.__refresh_table_terrain()

    def update_priorities(self):
        for i in range(0, self.table_terrain.rowCount()):
            self.project.execute("""update project.dem set priority={} where id={};""".format(
                i+1, self.table_terrain.item(i,0).text()))

    def add(self):
        title = tr('Add file')
        file = QFileDialog.getOpenFileName(self, title, self.project.get_project_dir(), tr("Terrain files (*.vrt)"))
        if not file is None and os.path.exists(file):
            self.project.execute("""insert into project.dem(source) values('{0}')""".format(self.project.pack_path(file)))
            self.__refresh_table_terrain()
            self.update_priorities()
            self.__refresh_table_terrain()

    def delete(self):
        current_index = self.table_terrain.currentRow()
        if current_index==-1:
            return
        self.project.execute("""delete from project.dem where id={0}""".format(self.table_terrain.item(current_index,0).text()))
        self.__refresh_table_terrain()
        self.update_priorities()
        self.__refresh_table_terrain()

    def asc_to_vrt(self):
        files = QFileDialog.getOpenFileNames(self, tr("Select one or more files to open"), self.project.get_project_dir(), tr("Images (*.asc *.tiff *.tif)"))
        if files:
            vrt = self.project.build_vrt(files, self.project.srid)
            self.__refresh_table_terrain()
            self.update_priorities()
            self.__refresh_table_terrain()
            self.project.save()

    def save(self):
        BaseDialog.save(self)
        self.close()
