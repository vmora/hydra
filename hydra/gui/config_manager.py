# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
from PyQt4 import uic, QtGui, QtCore
from PyQt4.QtGui import QTableWidgetItem, QFileDialog, QMessageBox
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.base_dialog import BaseDialog, tr
import hydra.utility.string as string

class ConfigManager(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "config_manager.ui"), self)

        self.project = project
        self.table_config = None

        models = project.get_models()
        if len(models)==0:
            project.log.notice(tr("No model in project"))
            self.close()

        for model in models :
            self.models_combo.addItem(model)
        self.models_combo.setCurrentIndex(0)
        self.models_combo.activated.connect(self.model_changed)

        self.table_config = HydraTableWidget(project,
            ("id", "name"),
            (tr("Id"), tr("Name")),
            self.models_combo.currentText(), "configuration",
            (self.new_config, self.delete_config), "", "id",
            self.table_config_placeholder)
        self.table_config.set_editable(True)
        self.table_config.data_edited.connect(self.save_config)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

    def model_changed(self):
        self.table_config.set_dbname(self.models_combo.currentText())

    def delete_config(self):
        items = self.table_config.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_config.table.row(items[0])
            id = int(self.table_config.table.item(selected_row,0).text())
            if int(id)>1:
                if self.project.get_current_model().get_current_configuration() != id:
                    self.project.get_current_model().delete_config(id)
                    self.table_config.update_data()
                    self.project.commit()
                else:
                    QMessageBox(QMessageBox.Warning, tr('Configuration error'), tr("Cannot delete active configuration {} on model {}.".format(self.table_config.table.item(selected_row,1).text(), self.models_combo.currentText())), QMessageBox.Ok).exec_()

    def new_config(self):
        self.project.execute("""insert into {}.configuration default values;""".format(self.models_combo.currentText()))
        self.table_config.update_data()

    def save_config(self):
        self.table_config.save_selected_row()

    def save(self):
        BaseDialog.save(self)
        self.close()