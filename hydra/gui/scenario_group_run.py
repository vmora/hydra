# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals

from PyQt4.QtGui import QDialog, QApplication, QTableWidgetItem, QWidget, QPushButton
from PyQt4 import uic, QtGui
from PyQt4.QtCore import Qt

from hydra.gui.base_dialog import tr
from hydra.database.export_calcul import ExportCalcul
from hydra.database.export_carto_data import ExportCartoData
from hydra.kernel import hydra_kernel
import os

class ScenarioGroupRun(QDialog):
    def __init__(self, project, group_id, parent=None):
        QDialog.__init__(self, parent)
        self.project = project
        self.group_id = group_id
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "scenario_group_run.ui"), self)
        self.project = project
        self.cancel_asked=False

        group_name, = self.project.execute("""
            select name
            from project.scenario_group
            where id={id}""".format(id=self.group_id)).fetchone()
        self.lbl_group_name.setText(group_name)

        self.btnRunAll.clicked.connect(self.run_all)
        self.btnRunNotCalculated.clicked.connect(self.run_not_calculated)
        self.btnCancel.clicked.connect(self.cancel)
        self.btnClose.clicked.connect(self.close)

        self.table.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.update_scenarios()
        self.show()

    def run_all(self):
        self.cancel_asked=False
        scenarios = self.project.execute("""
                select id
                from project.scenario as scn
                where scn.id in
                    (select scenario
                    from project.param_scenario_group
                    where grp=%i)
                order by scn.name asc"""%(self.group_id)).fetchall()
        for scn in scenarios:
            if self.cancel_asked:
                break
            self.run_scenario(int(scn[0]))

    def run_not_calculated(self):
        self.cancel_asked=False
        for i in range(0, self.table.rowCount()):
            if self.cancel_asked:
                break
            id_scn, comput_mode = self.project.execute("""
                select id, comput_mode
                from project.scenario as scn
                where scn.name='{name}'""".format(name =self.table.item(i, 0).text())).fetchone()
            scn_path = self.project.get_senario_path(id_scn)
            err, message, date = hydra_kernel.get_status(scn_path, comput_mode)
            if err==-1:
                self.run_scenario(int(id_scn))

    def cancel(self):
        self.cancel_asked=True

    def run_scenario(self, id_scn):
        self.project.set_current_scenario(id_scn)
        comput_mode, = self.project.execute("""
            select comput_mode
            from project.scenario
            where id={}
            """.format(id_scn)).fetchone()
        # tests validity?
        # export topological data for calculation
        exporter = ExportCalcul(self.project)
        exporter.export(id_scn)
        # export cartograpic data for triangulation
        exporter = ExportCartoData(self.project)
        exporter.export(id_scn)
        flag_gfx_control, = self.project.execute("""
            select flag_gfx_control
            from project.scenario
            where id={} limit 1""".format(str(id_scn))).fetchone()
        # run calculations
        try:
            QApplication.setOverrideCursor(Qt.WaitCursor)
            QApplication.processEvents()
            mute = False
            hydra_kernel.run(self.project.get_senario_path(id_scn), flag_gfx_control, comput_mode, self if not mute else None, False)
        except Exception as e:
            QApplication.restoreOverrideCursor()
            raise e
        QApplication.restoreOverrideCursor()
        self.update_scenarios()

    def make_run_scenario(self, id):
        def run():
            self.run_scenario(id)
        return run

    def update_scenarios(self):
        scenarios = self.project.execute("""
            select name, id, comput_mode
            from project.scenario as scn
            where scn.id in
                (select scenario
                from project.param_scenario_group
                where grp=%i)
			order by scn.name asc"""%(self.group_id)).fetchall()

        self.table.setRowCount(0)
        for scn in scenarios:
            self.add_scenario(scn[0], int(scn[1]), scn[2])

    def add_scenario(self, scn_name, scn_id, comput_mode):
        rowcount = self.table.rowCount() + 1
        self.table.setRowCount(rowcount)
        row = rowcount - 1

        item = QTableWidgetItem(scn_name)
        self.table.setItem(row, 0, item)

        scn_path = self.project.get_senario_path(scn_id)
        err, message, date = hydra_kernel.get_status(scn_path, comput_mode)

        item = QTableWidgetItem(message)
        self.table.setItem(row, 1, item)
        if err==-1: #not calculated
            self.table.item(row, 1).setForeground(QtGui.QColor(255,0,0))
        elif err!=0: #error
            self.table.item(row, 1).setBackground(QtGui.QColor(255,0,0))

        item = QTableWidgetItem(date)
        self.table.setItem(row, 2, item)

        btn = QPushButton(self.table)
        btn.setText(tr("Run") + " " + scn_name)
        btn.clicked.connect(self.make_run_scenario(scn_id))
        self.table.setCellWidget(row, 3, btn)

if __name__ == '__main__':
    from hydra.project import Project
    import sys
    app = QApplication(sys.argv)
    obj_project = Project.load_project("scngrp")
    run_dlg = ScenarioGroupRun(obj_project, 2)
    run_dlg.exec_()
