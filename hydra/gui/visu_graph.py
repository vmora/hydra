# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import division
from __future__ import unicode_literals
import json
import numpy
import weakref
import csv
import os
import shutil
import re
import io
import glob
from functools import partial
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT, FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt4 import uic, QtGui
from PyQt4.QtCore import QCoreApplication, QMargins, Qt
from PyQt4.QtGui import QDialog, QTreeWidgetItem , QApplication, QComboBox, QMenu, QMessageBox, QHBoxLayout, QWidget, QMainWindow, QCursor, QVBoxLayout, QFileDialog, QLabel, QPushButton, QImage, QStatusBar, QInputDialog, QTableWidgetItem
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.utility.result_decoder import HydraResult
from hydra.utility.tables_properties import TablesProperties

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

_desktop_dir = os.path.join(os.path.expanduser("~"), "Desktop")

elements = json.load(open(os.path.join(os.path.dirname(__file__), "visu.json")))

files_dir = {
    "w15":"hydraulique",
    "w14":"hydraulique",
    "w13":"hydrol"
}

titles = {
    "qam":'Flow(m3/s)',
    "zam":'Level(m)'
}

class Line(object):
    def __init__(self, levels, values, name ,type = None, table = None, color='k'):

        self.__levels = levels
        self.__values = values
        self.name = name
        self.type = type
        self.table = table
        self.color = color

    def get_values(self):
            return self.__values

    def get_levels(self):
        return self.__levels

class VisuGraph(QDialog):
    class NavigationToolbar(NavigationToolbar2QT):
        # only display the buttons we need
        toolitems = [t for t in NavigationToolbar2QT.toolitems if
                     t[0] in ('Home', 'Back', 'Forward', 'Pan', 'Zoom', 'Subplots')]

    class HydraResult_Line(Line):
        def __init__ (self, hydraresult,name,line_name,table,data,color, data_index) :
            results = hydraresult[str(name)][:]

            Line.__init__(self,[item[0] for item in results],[item[1][data_index] for item in results],
            line_name,data,table,color)

    class CustomLine(Line):
        def __init__(self, filepath, color ='k'):
            with open(filepath,'rb') as graph_extern_csv:
                filename = os.path.basename(filepath)
                dialect = csv.Sniffer().sniff(graph_extern_csv.read(1024))
                graph_extern_csv.seek(0)
                graph_extern_reader = csv.reader(graph_extern_csv,dialect=dialect)
                if len(graph_extern_reader.next()) >= 2 :
                    graph_extern_csv.seek(0)
                    xydata=[[row[0],row[1]] for row in graph_extern_reader]
                    xdata=[x for [x,y] in xydata]
                    ydata=[y for [x,y] in xydata]
                    filename = filename+" ; "+ydata[0]
                    xdata=xdata[1:]
                    ydata=ydata[1:]

                Line.__init__(self,xdata,ydata,filename, color=color)

    def __init__(self, current_project, model_name, name_item, table_item, id_scn, parent=None):
        QDialog.__init__(self, parent)
        self.setWindowFlags(self.windowFlags() |
            Qt.WindowSystemMenuHint |
            Qt.WindowMinMaxButtonsHint)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "visu_graph.ui"), self)
        self.__project = current_project
        self.__result_axe_1 = []
        self.__result_axe_2 = []
        self.__curves_axes = []
        self.__plot_list = []
        self.__dot_on_curve = None
        self.__available_objects = dict()
        self.__selected_curve = None
        self.__axe1_colors = ["#0000ff", "#aa00ff", "#00aaff", "#00ffff", "#55007f", "#00557f"]
        self.__axe2_colors = ["#00ff00", "#005500", "#aaaa00", "#aaff00", "#aaaa7f", "#ffaa00"]
        self.create_main_frame()

        # QTreeWidget
        self.curves.setColumnCount(1)
        labels = [tr("Axe"), tr("Curves")]
        self.curves.setHeaderLabels(labels)
        self.curves.setUniformRowHeights(True)
        self.__init_tree()

        self.transport_type = None
        self.quality_type = None
        self.data_index =0
        scn_id_name = self.__project.get_scenarios()
        for scn in scn_id_name :
            self.cbo_scenario.addItem(scn[1], scn[0])
        self.cbo_scenario.activated.connect(self.__scn_changed)
        self.cbo_model.activated.connect(self.__mdl_changed)
        self.cbo_types.activated.connect(self.__type_changed)
        self.cbo_objects.activated.connect(self.__object_changed)
        self.cbo_scenario.set_selected_value(id_scn)
        self.__scn_changed()
        self.cbo_model.set_selected_value(model_name)
        self.__mdl_changed()
        self.cbo_types.set_selected_value(table_item)
        self.__type_changed()
        self.cbo_objects.set_selected_value(name_item)
        self.__object_changed()
        first_result = "None"
        i = 0
        while first_result == "None":
            first_result = elements[table_item]["results"][i]
            i=i+1
        self.cbo_results.set_selected_value(first_result)
        self.btn_axe_1.clicked.connect(self.__add_axe1)
        self.btn_axe_2.clicked.connect(self.__add_axe2)
        self.btn_reset.clicked.connect(self.__reset)
        self.btn_remove.clicked.connect(self.__remove_selected_curve)
        self.save_graph.clicked.connect(self.__save_graph)
        self.load_graph.clicked.connect(self.__load_graph)
        self.import_graph.clicked.connect(self.__import_graph)
        self.next_graph.clicked.connect(self.__goto_next_graph)
        self.previous_graph.clicked.connect(self.__goto_previous_graph)
        self.chk_legend.stateChanged.connect(self.on_draw)
        self.__add_axe1()
        self.curves.header().setResizeMode(QtGui.QHeaderView.ResizeToContents)
        self.__set_graph_title("Hydra graph")
        graph_folder = os.path.join(self.__project.get_project_dir(), "graphs")
        if not os.path.isdir(graph_folder):
            os.mkdir(graph_folder)

        self.show()

    def __init_tree(self):
        axe1 = QTreeWidgetItem()
        self.curves.addTopLevelItem(axe1)
        axe1.setText(0, tr('Axe 1'))
        axe2 = QTreeWidgetItem()
        self.curves.addTopLevelItem(axe2)
        axe2.setText(0, tr('Axe 2'))
        self.__curves_axes = []
        self.__curves_axes.append(axe1)
        self.__curves_axes.append(axe2)


    def __goto_next_graph(self):
        self.__goto_file(1)

    def __goto_previous_graph(self):
        self.__goto_file(-1)

    def __goto_file(self, offset):
        graph_folder = os.path.join(self.__project.get_project_dir(), "graphs")
        if not os.path.isdir(graph_folder):
            return
        graph_files = glob.glob(graph_folder+"/*.hgf")
        currentFile = os.path.join(graph_folder, str(self.graph_title.text()) +".hgf")
        if not currentFile in graph_files:
            return
        index_file = graph_files.index(currentFile) + offset
        if index_file<0:
            index_file = len(graph_files)-1
        if index_file>=len(graph_files):
            index_file = 0
        self.__load_graph_file(graph_files[index_file])

    def __save_graph(self):
        graph_folder = os.path.join(self.__project.get_project_dir(), "graphs")
        if not os.path.isdir(graph_folder):
            os.mkdir(graph_folder)

        fileName = os.path.join(graph_folder, str(self.graph_title.text()) +".hgf")
        if fileName is None:
            return

        if os.path.isfile(fileName):
            reply = QtGui.QMessageBox.question(self, tr('Visu tool'), str(tr("The file {f} already exists,\ndo you want overwrite?")).format(f=fileName), QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.No:
                return

        data = dict()
        data["config"]=[str(self.graph_title.text()),str(self.left_axis_title.text()),str(self.right_axis_title.text())]
        data["axe1"]=[[str(n.name), n.color, str(n.table)] for n in self.__result_axe_1]
        data["axe2"]=[[str(n.name), n.color, str(n.table)] for n in self.__result_axe_2]
        with open(fileName, 'w') as fp:
            json.dump(data, fp, indent=4)
        self.__update_status()
        QMessageBox.information(self, tr('Graphic saved'),
            str(tr('Graphic successfully saved to:\n {f}')).format(f=fileName), QMessageBox.Ok)

    def __load_graph(self):
        title = tr('Load graphic')
        graph_folder = os.path.join(self.__project.get_project_dir(), "graphs")
        if not os.path.isdir(graph_folder):
            os.mkdir(graph_folder)

        graph_files = glob.glob(graph_folder+"/*.hgf")
        filenames = [os.path.basename(n).replace('.hgf','') for n in graph_files]
        file=None
        if len(filenames)==1:
            file=filenames[0]
        elif len(filenames)>1:
            f, ok = QInputDialog.getItem(None, "Graph selection", "Select the graph you want to display.", filenames, 0, False)
            if ok and f:
                file=f
        if file:
            self.__load_graph_file(graph_files[filenames.index(file)])

    def __load_graph_file(self, fileName):
        with open(fileName) as fp:
            data = json.load(fp)
            self.__reset()
            self.graph_title.setText(data["config"][0])
            self.left_axis_title.setText(data["config"][1])
            self.right_axis_title.setText(data["config"][2])
            for curve in data["axe1"]:
                result = [n.strip() for n in curve[0].split(';')]
                if result[0].find('.csv')>0 or result[0].find('.xls')>0:
                    self.__import_graph_from_save(result[0],1)
                else:
                    if self.__add_to_axe_results(self.__result_axe_1, curve[1], result[0], result[1], curve[2], result[2], result[3]):
                        self.__add_line_to_curves_table(self.__result_axe_1[-1].name, 1)
            for curve in data["axe2"]:
                if self.__axe2 is None:
                    self.__axe2 = self.__axe1.twinx()
                    self.__on_right_title_text_changed()
                result = [str(n.strip()) for n in curve[0].split(';')]
                if result[0].find('.csv')>0 or result[0].find('.xls')>0:
                    self.__import_graph_from_save(result[0],2)
                else :
                    if self.__add_to_axe_results(self.__result_axe_2, curve[1], result[0], result[1], curve[2], result[2], result[3]):
                        self.__add_line_to_curves_table(self.__result_axe_2[-1].name, 2)
        self.__update_status()
        self.on_draw()

    def __import_graph(self):
        title = tr('Import external graph (first row as X, second as Y)')
        graph_folder = os.path.join(self.__project.get_project_dir(), "graphs")
        filter = tr('(*.csv *.xls)')
        filepath = QFileDialog.getOpenFileName(None , title, _desktop_dir, filter)
        if filepath :
            line = VisuGraph.CustomLine(filepath)
            shutil.copy(filepath,graph_folder)
            if not self.__axe2 :
                ok = True
                axes = "Left axis"
            elif self.__axe2 is not None :
                axes, ok = QInputDialog.getItem(None, tr('Select axis'),tr('Chose an axis to display the graph'),["Left axis","Right axis"], 0, False)
            if ok and axes :
                if axes =="Left axis" :
                    self.__result_axe_1.append(line)
                    self.__add_line_to_curves_table(line.name, 1)
                elif axes =="Right axis" :
                    self.__result_axe_2.append(line)
                    self.__add_line_to_curves_table(line.name, 2)
            self.on_draw()

    def __import_graph_from_save(self,filename,axe):
        assert axe in [1,2]
        graph_folder = os.path.join(self.__project.get_project_dir(), "graphs")
        filepath = os.path.join(graph_folder,filename)
        line = VisuGraph.CustomLine(filepath)
        if axe==1:
            self.__result_axe_1.append(line)
            self.__add_line_to_curves_table(line.name, 1)
        elif axe==2:
            if self.__axe2 is not None:
                self.__result_axe_2.append(line)
                self.__add_line_to_curves_table(line.name, 2)

    def __update_status(self):
        graph_folder = os.path.join(self.__project.get_project_dir(), "graphs")
        if not os.path.isdir(graph_folder):
            return
        graph_files = glob.glob(graph_folder+"/*.hgf")
        currentFile = os.path.join(graph_folder, str(self.graph_title.text()) +".hgf")
        if not currentFile in graph_files:
            self.status_label.setText("File {f} is not in graph folder, navigation will not works.".format(f=currentFile))
            return
        self.status_label.setText("{f} (file {n}/{t})".format(f=currentFile, n=(graph_files.index(currentFile)+1), t=len(graph_files)))

    def __reset(self):
        self.__result_axe_1=[]
        self.__result_axe_2=[]
        self.curves.clear()
        self.__init_tree()
        self.on_draw()

    def __add_axe1(self):
        index = len(self.__result_axe_1)%len(self.__axe1_colors)
        self.__add_to_axe_results_from_gui(self.__result_axe_1, self.__axe1_colors[index])
        self.__add_line_to_curves_table(self.__result_axe_1[-1].name, 1)

    def __add_axe2(self):
        if (self.__axe2 is None):
            self.__axe2 = self.__axe1.twinx()
        index = len(self.__result_axe_2)%len(self.__axe2_colors)
        self.__add_to_axe_results_from_gui(self.__result_axe_2, self.__axe2_colors[index])
        self.__add_line_to_curves_table(self.__result_axe_2[-1].name, 2)

    def __add_to_axe_results_from_gui(self, axe_result, color):
        table = self.cbo_types.get_selected_value()
        scn_name = str(self.cbo_scenario.currentText())
        model_name = str(self.cbo_model.currentText())
        selected_result = self.cbo_results.get_selected_value()
        object_name = self.cbo_objects.currentText()
        self.__add_to_axe_results(axe_result, color, scn_name, model_name, table, object_name, selected_result)

    def __add_to_axe_results(self, axe_result, color, scn_name, model_name, table, object_name, selected_result):
        scn_id_name = self.__project.get_scenarios()
        scn_names = [n[1] for n in scn_id_name]
        if not scn_name in scn_names:
            return False
        models_name = self.__get_models_for_scn(scn_id_name[scn_names.index(scn_name)][0])
        if not model_name in models_name:
            return False
        ext = elements[table]["file"]
        self.__get_availables_objets(model_name, elements, self.__available_objects)
        objects = [n[1] for n in self.__available_objects[table][0]]
        if not object_name in objects:
            return False

        scn_path = self.__project.get_senario_path_from_name(scn_name)
        subdir = files_dir[elements[table]["file"]]
        result_path = os.path.join(scn_path,subdir, scn_name+ "_" + model_name + "." + ext)
        resultfile = HydraResult(result_path)
        index = 0
        if selected_result in elements[table]["results"]:
            index = elements[table]["results"].index(selected_result)
        else:
            index = 4
            if selected_result in elements[table]["pollution"]:
                index = index + elements[table]["pollution"].index(selected_result)
            if selected_result in elements[table]["physico_chemical"]:
                index = index + elements[table]["physico_chemical"].index(selected_result)
            if selected_result in elements[table]["quality_parameter_selection"]:
                index = index + elements[table]["quality_parameter_selection"].index(selected_result)
            if selected_result in elements[table]["suspended sediment transport"]:
                index = index + elements[table]["suspended sediment transport"].index(selected_result)
            if selected_result in elements[table]["sediment"]:
                index = index + elements[table]["sediment"].index(selected_result)
        line_name = scn_name + "; " + model_name + "; " + object_name + " ; " + selected_result
        axe_result.append(VisuGraph.HydraResult_Line(resultfile,
            object_name,
            line_name,
            table,
            selected_result,
            color, index))
        self.on_draw()
        return True

    def __add_line_to_curves_table(self, line_name, num_axe):
        curveItem = QTreeWidgetItem(self.__curves_axes[num_axe-1])
        self.__curves_axes[num_axe-1].setExpanded(True)
        curveItem.setText(1, line_name)

    def __remove_selected_curve(self):
        getSelected = self.curves.selectedItems()
        if getSelected:
            base_node = getSelected[0]

            if base_node.parent() is None:
                return

            selected_curve = base_node.parent().indexOfChild(base_node)

            if base_node.parent().text(0) == self.__curves_axes[0].text(0):
                del self.__result_axe_1[selected_curve]
            else:
                del self.__result_axe_2[selected_curve]
            base_node.parent().removeChild(base_node)
            self.on_draw()

    def __set_combo_and_keep_selection(self, cbo, items, localizable = False):
        selected_item = cbo.get_selected_value()
        cbo.clear()
        for item in items:
            item_text = tr(str(item)) if localizable else str(item)
            cbo.addItem(item_text, item)
        if selected_item in items:
            cbo.set_selected_value(selected_item)

    def __scn_changed(self):
        idscn = self.cbo_scenario.get_selected_value()
        models_name = self.__get_models_for_scn(idscn)
        self.transport_type, self.quality_type = self.__project.execute("""
            select transport_type, quality_type
            from project.scenario
            where id={} limit 1""".format(str(idscn))).fetchone()
        self.__set_combo_and_keep_selection(self.cbo_model, models_name)
        self.__mdl_changed()

    def __mdl_changed(self):
        name_scn = self.cbo_scenario.get_selected_value()
        name_model = self.cbo_model.get_selected_value()
        if name_model=="":
            return
        self.__available_objects = dict()
        self.__get_availables_objets(name_model, elements, self.__available_objects)
        object_types = sorted(self.__available_objects.keys())
        self.__set_combo_and_keep_selection(self.cbo_types, object_types, True)
        self.__type_changed()

    def __type_changed(self):
        object_type = self.cbo_types.get_selected_value()
        objects = [n[1] for n in self.__available_objects[object_type][0]]
        self.__set_combo_and_keep_selection(self.cbo_objects, objects)
        self.__object_changed()

    def __object_changed(self):
        object_type = self.cbo_types.get_selected_value()
        selected_result = self.cbo_results.get_selected_value()
        self.cbo_results.clear()
        reslist = []
        for res in elements[object_type]["results"]:
            if not res=="None":
                self.cbo_results.addItem(tr(res), res)
                reslist.append(res)
        if not self.transport_type == "no_transport" and not self.transport_type=="quality": # TL correction 10/08/2017
            for res in elements[object_type][self.transport_type]:
                if not res=="None":
                    self.cbo_results.addItem(tr(res), res)
                    reslist.append(res)
        if self.transport_type=="quality":
            for res in elements[object_type][self.quality_type]:
                if not res=="None":
                    self.cbo_results.addItem(tr(res), res)
                    reslist.append(res)
        if selected_result in reslist:
            self.cbo_results.set_selected_value(selected_result)

    def __get_models_for_scn(self, id_scn):
        model_connect_settings, = self.__project.execute("""
            select model_connect_settings
            from project.scenario
            where id={}
            """.format(id_scn)).fetchone()

        if model_connect_settings=="global" or model_connect_settings=="cascade":
            return [m for m, in self.__project.execute("""
                    select name
                    from project.model
                    """).fetchall()]
        else:
            return [m for m, in self.__project.execute("""
                    select name
                    from project.model as model
                    where model.name in (
                        select name from project.model_config
                        where id in(
                            select model from project.grp_model
                            where grp in (
                                select id
                                from project.mixed as mixed, project.grp as grp
                                where mixed.grp=grp.id
                                and mixed.scenario={}
                            )
                        )
                    )
                    """.format(id_scn)).fetchall()]

    def __get_availables_objets(self, name_model, items, objects):
        for key in items:
            object = items[key]
            if object['type']=="layer":
                sql = """
                        select {}, name from {}.{} order by name;
                        """.format(object['key'], name_model,
                        str(object['table']))
                result = self.__project.execute(sql).fetchall()
                if len(result)>0:
                    objects[str(object['table'])]=(result, object['key'])

            if object['type']=="group":
                self.__get_availables_objets(name_model, object['objects'], objects)

    def on_draw(self):
        self.__plot_list = []
        self.__dot_on_curve = None
        self.__axe1.clear()
        for res in self.__result_axe_1:
            self.__plot_data(self.__axe1, res.color, res)
            if self.left_axis_title.text()=="":
                self.__set_left_title(tr(titles[res.type]) if res.type in titles else res.type)
            else:
                self.__on_left_title_text_changed()
        self.__format_axes(self.__axe1, 'b')
        self.__on_graph_title_text_changed()
        if self.__axe2:
            self.__axe2.clear()
            for res in self.__result_axe_2:
                self.__plot_data(self.__axe2, res.color, res)
                if self.right_axis_title.text()=="":
                    self.__set_right_title(tr(titles[res.type]) if res.type in titles else res.type)
                else:
                    self.__on_right_title_text_changed()
            self.__format_axes(self.__axe2, 'g')
        self.__axe1.set_xlabel(tr('Time(hr)'))
        self.__fig.set_tight_layout(True)
        if self.chk_legend.isChecked():
            if len(self.__result_axe_1)>0:
                self.__axe1.legend(loc=2)
            if len(self.__result_axe_2)>0:
                self.__axe2.legend(loc=1)
        self.canvas.draw()

    def __set_graph_title(self, title):
        self.graph_title.setText(title)

    def __on_graph_title_text_changed(self):
        self.__axe1.set_title(self.graph_title.text())
        self.canvas.draw()

    def __set_left_title(self, title):
        self.left_axis_title.setText(title)

    def __on_left_title_text_changed(self):
        self.__axe1.set_ylabel(self.left_axis_title.text())
        self.canvas.draw()

    def __set_right_title(self, title):
        self.right_axis_title.setText(title)

    def __on_right_title_text_changed(self):
        if self.__axe2:
            self.__axe2.set_ylabel(self.right_axis_title.text())
        self.canvas.draw()

    def __plot_data(self, axe, color, data):
        self.__plot_list.append(axe.plot(data.get_levels(), data.get_values(), linestyle="-", marker=".", color=color, label=data.name))

    def __format_axes(self, axe, color):
        axe.get_xaxis().get_major_formatter().set_useOffset(False)
        axe.get_yaxis().get_major_formatter().set_useOffset(False)
        axe.grid(True)
        for tl in axe.get_yticklabels():
            tl.set_color(color)

    def export_data_plot(self):
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard )

        if len(self.__result_axe_1)>0:
            times = self.__result_axe_1[0].get_levels()
        elif len(self.__result_axe_2)>0:
            times = self.__result_axe_2[0].get_levels()
        else:
            return

        data_to_export = ""
        for res in self.__result_axe_1:
            data_to_export = data_to_export + '\t' + res.name
        for res in self.__result_axe_2:
            data_to_export = data_to_export + '\t' + res.name
        data_to_export = data_to_export + '\r\n'

        data_to_export = data_to_export + tr('Time (hr)')
        for res in self.__result_axe_1:
            data_to_export = data_to_export + '\t' + tr(titles[res.type]) if res.type in titles else res.type
        for res in self.__result_axe_2:
            data_to_export = data_to_export + '\t' + tr(titles[res.type]) if res.type in titles else res.type
        data_to_export = data_to_export + '\r\n'

        for i in range(0,len(times)):
            data_to_export = data_to_export + str(times[i])
            for res in self.__result_axe_1:
                data_to_export = data_to_export + '\t' + str(res.get_values()[i])
            for res in self.__result_axe_2:
                data_to_export = data_to_export + '\t' + str(res.get_values()[i])
            data_to_export = data_to_export + '\r\n'
        cb.setText(data_to_export, mode=cb.Clipboard)

    def export_image_plot(self):
        buf = io.BytesIO()
        self.__fig.savefig(buf)
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard )
        cb.setImage(QImage.fromData(buf.getvalue()), mode=cb.Clipboard)
        buf.close()

    def __hover_curves(self,event) :
        for line in self.__plot_list :
            cont, ind = line[0].contains(event)
            if cont :
                if self.__dot_on_curve is not None :
                    l = self.__dot_on_curve.pop(0)
                    wl = weakref.ref(l)
                    l.remove()
                    del l
                    self.__dot_on_curve = None
                x,y = line[0].get_data()
                axes = line[0].axes
                self.__labelXY.setText("{xlabel} : {x}  {ylabel} : {y}".format(x=round(x[ind["ind"][0]],2),y=round(y[ind["ind"][0]],2),xlabel=self.__axe1.get_xlabel(),ylabel=axes.get_ylabel()))
                self.__dot_on_curve = axes.plot(x[ind["ind"][0]],y[ind["ind"][0]],linestyle=None,marker=".",color='red')
                self.canvas.draw()
            else :
                self.canvas.draw()

    def create_main_frame(self):
        self.main_frame = QWidget()

        self.__dpi = 75
        self.__fig = Figure(dpi=self.__dpi)
        self.__axe1 = self.__fig.add_subplot(111)
        self.__axe2 = None
        self.canvas = FigureCanvas(self.__fig)
        self.canvas.setParent(self.main_frame)

        self.mpl_toolbar = VisuGraph.NavigationToolbar(self.canvas, self.main_frame)
        self.export_btn = QPushButton(tr("Copy data"))
        self.export_image_btn = QPushButton(tr("Copy image"))
        self.export_btn.clicked.connect(self.export_data_plot)
        self.export_image_btn.clicked.connect(self.export_image_plot)
        self.canvas.mpl_connect('motion_notify_event', self.__hover_curves)

        # graph toolbar
        hboxwidget = QWidget()
        hbox = QHBoxLayout(hboxwidget)
        hbox.setContentsMargins(QMargins(0,0,0,0))
        hbox.addWidget(self.mpl_toolbar)
        hbox.setAlignment(self.mpl_toolbar, Qt.AlignLeft | Qt.AlignVCenter)
        self.__labelXY = QLabel()
        hbox.addWidget(self.__labelXY)
        for w in [self.export_btn, self.export_image_btn]:
            hbox.addWidget(w)
            hbox.setAlignment(w, Qt.AlignRight | Qt.AlignVCenter)
        hboxwidget.setFixedHeight(22);

        # graph widget
        vbox = QVBoxLayout()
        vbox.addWidget(hboxwidget)
        vbox.addWidget(self.canvas)
        self.graph_placeholder.setLayout(vbox)

        # combo widget
        def create_combo_widget(label_text, combo):
            hboxwidget = QWidget()
            hbox = QHBoxLayout(hboxwidget)
            hbox.addWidget(QLabel(label_text))
            hbox.addWidget(combo)
            return hboxwidget
        vbox2 = QVBoxLayout()

        self.cbo_scenario = ComboWithValues()
        self.cbo_scenario.setEditable(True)
        vbox2.addWidget(create_combo_widget(tr("Scenario:"), self.cbo_scenario))
        self.cbo_model = ComboWithValues()
        self.cbo_model.setEditable(True)
        vbox2.addWidget(create_combo_widget(tr("model:"), self.cbo_model))
        self.cbo_types = ComboWithValues()
        self.cbo_types.setEditable(True)
        vbox2.addWidget(create_combo_widget(tr("object type:"), self.cbo_types))
        self.cbo_objects = ComboWithValues()
        self.cbo_objects.setEditable(True)
        vbox2.addWidget(create_combo_widget(tr("object name:"), self.cbo_objects))
        self.cbo_results = ComboWithValues()
        self.cbo_results.setEditable(True)
        vbox2.addWidget(create_combo_widget(tr("result:"), self.cbo_results))
        self.combo_placeholder.setLayout(vbox2)
        self.graph_title.textChanged.connect(self.__on_graph_title_text_changed)
        self.left_axis_title.textChanged.connect(self.__on_left_title_text_changed)
        self.right_axis_title.textChanged.connect(self.__on_right_title_text_changed)