# coding=utf-8

import sip
sip.setapi(u'QDate', 2)
sip.setapi(u'QDateTime', 2)
sip.setapi(u'QString', 2)
sip.setapi(u'QTextStream', 2)
sip.setapi(u'QTime', 2)
sip.setapi(u'QUrl', 2)
sip.setapi(u'QVariant', 2)

from PyQt4.QtGui import QApplication
from PyQt4.QtCore import QCoreApplication, QTranslator
from hydra.gui.config_manager import ConfigManager
from hydra.project import Project

import sys
app = QApplication(sys.argv)
translator = QTranslator()
translator.load("i18n/fr", "../..")
QCoreApplication.installTranslator(translator)
guitranslator = QTranslator()
guitranslator.load("/usr/share/qt4/translations/qt_fr")
QCoreApplication.installTranslator(guitranslator)

project = Project.load_project("config_test")
new_config_manager = ConfigManager(project)
new_config_manager.exec_()


