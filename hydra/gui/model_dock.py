# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
import os.path
import importlib
import json
from functools import partial
from qgis.gui import QgsCollapsibleGroupBox
from qgis.core import QGis, QgsPoint
from PyQt4.QtCore import Qt, QCoreApplication
from PyQt4.QtGui import QApplication, QMessageBox, QToolButton, QVBoxLayout, QHBoxLayout, QGridLayout, QSpacerItem, QScrollArea, QWidget, QDockWidget, QIcon, QSizePolicy
import hydra.utility.map_point_tool as map_point_tool
from hydra.utility.map_point_tool import create_polygon, create_line, create_segment, create_point, Snapper, VisibleGeometry, MapPointTool
from hydra.utility.tables_properties import TablesProperties
from hydra.utility.settings_properties import SettingsProperties
from hydra.database.mesh_interface import MeshInterface

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

_plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
_layer_icons_dir = os.path.join(_plugin_dir, "ressources", "layer_icons")
_icons_dir = os.path.join(_plugin_dir, "ressources", "images")

class ModelDock(QDockWidget):

    def __get_snapper(self, table, oriented=False):
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()
        snapper = Snapper(snap*map_per_pix)
        sql_subrequest = list()
        if not oriented and 'snap' in self.__properties[table]:
            snap_rules = self.__properties[table]['snap']
        elif oriented == 'start' and 'snap_start' in self.__properties[table]:
            snap_rules = self.__properties[table]['snap_start']
        elif oriented == 'end' and 'snap_end' in self.__properties[table]:
            snap_rules = self.__properties[table]['snap_end']
        nodes = snap_rules['points']
        contours = snap_rules['contours']
        lines = snap_rules['lines']
        for node in nodes:
            sql_subrequest.append("""
                select (st_dumppoints(geom)).geom as geom
                from {model}.{table}""".format(
                model=self.__currentproject.get_current_model().name,
                table=node))
        for contour in contours:
            sql_subrequest.append("""
                select (st_dumppoints(contour)).geom as geom
                from {model}.{table}""".format(
                model=self.__currentproject.get_current_model().name,
                table=contour))
        if len(nodes)>0 or len(contours)>0:
            sql_request = """
                with points as ({})
                select st_x(geom), st_y(geom) from points
                """.format(" union ".join(sql_subrequest))
            for x, y in self.__currentproject.execute(sql_request).fetchall():
                snapper.add_point((x,y))
        for line in lines:
            for id, x1, y1, x2, y2 in self.__currentproject.execute("""
                    with
                    dumps as (select id, st_dumppoints(geom) as pt from {model}.{table}),
                    pts as (select id, (pt).geom, (pt).path[1] as vert from dumps)
                    select a.id, st_x(a.geom), st_y(a.geom), st_x(b.geom),st_y(b.geom)
                    from pts a, pts b
                    where a.id = b.id and a.vert = b.vert-1 and b.vert > 1;
                    """.format(
                    model=self.__currentproject.get_current_model().name,
                    table=line)).fetchall():
                snapper.add_segment(((x1,y1), (x2,y2)), id)
        return snapper

    def __init__(self, title, current_project, iface, refresh_layers_fct):
        QDockWidget.__init__(self, title)

        self.refresh_layers = refresh_layers_fct
        self.__currentproject = current_project
        self.__log_manager = current_project.log
        self.__iface = iface
        self.__modeldockclicked = False
        self.__properties = TablesProperties.get_properties()
        self.__mesh_interface = MeshInterface(self.__iface.mapCanvas(), self.__log_manager, self.__currentproject, self.refresh_layers)

        # project box
        pj_box = QgsCollapsibleGroupBox(tr("Project"))
        pj_box.setLayout(QGridLayout())

        pj_box.layout().addWidget(self.__button("dry_inflow_sector", action=self.__dry_inflow_sector),0,0)
        pj_box.layout().addWidget(self.__button("rain_gage"),0,1)
        pj_box.layout().addWidget(self.__button("wind_gage"),0,2)

        # hydrology box
        hyd_box = QgsCollapsibleGroupBox(tr("Hydrology"))
        hyd_box.setLayout(QGridLayout())

        hyd_box.layout().addWidget(self.__button("catchment"),0,0)
        hyd_box.layout().addWidget(self.__button("manhole_hydrology_node"),1,0)
        hyd_box.layout().addWidget(self.__button("routing_hydrology_link", action=self.__routing_hydrology_link),2,0)
        hyd_box.layout().addWidget(self.__button("qq_split_hydrology_singularity", action=self.__qq_split_hydrology_singularity),3,0)
        hyd_box.layout().addWidget(self.__button("reservoir_rs_hydrology_singularity", action=self.__reservoir_rs_hydrology_singularity),4,0)

        hyd_box.layout().addWidget(self.__button("catchment_node"),0,1)
        hyd_box.layout().addWidget(self.__button("hydrology_bc_singularity"),1,1)
        hyd_box.layout().addWidget(self.__button("connector_hydrology_link", action=self.__connector_hydrology_link),2,1)
        hyd_box.layout().addWidget(self.__button("zq_split_hydrology_singularity", action=self.__zq_split_hydrology_singularity),3,1)
        hyd_box.layout().addWidget(self.__button("reservoir_rsp_hydrology_singularity", action=self.__reservoir_rsp_hydrology_singularity),4,1)

        # structures and headloss box
        hl_box = QgsCollapsibleGroupBox(tr("Structures and headlosses"))
        hl_box.setLayout(QGridLayout())

        hl_box.layout().addWidget(self.__button("gate_singularity"),0,0)
        hl_box.layout().addWidget(self.__button("zregul_weir_singularity"),0,1)
        hl_box.layout().addWidget(self.__button("borda_headloss_singularity"),0,2)
        hl_box.layout().addWidget(self.__button("param_headloss_singularity"),0,3)
        hl_box.layout().addWidget(self.__button("bradley_headloss_singularity"),0,4)
        hl_box.layout().addWidget(self.__button("bridge_headloss_singularity"),0,5)

        hl_box.layout().addWidget(self.__button("regul_sluice_gate_singularity", action=self.__regul_gate_singularity),1,0)
        hl_box.layout().addWidget(self.__button("hydraulic_cut_singularity"),1,1)
        hl_box.layout().addWidget(self.__button("marker_singularity"),1,2)


        # boundary conditions box
        bc_box = QgsCollapsibleGroupBox(tr("Boundary conditions"))
        bc_box.setLayout(QGridLayout())

        bc_box.layout().addWidget(self.__button("hydrograph_bc_singularity"),0,0)
        bc_box.layout().addWidget(self.__button("constant_inflow_bc_singularity"),0,1)
        bc_box.layout().addWidget(self.__button("model_connect_bc_singularity"),0,2)

        bc_box.layout().addWidget(self.__button("froude_bc_singularity"),1,0)
        bc_box.layout().addWidget(self.__button("strickler_bc_singularity"),1,1)
        bc_box.layout().addWidget(self.__button("zq_bc_singularity"),1,2)

        bc_box.layout().addWidget(self.__button("tank_bc_singularity"),2,0)
        bc_box.layout().addWidget(self.__button("weir_bc_singularity"),2,1)
        bc_box.layout().addWidget(self.__button("tz_bc_singularity"),2,2)

        # river reach box
        rr_box = QgsCollapsibleGroupBox(tr("River and free surface flow"))
        rr_box.setLayout(QGridLayout())

        rr_box.layout().addWidget(self.__button("river_cross_section_profile"),0,0)
        rr_box.layout().addWidget(self.__button("river_node", action=self.__river_node),0,1)
        rr_box.layout().addWidget(self.__button("reach", action=self.__river_reach),0,2)
        rr_box.layout().addWidget(self.__button("crossroad_node"),0,3)
        rr_box.layout().addWidget(self.__button("street_link"),0,4)
        rr_box.layout().addWidget(self.__button("street", action=self.__street),0,5)

        rr_box.layout().addWidget(self.__button(None, [tr("Create support polyline"), os.path.join(_icons_dir, "constraint.svg")], self.__mesh_interface.create_constrain),1,0)
        rr_box.layout().addWidget(self.__button(None, [tr("Create mesh"), os.path.join(_icons_dir, "mesh.svg")], self.__mesh_interface.create_mesh),1,1)
        rr_box.layout().addWidget(self.__button(None, [tr("Delete mesh"), os.path.join(_icons_dir, "delete_mesh.svg")], self.__mesh_interface.delete_mesh),1,2)
        rr_box.layout().addWidget(self.__button(None, [tr("Create links"), os.path.join(_icons_dir, "constrain_links.svg")], self.__mesh_interface.create_links),1,3)
        rr_box.layout().addWidget(self.__button(None, [tr("Regen ALL mesh"), os.path.join(_icons_dir, "mesh_regen.svg")], self.__mesh_interface.mesh_regen),1,4)
        rr_box.layout().addWidget(self.__button(None, [tr("Mesh unmeshed 2D domains"), os.path.join(_icons_dir, "mesh_unmeshed.svg")], self.__mesh_interface.mesh_unmeshed),1,5)

        rr_box.layout().addWidget(self.__button(None, [tr("Manage 2D domains"), os.path.join(_icons_dir, "create_domain.svg")], self.__manage_domains),2,0)
        rr_box.layout().addWidget(self.__button("domain_2d_marker"),2,1)
        rr_box.layout().addWidget(self.__button("storage_node"),2,2)
        rr_box.layout().addWidget(self.__button("coverage_marker"),2,3)
        # rr_box.layout().addWidget(self.__button("elem_2d_node"),2,4)

        rr_box.layout().addWidget(self.__button(None, [tr("Merge reachs"), os.path.join(_icons_dir, "fuse_reach.svg")], self.__fuse_reachs),3,0)
        rr_box.layout().addWidget(self.__button(None, [tr("Split reach"), os.path.join(_icons_dir, "split_reach.svg")], self.__split_reach),3,1)
        rr_box.layout().addWidget(self.__button(None, [tr("Split constrain"), os.path.join(_icons_dir, "split_constrain.svg")], self.__split_constrain),3,2)

        # wastewater branch box
        ww_box = QgsCollapsibleGroupBox(tr("Network"))
        ww_box.setLayout(QGridLayout())

        ww_box.layout().addWidget(self.__button("manhole_node"),0,0)
        ww_box.layout().addWidget(self.__button("pipe_link"),0,1)
        ww_box.layout().addWidget(self.__button("pipe_branch_marker_singularity"),0,2)
        ww_box.layout().addWidget(self.__button(None, [tr("Create network overflows"), os.path.join(_icons_dir, "network_overflow_gen.svg")], self.__mesh_interface.create_network_overflow_links),1,0)
        ww_box.layout().addWidget(self.__button(None, [tr("Insert node on pipe"), os.path.join(_icons_dir, "node_add.svg")], self.__insert_node),1,1)
        ww_box.layout().addWidget(self.__button(None, [tr("Delete node and merge pipes"), os.path.join(_icons_dir, "node_delete.svg")], self.__delete_node),1,2)

        # station box
        stn_box = QgsCollapsibleGroupBox(tr("Station"))
        stn_box.setLayout(QGridLayout())

        stn_box.layout().addWidget(self.__button("station_node"),0,0)
        stn_box.layout().addWidget(self.__button("station"),0,1)

        # link box
        link_box = QgsCollapsibleGroupBox(tr("Links"))
        link_box.setLayout(QGridLayout())

        link_box.layout().addWidget(self.__button("gate_link"),0,0)
        link_box.layout().addWidget(self.__button("regul_gate_link", action=self.__regul_gate_link),0,1)
        link_box.layout().addWidget(self.__button("weir_link"),0,2)
        link_box.layout().addWidget(self.__button("pump_link"),0,3)
        link_box.layout().addWidget(self.__button("deriv_pump_link"),0,4)
        link_box.layout().addWidget(self.__button("borda_headloss_link"),0,5)

        link_box.layout().addWidget(self.__button("connector_link"),1,0)
        link_box.layout().addWidget(self.__button("strickler_link"),1,1)
        link_box.layout().addWidget(self.__button("porous_link"),1,2)
        link_box.layout().addWidget(self.__button("overflow_link"),1,3)
        link_box.layout().addWidget(self.__button("network_overflow_link"),1,4)
        # link_box.layout().addWidget(self.__button("mesh_2d_link"),1,5)

        link_box.layout().addWidget(self.__button(None, [tr("Invert link orientation"), os.path.join(_icons_dir, "link_reverse.svg")], self.__reverse_link),2,0)

        # dock widget
        widget = QWidget()
        gridlayout = QGridLayout()
        gridlayout.addWidget(pj_box,0,0,2,4)
        gridlayout.addWidget(hyd_box,0,4,6,3)
        gridlayout.addWidget(bc_box,2,0,4,4)
        gridlayout.addWidget(hl_box,6,0,3,7)
        gridlayout.addWidget(link_box,9,0,3,7)
        gridlayout.addWidget(ww_box,12,0,3,4)
        gridlayout.addWidget(stn_box,12,4,2,3)
        gridlayout.addWidget(rr_box,16,0,4,7)

        vlayout = QVBoxLayout()
        vlayout.addLayout(gridlayout)
        vlayout.addItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))
        vlayout.addStretch()

        widget.setLayout(QHBoxLayout())
        widget.layout().addLayout(vlayout)
        widget.layout().addItem(QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))
        widget.layout().addStretch()

        scroll_area = QScrollArea()
        scroll_area.setWidget(widget)
        scroll_area.setWidgetResizable(True)
        self.setWidget(scroll_area)

    def __button(self, table=None, properties=None, action="auto"):
        # properties = [text, icon]
        assert not (table and properties)
        if table:
            properties = [tr(self.__properties[table]["name"]), os.path.join(_layer_icons_dir, self.__properties[table]["icon"])]
        button = QToolButton()
        button.setToolTip(properties[0])
        button.setIcon(QIcon(properties[1]))
        if action is None:
            button.setEnabled(False)
        else:
            if action == "auto":
                button.clicked.connect(partial(self.__build_object, table))
            else:
                button.clicked.connect(action)
        return button

    def __build_object(self, table):
        module = importlib.import_module("hydra.gui.forms." + self.__properties[table]['file'])
        form_class = getattr(module, self.__properties[table]['class'])
        create_func = getattr(map_point_tool, 'create_' + self.__properties[table]['type'])
        if 'snap' in self.__properties[table]:
            snapper = self.__get_snapper(table) if self.__properties[table]['snap'] else None
            geom = create_func(self.__iface.mapCanvas(),
                self.__log_manager, self.__currentproject.terrain(), snapper)
        else:
            if 'snap_start' in self.__properties[table] and 'snap_end' in self.__properties[table]:
                start_snapper = self.__get_snapper(table, 'start')
                end_snapper = self.__get_snapper(table, 'end')
                geom = create_func(self.__iface.mapCanvas(),
                        self.__log_manager, self.__currentproject.terrain(), start_snapper, end_snapper)
        if geom:
            if 'snap' in self.__properties[table]:
                if 'lines' in self.__properties[table]['snap'] and 'points' in self.__properties[table]['snap']:
                    if "reach" in self.__properties[table]['snap']["lines"] and "river_node" in self.__properties[table]['snap']["points"]:
                        self.__auto_node_on_reach(geom)

            cur_cfg = self.__currentproject.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(1)
            form_class(self.__currentproject, geom).exec_()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(cur_cfg)

            self.refresh_layers([self.__properties[table]['name'],
                                self.__properties['coverage']['name']])

    def __auto_node_on_reach(self, point):
        node_exists, = self.__currentproject.execute("""
                                                    select exists(
                                                        select 1 from {m}._node where ST_DWithin(geom, {g}, 0.1)
                                                        )""".format(
                                                    m=self.__currentproject.get_current_model().name,
                                                    g=self.__currentproject.get_current_model().make_point(point)
                                                    )).fetchone()
        reach_exists, = self.__currentproject.execute("""
                                                    select exists(
                                                        select 1 from {m}.reach where ST_DWithin(geom, {g}, 0.1)
                                                        )""".format(
                                                    m=self.__currentproject.get_current_model().name,
                                                    g=self.__currentproject.get_current_model().make_point(point)
                                                    )).fetchone()
        if not node_exists and reach_exists:
            self.__currentproject.get_current_model().add_river_node(point)

    def __river_node(self):
        from hydra.gui.forms.river_node import RiverNodeEditor
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()
        snapper = Snapper(snap*map_per_pix)
        for id, x1, y1, x2, y2 in self.__currentproject.execute("""
                with
                dumps as (
                  select id, st_dumppoints(geom) as pt from {model}.reach
                ),
                pts as (
                  select id, (pt).geom, (pt).path[1] as vert from dumps
                )
                select a.id, st_x(a.geom), st_y(a.geom), st_x(b.geom),st_y(b.geom)
                from pts a, pts b
                where a.id = b.id and a.vert = b.vert-1 and b.vert > 1;
                """.format(model=self.__currentproject.get_current_model().name)).fetchall():
            snapper.add_segment(((x1,y1), (x2,y2)), id)

        point = create_point(self.__iface.mapCanvas(), self.__log_manager, self.__currentproject.terrain(), snapper)
        if point:
            cur_cfg = self.__currentproject.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(1)
            RiverNodeEditor(self.__currentproject, point).exec_()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(cur_cfg)
            self.refresh_layers(self.__properties['river_node']['name'])

    def __river_reach(self):
        from hydra.gui.forms.river_reach import RiverReachEditor
        snapper = self.__get_snapper('reach')
        line = create_line(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(), snapper)
        if line:
            if self.__currentproject.get_current_model().try_merge_reach(line):
                self.__currentproject.commit()
            else:
                RiverReachEditor(self.__currentproject, line).exec_()
            self.refresh_layers((self.__properties['reach']['name'],
                                    self.__properties['river_node']['name'],
                                    self.__properties['coverage']['name'],
                                    self.__properties['constrain']['name']))

    def __street(self):
        from hydra.gui.forms.street import StreetEditor
        snapper = self.__get_snapper('street')
        line = create_line(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(), snapper)
        if line:
            StreetEditor(self.__currentproject, line).exec_()
            self.refresh_layers((self.__properties['street']['name'],
                                    self.__properties['crossroad_node']['name'],
                                    self.__properties['street_link']['name'],
                                    self.__properties['coverage']['name'],
                                    self.__properties['constrain']['name']))

    def __river_cross_section_pl1d(self):
        from .dialog import Dialog
        from .model_constrain import ModelConstrainWidget
        snapper = self.__get_snapper('constrain')

        line = create_line(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(), snapper)
        if line:
            constrain = ModelConstrainWidget(self.__currentproject, line)
            constrain.typeComboBox.setCurrentIndex(constrain.typeComboBox.findText(tr("flood_plain_transect")))
            constrain.save()
            self.__currentproject.commit()
            constrain.close()
            self.refresh_layers(self.__properties['constrain']['name'])

    def __routing_hydrology_link(self):
        from hydra.gui.forms.routing_hydrology_link import RoutingHydrologyLinkEditor
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()

        start_snapper = self.__get_snapper('routing_hydrology_link', 'start')
        end_snapper = Snapper(snap*map_per_pix)
        for x, y in self.__currentproject.execute("""
                with points as (
                        with id_node as (
                                select node
                                from {model}.hydrograph_bc_singularity
                                )
                        select (st_dumppoints(geom)).geom as geom
                        from {model}.manhole_hydrology_node
                        union
                        select (st_dumppoints(geom)).geom as geom
                        from {model}.station_node
                        union
                        select (st_dumppoints(geom)).geom as geom
                        from {model}.manhole_node, id_node
                        where id = id_node.node
                        union
                        select (st_dumppoints(geom)).geom as geom
                        from {model}.river_node, id_node
                        where id = id_node.node
                        )
                select st_x(geom), st_y(geom) from points
                """.format(model=self.__currentproject.get_current_model().name)).fetchall():
            end_snapper.add_point((x,y))

        line = create_segment(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(), start_snapper, end_snapper)
        if line:
            cur_cfg = self.__currentproject.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(1)
            RoutingHydrologyLinkEditor(self.__currentproject, line).exec_()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(cur_cfg)
            self.refresh_layers(self.__properties['routing_hydrology_link']['name'])

    def __connector_hydrology_link(self):
        from hydra.gui.forms.connector_hydrology_link import ConnectorHydrologyLinkEditor
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()

        start_snapper = self.__get_snapper('connector_hydrology_link', 'start')
        end_snapper = Snapper(snap*map_per_pix)
        for x, y in self.__currentproject.execute("""
                with points as (
                        with id_node as (
                                select node
                                from {model}.hydrograph_bc_singularity
                                )
                        select (st_dumppoints(geom)).geom as geom
                        from {model}.manhole_hydrology_node
                        union
                        select (st_dumppoints(geom)).geom as geom
                        from {model}.station_node
                        union
                        select (st_dumppoints(geom)).geom as geom
                        from {model}.manhole_node, id_node
                        where id = id_node.node
                        union
                        select (st_dumppoints(geom)).geom as geom
                        from {model}.river_node, id_node
                        where id = id_node.node
                        )
                select st_x(geom), st_y(geom) from points
                """.format(model=self.__currentproject.get_current_model().name)).fetchall():
            end_snapper.add_point((x,y))

        line = create_segment(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(), start_snapper, end_snapper)
        if line:
            cur_cfg = self.__currentproject.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(1)
            ConnectorHydrologyLinkEditor(self.__currentproject, line).exec_()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(cur_cfg)
            self.refresh_layers(self.__properties['connector_hydrology_link']['name'])

    def __regul_gate_link(self):
        from hydra.gui.forms.regul_gate_link import RegulGateLinkEditor
        snapper = self.__get_snapper('regul_gate_link')
        line = create_segment(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(), snapper)
        if line:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__currentproject.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(1)
            RegulGateLinkEditor(self.__currentproject, line, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(cur_cfg)
            self.refresh_layers(self.__properties['regul_gate_link']['name'])

    def __regul_gate_singularity(self):
        from hydra.gui.forms.regul_gate_singularity import RegulGateSingularityEditor
        snapper = self.__get_snapper('regul_sluice_gate_singularity')
        point = create_point(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(), snapper)
        if point:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__currentproject.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(1)
            RegulGateSingularityEditor(self.__currentproject, point, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(cur_cfg)
            self.refresh_layers(self.__properties['regul_sluice_gate_singularity']['name'])

    def __qq_split_hydrology_singularity(self):
        from hydra.gui.forms.qq_split_hydrology_singularity import QqSplitHydrologySingularityEditor
        snapper = self.__get_snapper('qq_split_hydrology_singularity')
        point = create_point(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(), snapper)
        if point:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__currentproject.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(1)
            QqSplitHydrologySingularityEditor(self.__currentproject, point, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(cur_cfg)
            self.refresh_layers(self.__properties['qq_split_hydrology_singularity']['name'])

    def __zq_split_hydrology_singularity(self):
        from hydra.gui.forms.zq_split_hydrology_singularity import ZqSplitHydrologySingularityEditor
        snapper = self.__get_snapper('zq_split_hydrology_singularity')
        point = create_point(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(), snapper)
        if point:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__currentproject.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(1)
            ZqSplitHydrologySingularityEditor(self.__currentproject, point, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(cur_cfg)
            self.refresh_layers(self.__properties['zq_split_hydrology_singularity']['name'])

    def __reservoir_rs_hydrology_singularity(self):
        from hydra.gui.forms.reservoir_rs_hydrology_singularity import ReservoirRsHydrologySingularityEditor
        snapper = self.__get_snapper('reservoir_rs_hydrology_singularity')
        point = create_point(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(), snapper)
        if point:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__currentproject.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(1)
            ReservoirRsHydrologySingularityEditor(self.__currentproject, point, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(cur_cfg)
            self.refresh_layers(self.__properties['reservoir_rs_hydrology_singularity']['name'])

    def __reservoir_rsp_hydrology_singularity(self):
        from hydra.gui.forms.reservoir_rsp_hydrology_singularity import ReservoirRspHydrologySingularityEditor
        snapper = self.__get_snapper('reservoir_rsp_hydrology_singularity')
        point = create_point(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(), snapper)
        if point:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__currentproject.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(1)
            ReservoirRspHydrologySingularityEditor(self.__currentproject, point, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__currentproject.get_current_model().set_current_configuration(cur_cfg)
            self.refresh_layers(self.__properties['reservoir_rsp_hydrology_singularity']['name'])

    def __dry_inflow_sector(self):
        from hydra.gui.forms.dry_inflow_sector import DryInflowSectorEditor
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()
        snapper = Snapper(snap*map_per_pix)
        snapper.add_points(self.__currentproject.execute("""
                with points as (
                        select (st_dumppoints(geom)).geom as geom
                        from project.dry_inflow_sector
                        )
                select st_x(geom), st_y(geom) from points
                """).fetchall())

        polygon = create_polygon(self.__iface.mapCanvas(),
            self.__log_manager, self.__currentproject.terrain(),snapper)
        if polygon:
            DryInflowSectorEditor(self.__currentproject, polygon).exec_()
            self.refresh_layers(self.__properties['dry_inflow_sector']['name'])

    def __manage_domains(self):
        from hydra.gui.domain_manager import DomainManager
        DomainManager(self.__currentproject).exec_()

    def __insert_node(self):
        from hydra.gui.forms.manhole_node import ManholeNodeEditor
        from hydra.gui.forms.manhole_hydrology_node import ManholeHydrologyNodeEditor
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()
        snapper = Snapper(snap*map_per_pix)
        for id, x1, y1, x2, y2 in self.__currentproject.execute("""
                with
                dumps as (
                  select id, st_dumppoints(geom) as pt from {model}.pipe_link
                ),
                pts as (
                  select id, (pt).geom, (pt).path[1] as vert from dumps
                )
                select a.id, st_x(a.geom), st_y(a.geom), st_x(b.geom),st_y(b.geom)
                from pts a, pts b
                where a.id = b.id and a.vert = b.vert-1 and b.vert > 1;
                """.format(model=self.__currentproject.get_current_model().name)).fetchall():
            snapper.add_segment(((x1,y1), (x2,y2)), id)
        point = create_point(self.__iface.mapCanvas(), self.__log_manager, self.__currentproject.terrain(), snapper)
        if point:
            id_node, type_node = self.__currentproject.get_current_model().insert_node_on_pipe(point)
            if id_node and type_node=='manhole':
                ManholeNodeEditor(self.__currentproject, None, id_node).exec_()
                self.refresh_layers([self.__properties['manhole_node']['name'], self.__properties['pipe_link']['name']])
            elif id_node and type_node=='manhole_hydrology':
                ManholeHydrologyNodeEditor(self.__currentproject, None, id_node).exec_()
                self.refresh_layers([self.__properties['manhole_hydrology_node']['name'], self.__properties['pipe_link']['name']])
            self.__currentproject.commit()

    def __fuse_reachs(self):
        from hydra.gui.forms.river_reach import RiverReachEditor
        srid = self.__currentproject.srid
        pu = create_point(self.__iface.mapCanvas(), self.__log_manager, self.__currentproject.terrain())
        pd = create_point(self.__iface.mapCanvas(), self.__log_manager, self.__currentproject.terrain())

        # see if selection is 2 reaches:
        precision = SettingsProperties.get_snap()
        reach1_id = self.__currentproject.execute("""
                select id
                from {model}.reach
                where ST_DWithin(geom, {point}, {prec})
                """.format(
                    model=self.__currentproject.get_current_model().name,
                    srid=srid,
                    point=self.__currentproject.get_current_model().make_point(pu),
                    prec=precision
                    )).fetchone()
        reach2_id = self.__currentproject.execute("""
                select id
                from {model}.reach
                where ST_DWithin(geom, {point}, {prec})
                """.format(
                    model=self.__currentproject.get_current_model().name,
                    srid=srid,
                    point=self.__currentproject.get_current_model().make_point(pd),
                    prec=precision
                    )).fetchone()
        if reach1_id is not None and reach2_id is not None:
            new_reach_id = self.__currentproject.get_current_model().fuse_reach(reach1_id[0], reach2_id[0])
            RiverReachEditor(self.__currentproject, None, new_reach_id).exec_()
            self.__currentproject.commit()
            self.refresh_layers()

    def __split_reach(self):
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()
        snapper = Snapper(snap*map_per_pix)
        for id, x1, y1, x2, y2 in self.__currentproject.execute("""
                with
                dumps as (
                  select id, ST_Dumppoints(geom) as pt from {model}.reach
                ),
                pts as (
                  select id, (pt).geom, (pt).path[1] as vert from dumps
                )
                select a.id, ST_X(a.geom), ST_Y(a.geom), ST_X(b.geom),ST_Y(b.geom)
                from pts a, pts b
                where a.id = b.id and a.vert = b.vert-1 and b.vert > 1;
                """.format(model=self.__currentproject.get_current_model().name)).fetchall():
            snapper.add_segment(((x1,y1), (x2,y2)), id)
        point = create_point(self.__iface.mapCanvas(), self.__log_manager, self.__currentproject.terrain(), snapper)
        if point:
            id, pk = self.__currentproject.execute("""select id, ST_LineLocatePoint(reach.geom, {p})
                                                    from {m}.reach
                                                    where ST_Intersects(ST_Buffer({p},0.1), reach.geom)""".format(
                                                    p=self.__currentproject.get_current_model().make_point(point),
                                                    m=self.__currentproject.get_current_model().name)).fetchone()
            self.__currentproject.get_current_model().split_reach(id, pk)
            self.__currentproject.commit()
            self.refresh_layers()

    def __split_constrain(self):
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()
        snapper = Snapper(snap*map_per_pix)
        sql_subrequest = list()
        for id, x1, y1, x2, y2 in self.__currentproject.execute("""
                with
                dumps as (
                  select id, ST_Dumppoints(geom) as pt from {model}.constrain
                ),
                pts as (
                  select id, (pt).geom, (pt).path[1] as vert from dumps
                )
                select a.id, ST_X(a.geom), ST_Y(a.geom), ST_X(b.geom),ST_Y(b.geom)
                from pts a, pts b
                where a.id = b.id and a.vert = b.vert-1 and b.vert > 1;
                """.format(model=self.__currentproject.get_current_model().name)).fetchall():
            snapper.add_segment(((x1,y1), (x2,y2)), id)
        point = create_point(self.__iface.mapCanvas(), self.__log_manager, self.__currentproject.terrain(), snapper)
        if point:
            id, = self.__currentproject.execute("""select id
                                                   from {m}.constrain
                                                   where ST_Intersects(ST_Buffer({p},0.1), constrain.geom)""".format(
                                                   p=self.__currentproject.get_current_model().make_point(point),
                                                   m=self.__currentproject.get_current_model().name)).fetchone()
            self.__currentproject.execute("""select {m}.split_constrain({i}, {p});""".format(
                                             m=self.__currentproject.get_current_model().name,
                                             i=id,
                                             p=self.__currentproject.get_current_model().make_point(point)))
            self.__currentproject.commit()
            self.refresh_layers()

    def __delete_node(self):
        from hydra.gui.select_menu import SelectTool
        from hydra.gui.forms.pipe_link import PipeLinkEditor

        def on_left_click(point):
            geom = VisibleGeometry(self.__iface.mapCanvas(), QGis.Point)
            geom.add_point(point)
            snap = SettingsProperties.get_snap()
            size = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()*snap
            tool = SelectTool(self.__currentproject, point, size, subset=[(self.__currentproject.get_current_model().name, 'manhole_node'), (self.__currentproject.get_current_model().name, 'manhole_hydrology_node')])
            if tool.table is None:
                return
            pipe_up = [i for i, in self.__currentproject.execute("""select id
                                            from {}.pipe_link
                                            where up={}""".format(self.__currentproject.get_current_model().name, tool.id)).fetchall()]
            pipe_down = [i for i, in self.__currentproject.execute("""select id
                                            from {}.pipe_link
                                            where down={}""".format(self.__currentproject.get_current_model().name, tool.id)).fetchall()]
            if len(pipe_up)==1 and len(pipe_down)==1:
                reply = QMessageBox.question(self, tr('Confirm'),
                    tr("Are you sure you want to delete this item?"), QMessageBox.Yes |
                    QMessageBox.No, QMessageBox.No)
                if reply == QMessageBox.Yes:
                    id_pipe = self.__currentproject.get_current_model().delete_node_update_pipe(tool.id, tool.table)
                    PipeLinkEditor(self.__currentproject, None, id_pipe).exec_()
                    self.__currentproject.commit()
                    self.refresh_layers()

        def on_right_click(point):
            self.__iface.mapCanvas().setMapTool(self.prev_tool)

        self.__current_tool = MapPointTool(self.__iface.mapCanvas(), self.__log_manager)
        self.__current_tool.leftClicked.connect(on_left_click)
        self.__current_tool.rightClicked.connect(on_right_click)
        self.__iface.mapCanvas().setMapTool(self.__current_tool)

        while self.__iface.mapCanvas().mapTool() == self.__current_tool:
            QApplication.instance().processEvents()

        if self.__current_tool:
            self.__iface.mapCanvas().unsetMapTool(self.__current_tool)
            self.__current_tool=None

    def __reverse_link(self):
        from hydra.gui.select_menu import SelectTool

        def on_left_click(point):
            geom = VisibleGeometry(self.__iface.mapCanvas(), QGis.Point)
            geom.add_point(point)
            snap = SettingsProperties.get_snap()
            size = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()*snap
            subset = [(self.__currentproject.get_current_model().name, i+'_link') for i, in self.__currentproject.execute("""select name from hydra.link_type""").fetchall()]
            tool = SelectTool(self.__currentproject, point, size, subset=subset)
            if tool.table is None:
                return
            self.__currentproject.get_current_model().reverse_link(tool.table, tool.id)
            self.__currentproject.commit()
            self.refresh_layers()

        def on_right_click(point):
            self.__iface.mapCanvas().setMapTool(self.prev_tool)

        self.__current_tool = MapPointTool(self.__iface.mapCanvas(), self.__log_manager)
        self.__current_tool.leftClicked.connect(on_left_click)
        self.__current_tool.rightClicked.connect(on_right_click)
        self.__iface.mapCanvas().setMapTool(self.__current_tool)

        while self.__iface.mapCanvas().mapTool() == self.__current_tool:
            QApplication.instance().processEvents()

        if self.__current_tool:
            self.__iface.mapCanvas().unsetMapTool(self.__current_tool)
            self.__current_tool=None

    def __del__(self):
        for button in self.findChildren(QToolButton):
            try: button.clicked.disconnect()
            except Exception: pass
        self.__iface.removeDockWidget(self)