# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
import os
from operator import itemgetter
from PyQt4 import uic
from PyQt4.QtGui import QWidget, QGridLayout
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.gui.base_dialog import tr

class ComputSettingsWidget(QWidget):

    def __init__(self, current_project, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "comput_settings.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)

        self.project = current_project
        self.__current_id_scn = None
        self.scenario_rstart = ComboWithValues(self.scenario_rstart_placeholder)
        self.flag_rstart.toggled.connect(self.flag_rstart_changed)
        self.flag_rest.toggled.connect(self.flag_rstart_changed)
        self.scenario_ref = ComboWithValues(self.scenario_ref_placeholder)
        self.scenario_ref_btn.stateChanged.connect(self.scenario_ref_statechanged)
        self.radio_comput_2.setEnabled(False) # Disable "Hydraulics" option, because hydrology selection from other scenario not implemented

        self.show()

    def set(self, id_scn):
        if id_scn:
            comput_mode, date0, tfin_hr, dt_hydrol_mn, dtmin_hydrau_hr, dtmax_hydrau_hr, dzmin_hydrau, \
            dzmax_hydrau, flag_save, tsave_hr, tini_hydrau_hr, dt_output_hr, scenario_ref,\
            tbegin_output_hr, tend_output_hr, flag_rstart, scenario_rstart, trstart_hr, flag_gfx_control = self.project.execute("""
            select comput_mode, date0, tfin_hr, dt_hydrol_mn, dtmin_hydrau_hr, dtmax_hydrau_hr, dzmin_hydrau,
                dzmax_hydrau, flag_save, tsave_hr, tini_hydrau_hr, dt_output_hr, scenario_ref,
                tbegin_output_hr, tend_output_hr, flag_rstart, scenario_rstart, trstart_hr, flag_gfx_control
            from project.scenario
            where id={} limit 1""".format(str(id_scn))).fetchone()

            self.flag_rstart.setChecked(flag_rstart)
            self.flag_rest.setChecked(not flag_rstart)
            self.flag_gfx_control.setChecked(flag_gfx_control)

            scn_id_name = self.project.get_scenarios()

            self.scenario_rstart.clear()
            self.scenario_rstart.addItem("None", None)
            for scn in sorted(scn_id_name, key=itemgetter(1)):
                if not float(scn[0])==float(id_scn):
                    self.scenario_rstart.addItem(scn[1], int(scn[0]))
            self.scenario_rstart.set_selected_value(scenario_rstart)

            self.scenario_ref_btn.setChecked(scenario_ref is not None)

            self.scenario_ref.clear()
            self.scenario_ref.addItem("None", None)
            for scn in sorted(scn_id_name, key=itemgetter(1)):
                if not float(scn[0])==float(id_scn):
                    self.scenario_ref.addItem(scn[1], int(scn[0]))
            self.scenario_ref.set_selected_value(scenario_ref)

            self.date0.setDate(date0)
            self.date0.setTime(date0.time())
            self.tfin_hr.setText(str(tfin_hr))
            self.dt_hydrol_mn.setText(str(dt_hydrol_mn))
            self.dtmin_hydrau_hr.setValue(dtmin_hydrau_hr)
            self.dtmax_hydrau_hr.setValue(dtmax_hydrau_hr)
            self.dzmin_hydrau.setValue(dzmin_hydrau)
            self.dzmax_hydrau.setValue(dzmax_hydrau)
            self.flag_save.setChecked(flag_save)
            self.tsave_hr.setText(str(tsave_hr))
            self.trstart_hr.setText(str(trstart_hr))
            self.tini_hydrau_hr.setText(str(tini_hydrau_hr))
            self.dt_output_hr.setText(str(dt_output_hr))
            self.tbegin_output_hr.setText(str(tbegin_output_hr))
            self.tend_output_hr.setText(str(tend_output_hr))

            self.set_comput_mode(comput_mode)
            self.flag_rstart_changed()
            self.scenario_ref_statechanged()
            self.__current_id_scn = id_scn

    def save(self):
        if self.__current_id_scn:
            if self.radio_comput_1.isChecked():
                comput_mode = "hydrology"
            elif self.radio_comput_2.isChecked():
                comput_mode = "hydraulics"
            elif self.radio_comput_3.isChecked():
                comput_mode = "hydrology_and_hydraulics"

            self.project.execute("""
            update project.scenario set
                comput_mode='{}',
                date0='{}', tfin_hr='{}', dt_hydrol_mn='{}', dtmin_hydrau_hr='{}', dtmax_hydrau_hr='{}',
                dzmin_hydrau='{}', dzmax_hydrau='{}', flag_save={}, tsave_hr='{}',
                tini_hydrau_hr='{}', dt_output_hr='{}', tbegin_output_hr='{}', tend_output_hr='{}',
                flag_rstart='{}', scenario_rstart={}, trstart_hr='{}', flag_gfx_control='{}', scenario_ref={}
            where id={}""".format(comput_mode, str(self.date0.dateTime().toString("yyyy-MM-dd hh:mm:ss")),str(self.tfin_hr.text()), \
                str(self.dt_hydrol_mn.text()), str(self.dtmin_hydrau_hr.value()),str(self.dtmax_hydrau_hr.value()), \
                str(self.dzmin_hydrau.value()),str(self.dzmax_hydrau.value()),self.flag_save.isChecked(),str(self.tsave_hr.text()), \
                str(self.tini_hydrau_hr.text()),str(self.dt_output_hr.text()),str(self.tbegin_output_hr.text()), str(self.tend_output_hr.text()),
                self.flag_rstart.isChecked(),
                self.scenario_rstart.get_selected_value() if self.scenario_rstart.get_selected_value() is not None else 'null',
                str(self.trstart_hr.text()),
                self.flag_gfx_control.isChecked(),
                self.scenario_ref.get_selected_value() if (self.scenario_ref_btn.isChecked() and self.scenario_ref.get_selected_value() is not None) else 'null',
                str(self.__current_id_scn)))

    def set_comput_mode(self, comput_mode):
        if comput_mode=="hydrology":
            self.radio_comput_1.setChecked(True)
        elif comput_mode=="hydraulics":
            self.radio_comput_2.setChecked(True)
        elif comput_mode=="hydrology_and_hydraulics":
            self.radio_comput_3.setChecked(True)
        else:
            raise "Unknown comput mode"

    def flag_rstart_changed(self):
        self.scenario_rstart.setEnabled(self.flag_rstart.isChecked())
        self.trstart_hr.setEnabled(self.flag_rstart.isChecked())
        if not self.flag_rstart.isChecked():
            self.scenario_rstart.set_selected_value(None)

    def scenario_ref_statechanged(self):
        if self.scenario_ref_btn.isChecked():
            self.scenario_ref.setEnabled(True)
        elif not self.scenario_ref_btn.isChecked():
            self.scenario_ref.setEnabled(False)
            self.scenario_ref.set_selected_value(None)