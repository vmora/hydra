# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from PyQt4.QtGui import QFileDialog, QWidget, QGridLayout, QTableWidgetItem, QComboBox
from PyQt4 import uic
from hydra.gui.base_dialog import tr
from hydra.utility.tables_properties import TablesProperties
from PyQt4 import QtGui, QtCore
import os

class ComputOptionsWidget(QWidget):
    def __init__(self, current_project, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "comput_options.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)
        self.__current_id = None
        self.__properties = TablesProperties.get_properties()
        self.__output_family_order = ["bc", "link", "singularity"]
        self.__output_family = {
            "link": {
                "title": tr("Links"),
                "tables": [ "borda_headloss_link",
                            "connector_link",
                            "fuse_spillway_link",
                            "connector_hydrology_link",
                            "routing_hydrology_link",
                            "gate_link",
                            "regul_gate_link",
                            "deriv_pump_link",
                            "mesh_2d_link",
                            "network_overflow_link",
                            "overflow_link",
                            "porous_link",
                            "pump_link",
                            "strickler_link",
                            "street_link",
                            "weir_link"]
            },
            "singularity": {
                "title": tr("Singularities"),
                "tables": [ "borda_headloss_singularity",
                            "bradley_headloss_singularity",
                            "marker_singularity",
                            "bridge_headloss_singularity",
                            "hydraulic_cut_singularity",
                            "param_headloss_singularity",
                            "pipe_branch_marker_singularity",
                            "gate_singularity",
                            "regul_sluice_gate_singularity",
                            "zregul_weir_singularity" ]
            },
            "bc": {
                "title": tr("Boundary conditions"),
                "tables": [ "constant_inflow_bc_singularity",
                            "froude_bc_singularity",
                            "hydrograph_bc_singularity",
                            "hydrology_bc_singularity",
                            "model_connect_bc_singularity",
                            "zq_bc_singularity" ,
                            "strickler_bc_singularity",
                            "tank_bc_singularity",
                            "tz_bc_singularity",
                            "weir_bc_singularity"]
            }
        }
        self.project = current_project
        self.select_option_file_path.clicked.connect(self.__select_file)
        self.btnEdit.clicked.connect(self.__edit_file)
        self.btnAffinAdd.clicked.connect(self.__affin_add)
        self.btnAffinRemove.clicked.connect(self.__affin_remove)
        self.btnStricklerAdd.clicked.connect(self.__strickler_add)
        self.btnStricklerRemove.clicked.connect(self.__strickler_remove)
        self.btnOutputAdd.clicked.connect(self.__output_add)
        self.btnOutputRemove.clicked.connect(self.__output_remove)

        self.option_file.stateChanged.connect(self.__option_file_changed)
        self.__init_combos()
        self.cboAffinModel.activated.connect(self.__refresh_affin_elem_list)
        self.cboAffinElemType.activated.connect(self.__refresh_affin_elem_list)
        self.cboStricklerModel.activated.connect(self.__refresh_strickler_elem_list)
        self.cboStricklerElemType.activated.connect(self.__refresh_strickler_elem_list)
        self.cboOutputModel.activated.connect(self.__refresh_output_elem_list)
        self.cboOutputElemFamily.activated.connect(self.__refresh_output_elem_type)
        self.cboOutputElemType.activated.connect(self.__refresh_output_elem_list)
        self.show()

    def __select_file(self):
        title = tr('Select options file')
        filter = tr('All file (*.*)')
        file_url = QFileDialog.getOpenFileName(self, title, self.project.get_project_dir(), filter)
        if file_url:
            self.option_file_path.setText(file_url)

    def __edit_file(self):
        file = self.option_file_path.text()
        os.system(file)

    def __option_file_changed(self):
        self.option_file_path.setEnabled(self.option_file.isChecked())
        self.select_option_file_path.setEnabled(self.option_file.isChecked())

    def __init_combos(self):
        self.cboAffinElemType.clear()
        self.cboAffinElemType.addItem(self.type_domain_to_str('branch'))
        self.cboAffinElemType.addItem(self.type_domain_to_str('domain_2d'))
        self.cboAffinElemType.addItem(self.type_domain_to_str('reach'))
        self.cboAffinElemType.setCurrentIndex(0)

        self.cboStricklerElemType.clear()
        self.cboStricklerElemType.addItem(self.type_domain_to_str('branch'))
        self.cboStricklerElemType.addItem(self.type_domain_to_str('domain_2d'))
        self.cboStricklerElemType.addItem(self.type_domain_to_str('reach'))
        self.cboStricklerElemType.setCurrentIndex(0)

        self.cboAffinModel.clear()
        self.cboOutputModel.clear()
        self.cboStricklerModel.clear()
        models = self.project.get_models()
        for model in models:
            self.cboAffinModel.addItem(model)
            self.cboOutputModel.addItem(model)
            self.cboStricklerModel.addItem(model)
        self.cboAffinModel.setCurrentIndex(0)
        self.cboOutputModel.setCurrentIndex(0)
        self.cboStricklerModel.setCurrentIndex(0)

        self.cboOutputElemFamily.clear()
        for family in self.__output_family_order:
            self.cboOutputElemFamily.addItem(self.__output_family[family]['title'])

    def type_domain_to_str(self,type):
        if type=='branch':
            return tr('Branch')
        if type=='reach':
            return tr('Reach')
        if type=='domain_2d':
            return tr('Domain 2D')

    def str_to_type_domain(self,text):
        if text==tr('Branch'):
            return 'branch'
        if text==tr('Reach'):
            return 'reach'
        if text==tr('Domain 2D'):
            return 'domain_2d'

    def set(self, id_scn):
        self.__current_id = id_scn
        option_file, brut_option_file_path, c_affin_param, t_affin_start_hr, t_affin_min_surf_ddomains, \
            output_option, strickler_param = self.project.execute("""
            select option_file, option_file_path, c_affin_param, t_affin_start_hr, t_affin_min_surf_ddomains,
            output_option, strickler_param
            from project.scenario
            where id={} limit 1;""".format(self.__current_id)).fetchone()

        option_file_path = self.project.unpack_path(brut_option_file_path)

        # affin option
        self.c_affin_param.setChecked(c_affin_param)
        self.t_affin_start_hr.setText(str(t_affin_start_hr))
        self.t_affin_min_surf_ddomains.setText(str(t_affin_min_surf_ddomains))
        self.refresh_affin_table()

        # affin option
        self.strickler_param.setChecked(strickler_param)
        self.refresh_strickler_table()

        # output option
        self.__refresh_output_elem_type()
        self.refresh_output_table()
        self.ouput_option.setChecked(output_option)

        # external file
        self.option_file_path.setText(option_file_path)
        self.option_file.setChecked(option_file)
        self.__option_file_changed()

    def save(self):
        if self.__current_id:
            brut_option_file_path = self.project.pack_path(self.option_file_path.text())
            self.project.execute("""
                update project.scenario
                set option_file='{}', option_file_path='{}',
                c_affin_param='{}', t_affin_start_hr='{}', t_affin_min_surf_ddomains='{}',
                output_option='{}', strickler_param='{}'
                where id={}""".format(
                    str(self.option_file.isChecked()), brut_option_file_path,
                    str(self.c_affin_param.isChecked()), str(self.t_affin_start_hr.text()), str(self.t_affin_min_surf_ddomains.text()),
                    str(self.ouput_option.isChecked()),str(self.strickler_param.isChecked()),
                    self.__current_id))
            self.save_strickler_coef()

# AFFIN Options
    def refresh_affin_table(self):
        if self.__current_id is not None and self.cboAffinModel.currentIndex()!=-1:
            affin_list = self.project.execute("""
                select model, type_domain, element
                from project.c_affin_param
                where scenario={} order by model, type_domain, element;""".format(self.__current_id)).fetchall()

            self.affin_table.setRowCount(0)
            for item in affin_list:
                rowcount = self.affin_table.rowCount() + 1
                self.affin_table.setRowCount(rowcount)
                row = rowcount - 1
                self.affin_table.setItem(row, 0, QTableWidgetItem(str(item[0])))
                self.affin_table.setItem(row, 1, QTableWidgetItem(self.type_domain_to_str(item[1])))
                self.affin_table.setItem(row, 2, QTableWidgetItem(str(item[2])))

            self.affin_table.resizeColumnsToContents()
            self.affin_table.horizontalHeader().setStretchLastSection(True)
            self.__refresh_affin_elem_list()

    def __refresh_affin_elem_list(self):
        if self.__current_id is not None and self.cboAffinModel.currentIndex()!=-1:
            model = str(self.cboAffinModel.currentText())
            type_domain=self.str_to_type_domain(str(self.cboAffinElemType.currentText()))
            elements = self.project.execute("""select name from {model}.{table}
            where name not in (
                select element from project.c_affin_param where scenario={scn} and model='{model}' and type_domain='{type}'
            ) order by name;""".format(model=model, table=type_domain, scn=self.__current_id, type=type_domain)).fetchall()
            self.cboAffinElem.clear()
            for item in elements:
                self.cboAffinElem.addItem(item[0])
            self.cboAffinElem.setCurrentIndex(0)

    def __affin_add(self):
        if self.cboAffinModel.currentIndex()==-1:
            return
        if self.cboAffinElemType.currentIndex()==-1:
            return
        if self.cboAffinElem.currentIndex()==-1:
            return

        model=str(self.cboAffinModel.currentText())
        type_domain=self.str_to_type_domain(str(self.cboAffinElemType.currentText()))
        element=str(self.cboAffinElem.currentText())

        self.project.execute("""insert into project.c_affin_param(scenario, model, type_domain, element)
            values({},'{}','{}','{}');""".format(self.__current_id, model, type_domain, element))
        self.refresh_affin_table()

    def __affin_remove(self):
        items = self.affin_table.selectedItems()
        if len(items)>0:
            selected_row = self.affin_table.row(items[0])
            model=self.affin_table.item(selected_row,0).text()
            type_domain=self.str_to_type_domain(self.affin_table.item(selected_row,1).text())
            element=self.affin_table.item(selected_row,2).text()

            self.project.execute("""delete from project.c_affin_param
                where scenario={} and model='{}' and type_domain='{}' and element='{}';""".format(self.__current_id,
                    model, type_domain, element))
            self.refresh_affin_table()

# Strickler Options
    def refresh_strickler_table(self):
        if self.__current_id is not None:
            item_list = self.project.execute("""
                select model, type_domain, element, rkmin, rkmaj, pk1, pk2
                from project.strickler_param
                where scenario={} order by model, type_domain, element;""".format(self.__current_id)).fetchall()

            self.strickler_table.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
            self.strickler_table.setEditTriggers(QtGui.QAbstractItemView.AllEditTriggers)

            self.strickler_table.setRowCount(0)
            for item in item_list:
                rowcount = self.strickler_table.rowCount() + 1
                self.strickler_table.setRowCount(rowcount)
                row = rowcount - 1

                self.strickler_table.setItem(row, 0, QTableWidgetItem(str(item[0])))
                self.strickler_table.setItem(row, 1, QTableWidgetItem(self.type_domain_to_str(item[1])))
                self.strickler_table.setItem(row, 2, QTableWidgetItem(str(item[2])))
                self.strickler_table.item(row,0).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.strickler_table.item(row,1).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.strickler_table.item(row,2).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

                for i in range(0,4):
                    self.strickler_table.setItem(row, 3+i, QTableWidgetItem(str(item[3+i])))

            self.strickler_table.resizeColumnsToContents()
            self.strickler_table.horizontalHeader().setStretchLastSection(True)
            self.__refresh_strickler_elem_list()

    def save_strickler_coef(self):
        for irow in range(0,self.strickler_table.rowCount()):
            model=self.strickler_table.item(irow,0).text()
            type_domain=self.str_to_type_domain(self.strickler_table.item(irow,1).text())
            element=self.strickler_table.item(irow,2).text()
            rkmin=self.strickler_table.item(irow,3).text()
            rkmaj=self.strickler_table.item(irow,4).text()
            pk1=self.strickler_table.item(irow,5).text()
            pk2=self.strickler_table.item(irow,6).text()
            self.project.execute("""update project.strickler_param set rkmin={}, rkmaj={}, pk1={}, pk2={}
            where scenario={} and model='{}' and type_domain='{}' and element='{}';""".format(rkmin, rkmaj, pk1, pk2,
            self.__current_id, model, type_domain, element))

    def __refresh_strickler_elem_list(self):
        if self.__current_id is not None and self.cboStricklerModel.currentIndex()!=-1:
            model = str(self.cboStricklerModel.currentText())
            type_domain=self.str_to_type_domain(str(self.cboStricklerElemType.currentText()))
            elements = self.project.execute("""select name from {model}.{table}
            where name not in (
                select element from project.strickler_param where scenario={scn} and model='{model}' and type_domain='{type}'
            ) order by name;""".format(model=model, table=type_domain, scn=self.__current_id, type=type_domain)).fetchall()
            self.cboStricklerElem.clear()
            for item in elements:
                self.cboStricklerElem.addItem(item[0])
            self.cboStricklerElem.setCurrentIndex(0)

    def __strickler_add(self):
        if self.cboStricklerModel.currentIndex()==-1:
            return
        if self.cboStricklerElemType.currentIndex()==-1:
            return
        if self.cboStricklerElem.currentIndex()==-1:
            return

        model=str(self.cboStricklerModel.currentText())
        type_domain=self.str_to_type_domain(str(self.cboStricklerElemType.currentText()))
        element=str(self.cboStricklerElem.currentText())

        self.project.execute("""insert into project.strickler_param(scenario, model, type_domain, element)
            values({},'{}','{}','{}');""".format(self.__current_id, model, type_domain, element))
        self.refresh_strickler_table()

    def __strickler_remove(self):
        items = self.strickler_table.selectedItems()
        if len(items)>0:
            selected_row = self.strickler_table.row(items[0])
            model=self.strickler_table.item(selected_row,0).text()
            type_domain=self.str_to_type_domain(self.strickler_table.item(selected_row,1).text())
            element=self.strickler_table.item(selected_row,2).text()

            self.project.execute("""delete from project.strickler_param
                where scenario={} and model='{}' and type_domain='{}' and element='{}';""".format(self.__current_id,
                    model, type_domain, element))
            self.refresh_strickler_table()

# OUTPUT Options
    def __refresh_output_elem_type(self):
        elem_family = self.__find_family(str(self.cboOutputElemFamily.currentText()))
        self.cboOutputElemType.clear()
        for table in self.__output_family[elem_family]['tables']:
            self.cboOutputElemType.addItem(self.__properties[table]['name'])
        self.__refresh_output_elem_list()

    def __refresh_output_elem_list(self):
        if self.__current_id is not None and self.cboOutputModel.currentIndex()!=-1:
            model = str(self.cboOutputModel.currentText())
            type_elem = self.__find_table_name(str(self.cboOutputElemType.currentText()))
            elements = self.project.execute("""select name from {model}.{table}
            where name not in (
                select element from project.output_option where scenario={scn} and model='{model}' and type_element='{table}'
            ) order by name;""".format(model=model, table=type_elem, scn=self.__current_id)).fetchall()
            self.cboOutputElem.clear()
            for item in elements:
                self.cboOutputElem.addItem(item[0])
            self.cboOutputElem.setCurrentIndex(0)

    def __find_family(self, text):
        for family in self.__output_family:
            if self.__output_family[family]['title']==text:
                return family
        raise "family not found"

    def __find_table_name(self, text):
        for table in self.__properties:
            if self.__properties[table]['name']==text:
                return table
        raise "table not found"

    def refresh_output_table(self):
        if self.__current_id is not None and self.cboOutputModel.currentIndex()!=-1:
            output_list = self.project.execute("""
                select model, type_element, element
                from project.output_option
                where scenario={} order by model, type_element, element;""".format(self.__current_id)).fetchall()

            self.output_table.setRowCount(0)
            for item in output_list:
                rowcount = self.output_table.rowCount() + 1
                self.output_table.setRowCount(rowcount)
                row = rowcount - 1
                self.output_table.setItem(row, 0, QTableWidgetItem(str(item[0])))
                self.output_table.setItem(row, 1, QTableWidgetItem(self.__properties[str(item[1])]['name']))
                self.output_table.setItem(row, 2, QTableWidgetItem(str(item[2])))

            self.output_table.resizeColumnsToContents()
            self.output_table.horizontalHeader().setStretchLastSection(True)
            self.__refresh_output_elem_list()

    def __output_add(self):
        if self.cboOutputModel.currentIndex()==-1:
            return
        if self.cboOutputElemType.currentIndex()==-1:
            return
        if self.cboOutputElem.currentIndex()==-1:
            return

        model = str(self.cboOutputModel.currentText())
        type_elem = self.__find_table_name(str(self.cboOutputElemType.currentText()))
        element=str(self.cboOutputElem.currentText())

        self.project.execute("""insert into project.output_option(scenario, model, type_element, element)
            values({},'{}','{}','{}');""".format(self.__current_id, model, type_elem, element))
        self.refresh_output_table()

    def __output_remove(self):
        items = self.output_table.selectedItems()
        if len(items)>0:
            selected_row = self.output_table.row(items[0])
            model=self.output_table.item(selected_row,0).text()
            type_elem=self.__find_table_name(self.output_table.item(selected_row,1).text())
            element=self.output_table.item(selected_row,2).text()

            self.project.execute("""delete from project.output_option
                where scenario={} and model='{}' and type_element='{}' and element='{}';""".format(self.__current_id,
                    model, type_elem, element))
            self.refresh_output_table()