# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
from PyQt4 import uic
from PyQt4.QtGui import QDialog, QMessageBox
from hydra.gui.base_dialog import BaseDialog, tr

class RadarRainDialog(QDialog):
    def __init__(self, parent = None, name='', template=12*'?'):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "radar_rain.ui"), self)
        self.buttonBox.accepted.connect(self.save)

        self.name.setText(name)
        self.template_.setText(template)

        self.__results = [name, template]

    def get_result(self):
        return self.__results

    def save(self):
        if 12*'?' in self.template_.text() and self.name.text() !='':
            self.__results = [self.name.text(), self.template_.text()]
            self.close()
        else:
            QMessageBox.critical(self, tr('Error'), tr("""Please enter a name and a template containing exactly 12 consecutive ''?''"""), QMessageBox.Ok)
