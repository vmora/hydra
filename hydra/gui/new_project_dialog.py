# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from PyQt4.QtCore import QCoreApplication
from PyQt4.QtGui import QDialog, QMessageBox, QFileDialog
from PyQt4 import uic
from qgis.gui import QgsGenericProjectionSelector
import os
from ..utility.string import normalized_name
from ..utility.settings_properties import SettingsProperties

_hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

class NewProjectDialog(QDialog):
    def __init__(self, parent=None, project_name='', srid='', working_dir=_hydra_dir):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "new_project_dialog.ui"), self)
        self.info.text=''
        self.txt_working_dir.setText(os.path.join(os.path.expanduser("~"), ".hydra"))
        self.buttonBox.accepted.connect(self.__save)
        self.project_name.textChanged.connect(self.__project_name_changed)
        self.__project_name=project_name
        self.project_name.setText(project_name)
        self.srid.setText(str(srid))
        if SettingsProperties.local():
            self.txt_working_dir.setText(str(working_dir))
        else:
            self.txt_working_dir.setText("/$user/.hydra")
            self.txt_working_dir.setEnabled(False)
        self.btn_workdir.clicked.connect(self.select_dir)
        self.btn_epsg.clicked.connect(self.select_epsg)

    def select_dir(self):
        title = tr('Select working dir path:')
        dir_name = QFileDialog.getExistingDirectory(None, title, self.txt_working_dir.text())
        if dir_name:
            self.txt_working_dir.setText(dir_name)

    def __project_name_changed(self):
        project_name = unicode(self.project_name.text())
        new_project_name = normalized_name(project_name)
        self.info.setText("" if project_name==new_project_name else new_project_name)

    def __save(self):
        self.__project_name= normalized_name(unicode(self.project_name.text()))

    def get_result(self):
        return self.__project_name, int(self.srid.text()), self.txt_working_dir.text()

    def select_epsg(self):
        projSelector = QgsGenericProjectionSelector()
        projSelector.exec_()
        authId = projSelector.selectedAuthId()
        if authId[:5]=='EPSG:':
            self.srid.setText(authId[5:])
        else:
            QMessageBox(QMessageBox.Warning, tr('Not a valid SRID'), tr('Please select a SRID with code starting with "ESPG:..."'), QMessageBox.Ok).exec_()

