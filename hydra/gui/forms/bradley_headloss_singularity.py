# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_singularity_dialog import SimpleSingularityDialog, tr
import hydra.utility.string as string
from ..widgets.graph_widget import GraphWidget
from PyQt4 import uic
from PyQt4.QtGui import QTableWidgetItem
import os
import re

class BradleyHeadlossEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleSingularityDialog.__init__(self, project, geom, "bradley_headloss_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "bradley_headloss_singularity.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, d_abutment_l, d_abutment_r, abutment_type, zw_array, z_ceiling = self.project.execute("""
            select name, d_abutment_l, d_abutment_r, abutment_type, zw_array, z_ceiling
            from {}.bradley_headloss_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.d_abutment_l.setText(string.get_str(d_abutment_l))
        self.d_abutment_r.setText(string.get_str(d_abutment_r))
        self.z_ceiling.setText(string.get_str(z_ceiling))

        self.populate_combo_from_sqlenum(self.abutment_type, "abutment_type", abutment_type)

        self.graph = GraphWidget(self.graph_placeholder)

        if not zw_array is None:
            self.set_table_items(zw_array)
            self.__draw_graph()

        self.table.cellChanged.connect(self.__cell_changed)
        self.table.currentCellChanged.connect(self.__draw_graph)

    def __draw_graph(self):
        datas = self.get_table_items()
        if len(datas)>0:
            self.graph.clear()
            self.graph.add_filled_symetric_line(
                [r[1]/2 for r in datas],
                [r[0] for r in datas],
                "k",
                self.name.text(),
                tr("Z (m)"), tr("Piers width (m)"),
                bottom=True)
        else:
            self.graph.canvas.setVisible(False)

    def __cell_changed(self,row,column):
        if row == (self.table.rowCount()-1):
            self.table.setRowCount(self.table.rowCount() + 1)

    def set_table_items(self, array):
        n = len(array)
        self.table.setRowCount(n+1)
        for row in range(0, n):
            self.table.setItem(row,0,QTableWidgetItem(string.get_str(array[row][0])))
            self.table.setItem(row,1,QTableWidgetItem(string.get_str(array[row][1])))

    def get_table_items(self):
        n = self.table.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            if not self.table.item(i,0) == None and not self.table.item(i,1) == None:
                result.append(list())
                m = self.table.columnCount()
                result[index_list].append(string.get_sql_float(self.table.item(i,0).text()))
                result[index_list].append(string.get_sql_float(self.table.item(i,1).text()))
                index_list = index_list + 1
        return result

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        abutment_type = self.project.execute("""
            select name
            from hydra.abutment_type
            where description='{}'""".format(self.abutment_type.currentText().encode('utf-8'))).fetchone()[0]

        self.model.update_singularity_brd(self.id, self.geom, self.get_table_items(),
                                            string.get_sql_float(self.d_abutment_l.text()), string.get_sql_float(self.d_abutment_r.text()),
                                            abutment_type, string.get_sql_float(self.z_ceiling.text()), self.name.text())
        SimpleSingularityDialog.save(self)
        self.close()
