# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from PyQt4 import uic
from PyQt4.QtGui import QWidget, QGridLayout, QTableWidgetItem
import hydra.utility.string as string
from hydra.gui.geom_dialog import GeomDialog
from hydra.gui.simple_link_dialog import SimpleLinkDialog, tr
from hydra.gui.widgets.circular_cross_section_widget import CircularCrossSectionWidget
from hydra.gui.widgets.ovoid_cross_section_widget import OvoidCrossSectionWidget
from hydra.gui.widgets.pipe_cross_section_widget import PipeCrossSectionWidget
from hydra.gui.widgets.channel_cross_section_widget import ChannelCrossSectionWidget


class PipeLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "pipe_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "pipe_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.custom_length.toggled.connect(self.customchecked)

        #get pipe info
        name, z_invert_up, z_invert_down, h_sable, type_cs, rk, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom, length, custom_length, comment = self.project.execute("""
            select name, z_invert_up, z_invert_down, h_sable, cross_section_type, rk, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom, ST_Length(geom), custom_length, comment
            from {}.pipe_link where id={} """.format(self.model.name, str(self.id))).fetchone()

        self.name.setText(name)
        self.z_invert_up.setText(string.get_str(z_invert_up))
        self.z_invert_down.setText(string.get_str(z_invert_down))
        self.h_sable.setText(string.get_str(h_sable))
        self.__type_cs = type_cs
        self.__rk = rk
        self.__circular_diameter = circular_diameter
        self.__ovoid_height = ovoid_height
        self.__ovoid_top_diameter = ovoid_top_diameter
        self.__ovoid_invert_diameter = ovoid_invert_diameter
        self.cp_geom_id = cp_geom
        self.op_geom_id = op_geom
        self.comment.setText(comment)

        if custom_length is not None:
            self.length.setText(str(custom_length))
            self.custom_length.setChecked(True)
        else:
            self.length.setText("{0:.3f}".format(length))

        self.init_cross_section_widgets(parent)

        # cross section combo box building
        cs_id_name_abb = self.project.execute("""
                                        select id, name, abbreviation
                                        from hydra.cross_section_type
                                        where id<>1 and id<>2 """).fetchall()
        for i in range(0, len(cs_id_name_abb)):
            self.combo_cross_section.addItem(str(cs_id_name_abb[i][1]))
            if self.__type_cs is not None and cs_id_name_abb[i][1]==self.__type_cs:
                self.combo_cross_section.setCurrentIndex(i)
        if self.__type_cs is None:
            self.combo_cross_section.setCurrentIndex(-1)
        self.combo_cross_section.activated.connect(self.cross_section_changed)
        self.current_layout = None
        self.cross_section_changed()

        #set current geom
        if self.selected_cs == 'pipe':
            self.geometry_name.setText(self.cp_geom_name)
        elif self.selected_cs == 'channel':
            self.geometry_name.setText(self.op_geom_name)

        #tools for geometry selection
        self.create_new.clicked.connect(self.new_geom)
        self.edit_selected.clicked.connect(self.edit_geom)
        self.table_existing_geometry.itemClicked.connect(self.select_geom)

    def customchecked(self):
        if self.custom_length.isChecked():
            self.length.setEnabled(True)
        else:
            self.length.setEnabled(False)
            l, = self.project.execute("""select ST_Length(geom) from {}.pipe_link where id={} """.format(self.model.name, str(self.id))).fetchone()
            self.length.setText("{0:.3f}".format(l))

    def init_cross_section_widgets(self, parent):
        self.circular_cross_section = CircularCrossSectionWidget(parent, self.__circular_diameter, self.__rk)

        self.ovoid_cross_section = OvoidCrossSectionWidget(parent, self.__ovoid_top_diameter, self.__ovoid_invert_diameter, self.__ovoid_height, self.__rk)

        self.pipe_cross_section = PipeCrossSectionWidget(parent, self.__rk)
        if self.cp_geom_id is not None:
            self.cp_geom_name, = self.project.execute("""select name
                                        from {}.closed_parametric_geometry
                                        where id={}""".format(self.model.name, self.cp_geom_id)).fetchone()
        else:
            self.cp_geom_name = None

        self.channel_cross_section = ChannelCrossSectionWidget(parent, self.__rk)
        if self.op_geom_id is not None:
            self.op_geom_name, = self.project.execute("""select name
                                        from {}.open_parametric_geometry
                                        where id={}""".format(self.model.name, self.op_geom_id)).fetchone()
        else:
            self.op_geom_name = None

        self.empty_cross_section = QGridLayout()

    def cross_section_changed(self):
        if self.combo_cross_section.currentIndex() != -1:
            self.selected_cs = self.combo_cross_section.currentText()
            if self.selected_cs == 'circular':
                self.geom_inactive()
                self.reset_layout(self.circular_cross_section.get_layout())
            elif self.selected_cs == 'ovoid':
                self.geom_inactive()
                self.reset_layout(self.ovoid_cross_section.get_layout())
            elif self.selected_cs == 'pipe':
                self.geom_refresh()
                self.reset_layout(self.pipe_cross_section.get_layout())
            elif self.selected_cs == 'channel':
                self.geom_refresh()
                self.reset_layout(self.channel_cross_section.get_layout())
        else:
            self.selected_cs = None

    def reset_layout(self, new_layout):
        if self.current_layout is not None:
            QWidget(self).setLayout(self.current_layout)
        self.widget_cross_section.setLayout(new_layout)
        self.current_layout = new_layout

    def new_geom(self):
        if self.selected_cs == 'pipe':
            id = self.model.add_pipe_channel_section_geometry('closed_parametric_geometry', string.list_to_sql_array([[0,0]]))
            geom_dialog = GeomDialog(self.project, id, self.selected_cs)
        elif self.selected_cs == 'channel':
            id = self.model.add_pipe_channel_section_geometry('open_parametric_geometry', string.list_to_sql_array([[0,0]]))
            geom_dialog = GeomDialog(self.project, id, self.selected_cs)
        geom_dialog.exec_()
        if self.selected_cs == 'pipe':
            if geom_dialog.canceled:
                self.project.execute("""delete from {}.closed_parametric_geometry
                                        where id={}""".format(self.model.name, id))
            else:
                self.cp_geom_id = id
        elif self.selected_cs == 'channel':
            if geom_dialog.canceled:
                self.project.execute("""delete from {}.open_parametric_geometry
                                        where id={}""".format(self.model.name, id))
            else:
                self.op_geom_id = id
        self.geom_refresh()

    def edit_geom(self):
        if self.selected_cs == 'pipe' and self.cp_geom_id:
            GeomDialog(self.project, self.cp_geom_id, self.selected_cs).exec_()
        elif self.selected_cs == 'channel' and self.op_geom_id:
            GeomDialog(self.project, self.op_geom_id, self.selected_cs).exec_()
        self.geom_refresh()

    def select_geom(self):
        if self.table_existing_geometry.currentRow()>-1:
            if self.selected_cs == 'pipe':
                self.cp_geom_id = int(self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 0).text())
                self.cp_geom_name = self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 1).text()
            elif self.selected_cs == 'channel':
                self.op_geom_id = int(self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 0).text())
                self.op_geom_name = self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 1).text()
            self.geometry_name.setText(self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 1).text())

    def geom_refresh(self):
        self.tab.setTabEnabled(1, True)
        self.label_geom.setEnabled(True)
        self.geometry_name.setEnabled(True)
        self.geometry_name.clear()
        if self.selected_cs == 'pipe':
            geoms = self.project.execute(""" select id, name from {}.closed_parametric_geometry""".format(self.model.name)).fetchall()
            if self.cp_geom_id is not None:
                self.cp_geom_name, = self.project.execute("""select name
                                            from {}.closed_parametric_geometry
                                            where id={}""".format(self.model.name, self.cp_geom_id)).fetchone()
                self.geometry_name.setText(self.cp_geom_name)
        elif self.selected_cs == 'channel':
            geoms = self.project.execute(""" select id, name from {}.open_parametric_geometry""".format(self.model.name)).fetchall()
            if self.op_geom_id is not None:
                self.op_geom_name, = self.project.execute("""select name
                                            from {}.open_parametric_geometry
                                            where id={}""".format(self.model.name, self.op_geom_id)).fetchone()
                self.geometry_name.setText(self.op_geom_name)

        geoms_sorted = sorted(geoms, key=lambda id_name: id_name[0])

        self.table_existing_geometry.setRowCount(0)
        self.table_existing_geometry.setSortingEnabled(False)
        self.table_existing_geometry.setHorizontalHeaderLabels(['Id', 'Name'])
        for geom in geoms_sorted:
            row_position = self.table_existing_geometry.rowCount()
            self.table_existing_geometry.insertRow(row_position)
            self.table_existing_geometry.setItem(row_position, 0, QTableWidgetItem(str(geom[0])))
            self.table_existing_geometry.setItem(row_position, 1, QTableWidgetItem(str(geom[1])))
            if ((self.selected_cs == 'pipe' and geom[0] == self.cp_geom_id)
                or (self.selected_cs == 'channel' and geom[0] == self.op_geom_id)):
                self.table_existing_geometry.selectRow(row_position)
        self.table_existing_geometry.resizeColumnsToContents()
        self.table_existing_geometry.setSortingEnabled(True)

        self.create_new.setEnabled(True)
        self.edit_selected.setEnabled(True)

    def geom_inactive(self):
        self.tab.setTabEnabled(1, False)
        self.label_geom.setEnabled(False)
        self.geometry_name.setEnabled(False)
        self.geometry_name.clear()
        self.table_existing_geometry.clear()

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        ci_diameter, ci_rk = self.circular_cross_section.return_values()
        ov_top_diameter, ov_invert_diameter, ov_height, ov_rk = self.ovoid_cross_section.return_values()
        pi_rk = self.pipe_cross_section.return_values()
        ch_rk = self.channel_cross_section.return_values()

        # Dirty way to reunify RK from different widgets
        # todo : rethink CS widget to put RK in pipe form directly
        cs_type = self.combo_cross_section.currentText()
        rk = locals()[cs_type[:2]+'_rk']

        self.model.update_pipe_link(self.id, self.geom,
                            string.get_sql_float(self.z_invert_up.text()), string.get_sql_float(self.z_invert_down.text()),
                            self.combo_cross_section.currentText(), string.get_sql_float(self.h_sable.text()),
                            rk, ci_diameter, ov_height, ov_top_diameter, ov_invert_diameter, self.cp_geom_id, self.op_geom_id, self.name.text(),
                            self.comment.toPlainText().encode('utf-8'))

        # update custom_length
        if self.custom_length.isChecked():
            custom_length = self.length.text()
        else:
            custom_length = 'null'
        self.project.execute("""update {}.pipe_link
                                set custom_length = {}
                                where id={}""".format(self.model.name, custom_length, self.id))


        SimpleLinkDialog.save(self)
        self.close()

