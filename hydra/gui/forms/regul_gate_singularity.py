# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_singularity_dialog import SimpleSingularityDialog, tr
import hydra.utility.string as string
from ..widgets.array_widget import ArrayWidget
from hydra.utility.map_point_tool import create_point, Snapper
from hydra.utility.log import LogManager, ConsoleLogger
from PyQt4 import uic, QtGui
from PyQt4.QtGui import QTableWidgetItem
import os
import re

class RegulGateSingularityEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None, iface=None, size=10):
        SimpleSingularityDialog.__init__(self, project, geom, "regul_sluice_gate_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "regul_gate.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.__size = size
        self.__iface = iface
        self.__log_manager = LogManager(ConsoleLogger(), "Hydra")

        self.button_z_control_node.clicked.connect(self.__select_control_node)

        if iface==None:
            self.button_z_control_node.setEnabled(False)

        name, z_invert, z_ceiling, width, cc, action_gate_type, v_max_cms, \
            z_invert_stop, z_ceiling_stop, dt_regul_hr, mode_regul, \
            z_control_node_id, z_pid_array, z_tz_array, \
            q_z_crit, q_tq_array, nr_z_gate, full_section_discharge_for_headloss = self.project.execute("""
            select name, z_invert, z_ceiling, width, cc, action_gate_type, v_max_cms,
            z_invert_stop, z_ceiling_stop, dt_regul_hr, mode_regul,
            z_control_node, z_pid_array, z_tz_array,
            q_z_crit, q_tq_array,
            nr_z_gate, full_section_discharge_for_headloss
            from {}.regul_sluice_gate_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.dir = current_dir
        self.name.setText(name)
        self.z_invert.setText(string.get_str(z_invert))
        self.z_ceiling.setText(string.get_str(z_ceiling))
        self.width.setText(string.get_str(width))
        self.cc.setText(string.get_str(cc))
        self.v_max_cms.setText(string.get_str(v_max_cms))
        self.z_invert_stop.setText(string.get_str(z_invert_stop))
        self.z_ceiling_stop.setText(string.get_str(z_ceiling_stop))
        self.dt_regul_hr.setText(string.get_str(dt_regul_hr))
        self.q_z_crit.setText(string.get_str(q_z_crit))
        self.nr_z_gate.setText(string.get_str(nr_z_gate))

        # action gate type init
        self.upward_opening_gate.toggled.connect(self.__upward_opening_gate_clicked)
        self.downward_opening_gate.toggled.connect(self.__downward_opening_gate_clicked)
        if action_gate_type == 'upward_opening':
            self.upward_opening_gate.setChecked(True)
            self.picture.setPixmap(QtGui.QPixmap(os.path.join(current_dir, "gate_upward_opening.png")))
        elif action_gate_type == 'downward_opening':
            self.downward_opening_gate.setChecked(True)
            self.picture.setPixmap(QtGui.QPixmap(os.path.join(current_dir, "gate_downward_opening.png")))

        # regulation mode init
        self.zregul.toggled.connect(self.__zregul_clicked)
        self.qregul.toggled.connect(self.__qregul_clicked)
        self.noregul.toggled.connect(self.__noregul_clicked)
        if mode_regul == 'elevation':
            self.zregul.setChecked(True)
        elif mode_regul == 'discharge':
            self.qregul.setChecked(True)
        elif mode_regul == 'no_regulation':
            self.noregul.setChecked(True)

        # control node init
        if z_control_node_id:
            z_control_node, = self.project.execute("""select name from {}._node where id={}""".format(self.model.name, z_control_node_id)).fetchone()
            self.z_control_node.setText(z_control_node)

        # pid init
        if z_pid_array:
            self.z_pid_0.setText(string.get_str(z_pid_array[0]))
            self.z_pid_1.setText(string.get_str(z_pid_array[1]))
            self.z_pid_2.setText(string.get_str(z_pid_array[2]))

        self.z_array = ArrayWidget(["Time (h)", "Elevation (m)"], [10,2], z_tz_array, self.__draw_graph, self.tz_array_placeholder)
        self.q_array = ArrayWidget(["Time (h)", "Discharge (m3/s)"], [10,2], q_tq_array, self.__draw_graph, self.tq_array_placeholder)

        # discharge for headloss computation
        node_type, = self.project.execute("""select node_type from {}._singularity where id={}""".format(self.model.name, self.id)).fetchone()
        if node_type != 'river':
            self.groupbox_discharge_for_headloss.setEnabled(False)
        elif full_section_discharge_for_headloss is True:
            self.radioBtn_full_section.setChecked(True)
        elif full_section_discharge_for_headloss is False:
            self.radioBtn_river_bed.setChecked(True)

        self.show()

    def __draw_graph(self, datas):
        pass

    def __select_control_node(self):
        from hydra.utility.item_selector import ItemSelector

        i_selector = ItemSelector(self.project, self.__iface)
        self.hide()

        i_selector.select(['manhole_node', 'river_node', 'station_node'])
        name, id, table = i_selector.get_selected_item_infos()

        self.z_control_node.setText(name)
        self.stash()
        RegulGateSingularityEditor(self.project, None, self.id, None, self.__iface, self.__size).exec_()
        self.ignore_rollback = True
        self.close()

    def __upward_opening_gate_clicked(self, enabled):
        if enabled:
            self.picture.setPixmap(QtGui.QPixmap(os.path.join(self.dir, "gate_upward_opening.png")))

    def __downward_opening_gate_clicked(self, enabled):
        if enabled:
            self.picture.setPixmap(QtGui.QPixmap(os.path.join(self.dir, "gate_downward_opening.png")))

    def __zregul_clicked(self, enabled):
        if enabled:
            self.stacked_widget_regul.setCurrentIndex(0)

    def __qregul_clicked(self, enabled):
        if enabled:
            self.stacked_widget_regul.setCurrentIndex(1)

    def __noregul_clicked(self, enabled):
        if enabled:
            self.stacked_widget_regul.setCurrentIndex(2)

    def get_id(self):
        return self.id

    def stash(self):
        # action gate type
        if self.upward_opening_gate.isChecked():
            action_gate_type = 'upward_opening'
        elif self.downward_opening_gate.isChecked():
            action_gate_type = 'downward_opening'
        # regulation mode
        if self.zregul.isChecked():
            mode_regul = 'elevation'
        elif self.qregul.isChecked():
            mode_regul = 'discharge'
        elif self.noregul.isChecked():
            mode_regul = 'no_regulation'
        # control node
        z_control_node_id = None
        if self.z_control_node.text():
            id, = self.project.execute("""select id from {}._node where name='{}'""".format(self.model.name, self.z_control_node.text())).fetchone()
            z_control_node_id = int(id)
        # pid
        z_pid_array = [string.get_sql_float(self.z_pid_0.text()),
                       string.get_sql_float(self.z_pid_1.text()),
                       string.get_sql_float(self.z_pid_2.text())]

        self.model.update_singularity_acta(self.id, self.geom,
                                            string.get_sql_float(self.z_invert.text()), string.get_sql_float(self.z_ceiling.text()),
                                            string.get_sql_float(self.width.text()), string.get_sql_float(self.cc.text()), action_gate_type,
                                            string.get_sql_float(self.z_invert_stop.text()), string.get_sql_float(self.z_ceiling_stop.text()),
                                            string.get_sql_float(self.v_max_cms.text()), string.get_sql_float(self.dt_regul_hr.text()), mode_regul,
                                            z_control_node_id, z_pid_array, self.z_array.get_table_items(),
                                            string.get_sql_float(self.q_z_crit.text()), self.q_array.get_table_items(),
                                            string.get_sql_float(self.nr_z_gate.text()), not self.radioBtn_river_bed.isChecked(), self.name.text())

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.stash()
        SimpleSingularityDialog.save(self)
        self.close()






