# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_singularity_dialog import SimpleSingularityDialog, tr
import hydra.utility.string as string
from ..widgets.graph_widget import GraphWidget
from ..widgets.array_widget import ArrayWidget
from PyQt4 import uic
import os
import re

class ModelConnectBcEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleSingularityDialog.__init__(self, project, geom, "model_connect_bc_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "model_connect_bc.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.cascade_values = self.project.execute("""
            select description, name
            from hydra.model_connect_mode""").fetchall()

        name, cascade_mode, zq_array = self.project.execute("""
            select name, cascade_mode, zq_array
            from {}.model_connect_bc_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()
        self.name.setText(name)

        # initialize cascade mode
        self.populate_combo_from_sqlenum(self.cascade_mode, "model_connect_mode", cascade_mode)
        self.cascade_mode.activated.connect(self.__cascadechanged)
        self.__cascadechanged()

        self.graph = GraphWidget(self.graph_placeholder)
        self.array = ArrayWidget(["Elevation (m)", "Discharge (m3/s)"], [100,2], zq_array, self.__draw_graph, self.array_placeholder)

    def __draw_graph(self, datas):
        if len(datas)>0:
            self.graph.clear()
            self.graph.add_line(
                [r[0] for r in datas],
                [r[1] for r in datas],
                "r",
                self.name.text(),
                tr("Z (m)"), tr("Q (m3/s)"))
        else:
            self.graph.canvas.setVisible(False)

    def __cascadechanged(self):
        for value in self.cascade_values:
            corrected_value = value if value[0] is not None else [value[1],value[1]]
            if self.cascade_mode.currentIndex() != -1:
                if self.cascade_mode.currentText() == corrected_value[0]:
                    if corrected_value[1] == 'zq_downstream_condition':
                        self.graph_placeholder.setEnabled(True)
                        self.array_placeholder.setEnabled(True)
                    elif corrected_value[1] == 'hydrograph':
                        self.graph_placeholder.setEnabled(False)
                        self.array_placeholder.setEnabled(False)

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        cascade_mode = None
        for value in self.cascade_values:
            corrected_value = value if value[0] is not None else [value[1],value[1]]
            if self.cascade_mode.currentIndex() != -1:
                if self.cascade_mode.currentText() == corrected_value[0]:
                    cascade_mode = corrected_value[1]

        self.model.update_singularity_racc(self.id, self.geom, cascade_mode, self.array.get_table_items(), self.name.text())
        SimpleSingularityDialog.save(self)
        self.close()