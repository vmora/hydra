# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.forms.domain.froude_bc_test [-dh] [-g project_name model_name x y x y]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name model_name x y
        run in gui mode

"""

from __future__ import absolute_import # important to read the doc !
import sip
for cls in [u'QDate', u'QDateTime', u'QString', u'QTextStream', u'QTime', u'QUrl', u'QVariant']:
    sip.setapi(cls, 2)
from hydra.database.database import TestProject, remove_project, project_exists
from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt

from hydra.project import Project

def gui_mode(project_name, model_name, geom, id):

    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.river_cross_section_profile import RiverCrossSectionProfileEditor
    from hydra.utility.string import normalized_name, normalized_model_name

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = RiverCrossSectionProfileEditor(obj_project, geom, id)
    test_dialog.exec_()


def test():
    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.river_cross_section_profile import RiverCrossSectionProfileEditor
    from hydra.database.database import TestProject

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)

    obj_project.set_current_model(model_name)
    model = obj_project.get_current_model()

    id_reach = model.add_reachpath([[150,200],[200,200]])
    model.add_river_node([185,200])

    test_dialog = RiverCrossSectionProfileEditor(obj_project, [185,200])
    test_dialog.name.setText("Test_pt_link")
    test_dialog.z_invert_up.setText('120')
    test_dialog.z_invert_down.setText('100')
    test_dialog.checkBox_down.setChecked(True)
    test_dialog.refresh_widgets() #simulates checking checkbox
    test_dialog.down_cs_widget.ovoid_cross_section.top_diameter.setText('100')
    test_dialog.down_cs_widget.ovoid_cross_section.invert_diameter.setText('101')
    test_dialog.down_cs_widget.ovoid_cross_section.height.setText('102')
    test_dialog.down_cs_widget.ovoid_cross_section.rk.setText('103')
    test_dialog.down_cs_widget.pipe_cross_section.rk.setText('0.5')
    test_dialog.down_cs_widget.combo_cross_section.setCurrentIndex(test_dialog.down_cs_widget.combo_cross_section.findText('channel'))
    test_dialog.down_cs_widget.cross_section_changed() # simulates click on combo box
    test_dialog.down_cs_widget.channel_cross_section.rk.setText('0.55')
    id = test_dialog.get_id()
    test_dialog.save()

    test_dialog = RiverCrossSectionProfileEditor(obj_project, None, id)
    assert test_dialog.name.text()=="Test_pt_link"
    assert float(test_dialog.z_invert_up.text()) == 120
    assert float(test_dialog.z_invert_down.text()) ==100
    assert test_dialog.checkBox_down.isChecked()
    assert float(test_dialog.down_cs_widget.ovoid_cross_section.top_diameter.text()) == 100
    assert float(test_dialog.down_cs_widget.ovoid_cross_section.invert_diameter.text()) == 101
    assert float(test_dialog.down_cs_widget.ovoid_cross_section.height.text()) == 102
    assert float(test_dialog.down_cs_widget.ovoid_cross_section.rk.text()) == 0.55
    assert float(test_dialog.down_cs_widget.pipe_cross_section.rk.text()) == 0.55
    assert test_dialog.down_cs_widget.combo_cross_section.currentText() == 'channel'
    assert float(test_dialog.down_cs_widget.channel_cross_section.rk.text()) == 0.55
    test_dialog.close()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name)
    test()
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    project_name = normalized_name(project_name)
    model_name = normalized_model_name(model_name)
    gui_mode(project_name, model_name, geom, id)
    exit(0)

test()
print "ok"


