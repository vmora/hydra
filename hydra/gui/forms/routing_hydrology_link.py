# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
import hydra.utility.string as string
from PyQt4 import uic
import os
import re

class RoutingHydrologyLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "routing_hydrology_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "routing_hydrology_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, cross_section, length, slope = self.project.execute("""
            select name, cross_section, length, slope
            from {}.routing_hydrology_link
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.cross_section.setText(string.get_str(cross_section))
        self.length.setText(string.get_str(length))
        self.slope.setText(string.get_str(slope))

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.model.update_routing_hydrology_link(self.id, self.geom,
                                                    string.get_sql_float(self.cross_section.text()),string.get_sql_float(self.slope.text()),
                                                     string.get_sql_float(self.length.text()), self.name.text())
        SimpleLinkDialog.save(self)
        self.close()
