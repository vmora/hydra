# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
import hydra.utility.string as string
import hydra.utility.sql_json as sql_json
from PyQt4 import uic
import os
import re
import json

class CatchmentNodeEditor(BaseDialog):
    def __init__(self, project, geom, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "catchment_node.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()
        assert not self.__model == None
        self.__geom = geom
        if id==None:
            self.__id = self.__model.add_catchment_node(geom)
        else:
            self.__id = id
            if self.__geom is None:
                geom,= self.project.execute("""
                select geom
                from {}.catchment_node
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name, area_ha, rl, slope, c_imp, \
            netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef, \
            holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm, \
            scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, permeable_soil_j_mm, \
            permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange, \
            runoff_type, socose_tc_mn, socose_shape_param_beta, define_k_mn, \
            q_limit, q0, catchment_pollution_mode, catchment_pollution_param = self.project.execute("""
            select name, area_ha, rl, slope, c_imp,
            netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef,
            holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm,
            scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, permeable_soil_j_mm,
            permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange,
            runoff_type, socose_tc_mn, socose_shape_param_beta, define_k_mn,
            q_limit, q0, catchment_pollution_mode, catchment_pollution_param
            from {}.catchment_node
            where id={}""".format(self.__model.name, self.__id)).fetchone()

        contour_id, = self.project.execute("""select contour from {m}.catchment_node where id={i}""".format(m=self.__model.name, i=self.__id)).fetchone()


        self.name.setText(name)
        self.area_ha.setText(string.get_str(area_ha))
        self.rl.setText(string.get_str(rl))
        self.slope.setText(string.get_str(slope))
        self.c_imp.setText(string.get_str(c_imp))

        self.q0.setText(string.get_str(q0))
        self.q_limit.setText(string.get_str(q_limit))

        #initialize netflow
        self.netflow_types = self.populate_combo_from_sqlenum(self.netflow_type,
            "netflow_type", netflow_type)

        self.netflow_type.activated.connect(self.__netflowchanged)
        self.stacked_netflow.setShown(False)
        self.__netflowchanged()

        #initialize netflow parameters
        self.cr.setText(string.get_str(constant_runoff))
        self.horner_ini_loss_coef.setText(string.get_str(horner_ini_loss_coef))
        self.horner_recharge_coef.setText(string.get_str(horner_recharge_coef))
        self.holtan_sat_inf_rate_mmh.setText(string.get_str(holtan_sat_inf_rate_mmh))
        self.holtan_dry_inf_rate_mmh.setText(string.get_str(holtan_dry_inf_rate_mmh))
        self.holtan_soil_storage_cap_mm.setText(string.get_str(holtan_soil_storage_cap_mm))
        self.scs_j_mm.setText(string.get_str(scs_j_mm))
        self.scs_soil_drainage_time_day.setText(string.get_str(scs_soil_drainage_time_day))
        self.scs_rfu_mm.setText(string.get_str(scs_rfu_mm))
        self.permeable_soil_j_mm.setText(string.get_str(permeable_soil_j_mm))
        self.permeable_soil_rfu_mm.setText(string.get_str(permeable_soil_rfu_mm))
        self.permeable_soil_ground_max_inf_rate.setText(string.get_str(permeable_soil_ground_max_inf_rate))
        self.permeable_soil_ground_lag_time_day.setText(string.get_str(permeable_soil_ground_lag_time_day))
        self.permeable_soil_coef_river_exchange.setText(string.get_str(permeable_soil_coef_river_exchange))

        # initialize runoff
        self.linear_reservoir.toggled.connect(self.linear_reservoir_clicked)
        self.socose.toggled.connect(self.socose_clicked)

        self.runoff_type.setEnabled(False)
        self.socose_tc_mn.setEnabled(False)
        self.socose_shape_param_beta.setEnabled(False)

        self.populate_combo_from_sqlenum(self.runoff_type,
            "runoff_type", runoff_type, ["socose"])

        # set selection active on runoff type
        if runoff_type == 'socose':
            self.socose.setChecked(True)
        elif runoff_type is not None:
            self.linear_reservoir.setChecked(True)

        self.runoff_type.activated.connect(self.__runoffchanged)
        self.define_k_mn.setEnabled(False)
        self.__runoffchanged()

        #initialize runoff parameters
        self.define_k_mn.setText(string.get_str(define_k_mn))
        self.socose_tc_mn.setText(string.get_str(socose_tc_mn))
        self.socose_shape_param_beta.setText(string.get_str(socose_shape_param_beta))

        #init contour
        if contour_id:
            contour, =self.project.execute("""select catchment.name
                                        from {m}.catchment, {m}.catchment_node
                                        where catchment.id={i}""".format(m=self.__model.name, i=contour_id)).fetchone()
            self.contour_name.setText(contour)

    def __netflowchanged(self):
        stacked_widget_index = {"constant_runoff":0,
                                "horner":1,
                                "holtan":2,
                                "scs":3,
                                "permeable_soil":4}
        if self.netflow_type.currentIndex() != -1:
            for netflow in self.netflow_types:
                if netflow[0] == self.netflow_type.currentText():
                    self.stacked_netflow.setShown(True)
                    self.stacked_netflow.setCurrentIndex(stacked_widget_index[netflow[1]])

    def __runoffchanged(self):
        if self.runoff_type.currentIndex() != -1:
            if self.get_runoff_type() == 'define_k' and self.linear_reservoir.isChecked():
                self.define_k_mn.setEnabled(True)
            else:
                self.define_k_mn.setEnabled(False)

    def linear_reservoir_clicked(self, enabled):
        if enabled:
            self.runoff_type.setEnabled(True)
            self.__runoffchanged()
            self.socose_tc_mn.setEnabled(False)
            self.socose_shape_param_beta.setEnabled(False)

    def socose_clicked(self, enabled):
        if enabled:
            self.runoff_type.setEnabled(False)
            self.__runoffchanged()
            self.socose_tc_mn.setEnabled(True)
            self.socose_shape_param_beta.setEnabled(True)

    def get_id(self):
        return self.__id

    def get_runoff_type(self):
        return self.project.execute("""
                    select name
                    from hydra.runoff_type
                    where description='{}'""".format(self.runoff_type.currentText())).fetchone()[0]

    def save(self):
        self.project.log.notice(tr("Saved"))

        netflow_type = self.project.execute("""
            select name
            from hydra.netflow_type
            where description='{}'""".format(self.netflow_type.currentText())).fetchone()[0]

        runoff_type = None
        if self.socose.isChecked():
            runoff_type = 'socose'
        elif self.linear_reservoir.isChecked():
            runoff_type = self.get_runoff_type()

        self.__model.update_catchment_node(self.__id, self.__geom, self.name.text(),
                                                    string.get_sql_float(self.area_ha.text()), string.get_sql_float(self.rl.text()),
                                                    string.get_sql_float(self.slope.text()), string.get_sql_float(self.c_imp.text()),
                                                    netflow_type, string.get_sql_float(self.cr.text()), string.get_sql_float(self.horner_ini_loss_coef.text()), string.get_sql_float(self.horner_recharge_coef.text()),
                                                    string.get_sql_float(self.holtan_sat_inf_rate_mmh.text()), string.get_sql_float(self.holtan_dry_inf_rate_mmh.text()), string.get_sql_float(self.holtan_soil_storage_cap_mm.text()),
                                                    string.get_sql_float(self.scs_j_mm.text()), string.get_sql_float(self.scs_soil_drainage_time_day.text()), string.get_sql_float(self.scs_rfu_mm.text()), string.get_sql_float(self.permeable_soil_j_mm.text()),
                                                    string.get_sql_float(self.permeable_soil_rfu_mm.text()), string.get_sql_float(self.permeable_soil_ground_max_inf_rate.text()), string.get_sql_float(self.permeable_soil_ground_lag_time_day.text()), string.get_sql_float(self.permeable_soil_coef_river_exchange.text()),
                                                    runoff_type, string.get_sql_float(self.socose_tc_mn.text()), string.get_sql_float(self.socose_shape_param_beta.text()), string.get_sql_float(self.define_k_mn.text()),
                                                    string.get_sql_float(self.q_limit.text()), string.get_sql_float(self.q0.text()),
                                                    None)
        BaseDialog.save(self)
        self.close()