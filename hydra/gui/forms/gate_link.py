# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
import hydra.utility.string as string
from PyQt4 import uic, QtGui
import os
import re

class GateLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "gate_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "gate.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.groupbox_discharge_for_headloss.hide()

        name, z_invert, z_ceiling, width, cc, valve, action_gate_type, z_gate, v_max_cms = self.project.execute("""
            select name, z_invert, z_ceiling, width, cc, mode_valve, action_gate_type, z_gate, v_max_cms
            from {}.gate_link
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.dir = current_dir
        self.name.setText(name)
        self.z_invert.setText(string.get_str(z_invert))
        self.z_ceiling.setText(string.get_str(z_ceiling))
        self.width.setText(string.get_str(width))
        self.cc.setText(string.get_str(cc))
        self.z_gate.setText(string.get_str(z_gate))
        self.v_max_cms.setText(string.get_str(v_max_cms))

        self.populate_combo_from_sqlenum(self.mode_valve, "valve_mode",
            valve if valve is not None else 'no valve')

        # action gate type initialisation
        self.upward_opening_gate.toggled.connect(self.upward_opening_gate_clicked)
        self.downward_opening_gate.toggled.connect(self.downward_opening_gate_clicked)
        if action_gate_type == 'upward_opening':
            self.upward_opening_gate.setChecked(True)
            self.picture.setPixmap(QtGui.QPixmap(os.path.join(current_dir, "gate_upward_opening.png")))
        elif action_gate_type == 'downward_opening':
            self.downward_opening_gate.setChecked(True)
            self.picture.setPixmap(QtGui.QPixmap(os.path.join(current_dir, "gate_downward_opening.png")))

    def upward_opening_gate_clicked(self, enabled):
        if enabled:
            self.picture.setPixmap(QtGui.QPixmap(os.path.join(self.dir, "gate_upward_opening.png")))

    def downward_opening_gate_clicked(self, enabled):
        if enabled:
            self.picture.setPixmap(QtGui.QPixmap(os.path.join(self.dir, "gate_downward_opening.png")))

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        #get valve mode :
        mode_valve, = self.project.execute("""
            select name
            from hydra.valve_mode
            where description='{}'""".format(self.mode_valve.currentText())).fetchone()
        #get action gate type :
        if self.upward_opening_gate.isChecked():
            action_gate_type = 'upward_opening'
        elif self.downward_opening_gate.isChecked():
            action_gate_type = 'downward_opening'

        self.model.update_link_qmv(self.id, self.geom,
                                        string.get_sql_float(self.z_invert.text()), string.get_sql_float(self.z_ceiling.text()),
                                        string.get_sql_float(self.width.text()), string.get_sql_float(self.cc.text()),
                                        mode_valve, action_gate_type,
                                        string.get_sql_float(self.z_gate.text()), string.get_sql_float(self.v_max_cms.text()),
                                        self.name.text())
        SimpleLinkDialog.save(self)
        self.close()







