# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
import hydra.utility.string as string
from PyQt4 import uic, QtGui
import os
import re

class StreetLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "street_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "street_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, length, width, rk = self.project.execute("""
            select name, length,  width, rk
            from {}.street_link
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.length.setText(string.get_str(length))
        self.width.setText(string.get_str(width))
        self.rk.setText(string.get_str(rk))

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.model.update_street_link(self.id, self.geom,
                                        string.get_sql_float(self.length.text()),
                                        string.get_sql_float(self.width.text()),
                                        string.get_sql_float(self.rk.text()),
                                        self.name.text())
        SimpleLinkDialog.save(self)
        self.close()









