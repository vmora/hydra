# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from PyQt4 import uic
from PyQt4.QtGui import QWidget, QGridLayout
import os
from PyQt4.QtGui import QListWidgetItem
from ..simple_singularity_dialog import SimpleSingularityDialog, tr
from ..widgets.cross_section_widget import CrossSectionWidget
import hydra.utility.string as string


class RiverCrossSectionProfileEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleSingularityDialog.__init__(self, project, geom, "river_cross_section_profile", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "river_cross_section_profile.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        #get PT info
        name, z_invert_up, z_invert_down, type_cs_up, type_cs_down = self.project.execute("""
                                        select name, z_invert_up, z_invert_down, type_cross_section_up, type_cross_section_down
                                        from {}.river_cross_section_profile
                                        where id={} """.format(self.model.name, str(self.id))).fetchone()

        self.name.setText(name)
        self.z_invert_up.setText(string.get_str(z_invert_up))
        self.z_invert_down.setText(string.get_str(z_invert_down))
        self.__type_cs_up = type_cs_up
        self.__type_cs_down = type_cs_down

        #preparing widgets to display if 'cs' checked
        self.up_cs_widget = CrossSectionWidget(self.project, self.id, 'up', parent)
        self.down_cs_widget = CrossSectionWidget(self.project, self.id, 'down', parent)
        self.up_current_layout = None
        self.down_current_layout = None

        #init checkboxes
        if self.__type_cs_up == None:
            self.checkBox_up.setChecked(False)
        else:
            self.checkBox_up.setChecked(True)
        if self.__type_cs_down == None:
            self.checkBox_down.setChecked(False)
        else:
            self.checkBox_down.setChecked(True)
        self.checkBox_up.stateChanged.connect(self.refresh_widgets)
        self.checkBox_down.stateChanged.connect(self.refresh_widgets)
        self.refresh_widgets()

    def refresh_widgets(self):
        if self.checkBox_up.isChecked():
            self.reset_layout('up', self.up_cs_widget.get_layout())
            self.tab.setTabEnabled(0, True)
            self.label_zup.setVisible(True)
            self.z_invert_up.setVisible(True)
        else:
            self.reset_layout('up', None)
            self.tab.setTabEnabled(0, False)
            self.label_zup.setVisible(False)
            self.z_invert_up.setVisible(False)
        if self.checkBox_down.isChecked():
            self.reset_layout('down', self.down_cs_widget.get_layout())
            self.tab.setTabEnabled(1, True)
            self.label_zdown.setVisible(True)
            self.z_invert_down.setVisible(True)
        else:
            self.reset_layout('down', None)
            self.tab.setTabEnabled(1, False)
            self.label_zdown.setVisible(False)
            self.z_invert_down.setVisible(False)

    def reset_layout(self, updown, new_layout):
        assert updown == 'up' or updown == 'down'
        if new_layout is None:
            new_layout = QGridLayout()
        if updown == 'up':
            if self.up_current_layout is not None:
                QWidget(self).setLayout(self.up_current_layout)
            self.widget_placeholder_up.setLayout(new_layout)
            self.up_current_layout = new_layout
        elif updown == 'down':
            if self.down_current_layout is not None:
                QWidget(self).setLayout(self.down_current_layout)
            self.widget_placeholder_down.setLayout(new_layout)
            self.down_current_layout = new_layout

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))
        if self.checkBox_up.isChecked():
            selected_type_cs_up = self.up_cs_widget.combo_cross_section.currentText()
            if selected_type_cs_up == '':
                selected_type_cs_up = 'null'
        else:
            selected_type_cs_up = 'null'
        if self.checkBox_down.isChecked():
            selected_type_cs_down = self.down_cs_widget.combo_cross_section.currentText()
            if selected_type_cs_down == '':
                selected_type_cs_down = 'null'
        else:
            selected_type_cs_down = 'null'

        self.model.update_river_cross_section_profile(self.id, self.geom,
                            string.get_sql_float(self.z_invert_up.text()), string.get_sql_float(self.z_invert_down.text()),
                            selected_type_cs_up, selected_type_cs_down, name=self.name.text())

        self.up_cs_widget.save()
        self.down_cs_widget.save()

        SimpleSingularityDialog.save(self)
        self.close()