# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from PyQt4 import uic
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QWidget, QLineEdit, QComboBox
from hydra.gui.base_dialog import BaseDialog, tr

class ModelConstrainWidget(BaseDialog):
    def __init__(self, project, line, id_=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "model_constrain.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.project = project
        model = project.get_current_model()
        # @todo remove the snap if we can fix the snapper to have exact coordinates
        self.id, name, elem_length, constrain_type, link_attributes, self.line = \
            project.execute("""
                insert into {model}.constrain(geom)
                values({geom})
                returning id, name, elem_length, constrain_type, link_attributes, ST_AsText(geom)
                """.format(model=model.name, geom=model.make_line(line))).fetchone() \
            if id_ is None else \
            project.execute("""
                select id, name, elem_length, constrain_type, link_attributes, ST_AsText(geom)
                from {}.constrain where id={}
                """.format(model.name, str(id_))).fetchone()
        self.nameEdit.setText(name)
        self.constrain_types = self.populate_combo_from_sqlenum(self.typeComboBox, "constrain_type", constrain_type)
        self.typeComboBox.addItem('')
        self.elemLengthSpinBox.setValue(elem_length)
        self.link_attributes.setText(link_attributes)


    def save(self):
        self.project.execute("""
            update {}.constrain set
            name = %s,
            elem_length = %s,
            constrain_type = %s,
            link_attributes = %s
            where id = {}
            """.format(self.project.get_current_model().name, self.id), (
                self.nameEdit.text(),
                self.elemLengthSpinBox.value(),
                self.typeComboBox.currentText() or None,
                self.link_attributes.document().toPlainText() or None
                ))
        BaseDialog.save(self)
        self.close()




