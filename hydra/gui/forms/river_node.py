# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
import hydra.utility.string as string
from PyQt4 import uic
import os
import re

class RiverNodeEditor(BaseDialog):
    def __init__(self, project, geom, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "river_node.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()
        assert not self.__model == None
        self.__geom = geom
        if id==None:
            self.__id = self.__model.add_river_node(self.__geom)
        else:
            self.__id = id
            if self.__geom is None:
                geom, = self.project.execute("""
                select geom
                from {}.river_node
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name, area, z_ground, pk_km = self.project.execute("""
            select name, area, z_ground, pk_km
            from {}.river_node
            where id={}""".format(self.__model.name, self.__id)).fetchone()
        self.name.setText(name)
        self.area.setText(string.get_str(area))
        self.z_ground.setText(string.get_str(z_ground))

        if self.get_reach() is not None:
            # init pk info
            self.pk_km.setText(str(round(pk_km,3)))

            # init Z invert info
            # [node_up(Zup, Zdown, pk, id), node_up(Zup, Zdown, pk, id)]
            pk_info = self.project.execute("""
                                    with
                                        node as (
                                            select pk_km, reach from {m}.river_node where id={i}
                                            ),
                                        pts_up as (
                                            select p.id, n.pk_km, p.z_invert_up, p.z_invert_down
                                            from {m}.river_node as n, node, {m}.river_cross_section_profile as p
                                            where n.reach=node.reach and p.id=n.id and node.pk_km>=n.pk_km
                                            ),
                                        max as (
                                            select max(pts_up.pk_km)as pk from pts_up
                                            ),
                                        pts_down as (
                                            select p.id, n.pk_km, p.z_invert_up, p.z_invert_down
                                            from {m}.river_node as n, node, {m}.river_cross_section_profile as p
                                            where n.reach=node.reach and p.id=n.id and node.pk_km<=n.pk_km
                                            ),
                                        min as (
                                            select min(pts_down.pk_km)as pk from pts_down
                                            )
                                    select pts_up.z_invert_up, pts_up.z_invert_down, pts_up.pk_km, pts_up.id
                                    from pts_up, max
                                    where pts_up.pk_km=max.pk
                                    union
                                    select pts_down.z_invert_up, pts_down.z_invert_down, pts_down.pk_km, pts_down.id
                                    from pts_down, min
                                    where pts_down.pk_km=min.pk
                                    order by pk_km;""".format(m=self.__model.name, i=self.__id)).fetchall()
            if (len(pk_info)==1 and int(pk_info[0][3])==int(self.__id)):
                z_node=None
                if (pk_info[0][0] is not None and int(pk_info[0][0])!=0):
                    z_node=pk_info[0][0]
                elif (pk_info[0][1] is not None and int(pk_info[0][1])!=0):
                    z_node= pk_info[0][1]
                if z_node is not None:
                    self.z_invert.setText(str(round(z_node,3)))
            if len(pk_info)==2:
                z=[None,None]
                pk=[pk_info[0][2], pk_info[1][2]]
                for i in [0,1]:
                    if (pk_info[i][1] is not None and int(pk_info[i][1])!=0):
                        z[i]=pk_info[i][1]
                    elif (pk_info[i][0] is not None and int(pk_info[i][0])!=0):
                        z[i]= pk_info[i][0]
                if (z[0] is not None and z[1] is not None):
                    z_node = z[1]+(pk_km-pk[1])*(z[0]-z[1])/(pk[0]-pk[1])
                    self.z_invert.setText(str(round(z_node,3)))

            title = self.windowTitle()
            self.setWindowTitle(title + " - " + str(self.get_reach()) + ": " + str(round(pk_km, 3)) + " km")
        else:
            title = self.windowTitle()
            self.setWindowTitle(title + " - No reach found")

    def get_id(self):
        return self.__id

    def get_reach(self):
        reach = self.project.execute("""
            with idreach as
            (select reach from {model}.river_node where id={id})
            select name
            from {model}.reach, idreach
            where id=idreach.reach""".format(model=self.__model.name, id=self.__id)).fetchone()
        return reach[0] if reach is not None else None

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.__model.update_river_node(
                self.__id,
                self.__geom,
                string.get_sql_float(self.z_ground.text()),
                string.get_sql_float(self.area.text()),
                self.name.text())
        #update reach reference if location changed
        reach = self.project.execute("""with g as (select geom from {model}.river_node where id={i})
                                            select reach.id
                                            from {model}.reach, g
                                            where ST_DWithin(g.geom, reach.geom, .1)
                                            """.format(model=self.__model.name, i=self.__id)).fetchone()
        if reach is not None:
            self.project.execute("""update {model}.river_node set reach={r} where id={i}""".format(model=self.__model.name, r=reach[0], i=self.__id))

        BaseDialog.save(self)
        self.close()