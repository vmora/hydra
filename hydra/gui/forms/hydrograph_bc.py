# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_singularity_dialog import SimpleSingularityDialog, tr
import hydra.utility.string as string
from ..widgets.graph_widget import GraphWidget
from PyQt4 import uic
from PyQt4.QtGui import QTableWidgetItem
import os
import re

class HydrographBcEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleSingularityDialog.__init__(self, project, geom, "hydrograph_bc_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "hydrograph_bc.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, storage_area, tq_array, external_file_data, constant_dry_flow, sector = self.project.execute("""
            select name, storage_area, tq_array, external_file_data, constant_dry_flow, sector
            from {}.hydrograph_bc_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()
        self.name.setText(name)
        self.constant_dry_flow.setText(string.get_str(constant_dry_flow))
        self.area.setText(string.get_str(storage_area))
        self.graph = GraphWidget(self.graph_placeholder)
        if not tq_array is None:
            self.set_table_items(tq_array)
            self.__draw_graph()

        self.table.cellChanged.connect(self.__cell_changed)
        self.table.currentCellChanged.connect(self.__draw_graph)
        combo_value = 'Yes' if external_file_data else 'No'
        self.combo_external_file_data.setCurrentIndex(self.combo_external_file_data.findText(combo_value))
        self.combo_external_file_data.activated.connect(self.__external_data_changed)

        containing_sector = self.project.execute("""select id, name
            from project.dry_inflow_sector where
                st_contains(geom, '{}')""".format(self.geom)).fetchone()
        if not containing_sector:
            self.defined_by_sector.setEnabled(False)
            self.sector = None
            self.sector_name.setText(tr("Not in a sector"))
            self.defined_by_sector.setChecked(False)
        else:
            self.defined_by_sector.setEnabled(True)
            if sector is not None:
                self.defined_by_sector.setChecked(True)
            self.sector = containing_sector
            self.sector_name.setText(tr("In sector: ")+ self.sector[1])
            self.defined_by_sector.stateChanged.connect(self.__defined_by_sector_changed)
        self.__defined_by_sector_changed()
        self.__external_data_changed()

    def __defined_by_sector_changed(self):
        self.constant_dry_flow.setEnabled(not self.defined_by_sector.isChecked())
        self.dry_label.setEnabled(not self.defined_by_sector.isChecked())

    def __external_data_changed(self):
        self.table.setEnabled(True)
        if self.combo_external_file_data.currentText() == 'Yes':
            self.table.setEnabled(False)

    def __draw_graph(self):
        datas = self.get_table_items()
        if len(datas)>0:
            self.graph.clear()
            self.graph.add_filled_line(
                [r[0] for r in datas],
                [r[1] for r in datas],
                "b",
                self.name.text(),
                tr("T (h)"), tr("Q (m3/s)"))
        else:
            self.graph.canvas.setVisible(False)

    def __cell_changed(self,row,column):
        if row == (self.table.rowCount()-1):
            self.table.setRowCount(self.table.rowCount() + 1)

    def set_table_items(self, tq_array):
        n = len(tq_array)
        self.table.setRowCount(n+1)
        for row in range(0, n):
            self.table.setItem(row,0,QTableWidgetItem(string.get_str(tq_array[row][0])))
            self.table.setItem(row,1,QTableWidgetItem(string.get_str(tq_array[row][1])))
        self.table.resizeColumnsToContents()

    def get_table_items(self):
        n = self.table.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            if not self.table.item(i,0) == None and not self.table.item(i,1) == None:
                result.append(list())
                m = self.table.columnCount()
                result[index_list].append(string.get_sql_float(self.table.item(i,0).text()))
                result[index_list].append(string.get_sql_float(self.table.item(i,1).text()))
                index_list = index_list + 1
        return result

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        bool = True if self.combo_external_file_data.currentText() == 'Yes' else False

        if self.defined_by_sector.isChecked():
            self.project.execute("""update {}.hydrograph_bc_singularity set sector={}
                where id={}""".format(self.model.name,self.sector[0], self.id))
        else:
            self.project.execute("""update {}.hydrograph_bc_singularity set sector=null
                where id={}""".format(self.model.name, self.id))

        self.model.update_hy_singularity(self.id, self.geom, self.name.text(), string.get_sql_float(self.area.text()),
            self.get_table_items(),constant_dry_flow=self.constant_dry_flow.text(), external_file_data=bool)
        SimpleSingularityDialog.save(self)
        self.close()