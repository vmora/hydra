# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
from functools import partial
from PyQt4 import uic, QtGui, QtCore
from PyQt4.QtGui import QTableWidgetItem
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.widgets.graph_widget import GraphWidget
from hydra.gui.widgets.array_widget import ArrayWidget
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
import hydra.utility.string as string

class DryInflowManager(BaseDialog):

    def list_to_sql_array(self, py_list):
        return str(map(str, py_list)).replace('[', '{').replace(']', '}').replace('\'', '') if py_list else None

    def float_sql(self, float_value):
        value = string.get_sql_float(float_value)
        if value is None:
            value="0"
        return value

    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "dry_inflow_manager.ui"), self)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.table_dry_scenario = HydraTableWidget(project,
            ("id", "name", "comment"),
            (tr("Id"), tr("Name"), tr("Comment")),
            "project", "dry_inflow_scenario",
            (self.new_dry_scenario, self.delete_dry_scenario), "", "id",
            self.table_dry_inflow_placeholder)
        self.table_dry_scenario.set_editable(True)
        self.table_dry_scenario.data_edited.connect(self.save_dry_scenario)
        self.table_dry_scenario.table.itemSelectionChanged.connect(self.set_active_dry_scenario)
        self.active_dry_scenario_id=None

        self.table_dry_inflow_sector_settings.resizeColumnsToContents()
        self.table_dry_inflow_sector_settings.horizontalHeader().setStretchLastSection(True);

        self.table_dry_inflow_modulation = HydraTableWidget(project,
            ("id", "name", "comment"),
            (tr("Id"), tr("Name"), tr("Comment")),
            "project", "dry_inflow_hourly_modulation",
            (self.new_modulation, self.delete_modulation), "", "id",
            self.table_dry_inflow_modulation_placeholder)
        self.table_dry_inflow_modulation.set_editable(True)
        self.table_dry_inflow_modulation.data_edited.connect(self.save_modulation)
        self.table_dry_inflow_modulation.table.itemSelectionChanged.connect(self.set_active_modulation)
        self.active_modulation_id=None

        self.graph_modulation = GraphWidget(self.graph_placeholder)

        self.table_modulation = ArrayWidget(["Time (h)", "V(t) / V(24h)"], [25,2], [[0,0],[24,1]], self.__draw_graph, self.table_modulation_placeholder)
        self.table_modulation.setEnabled(False)

        self.gbx_scenario.setEnabled(self.table_dry_inflow_modulation.table.rowCount()>0)

    def __draw_graph(self, datas):
        title = ''
        current_row = self.table_dry_inflow_modulation.table.currentRow()
        if current_row != -1:
            title = self.table_dry_inflow_modulation.table.item(current_row,1).text()
        if len(datas)>0:
            self.graph_modulation.clear()
            self.graph_modulation.add_line(
                [r[0] for r in datas],
                [r[1] for r in datas],
                "r",
                title,
                tr("t (h)"), tr("V/Vtot"))
        else:
            self.graph_modulation.canvas.setVisible(False)

    def select_by_name(self, scenario_name):
        for i in range(0, self.table_dry_scenario.table.rowCount()):
            if self.table_dry_scenario.table.item(i,1).text()==scenario_name:
                self.table_dry_scenario.table.setCurrentCell(i,0)

    def set_active_dry_scenario(self):
        if not self.active_modulation_id is None:
            self.save_modulation()

        items = self.table_dry_scenario.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_dry_scenario.table.row(items[0])
            if not self.active_dry_scenario_id is None:
                self.save_dry_scenario()

            self.active_dry_scenario_id = self.table_dry_scenario.table.item(selected_row,0).text()
            self.lbl_dry_scenario.setText(self.table_dry_scenario.table.item(selected_row,1).text())
            sectors_settings = self.project.execute("""select dis.id, dis.name, settings.vol_curve, settings.volume_sewage_m3day,
                settings.coef_volume_sewage_m3day, settings.volume_clear_water_m3day, settings.coef_volume_clear_water_m3day
                from project.dry_inflow_sector as dis
                left join project.dry_inflow_scenario_sector_setting as settings on dis.id = settings.sector
                and settings.dry_inflow_scenario={}
                order by dis.name;""".format(self.active_dry_scenario_id)).fetchall()
            self.set_dry_inflow_sector_settings_table_items(sectors_settings)
        else:
            self.active_dry_scenario_id = None
            self.lbl_dry_scenario.setText("")
            self.set_dry_inflow_sector_settings_table_items([])

    def set_dry_inflow_sector_settings_table_items(self, sectors_settings):
        self.table_dry_inflow_sector_settings.setRowCount(0)
        n = len(sectors_settings)
        self.table_dry_inflow_sector_settings.setRowCount(n)
        list_modulation = self.project.execute("select id, name from project.dry_inflow_hourly_modulation").fetchall()
        for row in range(0, n):
            self.table_dry_inflow_sector_settings.setItem(row,0,QTableWidgetItem(string.get_str(sectors_settings[row][0])))
            self.table_dry_inflow_sector_settings.setItem(row,1,QTableWidgetItem(string.get_str(sectors_settings[row][1])))

            self.table_dry_inflow_sector_settings.setItem(row,2,QTableWidgetItem(string.get_str(sectors_settings[row][2])))
            id_modulation = sectors_settings[row][2]
            cbox = self.get_new_modulation_combo(list_modulation, id_modulation)
            cbox.currentIndexChanged.connect(partial(self.combo_modulation_index_changed,row, 2))
            self.table_dry_inflow_sector_settings.setCellWidget(row, 2, cbox)

            self.table_dry_inflow_sector_settings.setItem(row,3,QTableWidgetItem(string.get_str(sectors_settings[row][3])))
            self.table_dry_inflow_sector_settings.setItem(row,4,QTableWidgetItem(string.get_str(sectors_settings[row][4])))
            self.table_dry_inflow_sector_settings.setItem(row,5,QTableWidgetItem(string.get_str(sectors_settings[row][5])))
            self.table_dry_inflow_sector_settings.setItem(row,6,QTableWidgetItem(string.get_str(sectors_settings[row][6])))

    def get_dry_inflow_sector_settings_table_items(self):
        n = self.table_dry_inflow_sector_settings.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            result.append(list())
            for o in range(0,2):
                result[index_list].append(self.table_dry_inflow_sector_settings.item(i,o).text())
            result[index_list].append(self.table_dry_inflow_sector_settings.item(i,2).text() \
                if self.table_dry_inflow_sector_settings.item(i,2).text()!='' else 'null')
            for o in range(3,7):
                result[index_list].append(self.float_sql(self.table_dry_inflow_sector_settings.item(i,o).text()))
            index_list = index_list + 1
        return result

    def get_new_modulation_combo(self, list_modulation, id_modulation):
        combobox = QtGui.QComboBox()
        combobox.addItem(tr("None"))
        for i in range(0, len(list_modulation)):
            modulation = list_modulation[i]
            combobox.addItem("{} - {}".format(modulation[0], modulation[1]))
            if (str(id_modulation) == str(modulation[0])):
                combobox.setCurrentIndex(i+1)
        return combobox

    def combo_modulation_index_changed(self, irow, comboBoxIndex):
        icol=2
        self.table_dry_inflow_sector_settings.selectRow(irow)
        id_modulation=self.table_dry_inflow_sector_settings.cellWidget(irow, icol).currentText().split("-")[0] \
            if self.table_dry_inflow_sector_settings.cellWidget(irow, icol).currentText()!=tr("None") else "null"
        self.table_dry_inflow_sector_settings.item(irow, icol).setText(id_modulation)


    def delete_dry_scenario(self):
        self.table_dry_scenario.del_selected_row()

    def new_dry_scenario(self):
        self.table_dry_scenario.add_row(['default'])

    def save_dry_scenario(self):
        self.table_dry_scenario.save_selected_row()
        if self.active_dry_scenario_id is not None:
            items = self.get_dry_inflow_sector_settings_table_items()
            if len(items)>0:
                values = list()
                for item in items:
                    values.append("({},{},{},{},{},{},{})".format(self.active_dry_scenario_id,
                        item[0], item[2], item[3], item[4], item[5], item[6]))
                sql= """insert into project.dry_inflow_scenario_sector_setting(dry_inflow_scenario, sector, vol_curve, volume_sewage_m3day,
                    coef_volume_sewage_m3day, volume_clear_water_m3day, coef_volume_clear_water_m3day)
                    values {} on conflict (dry_inflow_scenario, sector)
                    do update set sector=excluded.sector, vol_curve=excluded.vol_curve, volume_sewage_m3day=excluded.volume_sewage_m3day,
                    coef_volume_sewage_m3day=excluded.coef_volume_sewage_m3day, volume_clear_water_m3day=excluded.volume_clear_water_m3day,
                    coef_volume_clear_water_m3day=excluded.coef_volume_clear_water_m3day;
                    """.format(','.join(values))
                self.project.execute(sql)


        items = self.table_dry_scenario.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_dry_scenario.table.row(items[0])
            self.lbl_dry_scenario.setText(self.table_dry_scenario.table.item(selected_row,1).text())


    def delete_modulation(self):
        self.table_dry_inflow_modulation.del_selected_row()
        self.gbx_scenario.setEnabled(self.table_dry_inflow_modulation.table.rowCount()>0)

    def new_modulation(self):
        self.table_dry_inflow_modulation.add_row(['default','null', '{{0,0},{24,1}}'], ["hv_array"])
        self.gbx_scenario.setEnabled(self.table_dry_inflow_modulation.table.rowCount()>0)
        self.set_active_dry_scenario()

    def save_modulation(self):
        if self.active_modulation_id is not None:
            self.table_dry_inflow_modulation.save_selected_row()
            items = self.table_modulation.get_table_items()
            if items is not None:
                self.project.execute("""update project.dry_inflow_hourly_modulation set hv_array='{}' where id={}""".format(self.list_to_sql_array(items), self.active_modulation_id))

    def set_active_modulation(self):
        if not self.active_dry_scenario_id is None:
            self.save_dry_scenario()
        items = self.table_dry_inflow_modulation.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_dry_inflow_modulation.table.row(items[0])
            if not self.active_modulation_id is None:
                self.save_modulation()

            self.active_modulation_id = self.table_dry_inflow_modulation.table.item(selected_row,0).text()
            hv_array, = self.project.execute("""select hv_array from project.dry_inflow_hourly_modulation where id={};""".format(self.active_modulation_id)).fetchone()
            self.table_modulation.set_table_items(hv_array)
            self.table_modulation.setEnabled(True)

    def save(self):
        if not self.active_modulation_id is None:
            self.save_modulation()
        if not self.active_dry_scenario_id is None:
            self.save_dry_scenario()
        BaseDialog.save(self)
        self.close()
