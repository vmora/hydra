# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
import os.path
from operator import itemgetter
from qgis.gui import QgsCollapsibleGroupBox
from qgis.core import QGis, QgsPoint, QgsVectorLayer, QgsMapLayerRegistry, QgsFeature, QgsGeometry
from PyQt4.QtCore import QCoreApplication, Qt, QDateTime
from PyQt4.QtGui import QApplication, QAction, QPushButton, QIcon, QToolBar, QComboBox, QMessageBox, QFileDialog, QSlider
from hydra.utility.map_point_tool import create_point, Snapper, VisibleGeometry, MapPointTool
from hydra.utility.tables_properties import TablesProperties
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.timer import display_timestamp
from hydra.utility.result_decoder import W14Result, W15Result
from hydra.gui.time_control import TimeControl
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.database.radar_rain import RainVrt

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

_plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
_icons_dir = os.path.join(_plugin_dir, "ressources", "images")

class ToolBar(QToolBar):

    def __add_action_button(self, name, icon, action = None, togglable = False):
        action_button = QAction(QIcon(os.path.join(_icons_dir, icon)), tr(name), self)
        if action is not None:
            action_button.triggered.connect(action)
        self.addAction(action_button)
        action_button.setCheckable(togglable)
        return action_button

    def __init__(self, title, current_project, iface, refresh_layers_fct, refresh_ui_fct):
        QToolBar.__init__(self, title)

        self.refresh_layers = refresh_layers_fct
        self.refresh_ui = refresh_ui_fct

        self.__current_tool = None
        self.__currentproject = current_project
        self.__log_manager = current_project.log
        self.__iface = iface
        self.__properties = TablesProperties.get_properties()
        self.__last_tool_clicked = None
        self.__triangles_layer = None
        self.__water_level_layer = None

        self.__model_combo = QComboBox()
        self.__scn_combo = ComboWithValues()
        self.__config_combo = ComboWithValues()
        self.__model_combo.activated.connect(self.__model_changed)
        self.__scn_combo.activated.connect(self.__scn_changed)
        self.__config_combo.activated.connect(self.__config_changed)

        self.__run_button = QPushButton(tr('Run...'), self)
        self.__run_button.clicked.connect(self.__run_calculation)

        self.__time_control = TimeControl(blocker=None)
        # self.__isloading allow to re-populate comboboxes without triggering
        # the xx_changed function connected to the activated signal of each comboboxes
        self.__isloading = True

        #@todo find a robust mechanisme to issue nextFrame to TimeControl
        # and change blocker to True
        # historically nextFrame was connected to iface.mapCanvas().renderComplete wich
        # is not taken into account if we only display logi profile

        self.__visu_stored_data = list()

        # Assembling toolbar's widgets
        self.addWidget(self.__model_combo)
        self.addWidget(self.__config_combo)
        self.edit_button = self.__add_action_button(tr("Edit"), "edit_button.svg", self.__edit_tool, togglable = True)
        self.del_button = self.__add_action_button(tr("Delete"), "delete_button.svg", self.__delete_tool, togglable = True)
        self.__search_button = self.__add_action_button(tr("Search objects"), "search.svg", self.__search_tool)
        self.__geom_button =self.__add_action_button(tr("Geometries library"), "geom.svg", self.__edit_geometries)
        self.__valid_button =self.__add_action_button(tr("List invalid objects"), "validity.svg", self.__validity_tool)
        self.__config_button =self.__add_action_button(tr("List configured objects"), "configured.svg", self.__configuration_tool)
        # self.addSeparator()
        # self.__links_normal_button = self.__add_action_button(tr("Display current model links normally"), "links.svg", self.__normal_links)
        # self.__links_arrow_button = self.__add_action_button(tr("Display current model links as arrows"), "arrows.svg", self.__arrows_links)
        self.addSeparator()
        self.addWidget(self.__scn_combo)
        self.addWidget(self.__run_button)
        self.__visugraph_button = self.__add_action_button(tr("Show graph"), "visugraph.svg", self.__visu_toolgraph, togglable = True)
        self.__visutable_button = self.__add_action_button(tr("Show min/max table"), "visutable.svg", self.__visu_tooltable, togglable = True)
        self.__display_results_button = self.__add_action_button(tr("Display results"), "show_results.svg", self.__display_results)
        self.addSeparator()
        self.__display_w15_button = self.__add_action_button(tr("Show W15 results"), "show_w15.svg", self.__display_w15, togglable = True)
        self.__animate_rain_button = self.__add_action_button(tr("Animate rain"), "radar_rain.svg", self.__animate_rain, togglable = False)
        self.addWidget(self.__time_control)

        self.__iface.mapCanvas().mapToolSet.connect(self.map_tool_changed)
        self.__display_w15_button.triggered.connect(self.active_time_control)
        self.opengl_hydra_layer = None

        self.__refresh_combos()
        self.__refresh_buttons()
        self.active_time_control()
        self.__isloading = False

    def refresh_content(self, current_project):
        self.__isloading=True
        self.__currentproject = current_project

        self.__refresh_combos()
        self.__refresh_buttons()

        self.__isloading=False

    def __refresh_combos(self):
        # Models combobox
        self.__model_combo.clear()
        models = self.__currentproject.get_models()
        for model in sorted(models, key=unicode.lower):
            self.__model_combo.addItem(model)
        if self.__currentproject.get_current_model():
            self.__model_combo.setCurrentIndex(self.__model_combo.findText(self.__currentproject.get_current_model().name))
        else:
            self.__model_combo.setCurrentIndex(-1)

        # Scenarios combobox
        self.__scn_combo.clear()
        scn_id_name = self.__currentproject.get_scenarios()
        for scn in sorted(scn_id_name, key=itemgetter(1)):
            self.__scn_combo.addItem(scn[1],scn[0])
        if self.__currentproject.get_current_scenario():
            self.__scn_combo.set_selected_value(self.__currentproject.get_current_scenario()[0])
        else:
            self.__scn_combo.setCurrentIndex(-1)

        # Configurations combobox
        self.__config_combo.clear()
        if self.__currentproject.get_models():
            config_id_name = self.__currentproject.get_current_model().get_configurations()
            sorted_config_id_name = sorted(config_id_name, key=itemgetter(1))
            sorted_config_id_name.insert(0, sorted_config_id_name.pop(sorted_config_id_name.index((1, u'default'))))
            for config in sorted_config_id_name:
                self.__config_combo.addItem(config[1],config[0])
            if self.__currentproject.get_current_model().get_current_configuration():
                self.__config_combo.set_selected_value(self.__currentproject.get_current_model().get_current_configuration())
            else:
                self.__config_combo.setCurrentIndex(-1)
        else:
            self.__config_combo.setCurrentIndex(-1)

    def __refresh_buttons(self):
        if not self.__currentproject.get_models() or not self.__currentproject.get_current_scenario():
            self.__run_button.setEnabled(False)
        else:
            self.__run_button.setEnabled(True)

        exists_model = True
        if not self.__currentproject.get_current_model():
            exists_model = False

        exists_scenario = True
        if self.__currentproject.get_current_scenario() is None:
            exists_scenario = False

        exists_config = False
        if self.__currentproject.get_models() and len(self.__currentproject.get_current_model().get_configurations()) > 1:
            exists_config = True

        config_not_default = False
        if self.__currentproject.get_models() and self.__currentproject.get_current_model().get_current_configuration() != 1:
            config_not_default = True

        self.edit_button.setEnabled(exists_model)
        self.del_button.setEnabled(exists_model)
        self.__search_button.setEnabled(exists_model)
        self.__valid_button.setEnabled(exists_model)
        self.__config_button.setEnabled(exists_model and exists_config and config_not_default)
        self.__geom_button.setEnabled(exists_model)
        # self.__links_normal_button.setEnabled(exists_model)
        # self.__links_arrow_button.setEnabled(exists_model)
        self.__display_results_button.setEnabled(exists_model and exists_scenario)
        self.__visugraph_button.setEnabled(exists_model and exists_scenario)
        self.__visutable_button.setEnabled(exists_model and exists_scenario)
        self.__display_w15_button.setEnabled(exists_model and exists_scenario)

        # Untoggle inactive buttons
        for action in self.actions():
            if action.isCheckable():
                if not action.isEnabled():
                    action.setChecked(False)

    def __model_changed(self):
        if self.__isloading:
            return
        if self.__currentproject.get_current_model() is not None and self.__currentproject.get_current_model().name==self.__model_combo.currentText():
            return

        if self.__model_combo.currentIndex() != -1:
            self.__currentproject.log.notice(tr("Current model set to ") + self.__model_combo.currentText())
            self.__currentproject.set_current_model(self.__model_combo.currentText())
        else :
            self.__currentproject.log.notice(tr("No current model"))
            self.__currentproject.set_current_model(None)
        self.refresh_ui()

    def __scn_changed(self):
        if self.__isloading:
            return
        if self.__currentproject.get_current_scenario() is not None and self.__currentproject.get_current_scenario()[0]==self.__scn_combo.get_selected_value():
            return

        prec_scn = self.__currentproject.get_current_scenario()
        if self.__scn_combo.get_selected_value():
            self.__currentproject.log.notice(tr("Current scenario set to ") + self.__scn_combo.currentText())
            self.__currentproject.set_current_scenario(self.__scn_combo.get_selected_value())
        else :
            self.__currentproject.log.notice(tr("No current scenario"))
            self.__currentproject.set_current_scenario(None)
        new_scn = self.__currentproject.get_current_scenario()
        if new_scn != prec_scn:
            self.__display_w15()
        self.refresh_ui()

    def __config_changed(self):
        if self.__isloading:
            return
        if self.__currentproject.get_current_model().get_current_configuration()==self.__config_combo.get_selected_value():
            return

        if self.__config_combo.get_selected_value():
            self.__currentproject.log.notice(tr("Current configuration set to ") + self.__config_combo.currentText())
            self.__currentproject.get_current_model().set_current_configuration(self.__config_combo.get_selected_value())
        self.refresh_layers(self.__properties['invalid']['name'])
        self.refresh_ui()

    def __run_calculation(self):
        from ..database.export_calcul import ExportCalcul
        from ..database.export_carto_data import ExportCartoData
        from hydra.kernel import hydra_kernel
        from hydra.gui.validity import ValidityTool

        self.__currentproject.unload_csv_results_layers()
        self.drop_results()

        id_scn, name_scn = self.__currentproject.get_current_scenario()
        id, scenario, comput_mode, output_option, model_connect_settings = self.__currentproject.execute("""
            select id, scenario, comput_mode, output_option, model_connect_settings
            from project.scenario
            where id={}
            """.format(id_scn)).fetchone()
        id_comput_option, = self.__currentproject.execute("""
            select id from hydra.computation_mode
            where name='{}'
            """.format(comput_mode)).fetchone()
        lstmodel = [name for name, in self.__currentproject.execute("""
            select name
            from project.model
            """).fetchall()]
        if model_connect_settings=="mixed":
            lstmodel = []
            groups = self.__currentproject.execute("""
                    select id, name, ord
                    from project.mixed as mixed, project.grp as grp
                    where mixed.grp=grp.id
                    and mixed.scenario={}
                    order by mixed.ord""".format(id_scn)).fetchall()
            for group in groups:
                lstmodel = [name for name, in self.__currentproject.execute("""
                    select name
                    from project.model_config as model
                    where model.id in (
                        select model from project.grp_model
                        where grp = %i)
                    """%(int(group[0]))).fetchall()]

        invalid=False
        for model in lstmodel:
            if self.__currentproject.execute(""" select * from {}.invalid""".format(model)).fetchall():
                invalid=True
                invalid_model=model

        if invalid:
            validity = QMessageBox(QMessageBox.Warning, tr('Warning'), tr('There are still some invalidities in model {}. Do you want to open the Validity manager?'.format(invalid_model)), QMessageBox.Yes | QMessageBox.No).exec_()
            if validity == QMessageBox.Yes:
                self.__currentproject.set_current_model(invalid_model)
                ValidityTool(self.__currentproject, self.__iface).exec_()
                self.refresh_layers()
        else:
            # export topological data for calculation
            exporter = ExportCalcul(self.__currentproject)
            exporter.export(id_scn)
            # export cartograpic data for triangulation
            exporter = ExportCartoData(self.__currentproject)
            exporter.export(id_scn)
            flag_gfx_control, = self.__currentproject.execute("""
                select flag_gfx_control
                from project.scenario
                where id={} limit 1""".format(str(id_scn))).fetchone()
            # run calculations
            try:
                QApplication.setOverrideCursor(Qt.WaitCursor)
                QApplication.processEvents()
                hydra_kernel.run(self.__currentproject.get_senario_path(id_scn), flag_gfx_control, comput_mode, self.__iface.mainWindow())
            except Exception as e:
                QApplication.restoreOverrideCursor()
                raise e
            QApplication.restoreOverrideCursor()

        self.refresh_content(self.__currentproject)

    def __visu_toolgraph(self, flag=None):
        from hydra.gui.visu_tool import VisuToolGraph
        if self.__visugraph_button.isChecked():
            self.__last_tool_clicked = "visugraph"
            self.prev_tool = self.__iface.mapCanvas().mapTool()

            def on_left_click(point):
                geom = VisibleGeometry(self.__iface.mapCanvas(), QGis.Point)
                geom.add_point(point)
                snap = SettingsProperties.get_snap()
                size = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()*snap
                VisuToolGraph(self.__currentproject, point, size, self)

            def on_right_click(point):
                self.__iface.mapCanvas().setMapTool(self.prev_tool)

            self.__current_tool = MapPointTool(self.__iface.mapCanvas(), self.__log_manager)
            self.__current_tool.leftClicked.connect(on_left_click)
            self.__current_tool.rightClicked.connect(on_right_click)
            self.__iface.mapCanvas().setMapTool(self.__current_tool)

            while self.__iface.mapCanvas().mapTool() == self.__current_tool:
                QApplication.instance().processEvents()

            if self.__current_tool:
                self.__iface.mapCanvas().unsetMapTool(self.__current_tool)
                self.__current_tool=None
        else:
            self.map_tool_changed()

    def __visu_tooltable (self, flag = None) :
        from hydra.gui.visu_tool import VisuToolTable
        if self.__visutable_button.isChecked():
            self.__last_tool_clicked = "visutable"
            self.prev_tool = self.__iface.mapCanvas().mapTool()

            def on_left_click(point):
                geom = VisibleGeometry(self.__iface.mapCanvas(), QGis.Point)
                geom.add_point(point)
                snap = SettingsProperties.get_snap()
                size = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()*snap
                VisuToolTable(self.__currentproject, point, size, self)

            def on_right_click(point):
                self.__iface.mapCanvas().setMapTool(self.prev_tool)

            self.__current_tool = MapPointTool(self.__iface.mapCanvas(), self.__log_manager)
            self.__current_tool.leftClicked.connect(on_left_click)
            self.__current_tool.rightClicked.connect(on_right_click)
            self.__iface.mapCanvas().setMapTool(self.__current_tool)

            while self.__iface.mapCanvas().mapTool() == self.__current_tool:
                QApplication.instance().processEvents()

            if self.__current_tool:
                self.__iface.mapCanvas().unsetMapTool(self.__current_tool)
                self.__current_tool=None
        else:
            self.map_tool_changed()

    def map_tool_changed(self):
        self.__visugraph_button.setChecked(self.__last_tool_clicked == "visugraph")
        self.__visutable_button.setChecked(self.__last_tool_clicked == "visutable")
        self.edit_button.setChecked(self.__last_tool_clicked == "edit")
        self.del_button.setChecked(self.__last_tool_clicked == "delete")
        if self.__last_tool_clicked is None and self.__current_tool:
            self.__iface.mapCanvas().unsetMapTool(self.__current_tool)
            self.__current_tool=None
        self.__last_tool_clicked = None

    def __edit_tool(self, flag=None):
        from hydra.gui.edit import EditTool
        if self.edit_button.isChecked():
            self.__last_tool_clicked = "edit"
            self.prev_tool = self.__iface.mapCanvas().mapTool()

            def on_left_click(point):
                geom = VisibleGeometry(self.__iface.mapCanvas(), QGis.Point)
                geom.add_point(point)
                snap = SettingsProperties.get_snap()
                size = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()*snap
                table = EditTool(self.__currentproject, geom, size, None, self.__iface).get_table()
                properties = TablesProperties.get_properties()
                if table:
                    self.refresh_layers(properties[table]['name'])
                    if table == 'street':
                        self.refresh_layers([properties['constrain']['name'], properties['coverage']['name']])

            def on_right_click(point):
                self.__iface.mapCanvas().setMapTool(self.prev_tool)

            self.__current_tool = MapPointTool(self.__iface.mapCanvas(), self.__log_manager)
            self.__current_tool.leftClicked.connect(on_left_click)
            self.__current_tool.rightClicked.connect(on_right_click)
            self.__iface.mapCanvas().setMapTool(self.__current_tool)

            while self.__iface.mapCanvas().mapTool() == self.__current_tool:
                QApplication.instance().processEvents()

            if self.__current_tool:
                self.__iface.mapCanvas().unsetMapTool(self.__current_tool)
                self.__current_tool=None
        else:
            self.map_tool_changed()

    def __delete_tool(self, flag=None):
        from hydra.gui.delete import DeleteTool
        if self.del_button.isChecked():
            self.__last_tool_clicked = "delete"
            self.prev_tool = self.__iface.mapCanvas().mapTool()

            def on_left_click(point):
                geom = VisibleGeometry(self.__iface.mapCanvas(), QGis.Point)
                geom.add_point(point)
                snap = SettingsProperties.get_snap()
                size = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()*snap
                delete = DeleteTool(self.__currentproject, geom, size)
                table = delete.get_table()
                cascade = delete.is_cascade()
                properties = TablesProperties.get_properties()
                if table:
                    if cascade :
                        self.refresh_layers()
                    else:
                        self.refresh_layers([self.__properties[table]['name'], self.__properties['coverage']['name']])

            def on_right_click(point):
                self.__iface.mapCanvas().setMapTool(self.prev_tool)

            self.__current_tool = MapPointTool(self.__iface.mapCanvas(), self.__log_manager)
            self.__current_tool.leftClicked.connect(on_left_click)
            self.__current_tool.rightClicked.connect(on_right_click)
            self.__iface.mapCanvas().setMapTool(self.__current_tool)

            while self.__iface.mapCanvas().mapTool() == self.__current_tool:
                QApplication.instance().processEvents()

            if self.__current_tool:
                self.__iface.mapCanvas().unsetMapTool(self.__current_tool)
                self.__current_tool=None
        else:
            self.map_tool_changed()

    def __search_tool(self):
        from hydra.gui.search import SearchTool
        SearchTool(self.__currentproject, self.__iface).exec_()
        self.refresh_ui()
        self.refresh_layers()

    def __validity_tool(self):
        from hydra.gui.validity import ValidityTool
        ValidityTool(self.__currentproject, self.__iface).exec_()
        self.refresh_ui()
        self.refresh_layers()

    def __configuration_tool(self):
        from hydra.gui.configuration import ConfigurationTool
        ConfigurationTool(self.__currentproject, self.__iface).exec_()
        self.refresh_ui()
        self.refresh_layers(self.__properties['invalid']['name'])

    def __edit_geometries(self):
        from hydra.gui.geometry_manager import GeometryManager
        GeometryManager(self.__currentproject).exec_()

    def __normal_links(self):
        self.__currentproject.reload_model_layers(self.__currentproject.get_current_model().name)
        self.__currentproject.save()

    def __arrows_links(self):
        self.__currentproject.reload_model_layers(self.__currentproject.get_current_model().name, arrows=True)
        self.__currentproject.save()

    def __display_results(self):
        self.__currentproject.load_csv_results_layers()

    def __display_w15(self):
        if self.__display_w15_button.isChecked():
            project_path = self.__currentproject.get_workspace()
            project_name = self.__currentproject.name
            model_name = self.__currentproject.get_current_model().name
            scn_name = self.__currentproject.get_current_scenario()[1]

            file_name = os.path.join(project_path, project_name, scn_name, 'hydraulique', "{}_{}".format(scn_name, model_name.upper()))
            w14_file = file_name+'.w14'
            w15_file = file_name+'.w15'

            if os.path.isfile(w14_file) and os.path.isfile(w15_file):
                w14 = W14Result(w14_file)
                w15 = W15Result(w15_file)

                self.__time_control.setTimes(["%.2f"%(d) for d in w15.get_step_times()])
                self.opengl_hydra_layer = self.__currentproject.reload_w15_layer(w14, w15)
                self.__time_control.timeChanged.connect(self.opengl_hydra_layer.setStep)
            else:
                self.__drop_w15_layer()
        else:
            self.__drop_w15_layer()

    def __drop_w15_layer(self):
        self.opengl_hydra_layer = None
        self.__currentproject.unload_w15_layer()
        self.__currentproject.save()

    def drop_results(self):
        self.__time_control.set_active(False)
        self.__display_w15_button.setChecked(False)
        self.__drop_w15_layer()

    def active_time_control(self):
        if self.__display_w15_button.isChecked():
            self.__time_control.set_active(True)
        else:
            self.__time_control.set_active(False)

    def __animate_rain(self):
        layer = self.__iface.activeLayer()

        if layer is not None:
            file = os.path.normpath(layer.source())
            raster_rains = [os.path.normpath(self.__currentproject.unpack_path(f)) for f, in self.__currentproject.execute(""" select file from project.radar_rainfall""").fetchall()]

            if file in raster_rains:
                rain = RainVrt(file, self.__currentproject)

                self.__rain_control = TimeControl(blocker=None)
                self.__rain_control.setWindowTitle('Rain animation')
                self.__rain_control.setTimes([display_timestamp(str(date)) for [i, date, time] in rain.time_index])

                def p(i):
                    layer.renderer().setBand(i+1)
                    layer.triggerRepaint()
                    QApplication.instance().processEvents()

                self.__rain_control.timeChanged.connect(p)
                self.__rain_control.show()
            else:
                self.__log_manager.warning("Select a layer associated with a radar rain.")

    def __del__(self):
        self.__iface.mapCanvas().mapToolSet.disconnect(self.map_tool_changed)
        self.__iface.mainWindow().removeToolBar(self)