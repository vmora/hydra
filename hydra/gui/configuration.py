# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
from qgis.core import QgsPoint, QgsRectangle
from PyQt4 import uic
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QTableWidgetItem, QAbstractItemView, QTextEdit, QInputDialog, QHeaderView, QMessageBox
from base_dialog import BaseDialog, tr
from hydra.gui.edit import EditTool
from hydra.gui.delete import DeleteTool
from hydra.utility.tables_properties import TablesProperties
import hydra.utility.sql_json as sql_json

class ConfigurationTool(BaseDialog):
    def __init__(self, project, iface, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "configuration.ui"), self)

        self.project = project
        self.properties = TablesProperties.get_properties()
        self.iface = iface

        self.buttonBox.accepted.connect(self.sav)
        self.edit_button.clicked.connect(self.edit)
        self.zoom_button.clicked.connect(self.zoom)
        self.delete_button.clicked.connect(self.delete)

        self.model.addItems(self.project.get_models())
        if self.project.get_current_model() is not None :
            self.model.setCurrentIndex(self.project.get_models().index(self.project.get_current_model().name))
        else :
            self.model.setCurrentIndex(0)

        self.model.activated.connect(self.model_changed)
        self.config.activated.connect(self.config_changed)
        self.model_changed()

    def model_changed(self):
        if self.model.currentIndex() != -1 :
            self.project.set_current_model(self.model.currentText())

            self.config.clear()
            self.config.addItems([name for id,name in self.project.get_current_model().get_configurations() if name!='default'])
            if self.project.get_current_model().get_current_configuration() is not None :
                self.config.setCurrentIndex(self.project.get_current_model().get_configurations().index((self.project.get_current_model().get_current_configuration(), self.__get_current_config_name()))-1)
            else :
                self.config.setCurrentIndex(0)
            self.config_changed()
        else :
            self.project.log.warning(tr("No active model"))
            self.project.set_current_model(None)

    def config_changed(self):
        if self.config.currentIndex() != -1 :
            cfg_name = self.config.currentText()
            cfg_id = self.project.execute("""select id from {}.configuration where name='{}'""".format(self.project.get_current_model().name, cfg_name)).fetchone()[0]
            self.project.get_current_model().set_current_configuration(cfg_id)
        else :
            self.project.log.warning(tr("No active configuration or active configuration is default"))
            self.project.set_current_model(None)
        self.refresh()

    def refresh(self):
        self.data_table=[]
        self.config_table.setRowCount(0)
        self.config_table.setColumnCount(5)
        title_columns = [tr('Type'),tr('Id'),tr('Name'),tr('Default'), self.__get_current_config_name()]
        self.config_table.setHorizontalHeaderLabels(title_columns)

        configured = self.project.execute("""select subtype::varchar||'_'||type::varchar, id, name, configuration as config from {}.configured_current""".format(self.model.currentText())).fetchall()

        for item in configured:
            item_table = item[0]
            default = item[3]['default']
            config = item[3][self.__get_current_config_name()]
            self.__clean_config(default, config)

            self.data_table.append([item_table, item[1].split(':')[1], item[1].split(':')[0]])
            row_position = self.config_table.rowCount()
            self.config_table.insertRow(row_position)
            self.config_table.setRowHeight(row_position, self.config_table.rowHeight(row_position)*2.5)
            self.config_table.setItem(row_position, 0, QTableWidgetItem(self.properties[item_table]['name']))
            self.config_table.setItem(row_position, 1, QTableWidgetItem(item[1].split(':')[1]))
            self.config_table.setItem(row_position, 2, QTableWidgetItem(item[2]))

            def_text=''
            cfg_text=''
            for param in default.keys():
                if param in config.keys():
                    def_text = def_text + '{} = {}\n'.format(str(param), str(default[param]))
                    cfg_text = cfg_text + '{} = {}\n'.format(str(param), str(config[param]))
            def_item = QTableWidgetItem(def_text)
            def_item.setTextAlignment(Qt.AlignLeft|Qt.AlignTop)
            self.config_table.setItem(row_position, 3, def_item)
            def_item = QTableWidgetItem(cfg_text)
            def_item.setTextAlignment(Qt.AlignLeft|Qt.AlignTop)
            self.config_table.setItem(row_position, 4, def_item)

        self.config_table.resizeColumnsToContents()
        self.number_invalid.setText(str(len(configured)))
        self.details.setText(self.detail_string(self.data_table))

    def __clean_config(self, def_dict, cfg_dict):
        for param in def_dict.keys():
            if param in cfg_dict.keys():
                if def_dict[param]==cfg_dict[param]:
                        def_dict.pop(param, None)
                        cfg_dict.pop(param, None)

    def __get_current_config_name(self):
        return self.project.execute("""select c.name from {m}.configuration as c, {m}.metadata as m where m.configuration=c.id""".format(m=self.model.currentText())).fetchone()[0]

    def detail_string(self, table):
        n, s, l, o = 0, 0 ,0, 0
        for object in table:
            type= object[0].split(':')[0]
            if type == 'node':
                n += 1
            elif type == 'singularity':
                s += 1
            elif type == 'link':
                l += 1
        o = len(table) - n - s - l
        if o == 0 :
            string = "({} nodes, {} singularities and {} links)".format(str(n), str(s), str(l))
        elif o > 0:
            string = "({} nodes, {} singularities, {} links and {} others)".format(str(n), str(s), str(l), str(o))
        return string

    def edit(self):
        if self.config_table.currentItem() is None:
            return
        EditTool.edit(self.project,
                        self.data_table[self.config_table.currentRow()][0],
                        self.data_table[self.config_table.currentRow()][1])
        self.refresh()
        return

    def delete(self):
        if self.config_table.currentItem() is None:
            return

        config_id_name = self.project.get_current_model().get_configurations()
        config_id_name.pop(0)
        config_names = [n[1] for n in config_id_name]

        config = self.config.currentText()
        config_id = [c[0] for c in config_id_name if c[1]==config][0]

        self.project.get_current_model().drop_config_on_object(config_id, self.data_table[self.config_table.currentRow()][0], self.data_table[self.config_table.currentRow()][1])

        self.refresh()
        return

    def zoom(self):
        if self.config_table.currentItem() is not None:
            type, geom = self.project.execute("""select ST_GeometryType(ST_Envelope(geom)), ST_AsText(ST_Envelope(geom))  from {s}.{t} where id={i}
                                            """.format(s=self.project.get_current_model().name,
                                                       t=self.data_table[self.config_table.currentRow()][0],
                                                       i=self.data_table[self.config_table.currentRow()][1])).fetchone()
            self.__zoom_canvas(type, geom)

    def __zoom_canvas(self, type, geom):
        if type == 'ST_Point':
            # geom = 'POINT(X Y)'
            x, y = geom[6:-1].split(' ')
            point1 = QgsPoint(float(x)-10, float(y)-10)
            point2 = QgsPoint(float(x)+10, float(y)+10)
        elif type == 'ST_Polygon':
            # geom = 'POLYGON((X1 Y1,X2 Y2,X3 Y3,X4 Y4))'
            x1, y1 = geom[9:-2].split(',')[0].split(' ')
            x2, y2 = geom[9:-2].split(',')[2].split(' ')
            point1 = QgsPoint(float(x1), float(y1))
            point2 = QgsPoint(float(x2), float(y2))
        if point1:
            extent = QgsRectangle(point1, point2)
            self.iface.mapCanvas().setExtent(extent)
            self.iface.mapCanvas().refresh()
            self.close()
        else:
            return

    def sav(self):
        self.save()
        self.close()