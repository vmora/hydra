
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import time
from qgis.gui import QgsCollapsibleGroupBox
from PyQt4 import uic
from PyQt4.QtGui import QToolButton, QCheckBox, QSpacerItem, QVBoxLayout, QScrollArea, QHBoxLayout, QWidget, QDockWidget, QInputDialog, QIcon, QSizePolicy, QPushButton, QGroupBox
from PyQt4.QtCore import QCoreApplication
from hydra.database.mesh_interface import MeshInterface
from hydra.utility.map_point_tool import create_point
from hydra.gui.long_profil_interface import LongProfilInterface
from hydra.gui.select_menu import SelectTool

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

_plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
_layer_icons_dir = os.path.join(_plugin_dir, "ressources", "layer_icons")
_icons_dir = os.path.join(_plugin_dir, "ressources", "images")

_list_checkbox_ = {
    "links":["Links","Show links"],
    "sing":["Singularities","Show singularities"],
    "cross":["Cross sections", "Show cross sections"],
    "ground":["Ground level", "Show Ground level"],
    "baseline":["Base line", "Show base line"],
    "pipe":["Pipe ceiling line", "Show pipe's vault"],
    "rbank":["Reach right bank", "Show right bank"],
    "lbank":["Reach left bank", "Show left bank"],
    "waterz":["Water level Z", "Show water level"],
    "waterq":["Water flow Q", "Show water's debit"],
    "waterminmax":["Water min max Z", "Show water min/max z"],
    "overflow":["Overflow Z", "Show overflow Z"],
    "speed":["Water speed V", "Show water speed"]
    }


class LongProfilDock(QDockWidget):

    def __init__(self, title, current_project, iface, time_control_panneau):
        QDockWidget.__init__(self, title)

        self.__isreloading = False

        self.__currentproject = current_project
        self.__log_manager = current_project.log
        self.__iface = iface
        self.__currentDir = os.path.abspath(os.path.dirname(__file__))
        self.__time_control = time_control_panneau
        self.__time_control.set_active(False)
        self.__panneauwindow = None
        self.branchs=[]
        self.list_secondary_scenarios=[]
        self.branch_manager_widget = QWidget()
        self.scenario_comparator_widget = QWidget()

        uic.loadUi(os.path.join(self.__currentDir+"\widgets", "branch_manager.ui"), self.branch_manager_widget)
        uic.loadUi(os.path.join(self.__currentDir+"\widgets", "scenario_comparator_manager.ui"), self.scenario_comparator_widget)

        self.branch_manager_widget.btnAdd.clicked.connect(self.add_branch)
        self.branch_manager_widget.btnDel.clicked.connect(self.delete_branch)
        self.branch_manager_widget.btnUp.clicked.connect(self.move_up)
        self.branch_manager_widget.btnDown.clicked.connect(self.move_down)

        self.scenario_comparator_widget.btnAdd.clicked.connect(self.add_scenario_comparator)
        self.scenario_comparator_widget.btnDel.clicked.connect(self.delete_scenario_comparator)

        #premier layout : toolbox
        tool_box = QgsCollapsibleGroupBox(tr("Data"))
        tool_box.setLayout(QHBoxLayout())
        tool_box.layout().addWidget(self.__button(["Copy data","copy_data.svg"],self.copy_data),2)
        tool_box.layout().addWidget(self.__button(["Copy image","copy_image.svg"],self.copy_image),3)

        #deuxieme layout : labels
        labels_box = QGroupBox(tr("Labels"))
        labels_box.setLayout(QHBoxLayout())
        labels_box.layout().addWidget(self.__checkbox(_list_checkbox_['links'][0],_list_checkbox_['links'][1]),0)
        labels_box.layout().addWidget(self.__checkbox(_list_checkbox_['sing'][0],_list_checkbox_['sing'][1]),1)
        labels_box.layout().addWidget(self.__checkbox(_list_checkbox_['cross'][0],_list_checkbox_['cross'][1]),2)

        #troisieme layout : Geometry
        geom_box = QGroupBox(tr("Structures"))
        geom_box.setLayout(QVBoxLayout())
        geom_box.layout().addWidget(self.__checkbox(_list_checkbox_['ground'][0],_list_checkbox_['ground'][1]),0)
        geom_box.layout().addWidget(self.__checkbox(_list_checkbox_['baseline'][0],_list_checkbox_['baseline'][1]),1)
        geom_box.layout().addWidget(self.__checkbox(_list_checkbox_['pipe'][0],_list_checkbox_['pipe'][1]),2)
        geom_box.layout().addWidget(self.__checkbox(_list_checkbox_['lbank'][0],_list_checkbox_['lbank'][1]),3)
        geom_box.layout().addWidget(self.__checkbox(_list_checkbox_['rbank'][0],_list_checkbox_['rbank'][1]),4)

        #quatrieme layout : Results
        result_box = QGroupBox(tr("Results"))
        result_box.setLayout(QVBoxLayout())
        result_box.layout().addWidget(self.__checkbox(_list_checkbox_['waterz'][0],_list_checkbox_['waterz'][1]),0)
        result_box.layout().addWidget(self.__checkbox(_list_checkbox_['waterq'][0],_list_checkbox_['waterq'][1]),1)
        result_box.layout().addWidget(self.__checkbox(_list_checkbox_['waterminmax'][0],_list_checkbox_['waterminmax'][1]),2)
        result_box.layout().addWidget(self.__checkbox(_list_checkbox_['overflow'][0],_list_checkbox_['overflow'][1]),3)
        result_box.layout().addWidget(self.__checkbox(_list_checkbox_['speed'][0],_list_checkbox_['speed'][1]),4)

        self.labels_box = labels_box
        self.geom_box = geom_box
        self.result_box = result_box

        #branch manager
        self.branch_manager_box = QGroupBox(tr("Branch"))
        self.branch_manager_box.setLayout(QHBoxLayout())
        self.branch_manager_box.layout().addWidget(self.branch_manager_widget)

        #QlistWidget scenario comparaison
        self.list_scenario_box = QGroupBox(tr("Secondary scenarios"))
        self.list_scenario_box.setLayout(QHBoxLayout())
        self.list_scenario_box.layout().addWidget(self.scenario_comparator_widget)
        self.options_layout = QVBoxLayout()
        self.options_layout.addWidget(self.__checkbox(_list_checkbox_['waterz'][0],_list_checkbox_['waterz'][1]),0)
        self.options_layout.addWidget(self.__checkbox(_list_checkbox_['waterq'][0],_list_checkbox_['waterq'][1]),1)
        self.options_layout.addWidget(self.__checkbox(_list_checkbox_['waterminmax'][0],_list_checkbox_['waterminmax'][1]),2)
        self.options_layout.addWidget(self.__checkbox(_list_checkbox_['speed'][0],_list_checkbox_['speed'][1]),3)
        self.options_layout.addSpacing(40)
        self.list_scenario_box.layout().addLayout(self.options_layout)

        results_labels_layout = QHBoxLayout()
        results_labels_layout.addWidget(self.geom_box)
        results_labels_layout.addWidget(self.result_box)

        tool_box_time_layout = QHBoxLayout()
        tool_box_time_layout.addWidget(self.__time_control)
        tool_box_time_layout.addWidget(tool_box)

        # dock widget
        widget = QWidget()
        mainVBlayout = QVBoxLayout()
        mainVBlayout.addLayout(tool_box_time_layout)

        push_button = QPushButton(QIcon(os.path.join(_icons_dir, "profil.svg")),'Display graph', self)
        push_button.clicked.connect(self.display_graph)
        push_button.setToolTip("Display Graph")
        mainVBlayout.addWidget(push_button)

        mainVBlayout.addWidget(self.labels_box)
        mainVBlayout.addLayout(results_labels_layout)
        mainVBlayout.addWidget(self.branch_manager_box)
        mainVBlayout.addWidget(self.list_scenario_box)
        widget.setLayout(mainVBlayout)

        vlayout = QVBoxLayout()
        vlayout.addLayout(mainVBlayout)
        vlayout.addItem(QSpacerItem(20, 10, QSizePolicy.Expanding, QSizePolicy.Minimum))
        vlayout.addStretch()

        widget.setLayout(QHBoxLayout())
        widget.layout().addLayout(vlayout)
        widget.layout().addItem(QSpacerItem(10, 20, QSizePolicy.Minimum, QSizePolicy.Expanding))
        widget.layout().addStretch()

        scroll_area = QScrollArea()
        scroll_area.setWidget(widget)
        scroll_area.setWidgetResizable(True)
        self.setWidget(scroll_area)

    def __getattr__(self, att):
        if att=='display_ground_level':
            return self.geom_box.layout().itemAt(0).widget().isChecked()
        elif att=='display_base_line':
            return self.geom_box.layout().itemAt(1).widget().isChecked()
        elif att=='display_vault_line':
            return self.geom_box.layout().itemAt(2).widget().isChecked()
        elif att=='display_r_bank':
            return self.geom_box.layout().itemAt(3).widget().isChecked()
        elif att=='display_l_bank':
            return self.geom_box.layout().itemAt(4).widget().isChecked()
        elif att=='display_singularities':
            return self.labels_box.layout().itemAt(1).widget().isChecked()
        elif att=='display_links':
            return self.labels_box.layout().itemAt(0).widget().isChecked()
        elif att=='display_cross_sections':
            return self.labels_box.layout().itemAt(2).widget().isChecked()
        elif att=='display_water_line':
            return self.result_box.layout().itemAt(0).widget().isChecked()
        elif att=='display_water_flow':
            return self.result_box.layout().itemAt(1).widget().isChecked()
        elif att=='display_water_minmax':
            return self.result_box.layout().itemAt(2).widget().isChecked()
        elif att=='display_overflow_z':
            return self.result_box.layout().itemAt(3).widget().isChecked()
        elif att=='display_water_speed':
            return self.result_box.layout().itemAt(4).widget().isChecked()
        elif att=='display_water_line_comp':
            return self.options_layout.itemAt(0).widget().isChecked()
        elif att=='display_water_flow_comp':
            return self.options_layout.itemAt(1).widget().isChecked()
        elif att=='display_water_minmax_comp':
            return self.options_layout.itemAt(2).widget().isChecked()
        elif att=='display_water_speed_comp':
            return self.options_layout.itemAt(3).widget().isChecked()
        else:
            raise AttributeError

    def display_graph(self):
        if self.__panneauwindow is None:
            self.__panneauwindow = LongProfilInterface(self.__iface,self.__currentproject,self.__time_control,parent = self)
        self.__panneauwindow.display()
        if self.__currentproject.get_current_scenario():
            self.__time_control.set_active(True)

    def delete_branch(self):
        if self.branch_manager_widget.list_branch_widget.count()!=0:
            branch_selected_row = self.branch_manager_widget.list_branch_widget.currentRow()
            if branch_selected_row >= 0 :
                self.branchs.pop(branch_selected_row)
                self.reload_branchmanager()
                if self.__panneauwindow:
                    self.__panneauwindow.reload()

    def move_up(self):
        if self.__isreloading == False :
            self.__isreloading = True
            if self.branch_manager_widget.list_branch_widget.count()!=0:
                branch_selected_row = self.branch_manager_widget.list_branch_widget.currentRow()
                if branch_selected_row>0:
                    self.branchs.insert(branch_selected_row-1,self.branchs.pop(branch_selected_row))
                    self.reload_branchmanager()
                    if self.__panneauwindow:
                        self.__panneauwindow.reload()
            self.__isreloading = False

    def move_down(self):
        if self.__isreloading == False :
            self.__isreloading = True
            if self.branch_manager_widget.list_branch_widget.count()!=0:
                branch_selected_row = self.branch_manager_widget.list_branch_widget.currentRow()
                if branch_selected_row != (self.branch_manager_widget.list_branch_widget.count()-1) and branch_selected_row != -1:
                    self.branchs.insert(branch_selected_row+1,self.branchs.pop(branch_selected_row))
                    self.reload_branchmanager()
                    if self.__panneauwindow:
                        self.__panneauwindow.reload()
            self.__isreloading = False

    def reload_branchmanager(self):
        self.branch_manager_widget.list_branch_widget.clear()
        for branch in self.branchs:
            self.branch_manager_widget.list_branch_widget.addItem(branch.name)

    def reload_scn_comparator(self):
        self.scenario_comparator_widget.list_scenarios.clear()
        for scn in self.list_secondary_scenarios:
            self.scenario_comparator_widget.list_scenarios.addItem(scn[1])

    def add_branch(self):
        start = time.time()
        #print(start,'start')
        self.__idx = 0
        point = create_point(self.__iface.mapCanvas(), self.__log_manager)
        if point:
            #print("select_tool",time.time()-start)
            size = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()*10
            model = self.__currentproject.get_current_model().name

            self.__currentproject.execute("select {}.branch_update_fct()".format(model))
            self.__currentproject.commit()

            selected_branch = SelectTool(self.__currentproject, point, size, subset=[(model, 'reach'), (model, 'branch')])
            if selected_branch.name:
                #print("reload_labels",time.time()-start)

                self.branchs.append(selected_branch)
                #print("branch append",time.time()-start)

                self.branch_manager_widget.list_branch_widget.addItem(selected_branch.name)

                if self.__panneauwindow :
                    self.__panneauwindow.reload()

    def __button(self, properties=None, action="auto"):
        # properties = [text, icon]
        button = QToolButton()
        button.setToolTip(properties[0])
        button.setIcon(QIcon(_icons_dir + '\\' + properties[1]))
        if action != "auto":
            button.clicked.connect(action)
        return button

    def __checkbox(self,text,tooltip):
        checkbox = QCheckBox()
        checkbox.setText(text)
        checkbox.setToolTip(tooltip)
        checkbox.stateChanged.connect(self.checkbox_reload)
        return checkbox

    def copy_image (self):
        if self.__panneauwindow is not None:
            self.__panneauwindow.copy_image()

    def copy_data (self):
        if self.__panneauwindow is not None:
            self.__panneauwindow.copy_data()

    def add_scenario_comparator(self):
        scenarios = self.__currentproject.get_scenarios()
        if self.__currentproject.get_current_scenario() is not None:
            scenarios.pop(scenarios.index(self.__currentproject.get_current_scenario()))

        if scenarios:
            input_dialog, ok = QInputDialog.getItem(None,"Add scenario","Add a scenario to compare", [scn[1] for scn in scenarios],0,False)
            if input_dialog and ok :
                self.scenario_comparator_widget.list_scenarios.addItem(input_dialog)
                for scenario in scenarios :
                    if scenario[1] == input_dialog :
                        self.list_secondary_scenarios.append([scenario[0],scenario[1],""])
                        if self.__panneauwindow:
                            self.__panneauwindow.init_scn_comparator()
                            self.__panneauwindow.reload()

    def delete_scenario_comparator(self):
        if self.scenario_comparator_widget.list_scenarios.count()!=0:
            deleted_scn = self.list_secondary_scenarios[self.scenario_comparator_widget.list_scenarios.currentRow()]
            if self.scenario_comparator_widget.list_scenarios.currentRow() != -1 :
                if self.__panneauwindow :
                    self.__panneauwindow.remove_secondary_scn(deleted_scn)
                self.list_secondary_scenarios.pop(self.scenario_comparator_widget.list_scenarios.currentRow())
                self.reload_scn_comparator()
                if self.__panneauwindow :
                    self.__panneauwindow.init_scn_comparator()
                    self.__panneauwindow.reload()

    def checkbox_reload(self):
        if self.__panneauwindow:
            self.__panneauwindow.reload()
