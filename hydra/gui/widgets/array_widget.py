# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
import os
from PyQt4 import uic
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import pyqtSignal
from PyQt4.QtGui import QApplication, QTableWidgetItem, QWidget, QGridLayout
import hydra.utility.string as string

class ArrayWidget(QWidget):
    data_edited = pyqtSignal()

    def __init__(self, title_columns, size, array, draw_graph_func=None, parent=None, padding=False):
        QWidget.__init__(self, parent)

        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "array_widget.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self)
        parent.setLayout(tmplayout)


        self.max_len = size[0]
        self.width = size[1]
        self.draw_graph_func = draw_graph_func
        self.padding = padding

        assert len(title_columns) == self.width
        self.table.setColumnCount(self.width)
        self.table.setHorizontalHeaderLabels(title_columns)

        if array:
            assert len(array) <= self.max_len
            self.set_table_items(array)
            self.__draw_graph()

        self.table.cellChanged.connect(self.__cell_changed)
        self.table.currentCellChanged.connect(self.__draw_graph)

        self.add.clicked.connect(self.__add_row)
        self.delete.clicked.connect(self.__delete_row)

        self.show()

        self.table.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)

    def __add_row(self):
        if self.table.rowCount() <self.max_len:
            self.table.insertRow(self.table.currentRow())
        elif self.table.rowCount()==self.max_len:
            if self.__row_is_empty(self.max_len-1):
                self.table.insertRow(self.table.currentRow())
                self.table.setRowCount(self.max_len)
        self.__set_length()

    def __delete_row(self):
        selected = self.table.selectedRanges()
        if selected:
            for i in range(selected[0].topRow(), selected[0].bottomRow() + 1):
                self.table.removeRow(selected[0].topRow())
        self.__set_length()

    def keyPressEvent(self, event):
        if self.table.hasFocus():
            if event.key() == QtCore.Qt.Key_V and  (event.modifiers() & QtCore.Qt.ControlModifier):
                self.__paste()
                event.accept()
            elif event.key() == QtCore.Qt.Key_C and  (event.modifiers() & QtCore.Qt.ControlModifier):
                self.__copy()
                event.accept()
            else:
                event.ignore()
        else:
            event.ignore()

    def __paste(self):
        text = QtGui.QApplication.clipboard().text()
        row0 = self.table.currentRow()
        # placer ce contenu dans le QTableWidget
        for i, textline in enumerate(text.split('\n')):
            for j, celltext in enumerate(textline.split('\t')):
                if celltext.strip()!='':
                    self.table.setItem(row0+i, j, QtGui.QTableWidgetItem(celltext))

    def __copy(self):
        selected = self.table.selectedRanges()
        copied_text = ""
        for i in range(selected[0].topRow(), selected[0].bottomRow() + 1):
            for j in range(selected[0].leftColumn(), selected[0].rightColumn() + 1):
                try:
                    copied_text += self.table.item(i, j).text() + "\t"
                except AttributeError:
                    # quand une case n'a jamais été initialisée
                    copied_text += "\t"
            copied_text = copied_text[:-1] + "\n"  # le [:-1] élimine le '\t' en trop
        copied_text = copied_text[:-1]  # le [:-1] élimine le '\n' en trop
        # enregistrement dans le clipboard
        QtGui.QApplication.clipboard().setText(copied_text)

    def __draw_graph(self):
        datas = self.get_table_items()
        self.draw_graph_func(datas)

    def __cell_changed(self,row,column):
        if row == (self.table.rowCount()-1):
            self.__set_length()

    def set_table_items(self, array):
        self.table.setRowCount(0) # clear data first
        assert len(array) <= self.max_len
        n = len(array)
        #self.table.setRowCount(n+1)
        effective_rows = 0
        for row in range(0, n):
            if array[row] != [None for k in range(self.width)]:
                self.table.insertRow(self.table.rowCount())
                for i in range(self.width):
                    if not self.table.isColumnHidden(i):
                        self.table.setItem(effective_rows,i,QTableWidgetItem(string.get_str(array[row][i])))
                effective_rows = effective_rows +1
        self.__set_length()
        self.__draw_graph()

    def get_table_items(self):
        n = self.table.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            if self.__row_is_full(i):
                result.append(list())
                m = self.table.columnCount()
                for j in range(self.width):
                    if  not self.table.isColumnHidden(j):
                        result[index_list].append(string.get_sql_float(self.table.item(i,j).text()))
                    else:
                        result[index_list].append(None)
                index_list = index_list + 1
        if len(result) <self.max_len and self.padding:
            for j in range(len(result), self.max_len):
                result.append(list())
                for j in range(self.width):
                    result[index_list].append(None)
                index_list = index_list + 1
        return result

    def __set_length(self):
        n = self.table.rowCount()
        if self.__row_is_full(n-1) and n < self.max_len:
            self.table.setRowCount(n+1)
        if self.table.rowCount() > self.max_len:
            self.table.setRowCount(self.max_len)
        elif self.table.rowCount()==0:
            self.table.setRowCount(1)

    def __row_is_empty(self, row):
        row_empty = True
        for i in range(self.width):
            if self.table.item(row,i) is not None and not self.table.isColumnHidden(i):
                row_empty = False
        return row_empty

    def __row_is_full(self, row):
        row_full = True
        for i in range(self.width):
            if self.table.item(row,i) is None and not self.table.isColumnHidden(i):
                row_full = False
        return row_full