
################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from PyQt4.QtGui import QApplication, QWidget, QMessageBox
from PyQt4.QtCore import Qt
from graph_widget import GraphWidget
from PyQt4.QtGui import QTableWidgetItem
from PyQt4 import uic
import hydra.utility.string as string
from hydra.gui.base_dialog import tr
from shapely.geometry import LineString, Point
from shapely import wkt
from hydra.gui.widgets.combo_with_values import ComboWithValues
from ..widgets.array_widget import ArrayWidget
import sys, traceback
import time
import numpy
import os
from math import sqrt

class ValleyGeomWidget(QWidget):
    def __init__(self, project, parent=None, valley_geom_table=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "valley_geom_widget.ui"), self)
        self.project = project
        self.model = self.project.get_current_model()
        self.transect = None
        self.graph_vcs_geom = GraphWidget(self.graph_vcs_geom_placeholder, False, True)
        self.valley_geom_table = valley_geom_table
        self.__topo_profile = None
        self.__points_profile = None
        self.s_left_bank = None
        self.s_right_bank = None
        self.transposed_left_bank=None
        self.transposed_right_bank=None
        self.inter_reach=None
        self.__loading = False
        self.vcs_geom_name.textChanged.connect(self.vcs_geom_update_name)

        self.vcs_geom_xyarray_maj_lbank = ArrayWidget(["Z (m)", "Distance (m)"], [4,2], None, self.__refresh, self.vcs_geom_xyarray_maj_lbank_placeholder)
        self.vcs_geom_xyarray_maj_rbank = ArrayWidget(["Z (m)", "Distance (m)"], [4,2], None, self.__refresh, self.vcs_geom_xyarray_maj_rbank_placeholder)
        self.vcs_geom_xyarray_min = ArrayWidget(["Z (m)", "Width (m)"], [6,2], None, self.__refresh, self.vcs_geom_xyarray_min_placeholder)

        self.vcs_geom_zlevee_lb.textChanged.connect(self.__refresh)
        self.vcs_geom_zlevee_rb.textChanged.connect(self.__refresh)
        self.set_enabled(False)
        self.btn_reload.clicked.connect(self.__reload_from_linestring)
        self.btn_interpolate.clicked.connect(self.__interpolate_from_linestring)
        self.graph_vcs_geom.set_zoom_enabled(True)
        self.graph_vcs_geom.canvas.mpl_connect('button_press_event', self.__on_press)
        self.graph_vcs_geom.canvas.mpl_connect('motion_notify_event', self.__on_move)
        self.constrain_combo = ComboWithValues(self.constrain_combo_placeholder)
        self.__init_combo_constrain()
        self.constrain_combo.activated.connect(self.__constrain_changed)
        self.__move=None

    def __refresh(self, datas = None):
        x_min, x_max, y_min, y_max = self.graph_vcs_geom.get_zoomed_zone()
        self.__draw_vcs_geom(datas)
        self.graph_vcs_geom.set_zoomed_zone(x_min, x_max, y_min, y_max)

    def __init_combo_constrain(self):
        if self.model is not None:
            constrains = self.project.execute("""select name, id from {model}.constrain order by name asc;""".format(
                model=self.model.name
                )).fetchall()
            self.constrain_combo.clear()
            # self.constrain_combo.addItem(tr("None"), None)
            self.constrain_combo.addItem("", None)
            for constrain in constrains:
                self.constrain_combo.addItem(constrain[0], constrain[1])
            self.constrain_combo.set_selected_value(None)
            self.constrain_combo.setEditable(True)

    def __constrain_changed(self):
        self.transect = self.constrain_combo.get_selected_value()
        if self.model is not None and self.transect is not None:
            self.__loading = True
            self.__load_profile(self.transect, self.model.name)
            self.__loading = False
        else:
            self.__topo_profile = None
            self.__points_profile = None
        self.__draw_vcs_geom(None)

    def __interpolate_from_linestring(self):
        self.transect = self.constrain_combo.get_selected_value()
        if self.model is not None and self.transect is not None:
            self.__loading = True
            self.__load_pl1d(self.transect, self.model.name, True)
            self.__load_profile(self.transect, self.model.name)
            self.__loading = False
        else:
            self.__topo_profile = None
            self.__points_profile = None
        self.__draw_vcs_geom(None)

    def __reload_from_linestring(self):
        self.transect = self.constrain_combo.get_selected_value()
        if self.model is not None and self.transect is not None:
            self.__loading = True
            self.__load_pl1d(self.transect, self.model.name, False)
            self.__load_profile(self.transect, self.model.name)
            self.__loading = False
        else:
            self.__topo_profile = None
            self.__points_profile = None
        self.__draw_vcs_geom(None)

    def __load_pl1d(self, transect_id, model, interpolate):
        transect, = self.project.execute("""select geom from {model}.constrain where id={id}""".format(
                    model=model,
                    id=transect_id
                    )).fetchone()
        try:
            t_ignore_pt = self.chk_points_ignore.isChecked()
            t_distance_pt=int(self.distance.text())
            t_discret=int(self.discretisation.text())
            pl1d, = self.project.execute("""select {model}.profile_from_transect('{geometry}', {interpole}, {discret}, {ignore_pt}, {distance})""".format(model=model, geometry=transect, interpole=interpolate, discret=str(t_discret), ignore_pt=t_ignore_pt, distance=str(t_distance_pt))).fetchone()

            if pl1d is None:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Information)
                msg.setWindowTitle(tr("Geometry generation failed"))
                msg.setText(tr("No profile found, check that your constrain line crosses a reach."))
                msg.setStandardButtons(QMessageBox.Ok)
                msg.exec_()
                return

            self.vcs_geom_xyarray_maj_lbank.table.setRowCount(0)
            self.vcs_geom_xyarray_min.table.setRowCount(0)
            self.vcs_geom_xyarray_maj_rbank.table.setRowCount(0)

            self.vcs_geom_xyarray_maj_lbank.table.setRowCount(4)
            self.vcs_geom_xyarray_min.table.setRowCount(6)
            self.vcs_geom_xyarray_maj_rbank.table.setRowCount(4)

            for row in range(0, 4):
                self.vcs_geom_xyarray_maj_lbank.table.setItem(row,0,QTableWidgetItem(string.get_str(pl1d[row][0])))
                self.vcs_geom_xyarray_maj_lbank.table.setItem(row,1,QTableWidgetItem(string.get_str(pl1d[row][1])))
            for row in range(4, 10):
                self.vcs_geom_xyarray_min.table.setItem(row-4,0,QTableWidgetItem(string.get_str(pl1d[row][0])))
                self.vcs_geom_xyarray_min.table.setItem(row-4,1,QTableWidgetItem(string.get_str(pl1d[row][1])))
            for row in range(10, 14):
                self.vcs_geom_xyarray_maj_rbank.table.setItem(row-10,0,QTableWidgetItem(string.get_str(pl1d[row][0])))
                self.vcs_geom_xyarray_maj_rbank.table.setItem(row-10,1,QTableWidgetItem(string.get_str(pl1d[row][1])))
        except:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText(tr("Geometry generation failed, click details below to show more information."))
            msg.setWindowTitle(tr("Geometry generation failed"))
            msg.setDetailedText(''.join(traceback.format_exception(*sys.exc_info())[-2:]).strip().replace('\n',': '))
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()

    def __load_profile(self, transect_id, model):
        mid_center=None
        if self.vcs_geom_xyarray_maj_lbank.table.rowCount()>=4 and self.vcs_geom_xyarray_min.table.rowCount()>=6:
            item1 = self.vcs_geom_xyarray_maj_lbank.table.item(3,1)
            item2 = self.vcs_geom_xyarray_min.table.item(5,1)
            if item1 is not None and item2 is not None:
                mid_center = string.get_sql_float(item1.text()) + string.get_sql_float(item2.text())/2
        t_ignore_pt=self.chk_points_ignore.isChecked()
        t_distance_pt=int(self.distance.text())
        t_discret=int(self.discretisation.text())
        transect, = self.project.execute("""select geom from {model}.constrain where id={id}""".format(
                    model=model,
                    id=transect_id
                    )).fetchone()
        inter = self.project.execute("""
                with reach as (
                    select r.id, st_intersection(r.geom, '{transect}'::geometry) as inter, geom
                    from {model}.reach as r
                    where st_intersects(r.geom, '{transect}'::geometry)
                ),
                pk as (
                    select st_linelocatepoint(geom, inter) as pk from reach
                ),
                topo_transect as (
                    select project.set_altitude(
                        project.discretize_line('{transect}'::geometry, st_length('{transect}'::geometry)/{discret})
                        ) as geom
                )
                select r.id as reach, r.inter, t.geom as topo_transect from reach as r, topo_transect as t
                """.format(
                    model=model,
                    transect=transect,
                    discret=str(t_discret)
                    )).fetchall()
        if inter:
            self.inter_reach = wkt.loads((self.project.execute("""select wkb_loads('{}')""".format(inter[0][1])).fetchone())[0])
            transect_geom = wkt.loads((self.project.execute("""select wkb_loads('{}')""".format(transect)).fetchone())[0])

            if len(transect_geom.coords)==2:
                coords = transect_geom.coords
                new_coords_start, new_coords_end = self.__calc_new_start_end(coords[0],coords[1])
                geom=[new_coords_start]
                geom.extend(coords)
                geom.append(new_coords_end)
                transect_geom = LineString(geom)
                left_bank_pt = 2

            if len(transect_geom.coords)>=3:
                s_center = transect_geom.project(self.inter_reach)
                s = numpy.array([transect_geom.project(Point(p)) for p in transect_geom.coords])
                left_bank_pt = len(s[s < s_center])
                rigth_bank_pt = len(s[s > s_center])
                coords = transect_geom.coords
                if left_bank_pt==1:
                    new_coords_start, new_coords_end = self.__calc_new_start_end(coords[0],coords[1])
                    geom=[new_coords_start]
                    geom.extend(coords)
                    transect_geom = LineString(geom)
                    left_bank_pt = 2
                if rigth_bank_pt==1:
                    new_coords_start, new_coords_end = self.__calc_new_start_end(coords[left_bank_pt-1],coords[left_bank_pt])
                    geom=[]
                    geom.extend(coords)
                    geom.append(new_coords_end)
                    transect_geom = LineString(geom)

            s_center = transect_geom.project(self.inter_reach)
            s = numpy.array([transect_geom.project(Point(p)) for p in transect_geom.coords])
            self.s_left_bank = s[left_bank_pt-1]
            self.s_right_bank = s[left_bank_pt]

            if mid_center is None or mid_center==0:
                mid_center = (self.s_left_bank + self.s_right_bank)/2
            self.transposed_left_bank = self.s_left_bank - mid_center
            self.transposed_right_bank = self.s_right_bank - mid_center
            self.__topo_profile = numpy.array([(transect_geom.project(Point(p))-mid_center, p[2]) for p in wkt.loads((self.project.execute("""select wkb_loads('{}')""".format(inter[0][2])).fetchone())[0]).coords])
            if t_ignore_pt:
                self.__points_profile = None
            else:
                points_profile_geom = self.project.execute("""
                        select p.id,
                        p.z_ground as z,
                        st_linelocatepoint(cs.geom, p.geom)*st_length(cs.geom) as pk
                        from {model}.constrain as cs, project.points_xyz as p
                        where cs.id={id}
                        and (st_intersects(ST_Buffer(cs.geom, {distance}, 'endcap=flat join=round'),p.geom )=true
                        or st_startpoint(cs.geom)=p.geom
                        or st_endpoint(cs.geom)=p.geom)
                        order by pk""".format(
                            model=model,
                            id=transect_id,
                            distance=str(t_distance_pt)
                            )).fetchall()
                if len(points_profile_geom)>1:
                    self.__points_profile = numpy.array([(p[2]-mid_center, p[1]) for p in points_profile_geom])
                else:
                    self.__points_profile = None
        else:
            self.__topo_profile = None
            self.__points_profile = None

    def __calc_new_start_end(self, coordstart, coordend):
        inc_x = (coordend[0] - coordstart[0])/(coordend[1]-coordstart[1])
        inc_y = (coordend[1]-coordstart[1])/(coordend[0] - coordstart[0])
        dist = sqrt((coordend[0] - coordstart[0])**2 + (coordend[1]-coordstart[1])**2)
        inc_x = (coordend[0] - coordstart[0])/dist
        inc_y = (coordend[1]-coordstart[1])/dist

        new_coords_start = (coordstart[0]-inc_x, coordstart[1]-inc_y, coordstart[2])
        new_coords_end = (coordend[0]+inc_x, coordend[1]+inc_y, coordend[2])
        return new_coords_start, new_coords_end

    def set_items(self, id_valley_geom):
        if id_valley_geom is not None:
            self.__loading=True
            name, rlambda, rmu1, rmu2, zbmin_array, zlevee_lb, zlevee_rb, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, self.transect, t_ignore_pt, t_discret, t_distance_pt = self.project.execute("""
                                                                                            select name, rlambda, rmu1, rmu2, zbmin_array, zlevee_lb, zlevee_rb,
                                                                                            zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, transect, t_ignore_pt, t_discret, t_distance_pt
                                                                                            from {}.valley_cross_section_geometry where id={}""".format(self.model.name, id_valley_geom)).fetchone()

            self.vcs_geom_name.setText(name)
            self.vcs_geom_rlambda.setText(string.get_str(rlambda))
            self.vcs_geom_rmu1.setText(string.get_str(rmu1))
            self.vcs_geom_rmu2.setText(string.get_str(rmu2))
            self.vcs_geom_zlevee_lb.setText(string.get_str(zlevee_lb))
            self.vcs_geom_zlevee_rb.setText(string.get_str(zlevee_rb))

            self.vcs_geom_xyarray_min.set_table_items(zbmin_array)
            self.vcs_geom_xyarray_maj_lbank.set_table_items(zbmaj_lbank_array)
            self.vcs_geom_xyarray_maj_rbank.set_table_items(zbmaj_rbank_array)

            self.chk_points_ignore.setChecked(t_ignore_pt)
            self.discretisation.setText(string.get_str(t_discret))
            self.distance.setText(string.get_str(t_distance_pt))
            if self.model is not None and self.transect is not None:
                self.constrain_combo.set_selected_value(self.transect)
                self.__load_profile(self.transect, self.model.name)
            else:
                self.constrain_combo.set_selected_value(None)
                self.__topo_profile = None
                self.__points_profile = None

            self.__loading=False
            self.__draw_vcs_geom(None)
            self.set_enabled(True)

    def set_enabled(self, bool=True):
        self.vcs_geom_name.setEnabled(bool)
        self.vcs_geom_xyarray_min.setEnabled(bool)
        self.vcs_geom_xyarray_maj_lbank.setEnabled(bool)
        self.vcs_geom_xyarray_maj_rbank.setEnabled(bool)
        self.vcs_geom_rlambda.setEnabled(bool)
        self.vcs_geom_rmu2.setEnabled(bool)
        self.vcs_geom_rmu1.setEnabled(bool)
        self.vcs_geom_zlevee_lb.setEnabled(bool)
        self.vcs_geom_zlevee_rb.setEnabled(bool)
        self.graph_vcs_geom.canvas.setVisible(bool)

    def vcs_geom_update_name(self):
        if self.__loading:
            return
        if self.valley_geom_table:
            self.valley_geom_table.table.item(self.valley_geom_table.table.row(self.valley_geom_table.table.selectedItems()[0]),1).setText(self.vcs_geom_name.text())

    def save_geom(self, id_valley_geom):
        name = self.vcs_geom_name.text()
        rlambda = string.get_sql_float(self.vcs_geom_rlambda.text())
        rmu1 = string.get_sql_float(self.vcs_geom_rmu1.text())
        rmu2 = string.get_sql_float(self.vcs_geom_rmu2.text())
        zlevee_lb = string.get_sql_float(self.vcs_geom_zlevee_lb.text())
        zlevee_rb = string.get_sql_float(self.vcs_geom_zlevee_rb.text())
        zbmin_array = string.list_to_sql_array(self.vcs_geom_xyarray_min.get_table_items())
        zbmaj_lbank_array = string.list_to_sql_array(self.vcs_geom_xyarray_maj_lbank.get_table_items())
        zbmaj_rbank_array = string.list_to_sql_array(self.vcs_geom_xyarray_maj_rbank.get_table_items())
        transect = self.constrain_combo.get_selected_value() if self.constrain_combo.get_selected_value() is not None else 'null'

        t_ignore_pt=self.chk_points_ignore.isChecked()
        t_distance_pt=int(self.distance.text())
        t_discret=int(self.discretisation.text())

        self.model.update_valley_section_geometry(id_valley_geom, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb, name, transect, t_discret, t_ignore_pt, t_distance_pt)

    def __draw_vcs_geom(self, datas):
        # datas is not used, we get all datas from the 3 array widget
        if self.__loading:
            return

        title = self.vcs_geom_name.text()
        zlevee_lb = string.get_sql_float(self.vcs_geom_zlevee_lb.text())
        zlevee_rb = string.get_sql_float(self.vcs_geom_zlevee_rb.text())
        zbmin_array = self.vcs_geom_xyarray_min.get_table_items()
        zbmaj_lbank_array = self.vcs_geom_xyarray_maj_lbank.get_table_items()
        zbmaj_rbank_array = self.vcs_geom_xyarray_maj_rbank.get_table_items()

        if len(zbmin_array)>0 :
            self.graph_vcs_geom.clear()
            if self.__topo_profile is not None:
                self.graph_vcs_geom.add_line(
                    [r[0] for r in self.__topo_profile],
                    [r[1] for r in self.__topo_profile],
                    '#c3c3c3',
                    title,
                    tr("Width (m)"), tr("Z (m)"))

            if self.__points_profile is not None:
                self.graph_vcs_geom.add_line(
                    [r[0] for r in self.__points_profile],
                    [r[1] for r in self.__points_profile],
                    '#FF0000',
                    title,
                    tr("Width (m)"), tr("Z (m)"))
            if not (len(zbmin_array)==1 and zbmin_array[0][0]==0 \
                and len(zbmaj_lbank_array)==1 and zbmaj_lbank_array[0][0]==0 \
                and len(zbmaj_rbank_array)==1 and zbmaj_rbank_array[0][0]==0):

                zbmin_array.insert(0, [zbmin_array[0][0], 0])

                self.graph_vcs_geom.add_symetric_line(
                    [r[1]/2 for r in zbmin_array],
                    [r[0] for r in zbmin_array],
                    "#0066ff",
                    title,
                    tr("Width (m)"), tr("Z (m)"), marker=".")

                d0_lb = -zbmin_array[-1][1]/2
                left_bank = [[zbmin_array[-1][0], d0_lb]]
                d0_lb = d0_lb

                for p in zbmaj_lbank_array:
                    left_bank.append([p[0], d0_lb-p[1]])

                self.graph_vcs_geom.add_line(
                    [r[1] for r in left_bank],
                    [r[0] for r in left_bank],
                    "#0066ff",
                    title,
                    tr("Width (m)"), tr("Z (m)"), marker=".")

                if zlevee_lb> zbmin_array[-1][0]:
                    self.graph_vcs_geom.add_line(
                        [d0_lb-zbmaj_lbank_array[0][1], d0_lb-zbmaj_lbank_array[0][1]],
                        [zbmaj_lbank_array[0][0], zlevee_lb],
                        "r",
                        title,
                        tr("Width (m)"), tr("Z (m)"))

                d0_rb = zbmin_array[-1][1]/2
                right_bank = [[zbmin_array[-1][0], d0_rb]]
                d0_rb = d0_rb

                for p in zbmaj_rbank_array:
                    right_bank.append([p[0], d0_rb+p[1]])

                self.graph_vcs_geom.add_line(
                    [r[1] for r in right_bank],
                    [r[0] for r in right_bank],
                    "#0066ff",
                    title,
                    tr("Width (m)"), tr("Z (m)"), marker=".")

                if zlevee_rb> zbmin_array[-1][0]:
                    self.graph_vcs_geom.add_line(
                        [d0_rb+zbmaj_rbank_array[0][1], d0_rb+zbmaj_rbank_array[0][1]],
                        [zbmaj_rbank_array[0][0], zlevee_rb],
                        "r",
                        title,
                        tr("Width (m)"), tr("Z (m)"))

            if self.__topo_profile is not None:
                ymin = numpy.amin([r[1] for r in self.__topo_profile])
                ymax = numpy.amax([r[1] for r in self.__topo_profile])
                if self.s_left_bank is not None:
                    self.graph_vcs_geom.add_line([self.transposed_left_bank, self.transposed_left_bank],
                        [ymin, ymax],
                        color="g" if self.__move!="left" else "y", marker="D")
                if self.s_right_bank is not None:
                    self.graph_vcs_geom.add_line([self.transposed_right_bank, self.transposed_right_bank],
                        [ymin, ymax],
                        color="g" if self.__move!="right" else "y", marker="D")
        else:
            self.graph_vcs_geom.canvas.setVisible(False)

    def __on_press(self, event):
        "initialise data for moving banks"
        if event.button == 1:
            x_min, x_max, y_min, y_max = self.graph_vcs_geom.get_zoomed_zone()
            if self.__move == None:
                if self.transposed_left_bank is not None and abs((self.transposed_left_bank-event.xdata)/self.transposed_left_bank)<0.1:
                    self.__move="left"
                    self.__start_x=self.transposed_left_bank
                    self.__draw_vcs_geom(None)
                elif self.transposed_right_bank is not None and abs((self.transposed_right_bank-event.xdata)/self.transposed_right_bank)<0.1:
                    self.__move="right"
                    self.__start_x=self.transposed_right_bank
                    self.__draw_vcs_geom(None)
            else:
                if abs((self.transposed_left_bank-event.xdata)/self.transposed_left_bank)<0.1 or abs((self.transposed_right_bank-event.xdata)/self.transposed_right_bank)<0.1:
                    self.__move = None
                    self.__draw_vcs_geom(None)
                else:
                    self.__calc_transect(event)
                    self.__move = None
                    self.__constrain_changed()
            self.graph_vcs_geom.set_zoomed_zone(x_min, x_max, y_min, y_max)

    def __on_move(self, event):
        if self.transposed_left_bank is None or self.transposed_right_bank is None or event.xdata is None:
            QApplication.restoreOverrideCursor()
        elif abs((self.transposed_left_bank-event.xdata)/self.transposed_left_bank)<0.1 or abs((self.transposed_right_bank-event.xdata)/self.transposed_right_bank)<0.1:
            QApplication.setOverrideCursor(Qt.PointingHandCursor)
        else:
            QApplication.restoreOverrideCursor()

    def __calc_transect(self, event):
        if event.button == 1 and self.__move is not None and self.transect is not None:
            def get_point(geom, pk):
                for i in range(0, len(geom.coords)):
                    if geom.project(Point(geom.coords[i]))==pk:
                        return i, geom.coords[i]
                return None, None

            def get_extrapoled_point(p1, p2, pk):
                'Creates a line extrapoled in p1->p2 direction'
                l = LineString([p1,p2]).length
                ratio = (l+pk)/l
                a = p1
                b = (p1[0]+ratio*(p2[0]-p1[0]), p1[1]+ratio*(p2[1]-p1[1]))
                return b

            delta = event.xdata - self.__start_x

            transect, = self.project.execute("""select geom from {model}.constrain where id={id}""".format(
                model=self.model.name,
                id=self.transect
                )).fetchone()

            transect_geom = wkt.loads((self.project.execute("""select wkb_loads('{}')""".format(transect)).fetchone())[0])
            s_center = transect_geom.project(self.inter_reach)
            s = numpy.array([transect_geom.project(Point(p)) for p in transect_geom.coords])
            s_left_bank, s_right_bank = numpy.sort(s[numpy.argsort(numpy.abs(s - s_center))[:2]])

            index_l, point_l=get_point(transect_geom, s_left_bank)
            index_r, point_r=get_point(transect_geom, s_right_bank)

            if point_l==None or point_r==None:
                return

            index=None
            z_coord=None
            y_coord=None
            x_coord=None

            if self.__move=="left":
                index=index_l
                pt = get_extrapoled_point(point_r, point_l, -delta)
                x_coord=pt[0]
                y_coord=pt[1]
                z_coord=point_l[2]
            else:
                index=index_r
                pt = get_extrapoled_point(point_l, point_r, delta)
                x_coord=pt[0]
                y_coord=pt[1]
                z_coord=point_l[2]

            if index is not None:
                self.project.execute(""" with geometry as (
                                select geom from {model}.constrain where id={id}
                            )
                            update {model}.constrain
                            set geom=st_setpoint(geom, {index}, st_geomfromewkt('POINT({x} {y} {z})'))
                            where id={id};""".format(model=self.model.name, id=self.transect, index=index, x=x_coord, y=y_coord, z=z_coord))
