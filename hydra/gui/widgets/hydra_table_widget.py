# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from PyQt4.QtGui import QTableWidgetItem, QWidget, QGridLayout
from PyQt4 import uic
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import pyqtSignal
import os
import hydra.utility.string as string

class HydraTableWidget(QWidget):
    data_edited = pyqtSignal()

    @staticmethod
    def __quote(value):
        if ((isinstance(value, str) or isinstance(value, unicode)) and len(value)==0):
            return "null"

        return "'"+value+"'" \
            if (isinstance(value, str) or isinstance(value, unicode))\
            and value != "null" and value != "default" and value[0] != "'"  else value


    def __init__(self, project, data_columns,
            title_columns, dbname, table_name, callbacks, filter="",
            key_id="id", parent=None, autoInit=True, showEdit=False, file_columns=[]):

        self.__loading = True
        QWidget.__init__(self, parent)

        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "hydra_table_widget.ui"), self)
        self.__editable = False
        self.set_editable(self.__editable)

        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)

        self.project = project
        self.data_cols = data_columns
        self.__dbname = dbname
        self.table_name = table_name
        self.__filter = filter
        self.__key_id = key_id
        self.__file_columns = file_columns

        self.table.setColumnCount(0)
        self.table.setColumnCount(len(self.data_cols))
        self.table.setHorizontalHeaderLabels(title_columns)
        self.table.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)

        self.btnAdd.clicked.connect(callbacks[0])
        self.btnDel.clicked.connect(callbacks[1])

        if showEdit:
            self.btnEdit.clicked.connect(callbacks[2])
        self.btnEdit.setVisible(showEdit)

        self.table.sortItems(1)
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.setSortingEnabled(True)
        self.table.itemChanged.connect(self.on_data_edited)

        if autoInit:
            self.update_data()

        self.show()
        self.__loading = False

    def set_dbname(self, dbname):
        self.__dbname = dbname
        self.update_data()

    def on_data_edited(self, new_value):
        if not self.__loading:
            self.table.resizeColumnsToContents()
            self.table.horizontalHeader().setStretchLastSection(True);
            self.data_edited.emit()

    def set_editable(self, value):
        self.__editable = value
        if value == True:
            self.table.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
            self.table.setEditTriggers(QtGui.QAbstractItemView.AllEditTriggers)
            rows = self.table.rowCount()
            for i in range(rows):
                self.table.item(i,0).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        else:
            self.table.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
            self.table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

    def set_filter(self, filter):
        if (filter != self.__filter):
            self.__filter = filter
            self.update_data()

    def add_row(self, new_row_data=(), additional_rows=None):
        additional_rows = "," + ",".join(additional_rows) if additional_rows is not None else ""

        if len(new_row_data)<len(self.data_cols):
            new_row_data = list(new_row_data)
            while len(new_row_data)<len(self.data_cols)-1:
                new_row_data.append('null')

        new_row_formated = []
        for i in range(len(new_row_data)):
            if i+1 in self.__file_columns:
                new_row_formated.append(self.project.pack_path(str(new_row_data[i])))
            else:
                new_row_formated.append(str(new_row_data[i]))

        self.project.execute("insert into {}.{} ({}) values ({});".format(self.__dbname, self.table_name,
            ",".join([x for x in self.data_cols if x != self.__key_id]) + additional_rows,
            ",".join(HydraTableWidget.__quote(str(value)) for value in new_row_formated)))
        self.update_data()
        self.table.selectRow(self.table.rowCount()-1)

    def save_selected_row(self):
        items = self.table.selectedItems()
        if len(items)>0:
            selected_row = self.table.row(items[0])
            id = self.table.item(selected_row,0).text()
            self.data_cols
            data = list()
            for i in range(0, len(self.data_cols)):
                if not self.data_cols[i]==self.__key_id:
                    if string.isfloat(self.table.item(selected_row,i).text()):
                        value = string.get_sql_float(self.table.item(selected_row,i).text())
                    else:
                        value = self.table.item(selected_row,i).text()
                    if value is not None:
                        data.append("{}={}".format(self.data_cols[i], HydraTableWidget.__quote(self.project.pack_path(value) if i in self.__file_columns else str(value))))
            to_insert = ",".join(data)
            self.project.execute("update {}.{} set {} where {}={};".format(self.__dbname, self.table_name, to_insert, self.__key_id, id))

    def del_selected_row(self):
        items = self.table.selectedItems()
        if len(items)>0:
            selected_row = self.table.row(items[0])
            id = self.table.item(selected_row,0).text()
            self.project.execute("delete from {}.{} where {}={};".format(self.__dbname,
                self.table_name, self.__key_id, id))
            self.update_data()

    def to_str(self, value):
        if value is None:
            return ""
        elif type(value)==int:
            return str(value)
        elif type(value)==float:
            return str(value)
        elif type(value)!=unicode:
            return value.decode('utf8')
        else:
            return value

    def update_data(self):
        self.__loading = True
        datas = self.project.execute("select {} from {}.{} {} order by {};".format(
        ",".join(self.data_cols), self.__dbname, self.table_name, self.__filter, self.__key_id)).fetchall()

        self.table.setRowCount(0)
        self.table.setSortingEnabled(False)
        for item in datas:
            rowcount = self.table.rowCount() + 1
            self.table.setRowCount(rowcount)
            row = rowcount - 1
            for i in range(0,len(item)):
                new_item = QTableWidgetItem(self.to_str(self.project.unpack_path(item[i]) if i in self.__file_columns else item[i]))
                if i in self.__file_columns:
                    new_item.setToolTip(self.to_str(self.project.unpack_path(item[i])))
                self.table.setItem(row,i,new_item)
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.setSortingEnabled(True)
        self.set_editable(self.__editable)
        self.__loading = False