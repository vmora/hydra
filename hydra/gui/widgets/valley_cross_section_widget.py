# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from PyQt4.QtGui import QWidget, QGridLayout
from PyQt4 import uic
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import pyqtSignal
import os
import hydra.utility.string as string

class ValleyCrossSectionWidget(QWidget):
    data_edited = pyqtSignal()

    @staticmethod
    def __quote(value):
        return "'"+value+"'" \
            if (isinstance(value, str) or isinstance(value, unicode))\
            and value[0] != "'" and value != "null" else value


    def __init__(self, parent=None, rk_min=None, rk_maj=None, sinuosity=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "valley_cross_section_widget.ui"), self)
        self.tmplayout = QGridLayout()
        self.tmplayout.addWidget(self,0,0)

        if rk_maj is not None:
            self.rk_maj.setText(string.get_str(rk_maj))
        if rk_min is not None:
            self.rk_min.setText(string.get_str(rk_min))
        if sinuosity is not None:
            self.sinuosity.setText(string.get_str(sinuosity))

    def get_layout(self):
        return self.tmplayout

    def return_values(self):
        rk_maj = string.get_sql_float(self.rk_maj.text())
        rk_min = string.get_sql_float(self.rk_min.text())
        sinuosity = string.get_sql_float(self.sinuosity.text())
        return rk_maj, rk_min, sinuosity

