# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
import os
from PyQt4 import uic
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QWidget, QGridLayout, QTableWidgetItem
import hydra.utility.string as string
from hydra.gui.geom_dialog import GeomDialog
from hydra.gui.widgets.circular_cross_section_widget import CircularCrossSectionWidget
from hydra.gui.widgets.ovoid_cross_section_widget import OvoidCrossSectionWidget
from hydra.gui.widgets.pipe_cross_section_widget import PipeCrossSectionWidget
from hydra.gui.widgets.channel_cross_section_widget import ChannelCrossSectionWidget
from hydra.gui.widgets.valley_cross_section_widget import ValleyCrossSectionWidget

class CrossSectionWidget(QWidget):

    @staticmethod
    def __quote(value):
        return "'"+value+"'" \
            if (isinstance(value, str) or isinstance(value, unicode))\
            and value[0] != "'" and value != "null" else value


    def __init__(self, project, id_pt, updown, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "cross_section_widget.ui"), self)
        self.tmplayout = QGridLayout()
        self.tmplayout.addWidget(self,0,0)

        self.__updown = updown
        assert self.__updown == 'up' or self.__updown == 'down'
        if self.__updown == 'up':
            self.key_id_pt = 'id_river_cross_section_profile_up'
            self.key_type_cs = 'type_cross_section_up'
        elif self.__updown == 'down':
            self.key_id_pt = 'id_river_cross_section_profile_down'
            self.key_type_cs = 'type_cross_section_down'

        self.project = project
        self.__model = self.project.get_current_model()
        self.__id_pt = id_pt

        self.__type_cs, self.__rk, self.__rk_maj, self.__sinuosity, self.__circular_diameter, self.__ovoid_height, self.__ovoid_top_diameter, self.__ovoid_invert_diameter,\
            self.cp_geom_id, self.op_geom_id, self.vcs_geom_id = self.project.execute("""select {t}, {l}_rk, {l}_rk_maj, {l}_sinuosity, {l}_circular_diameter,
            {l}_ovoid_height, {l}_ovoid_top_diameter, {l}_ovoid_invert_diameter, {l}_cp_geom, {l}_op_geom, {l}_vcs_geom
            from {m}.river_cross_section_profile where id={i}""".format(t=self.key_type_cs, l=self.__updown, m=self.__model.name, i=self.__id_pt)).fetchone()

        self.init_cross_section_widgets(parent)

        # cross section combo box building
        cs_id_name_abb = self.project.execute("""
                                        select id, name, abbreviation
                                        from hydra.cross_section_type""").fetchall()
        for i in range(0, len(cs_id_name_abb)):
            self.combo_cross_section.addItem(str(cs_id_name_abb[i][1]))
            if self.__type_cs is not None and cs_id_name_abb[i][1]==self.__type_cs:
                self.combo_cross_section.setCurrentIndex(i)
        if self.__type_cs is None:
            self.combo_cross_section.setCurrentIndex(-1)
        self.combo_cross_section.activated.connect(self.cross_section_changed)
        self.current_layout = None
        self.cross_section_changed()

        #set current geom
        if self.selected_cs == 'pipe':
            self.geometry_name.setText(self.cp_geom_name)
        elif self.selected_cs == 'channel':
            self.geometry_name.setText(self.op_geom_name)
        elif self.selected_cs == 'valley':
            self.geometry_name.setText(self.vcs_geom_name)

        #tools for geometry selection
        self.create_new.clicked.connect(self.new_geom)
        self.edit_selected.clicked.connect(self.edit_geom)
        self.table_existing_geometry.itemClicked.connect(self.select_geom)

    def get_layout(self):
        return self.tmplayout

    def init_cross_section_widgets(self, parent):
        #circular
        self.circular_cross_section = CircularCrossSectionWidget(parent, self.__circular_diameter, self.__rk)
        #ovoid
        self.ovoid_cross_section = OvoidCrossSectionWidget(parent, self.__ovoid_top_diameter, self.__ovoid_invert_diameter, self.__ovoid_height, self.__rk)
        #pipe
        self.pipe_cross_section = PipeCrossSectionWidget(parent, self.__rk)
        if self.cp_geom_id is not None:
            self.cp_geom_name, = self.project.execute("""select name
                                        from {}.closed_parametric_geometry
                                        where id={}""".format(self.__model.name, self.cp_geom_id)).fetchone()
        else:
            self.cp_geom_name = None

        #channel
        self.channel_cross_section = ChannelCrossSectionWidget(parent, self.__rk)
        if self.op_geom_id is not None:
            self.op_geom_name, = self.project.execute("""select name
                                        from {}.open_parametric_geometry
                                        where id={}""".format(self.__model.name, self.op_geom_id)).fetchone()
        else:
            self.op_geom_name = None

        #valley
        self.valley_cross_section = ValleyCrossSectionWidget(parent, self.__rk, self.__rk_maj, self.__sinuosity)
        if self.vcs_geom_id is not None:
            self.vcs_geom_name, = self.project.execute("""select name
                                        from {}.valley_cross_section_geometry
                                        where id={}""".format(self.__model.name, self.vcs_geom_id)).fetchone()
        else:
            self.vcs_geom_name = None

    def cross_section_changed(self):
        if self.combo_cross_section.currentIndex() != -1:
            self.selected_cs = self.combo_cross_section.currentText()
            if self.selected_cs == 'circular':
                self.geom_inactive()
                self.reset_layout(self.circular_cross_section.get_layout())
            elif self.selected_cs == 'ovoid':
                self.geom_inactive()
                self.reset_layout(self.ovoid_cross_section.get_layout())
            elif self.selected_cs == 'pipe':
                self.geom_refresh()
                self.reset_layout(self.pipe_cross_section.get_layout())
            elif self.selected_cs == 'channel':
                self.geom_refresh()
                self.reset_layout(self.channel_cross_section.get_layout())
            elif self.selected_cs == 'valley':
                self.geom_refresh()
                self.reset_layout(self.valley_cross_section.get_layout())
        else:
            self.selected_cs = 'no cs'

    def reset_layout(self, new_layout):
        if self.current_layout is not None:
            QWidget(self).setLayout(self.current_layout)
        self.widget_cross_section.setLayout(new_layout)
        self.current_layout = new_layout


    def new_geom(self):
        if self.selected_cs == 'pipe':
             id = self.__model.add_pipe_channel_section_geometry('closed_parametric_geometry', string.list_to_sql_array([[0,0]]))
             geom_dialog = GeomDialog(self.project, id, self.selected_cs)
        elif self.selected_cs == 'channel':
            id = self.__model.add_pipe_channel_section_geometry('open_parametric_geometry', string.list_to_sql_array([[0,0]]))
            geom_dialog = GeomDialog(self.project, id, self.selected_cs)
        elif self.selected_cs == 'valley':
            id = self.__model.add_valley_section_geometry(string.list_to_sql_array([[0,0]]), string.list_to_sql_array([[0,0]]), string.list_to_sql_array([[0,0]]), 1, 0, 0, 0, 0)
            geom_dialog = GeomDialog(self.project, id, self.selected_cs)
        geom_dialog.exec_()
        if self.selected_cs == 'pipe':
            if geom_dialog.canceled:
                self.project.execute("""delete from {}.closed_parametric_geometry
                                        where id={}""".format(self.__model.name, id))
            else:
                self.cp_geom_id = id
        elif self.selected_cs == 'channel':
            if geom_dialog.canceled:
                self.project.execute("""delete from {}.open_parametric_geometry
                                        where id={}""".format(self.__model.name, id))
            else:
                self.op_geom_id = id
        elif self.selected_cs == 'valley':
            if geom_dialog.canceled:
                self.project.execute("""delete from {}.valley_cross_section_geometry
                                        where id={}""".format(self.__model.name, id))
            else:
                self.vcs_geom_id = id
        self.geom_refresh()

    def edit_geom(self):
        if self.selected_cs == 'pipe' and self.cp_geom_id:
            GeomDialog(self.project, self.cp_geom_id, self.selected_cs).exec_()
        elif self.selected_cs == 'channel' and self.op_geom_id:
            GeomDialog(self.project, self.op_geom_id, self.selected_cs).exec_()
        elif self.selected_cs == 'valley' and self.vcs_geom_id:
            GeomDialog(self.project, self.vcs_geom_id, self.selected_cs).exec_()
        self.geom_refresh()

    def select_geom(self):
        if self.table_existing_geometry.currentRow()>-1:
            if self.selected_cs == 'pipe':
                self.cp_geom_id = int(self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 0).text())
                self.cp_geom_name = self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 1).text()
            elif self.selected_cs == 'channel':
                self.op_geom_id = int(self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 0).text())
                self.op_geom_name = self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 1).text()
            elif self.selected_cs == 'valley':
                self.vcs_geom_id = int(self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 0).text())
                self.vcs_geom_name = self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 1).text()
            self.geometry_name.setText(self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 1).text())

    def geom_refresh(self):
        self.label_geometry.setEnabled(True)
        self.geometry_name.setEnabled(True)
        self.geometry_name.clear()
        if self.selected_cs == 'pipe':
            geoms = self.project.execute(""" select id, name from {}.closed_parametric_geometry""".format(self.__model.name)).fetchall()
            if self.cp_geom_id is not None:
                self.cp_geom_name, = self.project.execute("""select name
                                            from {}.closed_parametric_geometry
                                            where id={}""".format(self.__model.name, self.cp_geom_id)).fetchone()
                self.geometry_name.setText(self.cp_geom_name)
        elif self.selected_cs == 'channel':
            geoms = self.project.execute(""" select id, name from {}.open_parametric_geometry""".format(self.__model.name)).fetchall()
            if self.op_geom_id is not None:
                self.op_geom_name, = self.project.execute("""select name
                                            from {}.open_parametric_geometry
                                            where id={}""".format(self.__model.name, self.op_geom_id)).fetchone()
                self.geometry_name.setText(self.op_geom_name)
        elif self.selected_cs == 'valley':
            geoms = self.project.execute(""" select id, name from {}.valley_cross_section_geometry""".format(self.__model.name)).fetchall()
            if self.vcs_geom_id is not None:
                self.vcs_geom_name, = self.project.execute("""select name
                                            from {}.valley_cross_section_geometry
                                            where id={}""".format(self.__model.name, self.vcs_geom_id)).fetchone()
                self.geometry_name.setText(self.vcs_geom_name)

        geoms_sorted = sorted(geoms, key=lambda id_name: id_name[0])

        self.table_existing_geometry.setRowCount(0)
        self.table_existing_geometry.setSortingEnabled(False)
        self.table_existing_geometry.setHorizontalHeaderLabels(['Id', 'Name'])
        for geom in geoms_sorted:
            row_position = self.table_existing_geometry.rowCount()
            self.table_existing_geometry.insertRow(row_position)
            self.table_existing_geometry.setItem(row_position, 0, QTableWidgetItem(str(geom[0])))
            self.table_existing_geometry.setItem(row_position, 1, QTableWidgetItem(str(geom[1])))
            if ((self.selected_cs == 'pipe' and geom[0] == self.cp_geom_id)
                or (self.selected_cs == 'channel' and geom[0] == self.op_geom_id)
                or (self.selected_cs == 'valley' and geom[0] == self.vcs_geom_id)):
                self.table_existing_geometry.selectRow(row_position)
        self.table_existing_geometry.resizeColumnsToContents()
        self.table_existing_geometry.setSortingEnabled(True)

        self.create_new.setEnabled(True)
        self.edit_selected.setEnabled(True)

    def geom_inactive(self):
        self.label_geometry.setEnabled(False)
        self.geometry_name.setEnabled(False)
        self.geometry_name.clear()
        self.table_existing_geometry.setRowCount(0)
        self.create_new.setEnabled(False)
        self.edit_selected.setEnabled(False)

    def save(self):
        ci_diameter, ci_rk = self.circular_cross_section.return_values()
        ov_top_diameter, ov_invert_diameter, ov_height, ov_rk = self.ovoid_cross_section.return_values()
        pi_rk = self.pipe_cross_section.return_values()
        ch_rk = self.channel_cross_section.return_values()
        va_rk_maj, va_rk, va_sinuosity = self.valley_cross_section.return_values()

        # Dirty way to reunify RK from different widgets
        # todo : rethink CS widget to put RK in pipe form directly
        cs_type = self.combo_cross_section.currentText()
        rk = locals()[cs_type[:2]+'_rk'] if cs_type in ['circular', 'ovoid', 'pipe', 'channel', 'valley'] else None

        if self.__updown == 'up':
            self.__model.update_river_cross_section_profile(self.__id_pt, None, None, None, None, None,
                                        rk, va_rk_maj, va_sinuosity, ci_diameter,
                                        ov_height, ov_top_diameter, ov_invert_diameter,
                                        self.cp_geom_id, self.op_geom_id, self.vcs_geom_id, None)
        elif self.__updown == 'down':
            self.__model.update_river_cross_section_profile(self.__id_pt, None, None, None, None, None,
                                        None, None, None, None, None, None, None, None, None, None, None,
                                        rk, va_rk_maj, va_sinuosity, ci_diameter,
                                        ov_height, ov_top_diameter, ov_invert_diameter,
                                        self.cp_geom_id, self.op_geom_id, self.vcs_geom_id, None)

