# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from PyQt4.QtGui import QWidget, QGridLayout
from PyQt4 import uic
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import pyqtSignal
import os
import hydra.utility.string as string

class OvoidCrossSectionWidget(QWidget):
    data_edited = pyqtSignal()

    @staticmethod
    def __quote(value):
        return "'"+value+"'" \
            if (isinstance(value, str) or isinstance(value, unicode))\
            and value[0] != "'" and value != "null" else value


    def __init__(self, parent=None, top_diameter=None, invert_diameter=None, height=None, rk=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "ovoid_cross_section_widget.ui"), self)
        self.tmplayout = QGridLayout()
        self.tmplayout.addWidget(self,0,0)

        if top_diameter is not None:
            self.top_diameter.setText(string.get_str(top_diameter))
        if invert_diameter is not None:
            self.invert_diameter.setText(string.get_str(invert_diameter))
        if height is not None:
            self.height.setText(string.get_str(height))
        if rk is not None:
            self.rk.setText(string.get_str(rk))

    def get_layout(self):
        return self.tmplayout

    def return_values(self):
        top_diameter = string.get_sql_float(self.top_diameter.text())
        invert_diameter = string.get_sql_float(self.invert_diameter.text())
        height = string.get_sql_float(self.height.text())
        rk = string.get_sql_float(self.rk.text())
        return top_diameter, invert_diameter, height, rk


