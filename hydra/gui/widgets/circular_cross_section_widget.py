# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from PyQt4.QtGui import QWidget, QGridLayout
from PyQt4 import uic
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import pyqtSignal
import os
import hydra.utility.string as string

class CircularCrossSectionWidget(QWidget):
    data_edited = pyqtSignal()

    @staticmethod
    def __quote(value):
        return "'"+value+"'" \
            if (isinstance(value, str) or isinstance(value, unicode))\
            and value[0] != "'" and value != "null" else value


    def __init__(self, parent=None, diameter=None, rk=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "circular_cross_section_widget.ui"), self)
        self.tmplayout = QGridLayout()
        self.tmplayout.addWidget(self,0,0)

        if diameter is not None:
            self.diameter.setText(string.get_str(diameter))
        if rk is not None:
            self.rk.setText(string.get_str(rk))

    def get_layout(self):
        return self.tmplayout

    def return_values(self):
        diameter = string.get_sql_float(self.diameter.text())
        rk = string.get_sql_float(self.rk.text())
        return diameter, rk


