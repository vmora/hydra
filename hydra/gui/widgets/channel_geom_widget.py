
################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from PyQt4.QtGui import QApplication, QWidget
from graph_widget import GraphWidget
from ..widgets.array_widget import ArrayWidget
from PyQt4.QtGui import QTableWidgetItem
from PyQt4 import uic
import hydra.utility.string as string
from hydra.gui.base_dialog import tr
import os

class ChannelGeomWidget(QWidget):
    def __init__(self, project, parent=None, channel_geom_table=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "channel_geom_widget.ui"), self)
        self.project = project
        self.model = self.project.get_current_model()

        self.op_geom_xyarray = ArrayWidget(["H (m)", "Width (m)"], [20,2], None, self.__draw_op_geom, self.array_op_geom_placeholder)
        self.graph_op_geom = GraphWidget(self.graph_op_geom_placeholder, True, True)
        self.channel_geom_table = channel_geom_table

        self.op_geom_name.textChanged.connect(self.__op_geom_update_name)

        self.set_enabled(False)

    def set_items(self, id_channel_geom):
        if id_channel_geom is not None:
            name, zbmin_array = self.project.execute("""select name, zbmin_array
                                                from {}.open_parametric_geometry where id={}""".format(self.model.name, id_channel_geom)).fetchone()
            self.op_geom_name.setText(name)
            self.op_geom_xyarray.set_table_items(zbmin_array)
            self.set_enabled(True)

    def set_enabled(self, bool=True):
        self.op_geom_name.setEnabled(bool)
        self.op_geom_xyarray.setEnabled(bool)
        self.graph_op_geom.canvas.setVisible(bool)

    def __op_geom_update_name(self):
        if self.channel_geom_table:
            self.channel_geom_table.table.item(self.channel_geom_table.table.row(self.channel_geom_table.table.selectedItems()[0]),1).setText(self.op_geom_name.text())

    def save_geom(self, id_channel_geom):
        name = self.op_geom_name.text()
        zbmin_array = self.op_geom_xyarray.get_table_items()
        self.model.update_pipe_channel_section_geometry('open_parametric_geometry', id_channel_geom, zbmin_array, name)

    def __draw_op_geom(self, datas):
        title = self.op_geom_name.text()
        if len(datas)>0:
            datas.insert(0,[datas[0][0], 0])
            self.graph_op_geom.clear()
            self.graph_op_geom.add_symetric_line(
                [r[1]/2 for r in datas],
                [r[0] for r in datas],
                "k",
                title,
                tr("Width (m)"), tr("H (m)"))
        else:
            self.graph_op_geom.canvas.setVisible(False)

