
################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
import os
from PyQt4 import uic
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QWidget, QGridLayout
import hydra.utility.string as string
from hydra.gui.base_dialog import tr


properties = {
                'coverages':{
                    'function': 'coverage_update()',
                    'meta_col': 'trigger_coverage'
                    },
                'branches':{
                    'function': 'branch_update_fct()',
                    'meta_col': 'trigger_branch'
                    },
                'street links':{
                    'function': 'update_street_links()',
                    'meta_col': 'trigger_street_link'
                    }
                }


class TriggerControlWidget(QWidget):
    def __init__(self, parent, project, model, trigger_name):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "trigger_control_widget.ui"), self)
        self.tmplayout = QGridLayout()
        self.tmplayout.addWidget(self,0,0)

        assert trigger_name in properties.keys()

        self.__trigger_name = trigger_name
        self.__project = project
        self.__model = model

        self.activate_checkbox.setText(tr('Trigger for {}:'.format(self.__trigger_name)))

        check_state = self.__project.execute("""select {} from {}.metadata""".format(properties[self.__trigger_name]['meta_col'], self.__model)).fetchone()[0]
        if check_state:
            self.activate_checkbox.setChecked(True)

        self.btn_regen.setText(tr('Regenerate {}'.format(self.__trigger_name)))
        self.btn_regen.clicked.connect(self.__regenerate_fct)

    def __regenerate_fct(self):
        self.__project.execute("""select {}.{};""".format(self.__model, properties[self.__trigger_name]['function']))
        self.__project.commit()

    def update_metadata(self):
        check_state = self.activate_checkbox.isChecked()
        self.__project.execute("""update {}.metadata set {}={}""".format(self.__model, properties[self.__trigger_name]['meta_col'], check_state))
        self.__project.commit()


