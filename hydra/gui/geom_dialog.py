# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
base dialog with buttons
"""

from PyQt4.QtGui import QDialog, QDialogButtonBox, QVBoxLayout
from PyQt4.QtCore import Qt
from hydra.gui.widgets.valley_geom_widget import ValleyGeomWidget
from hydra.gui.widgets.channel_geom_widget import ChannelGeomWidget
from hydra.gui.widgets.pipe_geom_widget import PipeGeomWidget

class GeomDialog(QDialog):
    def __init__(self, project, geom_id, geom_type):
        QDialog.__init__(self)

        self.project = project
        self.model = self.project.get_current_model()
        self.geom_id = geom_id
        self.canceled = False

        self.setLayout(QVBoxLayout())
        if geom_type == 'pipe':
            self.widget = PipeGeomWidget(self.project)
            name, = self.project.execute("""select name
                                                from {}.closed_parametric_geometry
                                                where id={}""".format(self.model.name, self.geom_id)).fetchone()
        elif geom_type == 'channel':
            self.widget = ChannelGeomWidget(self.project)
            name, = self.project.execute("""select name
                                                from {}.open_parametric_geometry
                                                where id={}""".format(self.model.name, self.geom_id)).fetchone()
        elif geom_type == 'valley':
            self.widget = ValleyGeomWidget(self.project)
            name, = self.project.execute("""select name
                                                from {}.valley_cross_section_geometry
                                                where id={}""".format(self.model.name, self.geom_id)).fetchone()
        self.widget.set_items(self.geom_id)
        self.layout().addWidget(self.widget)
        box = QDialogButtonBox(QDialogButtonBox.Save, Qt.Horizontal)
        box.accepted.connect(self.accept)
        self.layout().addWidget(box)
        self.resize(712, 662)
        self.setWindowTitle(name)
        self.finished.connect(self.__finished)

    def __finished(self, ret=None):
        if ret == QDialog.Accepted:
            self.widget.save_geom(self.geom_id)
        else:
            self.canceled = True

