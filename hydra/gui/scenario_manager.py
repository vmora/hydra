# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re

from PyQt4 import uic, QtCore
from PyQt4.QtGui import QFileDialog, QMessageBox, QApplication
from PyQt4.QtCore import Qt
from ..database.export_calcul import ExportCalcul
from ..database.export_carto_data import ExportCartoData
from ..kernel import hydra_kernel
from hydra.gui.validity import ValidityTool
from ..utility.string import normalized_caps_name
from .base_dialog import BaseDialog, tr
from .widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.widgets.combo_with_values import ComboWithValues
from .scn_widgets.comput_options import ComputOptionsWidget
from .scn_widgets.comput_settings import ComputSettingsWidget
from .scn_widgets.hydrology_settings import HydrologySettingsWidget
from .scn_widgets.model_ordering import ModelOrderingWidget
from .scn_widgets.transport import TransportWidget
from .scn_widgets.regulation_and_configuration import RegulationConfigurationWidget


class ScenarioManager(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "scenario_manager.ui"), self)
        self.setWindowFlags(self.windowFlags() |
                              QtCore.Qt.WindowSystemMenuHint |
                              QtCore.Qt.WindowMinMaxButtonsHint)
        self.setWindowState(QtCore.Qt.WindowMaximized)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.buttonRun.clicked.connect(self.run)
        self.clone.clicked.connect(self.clone_scenario)

        self.comput_options = ComputOptionsWidget(project, self.comput_options_placeholder)
        self.comput_settings = ComputSettingsWidget(project, self.comput_settings_placeholder)
        self.hydrology_settings = HydrologySettingsWidget(project, self.hydrology_settings_placeholder)
        self.model_ordering = ModelOrderingWidget(project, self.model_ordering_placeholder)
        self.transport = TransportWidget(project, self.transport_placeholder)
        self.regulation_and_configuration = RegulationConfigurationWidget(project, self.regulation_and_configuration_placeholder)

        self.table_scenario = HydraTableWidget(project,
            ("id", "name", "comment"),
            (tr("Id"), tr("Name"), tr("Comment")),
            "project", "scenario",
            (self.new_scenario, self.delete_scenario), "", "id",
            self.table_scenario_placeholder)

        self.table_scenario.set_editable(True)
        self.table_scenario.data_edited.connect(self.save_active_scn)
        self.active_scn_id = None
        self.set_active_scenario()
        self.table_scenario.table.itemSelectionChanged.connect(self.set_active_scenario)

    def clone_scenario(self):
        if not self.active_scn_id is None:
            self.save_active_scn()
            if self.active_scn_id is None:
                return
            scn_fields = """comment, comput_mode, dry_inflow, rainfall, dt_hydrol_mn, soil_moisture_coef, runoff_adjust_coef,
                            option_dim_hydrol_network, date0, tfin_hr, tini_hydrau_hr, dtmin_hydrau_hr, dtmax_hydrau_hr,
                            dzmin_hydrau, dzmax_hydrau, flag_save, tsave_hr, flag_rstart, flag_gfx_control, scenario_rstart,
                            flag_hydrology_rstart, scenario_hydrology_rstart, trstart_hr, graphic_control, model_connect_settings,
                            tbegin_output_hr, tend_output_hr, dt_output_hr, c_affin_param, t_affin_start_hr, t_affin_min_surf_ddomains,
                            strickler_param, option_file, option_file_path, output_option, transport_type, iflag_mes, iflag_dbo5, iflag_dco, iflag_ntk,
                            quality_type, diffusion, dispersion_coef, longit_diffusion_coef, lateral_diff_coef, water_temperature_c,
                            min_o2_mgl, kdbo5_j_minus_1, koxygen_j_minus_1, knr_j_minus_1, kn_j_minus_1, bdf_mgl, o2sat_mgl, iflag_ad1, iflag_ad2, iflag_ad3, iflag_ad4,
                            ad1_decay_rate_jminus_one, ad2_decay_rate_jminus_one, ad3_decay_rate_jminus_one, ad4_decay_rate_jminus_one, sst_mgl, sediment_file"""

            id_new_scn, = self.project.execute("""
                insert into project.scenario({fields})
                select {fields}
                from project.scenario where id={scn_id}
                returning id;""".format(
                fields=scn_fields, scn_id=self.active_scn_id)).fetchone()

            c_aff_fields = """model, type_domain, element"""
            inflow_fields = """model, hydrograph_from, hydrograph_to, option, parameter"""
            mixed_fields = """grp, ord"""
            out_fields = """model, type_element, element"""
            ext_hy_fields = """external_file"""
            hy_fields = """model, hydrograph_from, hydrograph_to"""
            measure_fields = """measure_file"""
            regulation_fields = """control_file"""
            strickler_fields = """model, type_domain, element, rkmin, rkmaj, pk1, pk2"""
            config_fields = """model, configuration"""

            self.project.execute("""
                insert into project.c_affin_param (scenario, {fields})
                select {scn}, {fields}
                from project.c_affin_param where scenario={old_scn}
                """.format(fields=c_aff_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.inflow_rerouting (scenario, {fields})
                select {scn}, {fields}
                from project.inflow_rerouting where scenario={old_scn}
                """.format(fields=inflow_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.mixed (scenario, {fields})
                select {scn}, {fields}
                from project.mixed where scenario={old_scn}
                """.format(fields=mixed_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.output_option (scenario, {fields})
                select {scn}, {fields}
                from project.output_option where scenario={old_scn}
                """.format(fields=out_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.param_external_hydrograph (param_scenario, {fields})
                select {scn}, {fields}
                from project.param_external_hydrograph where param_scenario={old_scn}
                """.format(fields=ext_hy_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.param_hydrograph (param_scenario, {fields})
                select {scn}, {fields}
                from project.param_hydrograph where param_scenario={old_scn}
                """.format(fields=hy_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.param_measure (param_scenario, {fields})
                select {scn}, {fields}
                from project.param_measure where param_scenario={old_scn}
                """.format(fields=measure_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.param_regulation (param_scenario, {fields})
                select {scn}, {fields}
                from project.param_regulation where param_scenario={old_scn}
                """.format(fields=regulation_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.strickler_param (scenario, {fields})
                select {scn}, {fields}
                from project.strickler_param where scenario={old_scn}
                """.format(fields=strickler_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.config_scenario (scenario, {fields})
                select {scn}, {fields}
                from project.config_scenario where scenario={old_scn}
                """.format(fields=config_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.active_scn_id = id_new_scn
            self.table_scenario.update_data()
            self.table_scenario.table.selectRow(self.table_scenario.table.rowCount()-1)

    def delete_scenario(self):
        if not self.active_scn_id is None:
            self.project.execute("""
            delete from project.scenario where id={0}""".format(self.active_scn_id))
            self.active_scn_id = None
            self.table_scenario.update_data()

    def new_scenario(self):
        self.save_active_scn()
        self.active_scn_id = None
        id_scn = self.project.add_new_scenario()
        self.table_scenario.update_data()
        self.table_scenario.table.selectRow(self.table_scenario.table.rowCount()-1)

    def run(self, mute = False):
        if not self.active_scn_id is None:
            self.save_active_scn()
            if self.active_scn_id is None:
                return
            self.project.set_current_scenario(self.active_scn_id)

            self.project.unload_csv_results_layers()

            id_scn, name_scn = self.project.get_current_scenario()
            comput_mode, output_option, model_connect_settings = self.project.execute("""
                select comput_mode, output_option, model_connect_settings
                from project.scenario
                where id={}
                """.format(self.active_scn_id)).fetchone()
            id_comput_option, = self.project.execute("""
                select id from hydra.computation_mode
                where name='{}'
                """.format(comput_mode)).fetchone()
            lstmodel = [name for name, in self.project.execute("""
                select name
                from project.model
                """).fetchall()]
            if model_connect_settings=="mixed":
                lstmodel = []
                groups = self.project.execute("""
                    select id, name, ord
                    from project.mixed as mixed, project.grp as grp
                    where mixed.grp=grp.id
                    and mixed.scenario={}
                    order by mixed.ord""".format(self.active_scn_id)).fetchall()
                for group in groups:
                    lstmodel = [name for name, in self.project.execute("""
                        select name
                        from project.model_config as model
                        where model.id in (
                            select model from project.grp_model
                            where grp = %i)
                        """%(int(group[0]))).fetchall()]

            invalid=False
            for model in lstmodel:
                if self.project.execute(""" select * from {}.invalid""".format(model)).fetchall():
                    invalid=True
                    invalid_model=model
            if invalid:
                validity = QMessageBox(QMessageBox.Warning, tr('Warning'), tr('There are still some invalidities in model {}.'.format(invalid_model)), QMessageBox.Ok).exec_()
            else:
                # export topological data for calculation
                exporter = ExportCalcul(self.project)
                exporter.export(int(self.active_scn_id))
                # export cartograpic data for triangulation
                exporter = ExportCartoData(self.project)
                exporter.export(int(self.active_scn_id))
                flag_gfx_control, = self.project.execute("""
                    select flag_gfx_control
                    from project.scenario
                    where id={} limit 1""".format(str(id_scn))).fetchone()
                # run calculations
                try:
                    QApplication.setOverrideCursor(Qt.WaitCursor)
                    QApplication.processEvents()
                    hydra_kernel.run(self.project.get_senario_path(self.active_scn_id), flag_gfx_control, comput_mode, self if not mute else None)
                except Exception as e:
                    QApplication.restoreOverrideCursor()
                    raise e
                QApplication.restoreOverrideCursor()
                BaseDialog.save(self)
                self.close()

    def select_scenario_by_name(self, scenario_name):
        for i in range(0, self.table_scenario.table.rowCount()):
            if self.table_scenario.table.item(i,1).text()==scenario_name:
                self.table_scenario.table.setCurrentCell(i,0)

    def set_active_scenario(self):
        items = self.table_scenario.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_scenario.table.row(items[0])
            if not self.active_scn_id is None:
                self.save_active_scn()
                self.table_scenario.table.selectRow(selected_row)
            self.active_scn_id = self.table_scenario.table.item(selected_row,0).text()
            self.labelSelectedScn.setText(self.table_scenario.table.item(selected_row,1).text())
            self.labelScnToCalc.setText(self.table_scenario.table.item(selected_row,1).text())
            self.init_ui_scn_settings()
        else:
            self.labelSelectedScn.setText("")
            self.labelScnToCalc.setText("")
            self.gbxScenarioSettings.setEnabled(False)
            self.active_scn_id = None

    def init_ui_scn_settings(self):
        self.gbxScenarioSettings.setEnabled(True)
        if not self.active_scn_id is None:
            self.comput_options.set(self.active_scn_id)
            self.comput_settings.set(self.active_scn_id)
            self.model_ordering.set(self.active_scn_id)
            self.hydrology_settings.set(self.active_scn_id)
            self.transport.set(self.active_scn_id)
            self.regulation_and_configuration.set(self.active_scn_id)

    def save_active_scn(self):
        if not self.active_scn_id is None:
            new_name=None
            new_desc=None
            row=0
            for i in range(0, self.table_scenario.table.rowCount()):
                if int(self.active_scn_id) == int(self.table_scenario.table.item(i,0).text()):
                    new_name = normalized_caps_name(self.table_scenario.table.item(i, 1).text())
                    new_desc = self.table_scenario.table.item(i, 2).text().encode('utf-8')
                    row=i

            assert new_name is not None
            assert new_desc is not None

            self.project.execute("""
            update project.scenario set name='{}', comment='{}'
            where id={}""".format(str(new_name),str(new_desc),self.active_scn_id))

            self.comput_settings.save()
            self.comput_options.save()
            self.hydrology_settings.save()
            self.model_ordering.save()
            self.transport.save()
            self.regulation_and_configuration.save()

            self.labelSelectedScn.setText(new_name)
            self.labelScnToCalc.setText(new_name)
            if not new_name == self.table_scenario.table.item(row, 1).text():
                self.table_scenario.update_data()

    def save(self):
        self.save_active_scn()
        if self.active_scn_id:
            self.project.set_current_scenario(self.active_scn_id)
        BaseDialog.save(self)
        self.close()