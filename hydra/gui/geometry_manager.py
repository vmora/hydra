# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from .base_dialog import BaseDialog, tr
from .widgets.hydra_table_widget import HydraTableWidget
from .widgets.graph_widget import GraphWidget
from .widgets.valley_geom_widget import ValleyGeomWidget
from .widgets.channel_geom_widget import ChannelGeomWidget
from .widgets.pipe_geom_widget import PipeGeomWidget
from PyQt4 import uic
from PyQt4.QtGui import QTableWidgetItem
import hydra.utility.string as string
import os
import re

class GeometryManager(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        self.project = project
        self.model = self.project.get_current_model()
        uic.loadUi(os.path.join(current_dir, "geometry_manager.ui"), self)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        #closed parametric geometries init :
        self.table_cp_geom = HydraTableWidget(self.project, ("id", "name"),(tr("Id"), tr("Name")),
                                self.model.name, "closed_parametric_geometry",
                                (self.new_cp_geom, self.delete_cp_geom), "", "id",
                                self.table_cp_geom_placeholder)
        self.pipe_geom_widget = PipeGeomWidget(self.project, self.widget_pipe_geom_placeholder, self.table_cp_geom)
        self.table_cp_geom.table.itemSelectionChanged.connect(self.set_active_cp_geom)
        self.active_cp_geom_id = None
        self.table_cp_geom.table.setSortingEnabled(True)
        self.table_cp_geom.table.selectRow(0)

        #open parametric geometries init :
        self.table_op_geom = HydraTableWidget(self.project, ("id", "name"),(tr("Id"), tr("Name")),
                                self.model.name, "open_parametric_geometry",
                                (self.new_op_geom, self.delete_op_geom), "", "id",
                                self.table_op_geom_placeholder)
        self.channel_geom_widget = ChannelGeomWidget(self.project, self.widget_channel_geom_placeholder, self.table_op_geom)
        self.table_op_geom.table.itemSelectionChanged.connect(self.set_active_op_geom)
        self.active_op_geom_id = None
        self.table_op_geom.table.setSortingEnabled(True)
        self.table_op_geom.table.selectRow(0)

        #valley parametric geometries init :
        self.table_vcs_geom = HydraTableWidget(self.project, ("id", "name"),(tr("Id"), tr("Name")),
                                self.model.name, "valley_cross_section_geometry",
                                (self.new_vcs_geom, self.delete_vcs_geom), "", "id",
                                self.table_vcs_geom_placeholder)
        self.valley_geom_widget = ValleyGeomWidget(self.project, self.widget_valley_geom_placeholder, self.table_vcs_geom)
        self.table_vcs_geom.table.itemSelectionChanged.connect(self.set_active_vcs_geom)
        self.active_vcs_geom_id = None
        self.table_vcs_geom.table.setSortingEnabled(True)
        self.table_vcs_geom.table.selectRow(0)

        # closed parametric geometry piloting
    def new_cp_geom(self):
        self.save_cp_geom()
        zbmin_array=[[0,0]]
        self.table_cp_geom.add_row(['default', string.list_to_sql_array(zbmin_array)], ['zbmin_array'])
        self.set_active_cp_geom()

    def delete_cp_geom(self):
        if self.active_cp_geom_id is not None:
            self.table_cp_geom.del_selected_row()
        if len(self.table_cp_geom.table.selectedItems())==0:
            self.pipe_geom_widget.set_enabled(False)

    def save_cp_geom(self):
        if self.active_cp_geom_id is not None:
            self.pipe_geom_widget.save_geom(self.active_cp_geom_id)

    def set_active_cp_geom(self):
        items = self.table_cp_geom.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_cp_geom.table.row(items[0])
            if self.active_cp_geom_id is not None:
                self.save_cp_geom()
            self.active_cp_geom_id = self.table_cp_geom.table.item(selected_row,0).text()
            self.pipe_geom_widget.set_items(self.active_cp_geom_id)
        else:
            self.active_cp_geom_id=None

    # open parametric geometry piloting
    def new_op_geom(self):
        self.save_op_geom()
        zbmin_array=[[0,0]]
        self.table_op_geom.add_row(['default', string.list_to_sql_array(zbmin_array)], ['zbmin_array'])
        self.set_active_op_geom()

    def delete_op_geom(self):
        if self.active_op_geom_id is not None:
            self.table_op_geom.del_selected_row()
        if len(self.table_op_geom.table.selectedItems())==0:
            self.channel_geom_widget.set_enabled(False)

    def save_op_geom(self):
        if self.active_op_geom_id is not None:
            self.channel_geom_widget.save_geom(self.active_op_geom_id)

    def set_active_op_geom(self):
        items = self.table_op_geom.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_op_geom.table.row(items[0])
            if self.active_op_geom_id is not None:
                self.save_op_geom()
            self.active_op_geom_id = self.table_op_geom.table.item(selected_row,0).text()
            self.channel_geom_widget.set_items(self.active_op_geom_id)
        else:
            self.active_op_geom_id=None

    # valley cross section geometry piloting
    def new_vcs_geom(self):
        self.save_vcs_geom()
        zbmin_array=[[0,0]]
        zbmaj_lbank_array=[[0,0]]
        zbmaj_rbank_array=[[0,0]]
        self.table_vcs_geom.add_row(['default', string.list_to_sql_array(zbmin_array),
                                        string.list_to_sql_array(zbmaj_lbank_array), string.list_to_sql_array(zbmaj_rbank_array)],
                                        ['zbmin_array','zbmaj_lbank_array','zbmaj_rbank_array'])
        self.set_active_vcs_geom()

    def delete_vcs_geom(self):
        if self.active_vcs_geom_id is not None:
            self.table_vcs_geom.del_selected_row()
        if len(self.table_vcs_geom.table.selectedItems())==0:
            self.valley_geom_widget.set_enabled(False)

    def save_vcs_geom(self):
        if self.active_vcs_geom_id is not None:
            self.valley_geom_widget.save_geom(self.active_vcs_geom_id)

    def set_active_vcs_geom(self):
        if len(self.table_vcs_geom.table.selectedItems())>0:
            selected_row = self.table_vcs_geom.table.row(self.table_vcs_geom.table.selectedItems()[0])
            self.save_vcs_geom()
            self.active_vcs_geom_id = self.table_vcs_geom.table.item(selected_row,0).text()
            self.valley_geom_widget.set_items(self.active_vcs_geom_id)
        else:
            self.active_vcs_geom_id=None

    # all
    def get_table_items(self, array, formated = True):
        n = array.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            if not array.item(i,0) == None and not array.item(i,1) == None:
                result.append(list())
                m = array.columnCount()
                result[index_list].append(string.get_sql_float(array.item(i,0).text()))
                result[index_list].append(string.get_sql_float(array.item(i,1).text()))
                index_list = index_list + 1
        if not formated:
            return result
        return string.list_to_sql_array(result)

    def save(self):
        self.project.log.notice(tr("Saved"))
        if self.active_cp_geom_id is not None:
            self.save_cp_geom()
        if self.active_op_geom_id is not None:
            self.save_op_geom()
        if self.active_vcs_geom_id is not None:
            self.save_vcs_geom()
        BaseDialog.save(self)
        self.close()
