# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
from PyQt4 import uic, QtGui, QtCore
from PyQt4.QtGui import QTableWidgetItem, QTreeWidgetItem, QFileDialog, QFont, QColor, QMessageBox, QProgressDialog
import hydra.utility.string as string
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.radar_rain import RadarRainDialog
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.widgets.rain_table_widget import RainTableWidget
from hydra.gui.widgets.raingaged_table_widget import RainGagedTableWidget
from hydra.gui.widgets.graph_widget import GraphWidget
from hydra.database.radar_rain import RainVrt

bold = QFont()
bold.setWeight(QFont.Bold)

_hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")

class RainManager(BaseDialog):

    curve_color = "#0066ff"

    def list_to_sql_array(self, py_list):
        return str(map(str, py_list)).replace('[', '{').replace(']', '}').replace('\'', '') if py_list else None

    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "rain_manager.ui"), self)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.table_montana = HydraTableWidget(project,
            ("id", "name", "return_period_yr", "coef_a", "coef_b", "comment"),
            (tr("Id"), tr("Name"), tr("Return period (years)"), tr("a"), tr("b"), tr("Comment")),
            "project", "coef_montana",
            (self.new_montana, self.delete_montana), "", "id",
            self.table_montana_placeholder)
        self.table_montana.set_editable(True)
        self.table_montana.data_edited.connect(self.save_montana)

        self.table_hyetograph = HydraTableWidget(project,
            ("id", "name"),
            (tr("Id"), tr("Name")),
            "project", "intensity_curve_rainfall",
            (self.new_hyetograph, self.delete_hyetograph), "", "id",
            self.table_hyetograph_placeholder)
        self.table_hyetograph.set_editable(True)
        self.table_hyetograph.data_edited.connect(self.save_hyetograph)
        self.table_hyetograph.table.itemSelectionChanged.connect(self.set_active_hyetograph)
        self.active_hyetograph_id=None
        self.hyetograph_values.cellChanged.connect(self.__hyetograph_values_cell_changed)
        self.hyetograph_values.setEnabled(False)
        self.graph_hyetograph = GraphWidget(self.graph_hyetograph_placeholder)
        self.hyetograph_values.currentCellChanged.connect(self.__draw_hyetograph)

        self.table_caquot = RainTableWidget(project,
            ("id", "name", "montana"),
            (tr("Id"), tr("Name"), tr("Montana")),
            "project", "caquot_rainfall",
            (self.new_caquot, self.delete_caquot), "", "id",
            self.table_caquot_placeholder)
        self.table_caquot.set_editable(True)
        self.table_caquot.data_edited.connect(self.save_caquot)

        self.table_simple_tri = RainTableWidget(project,
            ("id", "name", "montana", "total_duration_mn", "peak_time_mn"),
            (tr("Id"), tr("Name"), tr("Montana"), tr("Total duration (min)"), tr("Peak time (min)")),
            "project", "single_triangular_rainfall",
            (self.new_simple_tri, self.delete_simple_tri), "", "id",
            self.table_simple_tri_placeholder)
        self.table_simple_tri.set_editable(True)
        self.table_simple_tri.data_edited.connect(self.save_simple_tri)
        self.graph_simple_tri = GraphWidget(self.graph_simple_placeholder)
        self.table_simple_tri.table.currentCellChanged.connect(self.__draw_simple_tri)
        self.table_simple_tri.table.itemSelectionChanged.connect(self.__draw_simple_tri)

        self.table_double_tri = RainTableWidget(project,
            ("id", "name", "peak_time_mn", "total_duration_mn", "montana_total", "peak_duration_mn", "montana_peak"),
            (tr("Id"), tr("Name"), tr("Peak time (min)"), tr("Total event\n Duration (min)"), tr("Total event\n Montana coefficient"),
            tr("Peak\n Duration (min)"), tr("Peak\n Montana coefficient"),),
            "project", "double_triangular_rainfall",
            (self.new_double_tri, self.delete_double_tri), "", "id",
            self.table_double_tri_placeholder)
        self.table_double_tri.set_editable(True)
        self.table_double_tri.data_edited.connect(self.save_double_tri)
        self.graph_double_tri = GraphWidget(self.graph_double_placeholder)
        self.table_double_tri.table.currentCellChanged.connect(self.__draw_double_tri)
        self.table_double_tri.table.itemSelectionChanged.connect(self.__draw_double_tri)

        self.table_gage_rainfall = RainGagedTableWidget(project,
            ("id", "name", "cbv_grid_connect_file", "interpolation"),
            (tr("Id"), tr("Name"), tr("Grid connect file"), tr("Interpolation type")),
            "project", "gage_rainfall",
            (self.new_gage_rainfall, self.delete_gage_rainfall), "", "id",
            self.gage_rainfall_placeholder)
        self.table_gage_rainfall.set_editable(True)
        self.table_gage_rainfall.data_edited.connect(self.save_gage_rainfall)
        self.table_gage_rainfall.table.itemSelectionChanged.connect(self.set_active_gage_rainfall)
        self.active_gage_rainfall_id=None
        self.gage_list.itemSelectionChanged.connect(self.set_active_rain_gage)
        self.gage_list.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.gage_list.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.gage_list.resizeColumnsToContents()
        self.gage_list.horizontalHeader().setStretchLastSection(True);
        self.gage_list.setEnabled(False)
        self.active_rain_gage_id=None
        self.gage_rainfall_data.cellChanged.connect(self.__gage_rainfall_data_values_cell_changed)
        self.gage_rainfall_data.resizeColumnsToContents()
        self.gage_rainfall_data.horizontalHeader().setStretchLastSection(True);
        self.gage_rainfall_data.setEnabled(False)
        self.graph_gage = GraphWidget(self.graph_gage_placeholder)
        self.gage_rainfall_data.currentCellChanged.connect(self.__draw_gage)

        self.btn_add_radar_rain.clicked.connect(self.__add_radar_rain)
        self.btn_delete_radar_rain.clicked.connect(self.__delete_radar_rain)
        self.btn_add_tiffs.clicked.connect(self.__add_tiffs)
        self.btn_generate_rain_file.clicked.connect(self.__export_selected_rain)

        self.radar_rain_tree.itemClicked.connect(self.__select_radar_rain)

        self.__refresh_radar_rain()
        self.__select_radar_rain()

    def select_montana(self, rain_name):
        self.__select_in_table(self.table_montana.table, rain_name)
        self.tabWidget.setCurrentIndex(0);

    def select_rain(self, rain_name):
        rain_type, = self.project.execute("""
                select rainfall_type from project._rainfall where name='{}';
            """.format(rain_name)).fetchone()

        if rain_type=="caquot":
            self.__select_in_table(self.table_caquot.table, rain_name)
            self.tabWidget.setCurrentIndex(0);
            self.tabWidget_2.setCurrentIndex(0);
        elif rain_type=="single_triangular":
            self.__select_in_table(self.table_simple_tri.table, rain_name)
            self.tabWidget.setCurrentIndex(0);
            self.tabWidget_2.setCurrentIndex(1);
        elif rain_type=="double_triangular":
            self.__select_in_table(self.table_double_tri.table, rain_name)
            self.tabWidget.setCurrentIndex(0);
            self.tabWidget_2.setCurrentIndex(2);
        elif rain_type=="intensity_curve":
            self.__select_in_table(self.table_hyetograph.table, rain_name)
            self.tabWidget.setCurrentIndex(1);
        elif rain_type=="gage":
            self.__select_in_table(self.table_gage_rainfall.table, rain_name)
            self.tabWidget.setCurrentIndex(2);

    def __select_in_table(self, table, text):
        for i in range(0, table.rowCount()):
            if table.item(i,1).text()==text:
                table.setCurrentCell(i,0)

    def keyPressEvent(self, event):
        if self.gage_rainfall_data.hasFocus():
            if event.key() == QtCore.Qt.Key_V and  (event.modifiers() & QtCore.Qt.ControlModifier):
                self.paste()
                event.accept()
            else:
                event.ignore()
        else:
            event.ignore()

    def paste(self):
        text = QtGui.QApplication.clipboard().text()
        # placer ce contenu dans le QTableWidget
        for i, textline in enumerate(text.split('\n')):
            for j, celltext in enumerate(textline.split('\t')):
                self.gage_rainfall_data.setItem(i, j, QtGui.QTableWidgetItem(celltext))

    def delete_caquot(self):
        self.table_caquot.del_selected_row()

    def new_caquot(self):
        self.table_caquot.add_row()

    def save_caquot(self):
        self.table_caquot.save_selected_row()

    def delete_simple_tri(self):
        self.table_simple_tri.del_selected_row()

    def new_simple_tri(self):
        self.table_simple_tri.add_row()

    def save_simple_tri(self):
        self.table_simple_tri.save_selected_row()

    def __draw_simple_tri(self):
        items = self.table_simple_tri.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_simple_tri.table.row(items[0])
            try:
                name = self.table_simple_tri.table.item(selected_row,1).text()
                duration = float(self.table_simple_tri.table.item(selected_row,3).text())
                peak_time = float(self.table_simple_tri.table.item(selected_row,4).text())
                id_montana = self.table_simple_tri.table.item(selected_row,2).text()
                a, b = self.project.execute("""select coef_a, coef_b from project.coef_montana
                    where id={}""".format(id_montana)).fetchone()
                i_peak = float(a) * (duration**float(b)) * 60
                self.graph_simple_tri.clear()
                self.graph_simple_tri.add_filled_line([0, peak_time, duration], [0, i_peak, 0],
                    RainManager.curve_color, name, tr("t (min)"), tr("i (mm/h)"))
            except:
                self.graph_simple_tri.canvas.setVisible(False)

    def delete_double_tri(self):
        self.table_double_tri.del_selected_row()

    def new_double_tri(self):
        self.table_double_tri.add_row()

    def save_double_tri(self):
        self.table_double_tri.save_selected_row()

    def __draw_double_tri(self):
        items = self.table_double_tri.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_double_tri.table.row(items[0])
            try:
                name = self.table_double_tri.table.item(selected_row,1).text()
                peak_time = float(self.table_double_tri.table.item(selected_row,2).text())

                duration1 = float(self.table_double_tri.table.item(selected_row,3).text())
                id_montana1 = self.table_double_tri.table.item(selected_row,4).text()
                if not id_montana1: return
                a1, b1 = self.project.execute("""select coef_a, coef_b from project.coef_montana
                    where id={}""".format(id_montana1)).fetchone()
                i_peak1 = float(a1) * (duration1**float(b1)) * 60

                duration2 = float(self.table_double_tri.table.item(selected_row,5).text())
                id_montana2 = self.table_double_tri.table.item(selected_row,6).text()
                if not id_montana2: return
                a2, b2 = self.project.execute("""select coef_a, coef_b from project.coef_montana
                    where id={}""".format(id_montana2)).fetchone()
                i_peak2 = float(a2) * (duration2**float(b2)) * 60

                self.graph_double_tri.clear()
                self.graph_double_tri.add_filled_line(
                    [0, peak_time, duration1],
                    [0, i_peak1, 0],
                    RainManager.curve_color, name, tr("t (min)"), tr("i (mm/h)"))
                self.graph_double_tri.add_filled_line(
                    [peak_time-duration2/2, peak_time, peak_time+duration2/2],
                    [0, i_peak2, 0],
                    RainManager.curve_color, name, tr("t (min)"), tr("i (mm/h)"))
            except:
                self.graph_double_tri.canvas.setVisible(False)

    def __hyetograph_values_cell_changed(self,row,column):
        if row == (self.hyetograph_values.rowCount()-1):
            self.hyetograph_values.setRowCount(self.hyetograph_values.rowCount() + 1)

    def __draw_hyetograph(self):
        items = self.table_hyetograph.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_hyetograph.table.row(items[0])
            t_mn_intensity_mmh = self.get_hyetograph_table_items()
            if len(t_mn_intensity_mmh)>0:
                self.graph_hyetograph.clear()
                self.graph_hyetograph.add_filled_line(
                    [r[0] for r in t_mn_intensity_mmh],
                    [r[1] for r in t_mn_intensity_mmh],
                    RainManager.curve_color,
                    self.table_hyetograph.table.item(selected_row,1).text(),
                    tr("t (min)"), tr("i (mm/h)"))
            else:
                self.graph_hyetograph.canvas.setVisible(False)
        else:
            self.graph_hyetograph.canvas.setVisible(False)

    def set_active_hyetograph(self):
        items = self.table_hyetograph.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_hyetograph.table.row(items[0])
            if not self.active_hyetograph_id is None:
                self.save_hyetograph()

            self.active_hyetograph_id = self.table_hyetograph.table.item(selected_row,0).text()
            t_mn_intensity_mmh, = self.project.execute("""
                select t_mn_intensity_mmh from project.intensity_curve_rainfall where id={};
            """.format(self.active_hyetograph_id)).fetchone()
            self.set_hyetograph_table_items(t_mn_intensity_mmh)
            self.hyetograph_values.setEnabled(True)
            self.__draw_hyetograph()

    def set_hyetograph_table_items(self, hg_array):
        self.hyetograph_values.setRowCount(0)
        if hg_array is not None:
            n = len(hg_array)
            self.hyetograph_values.setRowCount(n+1)
            for row in range(0, n):
                self.hyetograph_values.setItem(row,0,QTableWidgetItem(string.get_str(hg_array[row][0])))
                self.hyetograph_values.setItem(row,1,QTableWidgetItem(string.get_str(hg_array[row][1])))
        else:
            self.hyetograph_values.setRowCount(1)

    def get_hyetograph_table_items(self):
        n = self.hyetograph_values.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            if not self.hyetograph_values.item(i,0) == None and not self.hyetograph_values.item(i,1) == None:
                result.append(list())
                m = self.hyetograph_values.columnCount()
                result[index_list].append(string.get_sql_float(self.hyetograph_values.item(i,0).text()))
                result[index_list].append(string.get_sql_float(self.hyetograph_values.item(i,1).text()))
                index_list = index_list + 1
        return result

    def delete_hyetograph(self):
        if not self.active_hyetograph_id is None:
            self.table_hyetograph.del_selected_row()

    def new_hyetograph(self):
        if not self.active_hyetograph_id is None:
            self.save_hyetograph()
        self.table_hyetograph.add_row()
        self.set_active_hyetograph()

    def save_hyetograph(self):
        if not self.active_hyetograph_id is None:
            self.table_hyetograph.save_selected_row()
            items = self.list_to_sql_array(self.get_hyetograph_table_items())
            if not items is None:
                self.project.execute("""update project.intensity_curve_rainfall
                    set t_mn_intensity_mmh='{}' where id={}""".format(
                    items, self.active_hyetograph_id))

    def delete_montana(self):
        self.table_montana.del_selected_row()

    def new_montana(self):
        self.table_montana.add_row(('default', 0, 1, -1))
        self.table_caquot.update_data()
        self.table_simple_tri.update_data()
        self.table_double_tri.update_data()

    def save_montana(self):
        self.table_montana.save_selected_row()
        self.table_caquot.update_data()
        self.table_simple_tri.update_data()
        self.table_double_tri.update_data()

    def save(self):
        if not self.active_hyetograph_id is None:
            self.save_hyetograph()
        if not self.active_gage_rainfall_id is None:
            self.save_gage_rainfall()
        BaseDialog.save(self)
        self.close()

    def __gage_rainfall_data_values_cell_changed(self,row,column):
        if row == (self.gage_rainfall_data.rowCount()-1):
            self.gage_rainfall_data.setRowCount(self.gage_rainfall_data.rowCount() + 1)

    def __draw_gage(self):
        if not self.active_gage_rainfall_id is None:
            items = self.gage_list.selectedItems()
            if len(items)>0:
                selected_row = self.gage_list.row(items[0])
                t_mn_cumul_mm = self.get_gage_rainfall_data_table_items()
                if len(t_mn_cumul_mm)>0:
                    self.graph_gage.clear()
                    self.graph_gage.add_filled_line(
                        [r[0] for r in t_mn_cumul_mm],
                        [r[1] for r in t_mn_cumul_mm],
                        RainManager.curve_color,
                        self.gage_list.item(selected_row, 1).text(),
                        tr("Time (min)"), tr("Cumulated height (mm)"))
                else:
                    self.graph_gage.canvas.setVisible(False)
            else:
                self.graph_gage.canvas.setVisible(False)
        else:
            self.graph_gage.canvas.setVisible(False)

    def set_active_rain_gage(self):
        if not self.active_gage_rainfall_id is None:
            items = self.gage_list.selectedItems()
            if len(items)>0:
                selected_row = self.gage_list.row(items[0])
                if not self.active_rain_gage_id is None:
                    self.save_rain_gage()

                self.active_rain_gage_id = self.gage_list.item(selected_row,0).text()
                result = self.project.execute("""
                    select t_mn_hcum_mm from project.gage_rainfall_data
                    where rainfall={} and rain_gage={};
                """.format(self.active_gage_rainfall_id,self.active_rain_gage_id)).fetchone()
                t_mn_hcum_mm = result[0] if result else []
                self.set_gage_rainfall_data_table_items(t_mn_hcum_mm)
                self.gage_rainfall_data.setEnabled(True)
                self.__draw_gage()

    def save_rain_gage(self):
        if not (self.active_rain_gage_id is None or self.active_gage_rainfall_id is None):
            items = self.list_to_sql_array(self.get_gage_rainfall_data_table_items())
            if not items is None:
                self.project.execute("""update project.gage_rainfall_data
                    set t_mn_hcum_mm='{}' where rainfall={} and rain_gage={};""".format(
                    items, self.active_gage_rainfall_id,self.active_rain_gage_id))

    def set_gage_rainfall_data_table_items(self, data_array):
        self.gage_rainfall_data.setRowCount(0)
        if data_array is not None:
            n = len(data_array)
            self.gage_rainfall_data.setRowCount(n+1)
            for row in range(0, n):
                self.gage_rainfall_data.setItem(row,0,QTableWidgetItem(string.get_str(data_array[row][0])))
                self.gage_rainfall_data.setItem(row,1,QTableWidgetItem(string.get_str(data_array[row][1])))
        else:
            self.gage_rainfall_data.setRowCount(1)

    def get_gage_rainfall_data_table_items(self):
        n = self.gage_rainfall_data.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            if not self.gage_rainfall_data.item(i,0) == None and not self.gage_rainfall_data.item(i,1) == None:
                result.append(list())
                result[index_list].append(string.get_sql_float(self.gage_rainfall_data.item(i,0).text()))
                result[index_list].append(string.get_sql_float(self.gage_rainfall_data.item(i,1).text()))
                index_list = index_list + 1
        return result

    def set_active_gage_rainfall(self):
        items = self.table_gage_rainfall.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_gage_rainfall.table.row(items[0])
            if not self.active_gage_rainfall_id is None:
                self.save_gage_rainfall()

            self.active_gage_rainfall_id = self.table_gage_rainfall.table.item(selected_row,0).text()
            rain_gages = self.project.execute("""select rg.id, rg.name, grd.is_active
                from project.rain_gage as rg
                left join project.gage_rainfall_data as grd on rg.id = grd.rain_gage
                and grd.rainfall={}
                order by rg.name;""".format(self.active_gage_rainfall_id)
                ).fetchall()
            self.set_rain_gages_table_items(rain_gages)
            self.gage_list.setEnabled(True)

    def set_rain_gages_table_items(self, rain_gages):
        self.gage_list.setRowCount(0)
        self.active_rain_gage_id = None
        self.gage_rainfall_data.setRowCount(0)
        n = len(rain_gages)
        self.gage_list.setRowCount(n)
        for row in range(0, n):
            self.gage_list.setItem(row,0,QTableWidgetItem(string.get_str(rain_gages[row][0])))
            self.gage_list.setItem(row,1,QTableWidgetItem(string.get_str(rain_gages[row][1])))
            self.gage_list.setItem(row,2,QTableWidgetItem(""))
            if rain_gages[row][2]:
                self.gage_list.item(row,2).setCheckState(QtCore.Qt.Checked)
            else:
                self.gage_list.item(row,2).setCheckState(QtCore.Qt.Unchecked)

    def get_rain_gages_table_items(self):
        n = self.gage_list.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            result.append(list())
            result[index_list].append(self.gage_list.item(i,0).text())
            result[index_list].append(self.gage_list.item(i,1).text())
            result[index_list].append(self.gage_list.item(i,2).checkState()==QtCore.Qt.Checked)
            index_list = index_list + 1
        return result

    def delete_gage_rainfall(self):
        if not self.active_gage_rainfall_id is None:
            self.table_gage_rainfall.del_selected_row()
            self.active_gage_rainfall_id = None

    def new_gage_rainfall(self):
        if not self.active_gage_rainfall_id is None:
            self.save_gage_rainfall()
        self.table_gage_rainfall.add_row()
        self.set_active_gage_rainfall()

    def save_gage_rainfall(self):
        if not self.active_gage_rainfall_id is None:
            if not self.active_rain_gage_id is None:
                self.save_rain_gage()
            self.table_gage_rainfall.save_selected_row()
            items = self.get_rain_gages_table_items()
            if len(items)>0:
                values = list()
                for item in items:
                    values.append("({},{},{})".format(self.active_gage_rainfall_id, item[0], item[2]))
                sql= """insert into project.gage_rainfall_data(rainfall,rain_gage,is_active)
                    values {} on conflict (rainfall,rain_gage)
                    do update set is_active=excluded.is_active;
                    """.format(','.join(values))
                self.project.execute(sql)

    def __refresh_radar_rain(self):
        self.radar_rain_tree.clear()

        header = QTreeWidgetItem(["Id","Name", "File path"])
        self.radar_rain_tree.setHeaderItem(header)

        rains = self.project.execute("""select id, name, file, unit_correction_coef from project.radar_rainfall order by id asc;""").fetchall()
        attributes = {"x0": "X origin (m):",
                        "y0": "Y origin (m):",
                        "dx": "Horizontal pixel size (m):",
                        "dy": "Vertical pixel size (m):",
                        "nx": "Columns:",
                        "ny": "Rows:",
                        "t0": "Start time:",
                        "dt": "Timestep (s):",
                        "band_number": "Steps number:"}

        for radar_rain in rains:
            rain_node = QTreeWidgetItem(self.radar_rain_tree)
            rain_node.setExpanded(True)
            file_path = self.project.unpack_path(radar_rain[2])
            for i in range(2): # adds id, name and file in first line
                rain_node.setFont(i, bold)
                rain_node.setText(i, str(radar_rain[i]))
            rain_node.setFont(2, bold)
            rain_node.setText(2, file_path)
            if not (os.path.isfile(file_path) and os.path.isfile(file_path.replace('.vrt', '.time'))):
                rain_node.setForeground(2, QColor(255,0,0))
            else:
                rain_raster = RainVrt(file_path, self.project)
                for key, legend in attributes.iteritems():
                    param_node = QTreeWidgetItem(rain_node)
                    param_node.setText(1,legend)
                    param_node.setText(2,str(getattr(rain_raster, key)))
                    param_node.setTextAlignment(2,2)
                unit_cc_node = QTreeWidgetItem(rain_node)
                unit_cc_node.setText(1,"Unit correction coefficient")
                unit_cc_node.setText(2,str(radar_rain[3]))
                unit_cc_node.setTextAlignment(2,2)

        for j in range(3):
            self.radar_rain_tree.resizeColumnToContents(j)

    def __select_radar_rain(self):
        selected_item = self.radar_rain_tree.currentItem()
        if selected_item is not None:
            if selected_item.parent() is not None:
                selected_item = self.radar_rain_tree.currentItem().parent()
            self.__current_radar_rain_id = int(selected_item.text(0))
            self.__current_radar_rain_name = selected_item.text(1)
            self.__current_radar_rain_file = selected_item.text(2)
        else:
            self.__current_radar_rain_id = None
            self.__current_radar_rain_name = None
            self.__current_radar_rain_file = None

    def __add_radar_rain(self):
        file_url = QFileDialog.getOpenFileName(self, tr("Select a rain file"), self.project.get_project_dir(), tr("Hydra rain (*.vrt)"))
        if file_url:
            name, ext = os.path.splitext(os.path.basename(file_url))
            if not os.path.isfile(file_url.replace('.vrt', '.time')):
                QMessageBox.critical(self, tr('Radar rain error'), tr('Error importing file {}. Required file {} not found.').format(os.path.basename(file_url), os.path.basename(file_url.replace('.vrt', '.time'))), QMessageBox.Ok)
            else:
                self.project.execute("""insert into project.radar_rainfall(name, file) values ('{n}', '{f}');""".format(n=name, f=self.project.pack_path(file_url)))
        self.__refresh_radar_rain()

    def __add_tiffs(self):
        files = QFileDialog.getOpenFileNames(self, tr("Select one or more files to open"), self.project.get_project_dir(), tr("Images (*.asc *.tiff *.tif)"))
        new_radar_rain_dialog = RadarRainDialog(self)
        new_radar_rain_dialog.exec_()
        name, template = new_radar_rain_dialog.get_result()
        if name and files and (12*'?' in template):
            vrt = self.project.build_rain_multivrt(name, files, template, self.project.srid)
            self.__refresh_radar_rain()

    def __delete_radar_rain(self):
        self.project.execute("""delete from project.radar_rainfall where id={};""".format(self.__current_radar_rain_id))
        self.__refresh_radar_rain()

    def write_rain_file(self, model):
        rain_raster = RainVrt(self.__current_radar_rain_file, self.project)
        unit_cc = self.project.execute("""select unit_correction_coef from project.radar_rainfall where id={}""".format(self.__current_radar_rain_id)).fetchone()[0]
        file = os.path.join(self.project.radar_rain().data_directory, self.__current_radar_rain_name+"_"+model+".rad")
        catchments = self.project.execute("""select id, name from {}.catchment""".format(model)).fetchall()

        progress = QProgressDialog(self)
        progress.setWindowTitle(tr("Exporting rain"))
        progress.setLabelText(tr('Init...'))
        progress.setMinimum(0)
        progress.setMaximum(len(catchments))
        progress.setCancelButtonText(None)
        progress.show()
        progress.setWindowModality(1) # set QProgress dialog as modal

        i=0
        with open(file, 'w') as f:
            for catchment in catchments:
                i+=1
                progress.setLabelText(tr('Catchment {}').format(catchment[1]))
                progress.setValue(i)
                progress.show()
                f.write(rain_raster.export_hyetogram(catchment, model, unit_cc))
        progress.close()
        return file

    def __export_selected_rain(self):
        if self.__current_radar_rain_id is not None:
            rain = self.project.execute("""select name, file from project.radar_rainfall where id={};""".format(self.__current_radar_rain_id)).fetchone()
            rain_raster = RainVrt(self.project.unpack_path(rain[1]), self.project)
            res=[]
            if self.project.get_models()>0:
                for model in self.project.get_models():
                    f=self.write_rain_file(model)
                    res.append(f)
                if res:
                    QMessageBox.information(self, tr('Rain file generated'),
                                            tr("""Successfully created files {} in folder {}.""").format(', '.join([os.path.basename(file) for file in res]), os.path.dirname(res[0])), QMessageBox.Ok)

