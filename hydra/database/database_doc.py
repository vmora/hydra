# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
hydra dabase schema documentation module

USAGE
    database_doc.py [-h]

    generates RST on stdout

OPTIONS
    -h, --help:w
        print this help

"""

from tabulate import tabulate
import codecs
import locale
import string
import re
import os
import json
from ..utility.sql_json import instances
from .database import data_version

_HYDRA_DB_MODULE = os.path.abspath(os.path.join(os.path.dirname(__file__),'..', 'server', 'hydra'))

class DatabaseDocumentation(object):
    def __init__(self):
        self.__dot_nodes = u""
        self.__dot_links = u""
        self.__instances = instances

        # tables in various schema
        self.tables = {'hydra':{}, 'model':{}, 'project':{}}

        self.doc_sql("model", 'model_1.sql')
        self.doc_sql("model", 'model_2_domain.sql')
        self.document_model_abstract()
        self.document_tables("model", "node_type")
        self.document_tables("model", "singularity_type")
        self.document_tables("model", "link_type")
        self.document_tables("model", "cross_section_type")
        self.doc_sql("model", 'model_3_cross_section.sql')
        self.doc_sql("model", 'model_4_singularity.river_cross_section_profile.sql')
        self.doc_sql("model", 'model_5_functions.sql')
        self.doc_sql("model", 'model_6_utility.sql')
        self.document_hydra()
        self.doc_sql("project", 'project_1_inflow.sql')
        self.doc_sql("project", 'project_2_utility.sql')
        self.doc_sql("project", 'project_3_scenario.sql')
        self.document_tables("project", "rainfall_type")
        self.doc_sql("project", 'project_5_gage_rainfall.sql')

    @staticmethod
    def dot_node(schema, name, tbl):
        color = 'grey90'
        if re.match('.*_link$', name):
            color = 'green'
        elif re.match('.*_node$', name):
            color = 'blue'
        elif re.match('.*_cross_section$', name):
            color = 'red'
        elif re.match('.*_singularity$', name):
            color = 'yellow'
        elif re.match('.*_rainfall$', name):
            color = 'purple'


        raws = [u'<tr><td bgcolor="{}" colspan="3">{}</td></tr>'.format(
            color,
            schema+'.'+name
            )]
        raws += [u'<tr><td port="{}" align="left">{}</td><td align="left">{}</td><td align="left">{}{}</td></tr>'.format(
            raw[0].replace("*", ""), raw[0],
            raw[1],
            (raw[2].replace(u"calculé", "comp"))+" " if len(raw[2]) else "",
            raw[3].replace("primary key", "PK").replace("<", "&lt;").replace(">", "&gt;").replace("voir ci-dessous", "yes")
            ) for raw in tbl] \
                    if tbl else \
            [u'<tr><td port="id" align="left">id</td><td align="left">integer</td><td align="left">PK</td></tr>']
        return u'    {}_{} [shape=plaintext, label=< <table border="1" cellborder="0" cellspacing="0"> {} </table> >]'.format(
                schema,
                name,
                "".join(raws))
    def dot(self):
        "return a string for a .dot graph of relations"
        dot  =u"    digraph R {\n"
        dot += "        rankdir=LR\n\n"
        dot += "\n".join(["        "+line for line in self.__dot_nodes.split('\n')])
        dot += "\n".join(["        "+line for line in self.__dot_links.split('\n')])

        dot += "\n    }\n"
        return dot

    def doc_sql(self, schema, file_):
        sql = open(os.path.join(_HYDRA_DB_MODULE, file_)).read()
        sql = sql.replace('$version', str(data_version()))
        sql = re.sub(r"--.*$", "", sql, flags=re.MULTILINE)
        for statement in sql.split(";;"):
            spl = statement.split("create table ")
            if len(spl) > 2:
                print statement
            assert len(spl) <=2
            if len(spl) == 2:
                lines = spl[1].split('\n')
                name = re.sub(r'^.+\.([a-z_][a-z0-9_]+) *\(', r'\g<1>', lines[0])
                decl = [re.sub(r",$", "", line.replace("    ","",1)) for line in lines[1:-2]]
                self.document_table(schema, {"name":name, "default":{}, "decl":decl})

    def document_table(self, schema, table, prefix="", suffix=""):
        "return rst doc string for table"
        subtitle = prefix+table["name"]+suffix
        doc = subtitle+"\n"
        doc += "-"*len(subtitle)+"\n"
        doc += "type=%d\n\n\n"%(table["id"]) if "id" in table else "\n\n\n"
        tbl = []
        for line in table["decl"]:
            if line[0]!=" ":
                line = re.sub('\$\{([^\}]+)\}', '\g<1>', line).replace('$', '')
                tbl.append(line.split()[0:2]+["", " ".join(line.split()[2:]), ""])

        constrains = [re.sub('\$\{([^\}]+)\}', '\g<1>', re.sub(r"^ +", "", line)).replace('$', '')
                for line in table["decl"] if line[0]==' ']
        computed = {}
        for i in range(len(tbl)):
            name = tbl[i][0]
            tbl[i][0] += "" if re.match(".*(?<!(is ))not null.*|.*primary key.*", tbl[i][3]) else "*"
            if re.match(r".*references.*", tbl[i][3]):
                tbl[i][4] = re.sub(r".*references ([^ ]+).*", r"\g<1>", tbl[i][3])
                tbl[i][4]+= u' :ref:`🔗 <{}>`'.format(re.sub('[_\.]+', '-', re.sub('\([^\)]+\)', '', tbl[i][4])))
                tbl[i][3] = re.sub(r"references [^ ]+", r"", tbl[i][3])
                attr = name + \
                        ("" if re.match(r".*(?<!(is ))not null.*",  tbl[i][3]) else "*") + \
                        ("(1)" if re.match(r".*unique.*",  tbl[i][3]) else u"(∞)")

            if re.match(r".*default.*", tbl[i][3]):
                table["default"][name] = re.match(r".*default ([^ ]+).*", tbl[i][3]).group(1)
                tbl[i][3] = re.sub("default ([^ ]+)", "", tbl[i][3])

            #if lentbl[i][3].split("check")

            tbl[i][3] = re.sub(r"(?<!(is ))not null", "", re.sub(r".*check\((.*)\)", r"\g<1>", tbl[i][3]))#.replace("not null", "")
            if len(tbl[i][3]) > 20:
                constrains.append(tbl[i][3])
                tbl[i][3] = "voir ci-dessous"

            if name in table["default"]:
                if table["default"][name][0]!="(" and len(table["default"][name])<20:
                    tbl[i][2] = table["default"][name]
                else:
                    tbl[i][2] = u"calculé"
                    computed[name] = re.sub(r"^\((.*)\)$", r"\g<1>", table["default"][name])

            if tbl[i][1][:8]=="geometry":
                tbl[i][1] = re.sub(r"geometry\('([A-z]+)',srid\)", r'\g<1>', tbl[i][1])

        doc += tabulate(tbl, (u"Attribut", u"Type", u"Défaut", u"Contrainte", u"Clef Etrangère"), tablefmt='rst', numalign="left")
        #print tabulate(tbl, (), tablefmt='html', numalign="left").replace("\n"," ")+"\n"


        self.__dot_nodes += DatabaseDocumentation.dot_node(schema, prefix+table["name"]+suffix, tbl) + '\n\n'

        for attr, type_, default, constrain, foreign in tbl:
            if len(foreign):
                self.__dot_links += u'{}_{}:{} -> {}\n'.format(
                        schema,
                        prefix+table["name"]+suffix,
                        attr.replace("*", ""),
                        re.sub('\.', '_', re.sub(':ref:.*', '', foreign.replace(")", "").replace("(", ":")))#.replace(":id", "")
                        )

        if computed:
            doc += u"\n\nChamps calculés:"

        for name, value in computed.iteritems():
            doc += "\n\n    %s\n\n"%(name)
            doc += "    .. code-block:: sql\n\n"
            doc += " "*8+value.replace(" where ", "\n"+" "*8+"where ").replace(" from ", "\n"+" "*8+"from ")

        if len(constrains):
            doc += u"\n\nContraintes:"
        for constrain in constrains:
            doc += "\n\n    .. code-block:: sql\n\n"
            doc += " "*8+re.sub("^check\((.*)\)$", r"\g<1>", re.sub("^ +", "", constrain.replace(
                    "references", "\n"+" "*8+"references").replace(
                    " and ", "\n"+" "*8+"and "))).replace(" \n", "\n")

        for k in ["link_check", "node_check"]:
            if k in table:
                doc += u"\n\nContraintes de connectivité:\n\n"
                doc += "    .. code-block:: sql\n\n"
                doc += " "*8+re.sub(r"^\(([^)]+)\)$", r"\g<1>", table[k]).replace(
                        " and ", "\n"+" "*8+"and ").replace(
                        " in ", " in\n"+" "*8).replace(
                        " or ", "\n"+" "*8+"or ")

        if "validity" in table and len(table["validity"]):
            doc += u"\n\nValidité:\n\n"
            doc += "    .. code-block:: sql\n\n"
            doc += "\n".join("        "+rule for rule in table["validity"])
            doc += "\n"

        self.tables[schema][subtitle] = "\n".join([
            re.sub('\$\{([^\}]+)\}', '\g<1>', line.replace('nid_', 'nid')).replace('$', '')
            for line in doc.split('\n')])+"\n"
        #self.tables[schema][subtitle] += '\n'

    def document_tables(self, schema, type_):

        name = type_.replace("_type", "")
        first = self.__instances[type_].values()[0]
        if "decl" not in first:
            return ""

        for n, table in self.__instances[type_].iteritems():
            table['name'] = n
            self.document_table(schema, table, "_", '_'+name)
            if type_ == "rainfall_type" and table["name"] == "gage":
                self.doc_sql(schema, 'project_5_gage_rainfall.sql')
            elif type_ == "rainfall_type" and table["name"] == "radar":
                self.doc_sql(schema, 'project_1_inflow.sql')


    def document_model_abstract(self):

        sql = re.sub(r"--.*\n", "", open(os.path.join(_HYDRA_DB_MODULE, 'model_1.sql')).read())
        for statement in sql.split(";;"):
            spl = statement.split("create table ")
            assert len(spl) <=2
            if len(spl) == 2:
                lines = spl[1].split('\n')
                name = lines[0][7:-1]
                if name[1:]+"_type" in self.__instances and "decl" in self.__instances[name[1:]+"_type"].values()[0]:
                    decl = [re.sub(r",$", "", line.replace("    ","",1)) for line in lines[1:-2]]
                    self.document_table("model", {"name":name, "default":{}, "decl":decl})

    def document_hydra(self):

        documented_json = set()
        for type_, tables in self.__instances.iteritems():
            name = type_
            self.tables['hydra'][name] = name+"\n"
            self.tables['hydra'][name] += "-"*len(name)+"\n\n\n\n"

            first = tables.values()[0]
            header = [key for key in ['id', 'abbreviation', 'class_hydra', 'group_hydra_no', 'description', 'nb_param'] if key in first]
            tbl = [[n] + [table[key] for key in header] for n, table in tables.iteritems()]
            header = ['name'] + header
            self.tables['hydra'][name] += tabulate(tbl, header, tablefmt='rst', numalign="left")+'\n'
            self.__dot_nodes += DatabaseDocumentation.dot_node('hydra', name, None) + '\n\n'

            if "params" in first and type_ not in documented_json:
                documented_json.add(type_)
                for name_, instance in self.__instances[type_].iteritems():
                    subtitle = "{} ({})".format(name_, type_)
                    self.tables['hydra'][name] += "\n"+subtitle+"\n"
                    self.tables['hydra'][name] += "-"*len(subtitle)+"\n\n"
                    header = ['type', 'default', 'constrain', 'place']
                    tbl = [[k] + [v[n] if n in v else None for n in header] for k,v, in instance["params"].iteritems()]
                    header = ['name'] + header
                    self.tables['hydra'][name] += tabulate(tbl, header, tablefmt='rst', numalign="left")+"\n"


        # doc json fields
        self.tables['hydra']['json_fields'] = ""
        for key, types in self.__instances.iteritems():
            first = types.values()[0]
            if "params" in first and key not in documented_json:
                raise RuntimeError("%s are not documented"%(key))


if __name__ == "__main__":
    import sys
    import getopt
    import tempfile
    import shutil
    from graphviz import Source

    # Wrap sys.stdout into a StreamWriter to allow writing unicode.
    #sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)

    # parse CLI arguments

    dbname = None
    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "h",
                ["help"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    doc = DatabaseDocumentation()

    tempdir = tempfile.gettempdir()

    for schema, tables in doc.tables.iteritems():
        if os.path.isdir(os.path.join(tempdir, schema)):
            shutil.rmtree(os.path.join(tempdir, schema))
        os.mkdir(os.path.join(tempdir, schema))
        for table, rst in tables.iteritems():
            open(os.path.join(tempdir, schema, table+'.include.rst'), 'w').write(rst.encode('utf8'))


    #if dot:
    #    open(dot, "w").write(doc.dot().encode('utf8'))
    print doc.dot().encode('utf8')
    if len(args):
        Source(doc.dot()).render(args[0], view=False)


