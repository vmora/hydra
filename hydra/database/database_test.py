# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run database tests

USAGE

   database_test.py [-dh]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -r, --reset
        reset template database

"""

assert __name__ == "__main__"

import sys
import os
from subprocess import Popen, PIPE
import shlex
from .database import create_project, create_model, TestProject, project_exists
import psycopg2
from psycopg2.extras import LoggingConnection
import logging
import tempfile
import getopt
from ..utility.settings_properties import SettingsProperties

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdrk",
            ["help", "debug", "reset", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

# test model creation

sys.stdout.write("test model creation... ")
sys.stdout.flush()

debug = "-d" in optlist or "--debug" in optlist
reset = "-r" in optlist or "--reset" in optlist
keep = "-k" in optlist or "--keep" in optlist

if reset:
    TestProject.reset_template(debug)

dbname = "database_test"
tes_project = TestProject(dbname, keep, with_model=True)

db = SettingsProperties.get_db()
pg = SettingsProperties.get_postgre_settings()
con = psycopg2.connect(database=dbname, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'], connection_factory=LoggingConnection)
logging.basicConfig(level=logging.ERROR if not debug else logging.DEBUG)
logger = logging.getLogger(__name__)
con.initialize(logger)

cur = con.cursor()
create_model(cur, "modele1", 2154)
cur.execute("drop schema if exists brut cascade")
cur.execute("create schema brut")
con.commit()
con.close()

sys.stdout.write("ok\n")
sys.stdout.write("load test shapefiles ... ")
sys.stdout.flush()

current_dir = os.path.abspath(os.path.dirname(__file__))

test_data = os.path.join(current_dir, "test_data", "sketchy_model")

for table in [f[:-4] for f in os.listdir(test_data) if f.endswith(".shp")]:
    cmd = shlex.split("shp2pgsql -I -S -s 2154")+[
        os.path.join(test_data, table+'.shp'),
        "brut."+table]
    sql, err = Popen(cmd, stdout=PIPE, stderr=PIPE).communicate()
    # shp2pgsql is verbose on stderr (i.e. supid), uncomment this for debugging
    #if err :
    #    sys.stderr.write(err)
    #    exit(1)
    cmd = ["psql", '-h', db['host'], '-p', str(db['port']), '-d', dbname, '-U', pg['user']]
    out, err = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=PIPE).communicate(sql)
    if err :
        sys.stderr.write(" ".join(cmd)+'\n')
        sys.stderr.write(err)
        exit(1)

sys.stdout.write("ok\n")
sys.stdout.write("run database_test.sql and compare output... ")
sys.stdout.flush()

ref_output = os.path.join(test_data, "database_test.sql.ref")
test_output = os.path.join(tempfile.gettempdir(), "database_test.sql.out")

cmd = shlex.split("psql -tXA ") \
        +['-h',db['host'],'-p',str(db['port']),'-U',pg['user'],'-f',os.path.join(test_data, "database_test.sql"), dbname]
out, err = Popen(cmd, stdout=open(test_output, "w"), stderr=PIPE).communicate()
if err :
    sys.stderr.write(err)
    sys.stderr.write("error while running SQL command from {}\n".format(os.path.join(current_dir, "database_test.sql")))
    exit(1)

if open(test_output).read().replace('\n','').replace('\nt','') != open(ref_output).read().replace('\n','').replace('\nt',''):
    sys.stderr.write("diff found between {} and {}\n".format(ref_output, test_output))
    exit(1)

os.remove(test_output)

sys.stdout.write("ok\n")

