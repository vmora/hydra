# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
export database for computation
"""

import psycopg2
import struct
import ctypes
import numpy
import json
import os
import shutil
import re
from hydra.kernel import hydra_kernel
from hydra.utility import string


class ExportCartoData:

    def __init__(self, project):
        self.__project = project
        self.log = project.log


    #4.1
    def __exportcarto_controlfile(self, id_scenario, namescenario, lstmodel, streamf):

        streamf.write("*scenario\n%s\n\n" % namescenario)

        streamf.write("*reseau\n")
        for model in lstmodel:
            streamf.write("%s\n" % model)
        streamf.write("\n")

        tbegin_output_hr, tend_output_hr, dt_output_hr = self.__project.execute("""
            select tbegin_output_hr, tend_output_hr, dt_output_hr from project.scenario where id={id_scenario}
            """.format(id_scenario=id_scenario)).fetchone()
        streamf.write("*timeh\n %15.3f %15.3f %10.3f\n\n" % (tbegin_output_hr, tend_output_hr, dt_output_hr))

        srid, = self.__project.execute("""select srid from hydra.metadata""").fetchone()
        streamf.write("*sor_mif\n")
        if   (srid==2154): streamf.write('CoordSys Earth Projection 3, 33, "m", 3, 46.5, 44, 49.00000000002, 700000, 6600000 Bounds (75000, 6000000) (1275000, 7200000)')
        elif (srid==3942): streamf.write('CoordSys Earth Projection 3, 33, "m", 3, 42, 41.25, 42.75, 1700000, 1200000 Bounds (1000000, 1000000) (2400000, 1400000)') # Lambert-93 CC42 (zone 1) - Borné
        elif (srid==3943): streamf.write('CoordSys Earth Projection 3, 33, "m", 3, 43, 42.25, 43.75, 1700000, 2200000 Bounds (1000000, 2000000) (2400000, 2400000)') # Lambert-93 CC43 (zone 2) - Borné
        elif (srid==3944): streamf.write('CoordSys Earth Projection 3, 33, "m", 3, 44, 43.25, 44.75, 1700000, 3200000 Bounds (1000000, 3000000) (2400000, 3400000)') # Lambert-93 CC44 (zone 3) - Borné
        elif (srid==3945): streamf.write('CoordSys Earth Projection 3, 33, "m", 3, 45, 44.25, 45.75, 1700000, 4200000 Bounds (1000000, 4000000) (2400000, 4400000)') # Lambert-93 CC45 (zone 4) - Borné
        elif (srid==3946): streamf.write('CoordSys Earth Projection 3, 33, "m", 3, 46, 45.25, 46.75, 1700000, 5200000 Bounds (1000000, 5000000) (2400000, 5400000)') # Lambert-93 CC46 (zone 5) - Borné
        elif (srid==3947): streamf.write('CoordSys Earth Projection 3, 33, "m", 3, 47, 46.25, 47.75, 1700000, 6200000 Bounds (1000000, 6000000) (2400000, 6400000)') # Lambert-93 CC47 (zone 6) - Borné
        elif (srid==3948): streamf.write('CoordSys Earth Projection 3, 33, "m", 3, 48, 47.25, 48.75, 1700000, 7200000 Bounds (1000000, 7000000) (2400000, 7400000)') # Lambert-93 CC48 (zone 7) - Borné
        elif (srid==3949): streamf.write('CoordSys Earth Projection 3, 33, "m", 3, 49, 48.25, 49.75, 1700000, 8200000 Bounds (1000000, 8000000) (2400000, 8400000)') # Lambert-93 CC49 (zone 8) - Borné
        elif (srid==3950): streamf.write('CoordSys Earth Projection 3, 33, "m", 3, 50, 49.25, 50.75, 1700000, 9200000 Bounds (1000000, 9000000) (2400000, 9400000)') # Lambert-93 CC50 (zone 9) - Borné
        elif (srid==27561): streamf.write('CoordSys Earth Projection 3, 1002, "m", 0, 49.5, 48.5985227778, 50.39591167, 600000, 200000 Bounds (-40092, -818525) (1264450, 486383)')   # Lambert nord - Borné
        elif (srid==27562): streamf.write('CoordSys Earth Projection 3, 1002, "m", 0, 46.8, 45.8989188889, 47.69601444, 600000, 200000 Bounds (-42173, -517074) (1266604, 785681)')   # Lambert centre - Borné
        elif (srid==27563): streamf.write('CoordSys Earth Projection 3, 1002, "m", 0, 44.1, 43.1992913889, 44.99609389, 600000, 200000 Bounds (-45826, -217324) (1270389, 1086104)')   # Lambert sud - Borné
        elif (srid==27564): streamf.write('CoordSys Earth Projection 3, 1002, "m", 0, 42.165, 41.5603877778, 42.767663333, 234.358, 185861.36900000001 Bounds (-649267, -17367) (674434, 1288446)') # corse
        elif (srid==27571): streamf.write('CoordSys Earth Projection 3, 1002, "m", 0, 49.5, 48.5985227778, 50.39591167, 600000, 1200000 Bounds (-40092, 181475) (1264450, 1486383)')   # Lambert carto 1 - Borné
        elif (srid==27572): streamf.write('CoordSys Earth Projection 3, 1002, "m", 0, 46.8, 45.8989188889, 47.69601444, 600000, 2200000 Bounds (-42173, 1482926) (1266604, 2785681)')   # Lambert carto 2 - Borné
        elif (srid==27573): streamf.write('CoordSys Earth Projection 3, 1002, "m", 0, 44.1, 43.1992913889, 44.99609389, 600000, 3200000 Bounds (-45826, 2782676) (1270389, 4086104)')   # Lambert carto 3 - Borné
        elif (srid==27574): streamf.write('CoordSys Earth Projection 3, 1002, "m", 0, 42.165, 41.5603877778, 42.76766333, 234.358, 4185861.3689999999 Bounds (-649267, 3982633) (674434, 5288446)')   # Lambert carto 4 - Borné
        elif (srid==32636): streamf.write('CoordSys Earth Projection 8, 104, "m", 33, 0, 0.9996, 500000, 0 Bounds (-7745844.29597, -9997964.94324) (8745844.29597, 9997964.94324)')   # WSG84 Mercator transverse (UTM48 Nord) [angkor]
        elif (srid==32648): streamf.write('CoordSys Earth Projection 8, 104, "m", 105, 0, 0.9996, 500000, 0 Bounds (-7745844.29597, -9997964.94324) (8745844.29597, 9997964.94324)')   # WSG84 Mercator transverse (UTM48 Nord) [liban]
        else: self.log.error("Unexpected coordinate system, srid: %d" % srid)
        streamf.write("\n\n")

        return

    def __exportmodels(self, namescenario, lstmodel, directory):
        """
        export module for calculation: loop on models
        namescenario: name of a scenario
        lstmodel:   (tuple): array of models (formerly "réseaux")
        """

        for model in lstmodel:
            nomfichier=os.path.join(directory, model+"_geom.dat")
            with open(nomfichier,"w") as streamfgeom:
                self.__exportcarto_reach(model, streamfgeom)
                self.__exportcarto_2d(model, streamfgeom)
                self.__exportcarto_storage(model, "storage_node", streamfgeom)
                self.__exportcarto_street(model, streamfgeom)

            nomfichier=os.path.join(directory, model+"_ctrz.dat")
            with open(nomfichier,"w") as streamfgeom, \
                 open(os.path.join(directory, model+"_geom.mif"),"w") as streamfmif:
                self.__exportcarto_ctrz(model, streamfgeom, streamfmif)
        return

    def export(self, id_scenario):
        """
        export module for calculation: main function
        id_scenario: id of a scenario
        """

        # Get list of models and groups
        lstreseau = [name for name, in self.__project.execute("""
            select name
            from project.model
            """).fetchall()]

        try:
            name_scenario, =  self.__project.execute("""
                select name
                from project.scenario
                where id=%d
                """ %(id_scenario)).fetchone()
        except TypeError:
            exit(1)

        #  directories
        cur_dir = os.getcwd()
        directory_scenario = self.__project.get_senario_path(id_scenario)
        directory_calculation = os.path.join(directory_scenario, "travail")

        # Create control file
        with open(os.path.join(directory_calculation, "triangulz.ctl"),"w") as streamf:
            self.__exportcarto_controlfile(id_scenario, name_scenario, lstreseau, streamf)

        self.log.notice("Carto data exported to {}".format(directory_scenario))
        os.chdir(cur_dir)


