# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
run database doc tests

USAGE

   database_doc_test [-dh]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

"""

from .database_doc import DatabaseDocumentation
import sys
import getopt
import tempfile
import shutil
import os
import re
import tempfile
import difflib

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hd",
            ["help", "debug"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

sys.stdout.write("test if model structure correspont to specs... ")
sys.stdout.flush()


# test model structure is the same as specified by the doc
current_dir = os.path.abspath(os.path.dirname(__file__))
hydra_dir = os.path.dirname(current_dir)
doc_dir = os.path.join(hydra_dir, "docs")
doc_source_dir = os.path.join(doc_dir, "source")
tempdir = tempfile.gettempdir()

doc = DatabaseDocumentation()
ok  = True

for schema in ["model", "project", "hydra"]:
    if os.path.isdir(os.path.join(tempdir, schema)):
        shutil.rmtree(os.path.join(tempdir, schema))
    os.mkdir(os.path.join(tempdir, schema))
    doc_schema_dir = os.path.join(doc_source_dir, schema)
    for root, dirs, files in os.walk(doc_schema_dir):
        for file_ in files:
            if re.match('.*\.include\.rst$', file_):
                table = re.sub('\.include\.rst$', '', file_)
                if not table in doc.tables[schema]:
                    sys.stderr.write("{} missing in {}\n".format(table, schema))
                    ok &= False
                else:
                    gen_filename = os.path.join(tempfile.gettempdir(), table+'.include.rst')
                    open(gen_filename, 'w').write(doc.tables[schema][table].encode('utf-8'))
                    ref_filename = os.path.join(doc_schema_dir, file_)
                    ref = [line for line in open(ref_filename).readlines() if not re.match(r'^(=+ +)+=+$', line)]
                    gen = [line for line in open(gen_filename).readlines() if not re.match(r'^(=+ +)+=+$', line)]
                    diff = difflib.unified_diff(ref, gen, fromfile=ref_filename, tofile=gen_filename)
                    nodiff = True
                    for line in diff:
                        #print line.rstrip(), re.match(r'(=+ +)+', line)
                        nodiff &= False
                    if not nodiff:
                        sys.stderr.write("found diff {} {}\n".format(ref_filename, gen_filename))
                        ok &= False



# def check(ref_filename, generated_doc):
#     gen_filename = os.path.join(tempfile.gettempdir(), "generated_"+ref_filename)
#     open(gen_filename, 'w').write(generated_doc.encode('utf-8'))
#     ref_filename = os.path.join(doc_source_dir, ref_filename)
#     ref = open(ref_filename).readlines()
#     gen = open(gen_filename).readlines()
#     diff = difflib.unified_diff(ref, gen, fromfile=ref_filename, tofile=gen_filename)
#     try:
#         diff.next()
#         sys.stderr.write("diff found between specification {} and generated model documentation {}\n".format(ref_filename, gen_filename))
#         return False
#     except StopIteration:
#         return True
#
# ok &= check("model_domain.rst", dd.model_domain)
# ok &= check("model_abstract.rst", dd.model_abstract)
# ok &= check("model_node.rst", dd.model_node)
# ok &= check("model_singularity.rst", dd.model_singularity)
# ok &= check("model_cross_section.rst", dd.model_cross_section)
# ok &= check("model_link.rst", dd.model_link)
# ok &= check("hydra_schema.rst", dd.hydra_schema)
# ok &= check("project_scenario.rst", dd.project_scenario)
# ok &= check("project_hydrology.rst", dd.project_hydrology)
#
# with open(os.path.join(tempfile.gettempdir(), "references.dot"), "w") as dot:
#     dot.write(dd.dot().encode('utf8'))

if not ok:
    sys.stderr.write("failed: differences found between generated documentation and specs\n")
    exit(1)


sys.stdout.write("ok\n")
# sql scripts

