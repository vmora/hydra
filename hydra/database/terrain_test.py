# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
run terrain tests

USAGE

    terrain_test [-hd] [files]

    if files is specified, create terrain and hilshade from them

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode

   -r, --reset
        reset template database

   -k, --keep
        keep test database


"""

import sip
for cls in [u'QDate', u'QDateTime', u'QString', u'QTextStream', u'QTime', u'QUrl', u'QVariant']:
    sip.setapi(cls, 2)
import os
import sys
import numpy
from shutil import rmtree
from tempfile import mkdtemp
from getopt import getopt
from hydra.project import Project
from hydra.utility.log import LogManager
from hydra.utility.timer import Timer
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.database.terrain import Terrain
from hydra.database.database import TestProject, project_exists, remove_project

hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")

class CustomLogger(ConsoleLogger):
    def __init__(self):
        ConsoleLogger.__init__(self)

    def warning(self, title, message):
        sys.stdout.write("Warning: " + message + "\n")

try:
    optlist, args = getopt(sys.argv[1:],
            "hdrk",
            ["help", "debug", "reset", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)
debug = "-d" in optlist or "--debug" in optlist
reset = "-r" in optlist or "--reset" in optlist
keep = "-k" in optlist or "--keep" in optlist

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

project_name = "terrain_test"

if project_exists(project_name):
    remove_project(project_name)
if os.path.exists(os.path.join(hydra_dir, project_name)):
    rmtree(os.path.join(hydra_dir, project_name))

if reset:
    TestProject.reset_template(debug)

test_project = TestProject(project_name, debug=keep)
project = Project.load_project(project_name)

data_dir = os.path.join(os.path.dirname(__file__), 'test_data')

terrain = Terrain(os.path.join(project.directory, 'terrain'), project)

assert not terrain.has_data()

terrain.build_vrt(args or [os.path.join(data_dir, '2013_012.asc'),
                          os.path.join(data_dir, '2013_013.asc')])

assert terrain.has_data()

assert abs(terrain.altitude((997145.3, 6559237.3)) - 1098) < 1
assert terrain.altitude((0, 6559237.3)) == 9999
assert terrain.altitude((9997145.3, 6559237.3)) == 9999

if debug:
    print "testing rotation"
    import matplotlib.pyplot as plt

    l1 = ((997259.236097043, 6559019.59459126), (997166.65145224, 6559196.08407041))
    l2 = ((997108.786049238, 6559291.56198537), (997069.726902212, 6559463.7115593))
    l3 = ((997046.580741011, 6559592.46208098), (997217.283679867, 6559541.82985335))

    for l in [l1, l2, l3]:
        timer = Timer()
        img = terrain.altitudes(l, 50)
        if img is None:
            continue
        print numpy.mean(numpy.amax(img, 0))
        print timer.reset('total')
        #img = terrain.altitudes(l2, 50)
        #img = terrain.altitudes(l3, 50)
        plt.figure(1)
        ax = plt.subplot(221)
        #ax.imshow(orig)
        #ax.plot(corners[:,1], corners[:,0])
        ax = plt.subplot(222)
        ax.imshow(img)
        ax = plt.subplot(223)
        ax.plot(numpy.sort(numpy.amax(img, 1)))
        ax = plt.subplot(224)
        ax.plot(numpy.amax(img, 0))
        plt.show()

#import matplotlib.pyplot as plt
#
#terrain = Terrain('/home/vmo/.hydra/mesh/terrain', 2154, LogManager())
#img, orig, corners =terrain.altitudes(((998474.561575991, 2402692.80549499), (998489.64507541, 2402692.88504641)), 1.50837091974)
#plt.figure(1)
#ax = plt.subplot(121)
#ax.imshow(orig)
#ax.plot(corners[:,1], corners[:,0])
#ax = plt.subplot(122)
#ax.imshow(img)
#plt.show()

print 'ok'


