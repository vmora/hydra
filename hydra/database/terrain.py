# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from subprocess import Popen, PIPE
import os, re
from osgeo import gdal
import struct
from math import sqrt, ceil, floor
import numpy
from PyQt4.QtCore import QCoreApplication
from hydra.utility.timer import Timer


def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

class Terrain(object):
    "manage terrain data"
    def __init__(self, data_directory, project):
        self.__data_directory = data_directory
        if not os.path.isdir(self.__data_directory):
            os.mkdir(self.__data_directory)
        self.__project = project
        self.__log_manager = project.log
        self.__datasource = None

    def __getattr__(self, attr):
        if attr == 'data_directory':
            return self.__data_directory
        else:
            raise AttributeError

    def build_vrt(self, source_files, srid=None):
        """add a list of files to terrain data
        uses gdal to convert data to geotif"""

        created_files = []
        name = os.path.abspath(os.path.join(self.__data_directory, "terrain_"))
        (id_,) = self.__project.execute("""insert into project.dem(source) values ('{}'||lastval()||'.vrt') returning  id""".format(self.__project.pack_path(name))).fetchone()
        vrt_name = name+str(id_)+'.vrt'

        progress = self.__log_manager.progress(tr("Processing terrain data"))
        for i, file_ in enumerate(source_files):
            progress.set_ratio(float(i+1)/len(source_files))

            created_files.append(os.path.join(self.__data_directory, "terrain_"+str(id_)+"_"+str(i)+'.tif'))
            cmd = ['gdalwarp'] \
                + ['-ot', 'Float32'] \
                + (['-s_srs', 'EPSG:%d'%(int(srid))] if srid else []) \
                + ['-t_srs', 'EPSG:%d'%(int(self.__project.srid)),
                   file_,
                   created_files[-1]]
            if os.name == 'posix':
                out, err = Popen(cmd, stdout=PIPE, stderr=PIPE).communicate()
            else:
                out, err = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True).communicate()
            if err:
                for f in created_files[:-1]:
                    os.remove(f)
                self.__log_manager.error(err)
                self.__project.execute("""delete from project.dem where id ={}""".format(id_))
                return
        del progress

        if len(created_files):
            if os.name == 'posix':
                cmd = ['gdalbuildvrt', vrt_name] + created_files
                out, err = Popen(cmd, stdout=PIPE, stderr=PIPE).communicate()
            else:
                cmd = ['gdalbuildvrt', vrt_name, os.path.join(self.__data_directory, "terrain_"+str(id_)+"_*.tif")]
                out, err = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True).communicate()
            if err:
                self.__log_manager.error(err)
                self.__project.execute("""delete from project.dem where id ={}""".format(id_))
                return
            return vrt_name

    def altitude(self, point, no_warning=False):
        if not self.has_data():
            if not no_warning:
                self.__log_manager.error("No terrain data in this project")
            return 9999

        mx, my = (point[0], point[1]) if hasattr(point, '__getitem__') else (point.x(), point.y())
        return self.__project.execute("""
            select project.altitude({}, {})
            """.format(mx, my)).fetchone()[0]

    def altitudes(self, segment, width, no_warning=False):
        """returns values clipped by rectangle defined by middle line
        and width
        """
        if not self.has_data():
            if not no_warning:
                self.__log_manager.error("No terrain data in this project")
            return numpy.array([[9999]])
        timer = Timer()
        segment = numpy.array(segment)[:,:2]
        aspect_ratio = width/sqrt(numpy.sum(numpy.square(segment[1,:] - segment[0,:])))
        # move to pixel coordinates
        # x as the heigth direction and y as the width
        for dem, in self.__project.execute("select source from project.dem order by priority desc").fetchall():
            src_ds = gdal.Open(self.__project.unpack_path(dem))
            gt = src_ds.GetGeoTransform()
            start, end = (segment - numpy.array([gt[0], gt[3]]))/numpy.array([gt[1], gt[5]]) # scale + translate
            start, end = numpy.array([start[1], start[0]]), numpy.array([end[1], end[0]]) # rotate 90deg clockwise
            direction = end - start
            length  = sqrt(numpy.sum(numpy.square(direction)))
            direction /= length
            normal = numpy.array([direction[1], -direction[0]])
            corners = numpy.array([start - normal*aspect_ratio*length/2,
                                   start + normal*aspect_ratio*length/2,
                                   end + normal*aspect_ratio*length/2,
                                   end - normal*aspect_ratio*length/2])
            orig = numpy.array([floor(numpy.min(corners[:,0])), floor(numpy.min(corners[:,1]))])
            rang = numpy.array([ceil(numpy.max(corners[:,0]))+1, ceil(numpy.max(corners[:,1]))+1]) - orig
            corners -= orig # translate corners, because we floored origin

            height = src_ds.GetRasterBand(1).ReadAsArray(int(orig[1]), int(orig[0]), int(rang[1]), int(rang[0]))
            if height is not None:
                height = height.astype(numpy.float)
                dx, dy = int(ceil(length*aspect_ratio)), int(ceil(length))
                x, y = numpy.mgrid[:dx, :dy]
                idx = numpy.vstack((x.flatten(), y.flatten())).T
                rot = numpy.array([[normal[0], direction[0]], [normal[1], direction[1]]])
                nearest = (numpy.dot(idx.astype(numpy.float64), rot.T) + corners[0]).astype(numpy.int32)
                result = numpy.zeros((dx, dy))
                result[idx[:,0], idx[:,1]] = height[nearest[:,0], nearest[:,1]]
                return result #, height, corners
        return numpy.array([9999]).reshape(1,1)

    def has_data(self):
        return self.__project.execute("select count(1) from project.dem").fetchone()[0] > 0

    def dems(self):
        "ordered by asc priority, i.e. top is last"
        return [self.__project.unpack_path(f) for f, in self.__project.execute("""
            select source from project.dem order by priority asc""").fetchall()]


