# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
USAGE
   python -m hydra.database.export_calcul_test
"""

from .export_calcul import ExportCalcul
from .export_carto_data import ExportCartoData

from .database import create_model, TestProject, project_exists
import psycopg2
from psycopg2.extras import LoggingConnection
import logging
import tempfile
import sys
import subprocess
import os, re
import getopt
import difflib
import shlex
from subprocess import Popen, PIPE
import shutil
from hydra.project import Project
from ..utility.settings_properties import SettingsProperties

DEBUG = True


try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)
optlist = dict(optlist)
if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)
keep = True if "-k" in optlist or "--keep" in optlist else False


tesdb = "export_calcul_test"
test_project = TestProject(tesdb, keep)

db = SettingsProperties.get_db()
pg = SettingsProperties.get_postgre_settings()
con = psycopg2.connect(database=tesdb, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
cur = con.cursor()
create_model(cur, "model", 2154)
create_model(cur, "mhriv", 2154)
create_model(cur, "mhnet", 2154)
con.commit()
del cur
del con


current_dir = os.path.abspath(os.path.dirname(__file__))
test_data = os.path.join(current_dir, "test_data")


test_output = os.path.join(tempfile.gettempdir(), "export_calcul_test.out")
sql_sript = os.path.join(test_data, 'projettest1_insert.sql')
cmd = shlex.split("psql -d %s -tXA"%(tesdb)) \
        +['-h',db['host'],'-p',str(db['port']),'-U',pg['user'],'-f',sql_sript]

if DEBUG:
    sys.stdout.write(' '.join(cmd)+"\n")
    sys.stdout.flush()

out, err = Popen(cmd, stdout=open(test_output, "w"), stderr=PIPE).communicate()
if err :
    sys.stderr.write(err)
    sys.stderr.write("error while running SQL command from {}\n".format(sql_sript))
    exit(1)

ref_output = os.path.join(test_data, 'projettest1_insert.sql.out')

if open(test_output).read().replace('\n','').replace('\nt','') != open(ref_output).read().replace('\n','').replace('\nt',''):
    sys.stderr.write("diff found between {} and {}\n".format(ref_output, test_output))
    exit(1)
os.remove(test_output)

test_output_dir = os.path.join(tempfile.gettempdir(), 'export_calcul_test')
if os.path.isdir(test_output_dir):
    shutil.rmtree(test_output_dir)
os.mkdir(test_output_dir)

id_scenario=1
obj_project = Project.load_project(tesdb)

# export data for computing (for hydra)
exporter = ExportCalcul(obj_project)
exporter = ExportCalcul(obj_project)
exporter.export(id_scenario)

# export cartographic data (for triangulz.exe)
exporter = ExportCartoData(obj_project)
exporter.export(id_scenario)

scenario_dir = obj_project.get_senario_path(id_scenario)
workdir="travail"    #    "simulation"

#--- compare files - 1st: "scenario.nom"
test_output_work = scenario_dir
test_ref_dir = os.path.join(test_data, 'projettest1_export')
test_output, ref_output =  os.path.join(test_output_work, "scenario.nom"), os.path.join(test_ref_dir, "scenario.nom")
if open(test_output).read().replace('\n','').replace('\nt','') != open(ref_output).read().replace('\n','').replace('\nt',''):
    ref = [line for line in open(ref_output).readlines() if not re.match(r'^(=+ +)+=+$', line)]
    gen = [line for line in open(test_output).readlines() if not re.match(r'^(=+ +)+=+$', line)]
    diff = difflib.unified_diff(ref, gen, fromfile=ref_output, tofile=test_output)
    nodiff = True
    for line in diff:
        nodiff &= False
    if not nodiff:
        sys.stderr.write("found diff {} {}\n".format(ref_output, test_output))
        sys.stderr.write('\n'.join([l for l in diff]))
        exit(1)

#--- compare files - 2nd: other files (subdirectory workdir)
test_output_work = os.path.join(scenario_dir, workdir)

error = False
test_ref_dir = os.path.join(test_data, 'projettest1_export', "simulation")
for outfile in os.listdir(test_ref_dir):
    if outfile[-4:] in ['.li2', '.li1']: continue
    test_output, ref_output =  os.path.join(test_output_work, outfile), os.path.join(test_ref_dir, outfile)
    if open(test_output).read().replace('\n','').replace('\nt','') != open(ref_output).read().replace('\n','').replace('\nt',''):
        ref = [line for line in open(ref_output).readlines() if not re.match(r'^(=+ +)+=+$', line)]
        gen = [line for line in open(test_output).readlines() if not re.match(r'^(=+ +)+=+$', line)]
        diff = difflib.unified_diff(ref, gen, fromfile=ref_output, tofile=test_output)
        nodiff = True
        for line in diff:
            nodiff &= False
        if not nodiff:
            sys.stderr.write("found diff {} {}\n".format(ref_output, test_output))
            sys.stderr.write('\n'.join([l for l in diff]))
            if keep and  os.name != 'posix':
                os.system("pause")
            error = True
if error:
    exit(1)

if keep and os.name != 'posix':
    os.system("pause")
if not keep:
    shutil.rmtree(test_output_dir)

sys.stdout.write(" ok\n")

