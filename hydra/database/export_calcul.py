﻿# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
export database for computation

USAGE

   python -m hydra.database.export_calcul <project> [<scenario>]

   if scenario is not supplied, a default one named scn1 is created
"""

import os
import sys
import re
import json
import numpy
import shutil
import struct
import ctypes
from hydra.utility import string
from hydra.utility.sql_json import instances

class ExportError(Exception):
    pass

class ExportCalcul:

    def __init__(self, project):
        self.__instances = instances
        self.__record_length = {"LI2": 1024, "SECTION.DAT": 256}
        self.__project = project
        self.__node_max = 0
        self.__icount_hydrol = 0
        self.log = project.log

    def __writ_parametres_generaux(self, stream_, res, itype, npt, coment):
        """ writing first line of a bloc (general parameters) """
        numel=len(res) # number of elements
        if (numel>0):
            stream_.write("$ "+coment+"\n")
            if (npt>=0):
                stream_.write("%5d %9d %5d\n" % (itype,numel,npt))
            else:
                stream_.write("%5d %9d \n" % (itype,numel))
        return

    def __link_request(self, nommodel, srequete, stream_, itypehydra, npt, coment):
        """ query the database and obtain the links of the group
            common writings: list of the links of the group  """
        # (ecritures d'export communes à tous les groupes de liaisons)
        res = self.__project.execute(srequete).fetchall()

        if (len(res)>0):
            self.__writ_parametres_generaux(stream_, res, itypehydra, npt, coment)
        return res

    def __writ_one_singularity(self, name, node, i, stream_, streamtmp): # , node_new):
        """ writing node information """
        self.__node_max+=1
        stream_.write("%5d %9d %9d    '%-17s'\n" %(i+1, node, self.__node_max, name))
        streamtmp.write(" %9d %9d\n" %(node, self.__node_max))
        return

    def __writ_tableau(self, rec, stream_, sformat):
        """ writing an array with 2 columns (loi z-s d'un casier, q-z d'une liaison QDZ, ...)
            first the length of this array """
        cleared_rec=[[a,b] for [a,b] in rec if a is not None and b is not None]
        np=len(cleared_rec) # longueur du tableau
        stream_.write("%5d\n" % np)
        for i in range(np):
                stream_.write((sformat+"\n") % (cleared_rec[i][0],cleared_rec[i][1]))
        return

    def __add_array_to_list(self, lst,rec,nmax):
        try:
            lgrec=len(rec)
        except TypeError:
            lgrec=0

        for j in range(lgrec):
            lst.append(rec[j])
        for j in range(nmax-lgrec):
            lst.append(0)
        return
        #@todo à voir: "tu peux concaténer des listes avec l'opérateur +:  en gros ta fonction est équivalente à:  lst += rec + [0]*(nmax-len(rec))

    def __add_transpose_array_to_list(self, lst,r,nmax):
        a=numpy.array(r,numpy.float32)
        b=numpy.transpose(a)
        n1=len(a)
        n2=len(b)
        self.__add_array_to_list(lst,b[0],nmax)
        self.__add_array_to_list(lst,b[1],nmax)
        return

    def __update_node_max(self, res):
        self.__node_max
        n=len(res)
        if (n>0):
            if (res[n-1][0]>self.__node_max): self.__node_max=res[n-1][0]
        return

    # ------------------------------------------------------------------------------------
    # export functions for settings files (fichiers de paramétrages)
    # ------------------------------------------------------------------------------------
    def __exportsettings_gen_fexterne(self, id_param_scen,nomtable,fexterne,fbloc,stream1):
        """ called by exportsettings_gen """
        #  fbloc              fexterne (column)   nomtable (name of table)
        #  --------------     ----------------    -------------------------
        #  *f_hy_externe      external_file       param_external_hydrograph
        #  *f_regul           control_file        param_regulation
        #  *f_mesures         measure_file        param_measure
        res = self.__project.execute("""
            select param_scenario, %s
            from project.%s
            where param_scenario=%d
            """ % (fexterne,nomtable,id_param_scen)).fetchall()

        res_unpacked = [self.__project.unpack_path(f[1]) for f in res]

        if (len(res_unpacked) > 0):
            stream1.write("%s\n" % fbloc)
            for rec in res_unpacked:
                self.log.notice("Exporting external file: {}".format(rec))
                stream1.write("%s\n"%rec)
            stream1.write("\n")
        return

    #@todo à voir: alternative suggérée par VM:
    def __exportsettings_gen_fexterne_bis(self, id_param_scen, nomtable, fexterne, fbloc, stream1):
        count, = self.__project.execute("""
            select count(1)
            from project.%s
            where param_scenario=%d
            """ %(nomtable, id_param_scen)).fetchone()

        return '\n'.join([fbloc]+[fexterne]*count)+'\n' if count else None

    #2.1 - file scenario.nom
    def __exportsettings_scen(self, id_scenario, name_scenario, name_ref_scenario, stream1):
        #--- BLOC SCENARIO
        stream1.write("*scenario\n")
        stream1.write("%s\n\n" % name_scenario)

        comput_mode, output_option, model_connect_settings = self.__project.execute("""
            select comput_mode, output_option, model_connect_settings
            from project.scenario
            where id={}
            """.format(id_scenario)).fetchone()

        id_comput_option, = self.__project.execute("""
            select id from hydra.computation_mode
            where name='{}'
            """.format(comput_mode)).fetchone()

        #--- BLOC SCN_REF
        if name_ref_scenario:
            stream1.write("*scnref\n")
            stream1.write("%s\n\n" % name_ref_scenario)

        #--- BLOC MODELES    ajout  TL 18-11-2017
        stream1.write("*modeles\n")
        lstmodel = self.__project.execute("""select name from project.model""").fetchall()
        for model in lstmodel:
            stream1.write("%s    " % model[0])
        stream1.write("\n\n")

        #--- BLOC COMPUTATION_MODE
        stream1.write("*computation_mode\n")
        stream1.write("%5d\n\n" % id_comput_option)

        #--- BLOC GROUPAGE
        stream1.write("*groupage\n")
        lstmodel = [name for name, in self.__project.execute("""
            select name
            from project.model_config
            order by cascade_order asc
            """).fetchall()]
        if model_connect_settings=="global":
            self.log.notice("Exporting global")
            for model in lstmodel:
                stream1.write("%s    " % model)
            self.log.notice("Models to export: {}".format(', '.join(lstmodel)))
            stream1.write("\n\n")
        elif model_connect_settings=="cascade":
            self.log.notice("Exporting cascade")
            for model in lstmodel:
                stream1.write("%s\n" % model)
            self.log.notice("Models to export: {}".format(', '.join(lstmodel)))
            stream1.write("\n")
        elif model_connect_settings=="mixed":
            self.log.notice("Exporting mixed")
            lstmodel = []
            groups = self.__project.execute("""
                select id, name, ord
                from project.mixed as mixed, project.grp as grp
                where mixed.grp=grp.id
                and mixed.scenario=%i
                order by mixed.ord"""%(id_scenario)).fetchall()

            for group in groups:
                tmpmodel = [name for name, in self.__project.execute("""
                    select name
                    from project.model_config as model
                    where model.id in (
                        select model from project.grp_model
                        where grp = %i)
                    """%(int(group[0]))).fetchall()]
                for model in tmpmodel:
                    lstmodel.append(model)
                    stream1.write("%s    " % model)
                self.log.notice("Models to export: {}".format(', '.join(tmpmodel)))
                stream1.write("\n")
            stream1.write("\n")
        return lstmodel

    #2.2 - file X.cmd
    def __exportsettings_gen(self, id_scenario, name_scenario, stream1, stream_optsor):

        # Query the table "parametrage_scenario" and obtain data as Python objects
        comput_mode, scenario, dry_inflow, rainfall, soil_moisture_coef, runoff_adjust_coef, option_dim_hydrol_network, date0, tfin_hr, tini_hydrau_hr, dtmin_hydrau_hr, dtmax_hydrau_hr, dzmin_hydrau, dzmax_hydrau, flag_save, tsave_hr, flag_rstart, scenario_rstart, trstart_hr, flag_hydrology_rstart, scenario_hydrology_rstart, graphic_control, model_connect_settings, tbegin_output_hr, tend_output_hr, dt_output_hr, output_option, transport_type, quality_type, dt_hydrol_mn = self.__project.execute("""
            select comput_mode, scenario, dry_inflow, rainfall, soil_moisture_coef,
            runoff_adjust_coef, option_dim_hydrol_network, date0,
            tfin_hr, tini_hydrau_hr, dtmin_hydrau_hr, dtmax_hydrau_hr,
            dzmin_hydrau, dzmax_hydrau, flag_save, tsave_hr, flag_rstart,
            scenario_rstart, trstart_hr, flag_hydrology_rstart,
            scenario_hydrology_rstart, graphic_control, model_connect_settings,
            tbegin_output_hr, tend_output_hr, dt_output_hr,
            output_option, transport_type, quality_type,
            dt_hydrol_mn
            from project.scenario
            where id={} """.format(id_scenario)).fetchone()

        #--- BLOC TIME
        self.log.notice("Exporting settings: *time")
        stream1.write("*time\n")
        stream1.write("%s\n" % date0.strftime("%Y %m %d %H %M"))   # date0_hydrau,
        stream1.write("%12.3f\n" % (tfin_hr))                           # tfin_hydrau_hours
        # tbegin_ouput_hr,tend_output_hr,dt_output_hr
        tdeb=0.
        if (tbegin_output_hr!=None): tdeb=tbegin_output_hr
        tfin=tfin_hr
        if (tend_output_hr!=None): tfin=tend_output_hr
        dt=0.
        if (dt_output_hr!=None): dt=dt_output_hr
        stream1.write("%12.3f %12.3f% 12.3f\n\n" %(tdeb,tfin,dt))

        #--- BLOC HYDROL
        if (rainfall is None): rainfall=0
        if (dry_inflow is None): dry_inflow=0  #TL

        if not comput_mode=="hydraulics":
            self.log.notice("Exporting settings: *hydrol")
            stream1.write("*hydrol\n")
            stream1.write("%5d%5d\n"          % (dry_inflow,rainfall))  #TL
            stream1.write("%12.4f\n"          % (dt_hydrol_mn))
            stream1.write("%12.4f %12.4f\n"   % (soil_moisture_coef,runoff_adjust_coef))       # coef_humidite_sol,modulation_coef_ruissel
            i=0
            if (option_dim_hydrol_network): i=1
            stream1.write("%5d\n\n"           %  i)                          #  option_dim_reseau_hydrol

        #--- BLOC HYDRAU
        self.log.notice("Exporting settings: *hydrau")
        stream1.write("*hydrau\n")
        stream1.write("%12.3f\n"          % (tini_hydrau_hr))
        stream1.write("%12.6f %12.6f\n"   % (dtmin_hydrau_hr,dtmax_hydrau_hr))
        stream1.write("%12.4f %12.4f\n\n" % (dzmin_hydrau,dzmax_hydrau))

        #--- BLOC SAVE
        if (flag_save):
            self.log.notice("Exporting settings: *save")
            stream1.write("*save\n %5d\n %12.4f\n\n" % (1,tsave_hr))

        #--- BLOC DESSIN_CALCUL
        if (graphic_control):
            self.log.notice("Exporting settings: *dessin_calcul")
            stream1.write("*dessin_calcul\n %5d\n\n" % 1)              # option_dessin_calcull

        #--- BLOC RSTART
        if (flag_rstart):
            res2 = self.__project.execute("""
                select id,name
                from project.scenario
                where id=%d
                """%(scenario_rstart)).fetchall()

            self.log.notice("Exporting settings: *rstart")
            stream1.write("*rstart\n")
            stream1.write("%5d\n" % 1)
            stream1.write("%s\n %12.4f\n\n" % (res2[0][1],trstart_hr))      # scenario_rstart,trsart_hours

        #--- BLOC HYDROL RSTART
        if (flag_hydrology_rstart):
            res2 = self.__project.execute("""
                select id,name
                from project.scenario
                where id=%d
                """%(scenario_hydrology_rstart)).fetchall()

            self.log.notice("Exporting settings: *hydrol_rstart")
            stream1.write("*hydrol_rstart\n")
            stream1.write("%5d\n" % 1)
            stream1.write("%s\n\n" % (res2[0][1]))      # scenario_rstart,trsart_hours

        #--- BLOCS F_HY_EXTERNE, F_REGUL, F_MESURE        name of table  , name of column ,  name of bloc
        self.__exportsettings_gen_fexterne(id_scenario,"param_external_hydrograph", "external_file", "*f_hy_externe",stream1)
        self.__exportsettings_gen_fexterne(id_scenario,"param_regulation"         , "control_file" , "*f_regul"     ,stream1)
        self.__exportsettings_gen_fexterne(id_scenario,"param_measure"            , "measure_file" , "*f_mesures"   ,stream1)

        #--- BLOC DEROUTAGE_APPORT
        # Query the table "deroutage_apport"
        #                              0     1          2           3           4      5
        res2 = self.__project.execute("""
            select model, hydrograph_from, hydrograph_to, option, parameter
            from project.inflow_rerouting
            where scenario={}
            """.format(id_scenario)).fetchall()

        self.log.notice("Exporting settings: *deroutage_apport")
        stream1.write("*deroutage_apport\n")
        for rec in res2:
            name_from = self.__project.execute("""select name from {}.hydrograph_bc_singularity where id={}""".format(rec[0], rec[1])).fetchone()[0]
            name_to = self.__project.execute("""select name from {}.hydrograph_bc_singularity where id={}""".format(rec[0], rec[2])).fetchone()[0] if rec[2] is not None else 'null'
            stream1.write("%20s %20s %20s %i %10.4f\n" % (rec[0],name_from,name_to,self.__instances['rerouting_option'][rec[3]]['id'],rec[4]))
        stream1.write("\n")


        #--- BLOC OPTION_POLLUTION
        if (transport_type=="pollution"):
            #                    0  1       2         3         4         5
            res2 = self.__project.execute("""
                select id, id, iflag_mes, iflag_dbo5, iflag_dco, iflag_ntk
                from project.scenario
                where id=%d
                """%(id_scenario)).fetchall()

            if (len(res2)>0):
                self.log.notice("Exporting settings: *option_pollution")
                stream1.write("*option_pollution\n")
                nparam=0
                for i in range(4):
                    if (res2[0][2+i]): nparam+=1
                stream1.write("%5d\n" % nparam)
                if (res2[0][2]): stream1.write("MES\n")
                if (res2[0][3]): stream1.write("DBO5\n")
                if (res2[0][4]): stream1.write("DCO\n")
                if (res2[0][5]): stream1.write("NTK\n")
                stream1.write("\n")


        #--- BLOC OPTION_QUALITY1/2
        elif (transport_type=="quality"):
            #           0  1    2       3          4               5                       6
            res2 = self.__project.execute("""
                select id, id, id, diffusion, dispersion_coef, longit_diffusion_coef, lateral_diff_coef
                from project.scenario
                where id=%d
                """%(id_scenario)).fetchall()

            if (len(res2)>0):
                if (quality_type=="physico_chemical"):   # class=1 : fixed parameters: DBO5 NH4 NO3 O2
                    self.log.notice("Exporting settings: *option_quality1")
                    stream1.write("*option_quality1\n")
                    #           0       1                 2          3                4                   5             6            7        8
                    res3 = self.__project.execute("""
                        select id, water_temperature_c, min_o2_mgl, kdbo5_j_minus_1, koxygen_j_minus_1, knr_j_minus_1, kn_j_minus_1, bdf_mgl, o2sat_mgl
                        from project.scenario
                        where id=%d
                        """%(id_scenario)).fetchall()

                    stream1.write("%10.2f %9.3g %9.3g %9.3g %9.3g %9.3g %9.3g %9.3g\n" % (res3[0][1],res3[0][2],res3[0][3],res3[0][4],res3[0][5],res3[0][6],res3[0][7],res3[0][8]))  #
                    stream1.write("%12d %12.3g %12.3g %12.3g\n\n"     % (res2[0][3],res2[0][4],res2[0][5],res2[0][6]))  #  diffusion,coef_dispersion coef_diffusiv_long,coef_diffusiv_lat

                elif (quality_type=="quality_parameter_selection"): # class=2
                    name_param=["unused","EI","EC","Ad3","Ad4"]
                    self.log.notice("Exporting settings: *option_quality2")
                    stream1.write("*option_quality2\n")
                    #           0       1         2         3         4             5                           6                            7                      8
                    res3 = self.__project.execute("""
                        select id, iflag_ad1, iflag_ad2, iflag_ad3, iflag_ad4, ad1_decay_rate_jminus_one, ad2_decay_rate_jminus_one,ad3_decay_rate_jminus_one,ad4_decay_rate_jminus_one
                        from project.scenario
                        where id=%d
                        """ % (id_scenario)).fetchall()

                    nparam=0
                    for i in range(4):
                        if (res3[0][i+1]): nparam+=1
                    stream1.write("%5d\n" % nparam)

                    for i in range(4):
                        if (res3[0][i+1]): stream1.write("%-8s  %9.3g\n" %(name_param[i+1],res3[0][5+i]))  #@todo: paramètre "decay_rate" à mettre en place pour chaque paramètre
                    stream1.write("%12d %12.3g %12.3g %12.3g\n\n"     % (res2[0][3],res2[0][4],res2[0][5],res2[0][6]))  #  diffusion,coef_dispersion coef_diffusiv_long,coef_diffusiv_lat

                elif (quality_type=="suspended sediment transport"): # class=3
                    self.log.notice("Exporting settings: *option_quality3")
                    stream1.write("*option_quality3\n")
                    #@todo: les 2 lignes ci-dessous sont en attente de l'implémentation de la table param_quality_class3
                    # id,sst = self.__project.execute("""select id, sst    from project.param_quality_class2   where id=%d""" % (res2[0][8])).fetchall()
                    # stream1.write("%12.3g\n\n" % (sst))


        #--- BLOC OPTION_SEDIMENT
        elif (transport_type=="sediment"):
            sediment_file = self.__project.execute("""
                select sediment_file
                from  project.scenario
                where id=%d
                """%(id_scenario)).fetchone()

            self.log.notice("Exporting settings: *option_sediment")
            stream1.write("*option_sediment\n%s\n\n" % (self.__project.unpack_path(sediment_file[0])))

        #--- File X_OPTSOR.DAT
        self.__exportsettings_optsor(id_scenario, stream_optsor)

        return graphic_control

    #2.3
    def __exportsettings_optsor(self, id_scenario, stream2):
        option_file, option_file_path, c_affin_param, t_affin_start_hr, t_affin_min_surf_ddomains, \
            output_option, strickler_param = self.__project.execute("""
            select option_file, option_file_path, c_affin_param, t_affin_start_hr, t_affin_min_surf_ddomains,
            output_option, strickler_param
            from project.scenario
            where id={} limit 1;""".format(id_scenario)).fetchone()
        if option_file:
            self.log.notice("Exporting settings: *external file")
            with open(self.__project.unpack_path(option_file_path)) as f:
                lines = f.readlines()
                stream2.writelines(lines)
        if c_affin_param:
            #c affin
            self.log.notice("Exporting settings: *c_affin")
            stream2.write("\n*c_affin\n")
            stream2.write("%12.6f %12.6f\n" % (t_affin_start_hr, t_affin_min_surf_ddomains))
            item_list = self.__project.execute("""
                select model, element
                from project.c_affin_param
                where scenario={} order by model, type_domain, element;""".format(id_scenario)).fetchall()

            for item in item_list:
                stream2.write("%s %s\n" % (str(item[0]), str(item[1])))

        if strickler_param:
            #strickl
            self.log.notice("Exporting settings: *strickl")
            stream2.write("\n*strickl\n")
            item_list = self.__project.execute("""
                select model, element, rkmin, rkmaj, pk1, pk2
                from project.strickler_param
                where scenario={} order by model, element;""".format(id_scenario)).fetchall()

            for item in item_list:
                stream2.write("%s %s %12.2f %12.2f %12.4f %12.4f\n" %(str(item[0]), str(item[1]), item[2], item[3], item[4], item[5]))

        if output_option:
            #sor1
            self.log.notice("Exporting settings: *sor1")
            stream2.write("\n*sor1\n")
            item_list = self.__project.execute("""
                select model, element
                from project.output_option
                where scenario={} order by model, element;""".format(id_scenario)).fetchall()

            for item in item_list:
                stream2.write("%s %s \n" %(str(item[0]), str(item[1]) )  )

        stream2.close()
        return

    #2.4
    def __exportsettings_dry_inflow(self, id_scenario, stream):
        id_dry_scenario, =   self.__project.execute("""select dry_inflow from project.scenario
           where id={}""".format(id_scenario)).fetchone()

        if id_dry_scenario is None:
            return

        sectors = self.__project.execute("""
            select dis.id, dis.name, settings.vol_curve, settings.volume_sewage_m3day,
            settings.coef_volume_sewage_m3day, settings.volume_clear_water_m3day, settings.coef_volume_clear_water_m3day
            from project.dry_inflow_sector as dis
            left join project.dry_inflow_scenario_sector_setting as settings on dis.id = settings.sector
            and settings.dry_inflow_scenario={}
            order by dis.name;
            """.format(id_dry_scenario)).fetchall()

        nsectors = len(sectors)
        stream.write("%i\n"%(nsectors))
        for i in range(0, nsectors):
            self.log.notice("Exporting dry inflow sector: {}".format(sectors[i][1]))
            stream.write("\n%s %F %F %F %F\n"%(sectors[i][1],
            string.get_sql_forced_float(sectors[i][3]), string.get_sql_forced_float(sectors[i][4]),
            string.get_sql_forced_float(sectors[i][5]), string.get_sql_forced_float(sectors[i][6])))

            id_modulation = sectors[i][2]
            if id_modulation is None:
                stream.write("0\n")
            else:
                modulations_array,= self.__project.execute("""
                    select hv_array from project.dry_inflow_hourly_modulation where id={};
                    """.format(id_modulation)).fetchone()
                nmodul = len (modulations_array)
                stream.write("%i\n"%(nmodul))
                for j in range(0, nmodul):
                    stream.write("%F %F\n"%(string.get_sql_forced_float(modulations_array[j][0]),
                        string.get_sql_forced_float(modulations_array[j][1])))
        return

    #2.5
    def __exportsettings_rainfall_library(self, id_scenario, stream):
        id_rainfall, =  self.__project.execute("""select rainfall from project.scenario
            where id={};""".format(id_scenario)).fetchone()

        if id_rainfall is None:
            return

        type_rainfall, = self.__project.execute("""select rainfall_type from project._rainfall
            where id={};""".format(id_rainfall)).fetchone()

        if type_rainfall=="caquot":
            self.log.notice("Exporting rainfall - caquot: {}".format(id_rainfall))
            id_montana, = self.__project.execute("""select montana from project.caquot_rainfall
                where id={}""".format(id_rainfall)).fetchone()
            if id_montana is None:
                raise ExportError("Selected caquot rainfall has no montana coefficient")

            a_montana, b_montana = self.__project.execute("""select coef_a, coef_b from project.coef_montana
                where id={};""".format(id_montana)).fetchone()

            stream.write("CAQ\n")
            stream.write("%F %F\n"%(string.get_sql_forced_float(a_montana), string.get_sql_forced_float(b_montana)))
        elif type_rainfall=="single_triangular":
            self.log.notice("Exporting rainfall - single triangular: {}".format(id_rainfall))
            id_montana, total_duration_mn, peak_time_mn = self.__project.execute("""
                select montana, total_duration_mn, peak_time_mn
                from project.single_triangular_rainfall
                where id={};""".format(id_rainfall)).fetchone()
            if id_montana is None:
                raise ExportError("Selected single triangular rainfall has no montana coefficient")

            a_montana, b_montana = self.__project.execute("""select coef_a, coef_b from project.coef_montana
                where id={};""".format(id_montana)).fetchone()

            stream.write("PPST\n")
            stream.write("0 0 10e10\n")   #@todo : capture et export de l'épicentre et du rayon d'action de la pluie
            stream.write("%F %F\n"%(string.get_sql_forced_float(a_montana), string.get_sql_forced_float(b_montana)))
            stream.write("%F %F\n"%(string.get_sql_forced_float(total_duration_mn), string.get_sql_forced_float(peak_time_mn)))
        elif type_rainfall=="double_triangular":
            self.log.notice("Exporting rainfall - double triangular: {}".format(id_rainfall))
            montana_total, montana_peak, total_duration_mn, peak_duration_mn, peak_time_mn = self.__project.execute("""
                select montana_total, montana_peak, total_duration_mn, peak_duration_mn, peak_time_mn
                from project.double_triangular_rainfall
                where id={};""".format(id_rainfall)).fetchone()
            if montana_total is None:
                raise ExportError("Selected double triangular rainfall has no montana_total coefficient")
            if montana_peak is None:
                raise ExportError("Selected double triangular rainfall has no montana_peak coefficient")

            a_montana_tot, b_montana_tot = self.__project.execute("""select coef_a, coef_b from project.coef_montana
                where id={};""".format(montana_total)).fetchone()
            a_montana_pk, b_montana_pk = self.__project.execute("""select coef_a, coef_b from project.coef_montana
                where id={};""".format(montana_peak)).fetchone()

            stream.write("PPDT\n")
            stream.write("0 0 10e10\n")   #@todo : capture et export de l'épicentre et du rayon d'action de la pluie
            stream.write("%F %F\n"%(string.get_sql_forced_float(a_montana_tot), string.get_sql_forced_float(b_montana_tot)))
            stream.write("%F %F\n"%(string.get_sql_forced_float(a_montana_pk), string.get_sql_forced_float(b_montana_pk)))
            stream.write("%F %F %F\n"%(string.get_sql_forced_float(total_duration_mn),
                string.get_sql_forced_float(peak_duration_mn), string.get_sql_forced_float(peak_time_mn)))
        elif type_rainfall=="intensity_curve":
            self.log.notice("Exporting rainfall - intensity curve: {}".format(id_rainfall))
            t_mn_intensity_mmh, = self.__project.execute("""
                select t_mn_intensity_mmh from project.intensity_curve_rainfall where id={};
                """.format(id_rainfall)).fetchone()
            stream.write("PPHY\n")
            stream.write("0 0 10e10\n")   #@todo : capture et export de l'épicentre et du rayon d'action de la pluie
            n = len(t_mn_intensity_mmh)
            stream.write("%i\n"%(n))
            for row in range(0, n):
                stream.write("%F %F\n"%(string.get_sql_forced_float(t_mn_intensity_mmh[row][0]),
                    string.get_sql_forced_float(t_mn_intensity_mmh[row][1])))
        elif type_rainfall=="gage":
            self.log.notice("Exporting rainfall - gage: {}".format(id_rainfall))
            interpolation, = self.__project.execute("""
                select interpolation from project.gage_rainfall where id={};
                """.format(id_rainfall)).fetchone()

            rain_gages = self.__project.execute("""select rg.id, st_x(rg.geom) as x,st_y(rg.geom) as y, grd.t_mn_hcum_mm
                from project.rain_gage as rg
                left join project.gage_rainfall_data as grd on rg.id = grd.rain_gage
                where grd.rainfall={}
                and grd.is_active=true
                order by rg.name;""".format(id_rainfall)
                ).fetchall()
            n_rg = len(rain_gages)
            mode_interp = 1 if interpolation=="distance_ponderation" else 0

            stream.write("PR\n")
            stream.write("%i %i\n"%(n_rg, mode_interp))
            for i in range(0, n_rg):
                datas = rain_gages[i][3]
                npoints = len(datas)
                stream.write("%F %F %i\n"%(float(rain_gages[i][1]), float(rain_gages[i][2]), npoints))
                for j in range(0, npoints):
                    stream.write("%F %F\n"%(float(datas[j][0]), float(datas[j][1])))
        elif type_rainfall=="radar":
            self.log.notice("Exporting rainfall - radar: {}".format(id_rainfall))
            rain_folder = os.path.join(self.__project.get_project_dir(), 'rain')
            rain = self.__project.execute("""
                select id, name from project.radar_rainfall where id={}
                """.format(id_rainfall)).fetchone()

            stream.write("PRAD\n")
            for model in self.model_to_export:
                file_name = rain[1]+'_'+model+'.rad'
                if os.path.exists(os.path.join(rain_folder, file_name)):
                    stream.write("%s %s\n"%(model, file_name))
                else:
                    raise ExportError("Selected radar rainfall has not been generated (missing .rad files in /rain folder)")

        return


    #2.7
    def __export_model_connections(self, namemodel, stream1):
        res = self.__project.execute("""
            select id, name, node, cascade_mode, zq_array
            from %s.model_connect_bc_singularity
            order by id
            """%(namemodel)).fetchall()
        for id, name, node, cascade_mode, zq_array in res:
            self.log.notice("Exporting model connection: {}".format(name))
            stream1.write("%s   %s  %d  %d   " % (namemodel, name, node, self.__instances['model_connect_mode'][cascade_mode]['id']))
            if (cascade_mode=="zq_downstream_condition"): n=min(len(zq_array),10)
            else: n=0
            for i in range(n): stream1.write(" %F " % zq_array[i][0])
            for i in range(10-n): stream1.write(" %F " % 0.)  # pad with 0 up to 10
            for i in range(n): stream1.write(" %F " % zq_array[i][1])
            for i in range(10-n): stream1.write(" %F " % 0.)  # pad with 0 up to 10
            stream1.write("\n")



    # ------------------------------------------------------------------------------------
    # export functions for hydrologic comptuations (files .LI1  .LI2)
    #   file .LI1 : ascii. each line includes: i (rang de l'objet) nom itype_hydrol node_amont node_aval.
    #   file .LI2 : binary. each record is 1024 bytes long and includes up to 256 parameters (integer or real, size=4 bytes),
    #                depending of the type of object (bassin_versant, hydrology_routage, etc ...).
    #               if there are less than 256 parameters, the record must be completed with zeros.
    # ------------------------------------------------------------------------------------

    #3.2.1
    def __exporthydrol_manhole(self, nommodel, stream1, stream2):
        itypehydra=self.__instances["node_type"]["manhole_hydrology"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        n4byte=lgrecord/4 # number of 4-bytes words in LI2-record
        # Query the database and obtain data as Python objects
        #                    0  1     2    3
        res = self.__project.execute("""
            select id, name, area, z_ground
            from %s.manhole_hydrology_node
            order by id
            """%(nommodel)).fetchall()

        self.__update_node_max(res)
        # format for ascii file LI1
        sformat1="%10d   %-17s %4d %9d %9d\n"
        srecord=ctypes.create_string_buffer(lgrecord)

        for i, rec in enumerate(res):
            self.log.notice("Exporting hydrology manhole: {}".format(rec[1]))
            self.__icount_hydrol += 1
            # writing in LI1 ascii file
            stream1.write(sformat1 % (self.__icount_hydrol,rec[1],itypehydra,rec[0],0))

            # writing in LI2 binary file
            start = stream2.tell()
            stream2.write(struct.pack('ff', rec[2], rec[3] ))    # area, z_ground
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return

    #3.2.2
    def __exporthydrol_catchment(self, nommodel, stream1, stream2):
        itype_hydrol = self.__instances["node_type"]["catchment"]["group_hydra_no"]
        lgrecord = self.__record_length["LI2"]
        n4byte = lgrecord/4 # number of 4-bytes words in LI2-record

        # Query the database and obtain data as Python objects
        res = self.__project.execute("""
            select n.id, n.name, area_ha, rl, slope, c_imp, netflow_type,
            constant_runoff, horner_ini_loss_coef, horner_recharge_coef,
            holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm,
            scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, permeable_soil_j_mm,
            permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange,
            runoff_type, socose_tc_mn, socose_shape_param_beta, define_k_mn, q_limit, q0,
            catchment_pollution_mode, catchment_pollution_param_json->catchment_pollution_mode::varchar, ST_X(n.geom), ST_Y(n.geom), c.name
            from {m}.catchment_node as n left join {m}.catchment as c on n.contour = c.id
            order by n.id
            """.format(m=nommodel)).fetchall()
        self.__update_node_max(res)

        for i, (id_, name, area_ha, rl, slope, c_imp,
                netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef,
                holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm,
                scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, permeable_soil_j_mm,
                permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange,
                runoff_type, socose_tc_mn, socose_shape_param_beta, define_k_mn, q_limit, q0, mode_pol, pol_param, xcord, ycord, cbv) \
                in enumerate(res):
            self.log.notice("Exporting hydrology catchment: {}".format(name))
            self.__icount_hydrol += 1
            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n"%(self.__icount_hydrol, name, itype_hydrol, id_, 0))

            if netflow_type=='constant_runoff':
                np=[constant_runoff, 0, 0, 0, 0]
            elif netflow_type=='horner':
                np=[horner_ini_loss_coef, horner_recharge_coef, 0, 0, 0]
            elif netflow_type=='holtan':
                np=[holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm, 0, 0]
            elif netflow_type=='scs':
                np=[scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm,  0, 0]
            elif netflow_type=='permeable_soil':
                np=[ permeable_soil_j_mm, permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange]

            if runoff_type=='socose':
                rp=[socose_tc_mn, socose_shape_param_beta, 0]
            elif runoff_type=='define_k':
                rp=[define_k_mn, 0, 0]
            elif runoff_type in ['k_desbordes', 'passini', 'giandotti']:
                rp=[0, 0, 0]

            # prepare quality parameters for writing in .li2
            if (mode_pol is None):
                mode_pol_for_hydra = 0
                pp = [0]
                pp += [0.]*8 # pad with zeros
            elif (mode_pol=="variable_concentration"):
                mode_pol_for_hydra = self.__instances['catchment_pollution_mode'][mode_pol]['id']
                # network_type : type enumeration expected. conversion into integer
                try:
                    m = re.match(r'enum\((.*)\)', self.__instances['catchment_pollution_mode'][mode_pol]['params']['network_type']['type'])
                    enum_map = {v: i+1 for i, v in enumerate(m.group(1).replace("'","").split(', '))}
                except AttributeError:
                    self.log.error("Unexpected case. AttributeError on:"
                        +"n:"+str(0)
                        +"param[n]:"+str(param[0])
                        +"type(param[n]):"+type(param[0])
                        +str(self.__instances['catchment_pollution_mode'][mode_pol]['params'][0]['type']))
                    return
                pp = [enum_map[pol_param['network_type']]]
                for v in pol_param["percent_mos"]: pp += [v]
                pp += [0.]*4 # pad with zeros
            elif (mode_pol=="fixed_concentration"):
                mode_pol_for_hydra = self.__instances['catchment_pollution_mode'][mode_pol]['id']
                pp = [0]
                pp += [0.]*4 # pad with zeros
                pp += [float(pol_param[n]) for n in sorted(self.__instances['catchment_pollution_mode'][mode_pol]['params'].keys(), key=lambda k: self.__instances['catchment_pollution_mode'][mode_pol]['params'][k]['place'])]

            cbv = '' if cbv is None else cbv

            # A record inside the li2 file contains the following parameters :
            # {area_ha rl slope c_imp netflow_type netflow_parm(3) runoff_type runoff_param(3) qlimi q0 mod_pol network_type percent_mos(4) concentration(4) xccord  ycoord cbv}
            # if part or all of mode_pol network_type percent_mos(4) concentration(4) are undefined, value 0 is written instead
            # the record is completed with "0" up to 1024 bytes.
            start = stream2.tell()

            # writing in LI2 binary file
            #                                          x                 x = offset of catchment_pollution_mode
            stream2.write(struct.pack('ffffifffffifffffiiffffffffff24s',
                area_ha, rl, slope, c_imp,
                self.__instances['netflow_type'][netflow_type]['id_hydra_export'],
                np[0], np[1], np[2], np[3], np[4],
                self.__instances['runoff_type'][runoff_type]['id_hydra_export'],
                rp[0], rp[1], rp[2],
                q_limit, q0,
                mode_pol_for_hydra, pp[0], pp[1], pp[2], pp[3], pp[4], pp[5], pp[6], pp[7], pp[8],
                xcord, ycord, str(cbv)
                ))

            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return

    #3.2.3
    def __exporthydrol_routage(self, nommodel, stream1, stream2):
        itypehydra=self.__instances["link_type"]["routing_hydrology"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        n4byte=lgrecord/4

        # Query the database and obtain data as Python objects
        res = self.__project.execute("""
            select id, name, up, down, cross_section, length, slope
            from %s.routing_hydrology_link
            order by id
            """%(nommodel)).fetchall()

        for i, (id, name, up, down, cross_section, length, slope) in enumerate(res):
            self.log.notice("Exporting hydrology routing: {}".format(name))
            self.__icount_hydrol += 1
            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,up,down))

            # writing in LI2 binary file
            start = stream2.tell()
            stream2.write(struct.pack('fff', cross_section, length, slope ))
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return

    #3.2.4
    def __exporthydrol_translation(self, nommodel, stream1, stream2):
        itypehydra=self.__instances["link_type"]["connector_hydrology"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        n4byte=lgrecord/4
        # Query the database and obtain data as Python objects
        #                   0   1    2  3   4
        res = self.__project.execute("""
            select id, name, id, up, down
            from %s.connector_hydrology_link
            order by id
            """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            self.log.notice("Exporting hydrology connector: {}".format(rec[1]))
            self.__icount_hydrol += 1
            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,rec[1],itypehydra,rec[3],rec[4]))

            # writing in LI2 binary file
            stream2.write(bytearray(lgrecord)) # pad with zeros
        return

    #3.2.5
    def __exporthydrol_qqsplit(self, nommodel, stream1, stream2):
        itypehydra=self.__instances["singularity_type"]["qq_split_hydrology"]["group_hydra_no"] #120
        lgrecord=self.__record_length["LI2"]
        res = self.__project.execute("""
            select id, name, node, qq_array, downstream, split1, split2
            from %s.qq_split_hydrology_singularity
            order by id
            """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            id,name,node,qq_array,downstream,split1,split2 = rec
            self.log.notice("Exporting hydrology qq split: {}".format(name))
            self.__icount_hydrol += 1

            downstream = self.__project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,downstream)).fetchone()[0]
            if (split1!=None): split1 = self.__project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,split1)).fetchone()[0]
            if (split2!=None and split2!=0): split2 = self.__project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,split2)).fetchone()[0]

            if (split2==None): split2=0

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            lg=len(qq_array)
            p = []
            for q in qq_array: p+= [q[0]]
            p += [0.]*(10-lg) # pad with zeros
            for q in qq_array: p+= [q[1]]
            p += [0.]*(10-lg) # pad with zeros
            if (len(qq_array[0]))==3:
                for q in qq_array: p+= [q[2]]
                p += [0.]*(10-lg) # pad with zeros

            start = stream2.tell()
            stream2.write(struct.pack('iii', downstream, split1, split2))
            for p1 in p: stream2.write(struct.pack('f', p1))
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros

        return

    def __convert_to_array(self, mystring):
        return [[float(v) for v in level1.split(',')] for level1 in mystring.replace(' ', '').replace('{{', '').replace('}}', '').split('},{') ]



    #3.2.6
    def __exporthydrol_zqsplit(self, nommodel, stream1, stream2):
        #itypehydra=21
        itypehydra=self.__instances["singularity_type"]["zq_split_hydrology"]["group_hydra_no"]

        lgrecord=self.__record_length["LI2"]
        res = self.__project.execute("""
            select id, name, node, downstream, split1, split2, downstream_law, downstream_param_json->downstream_law::varchar, split1_law, split1_param_json->split1_law::varchar, split2_law, split2_param_json->split2_law::varchar
            from %s.zq_split_hydrology_singularity
            order by id
            """%(nommodel)).fetchall()

        for i, (id_, name, node, downstream, split1, split2,  downstream_law,
                downstream_param, split1_law, split1_param, split2_law, split2_param
                ) in enumerate(res):
            self.log.notice("Exporting hydrology zq split: {}".format(name))
            self.__icount_hydrol += 1

            downstream = self.__project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,downstream)).fetchone()[0]
            if (split1!=None): split1 = self.__project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,split1)).fetchone()[0]
            if (split2!=None): split2 = self.__project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,split2)).fetchone()[0]

            if (split2==None): split2=0

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            p = []
            for j, (law, param) in enumerate([(downstream_law, downstream_param), (split1_law, split1_param), (split2_law, split2_param)]):
                if (law=="zq"):
                    for v in param["zq_array"]: p += [v[0]]
                    p += [0.]*(10-len(p)) # pad with zeros
                    for v in param["zq_array"]: p += [v[1]]
                    p += [0.]*(20-len(p)) # pad with zeros
                elif law is not None:
                    p += [float(param[n]) for n in sorted(self.__instances['split_law_type'][law]['params'].keys(), key=lambda k: self.__instances['split_law_type'][law]['params'][k]['place'])]
                    p += [0.]*((j+1)*20-len(p)) # pad with zeros
                else:
                    p += [0.]*((j+1)*20-len(p)) # pad with zeros

            start = stream2.tell()
            stream2.write(struct.pack('iiiiii', downstream, split1, split2 or 0, self.__instances['split_law_type'][downstream_law]["id"], self.__instances['split_law_type'][split1_law]["id"], self.__instances['split_law_type'][split2_law]["id"] if split2_law else 0  ))
            for n in range(60): stream2.write(struct.pack('f', p[n]))
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros

        return

    #3.2.7
    def __exporthydrol_rs(self, nommodel, stream1, stream2):
        #itypehydra=22
        itypehydra=self.__instances["singularity_type"]["reservoir_rs_hydrology"]["group_hydra_no"]

        lgrecord=self.__record_length["LI2"]
        res = self.__project.execute("""
            select id, name, node, drainage, overflow, q_drainage, z_ini, zs_array, treatment_mode, treatment_param_json->treatment_mode::varchar
            from %s.reservoir_rs_hydrology_singularity
            order by id
            """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            id, name, node, drainage, overflow, q_drainage, z_ini, zs_array, treatment_mode, treatment_param = rec
            self.log.notice("Exporting hydrology rs reservoir: {}".format(name))
            self.__icount_hydrol += 1

            drainage = self.__project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,drainage)).fetchone()[0]
            overflow = self.__project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,overflow)).fetchone()[0]

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            start = stream2.tell()
            stream2.write(struct.pack('iiff', drainage, overflow, q_drainage, z_ini ))

            p = []
            for zs in zs_array: p+= [zs[0]]
            p += [0.]*(10-len(zs_array)) # pad with zeros
            for zs in zs_array: p+= [zs[1]]
            p += [0.]*(10-len(zs_array)) # pad with zeros
            for p1 in p: stream2.write(struct.pack('f', p1))

            p = [float(treatment_param[n]) for n in sorted(self.__instances['pollution_treatment_mode'][treatment_mode]['params'].keys(), key=lambda k: self.__instances['pollution_treatment_mode'][treatment_mode]['params'][k]['place'])]
            stream2.write(struct.pack('i', self.__instances['pollution_treatment_mode'][treatment_mode]["id"]  ))
            for p1 in p: stream2.write(struct.pack('f', p1))

            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros

        return


    #3.2.8
    def __exporthydrol_rsp(self, nommodel, stream1, stream2):
        #itypehydra=23
        itypehydra=self.__instances["singularity_type"]["reservoir_rsp_hydrology"]["group_hydra_no"]

        lgrecord=self.__record_length["LI2"]
        res = self.__project.execute("""
            select id, name, node, drainage, overflow, z_ini, zr_sr_qf_qs_array, treatment_mode, treatment_param_json->treatment_mode::varchar
            from %s.reservoir_rsp_hydrology_singularity
            order by id
            """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            id, name, node, drainage, overflow, z_ini, zr_sr_qf_qs_array, treatment_mode, treatment_param = rec
            self.log.notice("Exporting hydrology rsp reservoir: {}".format(name))
            self.__icount_hydrol += 1

            drainage = self.__project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,drainage)).fetchone()[0]
            overflow = self.__project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,overflow)).fetchone()[0]

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            start = stream2.tell()
            stream2.write(struct.pack('iif', drainage, overflow, z_ini ))

            p = []
            for j in range(4):
                for zs in zr_sr_qf_qs_array: p+= [zs[j]]
                p += [0.]*(10-len(zr_sr_qf_qs_array)) # pad with zeros
            for p1 in p: stream2.write(struct.pack('f', p1))

            p = [float(treatment_param[n]) for n in sorted(self.__instances['pollution_treatment_mode'][treatment_mode]['params'].keys(), key=lambda k: self.__instances['pollution_treatment_mode'][treatment_mode]['params'][k]['place'])]
            stream2.write(struct.pack('i', self.__instances['pollution_treatment_mode'][treatment_mode]["id"]  ))
            for p1 in p: stream2.write(struct.pack('f', p1))

            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return


    #3.2.9
    def __exporthydrol_bcsingularity(self, nommodel, stream1, stream2):
        itypehydra=self.__instances["singularity_type"]["hydrology_bc"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        n4byte=lgrecord/4
        # Query the database and obtain data as Python objects
        res = self.__project.execute("""
             select id, name, node
             from %s.hydrology_bc_singularity
             order by id
             """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            self.log.notice("Exporting hydrology bc: {}".format(rec[1]))
            self.__icount_hydrol += 1

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,rec[1],itypehydra,rec[2],0))

            # writing in LI2 binary file
            stream2.write(bytearray(lgrecord)) # pad with zeros
        return

    #3.2.10
    def __exporthydrol_pipe(self, nommodel, stream1, stream2):
        itypehydra=self.__instances["link_type"]["pipe"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        n4byte=lgrecord/4
        # Query the database and obtain data as Python objects
        #             0      1    2        3             4                5        6                          7             8              9               10             11                  12               13                      14                     15          16
        res = self.__project.execute("""
            select l.id, l.name, l.up, l.down, l.z_invert_up, l.z_invert_down, hydra.cross_section_type.id, l.h_sable, l.custom_length, ST_Length(l.geom), l.rk, l.cross_section_type, l.circular_diameter, l.ovoid_height, l.ovoid_top_diameter, l.ovoid_invert_diameter, l.cp_geom, l.op_geom
            from {model}.pipe_link as l
            join hydra.cross_section_type on (hydra.cross_section_type.name = l.cross_section_type)
            where branch is null
            order by l.id
            """.format(model=nommodel)).fetchall()

        for i, rec in enumerate(res):
            self.log.notice("Exporting hydrology pipe: {}".format(rec[1]))
            self.__icount_hydrol += 1
            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,rec[1],itypehydra,rec[2],rec[3]))

            # writing in LI2 binary file    corr TL 03/03/2017
            start = stream2.tell()
            length = rec[8] if rec[8] is not None else rec[9]
            if rec[11]=='circular':
                stream2.write(struct.pack('fffifff', rec[4],rec[5],length,rec[6],rec[10],rec[7],rec[12]))
            elif rec[11]=='ovoid':
                stream2.write(struct.pack('fffifffff', rec[4],rec[5],length,rec[6],rec[10],rec[7],rec[13],rec[14],rec[15]))
            elif rec[11]=='pipe':
                stream2.write(struct.pack('fffiffi', rec[4],rec[5],length,rec[6],rec[10],rec[7],rec[16]))
            elif rec[11]=='channel':
                stream2.write(struct.pack('fffiffi', rec[4],rec[5],length,rec[6],rec[10],rec[7],rec[17]))
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros

        return

    #3.2.11
    def __exporthydrol_hydrograph_bc(self, nommodel, stream1, stream2):
        itypehydra=self.__instances["singularity_type"]["hydrograph_bc"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        # Query the database and obtain data as Python objects
        res = self.__project.execute("""
            select s.id, s.name, s.node, s.constant_dry_flow, s.tq_array, s.sector, s.hourly_modulation, s.distrib_coef, s.lag_time_hr, s.pollution_quality_mode, s.pollution_quality_param_json->'pollution'::varchar, s.pollution_quality_param_json->'quality'::varchar, external_file_data
            from %s.hydrograph_bc_singularity as s
            order by s.id
            """%(nommodel)).fetchall()

        # get sum of total distrib coefs by sectors
        sectors_total_contributions={}
        sectors = [sect for sect, in self.__project.execute("""select id from project.dry_inflow_sector""").fetchall()]
        for s in sectors:
            total=0
            for model in self.__project.get_models():
                sum = self.__project.execute("""select sum(hy.distrib_coef)
                                            from {}.hydrograph_bc_singularity as hy
                                            where hy.sector={} and hy.distrib_coef>0""".format(model, s)).fetchone()
                if sum is not None and sum[0] is not None:
                    total += sum[0]
            sectors_total_contributions[s]=total

        for i, rec in enumerate(res):
            self.log.notice("Exporting bc hydrograph: {}".format(rec[1]))
            id,name,node,constant_dry_flow,tq_array,sector,hourly_modulation,distrib_coef,lag_time_hr,pollution_quality_mode, pq_param_pollution, pq_param_quality, external_file_data = rec
            self.__icount_hydrol += 1

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            if (external_file_data): imode_external=1
            else:                    imode_external=0

            if (tq_array is None): ltq=0
            else:                  ltq=len(tq_array)

            if (sector is None):
                imode_qts=0
                name_sector=" "*24   # 24 spaces
                contribution = 0
            else:
                imode_qts=1
                name_sector = self.__project.execute("""select  name from project.dry_inflow_sector where id=%d """ % (sector)).fetchone()[0]
                name_sector+=" "*(24-len(name_sector)) # in order to make it padded with spaces, not null char
                contribution = distrib_coef/sectors_total_contributions[sector] if distrib_coef else 0

            start = stream2.tell()
            stream2.write(struct.pack('ifii', imode_external, constant_dry_flow, ltq, imode_qts))
            stream2.write(str(name_sector))
            stream2.write(struct.pack('ff', contribution, lag_time_hr))

            for i in range(ltq): stream2.write(struct.pack('f', tq_array[i][0] ))
            for i in range(20-ltq): stream2.write(struct.pack('f', 0. ))
            for i in range(ltq): stream2.write(struct.pack('f', tq_array[i][1] ))
            for i in range(20-ltq): stream2.write(struct.pack('f', 0. ))

            if (pq_param_pollution is None):
                p = [0. for n in range(10)]
            else:
                p = [float(pq_param_pollution[n]) for n in sorted(self.__instances['pollution_quality_mode']['pollution']['params'].keys(), key=lambda k: self.__instances['pollution_quality_mode']['pollution']['params'][k]['place'])]
                p += [0.]*2 # pad with zeros

            if (pq_param_quality is None):
                p+= [0. for n in range(20)]
            else:
                p+= [float(pq_param_quality[n]) for n in sorted(self.__instances['pollution_quality_mode']['quality']['params'].keys(), key=lambda k: self.__instances['pollution_quality_mode']['quality']['params'][k]['place'])]
                p += [0.]*5 # pad with zeros

                for pp in p: stream2.write(struct.pack('f', pp))

            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return

    #3.2.12
    def __exporthydrol_tz_bc(self, nommodel, stream1, stream2):
        itypehydra=self.__instances["singularity_type"]["tz_bc"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        # Query the database and obtain data as Python objects
        res = self.__project.execute("""
            select id, name, node, tz_array, cyclic, external_file_data
            from %s.tz_bc_singularity as s
            order by id
            """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            self.log.notice("Exporting bc tz: {}".format(rec[1]))
            id, name, node, tz_array, cyclic, external_file_data = rec
            self.__icount_hydrol += 1

            if (external_file_data): imode_external=1
            else:                    imode_external=0

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            if (tz_array is None):
                ltz=0
            else:
                ltz=len(tz_array)
            start = stream2.tell()
            stream2.write(struct.pack('ii', imode_external, ltz ))
            for i in range(ltz): stream2.write(struct.pack('f', tz_array[i][0] ))
            for i in range(20-ltz): stream2.write(struct.pack('f', 0. ))
            for i in range(ltz): stream2.write(struct.pack('f', tz_array[i][1] ))
            for i in range(20-ltz): stream2.write(struct.pack('f', 0. ))
            #for i in range(ltz):
            #    stream2.write(struct.pack('ff', tz_array[i][0], tz_array[i][1] ))
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return

    #3.2.13   #TL 20180318
    def __exporthydrol_marker(self, nommodel, stream1, stream2):
        itypehydra=self.__instances["singularity_type"]["marker"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        # Query the database and obtain data as Python objects
        res = self.__project.execute("""
            select id, name,node
            from %s.marker_singularity as s
            order by id
            """%(nommodel)).fetchall()
        for i, rec in enumerate(res):
            self.log.notice("Exporting marker: {}".format(rec[1]))
            id, name,node = rec
            self.__icount_hydrol += 1

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))
        return

    # ------------------------------------------------------------------------------------
    # export functions for tables sections
    # ------------------------------------------------------------------------------------
    def __add_sgeom_to_list(self, lst,r,n,z0,z_invert):
        z=numpy.zeros(n,dtype=numpy.float32)
        b=numpy.zeros(n,dtype=numpy.float32)
        for i in range(n):
            if (i<len(r)):
                zi=r[i][0]
            else:
                zi=None

            if (zi==None):
                z[i]=z[i-1]
                b[i]=b[i-1]
            elif (z_invert==None):
                z[i]=r[i][0]
                b[i]=r[i][1]
            else:
                z[i]=r[i][0]-z0+z_invert
                b[i]=r[i][1]
        self.__add_array_to_list(lst,z,n)
        self.__add_array_to_list(lst,b,n)
        return

    #3.3
    def __export_section(self, nommodel, streamfsec):
        lgrecord=self.__record_length["SECTION.DAT"]
        n4byte=lgrecord/4 # number of 4-bytes words in LI2-record
        #
        #--- Section PF/PO (pipe/channel_cross_section) ---
        #
        for i in range(2):
            if (i==0):
                sh="PF"
                sc="pipe"
                sg="closed"
                sgeom="cp_geom"
            else:
                sh="PO"
                sc="channel"
                sg="open"
                sgeom="op_geom"
            itype=self.__instances["cross_section_type"][sc]["group_hydra_no"]
            srecord=ctypes.create_string_buffer(lgrecord)
            #      sc = pipe or channel      sg = closed or open                     sgeom = cp_geom or op_geom
            #      0     1
            res = self.__project.execute("""
            select id, name, zbmin_array
            from {model}.{sg}_parametric_geometry
            where id in (select {geom} from {model}.pipe_link where cross_section_type='{type}')
            union
            select id, name, zbmin_array
            from {model}.{sg}_parametric_geometry
            where id in (select up_{geom} from {model}.river_cross_section_profile where type_cross_section_up='{type}')
            union
            select id, name, zbmin_array
            from {model}.{sg}_parametric_geometry
            where id in (select down_{geom} from {model}.river_cross_section_profile where type_cross_section_down='{type}')
            order by id;
            """.format(model=nommodel, type=sc, sg=sg, geom=sgeom)).fetchall()

            for rec in res:
                self.log.notice("Exporting {} section: {}".format(sc, rec[0]))
                streamfsec.write("%4d %4d  %24s \n" % (rec[0],itype,rec[1]))
                self.__writ_tableau(rec[2], streamfsec, "%10.3f %9.3f")
                if (len(rec)>0): streamfsec.write("\n")

        #
        #--- Section VL ---
        #
        itype=self.__instances["cross_section_type"]["valley"]["group_hydra_no"]
        #          0    1     2             3                 4                  5      6      7       8          9
        res = self.__project.execute("""
            select id, name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb
            from {model}.valley_cross_section_geometry
            where id in (select up_vcs_geom from {model}.river_cross_section_profile where type_cross_section_up='valley')
            union
            select id, name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb
            from {model}.valley_cross_section_geometry
            where id in (select down_vcs_geom from {model}.river_cross_section_profile where type_cross_section_down='valley')
            order by id;
            """.format(model=nommodel)).fetchall()

        for rec in res:
            self.log.notice("Exporting valley section: {}".format(rec[0]))
            streamfsec.write("%4d %4d %24s \n" % (rec[0],itype,rec[1]))
            streamfsec.write("%9.3f %9.3f %9.3f %9.3f %9.3f\n" % (rec[5], rec[6], rec[7], rec[8], rec[9]))
            self.__writ_tableau(rec[2], streamfsec, "%10.3f %9.3f")
            self.__writ_tableau(rec[3], streamfsec, "%10.3f %9.3f")
            self.__writ_tableau(rec[4], streamfsec, "%10.3f %9.3f")
            if (len(rec)>0): streamfsec.write("\n")

        streamfsec.close()
        return

    # ------------------------------------------------------------------------------------
    # export functions for tables CONTAINER
    # ------------------------------------------------------------------------------------
    #4.2.1
    def __exportnode_station(self, nommodel, stream_):
        itypehydra=self.__instances["node_type"]["station"]["group_hydra_no"]

        resStn = self.__project.execute("""
            select id, name
            from %s.station
            order by id
            """%(nommodel)).fetchall()

        resNod = self.__project.execute("""select id    from %s.station_node    order by id"""%(nommodel)).fetchall()
        self.__update_node_max(resNod)
        for recStn in resStn:
            #                    0    1      2      3        4
            res = self.__project.execute("""
                select id, name, area, z_invert, station
                from  %s.station_node
                where station=%d
                """%(nommodel,recStn[0])).fetchall()

            comment="Element NDSG '%s'" % (recStn[1])
            self.__writ_parametres_generaux(stream_, res, itypehydra, 0, comment)

            for i, rec in enumerate(res):
                self.log.notice("Exporting station: {}".format(rec[1]))
                stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
                stream_.write("%10.3f %10.3f\n" % (rec[3],rec[2]))
            stream_.write("\n")
        return

    #4.2.2
    def __exportnode_storage(self, nommodel, stream_, nomtable, element):
        itypehydra=self.__instances["node_type"]["storage"]["group_hydra_no"]  #37
        res = self.__project.execute("""
            select id, name, zs_array, zini
            from %s.%s
            order by id
            """%(nommodel,nomtable)).fetchall()

        self.__update_node_max(res)
        self.__writ_parametres_generaux(stream_, res, itypehydra, 10, "Element "+element)

        for i, rec in enumerate(res):
            self.log.notice("Exporting storage: {}".format(rec[1]))
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            np=len(rec[2]) # nombre de points z-surface
            #stream_.write("%10.3f %10.2f %10.3f" % (rec[2][0][0],rec[2][np-1][1],rec[3]))
            stream_.write("%10.3f" % (rec[3]))
            self.__writ_tableau(rec[2], stream_, "%10.3f %19.2f")
        if (len(res)>0): stream_.write("\n")
        return

    #4.2.3
    def __exportnode_2d(self, nommodel, stream_):

        itypehydra=self.__instances["node_type"]["elem_2d"]["group_hydra_no"]  #36

        resDomain = self.__project.execute("""
            select id, name
            from %s.domain_2d
            order by id
            """%(nommodel)).fetchall()

        resDomain.insert(0,[0,'default_domain'])

        for recDomain in resDomain :
            #                    0   1      2   3     4     5     6
            res = self.__project.execute("""
            select id, name, area, zb, rk, ST_X(geom), ST_Y(geom)
            from {}.elem_2d_node
            where coalesce(domain_2d,0)={}
            order by id
            """.format(nommodel, recDomain[0])).fetchall()

            self.__update_node_max(res)

            self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element PAV  '%s'" % (recDomain[1]))
            for i, (id_, name, area, zb, rk, x, y) in enumerate(res):
                self.log.notice("Exporting elem 2D: {}".format(name))
                zb_min = zb #@todo: à capturer à partir des LPAV adjacents
                stream_.write("%5d %9d     '%s'\n" %(i+1, id_, name))
                stream_.write("%10.2f %10.4f %10.2f %10.4f %12.2f %12.2f\n" % ( area, zb, rk, zb_min, x, y))
            if (len(res)>0): stream_.write("\n")
        return

    #4.2.4
    def __exportnode_crossroad(self, nommodel, stream_):
        itypehydra=self.__instances["node_type"]["crossroad"]["group_hydra_no"]  #38
        res = self.__project.execute("""
            select id, name, area, z_ground, h, ST_X(geom), ST_Y(geom)
            from %s.crossroad_node
            order by id
            """%(nommodel)).fetchall()

        self.__update_node_max(res)
        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element CAR ")
        for i, rec in enumerate(res):
            self.log.notice("Exporting crossroad: {}".format(rec[1]))
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%10.4f %10.4f %10.3f  %12.2f %12.2f\n" % (rec[3], rec[4], rec[2], rec[5], rec[6]))
        if (len(res)>0): stream_.write("\n")

    # ------------------------------------------------------------------------------------
    # export functions for tables SINGULARITIES
    # ------------------------------------------------------------------------------------
    def __initialize_new_node(self, nommodel):
        res = self.__project.execute(
        "select id  from %s._node order by id"%(nommodel)
        ).fetchall()

        n=len(res)
        return res[n-1][0]+1

    #4.2.6
    def __exportsing_gate(self, nommodel, stream_, streamtmp):
        itypehydra=self.__instances["singularity_type"]["gate"]["group_hydra_no"]
        #            0    1         2       3              4         5      6      7                     8                        9          10
        res = self.__project.execute("""
            select s.id, s.name, s.node, s.z_invert, s.z_ceiling, s.width, s.cc, hydra.valve_mode.id, hydra.action_gate_type.id, s.z_gate, s.v_max_cms, s.full_section_discharge_for_headloss
            from %s.gate_singularity as s
            join hydra.valve_mode on hydra.valve_mode.name = s.mode_valve
            join hydra.action_gate_type on hydra.action_gate_type.name = s.action_gate_type
            order by s.id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element EORIF")
        for i, rec in enumerate(res):
            self.log.notice("Exporting singularity gate: {}".format(rec[1]))
            self.__writ_one_singularity(rec[1], rec[2], i, stream_, streamtmp)
            id_section_discharge = 0 if rec[11] else 1
            stream_.write("%9.3f %9.3f %9.2f %9.3f %4d %4d %9.3f %9.3f" % (rec[3:11]))
            stream_.write("%4d\n" % (id_section_discharge))
        if (len(res)>0): stream_.write("\n")
        return

    #4.2.7  &  4.2.9   -    export function for hydraulic_cut_singularity (4.2.7) and param_headloss_singularity (4.2.9)
    def __exportsing_param(self, nommodel, stream_, streamtmp, tablesing, name_array, code_elem, code_hydra):
        # tablesing                     name_array    code_elem  code_hydra
        # hydraulic_cut_singularity     qz_array      CPR        ECPR(23)
        # param_headloss_singularity    q_dz_array    DH         ESPRM(10)
        itypehydra=self.__instances["singularity_type"][tablesing.replace("_singularity", "")]["group_hydra_no"]
        res = self.__project.execute("""
            select id, name, node, %s, full_section_discharge_for_headloss
            from %s.%s
            order by id
            """%(name_array,nommodel,tablesing)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element %s"%(code_hydra))
        for i, rec in enumerate(res):
            self.log.notice("Exporting singularity {}: {}".format(tablesing, rec[1]))
            self.__writ_one_singularity(rec[1], rec[2], i, stream_, streamtmp)
            id_section_discharge = 0 if rec[4] else 1
            stream_.write("%5d" %(id_section_discharge))
            self.__writ_tableau(rec[3], stream_, "%10.3f %9.4f")
        if (len(res)>0): stream_.write("\n")
        return


    #4.2.8
    def __exportsing_borda_headloss(self, nommodel, stream_, streamtmp):
        itypehydra=self.__instances["singularity_type"]["borda_headloss"]["group_hydra_no"]
        res = self.__project.execute("""
            select id, name, node, law_type, param_json->law_type::varchar, full_section_discharge_for_headloss
            from %s.borda_headloss_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element ESMK")
        for i, (id_, name, node, law_type, param, full_section_discharge_for_headloss) in enumerate(res):
            self.log.notice("Exporting singularity borda: {}".format(name))
            self.__writ_one_singularity(name, node, i, stream_, streamtmp)

            num_law_type = self.__instances['borda_headloss_singularity_type'][law_type]['id']
            id_section_discharge = 0 if full_section_discharge_for_headloss else 1

            stream_.write("%5d %5d" %(num_law_type, id_section_discharge))
            stream_.write("       headloss type: %s" %(law_type))
            stream_.write("\n")
            if (law_type=="x"):
                self.log.warning("type %s: ne marche pas, ignoré" % law_type)
                #@todo: à voir, a priori fichier json
                continue

            np=0
            for n in sorted(self.__instances['borda_headloss_singularity_type'][law_type]['params'].keys(),
                    key=lambda k:
                        self.__instances['borda_headloss_singularity_type'][law_type]['params'][k]['place']):
                param_type = self.__instances['borda_headloss_singularity_type'][law_type]['params'][n]['type']
                np+=1

                if n == 'middle_option':
                    if param['middle_option'] == 'no':
                        param['middle_cs_area'] = 1

                if n == 'connector_no':
                    if param['connector_no'] == 0:
                        param['ls'] = 1

                if (param_type=="real"):
                    if law_type == "sharp_bend_rectangular" and (n == "param3" or n == "param4"):
                        if param['bend_shape'] == 'u_shaped_elbow':
                            stream_.write("  %f" % float(param[n]))
                        else:
                            np-=1
                    else:
                        stream_.write("  %f" % float(param[n]))  # corr TL 20171123

                elif (param_type=="integer"):
                    stream_.write("  %10d" % param[n])
                elif (param_type[0:5]=="real["):
                    param_cleared = [[q,h] for [q,h] in param[n] if q is not None and h is not None]
                    # case concerned : parametric
                    for v in param_cleared:
                        stream_.write("  %10.3f  %10.3f" % (v[0],v[1]))
                    np+=2*len(param_cleared)-1
                else:
                    # type enumeration expected. conversion into integer
                    try:
                        m = re.match(r'enum\((.*)\)', self.__instances['borda_headloss_singularity_type'][law_type]['params'][n]['type'])
                        enum_map = {v: i+1 for i, v in enumerate(m.group(1).replace("'","").split(', '))}
                        stream_.write("  %10d" % enum_map[param[n]])
                    except AttributeError:
                        self.log.error("Unexpected case. AttributeError on:"
                            +"n:"+str(n)
                            +"param[n]:"+str(param[n])
                            +"type(param[n]):"+type(param[n])
                            +str(self.__instances['borda_headloss_singularity_type'][law_type]['params'][n]['type']))
                        return

            for n in range(12-np):
                stream_.write("  %10.1f" % 0.) # pad with zeros
            stream_.write("\n")


        if (len(res)>0): stream_.write("\n")
        return

    #4.2.10
    def __exportsing_zregul_weir(self, nommodel, stream_, streamtmp):
        itypehydra=self.__instances["singularity_type"]["zregul_weir"]["group_hydra_no"]
        res = self.__project.execute("""
            select id, name, node, z_invert, z_regul, width, cc, mode_regul, reoxy_law, reoxy_param_json->reoxy_law::varchar, full_section_discharge_for_headloss
            from %s.zregul_weir_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element EDEVM")
        for i, rec in enumerate(res):
            self.log.notice("Exporting singularity weir: {}".format(rec[1]))
            self.__writ_one_singularity(rec[1], rec[2], i, stream_, streamtmp)

            id_mode_regul = self.__instances['weir_mode_regul'][rec[7]]['id']
            id_law = self.__instances['weir_mode_reoxy'][rec[8]]['id']
            id_section_discharge = 0 if rec[10] else 1
            p = [float(rec[9][n]) for n in sorted(self.__instances['weir_mode_reoxy'][rec[8]]['params'].keys(), key=lambda k: self.__instances['weir_mode_reoxy'][rec[8]]['params'][k]['place'])]
            p += [0.]*(3-len(p)) # pad with zeros

            stream_.write("%10.3f %9.3f %9.2f %9.3f %4d %4d\n" % (rec[3], rec[4], rec[5], rec[6], id_mode_regul, id_section_discharge))
            stream_.write("%5d %9.3f %9.3f %9.3f\n" % (id_law, p[0], p[1], p[2]))

        if (len(res)>0): stream_.write("\n")
        return

    #4.2.12  et #4.2.14  :  modif TL -  les objets markers sont exportés désormais comme des élments BC  ( cf % 4.3.29 et 4.3.30)

    #4.2.11
    def __exportsing_regul_gate(self, nommodel, stream_, streamtmp):
        itypehydra=self.__instances["singularity_type"]["regul_sluice_gate"]["group_hydra_no"]
        #           0    1    2           3      4        5          6     7     8                9                10            11          12
        res = self.__project.execute("""
            select id, name, mode_regul, node, z_invert, z_ceiling, width, cc, action_gate_type, z_invert_stop, z_ceiling_stop, v_max_cms, dt_regul_hr,
            z_control_node, z_pid_array, z_tz_array, q_tq_array, q_z_crit, nr_z_gate, full_section_discharge_for_headloss from %s.regul_sluice_gate_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element EACTA")
        for i, rec in enumerate(res):
            self.log.notice("Exporting singularity gate: {}".format(rec[1]))
            self.__writ_one_singularity(rec[1], rec[3], i, stream_, streamtmp)

            num_action = self.__instances['action_gate_type'][rec[8]]['id']
            regul = self.__instances['gate_mode_regul'][rec[2]]['id']
            id_section_discharge = 0 if rec[10] else 1

            stream_.write("%10.3f %9.3f %9.2f %9.3f" % (rec[4:8]))
            stream_.write("%4d" % num_action)
            stream_.write("%10.3f %9.3f %9.3f" % (rec[9:12]))
            stream_.write("%9.3f %4d %4d\n" % (rec[12], regul, id_section_discharge))
            if rec[2]=='elevation':
                stream_.write("%10d %9.3f %9.3f %9.3f" %(rec[13], rec[14][0], rec[14][1], rec[14][2]))
                self.__writ_tableau(rec[15], stream_, "%10.3f %9.3f")
            if rec[2]=='discharge':
                stream_.write("%9.3f" % rec[17])
                self.__writ_tableau(rec[16], stream_, "%10.3f %9.3f")
            if rec[2]=='no_regulation':
                stream_.write("%9.3f" % rec[18])
                # for other options, __write_tableau includes line-ends
                # for no_regulation, force line-end :
                stream_.write("\n")
        if (len(res)>0): stream_.write("\n")
        return

    #4.2.13
    def __exportsing_bradley_headloss(self, nommodel, stream_, streamtmp):
        itypehydra=self.__instances["singularity_type"]["bradley_headloss"]["group_hydra_no"]
        #            0     1        2       3              4                 5                       6            7
        res = self.__project.execute("""
            select s.id, s.name, s.node, s.d_abutment_l, s.d_abutment_r, hydra.abutment_type.id, s.zw_array, s.z_ceiling
            from %s.bradley_headloss_singularity as s
            join hydra.abutment_type on hydra.abutment_type.name = s.abutment_type
            order by s.id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element EBRD")
        for i, rec in enumerate(res):
            self.log.notice("Exporting singularity bradley: {}".format(rec[1]))
            self.__writ_one_singularity(rec[1], rec[2], i, stream_, streamtmp)
            stream_.write("%10.3f %9.3f %4d %9.3f" % (rec[3],rec[4],rec[5],rec[7]))
            self.__writ_tableau(rec[6], stream_, "%10.3f %9.2f")
        if (len(res)>0): stream_.write("\n")
        return

    #4.2.15
    def __exportsing_bridge_headloss(self, nommodel, stream_, streamtmp):
        itypehydra=self.__instances["singularity_type"]["bridge_headloss"]["group_hydra_no"]
        #          0    1    2      3        4       5
        res = self.__project.execute("""
            select id, name, node, z_road, l_road, zw_array, full_section_discharge_for_headloss
            from %s.bridge_headloss_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element EBRDG")
        for i, rec in enumerate(res):
            self.log.notice("Exporting singularity bridge: {}".format(rec[1]))
            self.__writ_one_singularity(rec[1], rec[2], i, stream_, streamtmp)
            id_section_discharge = 0 if rec[6] else 1
            stream_.write("%10.3f %9.3f %4d" % (rec[3],rec[4], id_section_discharge))
            self.__writ_tableau(rec[5], stream_, "%10.3f %9.2f")
        if (len(res)>0): stream_.write("\n")
        return

    # ------------------------------------------------------------------------------------
    # export functions for tables NODES manhole & river
    # ------------------------------------------------------------------------------------

    #4.3.1/4.3.2
    def __exportnode_network(self, nommodel, stream_, type, comment):
        # Query the database and obtain data as Python objects
        # itypehydra=28
        itypehydra=self.__instances["node_type"][type]["group_hydra_no"]
        nomtable=type+"_node"
        res = self.__project.execute("""
            select id, name, area, z_ground, ST_X(geom), ST_Y(geom)
            from %s.%s
            order by id
            """%(nommodel,nomtable)).fetchall()
        self.__update_node_max(res)
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, comment)
        for i, rec in enumerate(res):
            self.log.notice("Exporting node {}: {}".format(type, rec[1]))
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%10.3f %10.4f %14.3f %14.3f\n" % (rec[2],rec[3],rec[4],rec[5]))
        if (len(res)>0): stream_.write("\n")
        return

    # ------------------------------------------------------------------------------------
    # export functions for tables LINKS
    # ------------------------------------------------------------------------------------

    #4.3.5
    def __exportlink_derivation_pump(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["deriv_pump"]["group_hydra_no"]   #12
        srequete = """
            select id, name, up, down, q_pump, qz_array
            from %s.deriv_pump_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, 10, "Liaison LQDZ_pomp")
        for i, rec in enumerate(res):
            self.log.notice("Exporting link deriv pump: {}".format(rec[1]))
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            loipomp=list()
            for j in range(len(rec[5])):
                loipomp.append([rec[4]*rec[5][j][0], rec[5][j][1]])
            self.__writ_tableau(loipomp, stream_, "%10.3f %9.3f")
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.6
    def __exportlink_weir(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["weir"]["group_hydra_no"]   #3
        srequete = """
            select id, name, up, down, z_invert, width, cc, z_weir, v_max_cms
            from %s.weir_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LDEV")
        for i, rec in enumerate(res):
            self.log.notice("Exporting link weir: {}".format(rec[1]))
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            stream_.write("%10.3f %9.2f %9.3f %9.3f %9.3f\n" % (rec[4],rec[5],rec[6],rec[7],rec[8]))
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.7
    def __exportlink_gate(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["gate"]["group_hydra_no"]   #4
        #                   0    1    2    3     4          5         6    7      8              9               10    11
        srequete = """
            select l.id, l.name, l.up, l.down, l.z_invert, l.z_ceiling, l.width, l.cc, hydra.valve_mode.id, hydra.action_gate_type.id, l.z_gate, l.v_max_cms
            from %s.gate_link as l
            join hydra.valve_mode on hydra.valve_mode.name = l.mode_valve
            join hydra.action_gate_type on hydra.action_gate_type.name = l.action_gate_type
            order by l.id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LORF")
        for i, rec in enumerate(res):
            self.log.notice("Exporting link gate: {}".format(rec[1]))
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            stream_.write("%10.3f %9.3f %9.2f %9.3f %4d %4d %9.3f %9.3f \n" % (rec[4:12]))
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.8
    def __exportlink_pump(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["pump"]["group_hydra_no"]   #10
        srequete = """
            select id, name, up, down, npump, zregul_array, hq_array
            from %s.pump_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LPOMP")
        for i, rec in enumerate(res):
            self.log.notice("Exporting link pump: {}".format(rec[1]))
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            id, name, up, down, npump, zregul_array, hq_array = rec
            stream_.write("%5d\n" % (npump))
            for j in range(npump):
                stream_.write(" %9.3f %9.3f\n" % (zregul_array[j][0],zregul_array[j][1]))

                npt=len(hq_array[j])
                number_None = hq_array[j].count([None,None])
                stream_.write("%5d\n" % (npt-number_None))
                for k in range(npt):
                    if [hq_array[j][k][0],hq_array[j][k][1]] != [None, None]:
                        stream_.write(" %9.3f %9.3f\n" % (hq_array[j][k][0],hq_array[j][k][1]))
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.9
    def __exportlink_regul_gate(self, nommodel, stream_):  # liaisons RGZ et RGQ
        itypehydra= 8
        #           0    1    2           3    4      5          6        7     8         9                10            11          12          13
        srequete = """
            select id, name, mode_regul, up, down, z_invert, z_ceiling, width, cc, action_gate_type, z_invert_stop, z_ceiling_stop, v_max_cms, dt_regul_hr,
            z_control_node, z_pid_array, z_tz_array, q_tq_array, q_z_crit, nr_z_gate
            from %s.regul_gate_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison RGZ ou RGQ")
        for i, rec in enumerate(res):
            self.log.notice("Exporting link regul gate: {}".format(rec[1]))
            num_action = self.__instances['action_gate_type'][rec[9]]['id']
            regul = self.__instances['gate_mode_regul'][rec[2]]['id']

            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[3], rec[4], rec[1]))
            stream_.write("%10.3f %9.3f %9.2f %9.3f" % (rec[5:9]))
            stream_.write("%5d" % num_action)
            stream_.write("%10.3f %9.3f %9.3f" % (rec[10:13]))
            stream_.write("%9.3f %4d \n" % (rec[13], regul))
            if rec[2]=='elevation':
                stream_.write("%10d %9.3f %9.3f %9.3f\n" %(rec[14], rec[15][0], rec[15][1], rec[15][2]))
                self.__writ_tableau(rec[16], stream_, "%10.3f %9.3f")
            if rec[2]=='discharge':
                stream_.write("%9.3f" % rec[18])
                self.__writ_tableau(rec[17], stream_, "%10.3f %9.3f")
            if rec[2]=='no_regulation':
                stream_.write("%9.3f" % rec[19])
                # for other options, __write_tableau includes line-ends
                # for no_regulation, force line-end :
                stream_.write("\n")
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.11
    def __exportlink_connector(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["connector"]["group_hydra_no"]   #19
        srequete = """
            select id, name, up, down
            from %s.connector_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LMTR")
        for i, rec in enumerate(res):
            self.log.notice("Exporting link connector: {}".format(rec[1]))
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.12
    def __exportlink_borda_headloss(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["borda_headloss"]["group_hydra_no"]   #20
        srequete = """
            select id, name, up, down, law_type, param_json->law_type::varchar
            from %s.borda_headloss_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LMK")
        for i, (id_, name, up, down, law_type, param) in enumerate(res):
            self.log.notice("Exporting link borda: {}".format(name))
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, up, down, name))

            num_law_type = self.__instances['borda_headloss_link_type'][law_type]['id'],
            stream_.write("%5d" %(num_law_type))
            stream_.write("       headloss type: %s" %(law_type))
            stream_.write("\n")
            if (law_type=="x" or law_type=="xx"):
                self.log.warning("type %s: ne marche pas, ignoré" % law_type)
                #@todo: à voir, a priori fichier json
                continue

            # Conversion enum en entiers.
            np=0
            for n in sorted(self.__instances['borda_headloss_link_type'][law_type]['params'].keys(), key=lambda k: self.__instances['borda_headloss_link_type'][law_type]['params'][k]['place']):
                param_type = self.__instances['borda_headloss_link_type'][law_type]['params'][n]['type']
                np+=1

                if n == 'middle_option':
                    if param['middle_option'] == 'no':
                        param['middle_cs_area'] = 1

                if n == 'connector_no':
                    if param['connector_no'] == 0:
                        param['ls'] = 1

                if '_cs_area' in n or '_cs_zinvert' in n:
                    side, type = n.split('_cs_')
                    if side in ['up', 'down']:
                        if param[side+'_geom'] == 'computed':
                            param[n] = 1
                        if param[side+'_geom'] == 'circular' and '_cs_zinvert' in n:
                            param[n] = 1

                if (param_type=="real"):
                    if law_type == "sharp_bend_rectangular" and (n == "param3" or n == "param4"):
                        if param['bend_shape'] == 'u_shaped_elbow':
                            stream_.write("  %f" % float(param[n]))
                        else:
                            np-=1
                    else:
                        stream_.write("  %f" % float(param[n]))  # corr TL 20171123

                elif (param_type=="integer"):
                    stream_.write("  %10d" % param[n])
                elif (param_type[0:5]=="real["):
                    param_cleared = [[q,h] for [q,h] in param[n] if q is not None and h is not None]
                    # case concerned : parametric
                    for v in param_cleared:
                        stream_.write("  %10.3f  %10.3f" % (v[0],v[1]))
                    np+=2*len(param_cleared)-1
                else:
                    # type enumeration expected. conversion into integer
                    try:
                        m = re.match(r'enum\((.*)\)', self.__instances['borda_headloss_link_type'][law_type]['params'][n]['type'])
                        enum_map = {v: i+1 for i, v in enumerate(m.group(1).replace("'","").split(', '))}
                    except AttributeError:
                        raise Exception("Unexpected case. AttributeError on:"
                            +"n:"+str(n
                            +"param[n]:"+str(param[n]))
                            +"type(param[n]):"+type(param[n])
                            +str(self.__instances['borda_headloss_link_type'][law_type]['params'][n]['type']))

                    stream_.write("  %10d" % enum_map[param[n]])

            for n in range(12-np):
                stream_.write("  %10.1f" % 0.) # pad with zeros
            stream_.write("\n")

        if (len(res)>0): stream_.write("\n")
        return

    #4.3.13
    def __exportlink_2d_mesh(self, nommodel, stream_):
        coment="Liaison LPAV"
        res = self.__project.execute("""
            select id, name, up, down, z_invert, lateral_contraction_coef,
            ST_X(ST_StartPoint(border)),
            ST_Y(ST_StartPoint(border)),
            ST_X(ST_EndPoint(border)),
            ST_Y(ST_EndPoint(border))
            from %s.mesh_2d_link
            order by id
            """%(nommodel)).fetchall()

        # numbers of LPAV
        nlpav=len(res)

        # links LPAV
        if (nlpav>0):
            itypehydra=self.__instances["link_type"]["mesh_2d"]["group_hydra_no"]   #5
            stream_.write("$ Liaison LPAV\n")
            stream_.write("%5d %5d \n" % (itypehydra,nlpav))

            # self.__writ_parametres_generaux(stream_, res, itypehydra, -1, coment)
            # res = self.__link_request(nommodel, srequete, stream_, itypehydra, "Liaison LPAV")
            rec_link=[]
            for i, rec in enumerate(res):
                self.log.notice("Exporting link 2D: {}".format(rec[1]))
                id, name, up, down, z_invert, lateral_contraction_coef = rec[:6]
                border = rec[6:]
                stream_.write("%5d %10d %10d    '%-17s'\n" %(i+1, up, down, name))
                stream_.write("%f %f %f %f %f %f\n" % (z_invert, border[0], border[1], border[2], border[3], lateral_contraction_coef))
            stream_.write("\n")


    #4.3.14
    def __exportlink_strickler(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["strickler"]["group_hydra_no"]   #1
        srequete = """
            select id, name, up, down, z_crest1, width1, length, rk, z_crest2, width2
            from %s.strickler_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LSTK")
        for i, rec in enumerate(res):
            self.log.notice("Exporting link strickler: {}".format(rec[1]))
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            stream_.write("%10.3f %9.2f %9.2f %9.2f %9.3f %9.2f\n" % (rec[4:10]))
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.15
    def __exportlink_porous(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["porous"]["group_hydra_no"]   #2
        srequete = """
            select id, name, up, down, z_invert, width, transmitivity
            from %s.porous_link
            order by id
            """%(nommodel)  # s désigne zs
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LPOR")
        for i, rec in enumerate(res):
            self.log.notice("Exporting link porous: {}".format(rec[1]))
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            stream_.write("%10.3f %9.2f %10.3g\n" % (rec[4:7]))
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.16
    def __exportlink_overflow(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["overflow"]["group_hydra_no"]   #6
        #                     0    1    2    3     4        5       6          7     8      9               10
        srequete = """select id, name, up, down, z_crest1, width1, z_crest2, width2, cc, lateral_contraction_coef, break_mode,
            z_invert, width_breach, grp, z_break, t_break, dt_fracw_array
            from %s.overflow_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison OVFL")
        for i, rec in enumerate(res):
            self.log.notice("Exporting link overflow: {}".format(rec[1]))
            break_mode = self.__instances["fuse_spillway_break_mode"][rec[10]]["id"]

            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            stream_.write("%10.3f %9.2f %10.3f %9.2f %9.2f %9.2f" % (rec[4:10]))
            stream_.write("%4d\n" % (break_mode))
            if rec[10]=='zw_critical':
                stream_.write("%10.3g %10.3g %10d" %(rec[11:14]))  # coor format TL 20171129
                stream_.write("%9.3g" %(rec[14]))
                self.__writ_tableau(rec[16], stream_, "%10.3f %9.3f")
            elif rec[10]=='time_critical':
                stream_.write("%9.3g %9.3g %10d" %(rec[11:14]))
                stream_.write("%9.3g" %(rec[15]))
                self.__writ_tableau(rec[16], stream_, "%10.3f %9.3f")
        if (len(res)>0): stream_.write("\n")
        return

    def __exportlink_network_overflow(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["network_overflow"]["group_hydra_no"]   #6
        srequete = """
            select id, name, up, down, z_overflow, area
            from %s.network_overflow_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison NOVFL")
        for i, (id_, name, up, down, z_overflow, area) in enumerate(res):
            self.log.notice("Exporting link network overflow: {}".format(name))
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, up, down, name))
            stream_.write("%10.3f %9.2f\n" % (
                z_overflow, area))
        if (len(res)>0): stream_.write("\n")
        return


    #4.3.17
    def __exportlink_fuse_spillway(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["fuse_spillway"]["group_hydra_no"]   #7
        srequete = """
            select id, name, up, down, break_mode, z_invert, width, cc, z_break, t_break, grp, dt_fracw_array
            from %s.fuse_spillway_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LDVF")
        for i, (id_, name, up, down, break_mode, z_invert, width, cc, z_break, t_break, grp, dt_fracw_array) in enumerate(res):
            self.log.notice("Exporting link fuse spillway: {}".format(name))
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, up, down, name))
            #@todo: cas grp=null à véfifier
            #                                                   zs,     width,  cc ,break_mode,z_break,igroup
            stream_.write("%10.3f %9.2f %9.3f %9d %9.3f %9d " % (z_invert, width, cc,
                self.__instances["fuse_spillway_break_mode"][break_mode]["id"], z_break if break_mode =='zw_critical' else t_break, grp))
            self.__writ_tableau(dt_fracw_array, stream_, "%10.3f %9.3f")
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.18
    def __exportlink_street(self, nommodel, stream_):
        itypehydra=self.__instances["link_type"]["street"]["group_hydra_no"]   #21
        coment="Liaison LRUE"
        res = self.__project.execute("""
            select id, name, up, down, width, length, rk
            from %s.street_link
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, coment)
        for i, (id_, name, up, down, width, length, rk) in enumerate(res):
            self.log.notice("Exporting link street: {}".format(name))
            stream_.write("%5d %10d %10d    '%-17s'\n" %(i+1, up, down, name))
            stream_.write("%9.2f %9.2f %9.3f\n" % (width, length, rk))
        if (len(res)>0): stream_.write("\n")
        return

    # ------------------------------------------------------------------------------------
    # export functions for BOUNDARY CONDITIONS
    # ------------------------------------------------------------------------------------

    #4.3.20
    def __exportbc_weir(self, nommodel, stream_):
        itypehydra=self.__instances["singularity_type"]["weir_bc"]["group_hydra_no"]
        res = self.__project.execute("""
            select node, name, z_weir, width, cc
            from %s.weir_bc_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element QDL1")
        for i, rec in enumerate(res):
            self.log.notice("Exporting bc weir: {}".format(rec[1]))
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write(" %9.3f %9.3f %9.3f\n" % (rec[2],rec[3],rec[4]))
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.21
    def __exportbc_strickler(self, nommodel, stream_):
        itypehydra=self.__instances["singularity_type"]["strickler_bc"]["group_hydra_no"]
        res = self.__project.execute("""
            select node, name, slope, k, width
            from %s.strickler_bc_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element LCRK (CL Strickler)")
        for i, rec in enumerate(res):
            self.log.notice("Exporting bc strickler: {}".format(rec[1]))
            mode=0
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%5d %9.6f %9.2f %9.2f\n" % (mode,rec[2],rec[3],rec[4]))
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.22
    def __exportbc_zq(self, nommodel, stream_):
        itypehydra=self.__instances["singularity_type"]["zq_bc"]["group_hydra_no"]
        res = self.__project.execute("""
            select node, name, zq_array
            from %s.zq_bc_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, 10, "Element LCLQZ")
        for i, rec in enumerate(res):
            self.log.notice("Exporting bc zq: {}".format(rec[1]))
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            self.__writ_tableau(rec[2], stream_, "%14.3f %14.3f")
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.23
    def __exportbc_tz(self, nommodel, stream_):
        itypehydra=self.__instances["singularity_type"]["tz_bc"]["group_hydra_no"]
        res = self.__project.execute("""
            select node, name, cyclic, tz_array
            from %s.tz_bc_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, 0, "Element LCLZT")
        for i, rec in enumerate(res):
            self.log.notice("Exporting bc tz: {}".format(rec[1]))
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            icyclic=0
            if (rec[2]): icyclic=1
            stream_.write("%5d \n" %(icyclic))
            # self.__writ_tableau(rec[3], stream_, "%14.3f %14.3f")
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.24
    def __exportbc_froude(self, nommodel, stream_):
        itypehydra=self.__instances["singularity_type"]["froude_bc"]["group_hydra_no"]
        res = self.__project.execute("""
            select node, name
            from %s.froude_bc_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element LCLZF (CL Froude)")
        for i, rec in enumerate(res):
            self.log.notice("Exporting bc froude: {}".format(rec[1]))
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.25
    def __exportbc_tank(self, nommodel, stream_):
        itypehydra=self.__instances["singularity_type"]["tank_bc"]["group_hydra_no"]
        res = self.__project.execute("""
            select node, name, zs_array, zini
            from %s.tank_bc_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element LCLBO")
        for i, rec in enumerate(res):
            self.log.notice("Exporting bc tank: {}".format(rec[1]))
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%10.3f" % (rec[3]))
            self.__writ_tableau(rec[2], stream_, "%14.3f %14.3f")
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.26
    def __exportbc_hydrograph(self, nommodel, stream_):
        itypehydra=self.__instances["singularity_type"]["hydrograph_bc"]["group_hydra_no"]
        res = self.__project.execute("""
            select node, name
            from %s.hydrograph_bc_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element LCLQT (Hydrogrammes)")
        for i, rec in enumerate(res):
            self.log.notice("Exporting bc hydrograph: {}".format(rec[1]))
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.27
    def __exportbc_constant_inflow(self, nommodel, stream_):
        itypehydra=self.__instances["singularity_type"]["constant_inflow_bc"]["group_hydra_no"]
        res = self.__project.execute("""
            select node, name, q0
            from %s.constant_inflow_bc_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element LQCON")
        for i, rec in enumerate(res):
            self.log.notice("Exporting bc constant inflow: {}".format(rec[1]))
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%10.4f\n" % (rec[2]))
        if (len(res)>0): stream_.write("\n")
        return

    #4.3.29    modif TL le 04/07/2017   : l'objet MRKB est exporté désormais comme un élément BC
    def __exportbc_branch_marker(self, nommodel, stream_):
        itypehydra=self.__instances["singularity_type"]["pipe_branch_marker"]["group_hydra_no"]
        res = self.__project.execute("""
            select id, name, node
            from %s.pipe_branch_marker_singularity
            order by name asc
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Liaison LTRNS(MRKP)")
        for i, rec in enumerate(res):
            self.log.notice("Exporting bc MRKB: {}".format(rec[1]))
            mode=0
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[2], rec[1]))
        if (len(res)>0): stream_.write("\n")
        return

     #4.3.30    modif TL le 04/07/2017   : l'objet MRKP est exporté désormais comme un élément BC
    def __exportbc_marker(self, nommodel, stream_):
        itypehydra=self.__instances["singularity_type"]["marker"]["group_hydra_no"]
        res = self.__project.execute("""
            select id, name, node
            from %s.marker_singularity
            order by name asc
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Liaison LTRNS(MRKP)")
        for i, rec in enumerate(res):
            self.log.notice("Exporting bc MRKP: {}".format(rec[1]))
            mode=0
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[2], rec[1]))
        if (len(res)>0): stream_.write("\n")
        return

    # ------------------------------------------------------------------------------------
    # export functions for 1D domains
    # ------------------------------------------------------------------------------------

    #4.4.1   modif TL  3 mars 2017  + modif TL  01 mai 2017 : ajout du recnod(17) en écriture
    def __export_branch(self, name_model, stream1):
        res = self.__project.execute("""
            select id, name, pk0_km, dx
            from %s.branch
            order by id
            """ % (name_model)).fetchall()
        for rec in res:
            self.log.notice("Exporting branch: {}".format(rec[1]))
            stream1.write("$\n'%s' %12.5f %9.2f\n" % (rec[1],rec[2],rec[3]))
            #          0     1     2       3              4                 5                  6                           7          8         9     10                    11                  12                  13                  14                      15          16          17
            resnod = self.__project.execute("""
                select p.id, p.up, p.down, p.z_invert_up, p.z_invert_down, p.custom_length, ST_Length(p.geom), hydra.cross_section_type.id, p.h_sable, p.branch, p.rk, p.cross_section_type, p.circular_diameter, p.ovoid_height, p.ovoid_top_diameter, p.ovoid_invert_diameter, p.cp_geom, p.op_geom,  p.name
                from {model}.pipe_link as p
                join {model}.branch as b on p.branch=b.id
                join hydra.cross_section_type on (hydra.cross_section_type.name = p.cross_section_type)
                where p.branch={b}
                order by st_linelocatepoint(b.geom, st_startpoint(p.geom))
                """.format(model=name_model,b=rec[0])).fetchall()

            for recnod in resnod:
                length = recnod[5] if recnod[5] is not None else recnod[6]
                stream1.write("%10d %10d %9.3f %9.3f %9.2f %4d %9.3f  %s \n" % (recnod[1], recnod[2], recnod[3], recnod[4], length, recnod[7], recnod[8],recnod[18]    ))
                if recnod[7]==self.__instances["cross_section_type"]['circular']["group_hydra_no"]:
                    stream1.write("%9.3f %9.3f\n" % (recnod[12], recnod[10]))
                elif recnod[7]==self.__instances["cross_section_type"]['ovoid']["group_hydra_no"]:
                    stream1.write("%9.3f %9.3f %9.3f %9.3f\n" % (recnod[13], recnod[14], recnod[15], recnod[10]))
                elif recnod[7]==self.__instances["cross_section_type"]['pipe']["group_hydra_no"]:
                    stream1.write("%9.3f %4d\n" % (recnod[16], recnod[10]))
                elif recnod[7]==self.__instances["cross_section_type"]['channel']["group_hydra_no"]:
                    stream1.write("%9.3f %4d\n" % (recnod[17], recnod[10]))
            stream1.write("\n")
        stream1.close()
        return

    #4.4.2
    def __export_reach(self, name_model, stream1):
        res = self.__project.execute("""
            select id, name, pk0_km, dx
            from %s.reach
            order by id
            """ % (name_model)).fetchall()

        for rec in res:
            self.log.notice("Exporting reach: {}".format(rec[1]))
            stream1.write("$\n'%s' %12.5f %9.2f\n" % (rec[1],rec[2],rec[3]))
            resnod = self.__project.execute("""
                select id, pk_km
                from %s.river_node
                where reach=%d
                order by pk_km
                """ % (name_model,rec[0])).fetchall()

            for recnod in resnod:
                stream1.write("%10d%12.5f\n" % (recnod[0],recnod[1]))
            stream1.write("\n")
        stream1.close()
        return

    #4.4.3
    def __export_pt(self, name_model, stream1):
        #          0    1            2                    3                     4            5
        res = self.__project.execute("""
            select id, name, type_cross_section_up, type_cross_section_down, z_invert_up, z_invert_down,
            up_rk, up_rk_maj, up_sinuosity, up_circular_diameter, up_ovoid_height, up_ovoid_top_diameter, up_ovoid_invert_diameter, up_cp_geom, up_op_geom, up_vcs_geom,
            down_rk, down_rk_maj, down_sinuosity, down_circular_diameter, down_ovoid_height, down_ovoid_top_diameter, down_ovoid_invert_diameter, down_cp_geom, down_op_geom, down_vcs_geom
            from %s.river_cross_section_profile
            order by id
            """ % (name_model)).fetchall()

        for rec in res:
            self.log.notice("Exporting river profile: {}".format(rec[1]))
            it_csup=self.__instances["cross_section_type"][rec[2]]["group_hydra_no"] if rec[2]!=None else 0
            it_csdo=self.__instances["cross_section_type"][rec[3]]["group_hydra_no"] if rec[3]!=None else 0
            zup=rec[4] if rec[4]!=None else -999. #@todo: à vérifier avec TL
            zdo=rec[5] if rec[5]!=None else -999. #@todo: à vérifier avec TL
            stream1.write("%10d %4d %4d %9.3f %9.3f\n" % (rec[0],it_csup,it_csdo,zup,zdo))
            # up
            if rec[2]==None:
                stream1.write("%4d\n" % (0))
            elif rec[2]=='circular':
                stream1.write("%9.3f %9.3f\n" %(rec[9], rec[6]))
            elif rec[2]=='ovoid':
                stream1.write("%9.3f %9.3f %9.3f %9.3f\n" % (rec[10], rec[11], rec[12], rec[6]))
            elif rec[2]=='pipe':
                stream1.write("%4d %9.3f\n" % (rec[13], rec[6]))
            elif rec[2]=='channel':
                stream1.write("%4d %9.3f\n" % (rec[14], rec[6]))
            elif rec[2]=='valley':
                stream1.write("%4d %9.3f  %9.3f %9.3f\n" % (rec[15], rec[6], rec[7], rec[8]))   # corr TL
            # down
            if rec[3]==None:
                stream1.write("0\n")
            elif rec[3]=='circular':
                stream1.write("%9.3f %9.3f\n" % (rec[19], rec[16]))
            elif rec[3]=='ovoid':
                stream1.write("%9.3f %9.3f %9.3f %9.3f\n" % (rec[20], rec[21], rec[22], rec[16]))
            elif rec[3]=='pipe':
                stream1.write("%4d %9.3f\n" % (rec[23], rec[16]))
            elif rec[3]=='channel':
                stream1.write("%4d %9.3f\n" % (rec[24], rec[16]))
            elif rec[3]=='valley':
                stream1.write("%4d %9.3f %9.3f %9.3f\n" % (rec[25], rec[16], rec[17], rec[18]))   # corr TL
            stream1.write("\n")    # corr TL
        stream1.close()
        return
    #4.4.4 : cf 3.3 (sections)

    #4.4.6
    def __export_sup(self, name_model, node_user_max, streamtmp, stream1):
        streamtmp.seek(0)

        # bloc *NUMNP
        self.log.notice("Exporting *NUMNP")
        stream1.write("*NUMNP\n")
        stream1.write("%10d\n\n" % (node_user_max))

        # bloc *SING
        self.log.notice("Exporting *SING")
        stream1.write("*SING\n")
        str = streamtmp.readline()
        while str != "":
            stream1.write("%s" % str)
            str = streamtmp.readline()
        stream1.write("\n")

        # bloc *JUNCTION
        self.log.notice("Exporting *JUNCTION")
        stream1.write("*JUNCTION\n")
        res = self.__project.execute("""
            select id, z_ground, ST_X(geom), ST_Y(geom)
            from  %s.manhole_node
            order by id
            """ % (name_model)).fetchall()
        for rec in res:
            npipe_on_downstream = len(self.__project.execute("""
                select  id  from  %s.pipe_link
                where  up=%d
                """ % (name_model,rec[0])).fetchall())
            npipe_on_upstream = len(self.__project.execute("""
                select  id
                from  %s.pipe_link
                where  down=%d
                """ % (name_model,rec[0])).fetchall())
            if (npipe_on_downstream>=2 or npipe_on_upstream>=2): stream1.write("%10d %9.4f %19.6f %19.6f\n"%(rec[0],rec[1],rec[2],rec[3]))
        stream1.write("\n")
        stream1.close()
        return
    # ------------------------------------------------------------------------------------
    def __execute_program(self, name_exe):
        import subprocess
        subprocess.call(name_exe,shell=True)
        i=name_exe.find(".")
        cprogram=name_exe[0:i]
        if (cprogram=="whydram24_dos"):
            cprogram="whydram24"
        i=self.__get_err_status(cprogram)
        if (i==1): self.__display_log_file(cprogram)
        return i

    def __get_err_status(self, cprogram):
    # returned value: 0 : process "cprogram" was executed successfully
    #                 1 : an error occured during execution of "cprogram"
    #                 -1 : file "cprogram".err was not found, meaning process "cprogram" has not been executed
        try:
            with open(cprogram+".err","r") as f:
                c=f.readline()
            f.close()
        except IOError:
            return -1
        return int(c)

    def __display_log_file(self, cprogram):
        #@todo
        return
    # ------------------------------------------------------------------------------------

    def __exportmodel(self, nomscenario, model, directory):
        """
        export module for calculation
        nomscenario: name of a scenario
        model: name of a model
        """
        self.__node_max=0
        self.__icount_hydrol=0
        nomfichier=os.path.join(directory, nomscenario+"_"+model)
        #
        #--- export for hydrologic comptuations (files .LI1  .LI2)
        #
        with open(nomfichier+"_hydrol.li1","w") as streamfileLI1, \
             open(nomfichier+"_hydrol.li2","wb") as streamfileLI2:
            self.__exporthydrol_manhole(model, streamfileLI1, streamfileLI2)  #3.2.1
            self.__exporthydrol_catchment(model, streamfileLI1, streamfileLI2)
            self.__exporthydrol_routage(model, streamfileLI1, streamfileLI2)
            self.__exporthydrol_translation(model, streamfileLI1, streamfileLI2)
            self.__exporthydrol_qqsplit(model, streamfileLI1, streamfileLI2)   #3.2.5
            self.__exporthydrol_zqsplit(model, streamfileLI1, streamfileLI2)   #3.2.6
            self.__exporthydrol_rs(model, streamfileLI1, streamfileLI2)   #3.2.7
            self.__exporthydrol_rsp(model, streamfileLI1, streamfileLI2)   #3.2.8
            self.__exporthydrol_bcsingularity(model, streamfileLI1, streamfileLI2)
            self.__exporthydrol_pipe(model, streamfileLI1, streamfileLI2) #3.2.10
            self.__exporthydrol_hydrograph_bc(model, streamfileLI1, streamfileLI2) #3.2.11
            self.__exporthydrol_tz_bc(model, streamfileLI1, streamfileLI2) #3.2.12
            self.__exporthydrol_marker(model, streamfileLI1, streamfileLI2) #3.2.13 #TL 20180318
        #
        #--- export for hydrologic comptuations (files .EL  .LI)
        #
        nomfichie=os.path.join(directory, nomscenario+"_"+model)

        with open(nomfichier+"_sing.tmp","w+") as streamtmp, \
             open(nomfichier+".el","w") as streamfichierEL,\
             open(nomfichier+".li","w") as streamfichierLI:
            # note _sing.tmp : tempory file for singularities with additional node

            #
            #--- Tables CONTAINER: writing in file .EL
            self.__exportnode_station(model,streamfichierEL)   #4.2.1
            self.__exportnode_storage(model,streamfichierEL,"storage_node","CAS") # export des casiers
            self.__exportnode_2d(model,streamfichierEL)
            self.__exportnode_crossroad(model,streamfichierEL)
            #
            #--- Tables NODES 1D: writing in file .LI
            self.__exportnode_network(model,streamfichierLI,"manhole","Liaison LCLNOD manhole_node")  #4.3.1
            self.__exportnode_network(model,streamfichierLI,"river","Liaison LCLNOD river_node")
            node_user_max=self.__node_max
            #
            #--- Tables SINGULARITIES: writing in file .EL
            self.__exportsing_gate(model, streamfichierEL, streamtmp)  #4.2.6
            self.__exportsing_param(model, streamfichierEL, streamtmp,"hydraulic_cut_singularity","qz_array","CPR","ECPR")    #4.2.7
            self.__exportsing_borda_headloss(model, streamfichierEL, streamtmp)  #4.2.8
            self.__exportsing_param(model, streamfichierEL, streamtmp,"param_headloss_singularity","q_dz_array","DH","ESPRM") #4.2.9
            self.__exportsing_zregul_weir(model, streamfichierEL, streamtmp)  #4.2.10
            self.__exportsing_regul_gate(model, streamfichierEL, streamtmp)  #4.2.11
            self.__exportsing_bradley_headloss(model, streamfichierEL, streamtmp) #4.2.13
            self.__exportsing_bridge_headloss(model, streamfichierEL, streamtmp) #4.2.15
            #
            #--- Tables LINKS: (fichier .LI) : liaisons
            self.__exportlink_derivation_pump(model, streamfichierLI)
            self.__exportlink_weir(model, streamfichierLI) #4.3.6
            self.__exportlink_gate(model, streamfichierLI)
            self.__exportlink_pump(model, streamfichierLI)
            self.__exportlink_regul_gate(model, streamfichierLI)
            self.__exportlink_connector(model, streamfichierLI)  #4.3.11
            self.__exportlink_borda_headloss(model, streamfichierLI)
            self.__exportlink_2d_mesh(model, streamfichierLI)
            self.__exportlink_strickler(model, streamfichierLI)
            self.__exportlink_porous(model, streamfichierLI) #4.3.15
            self.__exportlink_overflow(model, streamfichierLI)
            self.__exportlink_network_overflow(model, streamfichierLI)
            self.__exportlink_fuse_spillway(model, streamfichierLI)
            self.__exportlink_street(model, streamfichierLI)
            self.__exportbc_weir(model, streamfichierLI)   #4.3.20
            self.__exportbc_strickler(model, streamfichierLI)   #4.3.21
            self.__exportbc_zq(model, streamfichierLI)
            self.__exportbc_tz(model, streamfichierLI)
            self.__exportbc_froude(model, streamfichierLI)      #4.3.24
            self.__exportbc_tank(model, streamfichierLI)        #4.3.25
            self.__exportbc_hydrograph(model, streamfichierLI)
            self.__exportbc_constant_inflow(model, streamfichierLI)
            self.__exportbc_branch_marker(model, streamfichierLI)             #4.2.12
            self.__exportbc_marker(model, streamfichierLI)             #4.2.14
            #
            #--- specific files for 1D-domains
            self.__export_section(model, open(nomfichier+"_geom_section.dat","wb"))
            self.__export_branch(model, open(nomfichier+"_branch.dat", "w"))
            self.__export_reach(model, open(nomfichier+"_reach.dat", "w"))
            self.__export_pt(model, open(nomfichier+"_pt.dat", "w"))
            self.__export_sup(model, node_user_max, streamtmp, open(nomfichier+"_sup.dat", "w"))


        # export geom.dat
        with open(nomfichier+"_geom.dat","w") as f:
            for res in self.__project.execute("""
                    select {model}.geom_dat('reach', id) from {model}.reach
                    union
                    select {model}.geom_dat('branch', id) from {model}.branch
                    union
                    select {model}.geom_dat('2d', id) from {model}.coverage where domain_type='2d'
                    union
                    select {model}.geom_dat('street', id) from {model}.coverage where domain_type='street'
                    union
                    select {model}.geom_dat('storage', id) from {model}.coverage where domain_type='storage'
                    union
                    select {model}.geom_dat('crossroad', id) from {model}.coverage where domain_type='crossroad'
                    """.format(model=model)):
                f.write(res[0])

        # export link.dat
        with open(nomfichier+"_link.dat","w") as f:
            for id, name in self.__project.execute("""
                    select id, UPPER(name)
                    from {model}._link
                    order by UPPER(name) asc
                    """.format(model=model)):
                f.write("{} {}\n".format(id, name))

        # export ctrz.dat
        with open(nomfichier+"_ctrz.dat","w") as f:
            for res in self.__project.execute("""
                    select {model}.ctrz_dat()
                    """.format(model=model)):
                f.write(res[0])

        # export l2d.dat
        with open(nomfichier+"_l2d.dat","w") as f:
            for name, xa, ya, xb, yb in self.__project.execute("""
                    select name, xa, ya, xb, yb from {model}.l2d
                    """.format(model=model)):
                f.write("{} {} {} {} {}\n".format(name, xa, ya, xb, yb))

    def __exportrac(self, nomscenario, directory):
        nomfichier=os.path.join(directory, nomscenario)

        with open(nomfichier+".rac","w") as streamfileRAC:
            for model in self.model_to_export:
                self.__export_model_connections(model, streamfileRAC)  #2.7

    def export(self, id_scenario):
        """
        export module for calculation: main function
        id_scenario: id of a scenario
        """

        try:
            if not isinstance(id_scenario, int):
                id_scenario, =  self.__project.execute("""
                    select id from project.scenario where name='{}'
                    """.format(id_scenario)).fetchone()

            name_scenario, name_ref_scenario =  self.__project.execute("""
                select s.name, r.name
                from project.scenario as s
                full outer join project.scenario as r on s.scenario_ref=r.id
                where s.id={}
                """.format(id_scenario)).fetchone()
        except TypeError:
            self.log.error("No scenario (id:{})".format(id_scenario))

        # If reference scenario :
        #  - Don't write file .rac,
        #  - Don't write files .LI1/.LI2,
        #  - Don't write files _sing.tmp, .el, .li, _geom.dat, _link.dat, _ctrz.dat, _l2d.dat
        #    -> No model export (is the same as previous points)
        #  + No topo update (branches, coverages,...)

        #  create directories if needed
        cur_dir = os.getcwd()
        os.chdir(os.path.dirname(__file__))
        directory_scenario = self.__project.get_senario_path(id_scenario)
        directory_calculation = os.path.join(directory_scenario, "travail")
        directory_work = os.path.join(directory_scenario, "hydraulique")
        directory_hydrol = os.path.join(directory_scenario, "hydrol")
        directory_carto = os.path.join(directory_scenario, "carto")

        self.__init_folder(directory_scenario)
        self.__init_folder(directory_calculation)
        self.__init_folder(directory_work)
        self.__init_folder(directory_hydrol)
        self.__init_folder(directory_carto)

        # Export settings files (Génération des fichiers de paramétrages - note NT1 §2)
        self.model_to_export = self.__exportsettings_scen(id_scenario, name_scenario, name_ref_scenario, open(os.path.join(directory_scenario, "scenario.nom"), "w"))
        # Export scenario files files
        graphic_control=self.__exportsettings_gen(id_scenario, name_scenario,
            open(os.path.join(directory_calculation, name_scenario+".cmd"), "w"),
            open(os.path.join(directory_calculation, name_scenario+"_optsor.dat"), "w"))
        self.__exportsettings_dry_inflow(id_scenario, open(os.path.join(directory_calculation, name_scenario+".qts"), "w"))
        self.__exportsettings_rainfall_library(id_scenario, open(os.path.join(directory_calculation, name_scenario+".plu"), "w"))
        if not name_ref_scenario:
            self.__exportrac(name_scenario, directory_calculation)
            for name_model in self.model_to_export:
                try:
                    configs_brut = self.__project.execute("""select configuration from project.config_scenario where scenario={} and model='{}' order by priority desc;""".format(id_scenario, name_model)).fetchall()
                    # Set scenario configurations
                    configs = [string for (string,) in configs_brut]
                    # Switch to default config
                    self.__project.execute("""update {}.metadata set configuration=1""".format(name_model))
                    for config in configs:
                        # For each config, highest (1) priority last, unpack configured items that exists in this config
                        self.__project.execute("""select {}.unpack_config({});""".format(name_model, config))
                    # Topo: update coverages
                    self.__project.execute("""
                        select {}.coverage_update()
                        """.format(name_model))
                    # Topo: update branches
                    self.__project.execute("""
                        select {}.branch_update_fct()
                        """.format(name_model))
                    # Export model files
                    self.__exportmodel(name_scenario, name_model, directory_calculation)
                    self.log.notice("Model {} exported to {}".format(name_model, directory_scenario))
                    # Switch back to top-most config (or default)
                    self.__project.execute("""update {}.metadata set configuration={}""".format(name_model, configs[-1] if configs else 1))
                except Exception:
                    # Switch to default config
                    self.__project.execute("""update {}.metadata set configuration=1""".format(name_model))
                    self.log.error("Error exporting scenario: {} / model: {}.".format(name_scenario, name_model), "File {}: {}".format(os.path.basename(sys.exc_info()[2].tb_frame.f_code.co_filename), sys.exc_info()[0].__name__))

        os.chdir(cur_dir)
    # ------------------------------------------------------------------------------------

    def __init_folder(self, folder):
        if not os.path.isdir(folder):
            os.mkdir(folder)
        else:
            for file in os.listdir(folder):
                file_path = os.path.join(folder, file)
                if os.path.isfile(file_path):
                    try:
                        os.remove(file_path)
                    except:
                        self.log.error("Error: cannot remove file {f} from directory {dir}".format(f=os.path.basename(file_path), dir=os.path.dirname(file_path)))
# ------------------------------------------------------------------------------------
# script de test
# ------------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    import sys
    import time
    import getopt
    import tempfile
    from hydra.project import Project
    #import export_carto_data
    from export_carto_data import ExportCartoData

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "htd",
                ["help", "timing", "debug"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    project = args[0]
    obj_project = Project.load_project(project)

    if len(args) == 2:
        try:
            idscn = int(args[-1])
        except ValueError:
            idscn = args[1]
    else:
        idscn = obj_project.add_new_scenario()
        obj_project.commit()



    timing = "-t" in optlist or "--timing" in optlist
    debug = "-d" in optlist or "--debug" in optlist

    exporter = ExportCalcul(obj_project)
    exporter.export(int(idscn))

    exporter = ExportCartoData(obj_project)
    exporter.export(int(idscn))