# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
hydra module to handle database

USAGE
    hydra.py [-h, -t, -d] project_name SRID [model]

    where conninfo are options to be passed to connect to the database
    and model is the model name

OPTIONS
    -d, --debug
        print sql statements

    -t, --timing
        print timing

    -h, --help
        print this help

"""

from __future__ import absolute_import # important to read the doc !
import os
import json
import string
import psycopg2
from psycopg2.extras import LoggingConnection
import logging
import shutil
from ..utility.string import normalized_name
from ..utility.settings_properties import SettingsProperties

__currendir = os.path.dirname(__file__)
__version_history__ = json.load(open(os.path.join(os.path.dirname(__currendir), "versions", "version_history.json")))
__version__ =  __version_history__["current_version"]
__hydra_path = os.path.join(os.path.expanduser("~"), ".hydra")

# this is the database version the plugin is meant to work with
def data_version():
    return __version__

def target_version(version):
    assert version in __version_history__.keys()
    return __version_history__[version]["target"]

class ProjectCreationError(Exception):
    pass

class TestProject(object):
    "create a project from template the project contains one model named model"

    NAME = "template_project"
    WORKINGDIR = os.path.join(os.path.expanduser("~"), ".hydra")

    @staticmethod
    def reset_template(debug=False):
        if project_exists(TestProject.NAME):
            remove_project(TestProject.NAME)
        create_project(TestProject.WORKINGDIR, TestProject.NAME, 2154, debug)
        db = SettingsProperties.get_db()
        pg = SettingsProperties.get_postgre_settings()
        con = psycopg2.connect(database="postgres", host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
        con.set_isolation_level(0)
        cur = con.cursor()
        # close all remaining opened connection to this database if any
        cur.execute("select pg_terminate_backend(pg_stat_activity.pid) \
                    from pg_stat_activity \
                    where pg_stat_activity.datname = '%s'"%(TestProject.NAME))
        con.close()

    def __init__(self, name, debug=False, with_model=False):
        if not project_exists(TestProject.NAME):
            self.reset_template()
        assert name[-5:] == "_test"
        self.__name = name
        self.__debug = debug
        db = SettingsProperties.get_db()
        pg = SettingsProperties.get_postgre_settings()
        con = psycopg2.connect(database="postgres", host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
        con.set_isolation_level(0)
        cur = con.cursor()
        pd = project_dir(TestProject.WORKINGDIR, name)

        if not os.path.exists(pd):
            clean_project_dir(pd)

        # close all remaining opened connection to this database if any
        cur.execute("select pg_terminate_backend(pg_stat_activity.pid) \
                    from pg_stat_activity \
                    where pg_stat_activity.datname in ('%s')"%(name))
        cur.execute("drop database if exists %s"%(name))
        cur.execute("create database {} with template {}".format(name, TestProject.NAME))
        con.commit()
        con.close()

        if with_model:
            con = psycopg2.connect(database=name, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
            cur = con.cursor()
            create_model(cur, "model")
            con.commit()
            con.close()

        if not os.path.exists(pd):
            os.mkdir(pd)

    def __del__(self):
        if not self.__debug:
            remove_project(self.__name)

def project_dir(working_dir, project_name):
    """returns directory associated with project"""
    assert project_name == normalized_name(project_name)
    return os.path.join(working_dir, project_name)

def project_file_path(working_dir, project_name):
    """returns path of the project file"""
    assert project_name == normalized_name(project_name)
    return os.path.join(working_dir, project_name, ".qgs")

def create_project(working_dir, project_name, srid, debug=False, version=__version__):
    """create project database and directory"""
    # @todo add user identification
    assert project_name == normalized_name(project_name)

    if os.path.exists(project_file_path(working_dir, project_name)):
        raise ProjectCreationError("File {} already exists. Cannot create project {}".format(project_file_path(working_dir, project_name), project_name))
    if project_exists(project_name):
        raise ProjectCreationError("A database named {} already exists. Use another name for the project.".format(project_name))

    if not SettingsProperties.local():
        working_dir = "/$user/.hydra"

    db = SettingsProperties.get_db()
    pg = SettingsProperties.get_postgre_settings()
    # connects to postgres DB
    con = psycopg2.connect(database="postgres", host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
    con.set_isolation_level(0)
    cur = con.cursor()
    # creates project DB
    cur.execute("create database %s"%(project_name))
    if not os.path.exists(project_dir(working_dir, project_name)) and SettingsProperties.local():
        os.makedirs(project_dir(working_dir, project_name))
    con.commit()
    con.close()
    # /!\ change connection to project DB
    con = psycopg2.connect(database=project_name, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'], connection_factory=LoggingConnection)
    logging.basicConfig(level=logging.ERROR if not debug else logging.DEBUG)
    logger = logging.getLogger(__name__)
    con.initialize(logger)
    cur = con.cursor()
    cur.execute("create extension postgis")
    cur.execute("create extension plpython3u")
    cur.execute("create extension hydra with version '{}'".format(version))
    cur.execute("insert into hydra.metadata(srid, workspace) values({}, '{}')".format(srid, working_dir))
    con.commit()
    con.close()

def get_project_models_list(cur):
    "return a list of models in the project"
    cur.execute("select name from project.model")
    models = cur.fetchall()
    result = []
    for model in models:
        result.append(model[0])
    return result

def is_hydra_db(dbname):
    #if dbname[-5:] == '_test':
    #    return False
    try:
        db = SettingsProperties.get_db()
        pg = SettingsProperties.get_postgre_settings()
        con = psycopg2.connect(database=dbname, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
        cur = con.cursor()
        cur.execute("select count(1) from pg_catalog.pg_available_extensions where name='hydra' and installed_version is not null")
        res = cur.fetchone()[0] == 1
        con.close()
        return res
    except psycopg2.OperationalError as e:
        return False

def get_projects_list():
    db = SettingsProperties.get_db()
    pg = SettingsProperties.get_postgre_settings()
    con = psycopg2.connect(database="postgres", host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
    cur = con.cursor()
    cur.execute("select datname from pg_database where datistemplate=false")
    databases = cur.fetchall()
    result = []
    con.close()
    for database in databases:
        if is_hydra_db(database[0]):
            result.append(database[0])
    return result

def get_available_hydra_versions():
    db = SettingsProperties.get_db()
    pg = SettingsProperties.get_postgre_settings()
    con = psycopg2.connect(database="postgres", host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
    cur = con.cursor()
    cur.execute("select version from pg_available_extension_versions where name='hydra'")
    versions = cur.fetchall()
    result = []
    con.close()
    for version in versions:
        result.append(version[0])
    return result

def project_exists(project_name):
    # @todo add user identification
    assert project_name == normalized_name(project_name)
    db = SettingsProperties.get_db()
    pg = SettingsProperties.get_postgre_settings()
    con = psycopg2.connect(database="postgres", host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
    cur = con.cursor()
    cur.execute("select count(1) from pg_database where datname='{}'".format(project_name))
    res = cur.fetchone()[0] == 1
    con.close()
    return res

def get_project_metadata(project_name):
    # @todo add user identification
    assert is_hydra_db(project_name)
    assert project_name == normalized_name(project_name)
    db = SettingsProperties.get_db()
    pg = SettingsProperties.get_postgre_settings()
    con = psycopg2.connect(database=project_name, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
    cur = con.cursor()
    cur.execute("select srid, version, workspace, creation_date from hydra.metadata")
    res = cur.fetchone()
    con.close()
    # returns [srid, version, workspace, creation_date]
    return res

def change_project_workspace(project_name, workspace):
    # @todo add user identification
    assert is_hydra_db(project_name)
    assert project_name == normalized_name(project_name)
    db = SettingsProperties.get_db()
    pg = SettingsProperties.get_postgre_settings()
    con = psycopg2.connect(database=project_name, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
    cur = con.cursor()

    cur.execute("update hydra.metadata set workspace='{}'".format(workspace))

    con.commit()
    con.close()

def remove_project(project_name):
    # @todo add user identification
    assert project_name == normalized_name(project_name)
    db = SettingsProperties.get_db()
    pg = SettingsProperties.get_postgre_settings()

    con = psycopg2.connect(database="postgres", host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
    con.set_isolation_level(0)
    cur = con.cursor()

    clean_project_dir(project_name)

    # close all remaining opened connection to this database if any
    cur.execute("select pg_terminate_backend(pg_stat_activity.pid) \
                from pg_stat_activity \
                where pg_stat_activity.datname='"+project_name+"';")
    cur.execute("drop database {}".format(project_name))

    con.commit()
    con.close()

def clean_project_dir(project_name):
    try:
        db = SettingsProperties.get_db()
        pg = SettingsProperties.get_postgre_settings()
        con_project = psycopg2.connect(database=project_name, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
        cur_project = con_project.cursor()
        cur_project.get_workspace()
        working_dir, = cur_project.fetchone()
        cur_project.execute("select name from project.scenario")
        scenarios = cur_project.fetchall()
        con_project.close()
        bd_exists = True
    except:
        bd_exists = False

    if bd_exists:
        # if project file still exists, remove it
        if os.path.exists(project_file_path(working_dir, project_name)):
            os.remove(project_file_path(working_dir, project_name))
        if os.path.exists(os.path.join(working_dir, project_name, project_name+".qgs~")):
            os.remove(os.path.join(working_dir, project_name, project_name+".qgs~"))
        # remove all scenarios paths
        for item in scenarios:
            scn_path = os.path.join(working_dir, project_name, item[0])
            if os.path.exists(scn_path):
                shutil.rmtree(scn_path)

        terrain_path = os.path.join(working_dir, project_name, "terrain")
        if os.path.exists(terrain_path) and not os.listdir(terrain_path):
            shutil.rmtree(terrain_path)
        project_dir_path = project_dir(working_dir, project_name)
        if os.path.exists(project_dir_path) and not os.listdir(project_dir_path):
            shutil.rmtree(project_dir_path)

def create_model(cur, model, srid=None, empty=False):
    "create model schema and related tables"

    # cur.execute("select installed_version from pg_catalog.pg_available_extensions where name='hydra'")
    # version, = cur.fetchone()
    # if version != __version__:
        # raise Exception("cannot create model '{}' version %s with project version {}".format(model, __version__, version))

    #uniformize model referenciation
    cur.execute("insert into project.model(name) values('{}')".format(model))
    cur.execute("insert into project.model_config(name,config) values('%s', 1);"%(model))

def remove_model(cur, name):
    cur.execute("drop schema if exists {} cascade".format(name))
    cur.execute("delete from project.model_config where name ='%s';"%(name))


def create_scenario(cur):
    """create scenario, returns its id"""
    cur.execute("""insert into project.scenario default values returning id;""")
    return cur.fetchone()[0]

if __name__ == "__main__":
    import sys
    from psycopg2.extras import LoggingConnection
    import logging
    import time
    import getopt

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "htd",
                ["help", "timing", "debug"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    timing = "-t" in optlist or "--timing" in optlist
    debug = "-d" in optlist or "--debug" in optlist

    start = time.time()
    create_project(args[0], int(args[1]), debug)
    if timing:
        sys.stdout.write("hydra %.2f sec"%(time.time()-start))

    start = time.time()
    db = SettingsProperties.get_db()
    pg = SettingsProperties.get_postgre_settings()
    con = psycopg2.connect(database=args[0], host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
    cur = con.cursor()
    remove_model(cur, args[2])
    create_model(cur, args[2])
    con.commit()
    con.close()
    if timing:
        sys.stdout.write(" %s %.2f sec"%(args[0], time.time()-start))