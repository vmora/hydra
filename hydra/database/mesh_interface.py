# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import numpy
import traceback
from math import log, sqrt
from collections import defaultdict
from shapely.geometry import Polygon, LineString, MultiLineString
from shapely import wkt
from shapely.ops import linemerge
from qgis.core import QGis, QgsDataSourceURI
from PyQt4.QtCore import Qt, QCoreApplication
from PyQt4.QtGui import QApplication, QMessageBox
from hydra.gui.text_info import TextInfo
from hydra.gui.forms.model_constrain import ModelConstrainWidget
from hydra.utility.map_point_tool import create_line, create_point, create_polygon, Snapper, VisibleGeometry, create_segment
from hydra.utility.timer import Timer
from hydra.utility.tables_properties import TablesProperties
from hydra.utility.settings_properties import SettingsProperties

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

def log_info(info, info_form):
    if info_form is not None:
        info_form.textInfos.append(info)
        QApplication.processEvents()


class MeshInterface(object):
    """handles interaction between project, meshing module and qgis interface"""

    def __init__(self, canvas, logger, project, refresh_layers_fct):
        self.__canvas = canvas
        self.__logger = logger
        self.__project = project
        self.__properties = TablesProperties.get_properties()
        self.__refresh_layers = refresh_layers_fct

    def __del_mesh(self, coverage_id):
        model = self.__project.get_current_model().name
        self.__project.execute("""
            delete from {model}._link
            where id in (
                select distinct l.id
                from {model}._link as l, {model}.coverage as c
                where st_intersects(l.geom, c.geom)
                and c.id={coverage_id}
                and l.generated is not null
                )
            """.format(
                model=model,
                coverage_id=coverage_id
                )
            )
        self.__project.execute("""
            delete from {model}.elem_2d_node
            where id in (
                select distinct e.id
                from {model}.elem_2d_node as e, {model}.coverage as c
                where st_intersects(e.geom, c.geom)
                and c.id={coverage_id}
                and e.generated is not null
                )
            """.format(
                model=model,
                coverage_id=coverage_id
                )
            )
        self.__project.commit()

    def __gen_mesh(self, coverage_id):
        model = self.__project.get_current_model().name
        res = self.__project.execute("""
                select {model}.mesh({coverage_id})
                """.format(
                    model=model,
                    coverage_id=coverage_id)).fetchone()
        if res and res[0] > 0:
            self.__logger.notice(tr('generated {} elements').format(res[0]))
            self.__project.commit()
        else:
            self.__project.rollback()
            self.__logger.warning(tr('no coverage here'))

    def create_2d_block_direction(self):
        self.__logger.warning(tr("obsolete, button should be removed"))

    def create_2d_block(self, direction=False):
        self.__logger.warning(tr("obsolete, button should be removed"))

    def create_constrain(self):
        map_per_pix = self.__canvas.getCoordinateTransform().mapUnitsPerPixel()
        snapper = Snapper(10*map_per_pix)
        for x, y in self.__project.execute("""
                with points as (
                    select (st_dumppoints(geom)).geom as geom
                    from {model}.constrain
                    )
                select st_x(geom), st_y(geom) from points
                """.format(model=self.__project.get_current_model().name)).fetchall():
            snapper.add_point((x,y))

        line = create_line(self.__canvas,
                self.__logger, self.__project.terrain(), snapper)#, ['constrain'])
        if line:
            ModelConstrainWidget(self.__project, line).exec_()
            self.__refresh_layers([self.__properties['coverage']['name'],self.__properties['constrain']['name']])

    def delete_mesh(self):
        point = create_point(self.__canvas,
                self.__logger, self.__project.terrain())#, ['constrain'])
        model = self.__project.get_current_model().name
        srid = str(self.__project.srid)
        if point:
            coverage_id, = self.__project.execute("""
                    select c.id from {model}.coverage as c
                    where st_intersects(c.geom, 'SRID={srid}; POINT({x} {y})'::geometry)
                    """.format(
                        model=model,
                        srid=srid,
                        x=point[0],
                        y=point[1]
                        )).fetchone()
            self.__del_mesh(coverage_id)
            self.__refresh_layers()

    def create_mesh(self):
        timer = Timer()

        model = self.__project.get_current_model().name
        active_layer = self.__canvas.currentLayer()
        if active_layer and QgsDataSourceURI(active_layer.source()).table() == 'coverage' \
            and active_layer.selectedFeatureCount() > 0:
            for f in active_layer.selectedFeatures():
                self.__gen_mesh(f['id'])
                self.__refresh_layers()
            return

        point = create_point(self.__canvas,
                self.__logger, self.__project.terrain())#, ['constrain'])
        srid = str(self.__project.srid)
        if point:
            coverage, = self.__project.execute("""
                select c.id
                from {model}.coverage as c
                where st_intersects(c.geom, 'SRID={srid}; POINT({x} {y})'::geometry)
                and c.domain_type='2d'
                """.format(
                    model=model,
                    srid=srid,
                    x=point[0],
                    y=point[1]
                )).fetchone()
            self.__gen_mesh(coverage)
            self.__refresh_layers()

    def mesh_regen(self):
        info_form = TextInfo()
        info_form.show()
        info_form.textInfos.clear()
        log_info(tr('Starting complete mesh regeneration'), info_form)

        model = self.__project.get_current_model().name
        meshed_coverages = [id for (id,) in self.__project.execute("""
                    with elem_2d as (select st_union(geom) as geom from {model}.elem_2d_node)
                    select c.id
                    from {model}.coverage as c, elem_2d as e
                    where st_intersects(e.geom, c.geom)
                    and c.domain_type='2d'
                    order by c.id
                    """.format(
                        model=model
                    )).fetchall()]
        for c in meshed_coverages:
            log_info(tr('Unmeshing coverage: {}'.format(c)), info_form)
            self.__del_mesh(c)

        coverages_to_mesh = [id for (id,) in self.__project.execute("""
                    select c.id
                    from {model}.coverage as c
                    where c.domain_type='2d'
                    order by c.id
                    """.format(
                        model=model
                    )).fetchall()]
        errors=[]
        for cov in coverages_to_mesh:
            try:
                log_info(tr('Meshing coverage: {}'.format(cov)), info_form)
                self.__gen_mesh(cov)
            except:
                errors.append(cov)
                log_info(tr('Error while meshing coverage: {}'.format(cov)), info_form)

        if info_form:
            info_form.close()
        self.__refresh_layers()

        if len(errors)==0:
            QMessageBox(QMessageBox.Information,
                        tr('Mesh finished'),
                        tr("Generated mesh on all coverages"),
                        QMessageBox.Ok).exec_()
        else:
            QMessageBox(QMessageBox.Warning,
                        tr('Mesh finished'),
                        tr("""Errors on coverages: {}
                            """.format(', '.join(map(str, errors)))),
                        QMessageBox.Ok).exec_()

    def mesh_unmeshed(self):
        info_form = TextInfo()
        info_form.show()
        info_form.textInfos.clear()
        log_info(tr('Starting mesh generation on unmeshed coverages'), info_form)

        model = self.__project.get_current_model().name
        unmeshed_coverages = [id for (id,) in self.__project.execute("""
                    with elem_2d as (select st_union(geom) as geom from {model}.elem_2d_node)
                    select c.id
                    from {model}.coverage as c, elem_2d as e
                    where not st_intersects(e.geom, c.geom)
                    and c.domain_type='2d'
                    order by c.id
                    """.format(
                        model=model
                    )).fetchall()]
        errors=[]
        for cov in unmeshed_coverages:
            try:
                log_info(tr('Meshing coverage: {}'.format(cov)), info_form)
                self.__gen_mesh(cov)
            except:
                errors.append(cov)
                log_info(tr('Error while meshing coverage: {}'.format(cov)), info_form)

        if info_form:
            info_form.close()
        self.__refresh_layers()

        if len(errors)==0:
            QMessageBox(QMessageBox.Information,
                        tr('Mesh finished'),
                        tr("""Generated mesh on coverages: {}
                            """.format(', '.join(map(str, unmeshed_coverages)))),
                        QMessageBox.Ok).exec_()
        else:
            QMessageBox(QMessageBox.Warning,
                        tr('Mesh finished'),
                        tr("""Generated mesh on coverages: {}\n
                                Errors on coverages: {}
                            """.format(', '.join(map(str, unmeshed_coverages)),
                                       ', '.join(map(str, errors)))),
                        QMessageBox.Ok).exec_()

    def create_links(self):
        active_layer = self.__canvas.currentLayer()
        model = self.__project.get_current_model().name
        srid = self.__project.srid

        if not active_layer or QgsDataSourceURI(active_layer.source()).table() != 'coverage' or active_layer.selectedFeatureCount() == 0:
            pu = create_point(self.__canvas,
                    self.__logger, self.__project.terrain())
            pd = create_point(self.__canvas,
                    self.__logger, self.__project.terrain())

            # see if selection is boundary constrain + station node:
            precision = SettingsProperties.get_snap()
            boundary_constrain = self.__project.execute("""
                select id
                from {model}.constrain
                where st_dwithin(geom, 'SRID={srid}; POINT({x} {y})'::geometry, {prec})
                    and constrain_type = 'boundary'
                """.format(
                    model=model,
                    srid=srid,
                    x=pu[0],
                    y=pu[1],
                    prec=precision
                    )).fetchone()
            station_node = self.__project.execute("""
                select id
                from {model}.station_node
                where st_dwithin(geom, 'SRID={srid}; POINT({x} {y})'::geometry, {prec})
                """.format(
                    model=model,
                    srid=srid,
                    x=pd[0],
                    y=pd[1],
                    prec=precision
                    )).fetchone()
            if boundary_constrain is not None and station_node is not None:
                self.__project.execute("""
                    select {model}.create_boundary_links({cid}, {nid})
                    """.format(
                        model=model,
                        cid=boundary_constrain[0],
                        nid=station_node[0]
                        ))
                self.__project.commit()
                self.__refresh_layers()
                return

            # else assume we try to create links between 2 coverages
            ids = []
            for p in [pu, pd]:
                res = self.__project.execute("""
                select id
                from {model}.coverage
                where st_intersects(geom, 'SRID={srid}; POINT({x} {y})'::geometry)
                """.format(
                    model=model,
                    srid=srid,
                    x=p[0],
                    y=p[1]
                    )).fetchall()
                if res:
                    ids.append(str(res[0][0]))
        else:
            ids = [str(f['id']) for f in active_layer.selectedFeatures()]

        if len(ids) != 2:
            self.__logger.warning(tr("you must select two domains"))
            return

        self.__project.execute("""
            select {model}.create_links({uid}, {did})
            """.format(
                model=model,
                uid=ids[0],
                did=ids[1]
                ))
        self.__project.commit()
        self.__refresh_layers()


    def create_network_overflow_links(self):
        active_layer = self.__canvas.currentLayer()
        model = self.__project.get_current_model().name
        srid = self.__project.srid

        if not active_layer or QgsDataSourceURI(active_layer.source()).table() != 'coverage' or active_layer.selectedFeatureCount() == 0:
            point = create_point(self.__canvas,
                    self.__logger, self.__project.terrain())

            res = self.__project.execute("""
                select id
                from {model}.coverage
                where st_intersects(geom, 'SRID={srid}; POINT({x} {y})'::geometry)
                """.format(
                    model=model,
                    srid=srid,
                    x=point[0],
                    y=point[1]
                    )).fetchone()
            if res:
                coverage_ids = [str(res[0])]
        else:
            coverage_ids = [str(f['id']) for f in active_layer.selectedFeatures()]

        for id in coverage_ids:
            self.__project.execute("""
                select {model}.create_network_overflow_links({id})
                """.format(
                    model=model,
                    id=id
                    ))

        self.__project.commit()
        self.__refresh_layers()