<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="2.14.18-Essen" minimumScale="0" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <pipe>
    <rasterrenderer opacity="1" alphaBand="-1" classificationMax="1" classificationMinMaxOrigin="User" band="1" classificationMin="0" type="singlebandpseudocolor">
      <rasterTransparency/>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" clip="0">
          <item alpha="0" value="0" label="0 mm" color="#ffffb2"/>
          <item alpha="128" value="0.2" label="20 mm" color="#ffffb2"/>
          <item alpha="128" value="0.4" label="40 mm" color="#fecc5c"/>
          <item alpha="128" value="0.6" label="60 mm" color="#fd8d3c"/>
          <item alpha="128" value="0.8" label="80 mm" color="#f03b20"/>
          <item alpha="128" value="1" label="100 mm" color="#bd0026"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeBlue="128" grayscaleMode="0" saturation="0" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
