# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run all tests

USAGE
    python -m hydra.test [-h, -etest, -c]

OPTIONS
    -h
        print this help

    -e test1,tes2
        exclude test1 and test2

    -c
        do not recompile fortan code (posix only)
"""

import subprocess
import os
import re
from subprocess import Popen, PIPE
from tempfile import gettempdir
from time import time
from multiprocessing.pool import ThreadPool
from .database.database import TestProject
import getopt
import sys

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hj:e:c",
            ["help"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

excludes = optlist['-e'].split(',') if '-e' in optlist else []

if len(excludes):
    print "excluded", ' '.join(excludes)

# test model creation

sys.stdout.write("template database creation... ")
sys.stdout.flush()

debug = "-d" in optlist or "--debug" in optlist

start = time()

sys.stdout.write("creating template database... ")
TestProject.reset_template()
sys.stdout.write("ok\n")

def list_tests():
    "return module names for tests"
    tests = []
    hydra_dir = os.path.abspath(os.path.dirname(__file__))
    top_dir = os.path.dirname(hydra_dir)
    for root, dirs, files in os.walk(hydra_dir):
        for file_ in files:
            if re.match(r".*_test.py$", file_):
                # remove the trailing '.py' (3 characters)
                # replace \ or / by dots
                test = '.'.join(
                            os.path.abspath(
                                os.path.join(root, file_)
                            ).replace(hydra_dir, "hydra").split(os.sep))[:-3]
                if test not in excludes:
                    tests.append(test)
    if 'hydra.docs.build' not in excludes:
        tests += ['hydra.docs.build']

    return tests


def run(test):
    start = time()
    our, err = subprocess.Popen(["python", "-m", test],
            stderr=PIPE,
            stdout=PIPE).communicate()
    if len(err):
        return 1, '%s: %s'%(test, str(err))
    return  0, "%s ran in %.2f sec"%(test, time() - start)

tests = list_tests()
nb_proc = int(optlist['-j']) if '-j' in optlist else len(tests)

if nb_proc > 1:
    print "start %d processes"%(nb_proc)
    i = 0
    for rc, msg in ThreadPool(processes=nb_proc).map(run, tests):
        print "% 3d/%d %s"%(i+1, len(tests), msg)
        if rc != 0:
            exit(1)
        i += 1
else:
    for i, test in enumerate(tests):
        rc, msg = run(test)
        print "% 3d/%d %s"%(i+1, len(tests), msg)


print "everything is fine %d sec"%(int(time() - start))
