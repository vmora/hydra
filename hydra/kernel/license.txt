HYDRA, a QGIS plugin for hydraulics
(see <http://hydra-software.net/>).

Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand
of Setec Hydratec, Paris.

Contact: <contact@hydra-software.net>

You can use this program under the terms of the GNU General Public
License as published by the Free Software Foundation, version 3 of
the License.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

REGARDING THE HYDRA COMPUTATION SUITE (in /hydra/kernel: hydrol.exe,
hydragen.exe, whydram24.exe, whydram24_dos.exe):

The program is the property of SETEC HYDRATEC and is protected by
the legislation pertaining to copyright.

The Licensee is granted the right to use the program, subject to
the conditions specified in the present Licensing Agreement.

By installing the program onto the hard disk of your computer or
another support device, you automatically accept the licensing terms
given hereafter.

The program remains the sole property of SETEC HYDRATEC. The rights
and obligations of the Licensee concerning the use of the program,
are as follows: 

THE LICENSEE IS GRANTED THE FOLLOWING RIGHTS:
•	Use the program on a single computer, if the Licensee has a
    license HYDRA-LOCAL
•	The program can only be installed on a network, if the Licensee
    has acquired a HYDRA-NETWORK ; it can be used by the number of
    instances specified in the License
•	It is possible to permanently transfer the license to a third
    party by making a written request to SETEC HYDRATEC. The transfer
    is subject to approval by SETEC HYDRATEC and is only valid if the
    original Licensee retains no copy of the program and the new Licensee
    expressly accepts the conditions of the present Licensing Agreement
    and Warranties.

THE FOLLOWING RIGHTS ARE NOT GRANTED TO THE LICENSEE:
•	The software and associated documents cannot be duplicated under any
    form, other than for archive purposes.
•	Loan or sublease of all or part of the software, or resale of
    the program is prohibited without the approval of SETEC HYDRATEC.
•	The program cannot be uncompiled, disassembled, modified or translated.
    In addition, it is prohibited to attempt to gain access to the source
    code or create tools derived from the program.

IMPORTANT
All violations of the clauses of the present Licensing Agreement will
result in the annulation of all associated agreements, in particular,
those pertaining to the maintenance and technical support packages. In
this case, the Licensee waives his right for any compensation resulting
from the annulation.

LIMITED WARRANTY

SETEC HYDRATEC does not guarantee that the software will fulfill your needs,
nor that it will function without interruption or without error.

This exclusive warranty replaces all other warranties, either expressed or
implied, including implied warranties of merchandizing and suitability of
the software for a particular purpose.

The possibility of extending the guarantee will be the subject of a separate
contract, stating the conditions relating to this extension.

LIMITATION OF CLAIMS AND RESPONSIBILITY
SETEC HYDRATEC disclaims all liability for any damages, either direct or
indirect, claims or losses resulting from the use of or inability to use
the program, even if SETEC HYDRATEC has been advised of the possibility
of such damages.

The program is only provided to the user as a design tool. SETEC HYDRATEC
cannot under any circumstances be considered as the constructor, as defined
by Article 1792.1 of the French Civil Code, nor as contractor nor sub-contractor.

In all cases, the entire liability of SETEC HYDRATEC is limited to the amount
actually paid for the software. The claims and limitations specified above are
applicable whether or not you have accepted the program.

GENERAL CLAUSES
The present Licensing Agreement can only be modified by an attached Addendum or
by another document duly signed by SETEC HYDRATEC and the Licensee.

Any litigation resulting from the use of the program or the application of the
present Licensing Agreement is the exclusive jurisdiction of the Commercial
Court of Paris.
