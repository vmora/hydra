--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpython3u; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpython3u WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpython3u; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpython3u IS 'PL/Python3U untrusted procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: hydra; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS hydra WITH SCHEMA public VERSION '1.0.0';


--
-- Name: EXTENSION hydra; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION hydra IS 'hydrological model hydra';


--
-- Name: model1; Type: SCHEMA; Schema: -; Owner: thierry.lepelletier
--

CREATE SCHEMA model1;


ALTER SCHEMA model1 OWNER TO "thierry.lepelletier";

--
-- Name: project; Type: SCHEMA; Schema: -; Owner: thierry.lepelletier
--

CREATE SCHEMA project;


ALTER SCHEMA project OWNER TO "thierry.lepelletier";

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = model1, pg_catalog;

--
-- Name: _link_after_delete_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION _link_after_delete_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        delete from model1._node
        where id=old.down
        and generated is not null
        and (select count(1) from model1._link where up=old.down and link_type != 'street') = 0
        and (select count(1) from model1._link where down=old.down and link_type != 'street') = 0
        and (select count(1) from model1._singularity as s where s.id=id) = 0;
        delete from model1._node
        where id=old.up
        and generated is not null
        and (select count(1) from model1._link where up=old.up and link_type != 'street') = 0
        and (select count(1) from model1._link where down=old.up and link_type != 'street') = 0
        and (select count(1) from model1._singularity as s where s.id=id) = 0;
        return old;
    end;
$$;


ALTER FUNCTION model1._link_after_delete_fct() OWNER TO "thierry.lepelletier";

--
-- Name: _str_profile_from_transect(public.geometry, boolean, integer, boolean, integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION _str_profile_from_transect(flood_plain_transect public.geometry, interpolate boolean DEFAULT false, discretization integer DEFAULT 150, ignore_points boolean DEFAULT false, distance_point integer DEFAULT 100) RETURNS character varying
    LANGUAGE plpython3u IMMUTABLE
    AS $$
    import plpy
    from hydra_1_0_0 import profile_from_transect
    return profile_from_transect(plpy, flood_plain_transect, 'model1', 2154, interpolate, discretization, ignore_points, distance_point)
$$;


ALTER FUNCTION model1._str_profile_from_transect(flood_plain_transect public.geometry, interpolate boolean, discretization integer, ignore_points boolean, distance_point integer) OWNER TO "thierry.lepelletier";

--
-- Name: add_configuration_fct(json, integer, character varying); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION add_configuration_fct(configuration json, id integer, table_name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
    declare
        config varchar;
    begin
        if configuration is not null then
            -- @todo complete configuration with default if needed
            if 'default' not in (select k from json_object_keys(configuration) as k) then
                raise exception 'trying to add a configuration with no default values';
            end if;
            insert into model1.configuration(name)
                select k
                from json_object_keys(configuration) as k
                where k not in (select name from model1.configuration);
        end if;
    end;
$$;


ALTER FUNCTION model1.add_configuration_fct(configuration json, id integer, table_name character varying) OWNER TO "thierry.lepelletier";

--
-- Name: borda_headloss_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION borda_headloss_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('borda_headloss', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'borda_headloss') where name = 'define_later' and id = id_;
            insert into model1._borda_headloss_link
                values (id_, 'borda_headloss', new.law_type, new.param::json);
            if 'borda_headloss' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'borda_headloss_link');
            update model1._link set validity = (select (law_type is not null) and (param is not null) from  model1._borda_headloss_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'borda_headloss' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._borda_headloss_link set law_type=new.law_type, param=new.param::json where id=old.id;
            if 'borda_headloss' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'borda_headloss_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'borda_headloss' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (law_type is not null) and (param is not null) from  model1._borda_headloss_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._borda_headloss_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'borda_headloss' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'borda_headloss' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.borda_headloss_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: borda_headloss_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION borda_headloss_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'borda_headloss', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'borda_headloss') where name = 'define_later' and id = id_;

            insert into model1._borda_headloss_singularity
                values (id_, 'borda_headloss', new.law_type, new.param::json);
            if 'borda_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'borda_headloss_singularity');
            update model1._singularity set validity = (select (law_type is not null) and (param is not null) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._borda_headloss_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._borda_headloss_singularity set law_type=new.law_type, param=new.param::json where id=old.id;
            if 'borda_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'borda_headloss_singularity');
            update model1._singularity set validity = (select (law_type is not null) and (param is not null) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._borda_headloss_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._borda_headloss_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'borda_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.borda_headloss_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: bradley_headloss_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION bradley_headloss_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'bradley_headloss', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'bradley_headloss') where name = 'define_later' and id = id_;

            insert into model1._bradley_headloss_singularity
                values (id_, 'bradley_headloss', new.d_abutment_l, new.d_abutment_r, coalesce(new.abutment_type, 'angle_90'), new.zw_array, coalesce(new.z_ceiling, new.zw_array[array_length(new.zw_array, 1)][1]));
            if 'bradley_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'bradley_headloss_singularity');
            update model1._singularity set validity = (select (d_abutment_l is not null) and (d_abutment_r is not null) and (abutment_type is not null) and (zw_array is not null ) and (z_ceiling is not null) and (array_length(zw_array, 1)<=10) and (array_length(zw_array, 1)>=1) and (array_length(zw_array, 2)=2) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._bradley_headloss_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._bradley_headloss_singularity set d_abutment_l=new.d_abutment_l, d_abutment_r=new.d_abutment_r, abutment_type=new.abutment_type, zw_array=new.zw_array, z_ceiling=new.z_ceiling where id=old.id;
            if 'bradley_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'bradley_headloss_singularity');
            update model1._singularity set validity = (select (d_abutment_l is not null) and (d_abutment_r is not null) and (abutment_type is not null) and (zw_array is not null ) and (z_ceiling is not null) and (array_length(zw_array, 1)<=10) and (array_length(zw_array, 1)>=1) and (array_length(zw_array, 2)=2) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._bradley_headloss_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._bradley_headloss_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'bradley_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.bradley_headloss_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: branch_after_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION branch_after_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        if tg_op = 'INSERT' then
            update model1.hydraulic_cut_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.zregul_weir_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.param_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.regul_sluice_gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.borda_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            return new;
        else
            if tg_op = 'UPDATE' then
                update model1.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return new;
            else
                update model1.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return old;
            end if;
        end if;
    end;
$$;


ALTER FUNCTION model1.branch_after_fct() OWNER TO "thierry.lepelletier";

--
-- Name: branch_update_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION branch_update_fct() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    begin
        update model1._pipe_link set branch = null;
        delete from model1.branch;

        -- count up and down node connectivity on pipes
        -- select pass_trhough manhole nodes that have one up and one down
        -- linemerge pipes that have pass_through at both ends
        -- add pipes that are touching

        with up as (
            select n.id, count(1) as ct, n.geom
            from model1.pipe_link as l join model1.manhole_node as n on n.id=l.up
            group by n.id, n.geom
        ),
        down as (
            select n.id, count(1) as ct
            from model1.pipe_link as l join model1.manhole_node as n on n.id=l.down
            group by n.id
        ),
        blade as (
            select st_collect(n.geom) as geom
            from model1.manhole_node as n
            where id in (
            select id from model1.manhole_node
            except
            select up.id
            from up join down on up.id=down.id
            where up.ct = 1
            and down.ct = 1)
        ),
        branch as (
            select (st_dump(st_split(st_linemerge(st_collect(l.geom)), b.geom))).geom as geom
            from model1.pipe_link as l, blade as b
            where l.up_type='manhole'
            group by b.geom
        ),
        oriented as (
            select (st_dump(st_linemerge(st_collect(p.geom)))).geom as geom
            from branch as b,  model1.pipe_link as p
            where st_covers(b.geom, p.geom)
            group by b.geom
        )
        insert into model1.branch(id, name, geom)
        select row_number() over(), 'BRANCH_'||(row_number() over())::varchar, geom
        from oriented
        ;

        update model1.branch as b set name=m.name, dx=m.dx, pk0_km=m.pk0_km
        from model1.pipe_branch_marker_singularity as m where st_intersects(m.geom, b.geom);

        update model1._pipe_link as p set branch = b.id
        from model1._link as l, model1.branch as b
        where p.id = l.id and st_covers(b.geom, l.geom);

        return 't';
    end;
$$;


ALTER FUNCTION model1.branch_update_fct() OWNER TO "thierry.lepelletier";

--
-- Name: bridge_headloss_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION bridge_headloss_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'bridge_headloss', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'bridge_headloss') where name = 'define_later' and id = id_;

            insert into model1._bridge_headloss_singularity
                values (id_, 'bridge_headloss', new.l_road, new.z_road, new.zw_array);
            if 'bridge_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'bridge_headloss_singularity');
            update model1._singularity set validity = (select (l_road is not null) and (l_road>=0) and (z_road is not null) and (zw_array is not null ) and (array_length(zw_array, 1)<=10) and (array_length(zw_array, 1)>=1) and (array_length(zw_array, 2)=2) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._bridge_headloss_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._bridge_headloss_singularity set l_road=new.l_road, z_road=new.z_road, zw_array=new.zw_array where id=old.id;
            if 'bridge_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'bridge_headloss_singularity');
            update model1._singularity set validity = (select (l_road is not null) and (l_road>=0) and (z_road is not null) and (zw_array is not null ) and (array_length(zw_array, 1)<=10) and (array_length(zw_array, 1)>=1) and (array_length(zw_array, 2)=2) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._bridge_headloss_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._bridge_headloss_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'bridge_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.bridge_headloss_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: catchment_after_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION catchment_after_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        if tg_op = 'INSERT' then
            update model1.catchment_node set contour=new.id where ST_Intersects(model1.catchment_node.geom, new.geom);
            return new;
        else
            if tg_op = 'UPDATE' then
                update model1.catchment_node set contour=null where contour=old.id;
                update model1.catchment_node set contour=new.id where ST_Intersects(model1.catchment_node.geom, new.geom);
                return new;
            end if;
        end if;
    end;
$$;


ALTER FUNCTION model1.catchment_after_fct() OWNER TO "thierry.lepelletier";

--
-- Name: catchment_node_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION catchment_node_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into model1._node(node_type, name, geom, configuration, generated)
                values('catchment', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'catchment') where name = 'define_later' and id = id_;
            insert into model1._catchment_node
                values (id_, 'catchment', new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.netflow_param::json, new.runoff_type, new.runoff_param::json, coalesce(new.q_limit, 9999), coalesce(new.q0, 0), coalesce(new.contour, (select id from model1.catchment where st_intersects(geom, new.geom))), new.catchment_pollution_mode, new.catchment_pollution_param::json);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'catchment' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_param is not null) and (runoff_type is not null) and (runoff_param is not null) and (q_limit is not null) and (q0 is not null) from  model1._catchment_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update model1._node set name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._catchment_node set area_ha=new.area_ha, rl=new.rl, slope=new.slope, c_imp=new.c_imp, netflow_type=new.netflow_type, netflow_param=new.netflow_param::json, runoff_type=new.runoff_type, runoff_param=new.runoff_param::json, q_limit=new.q_limit, q0=new.q0, contour=new.contour, catchment_pollution_mode=new.catchment_pollution_mode, catchment_pollution_param=new.catchment_pollution_param::json where id=old.id;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'catchment' then
                update model1._catchment_node set contour=(select id from model1.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'catchment' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'catchment' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_param is not null) and (runoff_type is not null) and (runoff_param is not null) and (q_limit is not null) and (q0 is not null) from  model1._catchment_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'storage' then
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;
            if 'catchment' = 'crossroad' then
                perform model1.update_street_links();
            end if;

            delete from model1._catchment_node where id=old.id;
            delete from model1._node where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION model1.catchment_node_fct() OWNER TO "thierry.lepelletier";

--
-- Name: check_catchment_has_one_route_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION check_catchment_has_one_route_fct() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    declare
        res boolean;
    begin
        with link as (
            select count(1) as count from model1.catchment_node as bv, model1._link as r
            where bv.id=r.up and r.link_type='routing_hydrology'
            group by r.id)
        select max(count)=min(count) and min(count)=1 from link into res;
        return res;
    end;
$$;


ALTER FUNCTION model1.check_catchment_has_one_route_fct() OWNER TO "thierry.lepelletier";

--
-- Name: check_connected_pipes(integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION check_connected_pipes(id_ integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    declare
        up_pipes_ integer;
        down_pipes_ integer;
    begin
        select count(*) from model1.pipe_link where down=id_ into up_pipes_;
        select count(*) from model1.pipe_link where up=id_ into down_pipes_;
        if (up_pipes_=1 or down_pipes_=1) then
            return true;
        else
            return false;
        end if;
    end;
$$;


ALTER FUNCTION model1.check_connected_pipes(id_ integer) OWNER TO "thierry.lepelletier";

--
-- Name: check_extremities_nodes(public.geometry); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION check_extremities_nodes(geom_ public.geometry) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    declare
        up_ integer;
        down_ integer;
    begin
        select id from model1.river_node as n where st_intersects(geom, st_startpoint(geom_)) into up_;
        select id from model1.river_node as n where st_intersects(geom, st_endpoint(geom_)) into down_;

        if (up_ is not null and down_ is not null) then
            return true;
        else
            return false;
        end if;
    end;
$$;


ALTER FUNCTION model1.check_extremities_nodes(geom_ public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: check_hydrogramme_apport_on_down_route_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION check_hydrogramme_apport_on_down_route_fct() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    declare
        res boolean;
    begin
        /* apres: routing with hydrograms at down */
        with routing_with_hydrogramme_apport_at_down as (
            select r.id as rid from model1._link as r, model1._hydrograph_bc_singularity as a
            where r.down=a.id and r.link_type='routing_hydrology'
        )
        select count(1)=0 from model1._link as r
        where r.down_type!='manhole_hydrology' and r.link_type='routing_hydrology'
        and r.id not in (select rid from routing_with_hydrogramme_apport_at_down) into res;
        return res;
    end;
$$;


ALTER FUNCTION model1.check_hydrogramme_apport_on_down_route_fct() OWNER TO "thierry.lepelletier";

--
-- Name: check_on_branch_or_reach_endpoint(public.geometry); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION check_on_branch_or_reach_endpoint(geom public.geometry) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    declare
        res_ boolean;
    begin
        with forbidden as (select st_startpoint(reach.geom) as point from model1.reach
                               union
                               select st_endpoint(reach.geom) as point from model1.reach
                               union
                               select st_startpoint(branch.geom) as point from model1.branch
                               union
                               select st_endpoint(branch.geom) as point from model1.branch
                               )
        select exists(select 1 from forbidden where st_intersects(geom, forbidden.point)) into res_;
        return res_;
    end;
$$;


ALTER FUNCTION model1.check_on_branch_or_reach_endpoint(geom public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: connector_hydrology_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION connector_hydrology_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('connector_hydrology', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'connector_hydrology') where name = 'define_later' and id = id_;
            insert into model1._connector_hydrology_link
                values (id_, 'connector_hydrology', coalesce(new.hydrograph, (select hbc.id from model1.hydrograph_bc_singularity as hbc where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1))));
            if 'connector_hydrology' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'connector_hydrology_link');
            update model1._link set validity = (select (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  model1._connector_hydrology_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'connector_hydrology' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._connector_hydrology_link set hydrograph=new.hydrograph where id=old.id;
            if 'connector_hydrology' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'connector_hydrology_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'connector_hydrology' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  model1._connector_hydrology_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._connector_hydrology_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'connector_hydrology' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'connector_hydrology' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.connector_hydrology_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: connector_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION connector_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('connector', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'connector') where name = 'define_later' and id = id_;
            insert into model1._connector_link
                values (id_, 'connector', coalesce(new.main_branch, 'f'));
            if 'connector' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'connector_link');
            update model1._link set validity = (select 't'::boolean from  model1._connector_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'connector' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._connector_link set main_branch=new.main_branch where id=old.id;
            if 'connector' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'connector_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'connector' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select 't'::boolean from  model1._connector_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._connector_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'connector' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'connector' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.connector_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: constant_inflow_bc_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION constant_inflow_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'constant_inflow_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'constant_inflow_bc') where name = 'define_later' and id = id_;

            insert into model1._constant_inflow_bc_singularity
                values (id_, 'constant_inflow_bc', new.q0);
            if 'constant_inflow_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'constant_inflow_bc_singularity');
            update model1._singularity set validity = (select (q0 is not null) from  model1._constant_inflow_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._constant_inflow_bc_singularity set q0=new.q0 where id=old.id;
            if 'constant_inflow_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'constant_inflow_bc_singularity');
            update model1._singularity set validity = (select (q0 is not null) from  model1._constant_inflow_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._constant_inflow_bc_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'constant_inflow_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.constant_inflow_bc_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: constrain_after_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION constrain_after_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        cov integer;
    begin
        -- remove from coverage the surafces that do not belong to domains
        select model1.coverage_update() into cov;

        if tg_op = 'INSERT' or tg_op = 'UPDATE'  then
            return new;
        else
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.constrain_after_fct() OWNER TO "thierry.lepelletier";

--
-- Name: constrain_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION constrain_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        prec_ real;
        snapped_ geometry;
        discretized_ geometry;
    begin
        if tg_op = 'INSERT' then
            select precision from hydra.metadata into prec_;
            select st_snaptogrid(new.geom, prec_) into snapped_;
            select st_snaptogrid(project.discretize_line(new.geom, coalesce(new.elem_length, 100)), prec_) into discretized_;
            insert into model1._constrain(name, geom, discretized, elem_length, constrain_type, link_attributes)
            values (coalesce(new.name, 'define_later'), snapped_, discretized_, coalesce(new.elem_length, 100), new.constrain_type, new.link_attributes)
            returning id, elem_length into new.id, new.elem_length;
            update model1._constrain set name = 'CONSTR'||new.id::varchar where name = 'define_later' and id = new.id
            returning name into new.name;
            return new;
        elsif tg_op = 'UPDATE' then
            select precision from hydra.metadata into prec_;
            select st_snaptogrid(new.geom, prec_) into snapped_;
            select st_snaptogrid(project.discretize_line(new.geom, new.elem_length), prec_) into discretized_;
            update model1._constrain set name=new.name, geom=snapped_, discretized=discretized_,
                elem_length=new.elem_length, constrain_type=new.constrain_type, link_attributes=new.link_attributes
                where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._constrain where id=old.id;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.constrain_fct() OWNER TO "thierry.lepelletier";

--
-- Name: coverage_after_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION coverage_after_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        if tg_op = 'INSERT' then
            update model1.storage_node set
                contour=new.id
                where ST_Intersects(model1.storage_node.geom, new.geom);
            return new;
        else
            if tg_op = 'UPDATE' then
                update model1.storage_node set contour=null where contour=old.id;

                update model1.storage_node set
                    contour=new.id
                    where ST_Intersects(model1.storage_node.geom, new.geom);
                    return new;
            end if;
        end if;
    end;
$$;


ALTER FUNCTION model1.coverage_after_fct() OWNER TO "thierry.lepelletier";

--
-- Name: coverage_update(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION coverage_update() RETURNS integer
    LANGUAGE plpgsql
    AS $$
    declare
        res_ integer;
    begin
        delete from model1.coverage ; -- where domain_2d is null;
        alter sequence model1.coverage_id_seq restart;

        with cst as (
            select
                    (st_dump(
                        coalesce(
                            st_split(
                                a.discretized,
                                (select st_collect(discretized)
                                    from model1.constrain as b
                                    where a.id!=b.id
                                    and st_intersects(a.discretized, b.discretized)
                                    )
                                ),
                            a.discretized)
                    )).geom as geom
            from model1.constrain as a

        )
        -- polygonize
        insert into model1.coverage(geom, domain_type)
        select (st_dump(st_polygonize(geom))).geom, '2d' from cst;


        with oned as (
            select c.id, count(1) as ct
            from model1.coverage as c, model1.street as d
            where st_intersects(c.geom, d.geom)
            group by c.id
        )
        update model1.coverage set domain_type='street' where id in (select oned.id from oned where ct=1);

        with oned as (
            select c.id, count(1) as ct
            from model1.coverage as c, model1.street as d
            where st_intersects(c.geom, d.geom)
            group by c.id
        )
        update model1.coverage set domain_type='crossroad' where id in (select oned.id from oned where ct>1);

        update model1.coverage as c set domain_type='reach'
        where exists (select 1 from model1.open_reach as r
        where st_intersects(c.geom, r.geom)
        and st_length(st_collectionextract(st_intersection(c.geom, r.geom), 2)) > 0);

        update model1.coverage as c set domain_type='reach_end'
        where exists (select 1 from model1.open_reach as r
            where (st_intersects(c.geom, st_startpoint(r.geom)) and st_distance(st_startpoint(r.geom), st_exteriorring(c.geom)) > 5)
            or (st_intersects(c.geom,  st_endpoint(r.geom))) and st_distance(st_endpoint(r.geom), st_exteriorring(c.geom)) > 5);

        with oned as (
            select distinct c.id
            from model1.coverage as c, model1.storage_node as d
            where st_intersects(c.geom, d.geom)
        )
        update model1.coverage set domain_type='storage' where id in (select oned.id from oned);

        with oned as (
            select c.id, count(1) as ct
            from model1.coverage as c, model1.coverage_marker as mkc
            where st_intersects(c.geom, mkc.geom)
            group by c.id
        )
        update model1.coverage set domain_type=null where id in (select oned.id from oned where ct=1);

        select count(*) from model1.coverage into res_;
        return res_;
    end;
$$;


ALTER FUNCTION model1.coverage_update() OWNER TO "thierry.lepelletier";

--
-- Name: cp_geometric_calc_p_fct(real[]); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION cp_geometric_calc_p_fct(real[]) RETURNS double precision
    LANGUAGE plpgsql IMMUTABLE
    AS $_$
    declare
        z_array alias for $1;
        x real[];
        s double precision := 0.0;
        last_h double precision := 0.0;
        last_w double precision := 0.0;
        w1 double precision := 0.0;
    begin
        foreach x slice 1 in array z_array
        loop
            s:=s+2*sqrt(pow(x[2]-last_w,2)+pow(x[1]-last_h,2))+z_array[1][2]+z_array[array_length(z_array, 1)][2];
            last_h:=x[1];
            last_w:=x[2];
        end loop;
        return s;
    end;
$_$;


ALTER FUNCTION model1.cp_geometric_calc_p_fct(real[]) OWNER TO "thierry.lepelletier";

--
-- Name: create_boundary_links(integer, integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION create_boundary_links(constrain_id integer, station_node_id integer) RETURNS integer
    LANGUAGE plpython3u
    AS $$
    import plpy
    from hydra_1_0_0 import create_boundary_links
    return create_boundary_links(plpy, constrain_id, station_node_id, 'model1', 2154)
$$;


ALTER FUNCTION model1.create_boundary_links(constrain_id integer, station_node_id integer) OWNER TO "thierry.lepelletier";

--
-- Name: create_elem2d_links(integer, public.geometry, integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION create_elem2d_links(elem_id integer, border public.geometry, generation_step integer DEFAULT NULL::integer) RETURNS integer
    LANGUAGE plpython3u
    AS $$
    import plpy
    from hydra_1_0_0 import create_elem2d_links
    return create_elem2d_links(plpy, elem_id, border, 'model1', 2154, generation_step)
$$;


ALTER FUNCTION model1.create_elem2d_links(elem_id integer, border public.geometry, generation_step integer) OWNER TO "thierry.lepelletier";

--
-- Name: create_links(integer, integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION create_links(up_domain_id integer, down_domain_id integer) RETURNS integer
    LANGUAGE plpython3u
    AS $$
    import plpy
    from hydra_1_0_0 import create_links
    return create_links(plpy, up_domain_id, down_domain_id, 'model1', 2154)
$$;


ALTER FUNCTION model1.create_links(up_domain_id integer, down_domain_id integer) OWNER TO "thierry.lepelletier";

--
-- Name: crossroad_node_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION crossroad_node_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into model1._node(node_type, name, geom, configuration, generated)
                values('crossroad', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'crossroad') where name = 'define_later' and id = id_;
            insert into model1._crossroad_node
                values (id_, 'crossroad', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.h, 0));
            perform model1.add_configuration_fct(new.configuration::json, id_, 'crossroad_node');

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'crossroad' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  model1._crossroad_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update model1._node set name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._crossroad_node set area=new.area, z_ground=new.z_ground, h=new.h where id=old.id;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'crossroad_node');

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'catchment' then
                update model1._catchment_node set contour=(select id from model1.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'crossroad' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'crossroad' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  model1._crossroad_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'storage' then
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;
            if 'crossroad' = 'crossroad' then
                perform model1.update_street_links();
            end if;

            delete from model1._crossroad_node where id=old.id;
            delete from model1._node where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION model1.crossroad_node_fct() OWNER TO "thierry.lepelletier";

--
-- Name: crossroad_update(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION crossroad_update() RETURNS integer
    LANGUAGE plpgsql
    AS $$
    declare
        res_ integer;
    begin
        with nodes as (
            insert into model1.crossroad_node(geom)
                (select distinct s.geom
                    from (select (st_dumppoints(geom)).geom as geom from model1.street) as s
                    left join (select geom from model1.crossroad_node) as c
                    on s.geom=c.geom
                    where c.geom is null)
            returning id
        )
        select count(1) from nodes into res_;
        return res_;
    end;
$$;


ALTER FUNCTION model1.crossroad_update() OWNER TO "thierry.lepelletier";

--
-- Name: ctrz_dat(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION ctrz_dat() RETURNS character varying
    LANGUAGE plpython3u
    AS $$
    import plpy
    from hydra_1_0_0 import ctrz_dat
    return ctrz_dat(plpy, 'model1')
$$;


ALTER FUNCTION model1.ctrz_dat() OWNER TO "thierry.lepelletier";

--
-- Name: deriv_pump_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION deriv_pump_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('deriv_pump', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'deriv_pump') where name = 'define_later' and id = id_;
            insert into model1._deriv_pump_link
                values (id_, 'deriv_pump', new.q_pump, new.qz_array);
            if 'deriv_pump' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'deriv_pump_link');
            update model1._link set validity = (select (q_pump is not null) and (q_pump>= 0) and (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) from  model1._deriv_pump_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'deriv_pump' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._deriv_pump_link set q_pump=new.q_pump, qz_array=new.qz_array where id=old.id;
            if 'deriv_pump' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'deriv_pump_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'deriv_pump' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (q_pump is not null) and (q_pump>= 0) and (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) from  model1._deriv_pump_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._deriv_pump_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'deriv_pump' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'deriv_pump' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.deriv_pump_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: downstream_valley_section(public.geometry); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION downstream_valley_section(transect public.geometry) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
    begin
        return (
        with reach as (
            select r.id, st_intersection(r.geom, transect) as inter, geom, pk0_km
            from model1.reach as r
            where st_intersects(r.geom, transect)
        ),
        pk as (
            select st_linelocatepoint(geom, inter)*st_length(geom)/1000+pk0_km as pk_km from reach
        ),
        down_reach as (
            select ndown.reach as id
            from model1.river_node as ndown
            join model1.connector_link as l on l.down=ndown.id
            join model1.river_node as nup on nup.id=l.up
            where l.main_branch
            and nup.reach = (select id from reach)
        ),
        extended_reach_node as (
            select n.id, n.pk_km
            from model1.river_node as n, pk
            where n.reach in (select id from reach union select id from down_reach)
            and n.pk_km >= pk.pk_km
        ),
        section_down as (
            select c.id
            from extended_reach_node as n
            join model1.river_cross_section_profile as c on c.id = n.id,
            pk
            where ((c.type_cross_section_down='valley' and c.down_vcs_geom is not null)
                or (c.type_cross_section_up='valley' and c.up_vcs_geom is not null))
            order by n.pk_km asc
            limit 1
        )
        select id from section_down);
    end;
$$;


ALTER FUNCTION model1.downstream_valley_section(transect public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: elem_2d_node_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION elem_2d_node_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into model1._node(node_type, name, geom, configuration, generated)
                values('elem_2d', coalesce(new.name, 'define_later'), ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), 2154), (select precision from hydra.metadata)), new.configuration::json, new.generated) returning id into id_;
            update model1._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'elem_2d') where name = 'define_later' and id = id_;
            insert into model1._elem_2d_node
                values (id_, 'elem_2d', coalesce(new.area, ST_Area(new.contour)), coalesce(new.zb, (select ST_Z(ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), 2154), (select precision from hydra.metadata))))), coalesce(new.rk, 12), new.domain_2d, new.contour);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'elem_2d_node');

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'elem_2d' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  model1._elem_2d_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update model1._node set name=new.name, geom=ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), 2154), (select precision from hydra.metadata)), configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._elem_2d_node set area=new.area, zb=new.zb, rk=new.rk, domain_2d=new.domain_2d, contour=new.contour where id=old.id;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'elem_2d_node');

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'catchment' then
                update model1._catchment_node set contour=(select id from model1.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'elem_2d' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'elem_2d' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  model1._elem_2d_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'storage' then
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;
            if 'elem_2d' = 'crossroad' then
                perform model1.update_street_links();
            end if;

            delete from model1._elem_2d_node where id=old.id;
            delete from model1._node where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION model1.elem_2d_node_fct() OWNER TO "thierry.lepelletier";

--
-- Name: find_link_fct(public.geometry); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION find_link_fct(geom_ public.geometry, OUT up_ integer, OUT up_type_ public.hydra_node_type, OUT down_ integer, OUT down_type_ public.hydra_node_type) RETURNS record
    LANGUAGE plpgsql
    AS $$
    begin
        select id, node_type from model1._node
        where ST_DWithin(ST_StartPoint(geom_), geom, .1)
        order by ST_Distance(ST_StartPoint(geom_), geom) asc
        limit 1 into up_, up_type_
        ;
        select id, node_type from model1._node
        where ST_DWithin(ST_EndPoint(geom_), geom, .1)
        order by ST_Distance(ST_EndPoint(geom_), geom) asc
        limit 1 into down_, down_type_
        ;
        if (up_ is null) then
            -- find link from contours
            with node as (
                select s.id, 'storage' as node_type, c.geom as contour from model1._storage_node as s, model1.coverage as c where c.id=s.contour
                union
                select id, 'elem_2d' as node_type, contour from model1._elem_2d_node
            )
            select id, node_type from node
            where ST_Intersects(ST_StartPoint(geom_), contour)
            order by ST_Distance(ST_StartPoint(geom_), ST_Centroid(contour)) asc
            limit 1 into up_, up_type_
            ;
        end if;
        if (down_ is null) then
            -- find link from contours
            with node as (
                select s.id, 'storage' as node_type, c.geom as contour from model1._storage_node as s, model1.coverage as c where c.id=s.contour
                union
                select id, 'elem_2d' as node_type, contour from model1._elem_2d_node
            )
            select id, node_type from node
            where ST_Intersects(ST_EndPoint(geom_), contour)
            order by ST_Distance(ST_EndPoint(geom_), ST_Centroid(contour)) asc
            limit 1 into down_, down_type_
            ;
        end if;

        if (up_ is null or down_ is null) then
            raise exception 'link % not touching nodes (distance from start % distance from end %)', ST_AsText(geom_), (select ST_Distance(ST_StartPoint(geom_), geom) from model1._node order by ST_Distance(ST_StartPoint(geom_), geom) limit 1), (select ST_Distance(ST_EndPoint(geom_), geom) from model1._node order by ST_Distance(ST_EndPoint(geom_), geom) limit 1);
        end if;
    end;
$$;


ALTER FUNCTION model1.find_link_fct(geom_ public.geometry, OUT up_ integer, OUT up_type_ public.hydra_node_type, OUT down_ integer, OUT down_type_ public.hydra_node_type) OWNER TO "thierry.lepelletier";

--
-- Name: froude_bc_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION froude_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'froude_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'froude_bc') where name = 'define_later' and id = id_;

            insert into model1._froude_bc_singularity
                values (id_, 'froude_bc');
            if 'froude_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'froude_bc_singularity');
            update model1._singularity set validity = (select 't'::boolean from  model1._froude_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            --update model1._froude_bc_singularity set  where id=old.id;
            if 'froude_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'froude_bc_singularity');
            update model1._singularity set validity = (select 't'::boolean from  model1._froude_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._froude_bc_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'froude_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.froude_bc_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: fuse_spillway_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION fuse_spillway_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('fuse_spillway', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'fuse_spillway') where name = 'define_later' and id = id_;
            insert into model1._fuse_spillway_link
                values (id_, 'fuse_spillway', new.z_invert, new.width, coalesce(new.cc, .6), coalesce(new.break_mode, 'none'), new.z_break, new.t_break, coalesce(new.grp, 1), new.dt_fracw_array);
            if 'fuse_spillway' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'fuse_spillway_link');
            update model1._link set validity = (select (z_invert is not null) and (width is not null) and (width>=0.) and (cc is not null) and (cc<=1) and (cc>=0.) and (break_mode is not null) and (break_mode!='zw_critical' or z_break is not null) and (break_mode!='time_critical' or t_break is not null) and (break_mode='none' or grp is not null) and (break_mode='none' or grp>0) and (break_mode='none' or grp<100) and (break_mode='none' or dt_fracw_array is not null) and (break_mode='none' or array_length(dt_fracw_array, 1)<=10) and (break_mode='none' or array_length(dt_fracw_array, 1)>=1) and (break_mode='none' or array_length(dt_fracw_array, 2)=2) from  model1._fuse_spillway_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'fuse_spillway' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._fuse_spillway_link set z_invert=new.z_invert, width=new.width, cc=new.cc, break_mode=new.break_mode, z_break=new.z_break, t_break=new.t_break, grp=new.grp, dt_fracw_array=new.dt_fracw_array where id=old.id;
            if 'fuse_spillway' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'fuse_spillway_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'fuse_spillway' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (z_invert is not null) and (width is not null) and (width>=0.) and (cc is not null) and (cc<=1) and (cc>=0.) and (break_mode is not null) and (break_mode!='zw_critical' or z_break is not null) and (break_mode!='time_critical' or t_break is not null) and (break_mode='none' or grp is not null) and (break_mode='none' or grp>0) and (break_mode='none' or grp<100) and (break_mode='none' or dt_fracw_array is not null) and (break_mode='none' or array_length(dt_fracw_array, 1)<=10) and (break_mode='none' or array_length(dt_fracw_array, 1)>=1) and (break_mode='none' or array_length(dt_fracw_array, 2)=2) from  model1._fuse_spillway_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._fuse_spillway_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'fuse_spillway' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'fuse_spillway' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.fuse_spillway_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: gate_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION gate_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('gate', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'gate') where name = 'define_later' and id = id_;
            insert into model1._gate_link
                values (id_, 'gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.mode_valve, 'no_valve'), coalesce(new.z_gate, new.z_ceiling), coalesce(new.v_max_cms, .2));
            if 'gate' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'gate_link');
            update model1._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) from  model1._gate_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'gate' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._gate_link set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, mode_valve=new.mode_valve, z_gate=new.z_gate, v_max_cms=new.v_max_cms where id=old.id;
            if 'gate' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'gate_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'gate' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) from  model1._gate_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._gate_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'gate' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'gate' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.gate_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: gate_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION gate_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'gate', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'gate') where name = 'define_later' and id = id_;

            insert into model1._gate_singularity
                values (id_, 'gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.mode_valve, 'no_valve'), coalesce(new.z_gate, new.z_ceiling), coalesce(new.v_max_cms, .2));
            if 'gate' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'gate_singularity');
            update model1._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._gate_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._gate_singularity set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, mode_valve=new.mode_valve, z_gate=new.z_gate, v_max_cms=new.v_max_cms where id=old.id;
            if 'gate' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'gate_singularity');
            update model1._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._gate_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._gate_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'gate' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.gate_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: gen_cst_street(integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION gen_cst_street(street_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    declare
        _street_geom geometry;
        _street_width real;
        _street_el real;
    begin
        select geom, width, elem_length
        from model1.street
        where id=street_id
        into _street_geom, _street_width, _street_el;

        with buf as (
                select st_buffer(st_force2d(_street_geom), _street_width/2, 'quad_segs=1 join=mitre mitre_limit='||(_street_width*2)::varchar) as geom
            ),
            cst as (
                select st_exteriorring(geom) as geom from buf
            ),
            snp as (
                update model1.constrain set geom = st_snap(geom, _street_geom, 1)
            ),
            spl as (
                select coalesce(
                    st_split(cst.geom, (select st_collect(st_force2d(c.geom))
                            from model1.constrain as c
                            where st_intersects(c.geom, cst.geom)
                            )), cst.geom) as geom
                from cst
            ),
            spld as (
                select st_force3d((st_dump(geom)).geom) as geom from spl
            ),
            diff as (
                select id, (st_dump(st_collectionextract(st_split(c.geom, b.geom), 2))).geom as geom
                from model1.constrain as c, cst as b
                where st_intersects(b.geom, c.geom)
            ),
            ins as (
                insert into model1.constrain(geom, elem_length, constrain_type)
                select d.geom, c.elem_length, c.constrain_type
                from model1.constrain as c, diff as d
                where c.id = d.id
                union
                select geom, _street_el, 'overflow'
                from spld
                -- where not st_intersects(spld.geom, new.geom)
                returning id
            )
            delete from model1.constrain where id in (select id from diff);
            return 1;
    end;
$$;


ALTER FUNCTION model1.gen_cst_street(street_id integer) OWNER TO "thierry.lepelletier";

--
-- Name: geom_dat(character varying, integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION geom_dat(type_ character varying, id integer) RETURNS character varying
    LANGUAGE plpython3u
    AS $$
    import plpy
    from hydra_1_0_0 import geom_dat
    return geom_dat(plpy, 'model1', type_, id)
$$;


ALTER FUNCTION model1.geom_dat(type_ character varying, id integer) OWNER TO "thierry.lepelletier";

--
-- Name: geometric_calc_s_fct(real[]); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION geometric_calc_s_fct(real[]) RETURNS double precision
    LANGUAGE plpgsql IMMUTABLE
    AS $_$
    declare
        z_array alias for $1;
        x real[];
        s double precision := 0.0;
        last_h double precision := 0.0;
        last_w double precision := 0.0;
    begin
        foreach x slice 1 in array z_array
        loop
            s:=s+0.5*(x[2]+last_w)*(x[1]-last_h);
            last_h:=x[1];
            last_w:=x[2];
        end loop;
        return s;
    end;
$_$;


ALTER FUNCTION model1.geometric_calc_s_fct(real[]) OWNER TO "thierry.lepelletier";

--
-- Name: hydraulic_cut_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION hydraulic_cut_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'hydraulic_cut', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'hydraulic_cut') where name = 'define_later' and id = id_;

            insert into model1._hydraulic_cut_singularity
                values (id_, 'hydraulic_cut', new.qz_array);
            if 'hydraulic_cut' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'hydraulic_cut_singularity');
            update model1._singularity set validity = (select (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._hydraulic_cut_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._hydraulic_cut_singularity set qz_array=new.qz_array where id=old.id;
            if 'hydraulic_cut' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'hydraulic_cut_singularity');
            update model1._singularity set validity = (select (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._hydraulic_cut_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._hydraulic_cut_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'hydraulic_cut' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.hydraulic_cut_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: hydrograph_bc_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION hydrograph_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'hydrograph_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'hydrograph_bc') where name = 'define_later' and id = id_;

            insert into model1._hydrograph_bc_singularity
                values (id_, 'hydrograph_bc', new.storage_area, new.tq_array, coalesce(new.constant_dry_flow, 0), coalesce(new.distrib_coef, 1), coalesce(new.lag_time_hr, 0), new.sector, new.hourly_modulation, new.pollution_quality_mode, new.pollution_quality_param::json, coalesce(new.external_file_data, 'f'));
            if 'hydrograph_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'hydrograph_bc_singularity');
            update model1._singularity set validity = (select (storage_area is not null) and (storage_area>=0) and (constant_dry_flow is not null) and (constant_dry_flow>=0) and (distrib_coef is null or (distrib_coef>=0 and distrib_coef<=1)) and (lag_time_hr is null or (lag_time_hr>=0)) and (sector is null or (distrib_coef is not null and lag_time_hr is not null)) and (external_file_data or tq_array is not null) and (external_file_data or array_length(tq_array, 1)<=10) and (external_file_data or array_length(tq_array, 1)>=1) and (external_file_data or array_length(tq_array, 2)=2) from  model1._hydrograph_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._hydrograph_bc_singularity set storage_area=new.storage_area, tq_array=new.tq_array, constant_dry_flow=new.constant_dry_flow, distrib_coef=new.distrib_coef, lag_time_hr=new.lag_time_hr, sector=new.sector, hourly_modulation=new.hourly_modulation, pollution_quality_mode=new.pollution_quality_mode, pollution_quality_param=new.pollution_quality_param::json, external_file_data=new.external_file_data where id=old.id;
            if 'hydrograph_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'hydrograph_bc_singularity');
            update model1._singularity set validity = (select (storage_area is not null) and (storage_area>=0) and (constant_dry_flow is not null) and (constant_dry_flow>=0) and (distrib_coef is null or (distrib_coef>=0 and distrib_coef<=1)) and (lag_time_hr is null or (lag_time_hr>=0)) and (sector is null or (distrib_coef is not null and lag_time_hr is not null)) and (external_file_data or tq_array is not null) and (external_file_data or array_length(tq_array, 1)<=10) and (external_file_data or array_length(tq_array, 1)>=1) and (external_file_data or array_length(tq_array, 2)=2) from  model1._hydrograph_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._hydrograph_bc_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'hydrograph_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.hydrograph_bc_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: hydrology_bc_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION hydrology_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'hydrology_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'hydrology_bc') where name = 'define_later' and id = id_;

            insert into model1._hydrology_bc_singularity
                values (id_, 'hydrology_bc');
            if 'hydrology_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'hydrology_bc_singularity');
            update model1._singularity set validity = (select 't'::boolean from  model1._hydrology_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            --update model1._hydrology_bc_singularity set  where id=old.id;
            if 'hydrology_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'hydrology_bc_singularity');
            update model1._singularity set validity = (select 't'::boolean from  model1._hydrology_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._hydrology_bc_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'hydrology_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.hydrology_bc_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: inter_pave_border_fct(integer, integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION inter_pave_border_fct(up integer, down integer, OUT out_ public.geometry) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
    begin
        with border as (
            select ST_CollectionHomogenize(
                ST_Intersection(n1.contour, n2.contour)) as geom, 0.5*(ST_Z(n1.geom) + ST_Z(n2.geom)) as z
            from model1.elem_2d_node as n1, model1.elem_2d_node as n2
            where n1.id=up and n2.id=down
        )
        select ST_SetSRID(ST_MakeLine(
                ST_MakePoint(ST_X(ST_StartPoint(geom)), ST_Y(ST_StartPoint(geom)), z),
                ST_MakePoint(ST_X(ST_EndPoint(geom)), ST_Y(ST_EndPoint(geom)), z)), 2154)
        from border into out_;
    end;
$$;


ALTER FUNCTION model1.inter_pave_border_fct(up integer, down integer, OUT out_ public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: inter_pave_symbol_fct(integer, integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION inter_pave_symbol_fct(up integer, down integer, OUT out_ public.geometry) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
    declare
       pstart_ geometry('POINTZ',2154);
       pend_ geometry('POINTZ',2154);
       tx_ real;
       ty_ real;
    begin
       select geom from model1._node where id=up into pstart_;
       select geom from model1._node where id=down into pend_;
       select .1*(ST_X(pend_)-ST_X(pstart_)), .1*(ST_Y(pend_)-ST_Y(pstart_)) into tx_, ty_;
       with p1 as (select (ST_DumpPoints(contour)).geom::geometry as geom from model1._elem_2d_node where id=up),
       p2 as (select (ST_DumpPoints(contour)).geom::geometry as geom from model1._elem_2d_node where id=down),
       p1_ as (select distinct geom from p1),
       p2_ as (select distinct geom from p2),
       pairs as (select ST_Collect(p1_.geom, p2_.geom) as geom from p1_, p2_ order by ST_Distance(p1_.geom, p2_.geom) limit 2),
       line as (select ST_Collect(geom) as geom from pairs),
       symb as (select ST_Centroid(geom) as geom from line)
       select ST_SetSRID(ST_MakeLine(ST_MakePoint(ST_X(geom)-tx_, ST_Y(geom)-ty_, ST_Z(pstart_)), ST_MakePoint(ST_X(geom)+tx_, ST_Y(geom)+ty_, ST_Z(pend_))), 2154) from symb into out_;
    end;
$$;


ALTER FUNCTION model1.inter_pave_symbol_fct(up integer, down integer, OUT out_ public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: interpolate_transect_at(public.geometry, integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION interpolate_transect_at(point public.geometry, coverage_id integer DEFAULT NULL::integer) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
    declare
        l_ real;
    begin
        l_ = 10000;
        --raise warning 'interpolate_transect_at % %', point, coverage_id;
        return (
            with reach as (
                select r.id, r.geom, c.geom as flood_plain
                from model1.coverage as c, model1.reach as r
                where st_intersects(r.geom, c.geom)
                and domain_type='reach'
                and coalesce(c.id = coverage_id, 't'::boolean)
                order by c.geom <-> point
                limit 1
            ),
            transect_piece as (
                select st_linemerge(st_intersection(c.discretized, r.flood_plain)) as geom
                from model1.constrain as c, reach as r
                where st_intersects(r.flood_plain, c.discretized)
                and st_intersects(r.geom, c.geom)
                and c.constrain_type='flood_plain_transect'
                and st_length(st_collectionextract(st_intersection(c.discretized, r.flood_plain), 2)) > 0
            ),
            transect as (
                select st_makeline(
                    st_translate(st_startpoint(c.geom), -l_*sin(2*pi()-st_azimuth(st_startpoint(c.geom), st_endpoint(c.geom))), l_*cos(2*pi()-st_azimuth(st_startpoint(c.geom), st_endpoint(c.geom)))),
                    st_translate(st_startpoint(c.geom), l_*sin(2*pi()-st_azimuth(st_startpoint(c.geom), st_endpoint(c.geom))), -l_*cos(2*pi()-st_azimuth(st_startpoint(c.geom), st_endpoint(c.geom))))
                ) as geom, st_azimuth(st_startpoint(c.geom), st_endpoint(c.geom)) as azi
                from transect_piece as c
            ),
            dist as (
                select (case
                            when
                            abs((select azi from transect order by azi desc limit 1)-(select azi from transect order by azi asc limit 1)) > pi()
                                then case
                                when azi = (select azi from transect order by azi desc limit 1)
                                    then azi-pi()
                                    else azi+pi() end
                                else azi end)
                            as azi, st_distance(point, geom) as d from transect
            ),
            ct as  (
                select count(1) as n from dist
            ),
            disttot as (
                select sum(d) as sumd from dist
            ),
            interp as (
                select case when ct.n=1 then (
                    select azi from dist
                )
                when ct.n=2 then (
                    select sum(azi*(1-d/sumd)) as azi
                    from dist, disttot
                )
                end as azi
                from ct
            )
            select st_intersection(st_makeline(
                st_translate(point, l_*sin(2*pi()-azi), -l_*cos(2*pi()-azi)),
                st_translate(point, -l_*sin(2*pi()-azi), l_*cos(2*pi()-azi))), flood_plain)
            from interp, reach
        );
    end;
$$;


ALTER FUNCTION model1.interpolate_transect_at(point public.geometry, coverage_id integer) OWNER TO "thierry.lepelletier";

--
-- Name: line_set_z(public.geometry, real, real); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION line_set_z(line public.geometry, z_up real, z_down real) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
    declare
        geom geometry('LINESTRINGZ', 2154);
    begin
        with d as (select (ST_DumpPoints(line)).geom as geom),
        p as (select ST_X(d.geom) as x, ST_Y(d.geom) as y,
            ST_LineLocatePoint(line, d.geom) as a
            from d),
        i as (select x, y, (1-a)*z_up + a*z_down as z, a from p)
        select ST_SetSRID(ST_Makeline(ST_MakePoint(x, y, z) order by a), 2154) as geom from i into geom;
        return geom;
    end;
$$;


ALTER FUNCTION model1.line_set_z(line public.geometry, z_up real, z_down real) OWNER TO "thierry.lepelletier";

--
-- Name: manhole_hydrology_node_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION manhole_hydrology_node_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into model1._node(node_type, name, geom, configuration, generated)
                values('manhole_hydrology', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'manhole_hydrology') where name = 'define_later' and id = id_;
            insert into model1._manhole_hydrology_node
                values (id_, 'manhole_hydrology', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))));
            perform model1.add_configuration_fct(new.configuration::json, id_, 'manhole_hydrology_node');

            -- Lines to update specific nodes that works with associated contours
            if 'manhole_hydrology' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'manhole_hydrology' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) from  model1._manhole_hydrology_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update model1._node set name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._manhole_hydrology_node set area=new.area, z_ground=new.z_ground where id=old.id;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'manhole_hydrology_node');

            -- Lines to update specific nodes that works with associated contours
            if 'manhole_hydrology' = 'catchment' then
                update model1._catchment_node set contour=(select id from model1.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'manhole_hydrology' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'manhole_hydrology' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) from  model1._manhole_hydrology_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'manhole_hydrology' = 'storage' then
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;
            if 'manhole_hydrology' = 'crossroad' then
                perform model1.update_street_links();
            end if;

            delete from model1._manhole_hydrology_node where id=old.id;
            delete from model1._node where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION model1.manhole_hydrology_node_fct() OWNER TO "thierry.lepelletier";

--
-- Name: manhole_node_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION manhole_node_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into model1._node(node_type, name, geom, configuration, generated)
                values('manhole', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'manhole') where name = 'define_later' and id = id_;
            insert into model1._manhole_node
                values (id_, 'manhole', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))));
            perform model1.add_configuration_fct(new.configuration::json, id_, 'manhole_node');

            -- Lines to update specific nodes that works with associated contours
            if 'manhole' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'manhole' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from model1.station where st_intersects(geom, new.geom)))) and (model1.check_connected_pipes(new.id)) from  model1._manhole_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update model1._node set name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._manhole_node set area=new.area, z_ground=new.z_ground where id=old.id;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'manhole_node');

            -- Lines to update specific nodes that works with associated contours
            if 'manhole' = 'catchment' then
                update model1._catchment_node set contour=(select id from model1.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'manhole' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'manhole' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from model1.station where st_intersects(geom, new.geom)))) and (model1.check_connected_pipes(new.id)) from  model1._manhole_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'manhole' = 'storage' then
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;
            if 'manhole' = 'crossroad' then
                perform model1.update_street_links();
            end if;

            delete from model1._manhole_node where id=old.id;
            delete from model1._node where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION model1.manhole_node_fct() OWNER TO "thierry.lepelletier";

--
-- Name: marker_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION marker_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'marker', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'marker') where name = 'define_later' and id = id_;

            insert into model1._marker_singularity
                values (id_, 'marker', new.comment);
            if 'marker' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'marker_singularity');
            update model1._singularity set validity = (select 't'::boolean from  model1._marker_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._marker_singularity set comment=new.comment where id=old.id;
            if 'marker' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'marker_singularity');
            update model1._singularity set validity = (select 't'::boolean from  model1._marker_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._marker_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'marker' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.marker_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: mesh(integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION mesh(domain_id integer) RETURNS integer
    LANGUAGE plpython3u
    AS $$
    import plpy
    from hydra_1_0_0 import mesh
    return mesh(plpy, domain_id, 'model1', 2154)
$$;


ALTER FUNCTION model1.mesh(domain_id integer) OWNER TO "thierry.lepelletier";

--
-- Name: mesh_2d_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION mesh_2d_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('mesh_2d', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'mesh_2d') where name = 'define_later' and id = id_;
            insert into model1._mesh_2d_link
                values (id_, 'mesh_2d', new.z_invert, coalesce(new.lateral_contraction_coef, 1), coalesce(new.border, model1.inter_pave_border_fct(up_, down_)));
            if 'mesh_2d' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'mesh_2d_link');
            update model1._link set validity = (select (z_invert is not null) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (ST_NPoints(border)=2) and (ST_IsValid(border)) from  model1._mesh_2d_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'mesh_2d' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._mesh_2d_link set z_invert=new.z_invert, lateral_contraction_coef=new.lateral_contraction_coef, border=new.border where id=old.id;
            if 'mesh_2d' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'mesh_2d_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'mesh_2d' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (z_invert is not null) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (ST_NPoints(border)=2) and (ST_IsValid(border)) from  model1._mesh_2d_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._mesh_2d_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'mesh_2d' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'mesh_2d' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.mesh_2d_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: metadata_configuration_after_update_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION metadata_configuration_after_update_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        node record;
        update_fields varchar;
        config_name varchar;
    begin
        select name from model1.configuration where id=new.configuration into config_name;
        for node in select * from model1._node where configuration is not null loop
            if config_name not in (select k from json_object_keys(node.configuration) as k) then
                select 'default' into config_name;
            end if;
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='model1'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type != 'USER-DEFINED'
            and column_name not in ('id', 'node_type')
            into update_fields
            ;
            execute 'update model1.'||node.node_type::varchar||'_node '||
                         'set '||update_fields||' where id='||node.id::varchar||';'
            ;
        end loop;

        return new;
    end;
$$;


ALTER FUNCTION model1.metadata_configuration_after_update_fct() OWNER TO "thierry.lepelletier";

--
-- Name: mkc_after_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION mkc_after_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        cov_ integer;
    begin
        select model1.coverage_update() into cov_;

        if tg_op = 'INSERT' or tg_op = 'UPDATE'  then
            return new;
        else
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.mkc_after_fct() OWNER TO "thierry.lepelletier";

--
-- Name: mkd_after_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION mkd_after_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        old_cov integer;
        new_cov integer;
    begin
        if tg_op = 'INSERT' then
            select id from model1.coverage as c where st_intersects(c.geom, new.geom) into new_cov;
            update model1.elem_2d_node as n set domain_2d = new.domain_2d from model1.coverage as c where st_intersects(n.geom, c.geom) and c.id = new_cov;
            return new;
        else
            if tg_op = 'UPDATE' then
                select id from model1.coverage as c where st_intersects(c.geom, old.geom) into old_cov;
                update model1.elem_2d_node as n set domain_2d = new.domain_2d from model1.coverage as c where st_intersects(n.geom, c.geom) and c.id = old_cov;
                select id from model1.coverage as c where st_intersects(c.geom, new.geom) into new_cov;
                update model1.elem_2d_node as n set domain_2d = new.domain_2d from model1.coverage as c where st_intersects(n.geom, c.geom) and c.id = new_cov;
                return new;
            else
                select id from model1.coverage as c where st_intersects(c.geom, old.geom) into old_cov;
                update model1.elem_2d_node as n set domain_2d = new.domain_2d from model1.coverage as c where st_intersects(n.geom, c.geom) and c.id = old_cov;
                return old;
            end if;
        end if;
    end;
$$;


ALTER FUNCTION model1.mkd_after_fct() OWNER TO "thierry.lepelletier";

--
-- Name: model_connect_bc_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION model_connect_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'model_connect_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'model_connect_bc') where name = 'define_later' and id = id_;

            insert into model1._model_connect_bc_singularity
                values (id_, 'model_connect_bc', coalesce(new.cascade_mode, 'hydrograph'), new.zq_array);
            if 'model_connect_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'model_connect_bc_singularity');
            update model1._singularity set validity = (select (cascade_mode is not null) and (cascade_mode='hydrograph' or zq_array is not null) and (array_length(zq_array, 1)<=100) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) from  model1._model_connect_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._model_connect_bc_singularity set cascade_mode=new.cascade_mode, zq_array=new.zq_array where id=old.id;
            if 'model_connect_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'model_connect_bc_singularity');
            update model1._singularity set validity = (select (cascade_mode is not null) and (cascade_mode='hydrograph' or zq_array is not null) and (array_length(zq_array, 1)<=100) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) from  model1._model_connect_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._model_connect_bc_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'model_connect_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.model_connect_bc_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: network_overflow_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION network_overflow_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('network_overflow', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'network_overflow') where name = 'define_later' and id = id_;
            insert into model1._network_overflow_link
                values (id_, 'network_overflow', coalesce(new.z_overflow, st_z(st_endpoint(new.geom))), coalesce(new.area, 1));
            if 'network_overflow' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'network_overflow_link');
            update model1._link set validity = (select (z_overflow is not null) and (area is not null) and (area>=0) from  model1._network_overflow_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'network_overflow' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._network_overflow_link set z_overflow=new.z_overflow, area=new.area where id=old.id;
            if 'network_overflow' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'network_overflow_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'network_overflow' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (z_overflow is not null) and (area is not null) and (area>=0) from  model1._network_overflow_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._network_overflow_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'network_overflow' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'network_overflow' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.network_overflow_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: node_update_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION node_update_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        update model1._link set geom=ST_SetPoint(geom, 0, new.geom) where up=new.id;
        update model1._link set geom=ST_SetPoint(geom, ST_NumPoints(geom)-1, new.geom) where down=new.id;
        return new;
    end;
$$;


ALTER FUNCTION model1.node_update_fct() OWNER TO "thierry.lepelletier";

--
-- Name: op_geometric_calc_p_fct(real[]); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION op_geometric_calc_p_fct(real[]) RETURNS double precision
    LANGUAGE plpgsql IMMUTABLE
    AS $_$
    declare
        z_array alias for $1;
        x real[];
        s double precision := 0.0;
        last_h double precision := 0.0;
        last_w double precision := 0.0;
        w1 double precision := 0.0;
    begin
        foreach x slice 1 in array z_array
        loop
            s:=s+2*sqrt(pow(x[2]-last_w,2)+pow(x[1]-last_h,2))+z_array[1][2];
            last_h:=x[1];
            last_w:=x[2];
        end loop;
        return s;
    end;
$_$;


ALTER FUNCTION model1.op_geometric_calc_p_fct(real[]) OWNER TO "thierry.lepelletier";

--
-- Name: overflow_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION overflow_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('overflow', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'overflow') where name = 'define_later' and id = id_;
            insert into model1._overflow_link
                values (id_, 'overflow', new.z_crest1, new.width1, coalesce(new.z_crest2, new.z_crest1), coalesce(new.width2, 0), coalesce(new.cc, .6), coalesce(new.lateral_contraction_coef, 1), coalesce(new.break_mode, 'none'), new.z_break, new.t_break, new.z_invert, new.width_breach, coalesce(new.grp, 1), new.dt_fracw_array, new.border);
            if 'overflow' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'overflow_link');
            update model1._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>=0) and (z_crest2 is not null) and (z_crest2>=z_crest1) and (width2 is not null) and (width2>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (break_mode is not null) and (break_mode!='zw_critical' or z_break is not null) and (break_mode!='time_critical' or t_break is not null) and (break_mode='none' or width_breach is not null) and (break_mode='none' or z_invert is not null) and (break_mode='none' or grp is not null) and (break_mode='none' or grp>0) and (break_mode='none' or grp<100) and (break_mode='none' or dt_fracw_array is not null) and (break_mode='none' or array_length(dt_fracw_array, 1)<=10) and (break_mode='none' or array_length(dt_fracw_array, 1)>=1) and (break_mode='none' or array_length(dt_fracw_array, 2)=2) from  model1._overflow_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'overflow' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._overflow_link set z_crest1=new.z_crest1, width1=new.width1, z_crest2=new.z_crest2, width2=new.width2, cc=new.cc, lateral_contraction_coef=new.lateral_contraction_coef, break_mode=new.break_mode, z_break=new.z_break, t_break=new.t_break, z_invert=new.z_invert, width_breach=new.width_breach, grp=new.grp, dt_fracw_array=new.dt_fracw_array, border=new.border where id=old.id;
            if 'overflow' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'overflow_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'overflow' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>=0) and (z_crest2 is not null) and (z_crest2>=z_crest1) and (width2 is not null) and (width2>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (break_mode is not null) and (break_mode!='zw_critical' or z_break is not null) and (break_mode!='time_critical' or t_break is not null) and (break_mode='none' or width_breach is not null) and (break_mode='none' or z_invert is not null) and (break_mode='none' or grp is not null) and (break_mode='none' or grp>0) and (break_mode='none' or grp<100) and (break_mode='none' or dt_fracw_array is not null) and (break_mode='none' or array_length(dt_fracw_array, 1)<=10) and (break_mode='none' or array_length(dt_fracw_array, 1)>=1) and (break_mode='none' or array_length(dt_fracw_array, 2)=2) from  model1._overflow_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._overflow_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'overflow' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'overflow' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.overflow_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: param_headloss_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION param_headloss_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'param_headloss', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'param_headloss') where name = 'define_later' and id = id_;

            insert into model1._param_headloss_singularity
                values (id_, 'param_headloss', new.q_dz_array);
            if 'param_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'param_headloss_singularity');
            update model1._singularity set validity = (select (q_dz_array is not null) and (array_length(q_dz_array, 1)<=10) and (array_length(q_dz_array, 1)>=1) and (array_length(q_dz_array, 2)=2) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._param_headloss_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._param_headloss_singularity set q_dz_array=new.q_dz_array where id=old.id;
            if 'param_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'param_headloss_singularity');
            update model1._singularity set validity = (select (q_dz_array is not null) and (array_length(q_dz_array, 1)<=10) and (array_length(q_dz_array, 1)>=1) and (array_length(q_dz_array, 2)=2) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._param_headloss_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._param_headloss_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'param_headloss' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.param_headloss_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: pipe_branch_marker_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION pipe_branch_marker_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'pipe_branch_marker', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'pipe_branch_marker') where name = 'define_later' and id = id_;

            insert into model1._pipe_branch_marker_singularity
                values (id_, 'pipe_branch_marker', coalesce(new.pk0_km, 0), coalesce(new.dx, 50));
            if 'pipe_branch_marker' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'pipe_branch_marker_singularity');
            update model1._singularity set validity = (select (pk0_km is not null) and (dx is not null) and (dx>=0.1) from  model1._pipe_branch_marker_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._pipe_branch_marker_singularity set pk0_km=new.pk0_km, dx=new.dx where id=old.id;
            if 'pipe_branch_marker' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'pipe_branch_marker_singularity');
            update model1._singularity set validity = (select (pk0_km is not null) and (dx is not null) and (dx>=0.1) from  model1._pipe_branch_marker_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._pipe_branch_marker_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'pipe_branch_marker' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.pipe_branch_marker_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: pipe_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION pipe_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('pipe', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'pipe') where name = 'define_later' and id = id_;
            insert into model1._pipe_link
                values (id_, 'pipe', new.comment, new.z_invert_up, new.z_invert_down, coalesce(new.cross_section_type, 'circular'), coalesce(new.h_sable, 0), new.branch, new.rk, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom);
            if 'pipe' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'pipe_link');
            update model1._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  model1._pipe_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._pipe_link set comment=new.comment, z_invert_up=new.z_invert_up, z_invert_down=new.z_invert_down, cross_section_type=new.cross_section_type, h_sable=new.h_sable, branch=new.branch, rk=new.rk, circular_diameter=new.circular_diameter, ovoid_height=new.ovoid_height, ovoid_top_diameter=new.ovoid_top_diameter, ovoid_invert_diameter=new.ovoid_invert_diameter, cp_geom=new.cp_geom, op_geom=new.op_geom where id=old.id;
            if 'pipe' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'pipe_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  model1._pipe_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._pipe_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'pipe' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.pipe_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: porous_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION porous_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('porous', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'porous') where name = 'define_later' and id = id_;
            insert into model1._porous_link
                values (id_, 'porous', new.z_invert, new.width, new.transmitivity, new.border);
            if 'porous' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'porous_link');
            update model1._link set validity = (select (z_invert is not null) and (width is not null) and (width>=0) and (transmitivity is not null) and (transmitivity>=0) from  model1._porous_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'porous' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._porous_link set z_invert=new.z_invert, width=new.width, transmitivity=new.transmitivity, border=new.border where id=old.id;
            if 'porous' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'porous_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'porous' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (z_invert is not null) and (width is not null) and (width>=0) and (transmitivity is not null) and (transmitivity>=0) from  model1._porous_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._porous_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'porous' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'porous' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.porous_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: profile_from_transect(public.geometry, boolean, integer, boolean, integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION profile_from_transect(flood_plain_transect public.geometry, interpolate boolean DEFAULT false, discretization integer DEFAULT 150, ignore_points boolean DEFAULT false, distance_point integer DEFAULT 100) RETURNS real[]
    LANGUAGE plpgsql IMMUTABLE
    AS $$
    begin
        return model1._str_profile_from_transect(flood_plain_transect, interpolate, discretization, ignore_points, distance_point)::real[][];
    end;
$$;


ALTER FUNCTION model1.profile_from_transect(flood_plain_transect public.geometry, interpolate boolean, discretization integer, ignore_points boolean, distance_point integer) OWNER TO "thierry.lepelletier";

--
-- Name: pump_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION pump_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('pump', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'pump') where name = 'define_later' and id = id_;
            insert into model1._pump_link
                values (id_, 'pump', new.npump, new.zregul_array, new.hq_array);
            if 'pump' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'pump_link');
            update model1._link set validity = (select (npump is not null) and (npump<=10) and (npump>=1) and (zregul_array is not null) and (hq_array is not null) and (array_length(zregul_array, 1)=npump) and (array_length(zregul_array, 2)=2) and (array_length(hq_array, 1)=npump) and (array_length(hq_array, 2)<=10) and (array_length(hq_array, 2)>=1) and (array_length(hq_array, 3)=2) from  model1._pump_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'pump' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._pump_link set npump=new.npump, zregul_array=new.zregul_array, hq_array=new.hq_array where id=old.id;
            if 'pump' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'pump_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'pump' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (npump is not null) and (npump<=10) and (npump>=1) and (zregul_array is not null) and (hq_array is not null) and (array_length(zregul_array, 1)=npump) and (array_length(zregul_array, 2)=2) and (array_length(hq_array, 1)=npump) and (array_length(hq_array, 2)<=10) and (array_length(hq_array, 2)>=1) and (array_length(hq_array, 3)=2) from  model1._pump_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._pump_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'pump' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'pump' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.pump_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: qq_split_hydrology_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION qq_split_hydrology_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'qq_split_hydrology', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'qq_split_hydrology') where name = 'define_later' and id = id_;

            insert into model1._qq_split_hydrology_singularity
                values (id_, 'qq_split_hydrology', new.qq_array, new.downstream, coalesce(new.downstream_type, (select link_type from model1._link where id=new.downstream)), new.split1, coalesce(new.split1_type, (select link_type from model1._link where id=new.split1)), new.split2, coalesce(new.split2_type, (select link_type from model1._link where id=new.split2)));
            if 'qq_split_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'qq_split_hydrology_singularity');
            update model1._singularity set validity = (select (qq_array is not null) and (downstream is not null) and (split1 is not null) and (array_length(qq_array, 1)<=10) and (array_length(qq_array, 1)>=1) and ((split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)) from  model1._qq_split_hydrology_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._qq_split_hydrology_singularity set qq_array=new.qq_array, downstream=new.downstream, downstream_type=new.downstream_type, split1=new.split1, split1_type=new.split1_type, split2=new.split2, split2_type=new.split2_type where id=old.id;
            if 'qq_split_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'qq_split_hydrology_singularity');
            update model1._singularity set validity = (select (qq_array is not null) and (downstream is not null) and (split1 is not null) and (array_length(qq_array, 1)<=10) and (array_length(qq_array, 1)>=1) and ((split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)) from  model1._qq_split_hydrology_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._qq_split_hydrology_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'qq_split_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.qq_split_hydrology_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: reach_after_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION reach_after_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        if tg_op = 'INSERT' then
            update model1.bradley_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.bridge_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.hydraulic_cut_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.zregul_weir_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.param_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.regul_sluice_gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update model1.borda_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            return new;
        else
            if tg_op = 'UPDATE' then
                update model1.bradley_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.bridge_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return new;
            else
                update model1.bradley_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.bridge_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update model1.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return old;
            end if;
        end if;
    end;
$$;


ALTER FUNCTION model1.reach_after_fct() OWNER TO "thierry.lepelletier";

--
-- Name: reach_after_insert_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION reach_after_insert_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        _up boolean;
        _down boolean;
    begin
        select exists(select 1 from model1.river_node as n where ST_Intersects(n.geom, ST_Buffer(ST_StartPoint(new.geom), 0.1))) into _up;
        select exists(select 1 from model1.river_node as n where ST_Intersects(n.geom, ST_Buffer(ST_EndPoint(new.geom), 0.1))) into _down;

        if not _up then
            insert into model1.river_node(geom) values(ST_StartPoint(new.geom));
        end if;
        if not _down then
            insert into model1.river_node(geom) values(ST_EndPoint(new.geom));
        end if;
        return new;
    end;
$$;


ALTER FUNCTION model1.reach_after_insert_fct() OWNER TO "thierry.lepelletier";

--
-- Name: reach_after_update_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION reach_after_update_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        update model1.river_node set
                -- il faut valider cette mise Ã  jour du pk
                geom = (select ST_LineInterpolatePoint(new.geom, ST_LineLocatePoint(new.geom, geom)))
                where reach=new.id
                    and id not in (select id from model1.river_node as n where ST_Intersects(n.geom, ST_LineInterpolatePoint(new.geom, ST_LineLocatePoint(new.geom, n.geom))));
        return new;
    end;
$$;


ALTER FUNCTION model1.reach_after_update_fct() OWNER TO "thierry.lepelletier";

--
-- Name: regul_gate_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION regul_gate_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('regul_gate', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'regul_gate') where name = 'define_later' and id = id_;
            insert into model1._regul_gate_link
                values (id_, 'regul_gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.z_invert_stop, new.z_invert), coalesce(new.z_ceiling_stop, new.z_ceiling), coalesce(new.v_max_cms, .5), coalesce(new.dt_regul_hr, 0), coalesce(new.mode_regul, 'elevation'), new.z_control_node, coalesce(new.z_pid_array, '{1, 0, 0}'::real[]), new.z_tz_array, new.q_z_crit, new.q_tq_array, coalesce(new.nr_z_gate, new.z_ceiling));
            if 'regul_gate' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'regul_gate_link');
            update model1._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) from  model1._regul_gate_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'regul_gate' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._regul_gate_link set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, z_invert_stop=new.z_invert_stop, z_ceiling_stop=new.z_ceiling_stop, v_max_cms=new.v_max_cms, dt_regul_hr=new.dt_regul_hr, mode_regul=new.mode_regul, z_control_node=new.z_control_node, z_pid_array=new.z_pid_array, z_tz_array=new.z_tz_array, q_z_crit=new.q_z_crit, q_tq_array=new.q_tq_array, nr_z_gate=new.nr_z_gate where id=old.id;
            if 'regul_gate' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'regul_gate_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'regul_gate' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) from  model1._regul_gate_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._regul_gate_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'regul_gate' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'regul_gate' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.regul_gate_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: regul_sluice_gate_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION regul_sluice_gate_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'regul_sluice_gate', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'regul_sluice_gate') where name = 'define_later' and id = id_;

            insert into model1._regul_sluice_gate_singularity
                values (id_, 'regul_sluice_gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.z_invert_stop, new.z_invert), coalesce(new.z_ceiling_stop, new.z_ceiling), coalesce(new.v_max_cms, .5), coalesce(new.dt_regul_hr, 0), coalesce(new.mode_regul, 'elevation'), new.z_control_node, coalesce(new.z_pid_array, '{1, 0, 0}'::real[]), new.z_tz_array, new.q_z_crit, new.q_tq_array, coalesce(new.nr_z_gate, new.z_ceiling));
            if 'regul_sluice_gate' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'regul_sluice_gate_singularity');
            update model1._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) from  model1._regul_sluice_gate_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._regul_sluice_gate_singularity set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, z_invert_stop=new.z_invert_stop, z_ceiling_stop=new.z_ceiling_stop, v_max_cms=new.v_max_cms, dt_regul_hr=new.dt_regul_hr, mode_regul=new.mode_regul, z_control_node=new.z_control_node, z_pid_array=new.z_pid_array, z_tz_array=new.z_tz_array, q_z_crit=new.q_z_crit, q_tq_array=new.q_tq_array, nr_z_gate=new.nr_z_gate where id=old.id;
            if 'regul_sluice_gate' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'regul_sluice_gate_singularity');
            update model1._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) from  model1._regul_sluice_gate_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._regul_sluice_gate_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'regul_sluice_gate' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.regul_sluice_gate_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: reservoir_rs_hydrology_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION reservoir_rs_hydrology_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'reservoir_rs_hydrology', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'reservoir_rs_hydrology') where name = 'define_later' and id = id_;

            insert into model1._reservoir_rs_hydrology_singularity
                values (id_, 'reservoir_rs_hydrology', new.drainage, coalesce(new.drainage_type, (select link_type from model1._link where id=new.drainage)), new.overflow, coalesce(new.overflow_type, (select link_type from model1._link where id=new.overflow)), new.q_drainage, coalesce(new.z_ini, new.zs_array[1][1]), new.zs_array, coalesce(new.treatment_mode, 'residual_concentration'), new.treatment_param::json);
            if 'reservoir_rs_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'reservoir_rs_hydrology_singularity');
            update model1._singularity set validity = (select (drainage is not null) and (overflow is not null) and (q_drainage is not null) and (q_drainage>=0) and (z_ini is not null) and (zs_array is not null ) and (treatment_mode is not null) and (array_length(zs_array, 1)<=10) and (array_length(zs_array, 1)>=1) and (array_length(zs_array, 2)=2) from  model1._reservoir_rs_hydrology_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._reservoir_rs_hydrology_singularity set drainage=new.drainage, drainage_type=new.drainage_type, overflow=new.overflow, overflow_type=new.overflow_type, q_drainage=new.q_drainage, z_ini=new.z_ini, zs_array=new.zs_array, treatment_mode=new.treatment_mode, treatment_param=new.treatment_param::json where id=old.id;
            if 'reservoir_rs_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'reservoir_rs_hydrology_singularity');
            update model1._singularity set validity = (select (drainage is not null) and (overflow is not null) and (q_drainage is not null) and (q_drainage>=0) and (z_ini is not null) and (zs_array is not null ) and (treatment_mode is not null) and (array_length(zs_array, 1)<=10) and (array_length(zs_array, 1)>=1) and (array_length(zs_array, 2)=2) from  model1._reservoir_rs_hydrology_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._reservoir_rs_hydrology_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'reservoir_rs_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.reservoir_rs_hydrology_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: reservoir_rsp_hydrology_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION reservoir_rsp_hydrology_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'reservoir_rsp_hydrology', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'reservoir_rsp_hydrology') where name = 'define_later' and id = id_;

            insert into model1._reservoir_rsp_hydrology_singularity
                values (id_, 'reservoir_rsp_hydrology', new.drainage, coalesce(new.drainage_type, (select link_type from model1._link where id=new.drainage)), new.overflow, coalesce(new.overflow_type, (select link_type from model1._link where id=new.overflow)), coalesce(new.z_ini, new.zr_sr_qf_qs_array[1][1]), new.zr_sr_qf_qs_array, coalesce(new.treatment_mode, 'residual_concentration'), new.treatment_param::json);
            if 'reservoir_rsp_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'reservoir_rsp_hydrology_singularity');
            update model1._singularity set validity = (select (drainage is not null) and (overflow is not null) and (z_ini is not null) and (zr_sr_qf_qs_array is not null ) and (treatment_mode is not null) and (treatment_param is not null) and (array_length(zr_sr_qf_qs_array, 1)<=10) and (array_length(zr_sr_qf_qs_array, 1)>=1) and (array_length(zr_sr_qf_qs_array, 2)=4) from  model1._reservoir_rsp_hydrology_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._reservoir_rsp_hydrology_singularity set drainage=new.drainage, drainage_type=new.drainage_type, overflow=new.overflow, overflow_type=new.overflow_type, z_ini=new.z_ini, zr_sr_qf_qs_array=new.zr_sr_qf_qs_array, treatment_mode=new.treatment_mode, treatment_param=new.treatment_param::json where id=old.id;
            if 'reservoir_rsp_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'reservoir_rsp_hydrology_singularity');
            update model1._singularity set validity = (select (drainage is not null) and (overflow is not null) and (z_ini is not null) and (zr_sr_qf_qs_array is not null ) and (treatment_mode is not null) and (treatment_param is not null) and (array_length(zr_sr_qf_qs_array, 1)<=10) and (array_length(zr_sr_qf_qs_array, 1)>=1) and (array_length(zr_sr_qf_qs_array, 2)=4) from  model1._reservoir_rsp_hydrology_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._reservoir_rsp_hydrology_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'reservoir_rsp_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.reservoir_rsp_hydrology_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: river_cross_section_pl1d_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION river_cross_section_pl1d_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id from model1.river_node where ST_DWithin(new.profile, geom, .1) into id_;
            if id_ is null then
                raise exception 'no river node nearby river_cross_section_pl1d %', st_astext(new.geom);
            end if;
            insert into model1._river_cross_section_pl1d
                values (id_, coalesce(new.name, 'PL1D'||id_::varchar), new.profile, new.generated);
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update model1._river_cross_section_pl1d set name=new.name, profile=new.profile, generated=new.generated where id=old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._river_cross_section_pl1d where id=old.id;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.river_cross_section_pl1d_fct() OWNER TO "thierry.lepelletier";

--
-- Name: river_cross_section_profile_del_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION river_cross_section_profile_del_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        delete from model1._river_cross_section_profile where id=old.id;
        return old;
    end;
$$;


ALTER FUNCTION model1.river_cross_section_profile_del_fct() OWNER TO "thierry.lepelletier";

--
-- Name: river_cross_section_profile_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION river_cross_section_profile_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        select (
            (new.type_cross_section_up is null or (
                new.z_invert_up is not null
                and new.up_rk is not null
                and (new.type_cross_section_up!='circular' or (new.up_circular_diameter is not null and new.up_circular_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_top_diameter is not null and new.up_ovoid_top_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_invert_diameter is not null and new.up_ovoid_invert_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height is not null and new.up_ovoid_height >0 ))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height > (new.up_ovoid_top_diameter+new.up_ovoid_invert_diameter)/2))
                and (new.type_cross_section_up!='pipe' or new.up_cp_geom is not null)
                and (new.type_cross_section_up!='channel' or new.up_op_geom is not null)
                and (new.type_cross_section_up!='valley'
                        or (new.up_rk = 0 and new.up_rk_maj = 0 and new.up_sinuosity = 0)
                        or (new.up_rk >0 and new.up_rk_maj is not null and new.up_rk_maj > 0 and new.up_sinuosity is not null and new.up_sinuosity > 0))
                and (new.type_cross_section_up!='valley' or new.up_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_down is null or (
                new.z_invert_down is not null
                and new.down_rk is not null
                and (new.type_cross_section_down!='circular' or (new.down_circular_diameter is not null and new.down_circular_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_top_diameter is not null and new.down_ovoid_top_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_invert_diameter is not null and new.down_ovoid_invert_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height is not null and new.down_ovoid_height >0 ))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height > (new.down_ovoid_top_diameter+new.down_ovoid_invert_diameter)/2))
                and (new.type_cross_section_down!='pipe' or new.down_cp_geom is not null)
                and (new.type_cross_section_down!='channel' or new.down_op_geom is not null)
                and (new.type_cross_section_down!='valley'
                        or (new.down_rk = 0 and new.down_rk_maj = 0 and new.down_sinuosity = 0)
                        or (new.down_rk >0 and new.down_rk_maj is not null and new.down_rk_maj > 0 and new.down_sinuosity is not null and new.down_sinuosity > 0))
                and (new.type_cross_section_down!='valley' or new.down_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_up is null or new.type_cross_section_down is null or (
                new.z_invert_up is not null and new.z_invert_down is not null
                )
            )
        ) into new.validity;

        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id from model1.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;
            insert into model1._river_cross_section_profile
                values (
                    new.id,
                    new.name,
                    new.z_invert_up,
                    new.z_invert_down,
                    new.type_cross_section_up,
                    new.type_cross_section_down,
                    new.up_rk,
                    new.up_rk_maj,
                    new.up_sinuosity,
                    new.up_circular_diameter,
                    new.up_ovoid_height,
                    new.up_ovoid_top_diameter,
                    new.up_ovoid_invert_diameter,
                    new.up_cp_geom,
                    new.up_op_geom,
                    new.up_vcs_geom,
                    new.up_vcs_topo_geom,
                    new.down_rk,
                    new.down_rk_maj,
                    new.down_sinuosity,
                    new.down_circular_diameter,
                    new.down_ovoid_height,
                    new.down_ovoid_top_diameter,
                    new.down_ovoid_invert_diameter,
                    new.down_cp_geom,
                    new.down_op_geom,
                    new.down_vcs_geom,
                    new.down_vcs_topo_geom,
                    new.validity
                );
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id from model1.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;
            update model1._river_cross_section_profile set
                id=new.id,
                name=new.name,
                z_invert_up=new.z_invert_up,
                z_invert_down=new.z_invert_down,
                type_cross_section_up=new.type_cross_section_up,
                type_cross_section_down=new.type_cross_section_down,
                up_rk=new.up_rk,
                up_rk_maj=new.up_rk_maj,
                up_sinuosity=new.up_sinuosity,
                up_circular_diameter=new.up_circular_diameter,
                up_ovoid_height=new.up_ovoid_height,
                up_ovoid_top_diameter=new.up_ovoid_top_diameter,
                up_ovoid_invert_diameter=new.up_ovoid_invert_diameter,
                up_cp_geom=new.up_cp_geom,
                up_op_geom=new.up_op_geom,
                up_vcs_geom=new.up_vcs_geom,
                up_vcs_topo_geom=new.up_vcs_topo_geom,
                down_rk=new.down_rk,
                down_rk_maj=new.down_rk_maj,
                down_sinuosity=new.down_sinuosity,
                down_circular_diameter=new.down_circular_diameter,
                down_ovoid_height=new.down_ovoid_height,
                down_ovoid_top_diameter=new.down_ovoid_top_diameter,
                down_ovoid_invert_diameter=new.down_ovoid_invert_diameter,
                down_cp_geom=new.down_cp_geom,
                down_op_geom=new.down_op_geom,
                down_vcs_geom=new.down_vcs_geom,
                down_vcs_topo_geom=new.down_vcs_topo_geom,
                validity=new.validity
            where id=old.id;
            return new;
        end if;
    end;
$$;


ALTER FUNCTION model1.river_cross_section_profile_fct() OWNER TO "thierry.lepelletier";

--
-- Name: river_node_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION river_node_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into model1._node(node_type, name, geom, configuration, generated)
                values('river', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'river') where name = 'define_later' and id = id_;
            insert into model1._river_node
                values (id_, 'river', coalesce(new.reach, (select id from model1.reach where ST_DWithin(new.geom, geom, .1))), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.area, 1));
            perform model1.add_configuration_fct(new.configuration::json, id_, 'river_node');

            -- Lines to update specific nodes that works with associated contours
            if 'river' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'river' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from model1.station where st_intersects(geom, new.geom)))) from  model1._river_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update model1._node set name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._river_node set reach=new.reach, z_ground=new.z_ground, area=new.area where id=old.id;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'river_node');

            -- Lines to update specific nodes that works with associated contours
            if 'river' = 'catchment' then
                update model1._catchment_node set contour=(select id from model1.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'river' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'river' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from model1.station where st_intersects(geom, new.geom)))) from  model1._river_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'river' = 'storage' then
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;
            if 'river' = 'crossroad' then
                perform model1.update_street_links();
            end if;

            delete from model1._river_node where id=old.id;
            delete from model1._node where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION model1.river_node_fct() OWNER TO "thierry.lepelletier";

--
-- Name: routing_hydrology_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION routing_hydrology_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('routing_hydrology', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'routing_hydrology') where name = 'define_later' and id = id_;
            insert into model1._routing_hydrology_link
                values (id_, 'routing_hydrology', coalesce(new.cross_section, 1), coalesce(new.length, 0.1), coalesce(new.slope, .01), coalesce(new.hydrograph, (select hbc.id from model1.hydrograph_bc_singularity as hbc where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1))));
            if 'routing_hydrology' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'routing_hydrology_link');
            update model1._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  model1._routing_hydrology_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'routing_hydrology' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._routing_hydrology_link set cross_section=new.cross_section, length=new.length, slope=new.slope, hydrograph=new.hydrograph where id=old.id;
            if 'routing_hydrology' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'routing_hydrology_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'routing_hydrology' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  model1._routing_hydrology_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._routing_hydrology_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'routing_hydrology' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'routing_hydrology' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.routing_hydrology_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: set_link_altitude(integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION set_link_altitude(link_id integer) RETURNS integer
    LANGUAGE plpython3u
    AS $$
    import plpy
    from hydra_1_0_0 import set_link_altitude
    return set_link_altitude(plpy, link_id, 'model1', 2154)
$$;


ALTER FUNCTION model1.set_link_altitude(link_id integer) OWNER TO "thierry.lepelletier";

--
-- Name: station_after_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION station_after_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        if tg_op = 'INSERT' then
            update model1.river_node
                set geom=geom
                where st_intersects(geom, new.geom);
            update model1.manhole_node
                set geom=geom
                where st_intersects(geom, new.geom);
            return new;
        else
            if tg_op = 'UPDATE' then
                update model1.river_node
                    set geom=geom
                    where st_intersects(geom, old.geom) or st_intersects(geom, new.geom);
                update model1.manhole_node
                    set geom=geom
                    where st_intersects(geom, old.geom) or st_intersects(geom, new.geom);
                return new;
            else
                update model1.river_node
                    set geom=geom
                    where st_intersects(geom, old.geom);
                update model1.manhole_node
                    set geom=geom
                    where st_intersects(geom, old.geom);
                return old;
            end if;
        end if;
    end;
$$;


ALTER FUNCTION model1.station_after_fct() OWNER TO "thierry.lepelletier";

--
-- Name: station_node_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION station_node_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into model1._node(node_type, name, geom, configuration, generated)
                values('station', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'station') where name = 'define_later' and id = id_;
            insert into model1._station_node
                values (id_, 'station', coalesce(new.area, 1), coalesce(new.z_invert, (select project.altitude(new.geom))), coalesce(new.station, (select id from model1.station where ST_Intersects(new.geom, geom))));
            perform model1.add_configuration_fct(new.configuration::json, id_, 'station_node');

            -- Lines to update specific nodes that works with associated contours
            if 'station' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'station' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area is not null) and (area>0) and (z_invert is not null) and (station is not null ) and ((select st_intersects(new.geom, geom) from model1.station where id=new.station)) from  model1._station_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update model1._node set name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._station_node set area=new.area, z_invert=new.z_invert, station=new.station where id=old.id;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'station_node');

            -- Lines to update specific nodes that works with associated contours
            if 'station' = 'catchment' then
                update model1._catchment_node set contour=(select id from model1.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'station' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'station' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (area is not null) and (area>0) and (z_invert is not null) and (station is not null ) and ((select st_intersects(new.geom, geom) from model1.station where id=new.station)) from  model1._station_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'station' = 'storage' then
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;
            if 'station' = 'crossroad' then
                perform model1.update_street_links();
            end if;

            delete from model1._station_node where id=old.id;
            delete from model1._node where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION model1.station_node_fct() OWNER TO "thierry.lepelletier";

--
-- Name: storage_node_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION storage_node_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into model1._node(node_type, name, geom, configuration, generated)
                values('storage', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'storage') where name = 'define_later' and id = id_;
            insert into model1._storage_node
                values (id_, 'storage', new.zs_array, new.zini, coalesce(new.contour, (select id from model1.coverage where st_intersects(geom, new.geom))));
            perform model1.add_configuration_fct(new.configuration::json, id_, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'storage' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10 ) and (array_length(zs_array, 1) >=1) and (array_length(zs_array, 2)=2) from  model1._storage_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update model1._node set name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._storage_node set zs_array=new.zs_array, zini=new.zini, contour=new.contour where id=old.id;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'catchment' then
                update model1._catchment_node set contour=(select id from model1.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'storage' = 'storage' then
                update model1._storage_node set contour=(select id from model1.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update model1.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'storage' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform model1.update_street_links();
            end if;

            update model1._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10 ) and (array_length(zs_array, 1) >=1) and (array_length(zs_array, 2)=2) from  model1._storage_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update model1.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;
            if 'storage' = 'crossroad' then
                perform model1.update_street_links();
            end if;

            delete from model1._storage_node where id=old.id;
            delete from model1._node where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION model1.storage_node_fct() OWNER TO "thierry.lepelletier";

--
-- Name: street_after_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION street_after_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        res integer;
    begin
        if new.width != 0 then
            if tg_op = 'INSERT' or (tg_op = 'UPDATE' and old.width=0) then
                perform model1.gen_cst_street(new.id);
            end if;
        end if;

        perform model1.crossroad_update();

        return new;
    end;
$$;


ALTER FUNCTION model1.street_after_fct() OWNER TO "thierry.lepelletier";

--
-- Name: street_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION street_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('street', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'street') where name = 'define_later' and id = id_;
            insert into model1._street_link
                values (id_, 'street', new.width, coalesce(new.length, ST_Length(new.geom)), coalesce(new.rk, 30.));
            if 'street' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'street_link');
            update model1._link set validity = (select (width is not null) and (width>=0) and (length is not null) and (length>=0) and (rk is not null) and (rk>0) from  model1._street_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'street' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._street_link set width=new.width, length=new.length, rk=new.rk where id=old.id;
            if 'street' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'street_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'street' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (width is not null) and (width>=0) and (length is not null) and (length>=0) and (rk is not null) and (rk>0) from  model1._street_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._street_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'street' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'street' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.street_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: strickler_bc_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION strickler_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'strickler_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'strickler_bc') where name = 'define_later' and id = id_;

            insert into model1._strickler_bc_singularity
                values (id_, 'strickler_bc', new.slope, new.k, new.width);
            if 'strickler_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'strickler_bc_singularity');
            update model1._singularity set validity = (select (slope is not null) and (k is not null) and (k>0) and (width is not null) and (width>=0) from  model1._strickler_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._strickler_bc_singularity set slope=new.slope, k=new.k, width=new.width where id=old.id;
            if 'strickler_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'strickler_bc_singularity');
            update model1._singularity set validity = (select (slope is not null) and (k is not null) and (k>0) and (width is not null) and (width>=0) from  model1._strickler_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._strickler_bc_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'strickler_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.strickler_bc_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: strickler_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION strickler_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('strickler', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'strickler') where name = 'define_later' and id = id_;
            insert into model1._strickler_link
                values (id_, 'strickler', new.z_crest1, new.width1, coalesce(new.length, ST_Length(new.geom)), coalesce(new.rk, 12), coalesce(new.z_crest2, new.z_crest1+0.001), coalesce(new.width2, new.width1+0.001), new.border);
            if 'strickler' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'strickler_link');
            update model1._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>0) and (length is not null) and (length>0) and (rk is not null) and (rk>=0) and (z_crest2 is not null) and (z_crest2>z_crest1) and (width2 is not null) and (width2>width1) from  model1._strickler_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'strickler' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._strickler_link set z_crest1=new.z_crest1, width1=new.width1, length=new.length, rk=new.rk, z_crest2=new.z_crest2, width2=new.width2, border=new.border where id=old.id;
            if 'strickler' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'strickler_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'strickler' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>0) and (length is not null) and (length>0) and (rk is not null) and (rk>=0) and (z_crest2 is not null) and (z_crest2>z_crest1) and (width2 is not null) and (width2>width1) from  model1._strickler_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._strickler_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'strickler' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'strickler' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.strickler_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: tank_bc_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION tank_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'tank_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'tank_bc') where name = 'define_later' and id = id_;

            insert into model1._tank_bc_singularity
                values (id_, 'tank_bc', new.zs_array, new.zini);
            if 'tank_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'tank_bc_singularity');
            update model1._singularity set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10) and (array_length(zs_array, 1)>=1) and (array_length(zs_array, 2)=2) from  model1._tank_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._tank_bc_singularity set zs_array=new.zs_array, zini=new.zini where id=old.id;
            if 'tank_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'tank_bc_singularity');
            update model1._singularity set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10) and (array_length(zs_array, 1)>=1) and (array_length(zs_array, 2)=2) from  model1._tank_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._tank_bc_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'tank_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.tank_bc_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: tz_bc_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION tz_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'tz_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'tz_bc') where name = 'define_later' and id = id_;

            insert into model1._tz_bc_singularity
                values (id_, 'tz_bc', new.tz_array, coalesce(new.cyclic, 'f'), coalesce(new.external_file_data, 'f'));
            if 'tz_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'tz_bc_singularity');
            update model1._singularity set validity = (select (external_file_data or tz_array is not null) and (external_file_data or array_length(tz_array, 1)<=10) and (external_file_data or array_length(tz_array, 1)>=1) and (external_file_data or array_length(tz_array, 2)=2) from  model1._tz_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._tz_bc_singularity set tz_array=new.tz_array, cyclic=new.cyclic, external_file_data=new.external_file_data where id=old.id;
            if 'tz_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'tz_bc_singularity');
            update model1._singularity set validity = (select (external_file_data or tz_array is not null) and (external_file_data or array_length(tz_array, 1)<=10) and (external_file_data or array_length(tz_array, 1)>=1) and (external_file_data or array_length(tz_array, 2)=2) from  model1._tz_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._tz_bc_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'tz_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.tz_bc_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: update_river_cross_section_pl1d(integer); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION update_river_cross_section_pl1d(coverage_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    declare
        prec_ real;
        res_ integer;
        reach_id_ integer;
    begin
        select precision from hydra.metadata into prec_;

        select r.id
        from model1.coverage as c, model1.reach as r
        where st_intersects(r.geom, c.geom)
        and c.id=coverage_id
        and st_intersects(r.geom, c.geom)
        limit 1
        into reach_id_;

        delete from model1.river_cross_section_pl1d
        where generated is not null;

        -- create banks
        drop table if exists banks;
        create  table banks
        as
        with res as (
            select (st_dump(st_linemerge((st_collectionextract(st_intersection(cs.discretized, cv.geom), 2))))).geom as geom
            from model1.coverage as cv, model1.constrain as cs, model1.reach as r
            where st_intersects(r.geom, cv.geom)
            and cv.id=coverage_id
            and r.id = reach_id_
            and not st_intersects(r.geom, cs.geom)
        )
        select row_number() over() as id, geom::geometry('LINESTRINGZ', 2154) from res
        ;


        -- river points that form the bedline are the one
        -- which are the closest point of their projection and the banks
        drop table if exists bedline;
        create  table bedline
        as
        with projected as (
            select b.id as bank, st_lineinterpolatepoint(b.geom, st_linelocatepoint(b.geom, r.geom)) as geom
            from (
                select (st_dumppoints(geom)).geom as geom
                from model1.reach where id=reach_id_
                union
                select n.geom
                from model1.river_node as n
                where n.reach=reach_id_
                ) as r, banks as b
            union
            select b.id as bank, (st_dumppoints(b.geom)).geom as geom
            from banks as b
        ),
        closest as (
            select b.bank, st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, b.geom)) as geom
            from model1.reach as r, projected as b
            where r.id=reach_id_
        )
        select row_number() over() as id, c.bank, st_makeline(c.geom order by st_linelocatepoint(b.geom, c.geom)) as geom
        from closest as c, banks as b
        where c.bank=b.id
        group by c.bank
        ;

        drop table if exists midline;
        create  table midline
        as
        select row_number() over() as id, st_makeline(st_centroid(st_makeline(p.geom, st_closestpoint(b.geom, p.geom)))) as geom, p.bank
        from banks as b, (select (st_dumppoints(geom)).geom as geom, bank from bedline) as p
        where b.id=p.bank
        group by p.bank
        ;

        drop table if exists pl1d;
        create  table pl1d
        as
        select  row_number() over() as id,
            st_makeline(st_lineinterpolatepoint((select geom from banks where id = 1), i::real/10),
                        st_lineinterpolatepoint((select geom from banks where id = 2), i::real/10)) as geom,
                    1 as node
        from generate_series(1, 10) as i
        ;
        return 1;
    end;
$$;


ALTER FUNCTION model1.update_river_cross_section_pl1d(coverage_id integer) OWNER TO "thierry.lepelletier";

--
-- Name: update_street_links(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION update_street_links() RETURNS integer
    LANGUAGE plpgsql
    AS $$
    declare
        prec_ real;
        res_ integer;
    begin
        select precision from hydra.metadata into prec_;

        delete from model1.street_link
        where generated is not null;

        with ins as (
            select rk, width, st_makeline(
                lag(n.geom)
                over (partition by s.id order by st_linelocatepoint(s.geom, n.geom)),
                n.geom) as geom, n.generated
            from model1.street as s, model1.crossroad_node as n
            where st_dwithin(s.geom, n.geom, prec_)
        ),
        gen as (
            insert into model1.generation_step default values returning id
        ),
        links as (
            insert into model1.street_link(rk, width, geom, generated)
            select rk, width, geom, gen.id from ins, gen where geom is not null
            returning id
        )
        select count(1) from links into res_;
        return res_;
    end;
$$;


ALTER FUNCTION model1.update_street_links() OWNER TO "thierry.lepelletier";

--
-- Name: update_z(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION update_z() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    begin

        update model1.river_node set geom=('SRID=2154;POINTZ('||ST_X(geom)||' '||ST_Y(geom)||' '||z_ground||')')::geometry;

        update model1.pipe_link as p set geom=model1.line_set_z(geom, z_invert_up, z_invert_down);


        -- find reach that are missing sections at end points, interpolate the sections

        /*
        -- the
        with d as (select id, (ST_DumpPoints(geom)).geom as geom
            from model1.reach
            union
            select reach as id, geom
            from model1.river_node),
        da as (select d.id, ST_X(d.geom) as x, ST_Y(d.geom) as y, ST_LineLocatePoint(l.geom, d.geom) as a
            from d join model1.reach as l on l.id=d.id),
        p as (select n.id, n.reach, t.z_invert_up, t.z_invert_down, ST_LineLocatePoint(r.geom, t.geom) as a
            from model1.river_node as n
            join model1.river_cross_section_profile as t on n.id=t.id
            join model1.reach as r on n.reach=r.id),
        i as (select id, x, y, a,
            (select max(a) from p where p.a <= da.a and p.reach=da.id) as au,
            (select min(a) from p where p.a >= da.a and p.reach=da.id) as ad
            from da),
        j as (select id, x, y, a, au, ad, (a-au)/(ad-au) as ratio
            from i where ad > au
            union
            select id, x, y, a, au, ad, 1. as ratio from i where ad=au),
        k as (select id, x, y,
            (select coalesce(z_invert_down, z_invert_up)*ratio + coalesce(z_invert_up, z_invert_down)*(1-ratio) from p where p.a=j.a) as z from j)
        select * from k;

        -- split the reach into measured segments
        -- give altitude to segments
        -- add discontinuties
        -- linemerge
        with s as (
            select ST_LineSubstring(r.l, u.a, d.a) as geom,
            coalesce(u.z_invert_down, u.z_invert_up) as z_up,
            coalesce(u.z_invert_up, u.z_invert_down) as z_down

        )


        --river_cross_section_profile
        */

        return 't';

    end;
$$;


ALTER FUNCTION model1.update_z() OWNER TO "thierry.lepelletier";

--
-- Name: upstream_valley_section(public.geometry); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION upstream_valley_section(transect public.geometry) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
    begin
        return (
        with reach as (
            select r.id, st_intersection(r.geom, transect) as inter, geom, pk0_km
            from model1.reach as r
            where st_intersects(r.geom, transect)
        ),
        pk as (
            select st_linelocatepoint(geom, inter)*st_length(geom)/1000+pk0_km as pk_km from reach
        ),
        up_reach as (
            select nup.reach as id
            from model1.river_node as nup
            join model1.connector_link as l on l.up=nup.id
            join model1.river_node as ndown on ndown.id=l.down
            where l.main_branch
            and ndown.reach = (select id from reach)
        ),
        extended_reach_node as (
            select n.id, n.pk_km
            from model1.river_node as n, pk
            where n.reach in (select id from reach union select id from up_reach)
            and n.pk_km <= pk.pk_km
        ),
        section_up as (
            select c.id
            from extended_reach_node as n
            join model1.river_cross_section_profile as c on c.id = n.id,
            pk
            where ((c.type_cross_section_up='valley' and c.up_vcs_geom is not null)
                or (c.type_cross_section_down='valley' and c.down_vcs_geom is not null))
            order by n.pk_km desc
            limit 1
        )
        select id from section_up);
    end;
$$;


ALTER FUNCTION model1.upstream_valley_section(transect public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: weir_bc_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION weir_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'weir_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'weir_bc') where name = 'define_later' and id = id_;

            insert into model1._weir_bc_singularity
                values (id_, 'weir_bc', new.z_weir, new.width, coalesce(new.cc, .6));
            if 'weir_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'weir_bc_singularity');
            update model1._singularity set validity = (select (z_weir is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0.) from  model1._weir_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._weir_bc_singularity set z_weir=new.z_weir, width=new.width, cc=new.cc where id=old.id;
            if 'weir_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'weir_bc_singularity');
            update model1._singularity set validity = (select (z_weir is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0.) from  model1._weir_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._weir_bc_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'weir_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.weir_bc_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: weir_link_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION weir_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
    begin
        if tg_op = 'INSERT' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into model1._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('weir', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update model1._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'weir') where name = 'define_later' and id = id_;
            insert into model1._weir_link
                values (id_, 'weir', new.z_invert, new.width, coalesce(new.cc, .6), new.z_weir, coalesce(new.v_max_cms, .5));
            if 'weir' = 'pipe' then
                perform model1.branch_update_fct();
            end if;
            --perform model1.set_link_altitude(id_);
            perform model1.add_configuration_fct(new.configuration::json, id_, 'weir_link');
            update model1._link set validity = (select (z_invert is not null ) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (v_max_cms is not null) and (v_max_cms>=0) from  model1._weir_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'weir' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            select * from model1.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update model1._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update model1._weir_link set z_invert=new.z_invert, width=new.width, cc=new.cc, z_weir=new.z_weir, v_max_cms=new.v_max_cms where id=old.id;
            if 'weir' = 'pipe' then
               perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'weir_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'weir' = 'pipe' then
                update model1.manhole_node set geom=geom where id=new.up or id=new.down or id=old.up or id=old.down;
            end if;

            update model1._link set validity = (select (z_invert is not null ) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (v_max_cms is not null) and (v_max_cms>=0) from  model1._weir_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._weir_link where id=old.id;
            delete from model1._link where id=old.id;
            if 'weir' = 'pipe' then
               perform model1.branch_update_fct();

            -- Update of connected manholes for connectivity check if pipe
            if 'weir' = 'pipe' then
                update model1.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;

            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.weir_link_fct() OWNER TO "thierry.lepelletier";

--
-- Name: zq_bc_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION zq_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'zq_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'zq_bc') where name = 'define_later' and id = id_;

            insert into model1._zq_bc_singularity
                values (id_, 'zq_bc', new.zq_array);
            if 'zq_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'zq_bc_singularity');
            update model1._singularity set validity = (select (zq_array is not null) and (array_length(zq_array, 1)<=10) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) from  model1._zq_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._zq_bc_singularity set zq_array=new.zq_array where id=old.id;
            if 'zq_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'zq_bc_singularity');
            update model1._singularity set validity = (select (zq_array is not null) and (array_length(zq_array, 1)<=10) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) from  model1._zq_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._zq_bc_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'zq_bc' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.zq_bc_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: zq_split_hydrology_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION zq_split_hydrology_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'zq_split_hydrology', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'zq_split_hydrology') where name = 'define_later' and id = id_;

            insert into model1._zq_split_hydrology_singularity
                values (id_, 'zq_split_hydrology', new.downstream, coalesce(new.downstream_type, (select link_type from model1._link where id=new.downstream)), new.split1, coalesce(new.split1_type, (select link_type from model1._link where id=new.split1)), new.split2, coalesce(new.split2_type, (select link_type from model1._link where id=new.split2)), coalesce(new.downstream_law, 'weir'), new.downstream_param::json, new.split1_law, new.split1_param::json, new.split2_law, new.split2_param::json);
            if 'zq_split_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'zq_split_hydrology_singularity');
            update model1._singularity set validity = (select (downstream is not null) and (split1 is not null) and (downstream_param is not null) and (split1_law is not null) and (split1_param is not null) and (split2 is null or split2_law is not null) and (split2 is null or split2_param is not null) from  model1._zq_split_hydrology_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._zq_split_hydrology_singularity set downstream=new.downstream, downstream_type=new.downstream_type, split1=new.split1, split1_type=new.split1_type, split2=new.split2, split2_type=new.split2_type, downstream_law=new.downstream_law, downstream_param=new.downstream_param::json, split1_law=new.split1_law, split1_param=new.split1_param::json, split2_law=new.split2_law, split2_param=new.split2_param::json where id=old.id;
            if 'zq_split_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'zq_split_hydrology_singularity');
            update model1._singularity set validity = (select (downstream is not null) and (split1 is not null) and (downstream_param is not null) and (split1_law is not null) and (split1_param is not null) and (split2 is null or split2_law is not null) and (split2 is null or split2_param is not null) from  model1._zq_split_hydrology_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._zq_split_hydrology_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'zq_split_hydrology' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.zq_split_hydrology_singularity_fct() OWNER TO "thierry.lepelletier";

--
-- Name: zregul_weir_singularity_fct(); Type: FUNCTION; Schema: model1; Owner: thierry.lepelletier
--

CREATE FUNCTION zregul_weir_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',2154);
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into model1._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'zregul_weir', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update model1._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'zregul_weir') where name = 'define_later' and id = id_;

            insert into model1._zregul_weir_singularity
                values (id_, 'zregul_weir', new.z_invert, new.z_regul, new.width, coalesce(new.cc, .6), coalesce(new.mode_regul, 'elevation'), coalesce(new.reoxy_law, 'gameson'), new.reoxy_param::json);
            if 'zregul_weir' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, id_, 'zregul_weir_singularity');
            update model1._singularity set validity = (select (z_invert is not null ) and (z_regul is not null ) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (mode_regul is not null) and (reoxy_law is not null) and (reoxy_param is not null) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._zregul_weir_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id, node_type, geom from model1._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from model1._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from model1._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update model1._singularity set node=nid_, node_type=node_type_, name=new.name, configuration=new.configuration::json where id=old.id;
            update model1._zregul_weir_singularity set z_invert=new.z_invert, z_regul=new.z_regul, width=new.width, cc=new.cc, mode_regul=new.mode_regul, reoxy_law=new.reoxy_law, reoxy_param=new.reoxy_param::json where id=old.id;
            if 'zregul_weir' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            perform model1.add_configuration_fct(new.configuration::json, old.id, 'zregul_weir_singularity');
            update model1._singularity set validity = (select (z_invert is not null ) and (z_regul is not null ) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (mode_regul is not null) and (reoxy_law is not null) and (reoxy_param is not null) and (not model1.check_on_branch_or_reach_endpoint(new.geom)) from  model1._zregul_weir_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from model1._zregul_weir_singularity where id=old.id;
            delete from model1._singularity where id=old.id;
            if 'zregul_weir' = 'pipe_branch_marker' then
                perform model1.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$;


ALTER FUNCTION model1.zregul_weir_singularity_fct() OWNER TO "thierry.lepelletier";

SET search_path = project, pg_catalog;

--
-- Name: _str_filling_curve(public.geometry); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION _str_filling_curve(polygon public.geometry) RETURNS character varying
    LANGUAGE plpython3u IMMUTABLE
    AS $$
    from hydra_1_0_0 import filling_curve
    import plpy

    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return filling_curve(plpy, sources, polygon)
$$;


ALTER FUNCTION project._str_filling_curve(polygon public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: altitude(public.geometry); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION altitude(point public.geometry) RETURNS real
    LANGUAGE plpgsql IMMUTABLE
    AS $$
    begin
        return project.altitude(st_x(point)::real, st_y(point)::real);
    end;
$$;


ALTER FUNCTION project.altitude(point public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: altitude(real, real); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION altitude(x real, y real) RETURNS real
    LANGUAGE plpython3u IMMUTABLE
    AS $$
    from hydra_1_0_0 import altitude
    import plpy

    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return altitude(plpy, sources, x, y)
$$;


ALTER FUNCTION project.altitude(x real, y real) OWNER TO "thierry.lepelletier";

--
-- Name: caquot_rainfall_fct(); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION caquot_rainfall_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into project._rainfall(rainfall_type, name)
                values('caquot', coalesce(new.name, 'define_later')) returning id into id_;
            update project._rainfall set name = 'RAIN_'||id_::varchar
                where name = 'define_later' and id = id_;
            insert into project._caquot_rainfall
                values (id_, 'caquot', new.montana);
            update project._rainfall set validity = (select (montana is not null) from  project._caquot_rainfall where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update project._rainfall set name=new.name where id=old.id;
            update project._caquot_rainfall set montana=new.montana where id=old.id;
            update project._rainfall set validity = (select (montana is not null) from  project._caquot_rainfall where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._caquot_rainfall where id=old.id;
            delete from project._rainfall where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION project.caquot_rainfall_fct() OWNER TO "thierry.lepelletier";

--
-- Name: create_result(character varying, character varying); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION create_result(scenario character varying, model character varying) RETURNS boolean
    LANGUAGE plpython3u
    AS $$
    import plpy
    from hydra_1_0_0 import create_result
    workspace = plpy.execute("select workspace from hydra.metadata")[0]["workspace"]
    for statement in create_result(workspace, scenario, model, 2154):
        plpy.execute(statement)
    return True
$$;


ALTER FUNCTION project.create_result(scenario character varying, model character varying) OWNER TO "thierry.lepelletier";

--
-- Name: crest_line(public.geometry); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION crest_line(line public.geometry) RETURNS public.geometry
    LANGUAGE plpython3u IMMUTABLE
    AS $$
    from hydra_1_0_0 import crest_line
    import plpy

    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return crest_line(plpy, sources, line, 2154)
$$;


ALTER FUNCTION project.crest_line(line public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: crossed_border(public.geometry, public.geometry); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION crossed_border(line public.geometry, polygon public.geometry) RETURNS public.geometry
    LANGUAGE plpython3u IMMUTABLE
    AS $$
    from hydra_1_0_0 import crossed_border
    import plpy
    return crossed_border(line, polygon)
$$;


ALTER FUNCTION project.crossed_border(line public.geometry, polygon public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: discretize_line(public.geometry, double precision); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION discretize_line(geom public.geometry, elem_length double precision) RETURNS public.geometry
    LANGUAGE plpython3u
    AS $$
    import plpy
    from hydra_1_0_0 import discretize_line
    return discretize_line(plpy, geom, elem_length)
$$;


ALTER FUNCTION project.discretize_line(geom public.geometry, elem_length double precision) OWNER TO "thierry.lepelletier";

--
-- Name: double_triangular_rainfall_fct(); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION double_triangular_rainfall_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into project._rainfall(rainfall_type, name)
                values('double_triangular', coalesce(new.name, 'define_later')) returning id into id_;
            update project._rainfall set name = 'RAIN_'||id_::varchar
                where name = 'define_later' and id = id_;
            insert into project._double_triangular_rainfall
                values (id_, 'double_triangular', new.montana_total, new.montana_peak, new.total_duration_mn, new.peak_duration_mn, new.peak_time_mn);
            update project._rainfall set validity = (select (montana_total is not null) and (montana_peak is not null ) and (total_duration_mn is not null) and (total_duration_mn>0) and (peak_duration_mn is not null) and (peak_duration_mn>0) and (peak_duration_mn<total_duration_mn) and (peak_time_mn is not null) and (peak_time_mn>.5*peak_duration_mn) and (peak_time_mn<(total_duration_mn - .5*peak_duration_mn)) from  project._double_triangular_rainfall where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update project._rainfall set name=new.name where id=old.id;
            update project._double_triangular_rainfall set montana_total=new.montana_total, montana_peak=new.montana_peak, total_duration_mn=new.total_duration_mn, peak_duration_mn=new.peak_duration_mn, peak_time_mn=new.peak_time_mn where id=old.id;
            update project._rainfall set validity = (select (montana_total is not null) and (montana_peak is not null ) and (total_duration_mn is not null) and (total_duration_mn>0) and (peak_duration_mn is not null) and (peak_duration_mn>0) and (peak_duration_mn<total_duration_mn) and (peak_time_mn is not null) and (peak_time_mn>.5*peak_duration_mn) and (peak_time_mn<(total_duration_mn - .5*peak_duration_mn)) from  project._double_triangular_rainfall where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._double_triangular_rainfall where id=old.id;
            delete from project._rainfall where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION project.double_triangular_rainfall_fct() OWNER TO "thierry.lepelletier";

--
-- Name: filling_curve(public.geometry); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION filling_curve(polygon public.geometry) RETURNS real[]
    LANGUAGE plpgsql IMMUTABLE
    AS $$
    begin
        return project._str_filling_curve(polygon)::real[][];
    end;
$$;


ALTER FUNCTION project.filling_curve(polygon public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: gage_rainfall_fct(); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION gage_rainfall_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into project._rainfall(rainfall_type, name)
                values('gage', coalesce(new.name, 'define_later')) returning id into id_;
            update project._rainfall set name = 'RAIN_'||id_::varchar
                where name = 'define_later' and id = id_;
            insert into project._gage_rainfall
                values (id_, 'gage', new.cbv_grid_connect_file, new.interpolation);
            update project._rainfall set validity = (select (interpolation is not null) from  project._gage_rainfall where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update project._rainfall set name=new.name where id=old.id;
            update project._gage_rainfall set cbv_grid_connect_file=new.cbv_grid_connect_file, interpolation=new.interpolation where id=old.id;
            update project._rainfall set validity = (select (interpolation is not null) from  project._gage_rainfall where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._gage_rainfall where id=old.id;
            delete from project._rainfall where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION project.gage_rainfall_fct() OWNER TO "thierry.lepelletier";

--
-- Name: intensity_curve_rainfall_fct(); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION intensity_curve_rainfall_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into project._rainfall(rainfall_type, name)
                values('intensity_curve', coalesce(new.name, 'define_later')) returning id into id_;
            update project._rainfall set name = 'RAIN_'||id_::varchar
                where name = 'define_later' and id = id_;
            insert into project._intensity_curve_rainfall
                values (id_, 'intensity_curve', new.t_mn_intensity_mmh);
            update project._rainfall set validity = (select (t_mn_intensity_mmh is not null) and (array_length(t_mn_intensity_mmh, 1)<=100 and array_length(t_mn_intensity_mmh, 1)>=1 and array_length(t_mn_intensity_mmh, 2)=2) from  project._intensity_curve_rainfall where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update project._rainfall set name=new.name where id=old.id;
            update project._intensity_curve_rainfall set t_mn_intensity_mmh=new.t_mn_intensity_mmh where id=old.id;
            update project._rainfall set validity = (select (t_mn_intensity_mmh is not null) and (array_length(t_mn_intensity_mmh, 1)<=100 and array_length(t_mn_intensity_mmh, 1)>=1 and array_length(t_mn_intensity_mmh, 2)=2) from  project._intensity_curve_rainfall where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._intensity_curve_rainfall where id=old.id;
            delete from project._rainfall where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION project.intensity_curve_rainfall_fct() OWNER TO "thierry.lepelletier";

--
-- Name: model_instead_fct(); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION model_instead_fct() RETURNS trigger
    LANGUAGE plpython3u
    AS $$
            from hydra_1_0_0 import create_model
            import plpy
            for statement in create_model(TD['new']['name'], 2154, '1.0.0'):
                plpy.execute(statement)
        $$;


ALTER FUNCTION project.model_instead_fct() OWNER TO "thierry.lepelletier";

--
-- Name: radar_rainfall_fct(); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION radar_rainfall_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into project._rainfall(rainfall_type, name)
                values('radar', coalesce(new.name, 'define_later')) returning id into id_;
            update project._rainfall set name = 'RAIN_'||id_::varchar
                where name = 'define_later' and id = id_;
            insert into project._radar_rainfall
                values (id_, 'radar', new.radar_file, new.radar_grid);
            update project._rainfall set validity = (select (radar_file is not null) and (radar_grid is not null) from  project._radar_rainfall where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update project._rainfall set name=new.name where id=old.id;
            update project._radar_rainfall set radar_file=new.radar_file, radar_grid=new.radar_grid where id=old.id;
            update project._rainfall set validity = (select (radar_file is not null) and (radar_grid is not null) from  project._radar_rainfall where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._radar_rainfall where id=old.id;
            delete from project._rainfall where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION project.radar_rainfall_fct() OWNER TO "thierry.lepelletier";

--
-- Name: set_altitude(public.geometry); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION set_altitude(geom public.geometry) RETURNS public.geometry
    LANGUAGE plpython3u IMMUTABLE
    AS $$
    from hydra_1_0_0 import set_altitude
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return set_altitude(plpy, sources, geom, 2154)
$$;


ALTER FUNCTION project.set_altitude(geom public.geometry) OWNER TO "thierry.lepelletier";

--
-- Name: single_triangular_rainfall_fct(); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION single_triangular_rainfall_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into project._rainfall(rainfall_type, name)
                values('single_triangular', coalesce(new.name, 'define_later')) returning id into id_;
            update project._rainfall set name = 'RAIN_'||id_::varchar
                where name = 'define_later' and id = id_;
            insert into project._single_triangular_rainfall
                values (id_, 'single_triangular', new.montana, new.total_duration_mn, new.peak_time_mn);
            update project._rainfall set validity = (select (montana is not null ) and (total_duration_mn is not null) and (total_duration_mn>0) and (peak_time_mn is not null) and (peak_time_mn>0) and (peak_time_mn<=total_duration_mn) from  project._single_triangular_rainfall where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update project._rainfall set name=new.name where id=old.id;
            update project._single_triangular_rainfall set montana=new.montana, total_duration_mn=new.total_duration_mn, peak_time_mn=new.peak_time_mn where id=old.id;
            update project._rainfall set validity = (select (montana is not null ) and (total_duration_mn is not null) and (total_duration_mn>0) and (peak_time_mn is not null) and (peak_time_mn>0) and (peak_time_mn<=total_duration_mn) from  project._single_triangular_rainfall where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._single_triangular_rainfall where id=old.id;
            delete from project._rainfall where id=old.id;
            return old;
        end if;

    end;
$$;


ALTER FUNCTION project.single_triangular_rainfall_fct() OWNER TO "thierry.lepelletier";

--
-- Name: unique_name(character varying); Type: FUNCTION; Schema: project; Owner: thierry.lepelletier
--

CREATE FUNCTION unique_name(base_name character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
            begin
                return base_name||nextval('project.unique_name_seq')::varchar;
            end;
        $$;


ALTER FUNCTION project.unique_name(base_name character varying) OWNER TO "thierry.lepelletier";

SET search_path = public, pg_catalog;

--
-- Name: hydra_to_json(character varying, character varying, character varying[]); Type: FUNCTION; Schema: public; Owner: thierry.lepelletier
--

CREATE FUNCTION hydra_to_json(tabl character varying, geom character varying, VARIADIC cols character varying[]) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
    declare
        res varchar;
        ct integer;
    begin
        execute 'select count(1) from '||tabl into ct;

        if ct>0 and geom is null then
            execute 'select row_to_json(everything)
            from
                (select ''FeatureCollection'' as type,
                        array_to_json(array_agg(feat)) as features
                 from (select ''Feature'' as type,
                           null As geometry,
                           row_to_json((select l from (select '||(select array_to_string(cols, ','))||') as l)) as properties
                       from '||tabl||'
                      ) as feat
                 ) as everything;' into res;
        elsif ct>0 then
            execute 'select row_to_json(everything)
            from
                (select ''FeatureCollection'' as type,
                        (select row_to_json(l) from (select ''name'' as type,
                            (select row_to_json(m)
                                from (select ''urn:ogc:def:crs:EPSG::''||(select ST_SRID('||geom||') from '||tabl ||' limit 1)::varchar as name) as m)
                            as properties) as l)
                        as crs,
                        array_to_json(array_agg(feat)) as features
                 from (select ''Feature'' as type,
                           ST_AsGeoJSON('||geom||')::json As geometry,
                           row_to_json((select l from (select '||(select array_to_string(cols, ','))||') as l)) as properties
                       from '||tabl||'
                      ) as feat
                 ) as everything;' into res;
        else
            select '' into res;
        end if;
        return res;
    end;
$$;


ALTER FUNCTION public.hydra_to_json(tabl character varying, geom character varying, VARIADIC cols character varying[]) OWNER TO "thierry.lepelletier";

--
-- Name: wkb_loads(character varying); Type: FUNCTION; Schema: public; Owner: thierry.lepelletier
--

CREATE FUNCTION wkb_loads(geom character varying) RETURNS character varying
    LANGUAGE plpython3u
    AS $$
    import plpy
    from hydra_1_0_0 import wkb_loads
    return wkb_loads(plpy, geom)
$$;


ALTER FUNCTION public.wkb_loads(geom character varying) OWNER TO "thierry.lepelletier";

SET search_path = model1, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: _borda_headloss_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _borda_headloss_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    law_type public.hydra_borda_headloss_link_type,
    param json,
    CONSTRAINT _borda_headloss_link_link_type_check CHECK ((link_type = 'borda_headloss'::public.hydra_link_type))
);


ALTER TABLE _borda_headloss_link OWNER TO "thierry.lepelletier";

--
-- Name: _borda_headloss_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _borda_headloss_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    law_type public.hydra_borda_headloss_singularity_type,
    param json,
    CONSTRAINT _borda_headloss_singularity_singularity_type_check CHECK ((singularity_type = 'borda_headloss'::public.hydra_singularity_type))
);


ALTER TABLE _borda_headloss_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _bradley_headloss_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _bradley_headloss_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    d_abutment_l real,
    d_abutment_r real,
    abutment_type public.hydra_abutment_type,
    zw_array real[],
    z_ceiling real,
    CONSTRAINT _bradley_headloss_singularity_singularity_type_check CHECK ((singularity_type = 'bradley_headloss'::public.hydra_singularity_type))
);


ALTER TABLE _bradley_headloss_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _bridge_headloss_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _bridge_headloss_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    l_road real,
    z_road real,
    zw_array real[],
    CONSTRAINT _bridge_headloss_singularity_singularity_type_check CHECK ((singularity_type = 'bridge_headloss'::public.hydra_singularity_type))
);


ALTER TABLE _bridge_headloss_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _catchment_node; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _catchment_node (
    id integer NOT NULL,
    node_type public.hydra_node_type NOT NULL,
    area_ha real,
    rl real,
    slope real,
    c_imp real,
    netflow_type public.hydra_netflow_type,
    netflow_param json,
    runoff_type public.hydra_runoff_type,
    runoff_param json,
    q_limit real,
    q0 real,
    contour integer,
    catchment_pollution_mode public.hydra_catchment_pollution_mode,
    catchment_pollution_param json,
    CONSTRAINT _catchment_node_node_type_check CHECK ((node_type = 'catchment'::public.hydra_node_type))
);


ALTER TABLE _catchment_node OWNER TO "thierry.lepelletier";

--
-- Name: _connector_hydrology_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _connector_hydrology_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    hydrograph integer,
    CONSTRAINT _connector_hydrology_link_link_type_check CHECK ((link_type = 'connector_hydrology'::public.hydra_link_type))
);


ALTER TABLE _connector_hydrology_link OWNER TO "thierry.lepelletier";

--
-- Name: _connector_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _connector_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    main_branch boolean,
    CONSTRAINT _connector_link_link_type_check CHECK ((link_type = 'connector'::public.hydra_link_type))
);


ALTER TABLE _connector_link OWNER TO "thierry.lepelletier";

--
-- Name: _constant_inflow_bc_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _constant_inflow_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    q0 real,
    CONSTRAINT _constant_inflow_bc_singularity_singularity_type_check CHECK ((singularity_type = 'constant_inflow_bc'::public.hydra_singularity_type))
);


ALTER TABLE _constant_inflow_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _constrain; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _constrain (
    id integer NOT NULL,
    name character varying(16) NOT NULL,
    geom public.geometry(LineStringZ,2154) NOT NULL,
    discretized public.geometry(LineStringZ,2154) NOT NULL,
    elem_length real DEFAULT 100 NOT NULL,
    constrain_type public.hydra_constrain_type,
    link_attributes json,
    CONSTRAINT _constrain_geom_check CHECK (public.st_isvalid(geom)),
    CONSTRAINT _constrain_geom_check1 CHECK (public.st_isvalid(geom))
);


ALTER TABLE _constrain OWNER TO "thierry.lepelletier";

--
-- Name: _constrain_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE _constrain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE _constrain_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: _constrain_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE _constrain_id_seq OWNED BY _constrain.id;


--
-- Name: _crossroad_node; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _crossroad_node (
    id integer NOT NULL,
    node_type public.hydra_node_type NOT NULL,
    area real,
    z_ground real,
    h real,
    CONSTRAINT _crossroad_node_node_type_check CHECK ((node_type = 'crossroad'::public.hydra_node_type))
);


ALTER TABLE _crossroad_node OWNER TO "thierry.lepelletier";

--
-- Name: _deriv_pump_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _deriv_pump_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    q_pump real,
    qz_array real[],
    CONSTRAINT _deriv_pump_link_link_type_check CHECK ((link_type = 'deriv_pump'::public.hydra_link_type))
);


ALTER TABLE _deriv_pump_link OWNER TO "thierry.lepelletier";

--
-- Name: _elem_2d_node; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _elem_2d_node (
    id integer NOT NULL,
    node_type public.hydra_node_type NOT NULL,
    area real,
    zb real,
    rk real,
    domain_2d integer,
    contour public.geometry(PolygonZ,2154) NOT NULL,
    CONSTRAINT _elem_2d_node_contour_check CHECK ((public.st_isvalid(contour) AND (public.st_npoints(contour) = ANY (ARRAY[4, 5])))),
    CONSTRAINT _elem_2d_node_node_type_check CHECK ((node_type = 'elem_2d'::public.hydra_node_type))
);


ALTER TABLE _elem_2d_node OWNER TO "thierry.lepelletier";

--
-- Name: _froude_bc_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _froude_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    CONSTRAINT _froude_bc_singularity_singularity_type_check CHECK ((singularity_type = 'froude_bc'::public.hydra_singularity_type))
);


ALTER TABLE _froude_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _fuse_spillway_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _fuse_spillway_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    z_invert real,
    width real,
    cc real,
    break_mode public.hydra_fuse_spillway_break_mode,
    z_break real,
    t_break real,
    grp integer,
    dt_fracw_array real[],
    CONSTRAINT _fuse_spillway_link_link_type_check CHECK ((link_type = 'fuse_spillway'::public.hydra_link_type))
);


ALTER TABLE _fuse_spillway_link OWNER TO "thierry.lepelletier";

--
-- Name: _gate_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _gate_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    z_invert real,
    z_ceiling real,
    width real,
    cc real,
    action_gate_type public.hydra_action_gate_type,
    mode_valve public.hydra_valve_mode,
    z_gate real,
    v_max_cms real,
    CONSTRAINT _gate_link_link_type_check CHECK ((link_type = 'gate'::public.hydra_link_type))
);


ALTER TABLE _gate_link OWNER TO "thierry.lepelletier";

--
-- Name: _gate_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _gate_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    z_invert real,
    z_ceiling real,
    width real,
    cc real,
    action_gate_type public.hydra_action_gate_type,
    mode_valve public.hydra_valve_mode,
    z_gate real,
    v_max_cms real,
    CONSTRAINT _gate_singularity_singularity_type_check CHECK ((singularity_type = 'gate'::public.hydra_singularity_type))
);


ALTER TABLE _gate_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _hydraulic_cut_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _hydraulic_cut_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    qz_array real[],
    CONSTRAINT _hydraulic_cut_singularity_singularity_type_check CHECK ((singularity_type = 'hydraulic_cut'::public.hydra_singularity_type))
);


ALTER TABLE _hydraulic_cut_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _hydrograph_bc_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _hydrograph_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    storage_area real,
    tq_array real[],
    constant_dry_flow real,
    distrib_coef real,
    lag_time_hr real,
    sector integer,
    hourly_modulation integer,
    pollution_quality_mode public.hydra_pollution_quality_mode,
    pollution_quality_param json,
    external_file_data boolean NOT NULL,
    CONSTRAINT _hydrograph_bc_singularity_singularity_type_check CHECK ((singularity_type = 'hydrograph_bc'::public.hydra_singularity_type))
);


ALTER TABLE _hydrograph_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _hydrology_bc_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _hydrology_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    CONSTRAINT _hydrology_bc_singularity_singularity_type_check CHECK ((singularity_type = 'hydrology_bc'::public.hydra_singularity_type))
);


ALTER TABLE _hydrology_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    name character varying(24) NOT NULL,
    generated integer,
    up integer,
    up_type public.hydra_node_type,
    down integer,
    down_type public.hydra_node_type,
    geom public.geometry(LineStringZ,2154) NOT NULL,
    configuration json,
    validity boolean,
    CONSTRAINT _link_check CHECK (((up IS NOT NULL) OR (up_type = 'elem_2d'::public.hydra_node_type))),
    CONSTRAINT _link_check1 CHECK (((down IS NOT NULL) OR (down_type = 'elem_2d'::public.hydra_node_type))),
    CONSTRAINT _link_down_type_check CHECK ((down_type <> 'catchment'::public.hydra_node_type)),
    CONSTRAINT _link_geom_check CHECK (public.st_isvalid(geom)),
    CONSTRAINT model1_link_connectivity_check_cstr CHECK ((((link_type = 'street'::public.hydra_link_type) AND ((up_type = 'crossroad'::public.hydra_node_type) AND (down_type = 'crossroad'::public.hydra_node_type))) OR ((link_type = 'regul_gate'::public.hydra_link_type) AND ((up_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])))) OR ((link_type = 'connector'::public.hydra_link_type) AND ((up_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])))) OR ((link_type = 'gate'::public.hydra_link_type) AND ((up_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])))) OR ((link_type = 'routing_hydrology'::public.hydra_link_type) AND ((up_type = 'catchment'::public.hydra_node_type) AND (down_type = ANY (ARRAY['manhole_hydrology'::public.hydra_node_type, 'manhole'::public.hydra_node_type, 'river'::public.hydra_node_type, 'station'::public.hydra_node_type, 'storage'::public.hydra_node_type])))) OR ((link_type = 'weir'::public.hydra_link_type) AND ((up_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])))) OR ((link_type = 'overflow'::public.hydra_link_type) AND ((up_type = ANY (ARRAY['river'::public.hydra_node_type, 'storage'::public.hydra_node_type, 'elem_2d'::public.hydra_node_type, 'crossroad'::public.hydra_node_type])) AND (down_type = ANY (ARRAY['river'::public.hydra_node_type, 'storage'::public.hydra_node_type, 'elem_2d'::public.hydra_node_type, 'crossroad'::public.hydra_node_type])))) OR ((link_type = 'pipe'::public.hydra_link_type) AND (((up_type = 'manhole_hydrology'::public.hydra_node_type) AND (down_type = 'manhole_hydrology'::public.hydra_node_type)) OR ((up_type = 'manhole'::public.hydra_node_type) AND (down_type = 'manhole'::public.hydra_node_type)))) OR ((link_type = 'network_overflow'::public.hydra_link_type) AND ((up_type = 'manhole'::public.hydra_node_type) AND (down_type = ANY (ARRAY['crossroad'::public.hydra_node_type, 'elem_2d'::public.hydra_node_type, 'storage'::public.hydra_node_type, 'river'::public.hydra_node_type])))) OR ((link_type = 'porous'::public.hydra_link_type) AND ((up_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])))) OR ((link_type = 'pump'::public.hydra_link_type) AND ((up_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])))) OR ((link_type = 'deriv_pump'::public.hydra_link_type) AND ((up_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])))) OR ((link_type = 'connector_hydrology'::public.hydra_link_type) AND ((up_type = 'manhole_hydrology'::public.hydra_node_type) AND (down_type = ANY (ARRAY['manhole_hydrology'::public.hydra_node_type, 'manhole'::public.hydra_node_type, 'river'::public.hydra_node_type, 'station'::public.hydra_node_type])))) OR ((link_type = 'borda_headloss'::public.hydra_link_type) AND ((up_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])))) OR ((link_type = 'strickler'::public.hydra_link_type) AND ((up_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])))) OR ((link_type = 'fuse_spillway'::public.hydra_link_type) AND ((up_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::public.hydra_node_type, 'manhole_hydrology'::public.hydra_node_type])))) OR ((link_type = 'mesh_2d'::public.hydra_link_type) AND ((up_type = 'elem_2d'::public.hydra_node_type) AND (down_type = 'elem_2d'::public.hydra_node_type)))))
);


ALTER TABLE _link OWNER TO "thierry.lepelletier";

--
-- Name: _link_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE _link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE _link_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: _link_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE _link_id_seq OWNED BY _link.id;


--
-- Name: _manhole_hydrology_node; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _manhole_hydrology_node (
    id integer NOT NULL,
    node_type public.hydra_node_type NOT NULL,
    area real,
    z_ground real,
    CONSTRAINT _manhole_hydrology_node_node_type_check CHECK ((node_type = 'manhole_hydrology'::public.hydra_node_type))
);


ALTER TABLE _manhole_hydrology_node OWNER TO "thierry.lepelletier";

--
-- Name: _manhole_node; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _manhole_node (
    id integer NOT NULL,
    node_type public.hydra_node_type NOT NULL,
    area real,
    z_ground real,
    CONSTRAINT _manhole_node_node_type_check CHECK ((node_type = 'manhole'::public.hydra_node_type))
);


ALTER TABLE _manhole_node OWNER TO "thierry.lepelletier";

--
-- Name: _marker_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _marker_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    comment character varying(256),
    CONSTRAINT _marker_singularity_singularity_type_check CHECK ((singularity_type = 'marker'::public.hydra_singularity_type))
);


ALTER TABLE _marker_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _mesh_2d_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _mesh_2d_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    z_invert real,
    lateral_contraction_coef real,
    border public.geometry(LineStringZ,2154),
    CONSTRAINT _mesh_2d_link_link_type_check CHECK ((link_type = 'mesh_2d'::public.hydra_link_type))
);


ALTER TABLE _mesh_2d_link OWNER TO "thierry.lepelletier";

--
-- Name: _model_connect_bc_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _model_connect_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    cascade_mode public.hydra_model_connect_mode,
    zq_array real[],
    CONSTRAINT _model_connect_bc_singularity_singularity_type_check CHECK ((singularity_type = 'model_connect_bc'::public.hydra_singularity_type))
);


ALTER TABLE _model_connect_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _network_overflow_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _network_overflow_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    z_overflow real,
    area real,
    CONSTRAINT _network_overflow_link_link_type_check CHECK ((link_type = 'network_overflow'::public.hydra_link_type))
);


ALTER TABLE _network_overflow_link OWNER TO "thierry.lepelletier";

--
-- Name: _node; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _node (
    id integer NOT NULL,
    node_type public.hydra_node_type NOT NULL,
    name character varying(24) NOT NULL,
    geom public.geometry(PointZ,2154) NOT NULL,
    generated integer,
    configuration json,
    validity boolean,
    CONSTRAINT _node_geom_check CHECK (public.st_isvalid(geom))
);


ALTER TABLE _node OWNER TO "thierry.lepelletier";

--
-- Name: _node_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE _node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE _node_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: _node_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE _node_id_seq OWNED BY _node.id;


--
-- Name: _overflow_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _overflow_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    z_crest1 real,
    width1 real,
    z_crest2 real,
    width2 real,
    cc real,
    lateral_contraction_coef real,
    break_mode public.hydra_fuse_spillway_break_mode,
    z_break real,
    t_break real,
    z_invert real,
    width_breach real,
    grp integer,
    dt_fracw_array real[],
    border public.geometry(LineStringZ,2154),
    CONSTRAINT _overflow_link_link_type_check CHECK ((link_type = 'overflow'::public.hydra_link_type))
);


ALTER TABLE _overflow_link OWNER TO "thierry.lepelletier";

--
-- Name: _param_headloss_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _param_headloss_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    q_dz_array real[],
    CONSTRAINT _param_headloss_singularity_singularity_type_check CHECK ((singularity_type = 'param_headloss'::public.hydra_singularity_type))
);


ALTER TABLE _param_headloss_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _pipe_branch_marker_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _pipe_branch_marker_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    pk0_km real,
    dx real,
    CONSTRAINT _pipe_branch_marker_singularity_singularity_type_check CHECK ((singularity_type = 'pipe_branch_marker'::public.hydra_singularity_type))
);


ALTER TABLE _pipe_branch_marker_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _pipe_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _pipe_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    comment character varying(256),
    z_invert_up real,
    z_invert_down real,
    cross_section_type public.hydra_cross_section_type,
    h_sable real,
    branch integer,
    rk real,
    circular_diameter real,
    ovoid_height real,
    ovoid_top_diameter real,
    ovoid_invert_diameter real,
    cp_geom integer,
    op_geom integer,
    CONSTRAINT _pipe_link_link_type_check CHECK ((link_type = 'pipe'::public.hydra_link_type))
);


ALTER TABLE _pipe_link OWNER TO "thierry.lepelletier";

--
-- Name: _porous_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _porous_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    z_invert real,
    width real,
    transmitivity real,
    border public.geometry(LineStringZ,2154),
    CONSTRAINT _porous_link_link_type_check CHECK ((link_type = 'porous'::public.hydra_link_type))
);


ALTER TABLE _porous_link OWNER TO "thierry.lepelletier";

--
-- Name: _pump_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _pump_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    npump integer,
    zregul_array real[],
    hq_array real[],
    CONSTRAINT _pump_link_link_type_check CHECK ((link_type = 'pump'::public.hydra_link_type))
);


ALTER TABLE _pump_link OWNER TO "thierry.lepelletier";

--
-- Name: _qq_split_hydrology_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _qq_split_hydrology_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    qq_array real[],
    downstream integer,
    downstream_type public.hydra_link_type,
    split1 integer,
    split1_type public.hydra_link_type,
    split2 integer,
    split2_type public.hydra_link_type,
    CONSTRAINT _qq_split_hydrology_singularity_check CHECK (((split2 IS NULL) OR (split2_type = 'pipe'::public.hydra_link_type) OR (split2_type = 'connector_hydrology'::public.hydra_link_type))),
    CONSTRAINT _qq_split_hydrology_singularity_downstream_type_check CHECK (((downstream_type = 'pipe'::public.hydra_link_type) OR (downstream_type = 'connector_hydrology'::public.hydra_link_type))),
    CONSTRAINT _qq_split_hydrology_singularity_singularity_type_check CHECK ((singularity_type = 'qq_split_hydrology'::public.hydra_singularity_type)),
    CONSTRAINT _qq_split_hydrology_singularity_split1_type_check CHECK (((split1_type = 'pipe'::public.hydra_link_type) OR (split1_type = 'connector_hydrology'::public.hydra_link_type)))
);


ALTER TABLE _qq_split_hydrology_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _regul_gate_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _regul_gate_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    z_invert real,
    z_ceiling real,
    width real,
    cc real,
    action_gate_type public.hydra_action_gate_type,
    z_invert_stop real,
    z_ceiling_stop real,
    v_max_cms real,
    dt_regul_hr real,
    mode_regul public.hydra_gate_mode_regul,
    z_control_node integer,
    z_pid_array real[],
    z_tz_array real[],
    q_z_crit real,
    q_tq_array real[],
    nr_z_gate real,
    CONSTRAINT _regul_gate_link_link_type_check CHECK ((link_type = 'regul_gate'::public.hydra_link_type))
);


ALTER TABLE _regul_gate_link OWNER TO "thierry.lepelletier";

--
-- Name: _regul_sluice_gate_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _regul_sluice_gate_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    z_invert real,
    z_ceiling real,
    width real,
    cc real,
    action_gate_type public.hydra_action_gate_type,
    z_invert_stop real,
    z_ceiling_stop real,
    v_max_cms real,
    dt_regul_hr real,
    mode_regul public.hydra_gate_mode_regul,
    z_control_node integer,
    z_pid_array real[],
    z_tz_array real[],
    q_z_crit real,
    q_tq_array real[],
    nr_z_gate real,
    CONSTRAINT _regul_sluice_gate_singularity_singularity_type_check CHECK ((singularity_type = 'regul_sluice_gate'::public.hydra_singularity_type))
);


ALTER TABLE _regul_sluice_gate_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _reservoir_rs_hydrology_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _reservoir_rs_hydrology_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    drainage integer,
    drainage_type public.hydra_link_type,
    overflow integer,
    overflow_type public.hydra_link_type,
    q_drainage real,
    z_ini real,
    zs_array real[],
    treatment_mode public.hydra_pollution_treatment_mode,
    treatment_param json,
    CONSTRAINT _reservoir_rs_hydrology_singularity_drainage_type_check CHECK (((drainage_type = 'pipe'::public.hydra_link_type) OR (drainage_type = 'connector_hydrology'::public.hydra_link_type))),
    CONSTRAINT _reservoir_rs_hydrology_singularity_overflow_type_check CHECK (((overflow_type = 'pipe'::public.hydra_link_type) OR (overflow_type = 'connector_hydrology'::public.hydra_link_type))),
    CONSTRAINT _reservoir_rs_hydrology_singularity_singularity_type_check CHECK ((singularity_type = 'reservoir_rs_hydrology'::public.hydra_singularity_type))
);


ALTER TABLE _reservoir_rs_hydrology_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _reservoir_rsp_hydrology_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _reservoir_rsp_hydrology_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    drainage integer,
    drainage_type public.hydra_link_type,
    overflow integer,
    overflow_type public.hydra_link_type,
    z_ini real,
    zr_sr_qf_qs_array real[],
    treatment_mode public.hydra_pollution_treatment_mode,
    treatment_param json,
    CONSTRAINT _reservoir_rsp_hydrology_singularity_drainage_type_check CHECK (((drainage_type = 'pipe'::public.hydra_link_type) OR (drainage_type = 'connector_hydrology'::public.hydra_link_type))),
    CONSTRAINT _reservoir_rsp_hydrology_singularity_overflow_type_check CHECK (((overflow_type = 'pipe'::public.hydra_link_type) OR (overflow_type = 'connector_hydrology'::public.hydra_link_type))),
    CONSTRAINT _reservoir_rsp_hydrology_singularity_singularity_type_check CHECK ((singularity_type = 'reservoir_rsp_hydrology'::public.hydra_singularity_type))
);


ALTER TABLE _reservoir_rsp_hydrology_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _river_cross_section_pl1d; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _river_cross_section_pl1d (
    id integer NOT NULL,
    name character varying(24) NOT NULL,
    profile public.geometry(LineStringZ,2154) NOT NULL,
    generated integer,
    CONSTRAINT _river_cross_section_pl1d_profile_check CHECK (((public.st_npoints(profile) = 4) AND public.st_isvalid(profile)))
);


ALTER TABLE _river_cross_section_pl1d OWNER TO "thierry.lepelletier";

--
-- Name: _river_cross_section_profile; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _river_cross_section_profile (
    id integer NOT NULL,
    name character varying(24) NOT NULL,
    z_invert_up real,
    z_invert_down real,
    type_cross_section_up public.hydra_cross_section_type DEFAULT 'valley'::public.hydra_cross_section_type,
    type_cross_section_down public.hydra_cross_section_type DEFAULT 'valley'::public.hydra_cross_section_type,
    up_rk real,
    up_rk_maj real,
    up_sinuosity real,
    up_circular_diameter real,
    up_ovoid_height real,
    up_ovoid_top_diameter real,
    up_ovoid_invert_diameter real,
    up_cp_geom integer,
    up_op_geom integer,
    up_vcs_geom integer,
    up_vcs_topo_geom integer,
    down_rk real,
    down_rk_maj real,
    down_sinuosity real,
    down_circular_diameter real,
    down_ovoid_height real,
    down_ovoid_top_diameter real,
    down_ovoid_invert_diameter real,
    down_cp_geom integer,
    down_op_geom integer,
    down_vcs_geom integer,
    down_vcs_topo_geom integer,
    validity boolean
);


ALTER TABLE _river_cross_section_profile OWNER TO "thierry.lepelletier";

--
-- Name: _river_node; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _river_node (
    id integer NOT NULL,
    node_type public.hydra_node_type NOT NULL,
    reach integer,
    z_ground real,
    area real,
    CONSTRAINT _river_node_node_type_check CHECK ((node_type = 'river'::public.hydra_node_type))
);


ALTER TABLE _river_node OWNER TO "thierry.lepelletier";

--
-- Name: _routing_hydrology_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _routing_hydrology_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    cross_section real,
    length real,
    slope real,
    hydrograph integer,
    CONSTRAINT _routing_hydrology_link_link_type_check CHECK ((link_type = 'routing_hydrology'::public.hydra_link_type))
);


ALTER TABLE _routing_hydrology_link OWNER TO "thierry.lepelletier";

--
-- Name: _singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    name character varying(24) NOT NULL,
    node integer NOT NULL,
    node_type public.hydra_node_type NOT NULL,
    configuration json,
    validity boolean,
    CONSTRAINT model1_singularity_connectivity_check_cstr CHECK ((((singularity_type = 'reservoir_rsp_hydrology'::public.hydra_singularity_type) AND (node_type = 'manhole_hydrology'::public.hydra_node_type)) OR ((singularity_type = 'constant_inflow_bc'::public.hydra_singularity_type) AND ((node_type <> 'catchment'::public.hydra_node_type) AND (node_type <> 'manhole_hydrology'::public.hydra_node_type))) OR ((singularity_type = 'tz_bc'::public.hydra_singularity_type) AND ((node_type <> 'catchment'::public.hydra_node_type) AND (node_type <> 'manhole_hydrology'::public.hydra_node_type))) OR ((singularity_type = 'zq_bc'::public.hydra_singularity_type) AND ((node_type <> 'catchment'::public.hydra_node_type) AND (node_type <> 'manhole_hydrology'::public.hydra_node_type))) OR ((singularity_type = 'tank_bc'::public.hydra_singularity_type) AND ((node_type <> 'catchment'::public.hydra_node_type) AND (node_type <> 'manhole_hydrology'::public.hydra_node_type))) OR ((singularity_type = 'regul_sluice_gate'::public.hydra_singularity_type) AND (node_type = ANY (ARRAY['river'::public.hydra_node_type, 'manhole'::public.hydra_node_type]))) OR ((singularity_type = 'pipe_branch_marker'::public.hydra_singularity_type) AND (node_type = 'manhole'::public.hydra_node_type)) OR ((singularity_type = 'gate'::public.hydra_singularity_type) AND (node_type = ANY (ARRAY['river'::public.hydra_node_type, 'manhole'::public.hydra_node_type]))) OR ((singularity_type = 'weir_bc'::public.hydra_singularity_type) AND ((node_type <> 'catchment'::public.hydra_node_type) AND (node_type <> 'manhole_hydrology'::public.hydra_node_type))) OR ((singularity_type = 'hydrograph_bc'::public.hydra_singularity_type) AND ((node_type <> 'catchment'::public.hydra_node_type) AND (node_type <> 'manhole_hydrology'::public.hydra_node_type))) OR ((singularity_type = 'froude_bc'::public.hydra_singularity_type) AND (node_type = ANY (ARRAY['river'::public.hydra_node_type, 'manhole'::public.hydra_node_type]))) OR ((singularity_type = 'borda_headloss'::public.hydra_singularity_type) AND (node_type = ANY (ARRAY['river'::public.hydra_node_type, 'manhole'::public.hydra_node_type]))) OR ((singularity_type = 'bradley_headloss'::public.hydra_singularity_type) AND (node_type = 'river'::public.hydra_node_type)) OR ((singularity_type = 'zregul_weir'::public.hydra_singularity_type) AND (node_type = ANY (ARRAY['river'::public.hydra_node_type, 'manhole'::public.hydra_node_type]))) OR ((singularity_type = 'hydraulic_cut'::public.hydra_singularity_type) AND (node_type = ANY (ARRAY['river'::public.hydra_node_type, 'manhole'::public.hydra_node_type]))) OR ((singularity_type = 'strickler_bc'::public.hydra_singularity_type) AND ((node_type <> 'catchment'::public.hydra_node_type) AND (node_type <> 'manhole_hydrology'::public.hydra_node_type))) OR ((singularity_type = 'model_connect_bc'::public.hydra_singularity_type) AND ((node_type <> 'catchment'::public.hydra_node_type) AND (node_type <> 'manhole_hydrology'::public.hydra_node_type))) OR ((singularity_type = 'param_headloss'::public.hydra_singularity_type) AND (node_type = ANY (ARRAY['river'::public.hydra_node_type, 'manhole'::public.hydra_node_type]))) OR ((singularity_type = 'zq_split_hydrology'::public.hydra_singularity_type) AND (node_type = 'manhole_hydrology'::public.hydra_node_type)) OR ((singularity_type = 'bridge_headloss'::public.hydra_singularity_type) AND (node_type = 'river'::public.hydra_node_type)) OR ((singularity_type = 'marker'::public.hydra_singularity_type) AND (node_type = ANY (ARRAY['river'::public.hydra_node_type, 'manhole'::public.hydra_node_type]))) OR ((singularity_type = 'qq_split_hydrology'::public.hydra_singularity_type) AND (node_type = 'manhole_hydrology'::public.hydra_node_type)) OR ((singularity_type = 'hydrology_bc'::public.hydra_singularity_type) AND (node_type = 'manhole_hydrology'::public.hydra_node_type)) OR ((singularity_type = 'reservoir_rs_hydrology'::public.hydra_singularity_type) AND (node_type = 'manhole_hydrology'::public.hydra_node_type))))
);


ALTER TABLE _singularity OWNER TO "thierry.lepelletier";

--
-- Name: _singularity_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE _singularity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE _singularity_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: _singularity_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE _singularity_id_seq OWNED BY _singularity.id;


--
-- Name: _station_node; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _station_node (
    id integer NOT NULL,
    node_type public.hydra_node_type NOT NULL,
    area real,
    z_invert real,
    station integer NOT NULL,
    CONSTRAINT _station_node_node_type_check CHECK ((node_type = 'station'::public.hydra_node_type))
);


ALTER TABLE _station_node OWNER TO "thierry.lepelletier";

--
-- Name: _storage_node; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _storage_node (
    id integer NOT NULL,
    node_type public.hydra_node_type NOT NULL,
    zs_array real[],
    zini real,
    contour integer,
    CONSTRAINT _storage_node_node_type_check CHECK ((node_type = 'storage'::public.hydra_node_type))
);


ALTER TABLE _storage_node OWNER TO "thierry.lepelletier";

--
-- Name: _street_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _street_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    width real,
    length real,
    rk real,
    CONSTRAINT _street_link_link_type_check CHECK ((link_type = 'street'::public.hydra_link_type))
);


ALTER TABLE _street_link OWNER TO "thierry.lepelletier";

--
-- Name: _strickler_bc_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _strickler_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    slope real,
    k real,
    width real,
    CONSTRAINT _strickler_bc_singularity_singularity_type_check CHECK ((singularity_type = 'strickler_bc'::public.hydra_singularity_type))
);


ALTER TABLE _strickler_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _strickler_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _strickler_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    z_crest1 real,
    width1 real,
    length real,
    rk real,
    z_crest2 real,
    width2 real,
    border public.geometry(LineStringZ,2154),
    CONSTRAINT _strickler_link_link_type_check CHECK ((link_type = 'strickler'::public.hydra_link_type))
);


ALTER TABLE _strickler_link OWNER TO "thierry.lepelletier";

--
-- Name: _tank_bc_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _tank_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    zs_array real[],
    zini real,
    CONSTRAINT _tank_bc_singularity_singularity_type_check CHECK ((singularity_type = 'tank_bc'::public.hydra_singularity_type))
);


ALTER TABLE _tank_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _tz_bc_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _tz_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    tz_array real[],
    cyclic boolean NOT NULL,
    external_file_data boolean NOT NULL,
    CONSTRAINT _tz_bc_singularity_singularity_type_check CHECK ((singularity_type = 'tz_bc'::public.hydra_singularity_type))
);


ALTER TABLE _tz_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _weir_bc_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _weir_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    z_weir real,
    width real,
    cc real,
    CONSTRAINT _weir_bc_singularity_singularity_type_check CHECK ((singularity_type = 'weir_bc'::public.hydra_singularity_type))
);


ALTER TABLE _weir_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _weir_link; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _weir_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    z_invert real,
    width real,
    cc real,
    z_weir real,
    v_max_cms real NOT NULL,
    CONSTRAINT _weir_link_link_type_check CHECK ((link_type = 'weir'::public.hydra_link_type))
);


ALTER TABLE _weir_link OWNER TO "thierry.lepelletier";

--
-- Name: _zq_bc_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _zq_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    zq_array real[],
    CONSTRAINT _zq_bc_singularity_singularity_type_check CHECK ((singularity_type = 'zq_bc'::public.hydra_singularity_type))
);


ALTER TABLE _zq_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _zq_split_hydrology_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _zq_split_hydrology_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    downstream integer,
    downstream_type public.hydra_link_type,
    split1 integer,
    split1_type public.hydra_link_type,
    split2 integer,
    split2_type public.hydra_link_type,
    downstream_law public.hydra_split_law_type NOT NULL,
    downstream_param json,
    split1_law public.hydra_split_law_type,
    split1_param json,
    split2_law public.hydra_split_law_type,
    split2_param json,
    CONSTRAINT _zq_split_hydrology_singularity_check CHECK (((split2 IS NULL) OR (split2_type = 'pipe'::public.hydra_link_type) OR (split2_type = 'connector_hydrology'::public.hydra_link_type))),
    CONSTRAINT _zq_split_hydrology_singularity_downstream_type_check CHECK (((downstream_type = 'pipe'::public.hydra_link_type) OR (downstream_type = 'connector_hydrology'::public.hydra_link_type))),
    CONSTRAINT _zq_split_hydrology_singularity_singularity_type_check CHECK ((singularity_type = 'zq_split_hydrology'::public.hydra_singularity_type)),
    CONSTRAINT _zq_split_hydrology_singularity_split1_type_check CHECK (((split1_type = 'pipe'::public.hydra_link_type) OR (split1_type = 'connector_hydrology'::public.hydra_link_type)))
);


ALTER TABLE _zq_split_hydrology_singularity OWNER TO "thierry.lepelletier";

--
-- Name: _zregul_weir_singularity; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE _zregul_weir_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    z_invert real,
    z_regul real,
    width real,
    cc real,
    mode_regul public.hydra_weir_mode_regul,
    reoxy_law public.hydra_weir_mode_reoxy,
    reoxy_param json,
    CONSTRAINT _zregul_weir_singularity_singularity_type_check CHECK ((singularity_type = 'zregul_weir'::public.hydra_singularity_type))
);


ALTER TABLE _zregul_weir_singularity OWNER TO "thierry.lepelletier";

--
-- Name: bank; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE bank (
    id integer NOT NULL,
    name character varying(16) DEFAULT project.unique_name('BANK_'::character varying) NOT NULL,
    geom public.geometry(LineStringZ,2154) NOT NULL
);


ALTER TABLE bank OWNER TO "thierry.lepelletier";

--
-- Name: bank_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE bank_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: bank_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE bank_id_seq OWNED BY bank.id;


--
-- Name: borda_headloss_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW borda_headloss_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.law_type,
    (c.param)::character varying AS param,
    c.param AS param_json,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _borda_headloss_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE borda_headloss_link OWNER TO "thierry.lepelletier";

--
-- Name: borda_headloss_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW borda_headloss_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.law_type,
    (c.param)::character varying AS param,
    c.param AS param_json,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _borda_headloss_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE borda_headloss_singularity OWNER TO "thierry.lepelletier";

--
-- Name: bradley_headloss_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW bradley_headloss_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.d_abutment_l,
    c.d_abutment_r,
    c.abutment_type,
    c.zw_array,
    c.z_ceiling,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _bradley_headloss_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE bradley_headloss_singularity OWNER TO "thierry.lepelletier";

--
-- Name: branch; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE branch (
    id integer NOT NULL,
    name character varying(24),
    pk0_km real DEFAULT 0 NOT NULL,
    dx real DEFAULT 50 NOT NULL,
    geom public.geometry(LineStringZ,2154) NOT NULL,
    CONSTRAINT branch_geom_check CHECK (public.st_isvalid(geom))
);


ALTER TABLE branch OWNER TO "thierry.lepelletier";

--
-- Name: bridge_headloss_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW bridge_headloss_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.l_road,
    c.z_road,
    c.zw_array,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _bridge_headloss_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE bridge_headloss_singularity OWNER TO "thierry.lepelletier";

--
-- Name: catchment; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE catchment (
    id integer NOT NULL,
    name character varying(16) DEFAULT project.unique_name('CATCHMENT_'::character varying) NOT NULL,
    geom public.geometry(Polygon,2154) NOT NULL,
    CONSTRAINT catchment_geom_check CHECK (public.st_isvalid(geom))
);


ALTER TABLE catchment OWNER TO "thierry.lepelletier";

--
-- Name: catchment_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE catchment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catchment_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: catchment_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE catchment_id_seq OWNED BY catchment.id;


--
-- Name: catchment_node; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW catchment_node AS
 SELECT p.id,
    p.name,
    c.area_ha,
    c.rl,
    c.slope,
    c.c_imp,
    c.netflow_type,
    (c.netflow_param)::character varying AS netflow_param,
    c.runoff_type,
    (c.runoff_param)::character varying AS runoff_param,
    c.q_limit,
    c.q0,
    c.contour,
    c.catchment_pollution_mode,
    (c.catchment_pollution_param)::character varying AS catchment_pollution_param,
    c.netflow_param AS netflow_param_json,
    c.runoff_param AS runoff_param_json,
    c.catchment_pollution_param AS catchment_pollution_param_json,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.configuration AS configuration_json
   FROM _catchment_node c,
    _node p
  WHERE (p.id = c.id);


ALTER TABLE catchment_node OWNER TO "thierry.lepelletier";

--
-- Name: closed_parametric_geometry; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE closed_parametric_geometry (
    id integer NOT NULL,
    name character varying(24) DEFAULT project.unique_name('CP_'::character varying) NOT NULL,
    zbmin_array real[],
    CONSTRAINT closed_parametric_geometry_zbmin_array_check CHECK (((array_length(zbmin_array, 1) <= 20) AND (array_length(zbmin_array, 1) >= 1) AND (array_length(zbmin_array, 2) = 2)))
);


ALTER TABLE closed_parametric_geometry OWNER TO "thierry.lepelletier";

--
-- Name: open_parametric_geometry; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE open_parametric_geometry (
    id integer NOT NULL,
    name character varying(24) DEFAULT project.unique_name('OP_'::character varying) NOT NULL,
    zbmin_array real[],
    CONSTRAINT open_parametric_geometry_zbmin_array_check CHECK (((array_length(zbmin_array, 1) <= 20) AND (array_length(zbmin_array, 1) >= 1) AND (array_length(zbmin_array, 2) = 2)))
);


ALTER TABLE open_parametric_geometry OWNER TO "thierry.lepelletier";

--
-- Name: reach; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE reach (
    id integer NOT NULL,
    name character varying(16) DEFAULT project.unique_name('REACH_'::character varying) NOT NULL,
    pk0_km real DEFAULT 0 NOT NULL,
    dx real DEFAULT 50 NOT NULL,
    geom public.geometry(LineStringZ,2154) NOT NULL,
    CONSTRAINT reach_geom_check CHECK (public.st_isvalid(geom))
);


ALTER TABLE reach OWNER TO "thierry.lepelletier";

--
-- Name: valley_cross_section_geometry; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE valley_cross_section_geometry (
    id integer NOT NULL,
    name character varying(24) DEFAULT project.unique_name('VA_'::character varying) NOT NULL,
    zbmin_array real[] NOT NULL,
    zbmaj_lbank_array real[],
    zbmaj_rbank_array real[],
    rlambda real DEFAULT 1 NOT NULL,
    rmu1 real DEFAULT '-999'::integer NOT NULL,
    rmu2 real DEFAULT '-999'::integer NOT NULL,
    zlevee_lb real DEFAULT '-999'::integer NOT NULL,
    zlevee_rb real DEFAULT '-999'::integer NOT NULL,
    transect integer,
    t_ignore_pt boolean DEFAULT false NOT NULL,
    t_discret integer DEFAULT 150 NOT NULL,
    t_distance_pt integer DEFAULT 10 NOT NULL,
    CONSTRAINT valley_cross_section_geometry_rlambda_check CHECK (((rlambda > (0)::double precision) AND (rlambda <= (1)::double precision))),
    CONSTRAINT valley_cross_section_geometry_zbmaj_lbank_array_check CHECK (((array_length(zbmaj_lbank_array, 1) <= 4) AND (array_length(zbmaj_lbank_array, 1) >= 1) AND (array_length(zbmaj_lbank_array, 2) = 2))),
    CONSTRAINT valley_cross_section_geometry_zbmaj_rbank_array_check CHECK (((array_length(zbmaj_rbank_array, 1) <= 4) AND (array_length(zbmaj_rbank_array, 1) >= 1) AND (array_length(zbmaj_rbank_array, 2) = 2))),
    CONSTRAINT valley_cross_section_geometry_zbmin_array_check CHECK (((array_length(zbmin_array, 1) <= 6) AND (array_length(zbmin_array, 1) >= 1) AND (array_length(zbmin_array, 2) = 2)))
);


ALTER TABLE valley_cross_section_geometry OWNER TO "thierry.lepelletier";

--
-- Name: river_cross_section_profile; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW river_cross_section_profile AS
 SELECT g.id,
    c.name,
    c.z_invert_up,
    c.z_invert_down,
    c.type_cross_section_up,
    c.type_cross_section_down,
    c.up_rk,
    c.up_rk_maj,
    c.up_sinuosity,
    c.up_circular_diameter,
    c.up_ovoid_height,
    c.up_ovoid_top_diameter,
    c.up_ovoid_invert_diameter,
    c.up_cp_geom,
    c.up_op_geom,
    c.up_vcs_geom,
    c.up_vcs_topo_geom,
    c.down_rk,
    c.down_rk_maj,
    c.down_sinuosity,
    c.down_circular_diameter,
    c.down_ovoid_height,
    c.down_ovoid_top_diameter,
    c.down_ovoid_invert_diameter,
    c.down_cp_geom,
    c.down_op_geom,
    c.down_vcs_geom,
    c.down_vcs_topo_geom,
    ( SELECT
                CASE
                    WHEN ((c.type_cross_section_up = 'channel'::public.hydra_cross_section_type) OR (c.type_cross_section_up = 'valley'::public.hydra_cross_section_type)) THEN (( SELECT c.z_invert_up))::double precision
                    ELSE ( SELECT public.st_z(public.st_startpoint(g.geom)) AS st_z)
                END AS st_z) AS z_tn_up,
    ( SELECT
                CASE
                    WHEN ((c.type_cross_section_down = 'channel'::public.hydra_cross_section_type) OR (c.type_cross_section_down = 'valley'::public.hydra_cross_section_type)) THEN (( SELECT c.z_invert_down))::double precision
                    ELSE ( SELECT public.st_z(public.st_endpoint(g.geom)) AS st_z)
                END AS st_z) AS z_tn_down,
    ( SELECT
                CASE
                    WHEN (c.type_cross_section_up = 'circular'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_up + c.up_circular_diameter))
                    WHEN (c.type_cross_section_up = 'ovoid'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_up + c.up_ovoid_height))
                    WHEN (c.type_cross_section_up = 'pipe'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (cpg.zbmin_array[array_length(cpg.zbmin_array, 1)][1] - cpg.zbmin_array[1][1]) AS height
                               FROM closed_parametric_geometry cpg
                              WHERE (c.up_cp_geom = cpg.id)
                            )
                     SELECT (c.z_invert_up + n.height)
                       FROM n)
                    WHEN (c.type_cross_section_up = 'channel'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (opg.zbmin_array[array_length(opg.zbmin_array, 1)][1] - opg.zbmin_array[1][1]) AS height
                               FROM open_parametric_geometry opg
                              WHERE (c.up_op_geom = opg.id)
                            )
                     SELECT (c.z_invert_up + n.height)
                       FROM n)
                    WHEN (c.type_cross_section_up = 'valley'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (opg.zbmaj_lbank_array[1][1] - opg.zbmin_array[1][1]) AS height
                               FROM valley_cross_section_geometry opg
                              WHERE (c.up_vcs_geom = opg.id)
                            )
                     SELECT (c.z_invert_up + n.height)
                       FROM n)
                    ELSE NULL::real
                END AS "case") AS z_lbank_up,
    ( SELECT
                CASE
                    WHEN (c.type_cross_section_down = 'circular'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_down + c.down_circular_diameter))
                    WHEN (c.type_cross_section_down = 'ovoid'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_down + c.down_ovoid_height))
                    WHEN (c.type_cross_section_down = 'pipe'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (cpg.zbmin_array[array_length(cpg.zbmin_array, 1)][1] - cpg.zbmin_array[1][1]) AS height
                               FROM closed_parametric_geometry cpg
                              WHERE (c.down_cp_geom = cpg.id)
                            )
                     SELECT (c.z_invert_down + n.height)
                       FROM n)
                    WHEN (c.type_cross_section_down = 'channel'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (opg.zbmin_array[array_length(opg.zbmin_array, 1)][1] - opg.zbmin_array[1][1]) AS height
                               FROM open_parametric_geometry opg
                              WHERE (c.down_op_geom = opg.id)
                            )
                     SELECT (c.z_invert_down + n.height)
                       FROM n)
                    WHEN (c.type_cross_section_down = 'valley'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (opg.zbmaj_lbank_array[1][1] - opg.zbmin_array[1][1]) AS height
                               FROM valley_cross_section_geometry opg
                              WHERE (c.down_vcs_geom = opg.id)
                            )
                     SELECT (c.z_invert_down + n.height)
                       FROM n)
                    ELSE NULL::real
                END AS "case") AS z_lbank_down,
    ( SELECT
                CASE
                    WHEN (c.type_cross_section_up = 'circular'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_up + c.up_circular_diameter))
                    WHEN (c.type_cross_section_up = 'ovoid'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_up + c.up_ovoid_height))
                    WHEN (c.type_cross_section_up = 'pipe'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (cpg.zbmin_array[array_length(cpg.zbmin_array, 1)][1] - cpg.zbmin_array[1][1]) AS height
                               FROM closed_parametric_geometry cpg
                              WHERE (c.up_cp_geom = cpg.id)
                            )
                     SELECT (c.z_invert_up + n.height)
                       FROM n)
                    WHEN (c.type_cross_section_up = 'channel'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (opg.zbmin_array[array_length(opg.zbmin_array, 1)][1] - opg.zbmin_array[1][1]) AS height
                               FROM open_parametric_geometry opg
                              WHERE (c.up_op_geom = opg.id)
                            )
                     SELECT (c.z_invert_up + n.height)
                       FROM n)
                    WHEN (c.type_cross_section_up = 'valley'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (opg.zbmaj_rbank_array[1][1] - opg.zbmin_array[1][1]) AS height
                               FROM valley_cross_section_geometry opg
                              WHERE (c.up_vcs_geom = opg.id)
                            )
                     SELECT (c.z_invert_up + n.height)
                       FROM n)
                    ELSE NULL::real
                END AS "case") AS z_rbank_up,
    ( SELECT
                CASE
                    WHEN (c.type_cross_section_down = 'circular'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_down + c.down_circular_diameter))
                    WHEN (c.type_cross_section_down = 'ovoid'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_down + c.down_ovoid_height))
                    WHEN (c.type_cross_section_down = 'pipe'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (cpg.zbmin_array[array_length(cpg.zbmin_array, 1)][1] - cpg.zbmin_array[1][1]) AS height
                               FROM closed_parametric_geometry cpg
                              WHERE (c.down_cp_geom = cpg.id)
                            )
                     SELECT (c.z_invert_down + n.height)
                       FROM n)
                    WHEN (c.type_cross_section_down = 'channel'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (opg.zbmin_array[array_length(opg.zbmin_array, 1)][1] - opg.zbmin_array[1][1]) AS height
                               FROM open_parametric_geometry opg
                              WHERE (c.down_op_geom = opg.id)
                            )
                     SELECT (c.z_invert_down + n.height)
                       FROM n)
                    WHEN (c.type_cross_section_down = 'valley'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (opg.zbmaj_rbank_array[1][1] - opg.zbmin_array[1][1]) AS height
                               FROM valley_cross_section_geometry opg
                              WHERE (c.down_vcs_geom = opg.id)
                            )
                     SELECT (c.z_invert_down + n.height)
                       FROM n)
                    ELSE NULL::real
                END AS "case") AS z_rbank_down,
    c.validity,
    g.geom
   FROM _river_cross_section_profile c,
    _node g
  WHERE (g.id = c.id);


ALTER TABLE river_cross_section_profile OWNER TO "thierry.lepelletier";

--
-- Name: channel_reach; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW channel_reach AS
 WITH ends AS (
         SELECT p.id,
            p.geom,
            'down'::text AS typ,
            r_1.id AS reach,
            public.st_linelocatepoint(r_1.geom, p.geom) AS alpha
           FROM river_cross_section_profile p,
            reach r_1
          WHERE ((p.type_cross_section_up = 'channel'::public.hydra_cross_section_type) AND (p.type_cross_section_down <> 'channel'::public.hydra_cross_section_type) AND public.st_dwithin(p.geom, r_1.geom, (0.1)::double precision))
        UNION
         SELECT p.id,
            p.geom,
            'up'::text AS typ,
            r_1.id AS reach,
            public.st_linelocatepoint(r_1.geom, p.geom) AS alpha
           FROM river_cross_section_profile p,
            reach r_1
          WHERE ((p.type_cross_section_up <> 'channel'::public.hydra_cross_section_type) AND (p.type_cross_section_down = 'channel'::public.hydra_cross_section_type) AND public.st_dwithin(p.geom, r_1.geom, (0.1)::double precision))
        UNION
         SELECT p.id,
            p.geom,
            'down'::text AS typ,
            r_1.id AS reach,
            public.st_linelocatepoint(r_1.geom, p.geom) AS alpha
           FROM river_cross_section_profile p,
            reach r_1
          WHERE ((p.type_cross_section_up = 'channel'::public.hydra_cross_section_type) AND public.st_dwithin(p.geom, r_1.geom, (0.1)::double precision) AND (public.st_linelocatepoint(r_1.geom, p.geom) > (0.99)::double precision))
        UNION
         SELECT p.id,
            p.geom,
            'up'::text AS typ,
            r_1.id AS reach,
            public.st_linelocatepoint(r_1.geom, p.geom) AS alpha
           FROM river_cross_section_profile p,
            reach r_1
          WHERE ((p.type_cross_section_down = 'channel'::public.hydra_cross_section_type) AND public.st_dwithin(p.geom, r_1.geom, (0.1)::double precision) AND (public.st_linelocatepoint(r_1.geom, p.geom) < (0.01)::double precision))
        ), bounds AS (
         SELECT first_value(ends.reach) OVER (PARTITION BY ends.reach ORDER BY ends.alpha) AS first_value,
            ends.typ,
            lag(ends.alpha) OVER (PARTITION BY ends.reach ORDER BY ends.alpha) AS start,
            ends.alpha AS "end",
            ends.id,
            ends.reach
           FROM ends
        )
 SELECT row_number() OVER () AS id,
    public.st_linesubstring(r.geom, b.start, b."end") AS geom,
    r.id AS reach
   FROM (bounds b
     JOIN reach r ON ((r.id = b.reach)))
  WHERE (b.typ = 'down'::text);


ALTER TABLE channel_reach OWNER TO "thierry.lepelletier";

--
-- Name: constrain; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW constrain AS
 SELECT _constrain.id,
    _constrain.name,
    _constrain.geom,
    _constrain.discretized,
    _constrain.elem_length,
    _constrain.constrain_type,
    _constrain.link_attributes
   FROM _constrain;


ALTER TABLE constrain OWNER TO "thierry.lepelletier";

--
-- Name: close_point; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW close_point AS
 WITH allpoints AS (
         SELECT constrain.id,
            (public.st_dumppoints(constrain.geom)).geom AS geom
           FROM constrain
        ), ends AS (
         SELECT constrain.id,
            public.st_startpoint(constrain.geom) AS geom
           FROM constrain
        UNION
         SELECT constrain.id,
            public.st_endpoint(constrain.geom) AS geom
           FROM constrain
        ), cpoints AS (
         SELECT allpoints.id,
            public.st_collect(allpoints.geom) AS geom
           FROM allpoints
          GROUP BY allpoints.id
        ), clust AS (
         SELECT sqrt((public.st_area(public.st_minimumboundingcircle(f.gc)) / pi())) AS radius,
            public.st_numgeometries(f.gc) AS nb,
            public.st_centroid(f.gc) AS geom
           FROM ( SELECT unnest(public.st_clusterwithin(allpoints.geom, (5)::double precision)) AS gc
                   FROM allpoints) f
        UNION
         SELECT 1 AS radius,
            0 AS nb,
            public.st_force2d(e.geom) AS geom
           FROM constrain c,
            cpoints cp,
            ends e
          WHERE (public.st_dwithin(c.geom, e.geom, (1)::double precision) AND (NOT public.st_intersects(cp.geom, e.geom)) AND (e.id <> c.id) AND (c.id = cp.id))
        )
 SELECT row_number() OVER () AS id,
    clust.radius,
    clust.nb,
    (clust.geom)::public.geometry(Point,2154) AS geom
   FROM clust
  WHERE (clust.radius > (0)::double precision);


ALTER TABLE close_point OWNER TO "thierry.lepelletier";

--
-- Name: closed_parametric_geometry_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE closed_parametric_geometry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE closed_parametric_geometry_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: closed_parametric_geometry_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE closed_parametric_geometry_id_seq OWNED BY closed_parametric_geometry.id;


--
-- Name: configuration; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE configuration (
    id integer NOT NULL,
    name character varying(24) DEFAULT project.unique_name('CFG_'::character varying) NOT NULL,
    description character varying
);


ALTER TABLE configuration OWNER TO "thierry.lepelletier";

--
-- Name: configuration_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE configuration_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: configuration_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE configuration_id_seq OWNED BY configuration.id;


--
-- Name: connectivity_manhole; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW connectivity_manhole AS
 WITH up AS (
         SELECT n_1.id AS amid,
            sum(((r.down = n_1.id))::integer) AS nb_up
           FROM _link r,
            _node n_1
          WHERE (r.link_type = 'pipe'::public.hydra_link_type)
          GROUP BY n_1.id
        ), down AS (
         SELECT n_1.id AS avid,
            sum(((r.up = n_1.id))::integer) AS nb_down
           FROM _link r,
            _node n_1
          WHERE (r.link_type = 'pipe'::public.hydra_link_type)
          GROUP BY n_1.id
        )
 SELECT n.id,
    n.geom,
    up.nb_up,
    down.nb_down
   FROM _node n,
    up,
    down
  WHERE ((n.id = up.amid) AND (n.id = down.avid) AND (n.node_type = 'manhole'::public.hydra_node_type));


ALTER TABLE connectivity_manhole OWNER TO "thierry.lepelletier";

--
-- Name: connectivity_manhole_hydrology; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW connectivity_manhole_hydrology AS
 WITH up AS (
         SELECT n_1.id AS amid,
            sum(((r.down = n_1.id))::integer) AS nb_up
           FROM _link r,
            _node n_1
          WHERE (r.link_type = 'pipe'::public.hydra_link_type)
          GROUP BY n_1.id
        ), down AS (
         SELECT n_1.id AS avid,
            sum(((r.up = n_1.id))::integer) AS nb_down
           FROM _link r,
            _node n_1
          WHERE (r.link_type = 'pipe'::public.hydra_link_type)
          GROUP BY n_1.id
        )
 SELECT n.id,
    n.geom,
    up.nb_up,
    down.nb_down
   FROM _node n,
    up,
    down
  WHERE ((n.id = up.amid) AND (n.id = down.avid) AND (n.node_type = 'manhole_hydrology'::public.hydra_node_type));


ALTER TABLE connectivity_manhole_hydrology OWNER TO "thierry.lepelletier";

--
-- Name: connector_hydrology_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW connector_hydrology_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.hydrograph,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _connector_hydrology_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE connector_hydrology_link OWNER TO "thierry.lepelletier";

--
-- Name: connector_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW connector_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.main_branch,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _connector_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE connector_link OWNER TO "thierry.lepelletier";

--
-- Name: constant_inflow_bc_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW constant_inflow_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.q0,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _constant_inflow_bc_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE constant_inflow_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: coverage; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE coverage (
    id integer NOT NULL,
    domain_type public.hydra_coverage_type,
    geom public.geometry(PolygonZ,2154) NOT NULL,
    CONSTRAINT coverage_geom_check CHECK (public.st_isvalid(geom))
);


ALTER TABLE coverage OWNER TO "thierry.lepelletier";

--
-- Name: coverage_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE coverage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coverage_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: coverage_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE coverage_id_seq OWNED BY coverage.id;


--
-- Name: coverage_marker; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE coverage_marker (
    id integer NOT NULL,
    name character varying(24) DEFAULT project.unique_name('COV_NULL_'::character varying) NOT NULL,
    comment character varying(256),
    geom public.geometry(PointZ,2154) NOT NULL
);


ALTER TABLE coverage_marker OWNER TO "thierry.lepelletier";

--
-- Name: coverage_marker_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE coverage_marker_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coverage_marker_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: coverage_marker_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE coverage_marker_id_seq OWNED BY coverage_marker.id;


--
-- Name: crossroad_node; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW crossroad_node AS
 SELECT p.id,
    p.name,
    c.area,
    c.z_ground,
    c.h,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.configuration AS configuration_json
   FROM _crossroad_node c,
    _node p
  WHERE (p.id = c.id);


ALTER TABLE crossroad_node OWNER TO "thierry.lepelletier";

--
-- Name: deriv_pump_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW deriv_pump_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.q_pump,
    c.qz_array,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _deriv_pump_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE deriv_pump_link OWNER TO "thierry.lepelletier";

--
-- Name: domain_2d; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE domain_2d (
    id integer NOT NULL,
    name character varying(16) DEFAULT project.unique_name('DOMAIN_2D_'::character varying) NOT NULL,
    geom public.geometry(PolygonZ,2154),
    comment character varying(256),
    CONSTRAINT domain_2d_name_check CHECK (((name)::text <> 'default_domain'::text))
);


ALTER TABLE domain_2d OWNER TO "thierry.lepelletier";

--
-- Name: domain_2d_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE domain_2d_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE domain_2d_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: domain_2d_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE domain_2d_id_seq OWNED BY domain_2d.id;


--
-- Name: domain_2d_marker; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE domain_2d_marker (
    id integer NOT NULL,
    name character varying(24) DEFAULT project.unique_name('DOMAIN_MARKER_'::character varying) NOT NULL,
    domain_2d integer,
    geom public.geometry(PointZ,2154) NOT NULL,
    comment character varying(256)
);


ALTER TABLE domain_2d_marker OWNER TO "thierry.lepelletier";

--
-- Name: domain_2d_marker_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE domain_2d_marker_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE domain_2d_marker_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: domain_2d_marker_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE domain_2d_marker_id_seq OWNED BY domain_2d_marker.id;


--
-- Name: elem_2d_node; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW elem_2d_node AS
 SELECT p.id,
    p.name,
    c.area,
    c.zb,
    c.rk,
    c.domain_2d,
    c.contour,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.configuration AS configuration_json
   FROM _elem_2d_node c,
    _node p
  WHERE (p.id = c.id);


ALTER TABLE elem_2d_node OWNER TO "thierry.lepelletier";

--
-- Name: froude_bc_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW froude_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _froude_bc_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE froude_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: fuse_spillway_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW fuse_spillway_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.z_invert,
    c.width,
    c.cc,
    c.break_mode,
    c.z_break,
    c.t_break,
    c.grp,
    c.dt_fracw_array,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _fuse_spillway_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE fuse_spillway_link OWNER TO "thierry.lepelletier";

--
-- Name: gate_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW gate_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.z_invert,
    c.z_ceiling,
    c.width,
    c.cc,
    c.action_gate_type,
    c.mode_valve,
    c.z_gate,
    c.v_max_cms,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _gate_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE gate_link OWNER TO "thierry.lepelletier";

--
-- Name: gate_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW gate_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.z_invert,
    c.z_ceiling,
    c.width,
    c.cc,
    c.action_gate_type,
    c.mode_valve,
    c.z_gate,
    c.v_max_cms,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _gate_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE gate_singularity OWNER TO "thierry.lepelletier";

--
-- Name: generation_step; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE generation_step (
    id integer NOT NULL,
    name character varying(24) DEFAULT project.unique_name('GEN_STEP_'::character varying) NOT NULL,
    description character varying
);


ALTER TABLE generation_step OWNER TO "thierry.lepelletier";

--
-- Name: generation_step_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE generation_step_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE generation_step_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: generation_step_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE generation_step_id_seq OWNED BY generation_step.id;


--
-- Name: hydraulic_cut_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW hydraulic_cut_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.qz_array,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _hydraulic_cut_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE hydraulic_cut_singularity OWNER TO "thierry.lepelletier";

--
-- Name: hydrograph_bc_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW hydrograph_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.storage_area,
    c.tq_array,
    c.constant_dry_flow,
    c.distrib_coef,
    c.lag_time_hr,
    c.sector,
    c.hourly_modulation,
    c.pollution_quality_mode,
    (c.pollution_quality_param)::character varying AS pollution_quality_param,
    c.external_file_data,
    c.pollution_quality_param AS pollution_quality_param_json,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _hydrograph_bc_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE hydrograph_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: hydrology_bc_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW hydrology_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _hydrology_bc_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE hydrology_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: manhole_hydrology_node; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW manhole_hydrology_node AS
 SELECT p.id,
    p.name,
    c.area,
    c.z_ground,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.configuration AS configuration_json
   FROM _manhole_hydrology_node c,
    _node p
  WHERE (p.id = c.id);


ALTER TABLE manhole_hydrology_node OWNER TO "thierry.lepelletier";

--
-- Name: manhole_node; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW manhole_node AS
 SELECT p.id,
    p.name,
    c.area,
    c.z_ground,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.configuration AS configuration_json
   FROM _manhole_node c,
    _node p
  WHERE (p.id = c.id);


ALTER TABLE manhole_node OWNER TO "thierry.lepelletier";

--
-- Name: mesh_2d_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW mesh_2d_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.z_invert,
    c.lateral_contraction_coef,
    c.border,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _mesh_2d_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE mesh_2d_link OWNER TO "thierry.lepelletier";

--
-- Name: model_connect_bc_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW model_connect_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.cascade_mode,
    c.zq_array,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _model_connect_bc_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE model_connect_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: network_overflow_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW network_overflow_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.z_overflow,
    c.area,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _network_overflow_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE network_overflow_link OWNER TO "thierry.lepelletier";

--
-- Name: overflow_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW overflow_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.z_crest1,
    c.width1,
    c.z_crest2,
    c.width2,
    c.cc,
    c.lateral_contraction_coef,
    c.break_mode,
    c.z_break,
    c.t_break,
    c.z_invert,
    c.width_breach,
    c.grp,
    c.dt_fracw_array,
    c.border,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _overflow_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE overflow_link OWNER TO "thierry.lepelletier";

--
-- Name: param_headloss_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW param_headloss_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.q_dz_array,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _param_headloss_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE param_headloss_singularity OWNER TO "thierry.lepelletier";

--
-- Name: pipe_branch_marker_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW pipe_branch_marker_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.pk0_km,
    c.dx,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _pipe_branch_marker_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE pipe_branch_marker_singularity OWNER TO "thierry.lepelletier";

--
-- Name: pipe_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW pipe_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.comment,
    c.z_invert_up,
    c.z_invert_down,
    c.cross_section_type,
    c.h_sable,
    c.branch,
    c.rk,
    c.circular_diameter,
    c.ovoid_height,
    c.ovoid_top_diameter,
    c.ovoid_invert_diameter,
    c.cp_geom,
    c.op_geom,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json,
    ( SELECT
                CASE
                    WHEN ((((c.z_invert_up - c.z_invert_down) * public.st_length(p.geom)) > (0)::double precision) AND (public.st_length(p.geom) <> (0)::double precision)) THEN ( SELECT
                            CASE
                                WHEN (c.cross_section_type = 'circular'::public.hydra_cross_section_type) THEN ( SELECT
CASE
 WHEN (c.circular_diameter > (0)::double precision) THEN ( WITH n AS (
    SELECT ((pi() / (4)::double precision) * pow((c.circular_diameter)::double precision, (2.0)::double precision)) AS s,
       (pi() * c.circular_diameter) AS p,
       c.rk AS k
   )
  SELECT (((n.k * n.s) * pow((n.s / n.p), ((2.0 / 3.0))::double precision)) * sqrt(((c.z_invert_up - c.z_invert_down) / public.st_length(p.geom))))
    FROM n)
 ELSE ('-999'::integer)::double precision
END AS "case")
                                WHEN (c.cross_section_type = 'ovoid'::public.hydra_cross_section_type) THEN ( WITH n AS (
 SELECT (((pi() / (8)::double precision) * ((c.ovoid_top_diameter * c.ovoid_top_diameter) + (c.ovoid_invert_diameter * c.ovoid_invert_diameter))) + (((0.5)::double precision * (c.ovoid_height - ((0.5)::double precision * (c.ovoid_top_diameter + c.ovoid_invert_diameter)))) * (c.ovoid_top_diameter + c.ovoid_invert_diameter))) AS s,
    (((pi() / (2)::double precision) * (c.ovoid_top_diameter + c.ovoid_invert_diameter)) + ((2)::double precision * (c.ovoid_height - ((0.5)::double precision * (c.ovoid_top_diameter + c.ovoid_invert_diameter))))) AS p,
    c.rk AS k
)
                                 SELECT
CASE
 WHEN ((n.s > (0)::double precision) AND (n.p > (0)::double precision)) THEN ( SELECT (((n_1.k * n_1.s) * pow((n_1.s / n_1.p), ((2.0 / 3.0))::double precision)) * sqrt(((c.z_invert_up - c.z_invert_down) / public.st_length(p.geom))))
    FROM n n_1)
 ELSE ('-999'::integer)::double precision
END AS "case"
                                   FROM n)
                                WHEN (c.cross_section_type = 'pipe'::public.hydra_cross_section_type) THEN ( WITH n AS (
 SELECT geometric_calc_s_fct(cpg.zbmin_array) AS s,
    cp_geometric_calc_p_fct(cpg.zbmin_array) AS p,
    c.rk AS k
   FROM closed_parametric_geometry cpg
  WHERE (c.cp_geom = cpg.id)
)
                                 SELECT
CASE
 WHEN ((n.s > (0)::double precision) AND (n.p > (0)::double precision)) THEN ( SELECT (((n_1.k * n_1.s) * pow((n_1.s / n_1.p), ((2.0 / 3.0))::double precision)) * sqrt(((c.z_invert_up - c.z_invert_down) / public.st_length(p.geom))))
    FROM n n_1)
 ELSE ('-999'::integer)::double precision
END AS "case"
                                   FROM n)
                                WHEN (c.cross_section_type = 'channel'::public.hydra_cross_section_type) THEN ( WITH n AS (
 SELECT geometric_calc_s_fct(opg.zbmin_array) AS s,
    op_geometric_calc_p_fct(opg.zbmin_array) AS p,
    c.rk AS k
   FROM open_parametric_geometry opg
  WHERE (c.op_geom = opg.id)
)
                                 SELECT
CASE
 WHEN ((n.s > (0)::double precision) AND (n.p > (0)::double precision)) THEN ( SELECT (((n_1.k * n_1.s) * pow((n_1.s / n_1.p), ((2.0 / 3.0))::double precision)) * sqrt(((c.z_invert_up - c.z_invert_down) / public.st_length(p.geom))))
    FROM n n_1)
 ELSE ('-999'::integer)::double precision
END AS "case"
                                   FROM n)
                                ELSE (0.0)::double precision
                            END AS "case")
                    ELSE ('-999'::integer)::double precision
                END AS "case") AS qcap,
    ( SELECT
                CASE
                    WHEN (c.cross_section_type = 'circular'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_up + c.circular_diameter))
                    WHEN (c.cross_section_type = 'ovoid'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_up + c.ovoid_height))
                    WHEN (c.cross_section_type = 'pipe'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (cpg.zbmin_array[array_length(cpg.zbmin_array, 1)][1] - cpg.zbmin_array[1][1]) AS height
                               FROM closed_parametric_geometry cpg
                              WHERE (c.cp_geom = cpg.id)
                            )
                     SELECT (c.z_invert_up + n.height)
                       FROM n)
                    WHEN (c.cross_section_type = 'channel'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (opg.zbmin_array[array_length(opg.zbmin_array, 1)][1] - opg.zbmin_array[1][1]) AS height
                               FROM open_parametric_geometry opg
                              WHERE (c.op_geom = opg.id)
                            )
                     SELECT (c.z_invert_up + n.height)
                       FROM n)
                    ELSE NULL::real
                END AS "case") AS z_vault_up,
    ( SELECT
                CASE
                    WHEN (c.cross_section_type = 'circular'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_down + c.circular_diameter))
                    WHEN (c.cross_section_type = 'ovoid'::public.hydra_cross_section_type) THEN ( SELECT (c.z_invert_down + c.ovoid_height))
                    WHEN (c.cross_section_type = 'pipe'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (cpg.zbmin_array[array_length(cpg.zbmin_array, 1)][1] - cpg.zbmin_array[1][1]) AS height
                               FROM closed_parametric_geometry cpg
                              WHERE (c.cp_geom = cpg.id)
                            )
                     SELECT (c.z_invert_down + n.height)
                       FROM n)
                    WHEN (c.cross_section_type = 'channel'::public.hydra_cross_section_type) THEN ( WITH n AS (
                             SELECT (opg.zbmin_array[array_length(opg.zbmin_array, 1)][1] - opg.zbmin_array[1][1]) AS height
                               FROM open_parametric_geometry opg
                              WHERE (c.op_geom = opg.id)
                            )
                     SELECT (c.z_invert_down + n.height)
                       FROM n)
                    ELSE NULL::real
                END AS "case") AS z_vault_down,
    ( SELECT
                CASE
                    WHEN (c.cross_section_type = 'channel'::public.hydra_cross_section_type) THEN (( SELECT c.z_invert_up))::double precision
                    ELSE (( SELECT manhole_node.z_ground
                       FROM manhole_node
                      WHERE (p.up = manhole_node.id)))::double precision
                END AS z_ground) AS z_tn_up,
    ( SELECT
                CASE
                    WHEN (c.cross_section_type = 'channel'::public.hydra_cross_section_type) THEN (( SELECT c.z_invert_down))::double precision
                    ELSE (( SELECT manhole_node.z_ground
                       FROM manhole_node
                      WHERE (p.down = manhole_node.id)))::double precision
                END AS z_ground) AS z_tn_down
   FROM _pipe_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE pipe_link OWNER TO "thierry.lepelletier";

--
-- Name: porous_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW porous_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.z_invert,
    c.width,
    c.transmitivity,
    c.border,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _porous_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE porous_link OWNER TO "thierry.lepelletier";

--
-- Name: pump_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW pump_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.npump,
    c.zregul_array,
    c.hq_array,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _pump_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE pump_link OWNER TO "thierry.lepelletier";

--
-- Name: qq_split_hydrology_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW qq_split_hydrology_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.qq_array,
    c.downstream,
    c.downstream_type,
    c.split1,
    c.split1_type,
    c.split2,
    c.split2_type,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _qq_split_hydrology_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE qq_split_hydrology_singularity OWNER TO "thierry.lepelletier";

--
-- Name: regul_gate_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW regul_gate_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.z_invert,
    c.z_ceiling,
    c.width,
    c.cc,
    c.action_gate_type,
    c.z_invert_stop,
    c.z_ceiling_stop,
    c.v_max_cms,
    c.dt_regul_hr,
    c.mode_regul,
    c.z_control_node,
    c.z_pid_array,
    c.z_tz_array,
    c.q_z_crit,
    c.q_tq_array,
    c.nr_z_gate,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _regul_gate_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE regul_gate_link OWNER TO "thierry.lepelletier";

--
-- Name: regul_sluice_gate_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW regul_sluice_gate_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.z_invert,
    c.z_ceiling,
    c.width,
    c.cc,
    c.action_gate_type,
    c.z_invert_stop,
    c.z_ceiling_stop,
    c.v_max_cms,
    c.dt_regul_hr,
    c.mode_regul,
    c.z_control_node,
    c.z_pid_array,
    c.z_tz_array,
    c.q_z_crit,
    c.q_tq_array,
    c.nr_z_gate,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _regul_sluice_gate_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE regul_sluice_gate_singularity OWNER TO "thierry.lepelletier";

--
-- Name: reservoir_rs_hydrology_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW reservoir_rs_hydrology_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.drainage,
    c.drainage_type,
    c.overflow,
    c.overflow_type,
    c.q_drainage,
    c.z_ini,
    c.zs_array,
    c.treatment_mode,
    (c.treatment_param)::character varying AS treatment_param,
    c.treatment_param AS treatment_param_json,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _reservoir_rs_hydrology_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE reservoir_rs_hydrology_singularity OWNER TO "thierry.lepelletier";

--
-- Name: reservoir_rsp_hydrology_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW reservoir_rsp_hydrology_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.drainage,
    c.drainage_type,
    c.overflow,
    c.overflow_type,
    c.z_ini,
    c.zr_sr_qf_qs_array,
    c.treatment_mode,
    (c.treatment_param)::character varying AS treatment_param,
    c.treatment_param AS treatment_param_json,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _reservoir_rsp_hydrology_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE reservoir_rsp_hydrology_singularity OWNER TO "thierry.lepelletier";

--
-- Name: river_node; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW river_node AS
 SELECT p.id,
    p.name,
    c.reach,
    c.z_ground,
    c.area,
    ( SELECT (((public.st_linelocatepoint(reach.geom, p.geom) * public.st_length(reach.geom)) / (1000)::double precision) + reach.pk0_km)
           FROM reach reach
          WHERE (reach.id = c.reach)) AS pk_km,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.configuration AS configuration_json
   FROM _river_node c,
    _node p
  WHERE (p.id = c.id);


ALTER TABLE river_node OWNER TO "thierry.lepelletier";

--
-- Name: routing_hydrology_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW routing_hydrology_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.cross_section,
    c.length,
    c.slope,
    c.hydrograph,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _routing_hydrology_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE routing_hydrology_link OWNER TO "thierry.lepelletier";

--
-- Name: station; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE station (
    id integer NOT NULL,
    name character varying(16) DEFAULT project.unique_name('STATION_'::character varying) NOT NULL,
    geom public.geometry(PolygonZ,2154) NOT NULL,
    CONSTRAINT station_geom_check CHECK (public.st_isvalid(geom))
);


ALTER TABLE station OWNER TO "thierry.lepelletier";

--
-- Name: station_node; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW station_node AS
 SELECT p.id,
    p.name,
    c.area,
    c.z_invert,
    c.station,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.configuration AS configuration_json
   FROM _station_node c,
    _node p
  WHERE (p.id = c.id);


ALTER TABLE station_node OWNER TO "thierry.lepelletier";

--
-- Name: storage_node; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW storage_node AS
 SELECT p.id,
    p.name,
    c.zs_array,
    c.zini,
    c.contour,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.configuration AS configuration_json
   FROM _storage_node c,
    _node p
  WHERE (p.id = c.id);


ALTER TABLE storage_node OWNER TO "thierry.lepelletier";

--
-- Name: street_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW street_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.width,
    c.length,
    c.rk,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _street_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE street_link OWNER TO "thierry.lepelletier";

--
-- Name: strickler_bc_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW strickler_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.slope,
    c.k,
    c.width,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _strickler_bc_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE strickler_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: strickler_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW strickler_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.z_crest1,
    c.width1,
    c.length,
    c.rk,
    c.z_crest2,
    c.width2,
    c.border,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _strickler_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE strickler_link OWNER TO "thierry.lepelletier";

--
-- Name: tank_bc_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW tank_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.zs_array,
    c.zini,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _tank_bc_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE tank_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: tz_bc_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW tz_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.tz_array,
    c.cyclic,
    c.external_file_data,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _tz_bc_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE tz_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: weir_bc_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW weir_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.z_weir,
    c.width,
    c.cc,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _weir_bc_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE weir_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: weir_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW weir_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.z_invert,
    c.width,
    c.cc,
    c.z_weir,
    c.v_max_cms,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM _weir_link c,
    _link p
  WHERE (p.id = c.id);


ALTER TABLE weir_link OWNER TO "thierry.lepelletier";

--
-- Name: zq_bc_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW zq_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.zq_array,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _zq_bc_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE zq_bc_singularity OWNER TO "thierry.lepelletier";

--
-- Name: zq_split_hydrology_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW zq_split_hydrology_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.downstream,
    c.downstream_type,
    c.split1,
    c.split1_type,
    c.split2,
    c.split2_type,
    c.downstream_law,
    (c.downstream_param)::character varying AS downstream_param,
    c.split1_law,
    (c.split1_param)::character varying AS split1_param,
    c.split2_law,
    (c.split2_param)::character varying AS split2_param,
    c.downstream_param AS downstream_param_json,
    c.split1_param AS split1_param_json,
    c.split2_param AS split2_param_json,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _zq_split_hydrology_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE zq_split_hydrology_singularity OWNER TO "thierry.lepelletier";

--
-- Name: zregul_weir_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW zregul_weir_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.z_invert,
    c.z_regul,
    c.width,
    c.cc,
    c.mode_regul,
    c.reoxy_law,
    (c.reoxy_param)::character varying AS reoxy_param,
    c.reoxy_param AS reoxy_param_json,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _zregul_weir_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE zregul_weir_singularity OWNER TO "thierry.lepelletier";

--
-- Name: invalid; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW invalid AS
 WITH node_invalidity_reason AS (
         SELECT new_1.id,
            ((((
                CASE
                    WHEN (new_1.area IS NOT NULL) THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN (new_1.area > (0)::double precision) THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN (new_1.z_ground IS NOT NULL) THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.h IS NOT NULL) THEN ''::text
                    ELSE '   h is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.h >= (0)::double precision) THEN ''::text
                    ELSE '   h>=0   '::text
                END) AS reason
           FROM crossroad_node new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN (new_1.area IS NOT NULL) THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN (new_1.area > (0)::double precision) THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN (new_1.zb IS NOT NULL) THEN ''::text
                    ELSE '   zb is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.rk IS NOT NULL) THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.rk > (0)::double precision) THEN ''::text
                    ELSE '   rk>0   '::text
                END) AS reason
           FROM elem_2d_node new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN (new_1.area IS NOT NULL) THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN (new_1.area > (0)::double precision) THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN (new_1.z_ground IS NOT NULL) THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) AS reason
           FROM manhole_hydrology_node new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN (new_1.z_ground IS NOT NULL) THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END ||
                CASE
                    WHEN (new_1.area IS NOT NULL) THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.area > (0)::double precision) THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN (new_1.reach IS NOT NULL) THEN ''::text
                    ELSE '   reach is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT (NOT (EXISTS ( SELECT 1
                               FROM station
                              WHERE public.st_intersects(station.geom, new_1.geom))))) THEN ''::text
                    ELSE '   (select not exists(select 1 from model1.station where st_intersects(geom, new.geom)))   '::text
                END) AS reason
           FROM river_node new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN (new_1.area IS NOT NULL) THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN (new_1.area > (0)::double precision) THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN (new_1.z_invert IS NOT NULL) THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.station IS NOT NULL) THEN ''::text
                    ELSE '   station is not null    '::text
                END) ||
                CASE
                    WHEN ( SELECT public.st_intersects(new_1.geom, station.geom) AS st_intersects
                       FROM station
                      WHERE (station.id = new_1.station)) THEN ''::text
                    ELSE '   (select st_intersects(new.geom, geom) from model1.station where id=new.station)   '::text
                END) AS reason
           FROM station_node new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN (new_1.area_ha IS NOT NULL) THEN ''::text
                    ELSE '   area_ha is not null   '::text
                END ||
                CASE
                    WHEN (new_1.area_ha > (0)::double precision) THEN ''::text
                    ELSE '   area_ha>0   '::text
                END) ||
                CASE
                    WHEN (new_1.rl IS NOT NULL) THEN ''::text
                    ELSE '   rl is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.rl > (0)::double precision) THEN ''::text
                    ELSE '   rl>0   '::text
                END) ||
                CASE
                    WHEN (new_1.slope IS NOT NULL) THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.c_imp IS NOT NULL) THEN ''::text
                    ELSE '   c_imp is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.c_imp >= (0)::double precision) THEN ''::text
                    ELSE '   c_imp>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.c_imp <= (1)::double precision) THEN ''::text
                    ELSE '   c_imp<=1   '::text
                END) ||
                CASE
                    WHEN (new_1.netflow_type IS NOT NULL) THEN ''::text
                    ELSE '   netflow_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.netflow_param IS NOT NULL) THEN ''::text
                    ELSE '   netflow_param is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.runoff_type IS NOT NULL) THEN ''::text
                    ELSE '   runoff_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.runoff_param IS NOT NULL) THEN ''::text
                    ELSE '   runoff_param is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.q_limit IS NOT NULL) THEN ''::text
                    ELSE '   q_limit is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.q0 IS NOT NULL) THEN ''::text
                    ELSE '   q0 is not null   '::text
                END) AS reason
           FROM catchment_node new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN (new_1.zs_array IS NOT NULL) THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN (new_1.zini IS NOT NULL) THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zs_array, 1) <= 10) THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10    '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zs_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(zs_array, 1) >=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zs_array, 2) = 2) THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END) AS reason
           FROM storage_node new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN (new_1.area IS NOT NULL) THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN (new_1.area > (0)::double precision) THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN (new_1.z_ground IS NOT NULL) THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT (NOT (EXISTS ( SELECT 1
                               FROM station
                              WHERE public.st_intersects(station.geom, new_1.geom))))) THEN ''::text
                    ELSE '   (select not exists(select 1 from model1.station where st_intersects(geom, new.geom)))   '::text
                END) ||
                CASE
                    WHEN check_connected_pipes(new_1.id) THEN ''::text
                    ELSE '   model1.check_connected_pipes(new.id)   '::text
                END) AS reason
           FROM manhole_node new_1
          WHERE (NOT new_1.validity)
        ), link_invalidity_reason AS (
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN (new_1.width IS NOT NULL) THEN ''::text
                    ELSE '   width is not null   '::text
                END ||
                CASE
                    WHEN (new_1.width >= (0)::double precision) THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.length IS NOT NULL) THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.length >= (0)::double precision) THEN ''::text
                    ELSE '   length>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.rk IS NOT NULL) THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.rk > (0)::double precision) THEN ''::text
                    ELSE '   rk>0   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM street_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((((((
                CASE
                    WHEN (new_1.z_invert IS NOT NULL) THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN (new_1.z_ceiling IS NOT NULL) THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width IS NOT NULL) THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width >= (0)::double precision) THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.cc IS NOT NULL) THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cc <= (1)::double precision) THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN (new_1.cc >= (0)::double precision) THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.action_gate_type IS NOT NULL) THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_invert_stop IS NOT NULL) THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_ceiling_stop IS NOT NULL) THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.v_max_cms IS NOT NULL) THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.v_max_cms >= (0)::double precision) THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.dt_regul_hr IS NOT NULL) THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (new_1.z_control_node IS NOT NULL)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (new_1.z_pid_array IS NOT NULL)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (new_1.z_tz_array IS NOT NULL)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (array_length(new_1.z_tz_array, 1) <= 10)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (array_length(new_1.z_tz_array, 1) >= 1)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (array_length(new_1.z_tz_array, 2) = 2)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'discharge'::public.hydra_gate_mode_regul) OR (new_1.q_z_crit IS NOT NULL)) THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'discharge'::public.hydra_gate_mode_regul) OR (array_length(new_1.q_tq_array, 1) <= 10)) THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'discharge'::public.hydra_gate_mode_regul) OR (array_length(new_1.q_tq_array, 1) >= 1)) THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'discharge'::public.hydra_gate_mode_regul) OR (array_length(new_1.q_tq_array, 2) = 2)) THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'no_regulation'::public.hydra_gate_mode_regul) OR (new_1.nr_z_gate IS NOT NULL)) THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM regul_gate_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((((((((((((((
                CASE
                    WHEN (new_1.z_invert IS NOT NULL) THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN (new_1.z_ceiling IS NOT NULL) THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_ceiling > new_1.z_invert) THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN (new_1.width IS NOT NULL) THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cc IS NOT NULL) THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cc <= (1)::double precision) THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN (new_1.cc >= (0)::double precision) THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN (new_1.action_gate_type IS NOT NULL) THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.mode_valve IS NOT NULL) THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_gate IS NOT NULL) THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_gate >= new_1.z_invert) THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN (new_1.z_gate <= new_1.z_ceiling) THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN (new_1.v_max_cms IS NOT NULL) THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM gate_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN (new_1.cross_section IS NOT NULL) THEN ''::text
                    ELSE '   cross_section is not null   '::text
                END ||
                CASE
                    WHEN (new_1.cross_section >= (0)::double precision) THEN ''::text
                    ELSE '   cross_section>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.length IS NOT NULL) THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.length >= (0)::double precision) THEN ''::text
                    ELSE '   length>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.slope IS NOT NULL) THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.down_type = 'manhole_hydrology'::public.hydra_node_type) OR ((new_1.down_type = ANY (ARRAY['manhole'::public.hydra_node_type, 'river'::public.hydra_node_type, 'station'::public.hydra_node_type])) AND (new_1.hydrograph IS NOT NULL))) THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'') and (hydrograph is not null))   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM routing_hydrology_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((((((
                CASE
                    WHEN (new_1.z_invert IS NOT NULL) THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN (new_1.width IS NOT NULL) THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width >= (0)::double precision) THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.cc IS NOT NULL) THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cc <= (1)::double precision) THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN (new_1.cc >= (0)::double precision) THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.v_max_cms IS NOT NULL) THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.v_max_cms >= (0)::double precision) THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM weir_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((((((((((((((((((((((((((
                CASE
                    WHEN (new_1.z_crest1 IS NOT NULL) THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN (new_1.width1 IS NOT NULL) THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width1 >= (0)::double precision) THEN ''::text
                    ELSE '   width1>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.z_crest2 IS NOT NULL) THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_crest2 >= new_1.z_crest1) THEN ''::text
                    ELSE '   z_crest2>=z_crest1   '::text
                END) ||
                CASE
                    WHEN (new_1.width2 IS NOT NULL) THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width2 >= (0)::double precision) THEN ''::text
                    ELSE '   width2>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.cc IS NOT NULL) THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cc <= (1)::double precision) THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN (new_1.cc >= (0)::double precision) THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.lateral_contraction_coef IS NOT NULL) THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.lateral_contraction_coef <= (1)::double precision) THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN (new_1.lateral_contraction_coef >= (0)::double precision) THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.break_mode IS NOT NULL) THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode <> 'zw_critical'::public.hydra_fuse_spillway_break_mode) OR (new_1.z_break IS NOT NULL)) THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode <> 'time_critical'::public.hydra_fuse_spillway_break_mode) OR (new_1.t_break IS NOT NULL)) THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (new_1.width_breach IS NOT NULL)) THEN ''::text
                    ELSE '   break_mode=''none'' or width_breach is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (new_1.z_invert IS NOT NULL)) THEN ''::text
                    ELSE '   break_mode=''none'' or z_invert is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (new_1.grp IS NOT NULL)) THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (new_1.grp > 0)) THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (new_1.grp < 100)) THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (new_1.dt_fracw_array IS NOT NULL)) THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (array_length(new_1.dt_fracw_array, 1) <= 10)) THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (array_length(new_1.dt_fracw_array, 1) >= 1)) THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (array_length(new_1.dt_fracw_array, 2) = 2)) THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM overflow_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((((((((((((
                CASE
                    WHEN (new_1.z_invert_up IS NOT NULL) THEN ''::text
                    ELSE '   z_invert_up is not null   '::text
                END ||
                CASE
                    WHEN (new_1.z_invert_down IS NOT NULL) THEN ''::text
                    ELSE '   z_invert_down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.h_sable IS NOT NULL) THEN ''::text
                    ELSE '   h_sable is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cross_section_type IS NOT NULL) THEN ''::text
                    ELSE '   cross_section_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cross_section_type <> 'valley'::public.hydra_cross_section_type) THEN ''::text
                    ELSE '   cross_section_type not in (''valley'')   '::text
                END) ||
                CASE
                    WHEN (new_1.rk IS NOT NULL) THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.rk > (0)::double precision) THEN ''::text
                    ELSE '   rk >0   '::text
                END) ||
                CASE
                    WHEN ((new_1.cross_section_type <> 'circular'::public.hydra_cross_section_type) OR ((new_1.circular_diameter IS NOT NULL) AND (new_1.circular_diameter > (0)::double precision))) THEN ''::text
                    ELSE '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN ((new_1.cross_section_type <> 'ovoid'::public.hydra_cross_section_type) OR ((new_1.ovoid_top_diameter IS NOT NULL) AND (new_1.ovoid_top_diameter > (0)::double precision))) THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN ((new_1.cross_section_type <> 'ovoid'::public.hydra_cross_section_type) OR ((new_1.ovoid_invert_diameter IS NOT NULL) AND (new_1.ovoid_invert_diameter > (0)::double precision))) THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN ((new_1.cross_section_type <> 'ovoid'::public.hydra_cross_section_type) OR ((new_1.ovoid_height IS NOT NULL) AND (new_1.ovoid_height > (0)::double precision))) THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   '::text
                END) ||
                CASE
                    WHEN ((new_1.cross_section_type <> 'ovoid'::public.hydra_cross_section_type) OR (new_1.ovoid_height > ((new_1.ovoid_top_diameter + new_1.ovoid_invert_diameter) / (2)::double precision))) THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   '::text
                END) ||
                CASE
                    WHEN ((new_1.cross_section_type <> 'pipe'::public.hydra_cross_section_type) OR (new_1.cp_geom IS NOT NULL)) THEN ''::text
                    ELSE '   cross_section_type!=''pipe'' or cp_geom is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.cross_section_type <> 'channel'::public.hydra_cross_section_type) OR (new_1.op_geom IS NOT NULL)) THEN ''::text
                    ELSE '   cross_section_type!=''channel'' or op_geom is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM pipe_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((((
                CASE
                    WHEN (new_1.z_overflow IS NOT NULL) THEN ''::text
                    ELSE '   z_overflow is not null   '::text
                END ||
                CASE
                    WHEN (new_1.area IS NOT NULL) THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.area >= (0)::double precision) THEN ''::text
                    ELSE '   area>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM network_overflow_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN (new_1.z_invert IS NOT NULL) THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN (new_1.width IS NOT NULL) THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width >= (0)::double precision) THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.transmitivity IS NOT NULL) THEN ''::text
                    ELSE '   transmitivity is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.transmitivity >= (0)::double precision) THEN ''::text
                    ELSE '   transmitivity>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM porous_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((((((((((((
                CASE
                    WHEN (new_1.npump IS NOT NULL) THEN ''::text
                    ELSE '   npump is not null   '::text
                END ||
                CASE
                    WHEN (new_1.npump <= 10) THEN ''::text
                    ELSE '   npump<=10   '::text
                END) ||
                CASE
                    WHEN (new_1.npump >= 1) THEN ''::text
                    ELSE '   npump>=1   '::text
                END) ||
                CASE
                    WHEN (new_1.zregul_array IS NOT NULL) THEN ''::text
                    ELSE '   zregul_array is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.hq_array IS NOT NULL) THEN ''::text
                    ELSE '   hq_array is not null   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zregul_array, 1) = new_1.npump) THEN ''::text
                    ELSE '   array_length(zregul_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zregul_array, 2) = 2) THEN ''::text
                    ELSE '   array_length(zregul_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.hq_array, 1) = new_1.npump) THEN ''::text
                    ELSE '   array_length(hq_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.hq_array, 2) <= 10) THEN ''::text
                    ELSE '   array_length(hq_array, 2)<=10   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.hq_array, 2) >= 1) THEN ''::text
                    ELSE '   array_length(hq_array, 2)>=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.hq_array, 3) = 2) THEN ''::text
                    ELSE '   array_length(hq_array, 3)=2   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM pump_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN (new_1.q_pump IS NOT NULL) THEN ''::text
                    ELSE '   q_pump is not null   '::text
                END ||
                CASE
                    WHEN (new_1.q_pump >= (0)::double precision) THEN ''::text
                    ELSE '   q_pump>= 0   '::text
                END) ||
                CASE
                    WHEN (new_1.qz_array IS NOT NULL) THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.qz_array, 1) <= 10) THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.qz_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.qz_array, 2) = 2) THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM deriv_pump_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN ((new_1.down_type = 'manhole_hydrology'::public.hydra_node_type) OR ((new_1.down_type = ANY (ARRAY['manhole'::public.hydra_node_type, 'river'::public.hydra_node_type, 'station'::public.hydra_node_type])) AND (new_1.hydrograph IS NOT NULL))) THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'') and (hydrograph is not null))   '::text
                END ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM connector_hydrology_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN (new_1.law_type IS NOT NULL) THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN (new_1.param IS NOT NULL) THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM borda_headloss_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((((((((((((
                CASE
                    WHEN (new_1.z_crest1 IS NOT NULL) THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN (new_1.width1 IS NOT NULL) THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width1 > (0)::double precision) THEN ''::text
                    ELSE '   width1>0   '::text
                END) ||
                CASE
                    WHEN (new_1.length IS NOT NULL) THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.length > (0)::double precision) THEN ''::text
                    ELSE '   length>0   '::text
                END) ||
                CASE
                    WHEN (new_1.rk IS NOT NULL) THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.rk >= (0)::double precision) THEN ''::text
                    ELSE '   rk>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.z_crest2 IS NOT NULL) THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_crest2 > new_1.z_crest1) THEN ''::text
                    ELSE '   z_crest2>z_crest1   '::text
                END) ||
                CASE
                    WHEN (new_1.width2 IS NOT NULL) THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width2 > new_1.width1) THEN ''::text
                    ELSE '   width2>width1   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM strickler_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((((((((((((((
                CASE
                    WHEN (new_1.z_invert IS NOT NULL) THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN (new_1.width IS NOT NULL) THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width >= ('0'::numeric)::double precision) THEN ''::text
                    ELSE '   width>=0.   '::text
                END) ||
                CASE
                    WHEN (new_1.cc IS NOT NULL) THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cc <= (1)::double precision) THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN (new_1.cc >= ('0'::numeric)::double precision) THEN ''::text
                    ELSE '   cc>=0.   '::text
                END) ||
                CASE
                    WHEN (new_1.break_mode IS NOT NULL) THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode <> 'zw_critical'::public.hydra_fuse_spillway_break_mode) OR (new_1.z_break IS NOT NULL)) THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode <> 'time_critical'::public.hydra_fuse_spillway_break_mode) OR (new_1.t_break IS NOT NULL)) THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (new_1.grp IS NOT NULL)) THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (new_1.grp > 0)) THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (new_1.grp < 100)) THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (new_1.dt_fracw_array IS NOT NULL)) THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (array_length(new_1.dt_fracw_array, 1) <= 10)) THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (array_length(new_1.dt_fracw_array, 1) >= 1)) THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN ((new_1.break_mode = 'none'::public.hydra_fuse_spillway_break_mode) OR (array_length(new_1.dt_fracw_array, 2) = 2)) THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM fuse_spillway_link new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN (new_1.z_invert IS NOT NULL) THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN (new_1.lateral_contraction_coef IS NOT NULL) THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.lateral_contraction_coef <= (1)::double precision) THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN (new_1.lateral_contraction_coef >= (0)::double precision) THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN (public.st_npoints(new_1.border) = 2) THEN ''::text
                    ELSE '   ST_NPoints(border)=2   '::text
                END) ||
                CASE
                    WHEN public.st_isvalid(new_1.border) THEN ''::text
                    ELSE '   ST_IsValid(border)   '::text
                END) ||
                CASE
                    WHEN (new_1.up IS NOT NULL) THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down IS NOT NULL) THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.up_type IS NOT NULL) THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.down_type IS NOT NULL) THEN ''::text
                    ELSE '   down_type is not null   '::text
                END) AS reason
           FROM mesh_2d_link new_1
          WHERE (NOT new_1.validity)
        ), singularity_invalidity_reason AS (
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN (new_1.drainage IS NOT NULL) THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN (new_1.overflow IS NOT NULL) THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_ini IS NOT NULL) THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.zr_sr_qf_qs_array IS NOT NULL) THEN ''::text
                    ELSE '   zr_sr_qf_qs_array is not null    '::text
                END) ||
                CASE
                    WHEN (new_1.treatment_mode IS NOT NULL) THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.treatment_param IS NOT NULL) THEN ''::text
                    ELSE '   treatment_param is not null   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zr_sr_qf_qs_array, 1) <= 10) THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zr_sr_qf_qs_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zr_sr_qf_qs_array, 2) = 4) THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 2)=4   '::text
                END) AS reason
           FROM reservoir_rsp_hydrology_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
                CASE
                    WHEN (new_1.q0 IS NOT NULL) THEN ''::text
                    ELSE '   q0 is not null   '::text
                END AS reason
           FROM constant_inflow_bc_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN (new_1.external_file_data OR (new_1.tz_array IS NOT NULL)) THEN ''::text
                    ELSE '   external_file_data or tz_array is not null   '::text
                END ||
                CASE
                    WHEN (new_1.external_file_data OR (array_length(new_1.tz_array, 1) <= 10)) THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (new_1.external_file_data OR (array_length(new_1.tz_array, 1) >= 1)) THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (new_1.external_file_data OR (array_length(new_1.tz_array, 2) = 2)) THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 2)=2   '::text
                END) AS reason
           FROM tz_bc_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN (new_1.zq_array IS NOT NULL) THEN ''::text
                    ELSE '   zq_array is not null   '::text
                END ||
                CASE
                    WHEN (array_length(new_1.zq_array, 1) <= 10) THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zq_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zq_array, 2) = 2) THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END) AS reason
           FROM zq_bc_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN (new_1.zs_array IS NOT NULL) THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN (new_1.zini IS NOT NULL) THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zs_array, 1) <= 10) THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zs_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zs_array, 2) = 2) THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END) AS reason
           FROM tank_bc_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((
                CASE
                    WHEN (new_1.z_invert IS NOT NULL) THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN (new_1.z_ceiling IS NOT NULL) THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width IS NOT NULL) THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width >= (0)::double precision) THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.cc IS NOT NULL) THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cc <= (1)::double precision) THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN (new_1.cc >= (0)::double precision) THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.action_gate_type IS NOT NULL) THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_invert_stop IS NOT NULL) THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_ceiling_stop IS NOT NULL) THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.v_max_cms IS NOT NULL) THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.v_max_cms >= (0)::double precision) THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.dt_regul_hr IS NOT NULL) THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (new_1.z_control_node IS NOT NULL)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (new_1.z_pid_array IS NOT NULL)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (new_1.z_tz_array IS NOT NULL)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (array_length(new_1.z_tz_array, 1) <= 10)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (array_length(new_1.z_tz_array, 1) >= 1)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'elevation'::public.hydra_gate_mode_regul) OR (array_length(new_1.z_tz_array, 2) = 2)) THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'discharge'::public.hydra_gate_mode_regul) OR (new_1.q_z_crit IS NOT NULL)) THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'discharge'::public.hydra_gate_mode_regul) OR (array_length(new_1.q_tq_array, 1) <= 10)) THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'discharge'::public.hydra_gate_mode_regul) OR (array_length(new_1.q_tq_array, 1) >= 1)) THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'discharge'::public.hydra_gate_mode_regul) OR (array_length(new_1.q_tq_array, 2) = 2)) THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN ((new_1.mode_regul <> 'no_regulation'::public.hydra_gate_mode_regul) OR (new_1.nr_z_gate IS NOT NULL)) THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END) AS reason
           FROM regul_sluice_gate_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN (new_1.pk0_km IS NOT NULL) THEN ''::text
                    ELSE '   pk0_km is not null   '::text
                END ||
                CASE
                    WHEN (new_1.dx IS NOT NULL) THEN ''::text
                    ELSE '   dx is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.dx >= (0.1)::double precision) THEN ''::text
                    ELSE '   dx>=0.1   '::text
                END) AS reason
           FROM pipe_branch_marker_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN (new_1.z_invert IS NOT NULL) THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN (new_1.z_ceiling IS NOT NULL) THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_ceiling > new_1.z_invert) THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN (new_1.width IS NOT NULL) THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cc IS NOT NULL) THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cc <= (1)::double precision) THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN (new_1.cc >= (0)::double precision) THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN (new_1.action_gate_type IS NOT NULL) THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.mode_valve IS NOT NULL) THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_gate IS NOT NULL) THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.z_gate >= new_1.z_invert) THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN (new_1.z_gate <= new_1.z_ceiling) THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN (new_1.v_max_cms IS NOT NULL) THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN (NOT check_on_branch_or_reach_endpoint(new_1.geom)) THEN ''::text
                    ELSE '   not model1.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END) AS reason
           FROM gate_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN (new_1.z_weir IS NOT NULL) THEN ''::text
                    ELSE '   z_weir is not null   '::text
                END ||
                CASE
                    WHEN (new_1.width IS NOT NULL) THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width >= (0)::double precision) THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.cc IS NOT NULL) THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cc <= (1)::double precision) THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN (new_1.cc >= ('0'::numeric)::double precision) THEN ''::text
                    ELSE '   cc>=0.   '::text
                END) AS reason
           FROM weir_bc_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN (new_1.storage_area IS NOT NULL) THEN ''::text
                    ELSE '   storage_area is not null   '::text
                END ||
                CASE
                    WHEN (new_1.storage_area >= (0)::double precision) THEN ''::text
                    ELSE '   storage_area>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.constant_dry_flow IS NOT NULL) THEN ''::text
                    ELSE '   constant_dry_flow is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.constant_dry_flow >= (0)::double precision) THEN ''::text
                    ELSE '   constant_dry_flow>=0   '::text
                END) ||
                CASE
                    WHEN ((new_1.distrib_coef IS NULL) OR ((new_1.distrib_coef >= (0)::double precision) AND (new_1.distrib_coef <= (1)::double precision))) THEN ''::text
                    ELSE '   distrib_coef is null or (distrib_coef>=0 and distrib_coef<=1)   '::text
                END) ||
                CASE
                    WHEN ((new_1.lag_time_hr IS NULL) OR (new_1.lag_time_hr >= (0)::double precision)) THEN ''::text
                    ELSE '   lag_time_hr is null or (lag_time_hr>=0)   '::text
                END) ||
                CASE
                    WHEN ((new_1.sector IS NULL) OR ((new_1.distrib_coef IS NOT NULL) AND (new_1.lag_time_hr IS NOT NULL))) THEN ''::text
                    ELSE '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   '::text
                END) ||
                CASE
                    WHEN (new_1.external_file_data OR (new_1.tq_array IS NOT NULL)) THEN ''::text
                    ELSE '   external_file_data or tq_array is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.external_file_data OR (array_length(new_1.tq_array, 1) <= 10)) THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (new_1.external_file_data OR (array_length(new_1.tq_array, 1) >= 1)) THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (new_1.external_file_data OR (array_length(new_1.tq_array, 2) = 2)) THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 2)=2   '::text
                END) AS reason
           FROM hydrograph_bc_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN (new_1.law_type IS NOT NULL) THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN (new_1.param IS NOT NULL) THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN (NOT check_on_branch_or_reach_endpoint(new_1.geom)) THEN ''::text
                    ELSE '   not model1.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END) AS reason
           FROM borda_headloss_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN (new_1.d_abutment_l IS NOT NULL) THEN ''::text
                    ELSE '   d_abutment_l is not null   '::text
                END ||
                CASE
                    WHEN (new_1.d_abutment_r IS NOT NULL) THEN ''::text
                    ELSE '   d_abutment_r is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.abutment_type IS NOT NULL) THEN ''::text
                    ELSE '   abutment_type is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.zw_array IS NOT NULL) THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN (new_1.z_ceiling IS NOT NULL) THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zw_array, 1) <= 10) THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zw_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zw_array, 2) = 2) THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN (NOT check_on_branch_or_reach_endpoint(new_1.geom)) THEN ''::text
                    ELSE '   not model1.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END) AS reason
           FROM bradley_headloss_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN (new_1.z_invert IS NOT NULL) THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN (new_1.z_regul IS NOT NULL) THEN ''::text
                    ELSE '   z_regul is not null    '::text
                END) ||
                CASE
                    WHEN (new_1.width IS NOT NULL) THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width >= (0)::double precision) THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.cc IS NOT NULL) THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.cc <= (1)::double precision) THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN (new_1.cc >= (0)::double precision) THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.mode_regul IS NOT NULL) THEN ''::text
                    ELSE '   mode_regul is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.reoxy_law IS NOT NULL) THEN ''::text
                    ELSE '   reoxy_law is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.reoxy_param IS NOT NULL) THEN ''::text
                    ELSE '   reoxy_param is not null   '::text
                END) ||
                CASE
                    WHEN (NOT check_on_branch_or_reach_endpoint(new_1.geom)) THEN ''::text
                    ELSE '   not model1.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END) AS reason
           FROM zregul_weir_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN (new_1.qz_array IS NOT NULL) THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END ||
                CASE
                    WHEN (array_length(new_1.qz_array, 1) <= 10) THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.qz_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.qz_array, 2) = 2) THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN (NOT check_on_branch_or_reach_endpoint(new_1.geom)) THEN ''::text
                    ELSE '   not model1.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END) AS reason
           FROM hydraulic_cut_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN (new_1.slope IS NOT NULL) THEN ''::text
                    ELSE '   slope is not null   '::text
                END ||
                CASE
                    WHEN (new_1.k IS NOT NULL) THEN ''::text
                    ELSE '   k is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.k > (0)::double precision) THEN ''::text
                    ELSE '   k>0   '::text
                END) ||
                CASE
                    WHEN (new_1.width IS NOT NULL) THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.width >= (0)::double precision) THEN ''::text
                    ELSE '   width>=0   '::text
                END) AS reason
           FROM strickler_bc_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN (new_1.cascade_mode IS NOT NULL) THEN ''::text
                    ELSE '   cascade_mode is not null   '::text
                END ||
                CASE
                    WHEN ((new_1.cascade_mode = 'hydrograph'::public.hydra_model_connect_mode) OR (new_1.zq_array IS NOT NULL)) THEN ''::text
                    ELSE '   cascade_mode=''hydrograph'' or zq_array is not null   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zq_array, 1) <= 100) THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=100   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zq_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zq_array, 2) = 2) THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END) AS reason
           FROM model_connect_bc_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN (new_1.q_dz_array IS NOT NULL) THEN ''::text
                    ELSE '   q_dz_array is not null   '::text
                END ||
                CASE
                    WHEN (array_length(new_1.q_dz_array, 1) <= 10) THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.q_dz_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.q_dz_array, 2) = 2) THEN ''::text
                    ELSE '   array_length(q_dz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN (NOT check_on_branch_or_reach_endpoint(new_1.geom)) THEN ''::text
                    ELSE '   not model1.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END) AS reason
           FROM param_headloss_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            ((((((
                CASE
                    WHEN (new_1.downstream IS NOT NULL) THEN ''::text
                    ELSE '   downstream is not null   '::text
                END ||
                CASE
                    WHEN (new_1.split1 IS NOT NULL) THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.downstream_param IS NOT NULL) THEN ''::text
                    ELSE '   downstream_param is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.split1_law IS NOT NULL) THEN ''::text
                    ELSE '   split1_law is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.split1_param IS NOT NULL) THEN ''::text
                    ELSE '   split1_param is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.split2 IS NULL) OR (new_1.split2_law IS NOT NULL)) THEN ''::text
                    ELSE '   split2 is null or split2_law is not null   '::text
                END) ||
                CASE
                    WHEN ((new_1.split2 IS NULL) OR (new_1.split2_param IS NOT NULL)) THEN ''::text
                    ELSE '   split2 is null or split2_param is not null   '::text
                END) AS reason
           FROM zq_split_hydrology_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN (new_1.l_road IS NOT NULL) THEN ''::text
                    ELSE '   l_road is not null   '::text
                END ||
                CASE
                    WHEN (new_1.l_road >= (0)::double precision) THEN ''::text
                    ELSE '   l_road>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.z_road IS NOT NULL) THEN ''::text
                    ELSE '   z_road is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.zw_array IS NOT NULL) THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zw_array, 1) <= 10) THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zw_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zw_array, 2) = 2) THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN (NOT check_on_branch_or_reach_endpoint(new_1.geom)) THEN ''::text
                    ELSE '   not model1.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END) AS reason
           FROM bridge_headloss_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN (new_1.qq_array IS NOT NULL) THEN ''::text
                    ELSE '   qq_array is not null   '::text
                END ||
                CASE
                    WHEN (new_1.downstream IS NOT NULL) THEN ''::text
                    ELSE '   downstream is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.split1 IS NOT NULL) THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.qq_array, 1) <= 10) THEN ''::text
                    ELSE '   array_length(qq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.qq_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(qq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (((new_1.split2 IS NOT NULL) AND (array_length(new_1.qq_array, 2) = 3)) OR ((new_1.split2 IS NULL) AND (array_length(new_1.qq_array, 2) = 2))) THEN ''::text
                    ELSE '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   '::text
                END) AS reason
           FROM qq_split_hydrology_singularity new_1
          WHERE (NOT new_1.validity)
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN (new_1.drainage IS NOT NULL) THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN (new_1.overflow IS NOT NULL) THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.q_drainage IS NOT NULL) THEN ''::text
                    ELSE '   q_drainage is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.q_drainage >= (0)::double precision) THEN ''::text
                    ELSE '   q_drainage>=0   '::text
                END) ||
                CASE
                    WHEN (new_1.z_ini IS NOT NULL) THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN (new_1.zs_array IS NOT NULL) THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END) ||
                CASE
                    WHEN (new_1.treatment_mode IS NOT NULL) THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zs_array, 1) <= 10) THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zs_array, 1) >= 1) THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN (array_length(new_1.zs_array, 2) = 2) THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END) AS reason
           FROM reservoir_rs_hydrology_singularity new_1
          WHERE (NOT new_1.validity)
        )
 SELECT concat('node:', n.id) AS id,
    n.validity,
    n.name,
    'node'::character varying AS type,
    (n.node_type)::character varying AS subtype,
    n.geom,
    r.reason
   FROM (_node n
     JOIN node_invalidity_reason r ON ((r.id = n.id)))
  WHERE (n.validity = false)
UNION
 SELECT concat('link:', l.id) AS id,
    l.validity,
    l.name,
    'link'::character varying AS type,
    (l.link_type)::character varying AS subtype,
    (public.st_force3d(public.st_centroid(l.geom)))::public.geometry(PointZ,2154) AS geom,
    r.reason
   FROM (_link l
     JOIN link_invalidity_reason r ON ((r.id = l.id)))
  WHERE (l.validity = false)
UNION
 SELECT concat('singularity:', s.id) AS id,
    s.validity,
    s.name,
    'singularity'::character varying AS type,
    (s.singularity_type)::character varying AS subtype,
    n.geom,
    r.reason
   FROM ((_singularity s
     JOIN _node n ON ((s.node = n.id)))
     JOIN singularity_invalidity_reason r ON ((r.id = s.id)))
  WHERE (s.validity = false)
UNION
 SELECT concat('profile:', p.id) AS id,
    p.validity,
    p.name,
    'profile'::character varying AS type,
    ''::character varying AS subtype,
    n.geom,
    ''::character varying AS reason
   FROM (_river_cross_section_profile p
     JOIN _node n ON ((p.id = n.id)))
  WHERE (p.validity = false);


ALTER TABLE invalid OWNER TO "thierry.lepelletier";

--
-- Name: l2d; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW l2d AS
 WITH link AS (
         SELECT l.name,
            project.crossed_border(l.geom, n.contour) AS geom
           FROM _link l,
            elem_2d_node n
          WHERE (((n.id = l.up) OR (n.id = l.down)) AND (l.link_type <> 'mesh_2d'::public.hydra_link_type) AND (l.link_type <> 'network_overflow'::public.hydra_link_type) AND public.st_intersects(l.geom, public.st_exteriorring(n.contour)))
        )
 SELECT link.name,
    public.st_x(public.st_startpoint(link.geom)) AS xb,
    public.st_y(public.st_startpoint(link.geom)) AS yb,
    public.st_x(public.st_endpoint(link.geom)) AS xa,
    public.st_y(public.st_endpoint(link.geom)) AS ya
   FROM link;


ALTER TABLE l2d OWNER TO "thierry.lepelletier";

--
-- Name: marker_singularity; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW marker_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.comment,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM _marker_singularity c,
    _singularity p,
    _node n
  WHERE ((p.id = c.id) AND (n.id = p.node));


ALTER TABLE marker_singularity OWNER TO "thierry.lepelletier";

--
-- Name: metadata; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE metadata (
    id integer DEFAULT 1 NOT NULL,
    version character varying(24) DEFAULT '1.0.0'::character varying NOT NULL,
    creation_date timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    configuration integer NOT NULL,
    CONSTRAINT metadata_id_check CHECK ((id = 1))
);


ALTER TABLE metadata OWNER TO "thierry.lepelletier";

--
-- Name: open_parametric_geometry_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE open_parametric_geometry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE open_parametric_geometry_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: open_parametric_geometry_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE open_parametric_geometry_id_seq OWNED BY open_parametric_geometry.id;


--
-- Name: open_reach; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW open_reach AS
 WITH ends AS (
         SELECT p.id,
            p.geom,
            'down'::text AS typ,
            r_1.id AS reach,
            public.st_linelocatepoint(r_1.geom, p.geom) AS alpha
           FROM river_cross_section_profile p,
            reach r_1
          WHERE ((p.type_cross_section_up = 'valley'::public.hydra_cross_section_type) AND (p.type_cross_section_down <> 'valley'::public.hydra_cross_section_type) AND public.st_dwithin(p.geom, r_1.geom, (0.1)::double precision))
        UNION
         SELECT p.id,
            p.geom,
            'up'::text AS typ,
            r_1.id AS reach,
            public.st_linelocatepoint(r_1.geom, p.geom) AS alpha
           FROM river_cross_section_profile p,
            reach r_1
          WHERE ((p.type_cross_section_up <> 'valley'::public.hydra_cross_section_type) AND (p.type_cross_section_down = 'valley'::public.hydra_cross_section_type) AND public.st_dwithin(p.geom, r_1.geom, (0.1)::double precision))
        UNION
         SELECT p.id,
            p.geom,
            'down'::text AS typ,
            r_1.id AS reach,
            public.st_linelocatepoint(r_1.geom, p.geom) AS alpha
           FROM river_cross_section_profile p,
            reach r_1
          WHERE ((p.type_cross_section_up = 'valley'::public.hydra_cross_section_type) AND public.st_dwithin(p.geom, r_1.geom, (0.1)::double precision) AND (public.st_linelocatepoint(r_1.geom, p.geom) > (0.99)::double precision))
        UNION
         SELECT p.id,
            p.geom,
            'up'::text AS typ,
            r_1.id AS reach,
            public.st_linelocatepoint(r_1.geom, p.geom) AS alpha
           FROM river_cross_section_profile p,
            reach r_1
          WHERE ((p.type_cross_section_down = 'valley'::public.hydra_cross_section_type) AND public.st_dwithin(p.geom, r_1.geom, (0.1)::double precision) AND (public.st_linelocatepoint(r_1.geom, p.geom) < (0.01)::double precision))
        ), bounds AS (
         SELECT first_value(ends.reach) OVER (PARTITION BY ends.reach ORDER BY ends.alpha) AS first_value,
            ends.typ,
            lag(ends.alpha) OVER (PARTITION BY ends.reach ORDER BY ends.alpha) AS start,
            ends.alpha AS "end",
            ends.id,
            ends.reach
           FROM ends
        )
 SELECT row_number() OVER () AS id,
    public.st_linesubstring(r.geom, b.start, b."end") AS geom,
    r.id AS reach
   FROM (bounds b
     JOIN reach r ON ((r.id = b.reach)))
  WHERE (b.typ = 'down'::text);


ALTER TABLE open_reach OWNER TO "thierry.lepelletier";

--
-- Name: reach_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE reach_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reach_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: reach_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE reach_id_seq OWNED BY reach.id;


--
-- Name: results_catchment; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW results_catchment AS
 SELECT catchment_node.id,
    catchment_node.name,
    catchment_node.geom
   FROM catchment_node;


ALTER TABLE results_catchment OWNER TO "thierry.lepelletier";

--
-- Name: results_link; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW results_link AS
 SELECT gate_link.id,
    gate_link.name,
    gate_link.geom
   FROM gate_link
UNION
 SELECT regul_gate_link.id,
    regul_gate_link.name,
    regul_gate_link.geom
   FROM regul_gate_link
UNION
 SELECT weir_link.id,
    weir_link.name,
    weir_link.geom
   FROM weir_link
UNION
 SELECT borda_headloss_link.id,
    borda_headloss_link.name,
    borda_headloss_link.geom
   FROM borda_headloss_link
UNION
 SELECT pump_link.id,
    pump_link.name,
    pump_link.geom
   FROM pump_link
UNION
 SELECT deriv_pump_link.id,
    deriv_pump_link.name,
    deriv_pump_link.geom
   FROM deriv_pump_link
UNION
 SELECT fuse_spillway_link.id,
    fuse_spillway_link.name,
    fuse_spillway_link.geom
   FROM fuse_spillway_link
UNION
 SELECT connector_link.id,
    connector_link.name,
    connector_link.geom
   FROM connector_link
UNION
 SELECT strickler_link.id,
    strickler_link.name,
    strickler_link.geom
   FROM strickler_link
UNION
 SELECT porous_link.id,
    porous_link.name,
    porous_link.geom
   FROM porous_link
UNION
 SELECT overflow_link.id,
    overflow_link.name,
    overflow_link.geom
   FROM overflow_link
UNION
 SELECT network_overflow_link.id,
    network_overflow_link.name,
    network_overflow_link.geom
   FROM network_overflow_link
UNION
 SELECT mesh_2d_link.id,
    mesh_2d_link.name,
    mesh_2d_link.geom
   FROM mesh_2d_link
UNION
 SELECT street_link.id,
    street_link.name,
    street_link.geom
   FROM street_link;


ALTER TABLE results_link OWNER TO "thierry.lepelletier";

--
-- Name: results_pipe; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW results_pipe AS
 SELECT pipe_link.id,
    pipe_link.name,
    pipe_link.geom
   FROM pipe_link
  WHERE ((pipe_link.up_type = 'manhole'::public.hydra_node_type) AND (pipe_link.down_type = 'manhole'::public.hydra_node_type));


ALTER TABLE results_pipe OWNER TO "thierry.lepelletier";

--
-- Name: results_pipe_hydrol; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW results_pipe_hydrol AS
 SELECT pipe_link.id,
    pipe_link.name,
    pipe_link.geom
   FROM pipe_link
  WHERE ((pipe_link.up_type = 'manhole_hydrology'::public.hydra_node_type) AND (pipe_link.down_type = 'manhole_hydrology'::public.hydra_node_type));


ALTER TABLE results_pipe_hydrol OWNER TO "thierry.lepelletier";

--
-- Name: results_surface; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE results_surface (
    id integer,
    name character varying(24),
    geom public.geometry
);

ALTER TABLE ONLY results_surface REPLICA IDENTITY NOTHING;


ALTER TABLE results_surface OWNER TO "thierry.lepelletier";

--
-- Name: river_cross_section_pl1d; Type: VIEW; Schema: model1; Owner: thierry.lepelletier
--

CREATE VIEW river_cross_section_pl1d AS
 SELECT g.id,
    c.name,
    c.profile,
    c.generated,
    g.geom
   FROM _river_cross_section_pl1d c,
    _node g
  WHERE (g.id = c.id);


ALTER TABLE river_cross_section_pl1d OWNER TO "thierry.lepelletier";

--
-- Name: station_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE station_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE station_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: station_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE station_id_seq OWNED BY station.id;


--
-- Name: street; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE street (
    id integer NOT NULL,
    name character varying(16) DEFAULT project.unique_name('STREET_'::character varying) NOT NULL,
    width real DEFAULT 0 NOT NULL,
    elem_length real DEFAULT 100,
    rk real DEFAULT '40'::numeric NOT NULL,
    geom public.geometry(LineStringZ,2154) NOT NULL,
    CONSTRAINT street_geom_check CHECK (public.st_isvalid(geom))
);


ALTER TABLE street OWNER TO "thierry.lepelletier";

--
-- Name: street_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE street_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE street_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: street_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE street_id_seq OWNED BY street.id;


--
-- Name: urban_flood_risk_area; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE urban_flood_risk_area (
    id integer NOT NULL,
    name character varying(16) DEFAULT project.unique_name('UFRA_'::character varying) NOT NULL,
    geom public.geometry(PolygonZ,2154)
);


ALTER TABLE urban_flood_risk_area OWNER TO "thierry.lepelletier";

--
-- Name: urban_flood_risk_area_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE urban_flood_risk_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE urban_flood_risk_area_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: urban_flood_risk_area_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE urban_flood_risk_area_id_seq OWNED BY urban_flood_risk_area.id;


--
-- Name: valley_cross_section_geometry_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE valley_cross_section_geometry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE valley_cross_section_geometry_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: valley_cross_section_geometry_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE valley_cross_section_geometry_id_seq OWNED BY valley_cross_section_geometry.id;


--
-- Name: valley_cross_section_topo_geometry; Type: TABLE; Schema: model1; Owner: thierry.lepelletier
--

CREATE TABLE valley_cross_section_topo_geometry (
    id integer NOT NULL,
    name character varying(24) DEFAULT project.unique_name('VAT_'::character varying) NOT NULL,
    xz_array real[],
    CONSTRAINT valley_cross_section_topo_geometry_xz_array_check CHECK (((array_length(xz_array, 1) <= 150) AND (array_length(xz_array, 1) >= 1) AND (array_length(xz_array, 2) = 2)))
);


ALTER TABLE valley_cross_section_topo_geometry OWNER TO "thierry.lepelletier";

--
-- Name: valley_cross_section_topo_geometry_id_seq; Type: SEQUENCE; Schema: model1; Owner: thierry.lepelletier
--

CREATE SEQUENCE valley_cross_section_topo_geometry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE valley_cross_section_topo_geometry_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: valley_cross_section_topo_geometry_id_seq; Type: SEQUENCE OWNED BY; Schema: model1; Owner: thierry.lepelletier
--

ALTER SEQUENCE valley_cross_section_topo_geometry_id_seq OWNED BY valley_cross_section_topo_geometry.id;


SET search_path = project, pg_catalog;

--
-- Name: _caquot_rainfall; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE _caquot_rainfall (
    id integer NOT NULL,
    rainfall_type public.hydra_rainfall_type NOT NULL,
    montana integer,
    CONSTRAINT _caquot_rainfall_rainfall_type_check CHECK ((rainfall_type = 'caquot'::public.hydra_rainfall_type))
);


ALTER TABLE _caquot_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: _double_triangular_rainfall; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE _double_triangular_rainfall (
    id integer NOT NULL,
    rainfall_type public.hydra_rainfall_type NOT NULL,
    montana_total integer,
    montana_peak integer,
    total_duration_mn real,
    peak_duration_mn real,
    peak_time_mn real,
    CONSTRAINT _double_triangular_rainfall_rainfall_type_check CHECK ((rainfall_type = 'double_triangular'::public.hydra_rainfall_type))
);


ALTER TABLE _double_triangular_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: _gage_rainfall; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE _gage_rainfall (
    id integer NOT NULL,
    rainfall_type public.hydra_rainfall_type NOT NULL,
    cbv_grid_connect_file character varying(256),
    interpolation public.hydra_rainfall_interpolation_type,
    CONSTRAINT _gage_rainfall_rainfall_type_check CHECK ((rainfall_type = 'gage'::public.hydra_rainfall_type))
);


ALTER TABLE _gage_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: _intensity_curve_rainfall; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE _intensity_curve_rainfall (
    id integer NOT NULL,
    rainfall_type public.hydra_rainfall_type NOT NULL,
    t_mn_intensity_mmh real[],
    CONSTRAINT _intensity_curve_rainfall_rainfall_type_check CHECK ((rainfall_type = 'intensity_curve'::public.hydra_rainfall_type))
);


ALTER TABLE _intensity_curve_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: _radar_rainfall; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE _radar_rainfall (
    id integer NOT NULL,
    rainfall_type public.hydra_rainfall_type NOT NULL,
    radar_file character varying(256) NOT NULL,
    radar_grid integer,
    CONSTRAINT _radar_rainfall_rainfall_type_check CHECK ((rainfall_type = 'radar'::public.hydra_rainfall_type))
);


ALTER TABLE _radar_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: _rainfall; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE _rainfall (
    id integer NOT NULL,
    rainfall_type public.hydra_rainfall_type NOT NULL,
    name character varying(24) NOT NULL,
    comment character varying(256),
    validity boolean
);


ALTER TABLE _rainfall OWNER TO "thierry.lepelletier";

--
-- Name: _rainfall_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE _rainfall_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE _rainfall_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: _rainfall_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE _rainfall_id_seq OWNED BY _rainfall.id;


--
-- Name: _single_triangular_rainfall; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE _single_triangular_rainfall (
    id integer NOT NULL,
    rainfall_type public.hydra_rainfall_type NOT NULL,
    montana integer,
    total_duration_mn real,
    peak_time_mn real,
    CONSTRAINT _single_triangular_rainfall_rainfall_type_check CHECK ((rainfall_type = 'single_triangular'::public.hydra_rainfall_type))
);


ALTER TABLE _single_triangular_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: bc_tz_data; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE bc_tz_data (
    id integer NOT NULL,
    filename character varying(256) NOT NULL
);


ALTER TABLE bc_tz_data OWNER TO "thierry.lepelletier";

--
-- Name: bc_tz_data_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE bc_tz_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bc_tz_data_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: bc_tz_data_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE bc_tz_data_id_seq OWNED BY bc_tz_data.id;


--
-- Name: c_affin_param; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE c_affin_param (
    id integer NOT NULL,
    scenario integer NOT NULL,
    model character varying(24) NOT NULL,
    type_domain character varying(24) NOT NULL,
    element character varying(24) NOT NULL,
    CONSTRAINT c_affin_param_type_domain_check CHECK (((type_domain)::text = ANY (ARRAY[('reach'::character varying)::text, ('domain_2d'::character varying)::text, ('branch'::character varying)::text])))
);


ALTER TABLE c_affin_param OWNER TO "thierry.lepelletier";

--
-- Name: c_affin_param_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE c_affin_param_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE c_affin_param_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: c_affin_param_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE c_affin_param_id_seq OWNED BY c_affin_param.id;


--
-- Name: caquot_rainfall; Type: VIEW; Schema: project; Owner: thierry.lepelletier
--

CREATE VIEW caquot_rainfall AS
 SELECT p.id,
    c.montana,
    p.name
   FROM _caquot_rainfall c,
    _rainfall p
  WHERE (p.id = c.id);


ALTER TABLE caquot_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: coef_montana; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE coef_montana (
    id integer NOT NULL,
    name character varying(24) DEFAULT unique_name('MONTANA_'::character varying) NOT NULL,
    comment character varying(256),
    return_period_yr real NOT NULL,
    coef_a real NOT NULL,
    coef_b real NOT NULL,
    CONSTRAINT coef_montana_coef_a_check CHECK ((coef_a > (0)::double precision)),
    CONSTRAINT coef_montana_coef_b_check CHECK ((coef_b < (0)::double precision))
);


ALTER TABLE coef_montana OWNER TO "thierry.lepelletier";

--
-- Name: coef_montana_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE coef_montana_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coef_montana_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: coef_montana_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE coef_montana_id_seq OWNED BY coef_montana.id;


--
-- Name: dem; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE dem (
    id integer NOT NULL,
    priority integer NOT NULL,
    source character varying(256) NOT NULL
);


ALTER TABLE dem OWNER TO "thierry.lepelletier";

--
-- Name: dem_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE dem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dem_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: dem_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE dem_id_seq OWNED BY dem.id;


--
-- Name: double_triangular_rainfall; Type: VIEW; Schema: project; Owner: thierry.lepelletier
--

CREATE VIEW double_triangular_rainfall AS
 SELECT p.id,
    c.montana_total,
    c.montana_peak,
    c.total_duration_mn,
    c.peak_duration_mn,
    c.peak_time_mn,
    p.name
   FROM _double_triangular_rainfall c,
    _rainfall p
  WHERE (p.id = c.id);


ALTER TABLE double_triangular_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: dry_inflow_hourly_modulation; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE dry_inflow_hourly_modulation (
    id integer NOT NULL,
    name character varying(24) DEFAULT unique_name('MODUL_'::character varying) NOT NULL,
    comment character varying(256),
    hv_array real[],
    CONSTRAINT dry_inflow_hourly_modulation_hv_array_check CHECK ((array_length(hv_array, 2) = 2))
);


ALTER TABLE dry_inflow_hourly_modulation OWNER TO "thierry.lepelletier";

--
-- Name: dry_inflow_hourly_modulation_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE dry_inflow_hourly_modulation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dry_inflow_hourly_modulation_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: dry_inflow_hourly_modulation_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE dry_inflow_hourly_modulation_id_seq OWNED BY dry_inflow_hourly_modulation.id;


--
-- Name: dry_inflow_scenario; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE dry_inflow_scenario (
    id integer NOT NULL,
    name character varying(24) DEFAULT unique_name('DRYSCN_'::character varying) NOT NULL,
    comment character varying(256)
);


ALTER TABLE dry_inflow_scenario OWNER TO "thierry.lepelletier";

--
-- Name: dry_inflow_scenario_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE dry_inflow_scenario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dry_inflow_scenario_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: dry_inflow_scenario_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE dry_inflow_scenario_id_seq OWNED BY dry_inflow_scenario.id;


--
-- Name: dry_inflow_scenario_sector_setting; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE dry_inflow_scenario_sector_setting (
    dry_inflow_scenario integer NOT NULL,
    sector integer NOT NULL,
    vol_curve integer,
    volume_sewage_m3day real DEFAULT 0 NOT NULL,
    coef_volume_sewage_m3day real DEFAULT 0 NOT NULL,
    volume_clear_water_m3day real DEFAULT 0 NOT NULL,
    coef_volume_clear_water_m3day real DEFAULT 0 NOT NULL
);


ALTER TABLE dry_inflow_scenario_sector_setting OWNER TO "thierry.lepelletier";

--
-- Name: dry_inflow_sector; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE dry_inflow_sector (
    id integer NOT NULL,
    name character varying(24) DEFAULT unique_name('SECT_'::character varying) NOT NULL,
    comment character varying(256),
    geom public.geometry(Polygon,2154) NOT NULL,
    CONSTRAINT dry_inflow_sector_geom_check CHECK (public.st_isvalid(geom))
);


ALTER TABLE dry_inflow_sector OWNER TO "thierry.lepelletier";

--
-- Name: dry_inflow_sector_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE dry_inflow_sector_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dry_inflow_sector_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: dry_inflow_sector_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE dry_inflow_sector_id_seq OWNED BY dry_inflow_sector.id;


--
-- Name: gage_rainfall; Type: VIEW; Schema: project; Owner: thierry.lepelletier
--

CREATE VIEW gage_rainfall AS
 SELECT p.id,
    c.cbv_grid_connect_file,
    c.interpolation,
    p.name
   FROM _gage_rainfall c,
    _rainfall p
  WHERE (p.id = c.id);


ALTER TABLE gage_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: gage_rainfall_data; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE gage_rainfall_data (
    rainfall integer NOT NULL,
    rain_gage integer NOT NULL,
    is_active boolean,
    t_mn_hcum_mm real[],
    CONSTRAINT gage_rainfall_data_t_mn_hcum_mm_check CHECK (((array_length(t_mn_hcum_mm, 1) <= 1000) AND (array_length(t_mn_hcum_mm, 1) >= 1) AND (array_length(t_mn_hcum_mm, 2) = 2)))
);


ALTER TABLE gage_rainfall_data OWNER TO "thierry.lepelletier";

--
-- Name: grp; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE grp (
    id integer NOT NULL,
    name character varying(256) NOT NULL
);


ALTER TABLE grp OWNER TO "thierry.lepelletier";

--
-- Name: grp_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE grp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE grp_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: grp_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE grp_id_seq OWNED BY grp.id;


--
-- Name: grp_model; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE grp_model (
    grp integer NOT NULL,
    model integer NOT NULL
);


ALTER TABLE grp_model OWNER TO "thierry.lepelletier";

--
-- Name: hydrograph_pollution; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE hydrograph_pollution (
    id integer NOT NULL,
    dry_flow_mes_mgl real NOT NULL,
    dry_flow_dbo5_mgl real NOT NULL,
    dry_flow_dco_mgl real NOT NULL,
    dry_flow_ntk_mgl real NOT NULL,
    runoff_mes_mgl real NOT NULL,
    runoff_flow_dbo5_mgl real NOT NULL,
    runoff_flow_dco_mgl real NOT NULL,
    runoff_flow_ntk_mgl real NOT NULL
);


ALTER TABLE hydrograph_pollution OWNER TO "thierry.lepelletier";

--
-- Name: hydrograph_pollution_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE hydrograph_pollution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hydrograph_pollution_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: hydrograph_pollution_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE hydrograph_pollution_id_seq OWNED BY hydrograph_pollution.id;


--
-- Name: hydrograph_quality; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE hydrograph_quality (
    id integer NOT NULL,
    dry_flow_dbo5_mgl real NOT NULL,
    dry_flow_nh4_mgl real NOT NULL,
    dry_flow_no3_mgl real NOT NULL,
    dry_flow_o2_mgl real NOT NULL,
    dry_flow_ad_mgl real NOT NULL,
    dry_flow_mes_mgl real NOT NULL,
    runoff_flow_dbo5_mgl real NOT NULL,
    runoff_flow_nh4_mgl real NOT NULL,
    runoff_flow_no3_mgl real NOT NULL,
    runoff_flow_ad_mgl real NOT NULL,
    runoff_flow_mes_mgl real NOT NULL,
    dbou_dbo5_ratio real DEFAULT 1.2 NOT NULL,
    dbo5_dcant_fraction real DEFAULT 0 NOT NULL,
    mes_decant_fraction real DEFAULT 0 NOT NULL
);


ALTER TABLE hydrograph_quality OWNER TO "thierry.lepelletier";

--
-- Name: hydrograph_quality_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE hydrograph_quality_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hydrograph_quality_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: hydrograph_quality_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE hydrograph_quality_id_seq OWNED BY hydrograph_quality.id;


--
-- Name: hydrograph_variable_dry_flow; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE hydrograph_variable_dry_flow (
    id integer NOT NULL,
    sector integer NOT NULL,
    distrib_coef real NOT NULL,
    lag_time_hr real DEFAULT 0 NOT NULL,
    dry_inflow_hourly_modulation integer NOT NULL
);


ALTER TABLE hydrograph_variable_dry_flow OWNER TO "thierry.lepelletier";

--
-- Name: hydrograph_variable_dry_flow_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE hydrograph_variable_dry_flow_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hydrograph_variable_dry_flow_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: hydrograph_variable_dry_flow_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE hydrograph_variable_dry_flow_id_seq OWNED BY hydrograph_variable_dry_flow.id;


--
-- Name: inflow_rerouting; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE inflow_rerouting (
    id integer NOT NULL,
    param_scenario integer NOT NULL,
    model character varying(24) NOT NULL,
    hydrograph_from character varying(24) NOT NULL,
    hydrograph_to character varying(256) NOT NULL,
    option public.hydra_rerouting_option NOT NULL,
    value real NOT NULL
);


ALTER TABLE inflow_rerouting OWNER TO "thierry.lepelletier";

--
-- Name: inflow_rerouting_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE inflow_rerouting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inflow_rerouting_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: inflow_rerouting_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE inflow_rerouting_id_seq OWNED BY inflow_rerouting.id;


--
-- Name: intensity_curve_rainfall; Type: VIEW; Schema: project; Owner: thierry.lepelletier
--

CREATE VIEW intensity_curve_rainfall AS
 SELECT p.id,
    c.t_mn_intensity_mmh,
    p.name
   FROM _intensity_curve_rainfall c,
    _rainfall p
  WHERE (p.id = c.id);


ALTER TABLE intensity_curve_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: land_use; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE land_use (
    id integer NOT NULL,
    geom public.geometry(Polygon,2154) NOT NULL,
    land_use public.hydra_land_use_type NOT NULL,
    CONSTRAINT land_use_geom_check CHECK (public.st_isvalid(geom))
);


ALTER TABLE land_use OWNER TO "thierry.lepelletier";

--
-- Name: land_use_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE land_use_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE land_use_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: land_use_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE land_use_id_seq OWNED BY land_use.id;


--
-- Name: mixed; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE mixed (
    ord integer NOT NULL,
    scenario integer NOT NULL,
    grp integer NOT NULL
);


ALTER TABLE mixed OWNER TO "thierry.lepelletier";

--
-- Name: model; Type: VIEW; Schema: project; Owner: thierry.lepelletier
--

CREATE VIEW model AS
 SELECT tables.table_schema AS name
   FROM information_schema.tables
  WHERE ((tables.table_name)::text = '_singularity'::text);


ALTER TABLE model OWNER TO "thierry.lepelletier";

--
-- Name: model_config; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE model_config (
    id integer NOT NULL,
    name character varying(16) NOT NULL,
    config integer DEFAULT 1 NOT NULL,
    cascade_order integer NOT NULL,
    comment character varying(256)
);


ALTER TABLE model_config OWNER TO "thierry.lepelletier";

--
-- Name: model_config_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE model_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE model_config_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: model_config_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE model_config_id_seq OWNED BY model_config.id;


--
-- Name: output_option; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE output_option (
    id integer NOT NULL,
    scenario integer NOT NULL,
    model character varying(24) NOT NULL,
    type_element character varying(48) NOT NULL,
    element character varying(24) NOT NULL
);


ALTER TABLE output_option OWNER TO "thierry.lepelletier";

--
-- Name: output_option_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE output_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE output_option_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: output_option_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE output_option_id_seq OWNED BY output_option.id;


--
-- Name: param_external_hydrograph; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE param_external_hydrograph (
    id integer NOT NULL,
    param_scenario integer NOT NULL,
    external_file character varying(256) NOT NULL
);


ALTER TABLE param_external_hydrograph OWNER TO "thierry.lepelletier";

--
-- Name: param_external_hydrograph_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE param_external_hydrograph_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param_external_hydrograph_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: param_external_hydrograph_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE param_external_hydrograph_id_seq OWNED BY param_external_hydrograph.id;


--
-- Name: param_hydrograph; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE param_hydrograph (
    id integer NOT NULL,
    param_scenario integer NOT NULL,
    model character varying(24) NOT NULL,
    hydrograph_from character varying(24) NOT NULL,
    hydrograph_to character varying(256) NOT NULL
);


ALTER TABLE param_hydrograph OWNER TO "thierry.lepelletier";

--
-- Name: param_hydrograph_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE param_hydrograph_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param_hydrograph_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: param_hydrograph_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE param_hydrograph_id_seq OWNED BY param_hydrograph.id;


--
-- Name: param_measure; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE param_measure (
    id integer NOT NULL,
    param_scenario integer NOT NULL,
    measure_file character varying(256) NOT NULL
);


ALTER TABLE param_measure OWNER TO "thierry.lepelletier";

--
-- Name: param_measure_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE param_measure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param_measure_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: param_measure_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE param_measure_id_seq OWNED BY param_measure.id;


--
-- Name: param_regulation; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE param_regulation (
    id integer NOT NULL,
    param_scenario integer NOT NULL,
    control_file character varying(256) NOT NULL
);


ALTER TABLE param_regulation OWNER TO "thierry.lepelletier";

--
-- Name: param_regulation_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE param_regulation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param_regulation_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: param_regulation_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE param_regulation_id_seq OWNED BY param_regulation.id;


--
-- Name: param_scenario_group; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE param_scenario_group (
    scenario integer NOT NULL,
    grp integer NOT NULL
);


ALTER TABLE param_scenario_group OWNER TO "thierry.lepelletier";

--
-- Name: points_xyz; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE points_xyz (
    id integer NOT NULL,
    name character varying(24) DEFAULT unique_name('P_XYZ_'::character varying) NOT NULL,
    source character varying(24) DEFAULT NULL::character varying,
    comment character varying(256) DEFAULT NULL::character varying,
    geom public.geometry(PointZ,2154) NOT NULL,
    z_ground real NOT NULL
);


ALTER TABLE points_xyz OWNER TO "thierry.lepelletier";

--
-- Name: points_xyz_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE points_xyz_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE points_xyz_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: points_xyz_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE points_xyz_id_seq OWNED BY points_xyz.id;


--
-- Name: pollution_land_accumulation; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE pollution_land_accumulation (
    id integer NOT NULL,
    sds_stockk_gha real[] NOT NULL,
    cds_stock_kgha real[] NOT NULL,
    CONSTRAINT pollution_land_accumulation_sds_stockk_gha_check CHECK (((array_length(sds_stockk_gha, 1) = 4) AND (array_length(sds_stockk_gha, 2) = 4))),
    CONSTRAINT pollution_land_accumulation_sds_stockk_gha_check1 CHECK (((array_length(sds_stockk_gha, 1) = 4) AND (array_length(sds_stockk_gha, 2) = 4)))
);


ALTER TABLE pollution_land_accumulation OWNER TO "thierry.lepelletier";

--
-- Name: pollution_land_accumulation_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE pollution_land_accumulation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pollution_land_accumulation_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: pollution_land_accumulation_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE pollution_land_accumulation_id_seq OWNED BY pollution_land_accumulation.id;


--
-- Name: radar_grid; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE radar_grid (
    id integer NOT NULL,
    name character varying(24) DEFAULT unique_name('RADGRD_'::character varying) NOT NULL,
    comment character varying(256),
    x0 real NOT NULL,
    y0 real NOT NULL,
    dx real NOT NULL,
    dy real NOT NULL,
    nx integer NOT NULL,
    ny integer NOT NULL
);


ALTER TABLE radar_grid OWNER TO "thierry.lepelletier";

--
-- Name: radar_grid_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE radar_grid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE radar_grid_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: radar_grid_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE radar_grid_id_seq OWNED BY radar_grid.id;


--
-- Name: radar_rainfall; Type: VIEW; Schema: project; Owner: thierry.lepelletier
--

CREATE VIEW radar_rainfall AS
 SELECT p.id,
    c.radar_file,
    c.radar_grid,
    p.name
   FROM _radar_rainfall c,
    _rainfall p
  WHERE (p.id = c.id);


ALTER TABLE radar_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: rain_gage; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE rain_gage (
    id integer NOT NULL,
    name character varying(24) DEFAULT unique_name('RG_'::character varying) NOT NULL,
    comment character varying(256),
    geom public.geometry(PointZ,2154) NOT NULL
);


ALTER TABLE rain_gage OWNER TO "thierry.lepelletier";

--
-- Name: rain_gage_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE rain_gage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rain_gage_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: rain_gage_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE rain_gage_id_seq OWNED BY rain_gage.id;


--
-- Name: scenario; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE scenario (
    id integer NOT NULL,
    name character varying(24) DEFAULT unique_name('SCN_'::character varying) NOT NULL,
    comment character varying(256),
    comput_mode public.hydra_computation_mode DEFAULT 'hydrology_and_hydraulics'::public.hydra_computation_mode NOT NULL,
    dry_inflow integer,
    rainfall integer,
    dt_hydrol_mn real DEFAULT 5 NOT NULL,
    soil_moisture_coef real DEFAULT 0,
    runoff_adjust_coef real DEFAULT 1,
    option_dim_hydrol_network boolean DEFAULT false NOT NULL,
    date0 timestamp without time zone DEFAULT '2000-01-01 12:00:00'::timestamp without time zone NOT NULL,
    tfin_hr real DEFAULT 12 NOT NULL,
    tini_hydrau_hr real DEFAULT 12 NOT NULL,
    dtmin_hydrau_hr real DEFAULT 0.005 NOT NULL,
    dtmax_hydrau_hr real DEFAULT 0.1 NOT NULL,
    dzmin_hydrau real DEFAULT 0.05 NOT NULL,
    dzmax_hydrau real DEFAULT 0.1 NOT NULL,
    flag_save boolean DEFAULT false NOT NULL,
    tsave_hr real DEFAULT 0,
    flag_rstart boolean DEFAULT false NOT NULL,
    flag_gfx_control boolean DEFAULT false NOT NULL,
    scenario_rstart integer,
    flag_hydrology_rstart boolean DEFAULT false NOT NULL,
    scenario_hydrology_rstart integer,
    trstart_hr real DEFAULT 0,
    graphic_control boolean DEFAULT false NOT NULL,
    model_connect_settings public.hydra_model_connection_settings DEFAULT 'cascade'::public.hydra_model_connection_settings NOT NULL,
    tbegin_output_hr real DEFAULT 0 NOT NULL,
    tend_output_hr real DEFAULT 12 NOT NULL,
    dt_output_hr real DEFAULT 0.1 NOT NULL,
    c_affin_param boolean DEFAULT false NOT NULL,
    t_affin_start_hr real DEFAULT 0 NOT NULL,
    t_affin_min_surf_ddomains real DEFAULT 0 NOT NULL,
    strickler_param boolean DEFAULT false NOT NULL,
    option_file boolean DEFAULT false NOT NULL,
    option_file_path character varying(256),
    output_option boolean DEFAULT false NOT NULL,
    transport_type public.hydra_transport_type DEFAULT 'no_transport'::public.hydra_transport_type NOT NULL,
    iflag_mes boolean DEFAULT false NOT NULL,
    iflag_dbo5 boolean DEFAULT false NOT NULL,
    iflag_dco boolean DEFAULT false NOT NULL,
    iflag_ntk boolean DEFAULT false NOT NULL,
    quality_type public.hydra_quality_type DEFAULT 'physico_chemical'::public.hydra_quality_type NOT NULL,
    diffusion boolean DEFAULT false NOT NULL,
    dispersion_coef real DEFAULT 20 NOT NULL,
    longit_diffusion_coef real DEFAULT 41 NOT NULL,
    lateral_diff_coef real DEFAULT 0.45 NOT NULL,
    water_temperature_c real DEFAULT 20 NOT NULL,
    min_o2_mgl real DEFAULT 1.5 NOT NULL,
    kdbo5_j_minus_1 real DEFAULT 0.2 NOT NULL,
    koxygen_j_minus_1 real DEFAULT 0.45 NOT NULL,
    knr_j_minus_1 real DEFAULT 0.09 NOT NULL,
    kn_j_minus_1 real DEFAULT 0.035 NOT NULL,
    bdf_mgl real DEFAULT 1.5 NOT NULL,
    o2sat_mgl real DEFAULT 9.07 NOT NULL,
    iflag_ad1 boolean DEFAULT false NOT NULL,
    iflag_ad2 boolean DEFAULT false NOT NULL,
    iflag_ad3 boolean DEFAULT false NOT NULL,
    iflag_ad4 boolean DEFAULT false NOT NULL,
    ad1_decay_rate_jminus_one real DEFAULT 0 NOT NULL,
    ad2_decay_rate_jminus_one real DEFAULT 0 NOT NULL,
    ad3_decay_rate_jminus_one real DEFAULT 0 NOT NULL,
    ad4_decay_rate_jminus_one real DEFAULT 0 NOT NULL,
    sst_mgl real DEFAULT 0 NOT NULL,
    sediment_file character varying(256) DEFAULT ''::character varying NOT NULL,
    CONSTRAINT scenario_runoff_adjust_coef_check CHECK ((runoff_adjust_coef >= (0)::double precision)),
    CONSTRAINT scenario_soil_moisture_coef_check CHECK (((soil_moisture_coef >= (0)::double precision) AND (soil_moisture_coef <= (1)::double precision)))
);


ALTER TABLE scenario OWNER TO "thierry.lepelletier";

--
-- Name: scenario_group; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE scenario_group (
    id integer NOT NULL,
    name character varying(24) NOT NULL
);


ALTER TABLE scenario_group OWNER TO "thierry.lepelletier";

--
-- Name: scenario_group_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE scenario_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE scenario_group_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: scenario_group_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE scenario_group_id_seq OWNED BY scenario_group.id;


--
-- Name: scenario_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE scenario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE scenario_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: scenario_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE scenario_id_seq OWNED BY scenario.id;


--
-- Name: single_triangular_rainfall; Type: VIEW; Schema: project; Owner: thierry.lepelletier
--

CREATE VIEW single_triangular_rainfall AS
 SELECT p.id,
    c.montana,
    c.total_duration_mn,
    c.peak_time_mn,
    p.name
   FROM _single_triangular_rainfall c,
    _rainfall p
  WHERE (p.id = c.id);


ALTER TABLE single_triangular_rainfall OWNER TO "thierry.lepelletier";

--
-- Name: strickler_param; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE strickler_param (
    id integer NOT NULL,
    scenario integer NOT NULL,
    model character varying(24) NOT NULL,
    type_domain character varying(24) NOT NULL,
    element character varying(24) NOT NULL,
    rkmin real DEFAULT 0 NOT NULL,
    rkmaj real DEFAULT 0 NOT NULL,
    pk1 real DEFAULT 0 NOT NULL,
    pk2 real DEFAULT 0 NOT NULL,
    CONSTRAINT strickler_param_type_domain_check CHECK (((type_domain)::text = ANY (ARRAY[('reach'::character varying)::text, ('domain_2d'::character varying)::text, ('branch'::character varying)::text])))
);


ALTER TABLE strickler_param OWNER TO "thierry.lepelletier";

--
-- Name: strickler_param_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE strickler_param_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE strickler_param_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: strickler_param_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE strickler_param_id_seq OWNED BY strickler_param.id;


--
-- Name: unique_name_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE unique_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE unique_name_seq OWNER TO "thierry.lepelletier";

--
-- Name: wind_gage; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE wind_gage (
    id integer NOT NULL,
    name character varying(24) DEFAULT unique_name('WG_'::character varying) NOT NULL,
    comment character varying(256),
    geom public.geometry(PointZ,2154) NOT NULL
);


ALTER TABLE wind_gage OWNER TO "thierry.lepelletier";

--
-- Name: wind_gage_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE wind_gage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wind_gage_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: wind_gage_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE wind_gage_id_seq OWNED BY wind_gage.id;


--
-- Name: wind_scenario; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE wind_scenario (
    id integer NOT NULL,
    name character varying(24) NOT NULL,
    grid_connect_file character varying(256) DEFAULT ''::character varying NOT NULL,
    interpolation public.hydra_rainfall_interpolation_type DEFAULT 'thyssen'::public.hydra_rainfall_interpolation_type NOT NULL
);


ALTER TABLE wind_scenario OWNER TO "thierry.lepelletier";

--
-- Name: wind_scenario_anemometer_data; Type: TABLE; Schema: project; Owner: thierry.lepelletier
--

CREATE TABLE wind_scenario_anemometer_data (
    wind_gage integer NOT NULL,
    wind_scenario integer NOT NULL,
    hwd_array real[],
    CONSTRAINT wind_scenario_anemometer_data_hwd_array_check CHECK ((array_length(hwd_array, 2) = 3))
);


ALTER TABLE wind_scenario_anemometer_data OWNER TO "thierry.lepelletier";

--
-- Name: wind_scenario_id_seq; Type: SEQUENCE; Schema: project; Owner: thierry.lepelletier
--

CREATE SEQUENCE wind_scenario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wind_scenario_id_seq OWNER TO "thierry.lepelletier";

--
-- Name: wind_scenario_id_seq; Type: SEQUENCE OWNED BY; Schema: project; Owner: thierry.lepelletier
--

ALTER SEQUENCE wind_scenario_id_seq OWNED BY wind_scenario.id;


SET search_path = model1, pg_catalog;

--
-- Name: _constrain id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _constrain ALTER COLUMN id SET DEFAULT nextval('_constrain_id_seq'::regclass);


--
-- Name: _link id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _link ALTER COLUMN id SET DEFAULT nextval('_link_id_seq'::regclass);


--
-- Name: _node id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _node ALTER COLUMN id SET DEFAULT nextval('_node_id_seq'::regclass);


--
-- Name: _singularity id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _singularity ALTER COLUMN id SET DEFAULT nextval('_singularity_id_seq'::regclass);


--
-- Name: bank id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY bank ALTER COLUMN id SET DEFAULT nextval('bank_id_seq'::regclass);


--
-- Name: catchment id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY catchment ALTER COLUMN id SET DEFAULT nextval('catchment_id_seq'::regclass);


--
-- Name: closed_parametric_geometry id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY closed_parametric_geometry ALTER COLUMN id SET DEFAULT nextval('closed_parametric_geometry_id_seq'::regclass);


--
-- Name: configuration id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY configuration ALTER COLUMN id SET DEFAULT nextval('configuration_id_seq'::regclass);


--
-- Name: coverage id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY coverage ALTER COLUMN id SET DEFAULT nextval('coverage_id_seq'::regclass);


--
-- Name: coverage_marker id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY coverage_marker ALTER COLUMN id SET DEFAULT nextval('coverage_marker_id_seq'::regclass);


--
-- Name: domain_2d id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY domain_2d ALTER COLUMN id SET DEFAULT nextval('domain_2d_id_seq'::regclass);


--
-- Name: domain_2d_marker id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY domain_2d_marker ALTER COLUMN id SET DEFAULT nextval('domain_2d_marker_id_seq'::regclass);


--
-- Name: generation_step id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY generation_step ALTER COLUMN id SET DEFAULT nextval('generation_step_id_seq'::regclass);


--
-- Name: open_parametric_geometry id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY open_parametric_geometry ALTER COLUMN id SET DEFAULT nextval('open_parametric_geometry_id_seq'::regclass);


--
-- Name: reach id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY reach ALTER COLUMN id SET DEFAULT nextval('reach_id_seq'::regclass);


--
-- Name: station id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY station ALTER COLUMN id SET DEFAULT nextval('station_id_seq'::regclass);


--
-- Name: street id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY street ALTER COLUMN id SET DEFAULT nextval('street_id_seq'::regclass);


--
-- Name: urban_flood_risk_area id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY urban_flood_risk_area ALTER COLUMN id SET DEFAULT nextval('urban_flood_risk_area_id_seq'::regclass);


--
-- Name: valley_cross_section_geometry id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY valley_cross_section_geometry ALTER COLUMN id SET DEFAULT nextval('valley_cross_section_geometry_id_seq'::regclass);


--
-- Name: valley_cross_section_topo_geometry id; Type: DEFAULT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY valley_cross_section_topo_geometry ALTER COLUMN id SET DEFAULT nextval('valley_cross_section_topo_geometry_id_seq'::regclass);


SET search_path = project, pg_catalog;

--
-- Name: _rainfall id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _rainfall ALTER COLUMN id SET DEFAULT nextval('_rainfall_id_seq'::regclass);


--
-- Name: bc_tz_data id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY bc_tz_data ALTER COLUMN id SET DEFAULT nextval('bc_tz_data_id_seq'::regclass);


--
-- Name: c_affin_param id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY c_affin_param ALTER COLUMN id SET DEFAULT nextval('c_affin_param_id_seq'::regclass);


--
-- Name: coef_montana id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY coef_montana ALTER COLUMN id SET DEFAULT nextval('coef_montana_id_seq'::regclass);


--
-- Name: dem id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dem ALTER COLUMN id SET DEFAULT nextval('dem_id_seq'::regclass);


--
-- Name: dem priority; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dem ALTER COLUMN priority SET DEFAULT currval('dem_id_seq'::regclass);


--
-- Name: dry_inflow_hourly_modulation id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dry_inflow_hourly_modulation ALTER COLUMN id SET DEFAULT nextval('dry_inflow_hourly_modulation_id_seq'::regclass);


--
-- Name: dry_inflow_scenario id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dry_inflow_scenario ALTER COLUMN id SET DEFAULT nextval('dry_inflow_scenario_id_seq'::regclass);


--
-- Name: dry_inflow_sector id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dry_inflow_sector ALTER COLUMN id SET DEFAULT nextval('dry_inflow_sector_id_seq'::regclass);


--
-- Name: grp id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY grp ALTER COLUMN id SET DEFAULT nextval('grp_id_seq'::regclass);


--
-- Name: grp name; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY grp ALTER COLUMN name SET DEFAULT ('GROUP_'::text || ((currval('grp_id_seq'::regclass))::character varying)::text);


--
-- Name: hydrograph_pollution id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY hydrograph_pollution ALTER COLUMN id SET DEFAULT nextval('hydrograph_pollution_id_seq'::regclass);


--
-- Name: hydrograph_quality id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY hydrograph_quality ALTER COLUMN id SET DEFAULT nextval('hydrograph_quality_id_seq'::regclass);


--
-- Name: hydrograph_variable_dry_flow id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY hydrograph_variable_dry_flow ALTER COLUMN id SET DEFAULT nextval('hydrograph_variable_dry_flow_id_seq'::regclass);


--
-- Name: inflow_rerouting id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY inflow_rerouting ALTER COLUMN id SET DEFAULT nextval('inflow_rerouting_id_seq'::regclass);


--
-- Name: land_use id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY land_use ALTER COLUMN id SET DEFAULT nextval('land_use_id_seq'::regclass);


--
-- Name: model_config id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY model_config ALTER COLUMN id SET DEFAULT nextval('model_config_id_seq'::regclass);


--
-- Name: model_config cascade_order; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY model_config ALTER COLUMN cascade_order SET DEFAULT currval('model_config_id_seq'::regclass);


--
-- Name: output_option id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY output_option ALTER COLUMN id SET DEFAULT nextval('output_option_id_seq'::regclass);


--
-- Name: param_external_hydrograph id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_external_hydrograph ALTER COLUMN id SET DEFAULT nextval('param_external_hydrograph_id_seq'::regclass);


--
-- Name: param_hydrograph id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_hydrograph ALTER COLUMN id SET DEFAULT nextval('param_hydrograph_id_seq'::regclass);


--
-- Name: param_measure id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_measure ALTER COLUMN id SET DEFAULT nextval('param_measure_id_seq'::regclass);


--
-- Name: param_regulation id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_regulation ALTER COLUMN id SET DEFAULT nextval('param_regulation_id_seq'::regclass);


--
-- Name: points_xyz id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY points_xyz ALTER COLUMN id SET DEFAULT nextval('points_xyz_id_seq'::regclass);


--
-- Name: pollution_land_accumulation id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY pollution_land_accumulation ALTER COLUMN id SET DEFAULT nextval('pollution_land_accumulation_id_seq'::regclass);


--
-- Name: radar_grid id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY radar_grid ALTER COLUMN id SET DEFAULT nextval('radar_grid_id_seq'::regclass);


--
-- Name: rain_gage id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY rain_gage ALTER COLUMN id SET DEFAULT nextval('rain_gage_id_seq'::regclass);


--
-- Name: scenario id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY scenario ALTER COLUMN id SET DEFAULT nextval('scenario_id_seq'::regclass);


--
-- Name: scenario_group id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY scenario_group ALTER COLUMN id SET DEFAULT nextval('scenario_group_id_seq'::regclass);


--
-- Name: scenario_group name; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY scenario_group ALTER COLUMN name SET DEFAULT ('GROUP_'::text || ((currval('scenario_group_id_seq'::regclass))::character varying)::text);


--
-- Name: strickler_param id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY strickler_param ALTER COLUMN id SET DEFAULT nextval('strickler_param_id_seq'::regclass);


--
-- Name: wind_gage id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY wind_gage ALTER COLUMN id SET DEFAULT nextval('wind_gage_id_seq'::regclass);


--
-- Name: wind_scenario id; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY wind_scenario ALTER COLUMN id SET DEFAULT nextval('wind_scenario_id_seq'::regclass);


--
-- Name: wind_scenario name; Type: DEFAULT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY wind_scenario ALTER COLUMN name SET DEFAULT ('WIND_'::text || ((currval('wind_scenario_id_seq'::regclass))::character varying)::text);


SET search_path = model1, pg_catalog;

--
-- Data for Name: _borda_headloss_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _borda_headloss_link (id, link_type, law_type, param) FROM stdin;
\.


--
-- Data for Name: _borda_headloss_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _borda_headloss_singularity (id, singularity_type, law_type, param) FROM stdin;
\.


--
-- Data for Name: _bradley_headloss_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _bradley_headloss_singularity (id, singularity_type, d_abutment_l, d_abutment_r, abutment_type, zw_array, z_ceiling) FROM stdin;
\.


--
-- Data for Name: _bridge_headloss_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _bridge_headloss_singularity (id, singularity_type, l_road, z_road, zw_array) FROM stdin;
8	bridge_headloss	12	554.599976	{{550.51001,1},{552.5,8},{552.599976,12},{554,28}}
9	bridge_headloss	12	544.299988	{{538.950012,1},{540.799988,6},{540.900024,8},{543.299988,28}}
\.


--
-- Data for Name: _catchment_node; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _catchment_node (id, node_type, area_ha, rl, slope, c_imp, netflow_type, netflow_param, runoff_type, runoff_param, q_limit, q0, contour, catchment_pollution_mode, catchment_pollution_param) FROM stdin;
1062	catchment	140	500	0.0799999982	0.200000003	scs	{"horner": {"recharge_coef": 0.12, "ini_loss_coef": 0.6}, "scs": {"soil_drainage_time_day": 10.0, "rfu_mm_scs": 50.0, "j_mm_scs": 150.0}, "holtan": {"soil_storage_cap_mm": 50.0, "sat_inf_rate_mmh": 2.0, "dry_inf_rate_mmh": 10.0}, "permeable_soil": {"ground_max_inf_rate": 1.0, "rfu_mm_ps": 50.0, "ground_lag_time_day": 1.0, "j_mm_ps": 150.0, "coef_river_exchange": 1.0}, "constant_runoff": {"cr": 0.2}}	giandotti	{"k_desbordes": {}, "passini": {}, "socose": {"tc_mn": null, "shape_param_beta": 2.0}, "giandotti": {}}	9999	0	3	\N	\N
1064	catchment	3800	8000	0.0500000007	0.0500000007	scs	{"horner": {"recharge_coef": 0.12, "ini_loss_coef": 0.6}, "scs": {"soil_drainage_time_day": 10.0, "rfu_mm_scs": 30.0, "j_mm_scs": 120.0}, "holtan": {"soil_storage_cap_mm": 50.0, "sat_inf_rate_mmh": 2.0, "dry_inf_rate_mmh": 10.0}, "permeable_soil": {"ground_max_inf_rate": 1.0, "rfu_mm_ps": 50.0, "ground_lag_time_day": 1.0, "j_mm_ps": 150.0, "coef_river_exchange": 1.0}, "constant_runoff": {"cr": null}}	socose	{"k_desbordes": {}, "passini": {}, "socose": {"tc_mn": 180.0, "shape_param_beta": 2.0}, "giandotti": {}}	9999	0	2	\N	\N
1063	catchment	1500	6500	0.0500000007	0.0500000007	scs	{"horner": {"recharge_coef": 0.12, "ini_loss_coef": 0.6}, "scs": {"soil_drainage_time_day": 10.0, "rfu_mm_scs": 30.0, "j_mm_scs": 120.0}, "holtan": {"soil_storage_cap_mm": 50.0, "sat_inf_rate_mmh": 2.0, "dry_inf_rate_mmh": 10.0}, "permeable_soil": {"ground_max_inf_rate": 1.0, "rfu_mm_ps": 50.0, "ground_lag_time_day": 1.0, "j_mm_ps": 150.0, "coef_river_exchange": 1.0}, "constant_runoff": {"cr": null}}	socose	{"k_desbordes": {}, "passini": {}, "socose": {"tc_mn": 120.0, "shape_param_beta": 2.0}, "giandotti": {}}	9999	0	\N	\N	\N
\.


--
-- Data for Name: _connector_hydrology_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _connector_hydrology_link (id, link_type, hydrograph) FROM stdin;
\.


--
-- Data for Name: _connector_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _connector_link (id, link_type, main_branch) FROM stdin;
1756	connector	f
1761	connector	f
1762	connector	f
\.


--
-- Data for Name: _constant_inflow_bc_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _constant_inflow_bc_singularity (id, singularity_type, q0) FROM stdin;
\.


--
-- Data for Name: _constrain; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _constrain (id, name, geom, discretized, elem_length, constrain_type, link_attributes) FROM stdin;
23	CONSTR23	01020000A06A08000002000000302F6643A41129416C5D07B881AB5841EC51B81E8595814080268F1FA0112941FEB4BFA672AB58416666666666528140	01020000A06A08000008000000302F6643A41129416C5D07B881AB5841EC51B81E85958140D095CCA9A3112941CA20FD907FAB5841D9541A72EE8B8140C0B45115A311294128E4F2697DAB5841C6577CC557828140601BB87BA211294186A7E8427BAB5841B35ADE18C1788140503A3DE7A1112941E46ADE1B79AB58419F5D406C2A6F8140F0A0A34DA1112941422ED4F476AB58418C60A2BF93658140E0BF28B9A0112941A0F1C9CD74AB584179630413FD5B814080268F1FA0112941FEB4BFA672AB58416666666666528140	10	overflow	\N
1	CONSTR1	01020000A06A08000004000000F0C7F605580E29412493114F96AB5841713D0AD7A3E48140A00B015D470E29413830AB088DAB584185EB51B81ECF814010FA52953F0E2941A41226FD89AB58418FC2F5285CD58140D0E8CDA91B0E2941E834AB8880AB58413D0AD7A3701B8240	01020000A06A08000004000000F0C7F605580E29412493114F96AB5841713D0AD7A3E48140A00B015D470E29413830AB088DAB584185EB51B81ECF814010FA52953F0E2941A41226FD89AB58418FC2F5285CD58140D0E8CDA91B0E2941E834AB8880AB58413D0AD7A3701B8240	100	flood_plain_transect	\N
6	CONSTR6	01020000A06A08000004000000C090511503122941A8DE1BC670AB584166666666664E8140005E1EE20112294188A111EF6FAB584185EB51B81E3F8140F065F5050812294160E11B8669AB584185EB51B81E3F8140B077A34D0F12294160C2BFF64EAB58417B14AE47E1828140	01020000A06A08000005000000C090511503122941A8DE1BC670AB584166666666664E8140005E1EE20112294188A111EF6FAB584185EB51B81E3F8140F065F5050812294160E11B8669AB584185EB51B81E3F8140D06ECCA90B122941E0D16D3E5CAB58410000000000618140B077A34D0F12294160C2BFF64EAB58417B14AE47E1828140	100	flood_plain_transect	\N
25	CONSTR25	01020000A06A08000003000000A0FB6F9AD112294190E5F2A979AB584133333333333F8140400DF585F4122941CE36AB787BAB58419A999999993B8140F0628E9FA913294176F5A0E185AB5841E17A14AE472B8140	01020000A06A08000003000000A0FB6F9AD112294190E5F2A979AB584133333333333F8140400DF585F4122941CE36AB787BAB58419A999999993B8140F0628E9FA913294176F5A0E185AB5841E17A14AE472B8140	100	\N	\N
2	CONSTR2	01020000A06A08000004000000F0628E9FA913294176F5A0E185AB5841E17A14AE472B8140E01C5BECDB132941264382DC75AB58419A99999999F78040B03AE057DE1329410CDE1B6672AB58411F85EB51B8FE8040100B5B6C0B142941069B3AFB65AB5841EC51B81E85598140	01020000A06A08000004000000F0628E9FA913294176F5A0E185AB5841E17A14AE472B8140E01C5BECDB132941264382DC75AB58419A99999999F78040B03AE057DE1329410CDE1B6672AB58411F85EB51B8FE8040100B5B6C0B142941069B3AFB65AB5841EC51B81E85598140	100	flood_plain_transect	\N
10	CONSTR10	01020000A06A08000004000000D05DCBA9E3142941FE9EE80292AB5841A4703D0AD7F980402091FE5CE3142941D25D07A880AB58410000000000000000E0AF8348E314294160F9A0717BAB58416666666666E88040603D463EE8142941242ED44477AB58410000000000DA8040	01020000A06A08000004000000D05DCBA9E3142941FE9EE80292AB5841A4703D0AD7F980402091FE5CE3142941D25D07A880AB58410000000000000000E0AF8348E314294160F9A0717BAB58416666666666E88040603D463EE8142941242ED44477AB58410000000000DA8040	100	flood_plain_transect	\N
3	CONSTR3	01020000A06A08000004000000E04D3C67181429418EA8E88278AB58410AD7A3703D068140901709B42014294124F2C96D73AB584185EB51B81E03814000D8FE5C261429410426FDA071AB584185EB51B81E038140B03E3CE740142941425C308469AB5841A4703D0AD7338140	01020000A06A08000004000000E04D3C67181429418EA8E88278AB58410AD7A3703D068140901709B42014294124F2C96D73AB584185EB51B81E03814000D8FE5C261429410426FDA071AB584185EB51B81E038140B03E3CE740142941425C308469AB5841A4703D0AD7338140	100	flood_plain_transect	\N
4	CONSTR4	01020000A06A08000004000000F0181F620F10294192D91B567EAB5841EC51B81E85B78140F0181F620F102941966DDEEB71AB58413333333333878140300A711A00102941D84582AC6EAB5841295C8FC2F58C8140209A3390FE0F2941AE5859C057AB5841713D0AD7A3DA8140	01020000A06A08000004000000F0181F620F10294192D91B567EAB5841EC51B81E85B78140F0181F620F102941966DDEEB71AB58413333333333878140300A711A00102941D84582AC6EAB5841295C8FC2F58C8140209A3390FE0F2941AE5859C057AB5841713D0AD7A3DA8140	100	flood_plain_transect	\N
30	CONSTR30	01020000A06A08000005000000307CCBA992142941B07607583EAB58419A999999994F814010D63110B41429412EE5441244AB584148E17A14AE3D814010D33110BC142941C65B59804FAB5841CDCCCCCCCC388140F01A138BBB14294172FBC99D5AAB5841A4703D0AD7258140B0992739B11429411A1E266D6BAB58416666666666248140	01020000A06A08000005000000307CCBA992142941B07607583EAB58419A999999994F814010D63110B41429412EE5441244AB584148E17A14AE3D814010D33110BC142941C65B59804FAB5841CDCCCCCCCC388140F01A138BBB14294172FBC99D5AAB5841A4703D0AD7258140B0992739B11429411A1E266D6BAB58416666666666248140	100	\N	\N
12	CONSTR12	01020000A06A08000004000000D05DCBA9E3142941FE9EE80292AB5841A4703D0AD7F980406098ACA406152941DE2BD4547DAB5841C3F5285C8FE28040703EF4051C1529419074B59F7AAB5841AE47E17A14D88040D0D33BE75D152941B6828C3370AB58410000000000048140	01020000A06A08000004000000D05DCBA9E3142941FE9EE80292AB5841A4703D0AD7F980406098ACA406152941DE2BD4547DAB5841C3F5285C8FE28040703EF4051C1529419074B59F7AAB5841AE47E17A14D88040D0D33BE75D152941B6828C3370AB58410000000000048140	100	flood_plain_transect	\N
9	CONSTR9	01020000A06A08000004000000104A6F1AAB14294186E3F2197FAB584185EB51B81EFD80409070CBA9B11429415EA8E80279AB58418FC2F5285CF9804070CC5A6CB2142941D24282BC76AB58418FC2F5285CF98040B0992739B11429411A1E266D6BAB58416666666666248140	01020000A06A08000004000000104A6F1AAB14294186E3F2197FAB584185EB51B81EFD80409070CBA9B11429415EA8E80279AB58418FC2F5285CF9804070CC5A6CB2142941D24282BC76AB58418FC2F5285CF98040B0992739B11429411A1E266D6BAB58416666666666248140	100	flood_plain_transect	\N
11	CONSTR11	01020000A06A08000004000000400BEAAE00152941889211EF97AB5841C3F5285C8FF88040E0ECE9AE51152941DEC16DEE86AB58416666666666DE804030DB128B6515294130ECC94D83AB58418FC2F5285CD7804080775AEC94152941C8BB96DA7BAB5841CDCCCCCCCCEC8040	01020000A06A08000004000000400BEAAE00152941889211EF97AB5841C3F5285C8FF88040E0ECE9AE51152941DEC16DEE86AB58416666666666DE804030DB128B6515294130ECC94D83AB58418FC2F5285CD7804080775AEC94152941C8BB96DA7BAB5841CDCCCCCCCCEC8040	100	flood_plain_transect	\N
14	CONSTR14	01020000A06A08000004000000307CCBA992142941B07607583EAB58419A999999994F8140404D6F9AA2142941B86359503AAB5841D7A3703D0A1D8140207BA2CDB0142941AE50AB7836AB5841D7A3703D0A1D81404079A2CDB51429418CCCBFD633AB5841A4703D0AD7518140	01020000A06A08000004000000307CCBA992142941B07607583EAB58419A999999994F8140404D6F9AA2142941B86359503AAB5841D7A3703D0A1D8140207BA2CDB0142941AE50AB7836AB5841D7A3703D0A1D81404079A2CDB51429418CCCBFD633AB5841A4703D0AD7518140	100	flood_plain_transect	\N
5	CONSTR5	01020000A06A08000004000000708D0AB43B1029419A3AAB5871AB5841B81E85EB518A8140708D0AB43B102941DEC0964A6EAB58417B14AE47E1888140604529B93C102941E632D4946AAB58417B14AE47E1888140304529393D102941AE5859C057AB5841CDCCCCCCCCC28140	01020000A06A08000004000000708D0AB43B1029419A3AAB5871AB5841B81E85EB518A8140708D0AB43B102941DEC0964A6EAB58417B14AE47E1888140604529B93C102941E632D4946AAB58417B14AE47E1888140304529393D102941AE5859C057AB5841CDCCCCCCCCC28140	100	flood_plain_transect	\N
7	CONSTR7	01020000A06A08000003000000F0E2B7FB381229413645825C70AB5841F6285C8FC24D81407034701A3A12294100848CC36CAB58416666666666448140F0FA3CE74A1229415E1478C54EAB584114AE47E17A808140	01020000A06A08000004000000F0E2B7FB381229413645825C70AB5841F6285C8FC24D81407034701A3A12294100848CC36CAB58416666666666448140B097D68042122941B437D4C45DAB58413D0AD7A370628140F0FA3CE74A1229415E1478C54EAB584114AE47E17A808140	100	flood_plain_transect	\N
8	CONSTR8	01020000A06A08000004000000400DF585F4122941CE36AB787BAB58419A999999993B814040F6CB294D1329411C40ABA862AB5841A4703D0AD71B814040F6CB294D13294130C6961A60AB5841A4703D0AD71B8140D02CD6805F132941A62FFDF057AB5841AE47E17A14788140	01020000A06A08000005000000400DF585F4122941CE36AB787BAB58419A999999993B8140C081E0D720132941F04F59106FAB58411F85EB51B82B814040F6CB294D1329411C40ABA862AB5841A4703D0AD71B814040F6CB294D13294130C6961A60AB5841A4703D0AD71B8140D02CD6805F132941A62FFDF057AB5841AE47E17A14788140	100	flood_plain_transect	\N
13	CONSTR13	01020000A06A0800000400000030C4E92EBE152941004E07D8AAAB58411F85EB51B8E08040D0E0A14D4C162941AE22D4D495AB58411F85EB51B8C28040D012D5804F1629412C94118F93AB58411F85EB51B8C280406017ACA45E16294126DDF21990AB5841713D0AD7A3088140	01020000A06A0800000500000030C4E92EBE152941004E07D8AAAB58411F85EB51B8E0804080D2453E05162941DCA3BF56A0AB58411F85EB51B8D18040D0E0A14D4C162941AE22D4D495AB58411F85EB51B8C28040D012D5804F1629412C94118F93AB58411F85EB51B8C280406017ACA45E16294126DDF21990AB5841713D0AD7A3088140	100	flood_plain_transect	\N
16	CONSTR16	01020000A06A08000004000000F01A138BBB14294172FBC99D5AAB5841A4703D0AD7258140505BF405CF1429417876DE3B5AAB5841CDCCCCCCCCFE804050E1B6FBE714294134BEBF165AAB5841CDCCCCCCCCFE8040705A50953E152941441078B559AB584133333333334F8140	01020000A06A08000004000000F01A138BBB14294172FBC99D5AAB5841A4703D0AD7258140505BF405CF1429417876DE3B5AAB5841CDCCCCCCCCFE804050E1B6FBE714294134BEBF165AAB5841CDCCCCCCCCFE8040705A50953E152941441078B559AB584133333333334F8140	100	flood_plain_transect	\N
17	CONSTR17	01020000A06A08000004000000603D463EE8142941242ED44477AB58410000000000DA8040D0B15A6CF9142941648A630777AB5841EC51B81E85D18040D005EA2E0F1529414824FD4076AB5841EC51B81E85D18040D0D33BE75D152941B6828C3370AB58410000000000048140	01020000A06A08000004000000603D463EE8142941242ED44477AB58410000000000DA8040D0B15A6CF9142941648A630777AB5841EC51B81E85D18040D005EA2E0F1529414824FD4076AB5841EC51B81E85D18040D0D33BE75D152941B6828C3370AB58410000000000048140	100	flood_plain_transect	\N
21	CONSTR21	01020000A06A0800000A000000A0FB6F9AD112294190E5F2A979AB584133333333333F8140406028B99F122941127B8C9384AB5841C3F5285C8F868140E085D600721229414CA5E83281AB5841F6285C8FC28181402014140B231229415C01787581AB58410AD7A3703D7C814010AB2839D81129416C5D07B881AB5841D7A3703D0A8F8140302F6643A41129416C5D07B881AB5841EC51B81E85958140C0B0F585401129410C35AB2880AB584114AE47E17A9C8140F0145295A21029410C35AB2880AB58416666666666B4814030E299764F102941D69B111F7FAB58410000000000B08140F0181F620F10294192D91B567EAB5841EC51B81E85B78140	01020000A06A08000018000000A0FB6F9AD112294190E5F2A979AB584133333333333F8140F0AFB7FBC01229416617264D7DAB584163C92F96FC568140F0ABE057B01229413C4959F080AB5841935F2CF9C56E8140406028B99F122941127B8C9384AB5841C3F5285C8F8681401073FFDC88122941B47B8CE382AB58415C8FC2F528848140E085D600721229414CA5E83281AB5841F6285C8FC2818140004DF5854A1229415453305481AB584100000000007F81402014140B231229415C01787581AB58410AD7A3703D7C8140C0BBADA4FD11294164AFBF9681AB5841703D0AD7A385814010AB2839D81129416C5D07B881AB5841D7A3703D0A8F8140206D473EBE1129416C5D07B881AB5841E27A14AE47928140302F6643A41129416C5D07B881AB5841EC51B81E85958140D097F505831129414CA5E83281AB5841A4703D0AD7978140204866C3611129412CEDC9AD80AB58415C8FC2F5289A8140C0B0F585401129410C35AB2880AB584114AE47E17A9C8140E077140B191129410C35AB2880AB5841285C8FC275A28140003F3390F11029410C35AB2880AB58413D0AD7A370A88140D04D3310CA1029410C35AB2880AB584152B81E856BAE8140F0145295A21029410C35AB2880AB58416666666666B4814020EC1EE286102941A84959D07FAB5841EFEEEEEEEEB28140000BCD296B1029413A8763777FAB58417777777777B1814030E299764F102941D69B111F7FAB58410000000000B08140907D5C6C2F102941B4BA96BA7EAB5841F6285C8FC2B38140F0181F620F10294192D91B567EAB5841EC51B81E85B78140	20	\N	\N
24	CONSTR24	01020000A06A08000006000000F0C7F605580E29412493114F96AB5841713D0AD7A3E48140A0A3CD29D40E29418A61DE0B92AB5841E17A14AE47ED81404040C352390F29415E4559408BAB58419A99999999F18140408929B9870F2941EC5B07B885AB5841D7A3703D0AE7814050E53DE7D90F2941F05D075880AB5841EC51B81E85CD8140F0181F620F10294192D91B567EAB5841EC51B81E85B78140	01020000A06A0800000B000000F0C7F605580E29412493114F96AB5841713D0AD7A3E4814080663E67810E2941F4748CE394AB5841EC51B81E85E78140100586C8AA0E2941BA7F637793AB58416666666666EA8140A0A3CD29D40E29418A61DE0B92AB5841E17A14AE47ED8140F07148BE060F294174D31BA68EAB58413E0AD7A370EF81404040C352390F29415E4559408BAB58419A99999999F18140C064F685600F29412065DE7B88AB5841B81E85EB51EC8140408929B9870F2941EC5B07B885AB5841D7A3703D0AE78140205BA4CDB00F2941EE5C070883AB5841E27A14AE47DA814050E53DE7D90F2941F05D075880AB5841EC51B81E85CD8140F0181F620F10294192D91B567EAB5841EC51B81E85B78140	30	\N	\N
20	CONSTR20	01020000A06A0800000B000000F0181F620F10294192D91B567EAB5841EC51B81E85B7814000F199F6271029410E114FE972AB58410AD7A3703D8E8140708D0AB43B1029419A3AAB5871AB5841B81E85EB518A8140C02729B98B1029411E6DDE2B73AB58415C8FC2F528728140809DC252EB10294188ABE89270AB58415C8FC2F5285E8140C071EBAE4411294138DF1B466FAB5841AE47E17A1454814080268F1FA0112941FEB4BFA672AB58416666666666528140C090511503122941A8DE1BC670AB584185EB51B81E478140F0E2B7FB381229413645825C70AB58415C8FC2F5284A814090537AF18A122941D4E6F24976AB5841A4703D0AD7458140A0FB6F9AD112294190E5F2A979AB584133333333333F8140	01020000A06A0800001B000000F0181F620F10294192D91B567EAB5841EC51B81E85B781401049529517102941148963877AAB5841F6285C8FC2A98140E0C066C31F1029418C6107B876AB584100000000009C814000F199F6271029410E114FE972AB58410AD7A3703D8E8140708D0AB43B1029419A3AAB5871AB5841B81E85EB518A814050FE1E6256102941185930F471AB58414444444444828140E0B6140B71102941A04E599072AB5841D069039D367A8140C02729B98B1029411E6DDE2B73AB58415C8FC2F52872814090115295AB1029419AC96D4E72AB5841B1E4174B7E6B8140B0B39976CB1029410C4F597071AB5841073A6DA0D3648140809DC252EB10294188ABE89270AB58415C8FC2F5285E8140D0A6701A09112941C659302470AB5841CDCCCCCCCC5A814070683DE726112941FA30D4B46FAB58413D0AD7A370578140C071EBAE4411294138DF1B466FAB5841AE47E17A1454814000AECC2963112941CCDE1B6670AB5841EB51B81E8553814040EAADA4811129416AB5BF8671AB5841295C8FC2F552814080268F1FA0112941FEB4BFA672AB58416666666666528140D061701AC11129413AB5BF0672AB5841703D0AD7A34E81407055701AE21129416CDE1B6671AB58417B14AE47E14A8140C090511503122941A8DE1BC670AB584185EB51B81E4781400016140B1E12294174FDA09170AB5841703D0AD7A3488140F0E2B7FB381229413645825C70AB58415C8FC2F5284A8140D05DA34D5412294112DE1B5672AB58411F85EB51B8488140B0D88E9F6F122941F84D595074AB5841E17A14AE4747814090537AF18A122941D4E6F24976AB5841A4703D0AD7458140C0838448AE12294132E6F2F977AB5841EC51B81E85428140A0FB6F9AD112294190E5F2A979AB584133333333333F8140	20	overflow	\N
26	CONSTR26	01020000A06A0800000F000000D0E8CDA91B0E2941E834AB8880AB58413D0AD7A3701B824060075DECBF0E2941EE0F4FE975AB58413D0AD7A3700B824010EA0AB4440F29413490638767AB58411F85EB51B802824070FE3DE7960F2941A82DFD405DAB584152B81E85EBFD8140209A3390FE0F2941AE5859C057AB5841713D0AD7A3DA8140304529393D102941AE5859C057AB5841CDCCCCCCCCC2814080EC1EE285102941D280B5EF59AB584185EB51B81EB38140D0C547BED1102941486C07185AAB5841F6285C8FC2A3814010AB1E6234112941F2FCC99D56AB5841CDCCCCCCCC8E8140B077A34D0F12294160C2BFF64EAB58417B14AE47E1828140F0FA3CE74A1229415E1478C54EAB584114AE47E17A808140D02CD6805F132941A62FFDF057AB5841AE47E17A14788140100B5B6C0B142941069B3AFB65AB5841EC51B81E85598140B03E3CE740142941425C308469AB5841A4703D0AD7338140B0992739B11429411A1E266D6BAB58416666666666248140	01020000A06A08000012000000D0E8CDA91B0E2941E834AB8880AB58413D0AD7A3701B824060075DECBF0E2941EE0F4FE975AB58413D0AD7A3700B824010EA0AB4440F29413490638767AB58411F85EB51B802824070FE3DE7960F2941A82DFD405DAB584152B81E85EBFD8140209A3390FE0F2941AE5859C057AB5841713D0AD7A3DA8140304529393D102941AE5859C057AB5841CDCCCCCCCCC2814080EC1EE285102941D280B5EF59AB584185EB51B81EB38140D0C547BED1102941486C07185AAB5841F6285C8FC2A3814010AB1E6234112941F2FCC99D56AB5841CDCCCCCCCC8E81406011E1D7A111294124F4F2C952AB5841A4703D0AD7888140B077A34D0F12294160C2BFF64EAB58417B14AE47E1828140F0FA3CE74A1229415E1478C54EAB584114AE47E17A808140E0930934D512294102A23A5B53AB5841E17A14AE477C8140D02CD6805F132941A62FFDF057AB5841AE47E17A14788140F09B9876B513294156E51BF65EAB5841CDCCCCCCCC688140100B5B6C0B142941069B3AFB65AB5841EC51B81E85598140B03E3CE740142941425C308469AB5841A4703D0AD7338140B0992739B11429411A1E266D6BAB58416666666666248140	100	\N	\N
27	CONSTR27	01020000A06A08000004000000E01C5BECDB132941264382DC75AB584185EB51B81E078140E04D3C67181429418EA8E88278AB58410AD7A3703D068140104A6F1AAB14294186E3F2197FAB584100000000000000002091FE5CE3142941D25D07A880AB584185EB51B81EFD8040	01020000A06A08000009000000E01C5BECDB132941264382DC75AB584185EB51B81E07814060B5CB29FA132941DA75B52F77AB584148E17A14AE068140E04D3C67181429418EA8E88278AB58410AD7A3703D06814000BB50153D1429414C37AB287AAB58418FC2F5285C897940202865C3611429410AC66DCE7BAB58410AD7A3703D067140F0DC5A6C86142941C85430747DAB58410AD7A3703D066140104A6F1AAB14294186E3F2197FAB58410000000000000000C049463EC7142941AC20FDE07FAB584185EB51B81EFD70402091FE5CE3142941D25D07A880AB584185EB51B81EFD8040	20	overflow	\N
28	CONSTR28	01020000A06A08000002000000D05DCBA9E3142941FE9EE80292AB5841AE47E17A140A8140400BEAAE00152941889211EF97AB5841F6285C8FC2078140	01020000A06A08000003000000D05DCBA9E3142941FE9EE80292AB5841AE47E17A140A81406058CB29F214294148044FF994AB584152B81E85EB088140400BEAAE00152941889211EF97AB5841F6285C8FC2078140	20	overflow	\N
29	CONSTR29	01020000A06A08000004000000F0628E9FA913294176F5A0E185AB5841E17A14AE472B8140C032130B7C142941A0E5C9CD94AB5841A4703D0AD71F8140E072A2CDC6142941A2E3C91D9AAB58410000000000000000400BEAAE00152941889211EF97AB5841F6285C8FC2078140	01020000A06A0800000D000000F0628E9FA913294176F5A0E185AB5841E17A14AE472B8140A080EAAEC7132941C8798C0388AB5841B40B884DA5298140509E46BEE51329411027D4248AAB5841869CFBEC0228814000BCA2CD0314294162ABBF468CAB5841592D6F8C60268140B0D9FEDC21142941B42FAB688EAB58412CBEE22BBE24814060F75AEC3F14294106B4968A90AB5841FF4E56CB1B2381401015B7FB5D1429414E61DEAB92AB5841D1DFC96A79218140C032130B7C142941A0E5C9CD94AB5841A4703D0AD71F8140204898F694142941489DE89296AB584186EB51B81ED57640805D1DE2AD142941FA2BAB5898AB584186EB51B81ED56640E072A2CDC6142941A2E3C91D9AAB58410000000000000000103F46BEE314294190CF1B0699AB5841F6285C8FC2077140400BEAAE00152941889211EF97AB5841F6285C8FC2078140	20	\N	\N
31	CONSTR31	01020000A06A080000080000004079A2CDB51429418CCCBFD633AB5841A4703D0AD7518140D096D580EF1429414C988CA336AB58410AD7A3703D548140A091ACA418152941904DABC83EAB58410000000000428140F043CBA9281529410C7CDE5B4BAB58410AD7A3703D4C8140606150152C1529418A1278A553AB58410AD7A3703D3A8140705A50953E152941441078B559AB584133333333334F8140F037CBA948152941CC5C301468AB5841AE47E17A140E8140D0D33BE75D152941B6828C3370AB58410000000000048140	01020000A06A080000080000004079A2CDB51429418CCCBFD633AB5841A4703D0AD7518140D096D580EF1429414C988CA336AB58410AD7A3703D548140A091ACA418152941904DABC83EAB58410000000000428140F043CBA9281529410C7CDE5B4BAB58410AD7A3703D4C8140606150152C1529418A1278A553AB58410AD7A3703D3A8140705A50953E152941441078B559AB584133333333334F8140F037CBA948152941CC5C301468AB5841AE47E17A140E8140D0D33BE75D152941B6828C3370AB58410000000000048140	100	\N	\N
32	CONSTR32	01020000A06A08000002000000B0992739B11429411A1E266D6BAB58416666666666248140603D463EE8142941242ED44477AB58410000000000DA8040	01020000A06A08000002000000B0992739B11429411A1E266D6BAB58416666666666248140603D463EE8142941242ED44477AB58410000000000DA8040	100	overflow	\N
33	CONSTR33	01020000A06A08000003000000400BEAAE00152941889211EF97AB5841C3F5285C8FF88040C03EA2CD511529412AB86DCEA0AB58416666666666F4804030C4E92EBE152941004E07D8AAAB58411F85EB51B8E08040	01020000A06A08000003000000400BEAAE00152941889211EF97AB5841C3F5285C8FF88040C03EA2CD511529412AB86DCEA0AB58416666666666F4804030C4E92EBE152941004E07D8AAAB58411F85EB51B8E08040	100	\N	\N
34	CONSTR34	01020000A06A08000004000000D0D33BE75D152941B6828C3370AB5841000000000004814080775AEC94152941C8BB96DA7BAB5841CDCCCCCCCCEC8040C0623190E715294184B7963A87AB58413D0AD7A370DF80406017ACA45E16294126DDF21990AB5841713D0AD7A3088140	01020000A06A08000004000000D0D33BE75D152941B6828C3370AB5841000000000004814080775AEC94152941C8BB96DA7BAB5841CDCCCCCCCCEC8040C0623190E715294184B7963A87AB58413D0AD7A370DF80406017ACA45E16294126DDF21990AB5841713D0AD7A3088140	100	\N	\N
35	CONSTR35	01020000A06A08000003000000603D463EE8142941242ED44477AB58410000000000DA8040D08DD580071529412AE5F2B97AAB584114AE47E17AC68040703EF4051C1529419074B59F7AAB58416666666666DA8040	01020000A06A08000003000000603D463EE8142941242ED44477AB58410000000000DA8040D08DD580071529412AE5F2B97AAB584114AE47E17AC68040703EF4051C1529419074B59F7AAB58416666666666DA8040	100	overflow	\N
\.


--
-- Name: _constrain_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('_constrain_id_seq', 37, true);


--
-- Data for Name: _crossroad_node; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _crossroad_node (id, node_type, area, z_ground, h) FROM stdin;
\.


--
-- Data for Name: _deriv_pump_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _deriv_pump_link (id, link_type, q_pump, qz_array) FROM stdin;
\.


--
-- Data for Name: _elem_2d_node; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _elem_2d_node (id, node_type, area, zb, rk, domain_2d, contour) FROM stdin;
1039	elem_2d	271.651886	544.01001	12	\N	01030000A06A08000001000000050000002091FE5CE3142941D25D07A880AB584100000080C2EB8040103E6F1ACB142941C8FE775588AB5841000000803D00814010D33110BC142941AC98118F87AB5841000000C01EFD8040901A138BBC142941BCB896FA83AB584100000000D7F980402091FE5CE3142941D25D07A880AB584100000080C2EB8040
1038	elem_2d	487.290039	542.780029	12	\N	01030000A06A08000001000000050000002091FE5CE3142941D25D07A880AB584100000080C2EB8040D05DCBA9E3142941FE9EE80292AB584100000000D7F98040307B79F1CB142941A426D4448BAB5841000000C01E018140103E6F1ACB142941C8FE775588AB5841000000803D0081402091FE5CE3142941D25D07A880AB584100000080C2EB8040
1037	elem_2d	255.655243	544.320007	12	\N	01030000A06A08000001000000050000002050463EB6142941BE63DE2B8CAB5841000000C01E098140307B79F1CB142941A426D4448BAB5841000000C01E018140D05DCBA9E3142941FE9EE80292AB584100000000D7F98040E0C55AECC31429417AE7C9DD8FAB584100000020AE0581402050463EB6142941BE63DE2B8CAB5841000000C01E098140
1036	elem_2d	660.207642	547.27002	12	\N	01030000A06A0800000100000005000000E01C5BECDB132941264382DC75AB5841000000C01E0781409082C1D2DD132941DE35ABF87DAB5841000000C0F5148140F071138BD31329419C8563C783AB5841000000C0CC1C8140F0628E9FA913294176F5A0E185AB5841000000A0702D8140E01C5BECDB132941264382DC75AB5841000000C01E078140
1035	elem_2d	331.450623	546.359985	12	\N	01030000A06A080000010000000500000060B5CB29FA132941DA75B52F77AB584100000020850781402002ADA4EC13294158EEC98D7DAB5841000000C0CC1281409082C1D2DD132941DE35ABF87DAB5841000000C0F5148140E01C5BECDB132941264382DC75AB5841000000C01E07814060B5CB29FA132941DA75B52F77AB58410000002085078140
1034	elem_2d	191.774796	547.359985	12	\N	01030000A06A080000010000000500000030FDDF5782142941EEE8C9FD8BAB5841000000A0701181405093A24D70142941BE1AFDB08FAB5841000000C0CC1E8140B088F40556142941CED31BB68DAB584100000020AE2181408082F4856614294110C06DBE8BAB584100000040E118814030FDDF5782142941EEE8C9FD8BAB5841000000A070118140
1033	elem_2d	170.584595	544.409973	12	\N	01030000A06A080000010000000500000070F43110631429415C72B57F80AB584100000040E1028140C00809344814294198903ACB81AB58410000000029068140802A8E1F401429411069DEFB7DAB5841000000A07003814070C5FEDC57142941ECE3F2097EAB5841000000C0F500814070F43110631429415C72B57F80AB584100000040E1028140
1032	elem_2d	202.23494	545.960022	12	\N	01030000A06A080000010000000500000030E25A6C78142941B2A2E82288AB5841000000E0510E81407015B7FB5C142941BADFF23989AB5841000000A04715814080D1D50053142941AA5130C485AB5841000000803D0C8140A094A2CD6C142941F0A3E8D284AB5841000000A09909814030E25A6C78142941B2A2E82288AB5841000000E0510E8140
1031	elem_2d	152.650497	544.429993	12	\N	01030000A06A0800000100000005000000A094A2CD6C142941F0A3E8D284AB5841000000A09909814000B4FE5C861429419E0B4F6981AB5841000000E0A30081409079CBA9991429417A0B4FC981AB5841000000A099FB8040B02D138B891429416C0A4F9984AB5841000000A047038140A094A2CD6C142941F0A3E8D284AB5841000000A099098140
1030	elem_2d	166.681686	545.960022	12	\N	01030000A06A08000001000000050000005094F405371429419E5B078886AB5841000000803D1081403021E057221429416A8463F786AB584100000080C2138140703A8E9F151429415ED71B3684AB584100000080C2118140D0453CE72D142941FC8563C782AB584100000080EB0981405094F405371429419E5B078886AB5841000000803D108140
1029	elem_2d	267.177002	545.320007	12	\N	01030000A06A0800000100000005000000304265431C1429419ECF44927DAB5841000000C0CC0A8140D0453CE72D142941FC8563C782AB584100000080EB098140703A8E9F151429415ED71B3684AB584100000080C2118140C09B1DE2071429415672B58F80AB584100000020850F8140304265431C1429419ECF44927DAB5841000000C0CC0A8140
1028	elem_2d	145.956497	543.26001	12	\N	01030000A06A08000001000000050000009079CBA9991429417A0B4FC981AB5841000000A099FB8040104A6F1AAB14294186E3F2197FAB5841000000C01EFD8040C049463EC7142941AC20FDE07FAB584100000020AE018140806ECB29B7142941AC3E82CC81AB5841000000C0F5F880409079CBA9991429417A0B4FC981AB5841000000A099FB8040
1027	elem_2d	145.121292	544.650024	12	\N	01030000A06A0800000100000005000000103F46BEE314294190CF1B0699AB58410000006066048140F0A6D580C4142941F417FD2097AB58410000006066088140F0E5DF57C01429415860DE3B95AB584100000000000A8140A024C152D81429410EA8BF2695AB584100000000D7038140103F46BEE314294190CF1B0699AB58410000006066048140
1026	elem_2d	218.584885	545.530029	12	\N	01030000A06A0800000100000005000000304265431C1429419ECF44927DAB5841000000C0CC0A814060E4FE5C0514294102933A5B7BAB5841000000400A0F814060B5CB29FA132941DA75B52F77AB58410000002085078140E04D3C67181429418EA8E88278AB5841000000803D068140304265431C1429419ECF44927DAB5841000000C0CC0A8140
822	elem_2d	119.203598	558.75	12	\N	01030000A06A0800000100000004000000E0B01EE2241129412E8A639777AB5841000000E051768140A091709A411129410A5830C474AB5841000000E051728140809A473E451129414C9E118F78AB584100000080C2798140E0B01EE2241129412E8A639777AB5841000000E051768140
823	elem_2d	60.5252991	554.820007	12	\N	01030000A06A080000010000000400000080268F1FA0112941FEB4BFA672AB5841000000400A55814050110AB4861129415CAAE8B273AB5841000000608F5C814040EAADA4811129416AB5BF8671AB5841000000E0A356814080268F1FA0112941FEB4BFA672AB5841000000400A558140
824	elem_2d	72.030899	561.330017	12	\N	01030000A06A08000001000000040000004080B87B95102941788863277CAB584100000060B88A814030E51E6299102941507F8C4379AB584100000080C2898140C06BE1D7B0102941440E4F597AAB5841000000C01E8B81404080B87B95102941788863277CAB584100000060B88A8140
825	elem_2d	67.8994446	561.549988	12	\N	01030000A06A08000001000000040000003005CDA97A102941F08863E77AAB584100000080148E81404080B87B95102941788863277CAB584100000060B88A8140B0A63DE7801029410AE4F2B97DAB5841000000C0CC9281403005CDA97A102941F08863E77AAB584100000080148E8140
826	elem_2d	83.5658951	564.539978	12	\N	01030000A06A080000010000000400000030E299764F102941D69B111F7FAB58410000000000B08140602C521564102941B640825C7CAB58410000002085938140000BCD296B1029413A8763777FAB5841000000C0CCB2814030E299764F102941D69B111F7FAB58410000000000B08140
827	elem_2d	95.5468445	559.289978	12	\N	01030000A06A080000010000000400000080A199F6FB1029419E61078876AB584100000060B87C814060BA473EF010294120A011AF73AB5841000000205C7981403047E157121129413225FDD073AB584100000000D777814080A199F6FB1029419E61078876AB584100000060B87C8140
828	elem_2d	164.540543	560.650024	12	\N	01030000A06A0800000100000004000000B099EB2EDA1029415823FDC078AB5841000000C01E878140C06BE1D7B0102941440E4F597AAB5841000000C01E8B81405037AE24B410294116F1C93D76AB58410000004033818140B099EB2EDA1029415823FDC078AB5841000000C01E878140
829	elem_2d	59.0165482	560.320007	12	\N	01030000A06A0800000100000004000000903D8F9F62112941844C593078AB5841000000E0A3808140A0045CEC71112941FAD044F279AB5841000000C01E83814060CC51156411294168B1BF367CAB58410000002085858140903D8F9F62112941844C593078AB5841000000E0A3808140
830	elem_2d	70.305397	559.369995	12	\N	01030000A06A080000010000000400000090115295AB1029419AC96D4E72AB584100000080146C81401032AE24C210294150B4BF7674AB584100000000007A81405037AE24B410294116F1C93D76AB5841000000403381814090115295AB1029419AC96D4E72AB584100000080146C8140
831	elem_2d	36.4786987	557.320007	12	\N	01030000A06A0800000100000004000000F0A0A34DA1112941422ED4F476AB5841000000E07A668140D0E4AD2490112941EABD962A76AB5841000000A0707B8140E0BF28B9A0112941A0F1C9CD74AB5841000000A0995D8140F0A0A34DA1112941422ED4F476AB5841000000E07A668140
832	elem_2d	115.734993	563.76001	12	\N	01030000A06A0800000100000004000000D04D3310CA1029410C35AB2880AB5841000000E051B0814080F97A71D010294144BB963A7DAB5841000000A070958140003F3390F11029410C35AB2880AB5841000000803DA88140D04D3310CA1029410C35AB2880AB5841000000E051B08140
833	elem_2d	43.6002464	560.960022	12	\N	01030000A06A080000010000000400000030E51E6299102941507F8C4379AB584100000080C28981404033005D8810294154C76D5E78AB5841000000A04789814070C69976991029415A2ED4B476AB5841000000C0CC84814030E51E6299102941507F8C4379AB584100000080C2898140
834	elem_2d	63.2626457	557.640015	12	\N	01030000A06A0800000100000004000000A091709A411129410A5830C474AB5841000000E0517281404077C25251112941606DDE7B72AB5841000000205C638140F068EB2E5C1129419EC86DEE74AB5841000000C0F57C8140A091709A411129410A5830C474AB5841000000E051728140
835	elem_2d	83.6207962	554.960022	12	\N	01030000A06A080000010000000400000000AECC2963112941CCDE1B6670AB5841000000C0F55481404077C25251112941606DDE7B72AB5841000000205C638140C071EBAE4411294138DF1B466FAB5841000000801454814000AECC2963112941CCDE1B6670AB5841000000C0F5548140
836	elem_2d	80.1175461	556.469971	12	\N	01030000A06A0800000100000004000000C071EBAE4411294138DF1B466FAB58410000008014548140803CE1D72E1129413663074872AB5841000000608F6C814070683DE726112941FA30D4B46FAB5841000000A070618140C071EBAE4411294138DF1B466FAB58410000008014548140
837	elem_2d	18.8737488	559.909973	12	\N	01030000A06A0800000100000004000000302F6643A41129416C5D07B881AB58410000002085958140E06599F69A1129419E72B5CF7FAB584100000020AE898140D095CCA9A3112941CA20FD907FAB584100000020AE7B8140302F6643A41129416C5D07B881AB58410000002085958140
838	elem_2d	55.7874489	565.200012	12	\N	01030000A06A0800000100000004000000000BCD296B1029413A8763777FAB5841000000C0CCB28140B0A63DE7801029410AE4F2B97DAB5841000000C0CC92814020EC1EE286102941A84959D07FAB584100000020AEAF8140000BCD296B1029413A8763777FAB5841000000C0CCB28140
839	elem_2d	59.0139465	561.450012	12	\N	01030000A06A0800000100000004000000708D0AB43B1029419A3AAB5871AB5841000000E0518A8140106F85C83A1029417439AB6874AB5841000000A0478F814000F199F6271029410E114FE972AB5841000000C0CC8E8140708D0AB43B1029419A3AAB5871AB5841000000E0518A8140
840	elem_2d	43.9322968	562.179993	12	\N	01030000A06A0800000100000004000000507B5C6C351029411E4C594079AB5841000000A09993814060B966C3331029415E0578C576AB5841000000608F908140109EB8FB451029412AB3BF8677AB5841000000A0478F8140507B5C6C351029411E4C594079AB5841000000A099938140
841	elem_2d	131.555191	561.960022	12	\N	01030000A06A0800000100000005000000E0C066C31F1029418C6107B876AB584100000040E190814000F199F6271029410E114FE972AB5841000000C0CC8E8140106F85C83A1029417439AB6874AB5841000000A0478F814060B966C3331029415E0578C576AB5841000000608F908140E0C066C31F1029418C6107B876AB584100000040E1908140
842	elem_2d	142.915451	561.909973	12	\N	01030000A06A0800000100000005000000109EB8FB451029412AB3BF8677AB5841000000A0478F8140007833905910294172DB1B5679AB584100000020858D814060061FE240102941C45F07787BAB58410000004033958140507B5C6C351029411E4C594079AB5841000000A099938140109EB8FB451029412AB3BF8677AB5841000000A0478F8140
843	elem_2d	168.139343	560.710022	12	\N	01030000A06A0800000100000005000000C06BE1D7B0102941440E4F597AAB5841000000C01E8B814030E51E6299102941507F8C4379AB584100000080C289814070C69976991029415A2ED4B476AB5841000000C0CC8481405037AE24B410294116F1C93D76AB58410000004033818140C06BE1D7B0102941440E4F597AAB5841000000C01E8B8140
844	elem_2d	261.581726	560.940002	12	\N	01030000A06A0800000100000005000000D051148B7E112941864082DC7CAB58410000008014888140D097F505831129414CA5E83281AB58410000000029968140204866C3611129412CEDC9AD80AB5841000000C0F59A814060CC51156411294168B1BF367CAB58410000002085858140D051148B7E112941864082DC7CAB58410000008014888140
845	elem_2d	241.433594	559.640015	12	\N	01030000A06A0800000100000005000000809A473E451129414C9E118F78AB584100000080C2798140903D8F9F62112941844C593078AB5841000000E0A380814060CC51156411294168B1BF367CAB5841000000208585814030B0F50542112941788863277CAB5841000000E0A37C8140809A473E451129414C9E118F78AB584100000080C2798140
846	elem_2d	154.339691	555.799988	12	\N	01030000A06A080000010000000500000000AECC2963112941CCDE1B6670AB5841000000C0F554814020B3A3CD70112941788B632774AB584100000060B86C8140F068EB2E5C1129419EC86DEE74AB5841000000C0F57C81404077C25251112941606DDE7B72AB5841000000205C63814000AECC2963112941CCDE1B6670AB5841000000C0F5548140
847	elem_2d	341.068878	563.140015	12	\N	01030000A06A0800000100000005000000E077140B191129410C35AB2880AB5841000000A070AB8140003F3390F11029410C35AB2880AB5841000000803DA88140504EE157FF1029410ABC962A7BAB5841000000400A8F8140D00EAE24201129411274B5EF7BAB5841000000400A8D8140E077140B191129410C35AB2880AB5841000000A070AB8140
848	elem_2d	149.053192	559.710022	12	\N	01030000A06A0800000100000005000000E067C2527A112941DAF0C9DD76AB584100000020857F8140903D8F9F62112941844C593078AB5841000000E0A3808140F068EB2E5C1129419EC86DEE74AB5841000000C0F57C814020B3A3CD70112941788B632774AB584100000060B86C8140E067C2527A112941DAF0C9DD76AB584100000020857F8140
849	elem_2d	220.336945	562.559998	12	\N	01030000A06A080000010000000500000060E3F585B91029417669DEEB7CAB5841000000E07A968140F0145295A21029410C35AB2880AB5841000000A099B581404080B87B95102941788863277CAB584100000060B88A8140C06BE1D7B0102941440E4F597AAB5841000000C01E8B814060E3F585B91029417669DEEB7CAB5841000000E07A968140
850	elem_2d	166.981644	560.330017	12	\N	01030000A06A080000010000000500000000A766C364102941D2B3BFC675AB5841000000400A8D814090E299764E102941706CDEFB74AB5841000000C0F590814050FE1E6256102941185930F471AB5841000000E051828140E0B6140B71102941A04E599072AB5841000000403375814000A766C364102941D2B3BFC675AB5841000000400A8D8140
851	elem_2d	286.123291	560.869995	12	\N	01030000A06A0800000100000005000000C0B0F585401129410C35AB2880AB5841000000C01EA1814030B0F50542112941788863277CAB5841000000E0A37C814060CC51156411294168B1BF367CAB58410000002085858140204866C3611129412CEDC9AD80AB5841000000C0F59A8140C0B0F585401129410C35AB2880AB5841000000C01EA18140
852	elem_2d	152.753296	562.169983	12	\N	01030000A06A080000010000000500000060B966C3331029415E0578C576AB5841000000608F908140507B5C6C351029411E4C594079AB5841000000A0999381401049529517102941148963877AAB58410000000000948140E0C066C31F1029418C6107B876AB584100000040E190814060B966C3331029415E0578C576AB5841000000608F908140
853	elem_2d	129.756241	561.789978	12	\N	01030000A06A0800000100000005000000602C521564102941B640825C7CAB58410000002085938140007833905910294172DB1B5679AB584100000020858D8140F0F41E626F102941D2E5F2F978AB5841000000E0A38C81403005CDA97A102941F08863E77AAB584100000080148E8140602C521564102941B640825C7CAB58410000002085938140
854	elem_2d	137.791992	561.22998	12	\N	01030000A06A08000001000000050000004080B87B95102941788863277CAB584100000060B88A81403005CDA97A102941F08863E77AAB584100000080148E81404033005D8810294154C76D5E78AB5841000000A04789814030E51E6299102941507F8C4379AB584100000080C28981404080B87B95102941788863277CAB584100000060B88A8140
855	elem_2d	124.184196	558.51001	12	\N	01030000A06A080000010000000500000050110AB4861129415CAAE8B273AB5841000000608F5C8140D0E4AD2490112941EABD962A76AB5841000000A0707B8140E067C2527A112941DAF0C9DD76AB584100000020857F814020B3A3CD70112941788B632774AB584100000060B86C814050110AB4861129415CAAE8B273AB5841000000608F5C8140
856	elem_2d	217.570633	560.73999	12	\N	01030000A06A0800000100000005000000B099EB2EDA1029415823FDC078AB5841000000C01E87814080A199F6FB1029419E61078876AB584100000060B87C81407092C2D208112941E689635778AB584100000020AE818140504EE157FF1029410ABC962A7BAB5841000000400A8F8140B099EB2EDA1029415823FDC078AB5841000000C01E878140
857	elem_2d	51.9994965	560.950012	12	\N	01030000A06A08000001000000050000001055EB2E91112941FCC46D9E7EAB5841000000608F8A8140C0B45115A311294128E4F2697DAB58410000000000728140D095CCA9A3112941CA20FD907FAB584100000020AE7B8140E06599F69A1129419E72B5CF7FAB584100000020AE8981401055EB2E91112941FCC46D9E7EAB5841000000608F8A8140
858	elem_2d	153.545639	564.48999	12	\N	01030000A06A080000010000000500000060061FE240102941C45F07787BAB5841000000403395814030E299764F102941D69B111F7FAB58410000000000B08140907D5C6C2F102941B4BA96BA7EAB584100000040E1B2814010B18F9F2E1029413C0378757CAB5841000000608FA0814060061FE240102941C45F07787BAB58410000004033958140
859	elem_2d	302.71994	562.26001	12	\N	01030000A06A0800000100000005000000D00EAE24201129411274B5EF7BAB5841000000400A8D814030B0F50542112941788863277CAB5841000000E0A37C8140C0B0F585401129410C35AB2880AB5841000000C01EA18140E077140B191129410C35AB2880AB5841000000A070AB8140D00EAE24201129411274B5EF7BAB5841000000400A8D8140
860	elem_2d	233.476395	559.51001	12	\N	01030000A06A080000010000000500000070C69976991029415A2ED4B476AB5841000000C0CC848140C02729B98B1029411E6DDE2B73AB5841000000205C75814090115295AB1029419AC96D4E72AB584100000080146C81405037AE24B410294116F1C93D76AB5841000000403381814070C69976991029415A2ED4B476AB5841000000C0CC848140
861	elem_2d	146.45079	560.820007	12	\N	01030000A06A080000010000000500000050FE1E6256102941185930F471AB5841000000E05182814090E299764E102941706CDEFB74AB5841000000C0F5908140106F85C83A1029417439AB6874AB5841000000A0478F8140708D0AB43B1029419A3AAB5871AB5841000000E0518A814050FE1E6256102941185930F471AB5841000000E051828140
862	elem_2d	212.64624	562.47998	12	\N	01030000A06A080000010000000500000060061FE240102941C45F07787BAB58410000004033958140007833905910294172DB1B5679AB584100000020858D8140602C521564102941B640825C7CAB5841000000208593814030E299764F102941D69B111F7FAB58410000000000B0814060061FE240102941C45F07787BAB58410000004033958140
863	elem_2d	71.1140442	560.01001	12	\N	01030000A06A0800000100000005000000B0403DE79011294124BD963A78AB5841000000205C7F8140503A3DE7A1112941E46ADE1B79AB5841000000C0F56C8140E054EBAE91112941929D117F7AAB5841000000E05182814020CB28B9821129412C7F8CA379AB58410000006066828140B0403DE79011294124BD963A78AB5841000000205C7F8140
864	elem_2d	191.622589	559.809998	12	\N	01030000A06A080000010000000500000080CE1EE2D51029419EBE964A74AB584100000080147C8140B099EB2EDA1029415823FDC078AB5841000000C01E8781405037AE24B410294116F1C93D76AB584100000040338181401032AE24C210294150B4BF7674AB584100000000007A814080CE1EE2D51029419EBE964A74AB584100000080147C8140
865	elem_2d	218.325287	560.73999	12	\N	01030000A06A0800000100000005000000D00EAE24201129411274B5EF7BAB5841000000400A8D8140504EE157FF1029410ABC962A7BAB5841000000400A8F81407092C2D208112941E689635778AB584100000020AE818140E0B01EE2241129412E8A639777AB5841000000E051768140D00EAE24201129411274B5EF7BAB5841000000400A8D8140
866	elem_2d	70.8565979	558.47998	12	\N	01030000A06A0800000100000005000000F0A0A34DA1112941422ED4F476AB5841000000E07A668140503A3DE7A1112941E46ADE1B79AB5841000000C0F56C8140B0403DE79011294124BD963A78AB5841000000205C7F8140D0E4AD2490112941EABD962A76AB5841000000A0707B8140F0A0A34DA1112941422ED4F476AB5841000000E07A668140
867	elem_2d	198.292191	559.47998	12	\N	01030000A06A0800000100000005000000809A473E451129414C9E118F78AB584100000080C2798140A091709A411129410A5830C474AB5841000000E051728140F068EB2E5C1129419EC86DEE74AB5841000000C0F57C8140903D8F9F62112941844C593078AB5841000000E0A3808140809A473E451129414C9E118F78AB584100000080C2798140
868	elem_2d	184.862442	557.809998	12	\N	01030000A06A0800000100000005000000809DC252EB10294188ABE89270AB584100000000295E814060BA473EF010294120A011AF73AB5841000000205C79814080CE1EE2D51029419EBE964A74AB584100000080147C8140B0B39976CB1029410C4F597071AB5841000000C0CC668140809DC252EB10294188ABE89270AB584100000000295E8140
869	elem_2d	225.753784	561.659973	12	\N	01030000A06A0800000100000005000000B099EB2EDA1029415823FDC078AB5841000000C01E87814080F97A71D010294144BB963A7DAB5841000000A07095814060E3F585B91029417669DEEB7CAB5841000000E07A968140C06BE1D7B0102941440E4F597AAB5841000000C01E8B8140B099EB2EDA1029415823FDC078AB5841000000C01E878140
870	elem_2d	168.397949	561.070007	12	\N	01030000A06A0800000100000005000000A0D299F678102941C64282DC76AB5841000000608F8A8140C02729B98B1029411E6DDE2B73AB5841000000205C75814070C69976991029415A2ED4B476AB5841000000C0CC8481404033005D8810294154C76D5E78AB5841000000A047898140A0D299F678102941C64282DC76AB5841000000608F8A8140
871	elem_2d	111.047997	560.119995	12	\N	01030000A06A0800000100000005000000E067C2527A112941DAF0C9DD76AB584100000020857F814020CB28B9821129412C7F8CA379AB58410000006066828140A0045CEC71112941FAD044F279AB5841000000C01E838140903D8F9F62112941844C593078AB5841000000E0A3808140E067C2527A112941DAF0C9DD76AB584100000020857F8140
872	elem_2d	103.257248	562.059998	12	\N	01030000A06A080000010000000500000060B966C3331029415E0578C576AB5841000000608F908140106F85C83A1029417439AB6874AB5841000000A0478F814090E299764E102941706CDEFB74AB5841000000C0F5908140109EB8FB451029412AB3BF8677AB5841000000A0478F814060B966C3331029415E0578C576AB5841000000608F908140
873	elem_2d	153.394943	561.75	12	\N	01030000A06A0800000100000005000000109EB8FB451029412AB3BF8677AB5841000000A0478F814090E299764E102941706CDEFB74AB5841000000C0F590814000A766C364102941D2B3BFC675AB5841000000400A8D8140007833905910294172DB1B5679AB584100000020858D8140109EB8FB451029412AB3BF8677AB5841000000A0478F8140
874	elem_2d	109.118195	555.469971	12	\N	01030000A06A080000010000000500000050110AB4861129415CAAE8B273AB5841000000608F5C814080268F1FA0112941FEB4BFA672AB5841000000400A558140E0BF28B9A0112941A0F1C9CD74AB5841000000A0995D8140D0E4AD2490112941EABD962A76AB5841000000A0707B814050110AB4861129415CAAE8B273AB5841000000608F5C8140
875	elem_2d	66.3668976	559.51001	12	\N	01030000A06A0800000100000005000000E054EBAE91112941929D117F7AAB5841000000E051828140503A3DE7A1112941E46ADE1B79AB5841000000C0F56C8140601BB87BA211294186A7E8427BAB584100000060B872814040E4ADA4911129410CF9A0517CAB5841000000C0F5848140E054EBAE91112941929D117F7AAB5841000000E051828140
876	elem_2d	126.764496	561.630005	12	\N	01030000A06A080000010000000500000000A766C364102941D2B3BFC675AB5841000000400A8D8140A0D299F678102941C64282DC76AB5841000000608F8A8140F0F41E626F102941D2E5F2F978AB5841000000E0A38C8140007833905910294172DB1B5679AB584100000020858D814000A766C364102941D2B3BFC675AB5841000000400A8D8140
877	elem_2d	260.412598	558.570007	12	\N	01030000A06A0800000100000005000000A091709A411129410A5830C474AB5841000000E051728140E0B01EE2241129412E8A639777AB5841000000E0517681403047E157121129413225FDD073AB584100000000D7778140803CE1D72E1129413663074872AB5841000000608F6C8140A091709A411129410A5830C474AB5841000000E051728140
878	elem_2d	195.254196	559.619995	12	\N	01030000A06A08000001000000050000007092C2D208112941E689635778AB584100000020AE81814080A199F6FB1029419E61078876AB584100000060B87C81403047E157121129413225FDD073AB584100000000D7778140E0B01EE2241129412E8A639777AB5841000000E0517681407092C2D208112941E689635778AB584100000020AE818140
879	elem_2d	267.689087	559.840027	12	\N	01030000A06A0800000100000005000000E0B01EE2241129412E8A639777AB5841000000E051768140809A473E451129414C9E118F78AB584100000080C279814030B0F50542112941788863277CAB5841000000E0A37C8140D00EAE24201129411274B5EF7BAB5841000000400A8D8140E0B01EE2241129412E8A639777AB5841000000E051768140
880	elem_2d	199.109787	557.039978	12	\N	01030000A06A08000001000000050000003047E157121129413225FDD073AB584100000000D7778140D0A6701A09112941C659302470AB584100000060B860814070683DE726112941FA30D4B46FAB5841000000A070618140803CE1D72E1129413663074872AB5841000000608F6C81403047E157121129413225FDD073AB584100000000D7778140
881	elem_2d	190.005096	556.880005	12	\N	01030000A06A0800000100000005000000803CE1D72E1129413663074872AB5841000000608F6C8140C071EBAE4411294138DF1B466FAB584100000080145481404077C25251112941606DDE7B72AB5841000000205C638140A091709A411129410A5830C474AB5841000000E051728140803CE1D72E1129413663074872AB5841000000608F6C8140
882	elem_2d	188.062195	559.789978	12	\N	01030000A06A0800000100000005000000E0B6140B71102941A04E599072AB58410000004033758140C02729B98B1029411E6DDE2B73AB5841000000205C758140A0D299F678102941C64282DC76AB5841000000608F8A814000A766C364102941D2B3BFC675AB5841000000400A8D8140E0B6140B71102941A04E599072AB58410000004033758140
883	elem_2d	152.648743	562.619995	12	\N	01030000A06A0800000100000005000000B0A63DE7801029410AE4F2B97DAB5841000000C0CC928140000BCD296B1029413A8763777FAB5841000000C0CCB28140602C521564102941B640825C7CAB584100000020859381403005CDA97A102941F08863E77AAB584100000080148E8140B0A63DE7801029410AE4F2B97DAB5841000000C0CC928140
884	elem_2d	86.5809937	560.590027	12	\N	01030000A06A0800000100000005000000E054EBAE91112941929D117F7AAB5841000000E05182814040E4ADA4911129410CF9A0517CAB5841000000C0F5848140D051148B7E112941864082DC7CAB5841000000801488814020CB28B9821129412C7F8CA379AB58410000006066828140E054EBAE91112941929D117F7AAB5841000000E051828140
885	elem_2d	102.014397	561.400024	12	\N	01030000A06A0800000100000005000000F0F41E626F102941D2E5F2F978AB5841000000E0A38C8140A0D299F678102941C64282DC76AB5841000000608F8A81404033005D8810294154C76D5E78AB5841000000A0478981403005CDA97A102941F08863E77AAB584100000080148E8140F0F41E626F102941D2E5F2F978AB5841000000E0A38C8140
886	elem_2d	96.6937943	559.97998	12	\N	01030000A06A0800000100000005000000B0403DE79011294124BD963A78AB5841000000205C7F814020CB28B9821129412C7F8CA379AB58410000006066828140E067C2527A112941DAF0C9DD76AB584100000020857F8140D0E4AD2490112941EABD962A76AB5841000000A0707B8140B0403DE79011294124BD963A78AB5841000000205C7F8140
887	elem_2d	175.96759	564.890015	12	\N	01030000A06A0800000100000005000000F0181F620F10294192D91B567EAB58410000002085B781401049529517102941148963877AAB5841000000000094814010B18F9F2E1029413C0378757CAB5841000000608FA08140907D5C6C2F102941B4BA96BA7EAB584100000040E1B28140F0181F620F10294192D91B567EAB58410000002085B78140
888	elem_2d	158.783295	563.090027	12	\N	01030000A06A08000001000000050000004080B87B95102941788863277CAB584100000060B88A8140F0145295A21029410C35AB2880AB5841000000A099B5814020EC1EE286102941A84959D07FAB584100000020AEAF8140B0A63DE7801029410AE4F2B97DAB5841000000C0CC9281404080B87B95102941788863277CAB584100000060B88A8140
889	elem_2d	196.933945	563.590027	12	\N	01030000A06A0800000100000005000000D04D3310CA1029410C35AB2880AB5841000000E051B08140F0145295A21029410C35AB2880AB5841000000A099B5814060E3F585B91029417669DEEB7CAB5841000000E07A96814080F97A71D010294144BB963A7DAB5841000000A070958140D04D3310CA1029410C35AB2880AB5841000000E051B08140
890	elem_2d	77.4982986	560.47998	12	\N	01030000A06A0800000100000005000000C0B45115A311294128E4F2697DAB584100000000007281401055EB2E91112941FCC46D9E7EAB5841000000608F8A814040E4ADA4911129410CF9A0517CAB5841000000C0F5848140601BB87BA211294186A7E8427BAB584100000060B8728140C0B45115A311294128E4F2697DAB58410000000000728140
891	elem_2d	138.86174	562.73999	12	\N	01030000A06A080000010000000500000010B18F9F2E1029413C0378757CAB5841000000608FA081401049529517102941148963877AAB58410000000000948140507B5C6C351029411E4C594079AB5841000000A09993814060061FE240102941C45F07787BAB5841000000403395814010B18F9F2E1029413C0378757CAB5841000000608FA08140
892	elem_2d	242.479996	559.460022	12	\N	01030000A06A080000010000000500000080A199F6FB1029419E61078876AB584100000060B87C8140B099EB2EDA1029415823FDC078AB5841000000C01E87814080CE1EE2D51029419EBE964A74AB584100000080147C814060BA473EF010294120A011AF73AB5841000000205C79814080A199F6FB1029419E61078876AB584100000060B87C8140
893	elem_2d	395.515747	562.359985	12	\N	01030000A06A080000010000000500000080F97A71D010294144BB963A7DAB5841000000A070958140B099EB2EDA1029415823FDC078AB5841000000C01E878140504EE157FF1029410ABC962A7BAB5841000000400A8F8140003F3390F11029410C35AB2880AB5841000000803DA8814080F97A71D010294144BB963A7DAB5841000000A070958140
894	elem_2d	148.953339	555.880005	12	\N	01030000A06A080000010000000500000020B3A3CD70112941788B632774AB584100000060B86C814000AECC2963112941CCDE1B6670AB5841000000C0F554814040EAADA4811129416AB5BF8671AB5841000000E0A356814050110AB4861129415CAAE8B273AB5841000000608F5C814020B3A3CD70112941788B632774AB584100000060B86C8140
895	elem_2d	218.836884	557.549988	12	\N	01030000A06A0800000100000005000000D0A6701A09112941C659302470AB584100000060B86081403047E157121129413225FDD073AB584100000000D777814060BA473EF010294120A011AF73AB5841000000205C798140809DC252EB10294188ABE89270AB584100000000295E8140D0A6701A09112941C659302470AB584100000060B8608140
896	elem_2d	100.687492	562.140015	12	\N	01030000A06A0800000100000005000000D097F505831129414CA5E83281AB584100000000299681401055EB2E91112941FCC46D9E7EAB5841000000608F8A8140E06599F69A1129419E72B5CF7FAB584100000020AE898140302F6643A41129416C5D07B881AB58410000002085958140D097F505831129414CA5E83281AB58410000000029968140
897	elem_2d	121.812096	560.650024	12	\N	01030000A06A0800000100000005000000D051148B7E112941864082DC7CAB5841000000801488814060CC51156411294168B1BF367CAB58410000002085858140A0045CEC71112941FAD044F279AB5841000000C01E83814020CB28B9821129412C7F8CA379AB58410000006066828140D051148B7E112941864082DC7CAB58410000008014888140
898	elem_2d	116.657845	561.590027	12	\N	01030000A06A08000001000000050000001055EB2E91112941FCC46D9E7EAB5841000000608F8A8140D097F505831129414CA5E83281AB58410000000029968140D051148B7E112941864082DC7CAB5841000000801488814040E4ADA4911129410CF9A0517CAB5841000000C0F58481401055EB2E91112941FCC46D9E7EAB5841000000608F8A8140
899	elem_2d	146.457687	558.359985	12	\N	01030000A06A08000001000000050000001032AE24C210294150B4BF7674AB584100000000007A814090115295AB1029419AC96D4E72AB584100000080146C8140B0B39976CB1029410C4F597071AB5841000000C0CC66814080CE1EE2D51029419EBE964A74AB584100000080147C81401032AE24C210294150B4BF7674AB584100000000007A8140
1025	elem_2d	159.348495	545.359985	12	\N	01030000A06A0800000100000005000000103F46BEE314294190CF1B0699AB58410000006066048140E072A2CDC6142941A2E3C91D9AAB584100000060B8108140805D1DE2AD142941FA2BAB5898AB58410000004033138140F0A6D580C4142941F417FD2097AB58410000006066088140103F46BEE314294190CF1B0699AB58410000006066048140
1024	elem_2d	214.79834	548.27002	12	\N	01030000A06A080000010000000500000060448E1FFB132941F646590087AB584100000020851B8140509E46BEE51329411027D4248AAB584100000020AE318140A080EAAEC7132941C8798C0388AB5841000000C0CC2E814020653C67DA1329414E1EFD3086AB5841000000002920814060448E1FFB132941F646590087AB584100000020851B8140
1023	elem_2d	303.871033	548.030029	12	\N	01030000A06A080000010000000500000090B179F13A1429412EB696CA8AAB5841000000C0CC1A8140B0D9FEDC21142941B42FAB688EAB584100000060B830814000BCA2CD0314294162ABBF468CAB5841000000205C2D814090FC83C81614294142CB443289AB5841000000A0991B814090B179F13A1429412EB696CA8AAB5841000000C0CC1A8140
1022	elem_2d	287.626251	544.669983	12	\N	01030000A06A0800000100000005000000304265431C1429419ECF44927DAB5841000000C0CC0A8140802A8E1F401429411069DEFB7DAB5841000000A070038140C00809344814294198903ACB81AB58410000000029068140D0453CE72D142941FC8563C782AB584100000080EB098140304265431C1429419ECF44927DAB5841000000C0CC0A8140
1021	elem_2d	138.39975	544.590027	12	\N	01030000A06A0800000100000005000000A00865C3B51429412C768CA391AB5841000000A0990B8140E0C55AECC31429417AE7C9DD8FAB584100000020AE058140D05DCBA9E3142941FE9EE80292AB584100000000D7F98040C05AF485D01429413061DEFB92AB584100000080C2038140A00865C3B51429412C768CA391AB5841000000A0990B8140
1020	elem_2d	246.730789	548.630005	12	\N	01030000A06A080000010000000500000060F75AEC3F14294106B4968A90AB5841000000608F328140B088F40556142941CED31BB68DAB584100000020AE2181405093A24D70142941BE1AFDB08FAB5841000000C0CC1E81401015B7FB5D1429414E61DEAB92AB5841000000803D2E814060F75AEC3F14294106B4968A90AB5841000000608F328140
1019	elem_2d	268.692627	546.789978	12	\N	01030000A06A08000001000000050000005093A24D70142941BE1AFDB08FAB5841000000C0CC1E814030FDDF5782142941EEE8C9FD8BAB5841000000A07011814080631DE29D142941CEE7C9FD8EAB584100000080EB0F8140E0C6ACA48A1429410CC844C291AB5841000000A0471581405093A24D70142941BE1AFDB08FAB5841000000C0CC1E8140
1018	elem_2d	157.432587	543.179993	12	\N	01030000A06A0800000100000005000000901A138BBC142941BCB896FA83AB584100000000D7F98040C0C68348A61429419E47594085AB5841000000E051FC80409079CBA9991429417A0B4FC981AB5841000000A099FB8040806ECB29B7142941AC3E82CC81AB5841000000C0F5F88040901A138BBC142941BCB896FA83AB584100000000D7F98040
1017	elem_2d	239.036987	548.179993	12	\N	01030000A06A0800000100000005000000C032130B7C142941A0E5C9CD94AB584100000000D71F81401015B7FB5D1429414E61DEAB92AB5841000000803D2E81405093A24D70142941BE1AFDB08FAB5841000000C0CC1E8140E0C6ACA48A1429410CC844C291AB5841000000A047158140C032130B7C142941A0E5C9CD94AB584100000000D71F8140
1016	elem_2d	118.359192	544.090027	12	\N	01030000A06A0800000100000005000000401165C39E142941C664DE6B89AB5841000000C0F5048140F07CCBA990142941F0C16DBE86AB58410000004033058140C0C68348A61429419E47594085AB5841000000E051FC804070E308B4AB1429419413267D87AB584100000080EBFD8040401165C39E142941C664DE6B89AB5841000000C0F5048140
1015	elem_2d	168.995239	544.080017	12	\N	01030000A06A08000001000000050000002091FE5CE3142941D25D07A880AB584100000080C2EB8040901A138BBC142941BCB896FA83AB584100000000D7F98040806ECB29B7142941AC3E82CC81AB5841000000C0F5F88040C049463EC7142941AC20FDE07FAB584100000020AE0181402091FE5CE3142941D25D07A880AB584100000080C2EB8040
1014	elem_2d	208.403687	545.469971	12	\N	01030000A06A0800000100000005000000E0C55AECC31429417AE7C9DD8FAB584100000020AE058140A00865C3B51429412C768CA391AB5841000000A0990B814080631DE29D142941CEE7C9FD8EAB584100000080EB0F81402050463EB6142941BE63DE2B8CAB5841000000C01E098140E0C55AECC31429417AE7C9DD8FAB584100000020AE058140
1013	elem_2d	184.269699	546.429993	12	\N	01030000A06A080000010000000500000030FDDF5782142941EEE8C9FD8BAB5841000000A0701181408082F4856614294110C06DBE8BAB584100000040E11881407015B7FB5C142941BADFF23989AB5841000000A04715814030E25A6C78142941B2A2E82288AB5841000000E0510E814030FDDF5782142941EEE8C9FD8BAB5841000000A070118140
1012	elem_2d	128.669846	546.52002	12	\N	01030000A06A0800000100000005000000805D1DE2AD142941FA2BAB5898AB58410000004033138140204898F694142941489DE89296AB5841000000A0701D814060E70834A1142941F8BC6DFE93AB5841000000E07A1081405071CBA9AF142941EE7E639795AB584100000040E10A8140805D1DE2AD142941FA2BAB5898AB58410000004033138140
1011	elem_2d	93.2467422	545.26001	12	\N	01030000A06A0800000100000005000000805D1DE2AD142941FA2BAB5898AB584100000040331381405071CBA9AF142941EE7E639795AB584100000040E10A8140F0E5DF57C01429415860DE3B95AB584100000000000A8140F0A6D580C4142941F417FD2097AB58410000006066088140805D1DE2AD142941FA2BAB5898AB58410000004033138140
1010	elem_2d	134.592239	545.039978	12	\N	01030000A06A0800000100000005000000A00865C3B51429412C768CA391AB5841000000A0990B8140C05AF485D01429413061DEFB92AB584100000080C2038140A024C152D81429410EA8BF2695AB584100000000D7038140F0E5DF57C01429415860DE3B95AB584100000000000A8140A00865C3B51429412C768CA391AB5841000000A0990B8140
1009	elem_2d	203.620438	543.539978	12	\N	01030000A06A0800000100000005000000104A6F1AAB14294186E3F2197FAB5841000000C01EFD80409079CBA9991429417A0B4FC981AB5841000000A099FB804000B4FE5C861429419E0B4F6981AB5841000000E0A3008140F0DC5A6C86142941C85430747DAB58410000000029FE8040104A6F1AAB14294186E3F2197FAB5841000000C01EFD8040
1008	elem_2d	195.552292	546.890015	12	\N	01030000A06A080000010000000500000090FC83C81614294142CB443289AB5841000000A0991B814060448E1FFB132941F646590087AB584100000020851B8140703A8E9F151429415ED71B3684AB584100000080C21181403021E057221429416A8463F786AB584100000080C213814090FC83C81614294142CB443289AB5841000000A0991B8140
1007	elem_2d	138.330093	544.320007	12	\N	01030000A06A08000001000000050000002050463EB6142941BE63DE2B8CAB5841000000C01E09814010D33110BC142941AC98118F87AB5841000000C01EFD8040103E6F1ACB142941C8FE775588AB5841000000803D008140307B79F1CB142941A426D4448BAB5841000000C01E0181402050463EB6142941BE63DE2B8CAB5841000000C01E098140
1006	elem_2d	239.156982	547.429993	12	\N	01030000A06A0800000100000005000000F071138BD31329419C8563C783AB5841000000C0CC1C814060AEF485F11329414E5D070882AB5841000000E05116814060448E1FFB132941F646590087AB584100000020851B814020653C67DA1329414E1EFD3086AB58410000000029208140F071138BD31329419C8563C783AB5841000000C0CC1C8140
1005	elem_2d	159.725143	545.179993	12	\N	01030000A06A0800000100000005000000F07CCBA990142941F0C16DBE86AB5841000000403305814030E25A6C78142941B2A2E82288AB5841000000E0510E8140A094A2CD6C142941F0A3E8D284AB5841000000A099098140B02D138B891429416C0A4F9984AB5841000000A047038140F07CCBA990142941F0C16DBE86AB58410000004033058140
1004	elem_2d	241.452286	544.859985	12	\N	01030000A06A080000010000000500000070F43110631429415C72B57F80AB584100000040E1028140A094A2CD6C142941F0A3E8D284AB5841000000A09909814080D1D50053142941AA5130C485AB5841000000803D0C8140C00809344814294198903ACB81AB5841000000002906814070F43110631429415C72B57F80AB584100000040E1028140
1003	elem_2d	255.590683	546.330017	12	\N	01030000A06A080000010000000500000090B179F13A1429412EB696CA8AAB5841000000C0CC1A81405094F405371429419E5B078886AB5841000000803D10814080D1D50053142941AA5130C485AB5841000000803D0C81407015B7FB5C142941BADFF23989AB5841000000A04715814090B179F13A1429412EB696CA8AAB5841000000C0CC1A8140
1002	elem_2d	283.539246	544.140015	12	\N	01030000A06A0800000100000005000000F0DC5A6C86142941C85430747DAB58410000000029FE804000B4FE5C861429419E0B4F6981AB5841000000E0A3008140A094A2CD6C142941F0A3E8D284AB5841000000A09909814070F43110631429415C72B57F80AB584100000040E1028140F0DC5A6C86142941C85430747DAB58410000000029FE8040
1001	elem_2d	156.827988	544.030029	12	\N	01030000A06A08000001000000050000002050463EB6142941BE63DE2B8CAB5841000000C01E098140401165C39E142941C664DE6B89AB5841000000C0F504814070E308B4AB1429419413267D87AB584100000080EBFD804010D33110BC142941AC98118F87AB5841000000C01EFD80402050463EB6142941BE63DE2B8CAB5841000000C01E098140
1000	elem_2d	289.241394	545.679993	12	\N	01030000A06A08000001000000050000002050463EB6142941BE63DE2B8CAB5841000000C01E09814080631DE29D142941CEE7C9FD8EAB584100000080EB0F814030FDDF5782142941EEE8C9FD8BAB5841000000A070118140401165C39E142941C664DE6B89AB5841000000C0F50481402050463EB6142941BE63DE2B8CAB5841000000C01E098140
999	elem_2d	188.876999	546.830017	12	\N	01030000A06A080000010000000500000090B179F13A1429412EB696CA8AAB5841000000C0CC1A814090FC83C81614294142CB443289AB5841000000A0991B81403021E057221429416A8463F786AB584100000080C21381405094F405371429419E5B078886AB5841000000803D10814090B179F13A1429412EB696CA8AAB5841000000C0CC1A8140
998	elem_2d	188.836639	544.099976	12	\N	01030000A06A0800000100000005000000802A8E1F401429411069DEFB7DAB5841000000A07003814000BB50153D1429414C37AB287AAB5841000000C0CC068140202865C3611429410AC66DCE7BAB584100000080EB03814070C5FEDC57142941ECE3F2097EAB5841000000C0F5008140802A8E1F401429411069DEFB7DAB5841000000A070038140
997	elem_2d	187.284592	547.200012	12	\N	01030000A06A0800000100000005000000E0C6ACA48A1429410CC844C291AB5841000000A04715814060E70834A1142941F8BC6DFE93AB5841000000E07A108140204898F694142941489DE89296AB5841000000A0701D8140C032130B7C142941A0E5C9CD94AB584100000000D71F8140E0C6ACA48A1429410CC844C291AB5841000000A047158140
996	elem_2d	168.787643	546.090027	12	\N	01030000A06A080000010000000500000060B5CB29FA132941DA75B52F77AB5841000000208507814060E4FE5C0514294102933A5B7BAB5841000000400A0F8140502EE057FF13294158EEC98D7DAB584100000000291081402002ADA4EC13294158EEC98D7DAB5841000000C0CC12814060B5CB29FA132941DA75B52F77AB58410000002085078140
995	elem_2d	216.106186	546.099976	12	\N	01030000A06A0800000100000005000000A00865C3B51429412C768CA391AB5841000000A0990B814060E70834A1142941F8BC6DFE93AB5841000000E07A108140E0C6ACA48A1429410CC844C291AB5841000000A04715814080631DE29D142941CEE7C9FD8EAB584100000080EB0F8140A00865C3B51429412C768CA391AB5841000000A0990B8140
994	elem_2d	148.351044	543.840027	12	\N	01030000A06A0800000100000005000000F07CCBA990142941F0C16DBE86AB58410000004033058140B02D138B891429416C0A4F9984AB5841000000A0470381409079CBA9991429417A0B4FC981AB5841000000A099FB8040C0C68348A61429419E47594085AB5841000000E051FC8040F07CCBA990142941F0C16DBE86AB58410000004033058140
993	elem_2d	130.644852	545.659973	12	\N	01030000A06A0800000100000005000000F0E5DF57C01429415860DE3B95AB584100000000000A81405071CBA9AF142941EE7E639795AB584100000040E10A814060E70834A1142941F8BC6DFE93AB5841000000E07A108140A00865C3B51429412C768CA391AB5841000000A0990B8140F0E5DF57C01429415860DE3B95AB584100000000000A8140
992	elem_2d	128.196198	543.960022	12	\N	01030000A06A08000001000000050000006058CB29F214294148044FF994AB5841000000C0F5FA8040A024C152D81429410EA8BF2695AB584100000000D7038140C05AF485D01429413061DEFB92AB584100000080C2038140D05DCBA9E3142941FE9EE80292AB584100000000D7F980406058CB29F214294148044FF994AB5841000000C0F5FA8040
991	elem_2d	219.590347	545.22998	12	\N	01030000A06A0800000100000005000000F07CCBA990142941F0C16DBE86AB58410000004033058140401165C39E142941C664DE6B89AB5841000000C0F504814030FDDF5782142941EEE8C9FD8BAB5841000000A07011814030E25A6C78142941B2A2E82288AB5841000000E0510E8140F07CCBA990142941F0C16DBE86AB58410000004033058140
990	elem_2d	260.300781	546.570007	12	\N	01030000A06A080000010000000500000060AEF485F11329414E5D070882AB5841000000E051168140C09B1DE2071429415672B58F80AB584100000020850F8140703A8E9F151429415ED71B3684AB584100000080C211814060448E1FFB132941F646590087AB584100000020851B814060AEF485F11329414E5D070882AB5841000000E051168140
989	elem_2d	203.581635	543.940002	12	\N	01030000A06A0800000100000005000000400BEAAE00152941889211EF97AB584100000020AEF98040103F46BEE314294190CF1B0699AB58410000006066048140A024C152D81429410EA8BF2695AB584100000000D70381406058CB29F214294148044FF994AB5841000000C0F5FA8040400BEAAE00152941889211EF97AB584100000020AEF98040
988	elem_2d	227.086487	545.299988	12	\N	01030000A06A08000001000000050000005094F405371429419E5B078886AB5841000000803D108140D0453CE72D142941FC8563C782AB584100000080EB098140C00809344814294198903ACB81AB5841000000002906814080D1D50053142941AA5130C485AB5841000000803D0C81405094F405371429419E5B078886AB5841000000803D108140
987	elem_2d	150.484497	545.789978	12	\N	01030000A06A0800000100000005000000C09B1DE2071429415672B58F80AB584100000020850F8140502EE057FF13294158EEC98D7DAB5841000000002910814060E4FE5C0514294102933A5B7BAB5841000000400A0F8140304265431C1429419ECF44927DAB5841000000C0CC0A8140C09B1DE2071429415672B58F80AB584100000020850F8140
986	elem_2d	115.306091	543.440002	12	\N	01030000A06A0800000100000005000000901A138BBC142941BCB896FA83AB584100000000D7F9804010D33110BC142941AC98118F87AB5841000000C01EFD804070E308B4AB1429419413267D87AB584100000080EBFD8040C0C68348A61429419E47594085AB5841000000E051FC8040901A138BBC142941BCB896FA83AB584100000000D7F98040
985	elem_2d	219.243286	543.919983	12	\N	01030000A06A080000010000000500000070F43110631429415C72B57F80AB584100000040E102814070C5FEDC57142941ECE3F2097EAB5841000000C0F5008140202865C3611429410AC66DCE7BAB584100000080EB038140F0DC5A6C86142941C85430747DAB58410000000029FE804070F43110631429415C72B57F80AB584100000040E1028140
984	elem_2d	202.085693	547.400024	12	\N	01030000A06A0800000100000005000000B088F40556142941CED31BB68DAB584100000020AE21814090B179F13A1429412EB696CA8AAB5841000000C0CC1A81407015B7FB5C142941BADFF23989AB5841000000A0471581408082F4856614294110C06DBE8BAB584100000040E1188140B088F40556142941CED31BB68DAB584100000020AE218140
983	elem_2d	303.393524	548.390015	12	\N	01030000A06A0800000100000005000000B0D9FEDC21142941B42FAB688EAB584100000060B830814090B179F13A1429412EB696CA8AAB5841000000C0CC1A8140B088F40556142941CED31BB68DAB584100000020AE21814060F75AEC3F14294106B4968A90AB5841000000608F328140B0D9FEDC21142941B42FAB688EAB584100000060B8308140
982	elem_2d	210.259888	548.349976	12	\N	01030000A06A080000010000000500000020653C67DA1329414E1EFD3086AB58410000000029208140A080EAAEC7132941C8798C0388AB5841000000C0CC2E8140F0628E9FA913294176F5A0E185AB5841000000A0702D8140F071138BD31329419C8563C783AB5841000000C0CC1C814020653C67DA1329414E1EFD3086AB58410000000029208140
981	elem_2d	266.910278	548.059998	12	\N	01030000A06A080000010000000500000090FC83C81614294142CB443289AB5841000000A0991B814000BCA2CD0314294162ABBF468CAB5841000000205C2D8140509E46BEE51329411027D4248AAB584100000020AE31814060448E1FFB132941F646590087AB584100000020851B814090FC83C81614294142CB443289AB5841000000A0991B8140
980	elem_2d	315.151947	544.830017	12	\N	01030000A06A0800000100000005000000802A8E1F401429411069DEFB7DAB5841000000A070038140304265431C1429419ECF44927DAB5841000000C0CC0A8140E04D3C67181429418EA8E88278AB5841000000803D06814000BB50153D1429414C37AB287AAB5841000000C0CC068140802A8E1F401429411069DEFB7DAB5841000000A070038140
979	elem_2d	163.530746	546.280029	12	\N	01030000A06A0800000100000005000000502EE057FF13294158EEC98D7DAB58410000000029108140C09B1DE2071429415672B58F80AB584100000020850F814060AEF485F11329414E5D070882AB5841000000E0511681402002ADA4EC13294158EEC98D7DAB5841000000C0CC128140502EE057FF13294158EEC98D7DAB58410000000029108140
978	elem_2d	224.538483	546.789978	12	\N	01030000A06A08000001000000050000009082C1D2DD132941DE35ABF87DAB5841000000C0F51481402002ADA4EC13294158EEC98D7DAB5841000000C0CC12814060AEF485F11329414E5D070882AB5841000000E051168140F071138BD31329419C8563C783AB5841000000C0CC1C81409082C1D2DD132941DE35ABF87DAB5841000000C0F5148140
916	elem_2d	237.834137	556.880005	12	\N	01030000A06A0800000100000005000000F0D184C8DD112941447F8C6379AB58410000008014648140C0BBADA4FD1129414242823C78AB5841000000E07A648140E0E1093405122941FA69DE8B7BAB584100000080EB6B8140C08F7A71EA1129411C5F07387DAB5841000000C0CC748140F0D184C8DD112941447F8C6379AB58410000008014648140
917	elem_2d	248.162582	556.570007	12	\N	01030000A06A08000001000000050000004065CC29251229413AEEC9DD7DAB5841000000403365814060F365C343122941F40C4FD97DAB5841000000E0A3648140004DF5854A1229415453305481AB584100000080147881402014140B231229415C01787581AB5841000000803D7C81404065CC29251229413AEEC9DD7DAB58410000004033658140
918	elem_2d	228.548584	556.440002	12	\N	01030000A06A0800000100000005000000204199F6FC1129411A2FD4B474AB5841000000400A638140C0BBADA4FD1129414242823C78AB5841000000E07A648140F0D184C8DD112941447F8C6379AB5841000000801464814020937A71E1112941E657302475AB584100000080EB638140204199F6FC1129411A2FD4B474AB5841000000400A638140
919	elem_2d	166.781143	556.609985	12	\N	01030000A06A0800000100000005000000E0E1093405122941FA69DE8B7BAB584100000080EB6B8140C0BBADA4FD1129414242823C78AB5841000000E07A6481405006664311122941EAD1447277AB584100000060B864814030E1E0572212294164B2BF9679AB5841000000E0A3648140E0E1093405122941FA69DE8B7BAB584100000080EB6B8140
920	elem_2d	248.509796	556.52002	12	\N	01030000A06A080000010000000500000070B35B6C4A122941FA9E11BF76AB5841000000205C5B8140802699F643122941E47E8C637AAB5841000000205C63814030E1E0572212294164B2BF9679AB5841000000E0A364814080DCE0D72E1229416A76B5AF75AB5841000000205C5F814070B35B6C4A122941FA9E11BF76AB5841000000205C5B8140
921	elem_2d	219.202545	554.200012	12	\N	01030000A06A080000010000000500000020937A71E1112941E657302475AB584100000080EB638140B0178F9FC71129417A4D59A075AB58410000008014648140D061701AC11129413AB5BF0672AB584100000000295681407055701AE21129416CDE1B6671AB5841000000C0F550814020937A71E1112941E657302475AB584100000080EB638140
922	elem_2d	98.4461975	556.099976	12	\N	01030000A06A080000010000000500000020741EE2C6112941CAE4F2B97BAB584100000080145C8140D0DC84C8C011294164BC963A7AAB584100000040335B814080F90934C61129410EE6F25978AB5841000000803D628140F0D184C8DD112941447F8C6379AB5841000000801464814020741EE2C6112941CAE4F2B97BAB584100000080145C8140
923	elem_2d	184.425598	553.799988	12	\N	01030000A06A080000010000000500000040DAE0D734122941D28B633773AB5841000000803D4A814080DCE0D72E1229416A76B5AF75AB5841000000205C5F814040FA8E1F1612294122DD1BD674AB5841000000A0996581400016140B1E12294174FDA09170AB584100000080EB51814040DAE0D734122941D28B633773AB5841000000803D4A8140
924	elem_2d	222.409348	557.799988	12	\N	01030000A06A0800000100000005000000F0D184C8DD112941447F8C6379AB58410000008014648140C08F7A71EA1129411C5F07387DAB5841000000C0CC748140A05E709AC9112941C268DECB7EAB5841000000C0CC6E814020741EE2C6112941CAE4F2B97BAB584100000080145C8140F0D184C8DD112941447F8C6379AB58410000008014648140
925	elem_2d	258.404541	552.840027	12	\N	01030000A06A0800000100000005000000D05DA34D5412294112DE1B5672AB5841000000803D3E8140B0D88E9F6F122941F84D595074AB5841000000E0A3468140C094ADA465122941E4DB1B2678AB584100000000295E814070B35B6C4A122941FA9E11BF76AB5841000000205C5B8140D05DA34D5412294112DE1B5672AB5841000000803D3E8140
926	elem_2d	204.526794	553.039978	12	\N	01030000A06A080000010000000500000080DCE0D72E1229416A76B5AF75AB5841000000205C5F814040DAE0D734122941D28B633773AB5841000000803D4A8140D05DA34D5412294112DE1B5672AB5841000000803D3E814070B35B6C4A122941FA9E11BF76AB5841000000205C5B814080DCE0D72E1229416A76B5AF75AB5841000000205C5F8140
927	elem_2d	173.6241	556.400024	12	\N	01030000A06A0800000100000005000000F0D184C8DD112941447F8C6379AB5841000000801464814080F90934C61129410EE6F25978AB5841000000803D628140B0178F9FC71129417A4D59A075AB5841000000801464814020937A71E1112941E657302475AB584100000080EB638140F0D184C8DD112941447F8C6379AB58410000008014648140
928	elem_2d	244.852234	552.51001	12	\N	01030000A06A0800000100000005000000F0AFB7FBC01229416617264D7DAB5841000000C01E498140805C28B9A9122941B6B1BF667BAB584100000080C2478140C0838448AE12294132E6F2F977AB584100000080EB438140A0FB6F9AD112294190E5F2A979AB5841000000E051408140F0AFB7FBC01229416617264D7DAB5841000000C01E498140
929	elem_2d	285.059326	559.869995	12	\N	01030000A06A0800000100000005000000C0BBADA4FD11294164AFBF9681AB5841000000606680814010AB2839D81129416C5D07B881AB5841000000400A8F8140A05E709AC9112941C268DECB7EAB5841000000C0CC6E8140C08F7A71EA1129411C5F07387DAB5841000000C0CC748140C0BBADA4FD11294164AFBF9681AB58410000006066808140
930	elem_2d	221.859741	559.23999	12	\N	01030000A06A0800000100000005000000C08F7A71EA1129411C5F07387DAB5841000000C0CC748140E0E1093405122941FA69DE8B7BAB584100000080EB6B814090B5AD240E122941CE5E07087EAB584100000000007A8140C0BBADA4FD11294164AFBF9681AB58410000006066808140C08F7A71EA1129411C5F07387DAB5841000000C0CC748140
931	elem_2d	207.390198	556.340027	12	\N	01030000A06A0800000100000005000000802699F643122941E47E8C637AAB5841000000205C63814070B35B6C4A122941FA9E11BF76AB5841000000205C5B8140C094ADA465122941E4DB1B2678AB584100000000295E8140F0AB5B6C5E12294168A7E8927BAB58410000008014628140802699F643122941E47E8C637AAB5841000000205C638140
932	elem_2d	142.737747	556.530029	12	\N	01030000A06A0800000100000005000000802699F643122941E47E8C637AAB5841000000205C638140F0AB5B6C5E12294168A7E8927BAB58410000008014628140B0FF138B59122941C6B0BFE67DAB5841000000C0CC62814060F365C343122941F40C4FD97DAB5841000000E0A3648140802699F643122941E47E8C637AAB5841000000205C638140
933	elem_2d	244.210297	552.679993	12	\N	01030000A06A0800000100000005000000B0D88E9F6F122941F84D595074AB5841000000E0A346814090537AF18A122941D4E6F24976AB584100000080EB458140501B701A7D122941C44B59307AAB584100000060B84C8140C094ADA465122941E4DB1B2678AB584100000000295E8140B0D88E9F6F122941F84D595074AB5841000000E0A3468140
934	elem_2d	73.5380936	557.099976	12	\N	01030000A06A0800000100000005000000601BB87BA211294186A7E8427BAB584100000060B8728140503A3DE7A1112941E46ADE1B79AB5841000000C0F56C8140B05C9976B31129415675B58F78AB5841000000205C65814010EC5B6CB3112941088963A77AAB5841000000608F668140601BB87BA211294186A7E8427BAB584100000060B8728140
935	elem_2d	178.575394	554.359985	12	\N	01030000A06A0800000100000005000000F0ABE057B01229413C4959F080AB584100000000005C8140505F2839A2122941DA68DE8B7EAB5841000000205C558140805C28B9A9122941B6B1BF667BAB584100000080C2478140F0AFB7FBC01229416617264D7DAB5841000000C01E498140F0ABE057B01229413C4959F080AB584100000000005C8140
936	elem_2d	67.5823441	558.099976	12	\N	01030000A06A0800000100000005000000D095CCA9A3112941CA20FD907FAB584100000020AE7B8140C0B45115A311294128E4F2697DAB58410000000000728140E0F53290B411294134DA1BA67CAB5841000000A099638140D0323DE7B5112941E0E3F2297EAB5841000000A0476B8140D095CCA9A3112941CA20FD907FAB584100000020AE7B8140
937	elem_2d	113.718742	556.75	12	\N	01030000A06A080000010000000500000080CDD600B3112941D4E6F24976AB5841000000C01E658140F00BE157B01129416C104F9974AB584100000080145C8140D061701AC11129413AB5BF0672AB58410000000029568140B0178F9FC71129417A4D59A075AB5841000000801464814080CDD600B3112941D4E6F24976AB5841000000C01E658140
938	elem_2d	228.575333	553.780029	12	\N	01030000A06A080000010000000500000020937A71E1112941E657302475AB584100000080EB6381407055701AE21129416CDE1B6671AB5841000000C0F5508140C090511503122941A8DE1BC670AB584100000060664E8140204199F6FC1129411A2FD4B474AB5841000000400A63814020937A71E1112941E657302475AB584100000080EB638140
939	elem_2d	247.426041	556.580017	12	\N	01030000A06A080000010000000500000030E1E0572212294164B2BF9679AB5841000000E0A3648140802699F643122941E47E8C637AAB5841000000205C63814060F365C343122941F40C4FD97DAB5841000000E0A36481404065CC29251229413AEEC9DD7DAB5841000000403365814030E1E0572212294164B2BF9679AB5841000000E0A3648140
940	elem_2d	72.5725479	557.159973	12	\N	01030000A06A080000010000000500000010EC5B6CB3112941088963A77AAB5841000000608F668140E0F53290B411294134DA1BA67CAB5841000000A099638140C0B45115A311294128E4F2697DAB58410000000000728140601BB87BA211294186A7E8427BAB584100000060B872814010EC5B6CB3112941088963A77AAB5841000000608F668140
941	elem_2d	78.6567001	556.72998	12	\N	01030000A06A0800000100000005000000503A3DE7A1112941E46ADE1B79AB5841000000C0F56C8140F0A0A34DA1112941422ED4F476AB5841000000E07A66814080CDD600B3112941D4E6F24976AB5841000000C01E658140B05C9976B31129415675B58F78AB5841000000205C658140503A3DE7A1112941E46ADE1B79AB5841000000C0F56C8140
942	elem_2d	208.804596	556.419983	12	\N	01030000A06A0800000100000005000000E085D600721229414CA5E83281AB584100000080C2818140004DF5854A1229415453305481AB5841000000801478814060F365C343122941F40C4FD97DAB5841000000E0A3648140B0FF138B59122941C6B0BFE67DAB5841000000C0CC628140E085D600721229414CA5E83281AB584100000080C2818140
943	elem_2d	213.165497	553.51001	12	\N	01030000A06A08000001000000050000000016140B1E12294174FDA09170AB584100000080EB51814040FA8E1F1612294122DD1BD674AB5841000000A099658140204199F6FC1129411A2FD4B474AB5841000000400A638140C090511503122941A8DE1BC670AB584100000060664E81400016140B1E12294174FDA09170AB584100000080EB518140
944	elem_2d	80.0222473	557.97998	12	\N	01030000A06A0800000100000005000000A05E709AC9112941C268DECB7EAB5841000000C0CC6E8140C0FE0934B81129411AE3F23980AB5841000000A047738140D095CCA9A3112941CA20FD907FAB584100000020AE7B8140D0323DE7B5112941E0E3F2297EAB5841000000A0476B8140A05E709AC9112941C268DECB7EAB5841000000C0CC6E8140
957	elem_2d	97.9319992	556.400024	12	\N	01030000A06A0800000100000005000000B0178F9FC71129417A4D59A075AB5841000000801464814080F90934C61129410EE6F25978AB5841000000803D628140B05C9976B31129415675B58F78AB5841000000205C65814080CDD600B3112941D4E6F24976AB5841000000C01E658140B0178F9FC71129417A4D59A075AB58410000008014648140
958	elem_2d	157.559494	554.099976	12	\N	01030000A06A0800000100000005000000501B701A7D122941C44B59307AAB584100000060B84C814090281E6290122941B040826C7CAB5841000000608F4C8140A09B5BEC891229417468DE9B7FAB58410000006066628140D0EA3CE775122941CC2BD4847DAB5841000000C01E5F8140501B701A7D122941C44B59307AAB584100000060B84C8140
959	elem_2d	117.572792	553.559998	12	\N	01030000A06A0800000100000005000000B04E7AF197122941D6D044527AAB5841000000608F468140805C28B9A9122941B6B1BF667BAB584100000080C2478140505F2839A2122941DA68DE8B7EAB5841000000205C55814090281E6290122941B040826C7CAB5841000000608F4C8140B04E7AF197122941D6D044527AAB5841000000608F468140
960	elem_2d	60.1991463	555.429993	12	\N	01030000A06A0800000100000005000000E0F53290B411294134DA1BA67CAB5841000000A09963814010EC5B6CB3112941088963A77AAB5841000000608F668140D0DC84C8C011294164BC963A7AAB584100000040335B814020741EE2C6112941CAE4F2B97BAB584100000080145C8140E0F53290B411294134DA1BA67CAB5841000000A099638140
961	elem_2d	61.9849472	555.77002	12	\N	01030000A06A0800000100000005000000B05C9976B31129415675B58F78AB5841000000205C65814080F90934C61129410EE6F25978AB5841000000803D628140D0DC84C8C011294164BC963A7AAB584100000040335B814010EC5B6CB3112941088963A77AAB5841000000608F668140B05C9976B31129415675B58F78AB5841000000205C658140
962	elem_2d	163.942596	552.590027	12	\N	01030000A06A080000010000000500000090281E6290122941B040826C7CAB5841000000608F4C8140501B701A7D122941C44B59307AAB584100000060B84C814090537AF18A122941D4E6F24976AB584100000080EB458140B04E7AF197122941D6D044527AAB5841000000608F46814090281E6290122941B040826C7CAB5841000000608F4C8140
963	elem_2d	188.846298	557.789978	12	\N	01030000A06A080000010000000500000030E1E0572212294164B2BF9679AB5841000000E0A36481404065CC29251229413AEEC9DD7DAB5841000000403365814090B5AD240E122941CE5E07087EAB584100000000007A8140E0E1093405122941FA69DE8B7BAB584100000080EB6B814030E1E0572212294164B2BF9679AB5841000000E0A3648140
964	elem_2d	113.135498	554.909973	12	\N	01030000A06A0800000100000005000000106DFFDC98122941A40178B580AB584100000080EB698140A09B5BEC891229417468DE9B7FAB5841000000606662814090281E6290122941B040826C7CAB5841000000608F4C8140505F2839A2122941DA68DE8B7EAB5841000000205C558140106DFFDC98122941A40178B580AB584100000080EB698140
965	elem_2d	186.392349	552.580017	12	\N	01030000A06A080000010000000500000090537AF18A122941D4E6F24976AB584100000080EB458140C0838448AE12294132E6F2F977AB584100000080EB438140805C28B9A9122941B6B1BF667BAB584100000080C2478140B04E7AF197122941D6D044527AAB5841000000608F46814090537AF18A122941D4E6F24976AB584100000080EB458140
966	elem_2d	142.159393	556.590027	12	\N	01030000A06A0800000100000005000000406028B99F122941127B8C9384AB5841000000608F868140106DFFDC98122941A40178B580AB584100000080EB698140505F2839A2122941DA68DE8B7EAB5841000000205C558140F0ABE057B01229413C4959F080AB584100000000005C8140406028B99F122941127B8C9384AB5841000000608F868140
967	elem_2d	108.111092	554.859985	12	\N	01030000A06A0800000100000005000000E0BF28B9A0112941A0F1C9CD74AB5841000000A0995D814080268F1FA0112941FEB4BFA672AB5841000000400A558140D061701AC11129413AB5BF0672AB58410000000029568140F00BE157B01129416C104F9974AB584100000080145C8140E0BF28B9A0112941A0F1C9CD74AB5841000000A0995D8140
945	elem_2d	162.229736	553.719971	12	\N	01030000A06A08000001000000050000000016140B1E12294174FDA09170AB584100000080EB518140F0E2B7FB381229413645825C70AB584100000080EB498140D05DA34D5412294112DE1B5672AB5841000000803D3E814040DAE0D734122941D28B633773AB5841000000803D4A81400016140B1E12294174FDA09170AB584100000080EB518140
946	elem_2d	110.533241	558.780029	12	\N	01030000A06A0800000100000005000000C0FE0934B81129411AE3F23980AB5841000000A047738140A05E709AC9112941C268DECB7EAB5841000000C0CC6E814010AB2839D81129416C5D07B881AB5841000000400A8F8140206D473EBE1129416C5D07B881AB5841000000205C898140C0FE0934B81129411AE3F23980AB5841000000A047738140
947	elem_2d	65.4294968	556.580017	12	\N	01030000A06A0800000100000005000000F0A0A34DA1112941422ED4F476AB5841000000E07A668140E0BF28B9A0112941A0F1C9CD74AB5841000000A0995D8140F00BE157B01129416C104F9974AB584100000080145C814080CDD600B3112941D4E6F24976AB5841000000C01E658140F0A0A34DA1112941422ED4F476AB5841000000E07A668140
948	elem_2d	158.596786	558.080017	12	\N	01030000A06A08000001000000050000001073FFDC88122941B47B8CE382AB584100000080EB8B8140E085D600721229414CA5E83281AB584100000080C2818140D0EA3CE775122941CC2BD4847DAB5841000000C01E5F8140A09B5BEC891229417468DE9B7FAB584100000060666281401073FFDC88122941B47B8CE382AB584100000080EB8B8140
949	elem_2d	162.086136	556.5	12	\N	01030000A06A080000010000000500000030E1E0572212294164B2BF9679AB5841000000E0A36481405006664311122941EAD1447277AB584100000060B864814040FA8E1F1612294122DD1BD674AB5841000000A09965814080DCE0D72E1229416A76B5AF75AB5841000000205C5F814030E1E0572212294164B2BF9679AB5841000000E0A3648140
950	elem_2d	213.255035	559.890015	12	\N	01030000A06A08000001000000050000004065CC29251229413AEEC9DD7DAB584100000040336581402014140B231229415C01787581AB5841000000803D7C8140C0BBADA4FD11294164AFBF9681AB5841000000606680814090B5AD240E122941CE5E07087EAB584100000000007A81404065CC29251229413AEEC9DD7DAB58410000004033658140
951	elem_2d	82.5914993	558.880005	12	\N	01030000A06A0800000100000005000000206D473EBE1129416C5D07B881AB5841000000205C898140302F6643A41129416C5D07B881AB58410000002085958140D095CCA9A3112941CA20FD907FAB584100000020AE7B8140C0FE0934B81129411AE3F23980AB5841000000A047738140206D473EBE1129416C5D07B881AB5841000000205C898140
952	elem_2d	187.271698	556.099976	12	\N	01030000A06A0800000100000005000000D0EA3CE775122941CC2BD4847DAB5841000000C01E5F8140F0AB5B6C5E12294168A7E8927BAB58410000008014628140C094ADA465122941E4DB1B2678AB584100000000295E8140501B701A7D122941C44B59307AAB584100000060B84C8140D0EA3CE775122941CC2BD4847DAB5841000000C01E5F8140
953	elem_2d	87.7495499	556.320007	12	\N	01030000A06A0800000100000005000000A05E709AC9112941C268DECB7EAB5841000000C0CC6E8140D0323DE7B5112941E0E3F2297EAB5841000000A0476B8140E0F53290B411294134DA1BA67CAB5841000000A09963814020741EE2C6112941CAE4F2B97BAB584100000080145C8140A05E709AC9112941C268DECB7EAB5841000000C0CC6E8140
954	elem_2d	167.014343	556.190002	12	\N	01030000A06A0800000100000005000000E085D600721229414CA5E83281AB584100000080C2818140B0FF138B59122941C6B0BFE67DAB5841000000C0CC628140F0AB5B6C5E12294168A7E8927BAB58410000008014628140D0EA3CE775122941CC2BD4847DAB5841000000C01E5F8140E085D600721229414CA5E83281AB584100000080C2818140
955	elem_2d	127.003998	558.150024	12	\N	01030000A06A0800000100000005000000A09B5BEC891229417468DE9B7FAB58410000006066628140106DFFDC98122941A40178B580AB584100000080EB698140406028B99F122941127B8C9384AB5841000000608F8681401073FFDC88122941B47B8CE382AB584100000080EB8B8140A09B5BEC891229417468DE9B7FAB58410000006066628140
956	elem_2d	136.0952	556.48999	12	\N	01030000A06A0800000100000005000000C0BBADA4FD1129414242823C78AB5841000000E07A648140204199F6FC1129411A2FD4B474AB5841000000400A63814040FA8E1F1612294122DD1BD674AB5841000000A0996581405006664311122941EAD1447277AB584100000060B8648140C0BBADA4FD1129414242823C78AB5841000000E07A648140
\.


--
-- Data for Name: _froude_bc_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _froude_bc_singularity (id, singularity_type) FROM stdin;
\.


--
-- Data for Name: _fuse_spillway_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _fuse_spillway_link (id, link_type, z_invert, width, cc, break_mode, z_break, t_break, grp, dt_fracw_array) FROM stdin;
\.


--
-- Data for Name: _gate_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _gate_link (id, link_type, z_invert, z_ceiling, width, cc, action_gate_type, mode_valve, z_gate, v_max_cms) FROM stdin;
\.


--
-- Data for Name: _gate_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _gate_singularity (id, singularity_type, z_invert, z_ceiling, width, cc, action_gate_type, mode_valve, z_gate, v_max_cms) FROM stdin;
\.


--
-- Data for Name: _hydraulic_cut_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _hydraulic_cut_singularity (id, singularity_type, qz_array) FROM stdin;
\.


--
-- Data for Name: _hydrograph_bc_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _hydrograph_bc_singularity (id, singularity_type, storage_area, tq_array, constant_dry_flow, distrib_coef, lag_time_hr, sector, hourly_modulation, pollution_quality_mode, pollution_quality_param, external_file_data) FROM stdin;
13	hydrograph_bc	1	{{0,0},{999,0}}	0	1	0	\N	\N	\N	\N	t
11	hydrograph_bc	1	{{0,0},{999,0}}	0	1	0	\N	\N	\N	\N	t
12	hydrograph_bc	1	{{0,0},{999,0}}	0	1	0	\N	\N	\N	\N	t
\.


--
-- Data for Name: _hydrology_bc_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _hydrology_bc_singularity (id, singularity_type) FROM stdin;
\.


--
-- Data for Name: _link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _link (id, link_type, name, generated, up, up_type, down, down_type, geom, configuration, validity) FROM stdin;
1625	mesh_2d	LPAV1625	12	1035	elem_2d	978	elem_2d	01020000A06A08000002000000804B8E1FE813294108FAA0B179AB584100000040E112814050FBD580E31329416420FDA080AB5841000000E051168140	\N	t
1373	mesh_2d	LPAV1373	10	848	elem_2d	846	elem_2d	01020000A06A08000002000000D0961E626A112941BE8A631776AB584100000020AE7D8140609A1EE260112941366DDEEB72AB584100000060665E8140	\N	t
1759	routing_hydrology	ROUT1759	\N	1063	catchment	1	river	01020000A06A08000002000000E36F7B4A550D29418880436EB2AB58410000000000168240EDD79DBE3F0E29414AB54F5B8BAB58415C8FC2F528CE8140	\N	t
1751	pipe	TRC1751	\N	1051	manhole	1052	manhole	01020000A06A08000002000000D4F36E7C2D1329410F971C138BAB5841CDCCCCCCCC7881400FB8AEE8BE122941AA9A209E82AB5841F6285C8FC2578140	\N	t
1626	mesh_2d	LPAV1626	12	1036	elem_2d	978	elem_2d	01020000A06A080000020000000042E0D7CA13294130B0BF767FAB584100000000291A814050FBD580E31329416420FDA080AB5841000000E051168140	\N	t
1374	mesh_2d	LPAV1374	10	849	elem_2d	824	elem_2d	01020000A06A08000002000000401D29B9A7102941841726FD7CAB5841000000E07A948140109B3DE79F10294104B2BF967AAB5841000000E0A38A8140	\N	t
1381	mesh_2d	LPAV1381	10	854	elem_2d	833	elem_2d	01020000A06A08000002000000601D52158C102941FA5530447AAB584100000000D789814010DD47BE931029415819261D78AB584100000020AE878140	\N	t
1627	mesh_2d	LPAV1627	12	1006	elem_2d	978	elem_2d	01020000A06A0800000200000040EFFE5CE8132941F2CC44B284AB5841000000A0701B814050FBD580E31329416420FDA080AB5841000000E051168140	\N	t
1375	mesh_2d	LPAV1375	10	851	elem_2d	844	elem_2d	01020000A06A0800000200000020583D6752112941FC3F824C7EAB5841000000C0F586814080D128B9711129411CF8A0D17EAB58410000002085878140	\N	t
1760	routing_hydrology	ROUT1760	\N	1064	catchment	3	river	01020000A06A08000002000000A2D3F31E9F132941CB9C2E4319AB5841D7A3703D0A4781409148DB50A6142941EFACDD2C39AB5841EC51B81E85018140	\N	t
1628	mesh_2d	LPAV1628	12	979	elem_2d	978	elem_2d	01020000A06A0800000200000050CA79F1F8132941520C4F897FAB5841000000803D12814050FBD580E31329416420FDA080AB5841000000E051168140	\N	t
1376	mesh_2d	LPAV1376	10	851	elem_2d	845	elem_2d	01020000A06A0800000200000020583D6752112941FC3F824C7EAB5841000000C0F586814000BEA3CD53112941EA7E8C537AAB5841000000C01E7D8140	\N	t
1752	pipe	TRC1752	\N	1052	manhole	1053	manhole	01020000A06A080000020000000FB8AEE8BE122941AA9A209E82AB5841F6285C8FC2578140EBFD46138912294196218E5778AB5841CDCCCCCCCC448140	\N	t
1629	mesh_2d	LPAV1629	12	996	elem_2d	979	elem_2d	01020000A06A08000002000000C0BFA2CDF9132941D28863377BAB584100000060B810814050CA79F1F8132941520C4F897FAB5841000000803D128140	\N	t
1753	pipe	TRC1753	\N	1053	manhole	1054	manhole	01020000A06A08000002000000EBFD46138912294196218E5778AB5841CDCCCCCCCC4481409A09863B33122941020EA12873AB584114AE47E17A4A8140	\N	t
1377	mesh_2d	LPAV1377	10	852	elem_2d	840	elem_2d	01020000A06A08000002000000B02EA44D271029413A19266D78AB5841000000205C918140D0081F623A102941340F4FD977AB5841000000A070918140	\N	t
1630	mesh_2d	LPAV1630	12	990	elem_2d	979	elem_2d	01020000A06A08000002000000700E5B6C0214294168A4E89283AB5841000000608F14814050CA79F1F8132941520C4F897FAB5841000000803D128140	\N	t
1382	mesh_2d	LPAV1382	10	855	elem_2d	848	elem_2d	01020000A06A0800000200000040D6FF5C80112941586CDE3B75AB58410000008014748140D0961E626A112941BE8A631776AB584100000020AE7D8140	\N	t
1378	mesh_2d	LPAV1378	10	852	elem_2d	841	elem_2d	01020000A06A08000002000000B02EA44D271029413A19266D78AB5841000000205C918140807E5CEC2C102941FA05782575AB584100000020AE8F8140	\N	t
1750	pipe	TRC1750	\N	1050	manhole	1051	manhole	01020000A06A08000002000000936FB6F9A613294147ACC51394AB58410AD7A3703D808140D4F36E7C2D1329410F971C138BAB5841CDCCCCCCCC788140	\N	t
1631	mesh_2d	LPAV1631	12	987	elem_2d	979	elem_2d	01020000A06A080000020000000034B77B0B142941A45430D47DAB5841000000E0510E814050CA79F1F8132941520C4F897FAB5841000000803D128140	\N	t
1379	mesh_2d	LPAV1379	10	854	elem_2d	824	elem_2d	01020000A06A08000002000000601D52158C102941FA5530447AAB584100000000D7898140109B3DE79F10294104B2BF967AAB5841000000E0A38A8140	\N	t
1632	mesh_2d	LPAV1632	12	1026	elem_2d	980	elem_2d	01020000A06A08000002000000801432900D142941D04B59107AAB5841000000803D0C814030328E9F2B142941C836AB887BAB5841000000E0A3068140	\N	t
1756	connector	QMTR1756	\N	1054	manhole	12	river	01020000A06A080000020000009A09863B33122941020EA12873AB584114AE47E17A4A8140A75D4CCB38122941210725E06EAB5841B81E85EB51328140	\N	t
1380	mesh_2d	LPAV1380	10	854	elem_2d	825	elem_2d	01020000A06A08000002000000601D52158C102941FA5530447AAB584100000000D789814060B9EBAE8510294126A7E8427CAB584100000060668C8140	\N	t
1383	mesh_2d	LPAV1383	10	857	elem_2d	837	elem_2d	01020000A06A0800000200000020E0ADA49C112941B4BA96BA7EAB5841000000A099878140A06399F6A01129414AEDC95D80AB5841000000A0477F8140	\N	t
1633	mesh_2d	LPAV1633	12	1022	elem_2d	980	elem_2d	01020000A06A08000002000000E0766F9A33142941280C4FF97FAB5841000000205C05814030328E9F2B142941C836AB887BAB5841000000E0A3068140	\N	t
1758	routing_hydrology	ROUT1758	\N	1062	catchment	1050	manhole	01020000A06A08000002000000EAEC647844132941A9F6E96ABBAB5841A4703D0AD7ED8240936FB6F9A613294147ACC51394AB58410AD7A3703D808140	\N	t
1634	mesh_2d	LPAV1634	12	998	elem_2d	980	elem_2d	01020000A06A0800000200000090C9FEDC4C142941840D4F597CAB5841000000C0CC00814030328E9F2B142941C836AB887BAB5841000000E0A3068140	\N	t
1384	mesh_2d	LPAV1384	10	859	elem_2d	847	elem_2d	01020000A06A0800000200000050320AB42E112941AEB0BF267EAB58410000008014928140B00200DD09112941C6B0BFE67DAB5841000000C01E998140	\N	t
1392	mesh_2d	LPAV1392	10	862	elem_2d	853	elem_2d	01020000A06A08000002000000D009F60553102941B0BB961A7CAB584100000000D793814020207B71691029411A8963777AAB5841000000E0518E8140	\N	t
1635	mesh_2d	LPAV1635	12	1024	elem_2d	981	elem_2d	01020000A06A08000002000000B06C138BE11329416A1326ED87AB58410000000029228140B0B3CBA9FE132941725A07A889AB5841000000E07A208140	\N	t
1385	mesh_2d	LPAV1385	10	859	elem_2d	851	elem_2d	01020000A06A0800000200000050320AB42E112941AEB0BF267EAB5841000000801492814020583D6752112941FC3F824C7EAB5841000000C0F5868140	\N	t
1755	network_overflow	NOV1755	13	1053	manhole	962	elem_2d	01020000A06A08000002000000EBFD46138912294196218E5778AB5841CDCCCCCCCC4481403049A34D8B12294154DB1BA679AB584100000060B8448140	\N	t
1754	network_overflow	NOV1754	13	1054	manhole	923	elem_2d	01020000A06A080000020000009A09863B33122941020EA12873AB584114AE47E17A4A8140C0A2D600251229410A44827C73AB584100000060664E8140	\N	t
1636	mesh_2d	LPAV1636	12	1023	elem_2d	981	elem_2d	01020000A06A08000002000000E02CB77B1E14294116C06DAE8BAB5841000000803D208140B0B3CBA9FE132941725A07A889AB5841000000E07A208140	\N	t
1386	mesh_2d	LPAV1386	10	860	elem_2d	830	elem_2d	01020000A06A0800000200000060670A34A11029417CE7F28974AB584100000080147C8140C0BB99F6B510294152DD1B5674AB5841000000C0F57A8140	\N	t
1637	mesh_2d	LPAV1637	12	1008	elem_2d	981	elem_2d	01020000A06A08000002000000601D093411142941CC3C82CC86AB5841000000C01E178140B0B3CBA9FE132941725A07A889AB5841000000E07A208140	\N	t
1387	mesh_2d	LPAV1387	10	860	elem_2d	843	elem_2d	01020000A06A0800000200000060670A34A11029417CE7F28974AB584100000080147C814060C199F6A61029418A4C592078AB584100000020AE858140	\N	t
1393	mesh_2d	LPAV1393	10	862	elem_2d	858	elem_2d	01020000A06A08000002000000D009F60553102941B0BB961A7CAB584100000000D793814030C03DE73C102941F48763877DAB584100000080EBA38140	\N	t
1638	mesh_2d	LPAV1638	12	1036	elem_2d	982	elem_2d	01020000A06A080000020000000042E0D7CA13294130B0BF767FAB584100000000291A8140A0E75015C61329410C8F3AEB85AB5841000000C0CC228140	\N	t
1388	mesh_2d	LPAV1388	10	861	elem_2d	839	elem_2d	01020000A06A08000002000000500EF6054710294106E8F21973AB5841000000608F868140F0E1C2D234102941B4818CE372AB5841000000A0998B8140	\N	t
1639	mesh_2d	LPAV1639	12	1024	elem_2d	982	elem_2d	01020000A06A08000002000000B06C138BE11329416A1326ED87AB58410000000029228140A0E75015C61329410C8F3AEB85AB5841000000C0CC228140	\N	t
1389	mesh_2d	LPAV1389	10	861	elem_2d	850	elem_2d	01020000A06A08000002000000500EF6054710294106E8F21973AB5841000000608F86814060DC99F65E102941C4E7F2C973AB5841000000E0A3828140	\N	t
1640	mesh_2d	LPAV1640	12	1006	elem_2d	982	elem_2d	01020000A06A0800000200000040EFFE5CE8132941F2CC44B284AB5841000000A0701B8140A0E75015C61329410C8F3AEB85AB5841000000C0CC228140	\N	t
1390	mesh_2d	LPAV1390	10	862	elem_2d	826	elem_2d	01020000A06A08000002000000D009F60553102941B0BB961A7CAB584100000000D7938140C07533905F1029414221FD507EAB5841000000E051A48140	\N	t
1641	mesh_2d	LPAV1641	12	1023	elem_2d	983	elem_2d	01020000A06A08000002000000E02CB77B1E14294116C06DAE8BAB5841000000803D208140205FC1523C1429414ABF6DCE8DAB5841000000C01E238140	\N	t
1391	mesh_2d	LPAV1391	10	862	elem_2d	842	elem_2d	01020000A06A08000002000000D009F60553102941B0BB961A7CAB584100000000D793814000C7140B46102941F89D116F79AB5841000000A0478F8140	\N	t
1394	mesh_2d	LPAV1394	10	864	elem_2d	828	elem_2d	01020000A06A08000002000000D0570AB4CA102941309F112F76AB5841000000E07A7E8140401429B9BF10294194A8E87278AB58410000004033858140	\N	t
1642	mesh_2d	LPAV1642	12	1020	elem_2d	983	elem_2d	01020000A06A08000002000000209CA2CD581429415210262D90AB5841000000400A258140205FC1523C1429414ABF6DCE8DAB5841000000C01E238140	\N	t
1395	mesh_2d	LPAV1395	10	864	elem_2d	830	elem_2d	01020000A06A08000002000000D0570AB4CA102941309F112F76AB5841000000E07A7E8140C0BB99F6B510294152DD1B5674AB5841000000C0F57A8140	\N	t
1643	mesh_2d	LPAV1643	12	984	elem_2d	983	elem_2d	01020000A06A0800000200000080DBACA453142941BCABBF568BAB584100000040331B8140205FC1523C1429414ABF6DCE8DAB5841000000C01E238140	\N	t
1396	mesh_2d	LPAV1396	10	865	elem_2d	847	elem_2d	01020000A06A0800000200000090F428391411294142DB1BD679AB584100000080EB858140B00200DD09112941C6B0BFE67DAB5841000000C01E998140	\N	t
1644	mesh_2d	LPAV1644	12	1034	elem_2d	984	elem_2d	01020000A06A08000002000000E0F031906C142941D825D4648DAB584100000040E11A814080DBACA453142941BCABBF568BAB584100000040331B8140	\N	t
1761	connector	QMTR1761	\N	2	river	5	river	01020000A06A08000002000000D6A71C53FF1429415CE6748D7BAB58415C8FC2F528CA8040425BCE8D1315294115A930CE7BAB58410000000000C28040	\N	t
1645	mesh_2d	LPAV1645	12	1013	elem_2d	984	elem_2d	01020000A06A0800000200000090741D62701429415ADFF2398AAB5841000000A07013814080DBACA453142941BCABBF568BAB584100000040331B8140	\N	t
1397	mesh_2d	LPAV1397	10	865	elem_2d	856	elem_2d	01020000A06A0800000200000090F428391411294142DB1BD679AB584100000080EB858140C01EAEA4F5102941D037ABC878AB584100000080EB858140	\N	t
1404	mesh_2d	LPAV1404	10	869	elem_2d	828	elem_2d	01020000A06A0800000200000040FD7A71C61029416074B51F7BAB5841000000A0478D8140401429B9BF10294194A8E87278AB58410000004033858140	\N	t
1762	connector	QMTR1762	\N	4	river	5	river	01020000A06A08000002000000CAA8327C0D1529411366DA4679AB58416666666666C48040425BCE8D1315294115A930CE7BAB58410000000000C28040	\N	t
1646	mesh_2d	LPAV1646	12	1003	elem_2d	984	elem_2d	01020000A06A08000002000000B0316543481429412AB7962A88AB5841000000E0A312814080DBACA453142941BCABBF568BAB584100000040331B8140	\N	t
1398	mesh_2d	LPAV1398	10	866	elem_2d	831	elem_2d	01020000A06A0800000200000020298F1F9911294192F0C99D77AB584100000000D773814070841E629B112941F2E6F2F975AB5841000000608F6A8140	\N	t
1647	mesh_2d	LPAV1647	12	1033	elem_2d	985	elem_2d	01020000A06A0800000200000010BE27395014294164913AAB7FAB5841000000A04703814040E75AEC6A142941C0B0BFF67DAB5841000000205CFF8040	\N	t
1399	mesh_2d	LPAV1399	10	866	elem_2d	863	elem_2d	01020000A06A0800000200000020298F1F9911294192F0C99D77AB584100000000D7738140406999F691112941D641825C79AB58410000008014808140	\N	t
1648	mesh_2d	LPAV1648	12	1002	elem_2d	985	elem_2d	01020000A06A08000002000000C0D8834876142941B834AB0881AB5841000000C01E01814040E75AEC6A142941C0B0BFF67DAB5841000000205CFF8040	\N	t
1400	mesh_2d	LPAV1400	10	867	elem_2d	822	elem_2d	01020000A06A0800000200000020448F1F5111294174DC1BA676AB584100000000D77B8140009F473E391129417E38ABF876AB58410000000000768140	\N	t
1405	mesh_2d	LPAV1405	10	869	elem_2d	849	elem_2d	01020000A06A0800000200000040FD7A71C61029416074B51F7BAB5841000000A0478D8140401D29B9A7102941841726FD7CAB5841000000E07A948140	\N	t
1649	mesh_2d	LPAV1649	12	998	elem_2d	985	elem_2d	01020000A06A0800000200000090C9FEDC4C142941840D4F597CAB5841000000C0CC00814040E75AEC6A142941C0B0BFF67DAB5841000000205CFF8040	\N	t
1401	mesh_2d	LPAV1401	10	867	elem_2d	834	elem_2d	01020000A06A0800000200000020448F1F5111294174DC1BA676AB584100000000D77B8140F0250AB44F1129410677B50F74AB5841000000C01E6D8140	\N	t
1650	mesh_2d	LPAV1650	12	1039	elem_2d	986	elem_2d	01020000A06A08000002000000C08F27B9CB142941465C07C884AB58410000008014008140A0E00834B3142941C0ADBFF685AB58410000002085FB8040	\N	t
1402	mesh_2d	LPAV1402	10	867	elem_2d	845	elem_2d	01020000A06A0800000200000020448F1F5111294174DC1BA676AB584100000000D77B814000BEA3CD53112941EA7E8C537AAB5841000000C01E7D8140	\N	t
1403	mesh_2d	LPAV1403	10	867	elem_2d	848	elem_2d	01020000A06A0800000200000020448F1F5111294174DC1BA676AB584100000000D77B8140D0961E626A112941BE8A631776AB584100000020AE7D8140	\N	t
1651	mesh_2d	LPAV1651	12	1018	elem_2d	986	elem_2d	01020000A06A08000002000000B0C483C8AB142941967B8C3383AB5841000000A070F98040A0E00834B3142941C0ADBFF685AB58410000002085FB8040	\N	t
1406	mesh_2d	LPAV1406	10	841	elem_2d	839	elem_2d	01020000A06A08000002000000807E5CEC2C102941FA05782575AB584100000020AE8F8140F0E1C2D234102941B4818CE372AB5841000000A0998B8140	\N	t
1652	mesh_2d	LPAV1652	12	1016	elem_2d	986	elem_2d	01020000A06A08000002000000C0DD31909F1429416632AB3887AB584100000060B8008140A0E00834B3142941C0ADBFF685AB58410000002085FB8040	\N	t
1407	mesh_2d	LPAV1407	10	842	elem_2d	840	elem_2d	01020000A06A0800000200000000C7140B46102941F89D116F79AB5841000000A0478F8140D0081F623A102941340F4FD977AB5841000000A070918140	\N	t
1653	mesh_2d	LPAV1653	12	1001	elem_2d	986	elem_2d	01020000A06A08000002000000B0008E9FAF1429415827D46489AB5841000000803D008140A0E00834B3142941C0ADBFF685AB58410000002085FB8040	\N	t
1415	mesh_2d	LPAV1415	10	870	elem_2d	860	elem_2d	01020000A06A0800000200000030700AB489102941046CDE1B76AB5841000000608F88814060670A34A11029417CE7F28974AB584100000080147C8140	\N	t
1408	mesh_2d	LPAV1408	10	843	elem_2d	824	elem_2d	01020000A06A0800000200000060C199F6A61029418A4C592078AB584100000020AE858140109B3DE79F10294104B2BF967AAB5841000000E0A38A8140	\N	t
1654	mesh_2d	LPAV1654	12	1029	elem_2d	987	elem_2d	01020000A06A08000002000000C0BD79711A142941567C8C3381AB5841000000608F0A81400034B77B0B142941A45430D47DAB5841000000E0510E8140	\N	t
1409	mesh_2d	LPAV1409	10	843	elem_2d	828	elem_2d	01020000A06A0800000200000060C199F6A61029418A4C592078AB584100000020AE858140401429B9BF10294194A8E87278AB58410000004033858140	\N	t
1655	mesh_2d	LPAV1655	12	1026	elem_2d	987	elem_2d	01020000A06A08000002000000801432900D142941D04B59107AAB5841000000803D0C81400034B77B0B142941A45430D47DAB5841000000E0510E8140	\N	t
1410	mesh_2d	LPAV1410	10	843	elem_2d	833	elem_2d	01020000A06A0800000200000060C199F6A61029418A4C592078AB584100000020AE85814010DD47BE931029415819261D78AB584100000020AE878140	\N	t
1656	mesh_2d	LPAV1656	12	996	elem_2d	987	elem_2d	01020000A06A08000002000000C0BFA2CDF9132941D28863377BAB584100000060B81081400034B77B0B142941A45430D47DAB5841000000E0510E8140	\N	t
1416	mesh_2d	LPAV1416	10	871	elem_2d	829	elem_2d	01020000A06A08000002000000C0DAFF5C741129412442828C78AB5841000000C0F5808140C01C0A3468112941A2EFC91D7AAB5841000000608F828140	\N	t
1411	mesh_2d	LPAV1411	10	845	elem_2d	829	elem_2d	01020000A06A0800000200000000BEA3CD53112941EA7E8C537AAB5841000000C01E7D8140C01C0A3468112941A2EFC91D7AAB5841000000608F828140	\N	t
1657	mesh_2d	LPAV1657	12	1030	elem_2d	988	elem_2d	01020000A06A0800000200000040483C67271429412E5C070885AB584100000020AE0F8140C00B093440142941900A4F3984AB584100000060660A8140	\N	t
1658	mesh_2d	LPAV1658	12	1022	elem_2d	988	elem_2d	01020000A06A08000002000000E0766F9A33142941280C4FF97FAB5841000000205C058140C00B093440142941900A4F3984AB584100000060660A8140	\N	t
1412	mesh_2d	LPAV1412	10	846	elem_2d	834	elem_2d	01020000A06A08000002000000609A1EE260112941366DDEEB72AB584100000060665E8140F0250AB44F1129410677B50F74AB5841000000C01E6D8140	\N	t
1659	mesh_2d	LPAV1659	12	1004	elem_2d	988	elem_2d	01020000A06A0800000200000050C4FEDC5A1429416E1FFD3083AB584100000040E1068140C00B093440142941900A4F3984AB584100000060660A8140	\N	t
1413	mesh_2d	LPAV1413	10	846	elem_2d	835	elem_2d	01020000A06A08000002000000609A1EE260112941366DDEEB72AB584100000060665E8140701A331053112941CC6307B870AB584100000020AE578140	\N	t
1414	mesh_2d	LPAV1414	10	870	elem_2d	833	elem_2d	01020000A06A0800000200000030700AB489102941046CDE1B76AB5841000000608F88814010DD47BE931029415819261D78AB584100000020AE878140	\N	t
1660	mesh_2d	LPAV1660	12	1003	elem_2d	988	elem_2d	01020000A06A08000002000000B0316543481429412AB7962A88AB5841000000E0A3128140C00B093440142941900A4F3984AB584100000060660A8140	\N	t
1417	mesh_2d	LPAV1417	10	871	elem_2d	848	elem_2d	01020000A06A08000002000000C0DAFF5C741129412442828C78AB5841000000C0F5808140D0961E626A112941BE8A631776AB584100000020AE7D8140	\N	t
1661	mesh_2d	LPAV1661	12	1027	elem_2d	989	elem_2d	01020000A06A08000002000000C06EA2CDD1142941504B30B496AB58410000004033058140606F7971EB1429418A2CABD896AB58410000002085FF8040	\N	t
1418	mesh_2d	LPAV1418	10	872	elem_2d	840	elem_2d	01020000A06A08000002000000B0BE3DE7401029410C953AEB75AB5841000000E07A908140D0081F623A102941340F4FD977AB5841000000A070918140	\N	t
1426	mesh_2d	LPAV1426	10	874	elem_2d	855	elem_2d	01020000A06A0800000200000030C4283995112941B26CDE4B74AB584100000080C25B814040D6FF5C80112941586CDE3B75AB58410000008014748140	\N	t
1662	mesh_2d	LPAV1662	12	992	elem_2d	989	elem_2d	01020000A06A0800000200000030BB5A6CE0142941CEB296CA93AB584100000020AEFF8040606F7971EB1429418A2CABD896AB58410000002085FF8040	\N	t
1419	mesh_2d	LPAV1419	10	872	elem_2d	841	elem_2d	01020000A06A08000002000000B0BE3DE7401029410C953AEB75AB5841000000E07A908140807E5CEC2C102941FA05782575AB584100000020AE8F8140	\N	t
1663	mesh_2d	LPAV1663	12	1029	elem_2d	990	elem_2d	01020000A06A08000002000000C0BD79711A142941567C8C3381AB5841000000608F0A8140700E5B6C0214294168A4E89283AB5841000000608F148140	\N	t
1420	mesh_2d	LPAV1420	10	872	elem_2d	861	elem_2d	01020000A06A08000002000000B0BE3DE7401029410C953AEB75AB5841000000E07A908140500EF6054710294106E8F21973AB5841000000608F868140	\N	t
1664	mesh_2d	LPAV1664	12	1008	elem_2d	990	elem_2d	01020000A06A08000002000000601D093411142941CC3C82CC86AB5841000000C01E178140700E5B6C0214294168A4E89283AB5841000000608F148140	\N	t
1421	mesh_2d	LPAV1421	10	873	elem_2d	842	elem_2d	01020000A06A0800000200000080F4473E551029411AD244F276AB584100000000008E814000C7140B46102941F89D116F79AB5841000000A0478F8140	\N	t
1427	mesh_2d	LPAV1427	10	875	elem_2d	863	elem_2d	01020000A06A08000002000000B051EB2E9A1129411037ABC87AAB584100000080147C8140406999F691112941D641825C79AB58410000008014808140	\N	t
1665	mesh_2d	LPAV1665	12	1006	elem_2d	990	elem_2d	01020000A06A0800000200000040EFFE5CE8132941F2CC44B284AB5841000000A0701B8140700E5B6C0214294168A4E89283AB5841000000608F148140	\N	t
1422	mesh_2d	LPAV1422	10	873	elem_2d	850	elem_2d	01020000A06A0800000200000080F4473E551029411AD244F276AB584100000000008E814060DC99F65E102941C4E7F2C973AB5841000000E0A3828140	\N	t
1666	mesh_2d	LPAV1666	12	1016	elem_2d	991	elem_2d	01020000A06A08000002000000C0DD31909F1429416632AB3887AB584100000060B800814030FADF578A142941F212262D89AB584100000000D7098140	\N	t
1423	mesh_2d	LPAV1423	10	873	elem_2d	872	elem_2d	01020000A06A0800000200000080F4473E551029411AD244F276AB584100000000008E8140B0BE3DE7401029410C953AEB75AB5841000000E07A908140	\N	t
1667	mesh_2d	LPAV1667	12	1013	elem_2d	991	elem_2d	01020000A06A0800000200000090741D62701429415ADFF2398AAB5841000000A07013814030FADF578A142941F212262D89AB584100000000D7098140	\N	t
1668	mesh_2d	LPAV1668	12	1005	elem_2d	991	elem_2d	01020000A06A08000002000000206F1DE27E142941E6E0F21986AB5841000000A07009814030FADF578A142941F212262D89AB584100000000D7098140	\N	t
1424	mesh_2d	LPAV1424	10	874	elem_2d	823	elem_2d	01020000A06A0800000200000030C4283995112941B26CDE4B74AB584100000080C25B81404023B87B8D1129419A4E59A072AB5841000000608F568140	\N	t
1428	mesh_2d	LPAV1428	10	876	elem_2d	853	elem_2d	01020000A06A08000002000000A0D899F668102941A09E11AF77AB5841000000400A8D814020207B71691029411A8963777AAB5841000000E0518E8140	\N	t
1425	mesh_2d	LPAV1425	10	874	elem_2d	831	elem_2d	01020000A06A0800000200000030C4283995112941B26CDE4B74AB584100000080C25B814070841E629B112941F2E6F2F975AB5841000000608F6A8140	\N	t
1669	mesh_2d	LPAV1669	12	1000	elem_2d	991	elem_2d	01020000A06A08000002000000204598F69C14294196074F298CAB5841000000A0700D814030FADF578A142941F212262D89AB584100000000D7098140	\N	t
1429	mesh_2d	LPAV1429	10	876	elem_2d	873	elem_2d	01020000A06A08000002000000A0D899F668102941A09E11AF77AB5841000000400A8D814080F4473E551029411AD244F276AB584100000000008E8140	\N	t
1670	mesh_2d	LPAV1670	12	1021	elem_2d	992	elem_2d	01020000A06A08000002000000209AFE5CCB142941F66BB58F91AB584100000060B804814030BB5A6CE0142941CEB296CA93AB584100000020AEFF8040	\N	t
1430	mesh_2d	LPAV1430	10	877	elem_2d	822	elem_2d	01020000A06A08000002000000B071148B2911294138B4BFB674AB5841000000608F748140009F473E391129417E38ABF876AB58410000000000768140	\N	t
1671	mesh_2d	LPAV1671	12	1010	elem_2d	992	elem_2d	01020000A06A08000002000000A09127B9C614294110BD6DBE93AB5841000000E05108814030BB5A6CE0142941CEB296CA93AB584100000020AEFF8040	\N	t
1431	mesh_2d	LPAV1431	10	878	elem_2d	827	elem_2d	01020000A06A08000002000000E0EB5115101129414AD2447276AB5841000000C0F57C814020C9F585FF102941A21A26AD74AB5841000000E0517A8140	\N	t
1672	mesh_2d	LPAV1672	12	1012	elem_2d	993	elem_2d	01020000A06A08000002000000D07FA24DA414294162C6443296AB58410000000029148140F08479F1B114294130F0A0F193AB5841000000A0470D8140	\N	t
1438	mesh_2d	LPAV1438	10	879	elem_2d	865	elem_2d	01020000A06A08000002000000506E148B32112941BC9D110F7AAB584100000060B87E814090F428391411294142DB1BD679AB584100000080EB858140	\N	t
1432	mesh_2d	LPAV1432	10	878	elem_2d	856	elem_2d	01020000A06A08000002000000E0EB5115101129414AD2447276AB5841000000C0F57C8140C01EAEA4F5102941D037ABC878AB584100000080EB858140	\N	t
1673	mesh_2d	LPAV1673	12	1011	elem_2d	993	elem_2d	01020000A06A08000002000000C0DE0834B8142941887E63A796AB584100000080140A8140F08479F1B114294130F0A0F193AB5841000000A0470D8140	\N	t
1674	mesh_2d	LPAV1674	12	1010	elem_2d	993	elem_2d	01020000A06A08000002000000A09127B9C614294110BD6DBE93AB5841000000E051088140F08479F1B114294130F0A0F193AB5841000000A0470D8140	\N	t
1433	mesh_2d	LPAV1433	10	878	elem_2d	865	elem_2d	01020000A06A08000002000000E0EB5115101129414AD2447276AB5841000000C0F57C814090F428391411294142DB1BD679AB584100000080EB858140	\N	t
1675	mesh_2d	LPAV1675	12	995	elem_2d	993	elem_2d	01020000A06A08000002000000B08B79F19F142941F66BB58F91AB5841000000C0CC108140F08479F1B114294130F0A0F193AB5841000000A0470D8140	\N	t
1434	mesh_2d	LPAV1434	10	878	elem_2d	877	elem_2d	01020000A06A08000002000000E0EB5115101129414AD2447276AB5841000000C0F57C8140B071148B2911294138B4BFB674AB5841000000608F748140	\N	t
1439	mesh_2d	LPAV1439	10	880	elem_2d	836	elem_2d	01020000A06A08000002000000A0BEF5851B112941748C638771AB5841000000E0516881400045B87B33112941266EDE6B70AB584100000080C2638140	\N	t
1676	mesh_2d	LPAV1676	12	1031	elem_2d	994	elem_2d	01020000A06A08000002000000A08BA2CD84142941BED71B3683AB5841000000A07003814050F5DF5797142941187B8C8384AB584100000060B8FE8040	\N	t
1435	mesh_2d	LPAV1435	10	879	elem_2d	822	elem_2d	01020000A06A08000002000000506E148B32112941BC9D110F7AAB584100000060B87E8140009F473E391129417E38ABF876AB58410000000000768140	\N	t
1677	mesh_2d	LPAV1677	12	1018	elem_2d	994	elem_2d	01020000A06A08000002000000B0C483C8AB142941967B8C3383AB5841000000A070F9804050F5DF5797142941187B8C8384AB584100000060B8FE8040	\N	t
1436	mesh_2d	LPAV1436	10	879	elem_2d	845	elem_2d	01020000A06A08000002000000506E148B32112941BC9D110F7AAB584100000060B87E814000BEA3CD53112941EA7E8C537AAB5841000000C01E7D8140	\N	t
1437	mesh_2d	LPAV1437	10	879	elem_2d	859	elem_2d	01020000A06A08000002000000506E148B32112941BC9D110F7AAB584100000060B87E814050320AB42E112941AEB0BF267EAB58410000008014928140	\N	t
1678	mesh_2d	LPAV1678	12	1016	elem_2d	994	elem_2d	01020000A06A08000002000000C0DD31909F1429416632AB3887AB584100000060B800814050F5DF5797142941187B8C8384AB584100000060B8FE8040	\N	t
1440	mesh_2d	LPAV1440	10	880	elem_2d	877	elem_2d	01020000A06A08000002000000A0BEF5851B112941748C638771AB5841000000E051688140B071148B2911294138B4BFB674AB5841000000608F748140	\N	t
1679	mesh_2d	LPAV1679	12	1005	elem_2d	994	elem_2d	01020000A06A08000002000000206F1DE27E142941E6E0F21986AB5841000000A07009814050F5DF5797142941187B8C8384AB584100000060B8FE8040	\N	t
1441	mesh_2d	LPAV1441	10	881	elem_2d	834	elem_2d	01020000A06A080000020000001073EB2E41112941F2AAE82272AB5841000000400A678140F0250AB44F1129410677B50F74AB5841000000C01E6D8140	\N	t
1449	mesh_2d	LPAV1449	10	883	elem_2d	853	elem_2d	01020000A06A0800000200000000FEF58572102941AA4A59207DAB5841000000C0F594814020207B71691029411A8963777AAB5841000000E0518E8140	\N	t
1680	mesh_2d	LPAV1680	12	1019	elem_2d	995	elem_2d	01020000A06A08000002000000B080CBA9861429416E5807088FAB5841000000E051168140B08B79F19F142941F66BB58F91AB5841000000C0CC108140	\N	t
1442	mesh_2d	LPAV1442	10	881	elem_2d	835	elem_2d	01020000A06A080000020000001073EB2E41112941F2AAE82272AB5841000000400A678140701A331053112941CC6307B870AB584100000020AE578140	\N	t
1681	mesh_2d	LPAV1681	12	1014	elem_2d	995	elem_2d	01020000A06A0800000200000090FF8D9FB214294196B4960A8FAB584100000080C20B8140B08B79F19F142941F66BB58F91AB5841000000C0CC108140	\N	t
1443	mesh_2d	LPAV1443	10	881	elem_2d	836	elem_2d	01020000A06A080000020000001073EB2E41112941F2AAE82272AB5841000000400A6781400045B87B33112941266EDE6B70AB584100000080C2638140	\N	t
1682	mesh_2d	LPAV1682	12	997	elem_2d	995	elem_2d	01020000A06A0800000200000020691DE28E14294112F0A04194AB5841000000A099198140B08B79F19F142941F66BB58F91AB5841000000C0CC108140	\N	t
1683	mesh_2d	LPAV1683	12	1035	elem_2d	996	elem_2d	01020000A06A08000002000000804B8E1FE813294108FAA0B179AB584100000040E1128140C0BFA2CDF9132941D28863377BAB584100000060B8108140	\N	t
1444	mesh_2d	LPAV1444	10	881	elem_2d	877	elem_2d	01020000A06A080000020000001073EB2E41112941F2AAE82272AB5841000000400A678140B071148B2911294138B4BFB674AB5841000000608F748140	\N	t
1450	mesh_2d	LPAV1450	10	883	elem_2d	825	elem_2d	01020000A06A0800000200000000FEF58572102941AA4A59207DAB5841000000C0F594814060B9EBAE8510294126A7E8427CAB584100000060668C8140	\N	t
1684	mesh_2d	LPAV1684	12	1026	elem_2d	996	elem_2d	01020000A06A08000002000000801432900D142941D04B59107AAB5841000000803D0C8140C0BFA2CDF9132941D28863377BAB584100000060B8108140	\N	t
1445	mesh_2d	LPAV1445	10	882	elem_2d	850	elem_2d	01020000A06A08000002000000D0BEEB2E77102941CC9F118F74AB5841000000E0517E814060DC99F65E102941C4E7F2C973AB5841000000E0A3828140	\N	t
1685	mesh_2d	LPAV1685	12	1017	elem_2d	997	elem_2d	01020000A06A08000002000000700CB7FB74142941B46BB53F92AB5841000000A07021814020691DE28E14294112F0A04194AB5841000000A099198140	\N	t
1446	mesh_2d	LPAV1446	10	882	elem_2d	876	elem_2d	01020000A06A08000002000000D0BEEB2E77102941CC9F118F74AB5841000000E0517E8140A0D899F668102941A09E11AF77AB5841000000400A8D8140	\N	t
1686	mesh_2d	LPAV1686	12	1012	elem_2d	997	elem_2d	01020000A06A08000002000000D07FA24DA414294162C6443296AB5841000000002914814020691DE28E14294112F0A04194AB5841000000A099198140	\N	t
1447	mesh_2d	LPAV1447	10	882	elem_2d	870	elem_2d	01020000A06A08000002000000D0BEEB2E77102941CC9F118F74AB5841000000E0517E814030700AB489102941046CDE1B76AB5841000000608F888140	\N	t
1448	mesh_2d	LPAV1448	10	883	elem_2d	826	elem_2d	01020000A06A0800000200000000FEF58572102941AA4A59207DAB5841000000C0F5948140C07533905F1029414221FD507EAB5841000000E051A48140	\N	t
1687	mesh_2d	LPAV1687	12	1033	elem_2d	998	elem_2d	01020000A06A0800000200000010BE27395014294164913AAB7FAB5841000000A04703814090C9FEDC4C142941840D4F597CAB5841000000C0CC008140	\N	t
1451	mesh_2d	LPAV1451	10	883	elem_2d	838	elem_2d	01020000A06A0800000200000000FEF58572102941AA4A59207DAB5841000000C0F5948140804CAEA47B102941F64959007FAB5841000000A099A98140	\N	t
1688	mesh_2d	LPAV1688	12	1030	elem_2d	999	elem_2d	01020000A06A0800000200000040483C67271429412E5C070885AB584100000020AE0F814000ADA2CD2B142941566FB58F88AB5841000000E0A3168140	\N	t
1452	mesh_2d	LPAV1452	10	884	elem_2d	863	elem_2d	01020000A06A08000002000000A062C252881129410C6ADE5B7BAB584100000060B8848140406999F691112941D641825C79AB58410000008014808140	\N	t
1689	mesh_2d	LPAV1689	12	1023	elem_2d	999	elem_2d	01020000A06A08000002000000E02CB77B1E14294116C06DAE8BAB5841000000803D20814000ADA2CD2B142941566FB58F88AB5841000000E0A3168140	\N	t
1453	mesh_2d	LPAV1453	10	884	elem_2d	875	elem_2d	01020000A06A08000002000000A062C252881129410C6ADE5B7BAB584100000060B8848140B051EB2E9A1129411037ABC87AAB584100000080147C8140	\N	t
1690	mesh_2d	LPAV1690	12	1008	elem_2d	999	elem_2d	01020000A06A08000002000000601D093411142941CC3C82CC86AB5841000000C01E17814000ADA2CD2B142941566FB58F88AB5841000000E0A3168140	\N	t
1454	mesh_2d	LPAV1454	10	885	elem_2d	853	elem_2d	01020000A06A08000002000000A0750A347B102941026BDECB78AB584100000040338B814020207B71691029411A8963777AAB5841000000E0518E8140	\N	t
1461	mesh_2d	LPAV1461	10	886	elem_2d	871	elem_2d	01020000A06A08000002000000403A66C386112941DCA8E8B277AB584100000000D77F8140C0DAFF5C741129412442828C78AB5841000000C0F5808140	\N	t
1691	mesh_2d	LPAV1691	12	1003	elem_2d	999	elem_2d	01020000A06A08000002000000B0316543481429412AB7962A88AB5841000000E0A312814000ADA2CD2B142941566FB58F88AB5841000000E0A3168140	\N	t
1455	mesh_2d	LPAV1455	10	885	elem_2d	876	elem_2d	01020000A06A08000002000000A0750A347B102941026BDECB78AB584100000040338B8140A0D899F668102941A09E11AF77AB5841000000400A8D8140	\N	t
1692	mesh_2d	LPAV1692	12	1019	elem_2d	1000	elem_2d	01020000A06A08000002000000B080CBA9861429416E5807088FAB5841000000E051168140204598F69C14294196074F298CAB5841000000A0700D8140	\N	t
1456	mesh_2d	LPAV1456	10	885	elem_2d	854	elem_2d	01020000A06A08000002000000A0750A347B102941026BDECB78AB584100000040338B8140601D52158C102941FA5530447AAB584100000000D7898140	\N	t
1693	mesh_2d	LPAV1693	12	1014	elem_2d	1000	elem_2d	01020000A06A0800000200000090FF8D9FB214294196B4960A8FAB584100000080C20B8140204598F69C14294196074F298CAB5841000000A0700D8140	\N	t
1694	mesh_2d	LPAV1694	12	1001	elem_2d	1000	elem_2d	01020000A06A08000002000000B0008E9FAF1429415827D46489AB5841000000803D008140204598F69C14294196074F298CAB5841000000A0700D8140	\N	t
1457	mesh_2d	LPAV1457	10	885	elem_2d	870	elem_2d	01020000A06A08000002000000A0750A347B102941026BDECB78AB584100000040338B814030700AB489102941046CDE1B76AB5841000000608F888140	\N	t
1462	mesh_2d	LPAV1462	10	887	elem_2d	858	elem_2d	01020000A06A080000020000008031A4CD1F102941F25430047DAB5841000000C01EA7814030C03DE73C102941F48763877DAB584100000080EBA38140	\N	t
1695	mesh_2d	LPAV1695	12	1016	elem_2d	1001	elem_2d	01020000A06A08000002000000C0DD31909F1429416632AB3887AB584100000060B8008140B0008E9FAF1429415827D46489AB5841000000803D008140	\N	t
1458	mesh_2d	LPAV1458	10	886	elem_2d	855	elem_2d	01020000A06A08000002000000403A66C386112941DCA8E8B277AB584100000000D77F814040D6FF5C80112941586CDE3B75AB58410000008014748140	\N	t
1459	mesh_2d	LPAV1459	10	886	elem_2d	863	elem_2d	01020000A06A08000002000000403A66C386112941DCA8E8B277AB584100000000D77F8140406999F691112941D641825C79AB58410000008014808140	\N	t
1696	mesh_2d	LPAV1696	12	1007	elem_2d	1001	elem_2d	01020000A06A08000002000000A0416F9AC1142941BEC06DEE89AB5841000000608F028140B0008E9FAF1429415827D46489AB5841000000803D008140	\N	t
1460	mesh_2d	LPAV1460	10	886	elem_2d	866	elem_2d	01020000A06A08000002000000403A66C386112941DCA8E8B277AB584100000000D77F814020298F1F9911294192F0C99D77AB584100000000D7738140	\N	t
1697	mesh_2d	LPAV1697	12	1031	elem_2d	1002	elem_2d	01020000A06A08000002000000A08BA2CD84142941BED71B3683AB5841000000A070038140C0D8834876142941B834AB0881AB5841000000C01E018140	\N	t
1463	mesh_2d	LPAV1463	10	888	elem_2d	825	elem_2d	01020000A06A08000002000000A04E8548911029413621FD707EAB584100000060B898814060B9EBAE8510294126A7E8427CAB584100000060668C8140	\N	t
1698	mesh_2d	LPAV1698	12	1009	elem_2d	1002	elem_2d	01020000A06A080000020000005048987694142941723F82BC7FAB5841000000E051FC8040C0D8834876142941B834AB0881AB5841000000C01E018140	\N	t
1464	mesh_2d	LPAV1464	10	888	elem_2d	838	elem_2d	01020000A06A08000002000000A04E8548911029413621FD707EAB584100000060B8988140804CAEA47B102941F64959007FAB5841000000A099A98140	\N	t
1699	mesh_2d	LPAV1699	12	1004	elem_2d	1002	elem_2d	01020000A06A0800000200000050C4FEDC5A1429416E1FFD3083AB584100000040E1068140C0D8834876142941B834AB0881AB5841000000C01E018140	\N	t
1472	mesh_2d	LPAV1472	10	891	elem_2d	852	elem_2d	01020000A06A08000002000000B09CE1572E102941501826DD7AAB584100000080EB958140B02EA44D271029413A19266D78AB5841000000205C918140	\N	t
1465	mesh_2d	LPAV1465	10	888	elem_2d	849	elem_2d	01020000A06A08000002000000A04E8548911029413621FD707EAB584100000060B8988140401D29B9A7102941841726FD7CAB5841000000E07A948140	\N	t
1700	mesh_2d	LPAV1700	12	1032	elem_2d	1003	elem_2d	01020000A06A08000002000000C04FC1526514294114CC440287AB584100000020AE0F8140B0316543481429412AB7962A88AB5841000000E0A3128140	\N	t
1466	mesh_2d	LPAV1466	10	889	elem_2d	849	elem_2d	01020000A06A08000002000000008666C3BC102941D23F82BC7EAB584100000060B89C8140401D29B9A7102941841726FD7CAB5841000000E07A948140	\N	t
1701	mesh_2d	LPAV1701	12	1032	elem_2d	1004	elem_2d	01020000A06A08000002000000C04FC1526514294114CC440287AB584100000020AE0F814050C4FEDC5A1429416E1FFD3083AB584100000040E1068140	\N	t
1467	mesh_2d	LPAV1467	10	889	elem_2d	869	elem_2d	01020000A06A08000002000000008666C3BC102941D23F82BC7EAB584100000060B89C814040FD7A71C61029416074B51F7BAB5841000000A0478D8140	\N	t
1702	mesh_2d	LPAV1702	12	1033	elem_2d	1004	elem_2d	01020000A06A0800000200000010BE27395014294164913AAB7FAB5841000000A04703814050C4FEDC5A1429416E1FFD3083AB584100000040E1068140	\N	t
1473	mesh_2d	LPAV1473	10	891	elem_2d	858	elem_2d	01020000A06A08000002000000B09CE1572E102941501826DD7AAB584100000080EB95814030C03DE73C102941F48763877DAB584100000080EBA38140	\N	t
1468	mesh_2d	LPAV1468	10	889	elem_2d	832	elem_2d	01020000A06A08000002000000008666C3BC102941D23F82BC7EAB584100000060B89C8140E014005DD9102941C6C46D2E7FAB584100000080149E8140	\N	t
1703	mesh_2d	LPAV1703	12	1031	elem_2d	1005	elem_2d	01020000A06A08000002000000A08BA2CD84142941BED71B3683AB5841000000A070038140206F1DE27E142941E6E0F21986AB5841000000A070098140	\N	t
1469	mesh_2d	LPAV1469	10	890	elem_2d	857	elem_2d	01020000A06A080000020000008047140B9A1129416C923AEB7CAB584100000000D783814020E0ADA49C112941B4BA96BA7EAB5841000000A099878140	\N	t
1704	mesh_2d	LPAV1704	12	1032	elem_2d	1005	elem_2d	01020000A06A08000002000000C04FC1526514294114CC440287AB584100000020AE0F8140206F1DE27E142941E6E0F21986AB5841000000A070098140	\N	t
1470	mesh_2d	LPAV1470	10	890	elem_2d	875	elem_2d	01020000A06A080000020000008047140B9A1129416C923AEB7CAB584100000000D7838140B051EB2E9A1129411037ABC87AAB584100000080147C8140	\N	t
1705	mesh_2d	LPAV1705	12	1024	elem_2d	1006	elem_2d	01020000A06A08000002000000B06C138BE11329416A1326ED87AB5841000000002922814040EFFE5CE8132941F2CC44B284AB5841000000A0701B8140	\N	t
1471	mesh_2d	LPAV1471	10	891	elem_2d	842	elem_2d	01020000A06A08000002000000B09CE1572E102941501826DD7AAB584100000080EB95814000C7140B46102941F89D116F79AB5841000000A0478F8140	\N	t
1474	mesh_2d	LPAV1474	10	891	elem_2d	887	elem_2d	01020000A06A08000002000000B09CE1572E102941501826DD7AAB584100000080EB9581408031A4CD1F102941F25430047DAB5841000000C01EA78140	\N	t
1706	mesh_2d	LPAV1706	12	1037	elem_2d	1007	elem_2d	01020000A06A0800000200000070D708B4CB142941B05807588EAB5841000000608F028140A0416F9AC1142941BEC06DEE89AB5841000000608F028140	\N	t
1475	mesh_2d	LPAV1475	10	892	elem_2d	864	elem_2d	01020000A06A08000002000000301AD780E6102941985730F475AB584100000020AE7B8140D0570AB4CA102941309F112F76AB5841000000E07A7E8140	\N	t
1707	mesh_2d	LPAV1707	12	1038	elem_2d	1007	elem_2d	01020000A06A0800000200000030DCDF57DA14294170ACBF7689AB5841000000803DF68040A0416F9AC1142941BEC06DEE89AB5841000000608F028140	\N	t
1483	mesh_2d	LPAV1483	10	894	elem_2d	823	elem_2d	01020000A06A08000002000000E021E1D775112941F82FD46472AB5841000000400A5F81404023B87B8D1129419A4E59A072AB5841000000608F568140	\N	t
1476	mesh_2d	LPAV1476	10	892	elem_2d	827	elem_2d	01020000A06A08000002000000301AD780E6102941985730F475AB584100000020AE7B814020C9F585FF102941A21A26AD74AB5841000000E0517A8140	\N	t
1708	mesh_2d	LPAV1708	12	1039	elem_2d	1007	elem_2d	01020000A06A08000002000000C08F27B9CB142941465C07C884AB58410000008014008140A0416F9AC1142941BEC06DEE89AB5841000000608F028140	\N	t
1477	mesh_2d	LPAV1477	10	892	elem_2d	856	elem_2d	01020000A06A08000002000000301AD780E6102941985730F475AB584100000020AE7B8140C01EAEA4F5102941D037ABC878AB584100000080EB858140	\N	t
1709	mesh_2d	LPAV1709	12	1030	elem_2d	1008	elem_2d	01020000A06A0800000200000040483C67271429412E5C070885AB584100000020AE0F8140601D093411142941CC3C82CC86AB5841000000C01E178140	\N	t
1710	mesh_2d	LPAV1710	12	1028	elem_2d	1009	elem_2d	01020000A06A080000020000006052463EB01429410A913A9B80AB58410000008014FA80405048987694142941723F82BC7FAB5841000000E051FC8040	\N	t
1478	mesh_2d	LPAV1478	10	892	elem_2d	868	elem_2d	01020000A06A08000002000000301AD780E6102941985730F475AB584100000020AE7B8140D097EB2EDF102941CAD3447272AB5841000000E07A6E8140	\N	t
1484	mesh_2d	LPAV1484	10	894	elem_2d	846	elem_2d	01020000A06A08000002000000E021E1D775112941F82FD46472AB5841000000400A5F8140609A1EE260112941366DDEEB72AB584100000060665E8140	\N	t
1711	mesh_2d	LPAV1711	12	1031	elem_2d	1009	elem_2d	01020000A06A08000002000000A08BA2CD84142941BED71B3683AB5841000000A0700381405048987694142941723F82BC7FAB5841000000E051FC8040	\N	t
1479	mesh_2d	LPAV1479	10	893	elem_2d	869	elem_2d	01020000A06A08000002000000F0C71E62E7102941480378557CAB584100000040E192814040FD7A71C61029416074B51F7BAB5841000000A0478D8140	\N	t
1712	mesh_2d	LPAV1712	12	1027	elem_2d	1010	elem_2d	01020000A06A08000002000000C06EA2CDD1142941504B30B496AB58410000004033058140A09127B9C614294110BD6DBE93AB5841000000E051088140	\N	t
1480	mesh_2d	LPAV1480	10	893	elem_2d	832	elem_2d	01020000A06A08000002000000F0C71E62E7102941480378557CAB584100000040E1928140E014005DD9102941C6C46D2E7FAB584100000080149E8140	\N	t
1713	mesh_2d	LPAV1713	12	1021	elem_2d	1010	elem_2d	01020000A06A08000002000000209AFE5CCB142941F66BB58F91AB584100000060B8048140A09127B9C614294110BD6DBE93AB5841000000E051088140	\N	t
1481	mesh_2d	LPAV1481	10	893	elem_2d	847	elem_2d	01020000A06A08000002000000F0C71E62E7102941480378557CAB584100000040E1928140B00200DD09112941C6B0BFE67DAB5841000000C01E998140	\N	t
1485	mesh_2d	LPAV1485	10	894	elem_2d	855	elem_2d	01020000A06A08000002000000E021E1D775112941F82FD46472AB5841000000400A5F814040D6FF5C80112941586CDE3B75AB58410000008014748140	\N	t
1714	mesh_2d	LPAV1714	12	1027	elem_2d	1011	elem_2d	01020000A06A08000002000000C06EA2CDD1142941504B30B496AB58410000004033058140C0DE0834B8142941887E63A796AB584100000080140A8140	\N	t
1482	mesh_2d	LPAV1482	10	893	elem_2d	856	elem_2d	01020000A06A08000002000000F0C71E62E7102941480378557CAB584100000040E1928140C01EAEA4F5102941D037ABC878AB584100000080EB858140	\N	t
1715	mesh_2d	LPAV1715	12	1025	elem_2d	1011	elem_2d	01020000A06A08000002000000E00165C3C7142941D25407A898AB584100000040E10A8140C0DE0834B8142941887E63A796AB584100000080140A8140	\N	t
1486	mesh_2d	LPAV1486	10	895	elem_2d	827	elem_2d	01020000A06A08000002000000706D6643FE1029410C59301472AB584100000060666C814020C9F585FF102941A21A26AD74AB5841000000E0517A8140	\N	t
1716	mesh_2d	LPAV1716	12	1012	elem_2d	1011	elem_2d	01020000A06A08000002000000D07FA24DA414294162C6443296AB58410000000029148140C0DE0834B8142941887E63A796AB584100000080140A8140	\N	t
1487	mesh_2d	LPAV1487	10	895	elem_2d	868	elem_2d	01020000A06A08000002000000706D6643FE1029410C59301472AB584100000060666C8140D097EB2EDF102941CAD3447272AB5841000000E07A6E8140	\N	t
1717	mesh_2d	LPAV1717	12	1032	elem_2d	1013	elem_2d	01020000A06A08000002000000C04FC1526514294114CC440287AB584100000020AE0F814090741D62701429415ADFF2398AAB5841000000A070138140	\N	t
1488	mesh_2d	LPAV1488	10	895	elem_2d	880	elem_2d	01020000A06A08000002000000706D6643FE1029410C59301472AB584100000060666C8140A0BEF5851B112941748C638771AB5841000000E051688140	\N	t
1495	mesh_2d	LPAV1495	10	898	elem_2d	844	elem_2d	01020000A06A08000002000000E0BE511588112941C035AB487EAB584100000060B88C814080D128B9711129411CF8A0D17EAB58410000002085878140	\N	t
1718	mesh_2d	LPAV1718	12	1034	elem_2d	1013	elem_2d	01020000A06A08000002000000E0F031906C142941D825D4648DAB584100000040E11A814090741D62701429415ADFF2398AAB5841000000A070138140	\N	t
1489	mesh_2d	LPAV1489	10	896	elem_2d	837	elem_2d	01020000A06A0800000200000010F85B6C9311294102E3F27980AB5841000000C01E918140A06399F6A01129414AEDC95D80AB5841000000A0477F8140	\N	t
1719	mesh_2d	LPAV1719	12	1037	elem_2d	1014	elem_2d	01020000A06A0800000200000070D708B4CB142941B05807588EAB5841000000608F02814090FF8D9FB214294196B4960A8FAB584100000080C20B8140	\N	t
1490	mesh_2d	LPAV1490	10	896	elem_2d	857	elem_2d	01020000A06A0800000200000010F85B6C9311294102E3F27980AB5841000000C01E91814020E0ADA49C112941B4BA96BA7EAB5841000000A099878140	\N	t
1720	mesh_2d	LPAV1720	12	1021	elem_2d	1014	elem_2d	01020000A06A08000002000000209AFE5CCB142941F66BB58F91AB584100000060B804814090FF8D9FB214294196B4960A8FAB584100000080C20B8140	\N	t
1491	mesh_2d	LPAV1491	10	897	elem_2d	829	elem_2d	01020000A06A0800000200000070BB7AF1751129412618264D7BAB58410000004033858140C01C0A3468112941A2EFC91D7AAB5841000000608F828140	\N	t
1721	mesh_2d	LPAV1721	12	1028	elem_2d	1015	elem_2d	01020000A06A080000020000006052463EB01429410A913A9B80AB58410000008014FA8040D03E6F1AC9142941AA903A9B81AB5841000000E0A3008140	\N	t
1496	mesh_2d	LPAV1496	10	898	elem_2d	884	elem_2d	01020000A06A08000002000000E0BE511588112941C035AB487EAB584100000060B88C8140A062C252881129410C6ADE5B7BAB584100000060B8848140	\N	t
1492	mesh_2d	LPAV1492	10	897	elem_2d	844	elem_2d	01020000A06A0800000200000070BB7AF1751129412618264D7BAB5841000000403385814080D128B9711129411CF8A0D17EAB58410000002085878140	\N	t
1722	mesh_2d	LPAV1722	12	1039	elem_2d	1015	elem_2d	01020000A06A08000002000000C08F27B9CB142941465C07C884AB58410000008014008140D03E6F1AC9142941AA903A9B81AB5841000000E0A3008140	\N	t
1493	mesh_2d	LPAV1493	10	897	elem_2d	871	elem_2d	01020000A06A0800000200000070BB7AF1751129412618264D7BAB58410000004033858140C0DAFF5C741129412442828C78AB5841000000C0F5808140	\N	t
1723	mesh_2d	LPAV1723	12	1018	elem_2d	1015	elem_2d	01020000A06A08000002000000B0C483C8AB142941967B8C3383AB5841000000A070F98040D03E6F1AC9142941AA903A9B81AB5841000000E0A3008140	\N	t
1494	mesh_2d	LPAV1494	10	897	elem_2d	884	elem_2d	01020000A06A0800000200000070BB7AF1751129412618264D7BAB58410000004033858140A062C252881129410C6ADE5B7BAB584100000060B8848140	\N	t
1724	mesh_2d	LPAV1724	12	1020	elem_2d	1017	elem_2d	01020000A06A08000002000000209CA2CD581429415210262D90AB5841000000400A258140700CB7FB74142941B46BB53F92AB5841000000A070218140	\N	t
1497	mesh_2d	LPAV1497	10	898	elem_2d	890	elem_2d	01020000A06A08000002000000E0BE511588112941C035AB487EAB584100000060B88C81408047140B9A1129416C923AEB7CAB584100000000D7838140	\N	t
1725	mesh_2d	LPAV1725	12	1019	elem_2d	1017	elem_2d	01020000A06A08000002000000B080CBA9861429416E5807088FAB5841000000E051168140700CB7FB74142941B46BB53F92AB5841000000A070218140	\N	t
1498	mesh_2d	LPAV1498	10	898	elem_2d	896	elem_2d	01020000A06A08000002000000E0BE511588112941C035AB487EAB584100000060B88C814010F85B6C9311294102E3F27980AB5841000000C01E918140	\N	t
1726	mesh_2d	LPAV1726	12	1028	elem_2d	1018	elem_2d	01020000A06A080000020000006052463EB01429410A913A9B80AB58410000008014FA8040B0C483C8AB142941967B8C3383AB5841000000A070F98040	\N	t
1505	overflow	OFL1505	10	903	river	852	elem_2d	01020000A06A08000003000000E29DACD918102941BE808C786EAB5841000000C01E7B8140F8845CAC1B1029415075B59F78AB58417B14AE47E1A28140D459007D2110294145C76D8678AB58410000000000000000	\N	t
1727	mesh_2d	LPAV1727	12	1034	elem_2d	1019	elem_2d	01020000A06A08000002000000E0F031906C142941D825D4648DAB584100000040E11A8140B080CBA9861429416E5807088FAB5841000000E051168140	\N	t
1499	mesh_2d	LPAV1499	10	899	elem_2d	830	elem_2d	01020000A06A0800000200000080ACC252C31029416C77B5FF72AB584100000040E1728140C0BB99F6B510294152DD1B5674AB5841000000C0F57A8140	\N	t
1728	mesh_2d	LPAV1728	12	1034	elem_2d	1020	elem_2d	01020000A06A08000002000000E0F031906C142941D825D4648DAB584100000040E11A8140209CA2CD581429415210262D90AB5841000000400A258140	\N	t
1500	mesh_2d	LPAV1500	10	899	elem_2d	864	elem_2d	01020000A06A0800000200000080ACC252C31029416C77B5FF72AB584100000040E1728140D0570AB4CA102941309F112F76AB5841000000E07A7E8140	\N	t
1729	mesh_2d	LPAV1729	12	1037	elem_2d	1021	elem_2d	01020000A06A0800000200000070D708B4CB142941B05807588EAB5841000000608F028140209AFE5CCB142941F66BB58F91AB584100000060B8048140	\N	t
1501	mesh_2d	LPAV1501	10	899	elem_2d	868	elem_2d	01020000A06A0800000200000080ACC252C31029416C77B5FF72AB584100000040E1728140D097EB2EDF102941CAD3447272AB5841000000E07A6E8140	\N	t
1506	overflow	OFL1506	10	904	river	861	elem_2d	01020000A06A0800000300000047132C7D4910294111FDDF1E6BAB584100000080EB7F8140E0C5140B49102941D9C96DA671AB58415C8FC2E551868140186A850848102941F058306072AB58410000000000000000	\N	t
1730	mesh_2d	LPAV1730	12	1029	elem_2d	1022	elem_2d	01020000A06A08000002000000C0BD79711A142941567C8C3381AB5841000000608F0A8140E0766F9A33142941280C4FF97FAB5841000000205C058140	\N	t
1502	overflow	OFL1502	10	900	river	839	elem_2d	01020000A06A0800000300000096E40A9931102941DA42790D6CAB5841000000A0997B8140383F52D531102941D425FD2072AB5841333333AB478C814094900A5433102941C4D3448272AB58410000000000000000	\N	t
1731	mesh_2d	LPAV1731	12	1033	elem_2d	1022	elem_2d	01020000A06A0800000200000010BE27395014294164913AAB7FAB5841000000A047038140E0766F9A33142941280C4FF97FAB5841000000205C058140	\N	t
1503	overflow	OFL1503	10	901	river	841	elem_2d	01020000A06A080000030000009D1D6A87221029416F50A8876DAB58410000008014768140F05800DD231029414D39ABD074AB584185EB510857928140B86BAE6428102941A49F11FB74AB58410000000000000000	\N	t
1732	mesh_2d	LPAV1732	12	1027	elem_2d	1025	elem_2d	01020000A06A08000002000000C06EA2CDD1142941504B30B496AB58410000004033058140E00165C3C7142941D25407A898AB584100000040E10A8140	\N	t
1504	overflow	OFL1504	10	902	river	850	elem_2d	01020000A06A08000003000000E047D10564102941E24A13A96EAB5841000000208575814098DA99B663102941DCD3444272AB58410AD7A3703D7E81407CDB995661102941D0DD1B0673AB58410000000000000000	\N	t
1733	mesh_2d	LPAV1733	12	1030	elem_2d	1029	elem_2d	01020000A06A0800000200000040483C67271429412E5C070885AB584100000020AE0F8140C0BD79711A142941567C8C3381AB5841000000608F0A8140	\N	t
1507	overflow	OFL1507	10	905	river	887	elem_2d	01020000A06A0800000300000038799F8B0E102941FDA706796FAB584100000060B882814000B1B87B1310294153B1BF6E7CAB58417B14AE8FC2A581404071AEA419102941220378B97CAB58410000000000000000	\N	t
1734	mesh_2d	LPAV1734	12	1039	elem_2d	1038	elem_2d	01020000A06A08000002000000C08F27B9CB142941465C07C884AB5841000000801400814030DCDF57DA14294170ACBF7689AB5841000000803DF68040	\N	t
1735	mesh_2d	LPAV1735	12	1038	elem_2d	1037	elem_2d	01020000A06A0800000200000030DCDF57DA14294170ACBF7689AB5841000000803DF6804070D708B4CB142941B05807588EAB5841000000608F028140	\N	t
1508	overflow	OFL1508	10	906	river	823	elem_2d	01020000A06A08000003000000497C756E91112941B2A9EA2B70AB5841000000208551814060881EE29011294134B5BF1672AB584148E17A14AE528140D055EB2E8F112941E7818C5B72AB58410000000000000000	\N	t
1514	overflow	OFL1514	10	912	river	895	elem_2d	01020000A06A08000003000000854C8AD2FA102941342FBF136DAB5841000000E05176814028A29936FA102941A7828C5B70AB584114AE47E17A5C8140CC07003DFC102941DA6DDE3771AB58410000000000000000	\N	t
1736	mesh_2d	LPAV1736	12	1036	elem_2d	1035	elem_2d	01020000A06A080000020000000042E0D7CA13294130B0BF767FAB584100000000291A8140804B8E1FE813294108FAA0B179AB584100000040E1128140	\N	t
1509	overflow	OFL1509	10	907	river	835	elem_2d	01020000A06A08000003000000BB77E08C5411294111C62A476DAB5841000000803D5C8140E00F5CEC5311294102DF1BD66FAB5841D7A3701D855481402895477E5311294167A1114770AB58410000000000000000	\N	t
1737	overflow	OFL1737	12	1040	river	1028	elem_2d	01020000A06A08000003000000C40766C3BB142941C04D255B78AB5841000000400AE58040E8C95A2CB91429411902787D7FAB584185EB51B81EFD6040248E50B5B41429419249590C80AB58410000000000000000	\N	t
1510	overflow	OFL1510	10	908	river	836	elem_2d	01020000A06A0800000300000095281E77361129415FCC11816CAB5841000000E0A3688140186D14CB351129411908787D6FAB58410AD7A390C25581400C5966A334112941203BABF46FAB58410000000000000000	\N	t
1738	overflow	OFL1738	12	1041	river	1035	elem_2d	01020000A06A080000030000009B1EF969F51329415E7D45F872AB5841000000400AF780402069130BEB13294180DC1B8676AB58411F85EB9959FF804050DA5095E9132941446BDE1B78AB58410000000000000000	\N	t
1511	overflow	OFL1511	10	909	river	868	elem_2d	01020000A06A080000030000009E25CCDDDB102941478903256EAB584100000040E17681409828AE64DB1029414AFDA00171AB5841B2E4174B7E61814034E0CC49DD1029418AE8F2B971AB58410000000000000000	\N	t
1739	overflow	OFL1739	12	7	river	1036	elem_2d	01020000A06A08000003000000DD9909FEDC1329412F17F17D73AB5841B81E85EB51F48040E8BFF4C5C21329414E9C11DF7DAB58416666666666168140F480EACEC61329413FA6E8AA7EAB58410000000000000000	\N	t
1515	overflow	OFL1515	10	913	river	860	elem_2d	01020000A06A08000003000000459A1EFC9B1029416501130C70AB5841000000400A6D8140A89C3DA79B1029415C1B26BD72AB5841AE47E1BA1E6F81400402A46D9E1029416C818CA373AB58410000000000000000	\N	t
1512	overflow	OFL1512	10	910	river	880	elem_2d	01020000A06A0800000300000076EED6A618112941894D41C46CAB5841000000C0F56A8140A007D70018112941604582EC6FAB584152B81E9D995A8140206366C319112941EAE8F2B970AB58410000000000000000	\N	t
1740	overflow	OFL1740	12	1038	elem_2d	2	river	01020000A06A08000003000000D469A2EDDE1429416CD51B6689AB5841000000000000000078F76483E314294168FE775589AB5841333333D323F78040D6A71C53FF1429415CE6748D7BAB58415C8FC2F528CA8040	\N	t
1513	overflow	OFL1513	10	911	river	894	elem_2d	01020000A06A080000030000005F1D44F5721129418F1F77DF6EAB584100000060665A8140204C3D67721129411BCA6DF670AB58410AD7A3703D53814000378F1F741129410AFDA0AD71AB58410000000000000000	\N	t
1741	overflow	OFL1741	12	1043	river	1026	elem_2d	01020000A06A0800000300000091B44F8D1814294138D4A55772AB5841000000C0F5EA8040A001844809142941340F4FD977AB5841295C8FC275068140100B5B6C0B142941822DD4F478AB58410000000000000000	\N	t
1516	overflow	OFL1516	10	914	river	882	elem_2d	01020000A06A08000003000000595007AD7E1029416501130C70AB584100000040E174814050EF1E627E102941DFDD1BDE72AB58414B7EB1BCBB758140105785C87A102941D6BE96B673AB58410000000000000000	\N	t
1524	mesh_2d	LPAV1524	11	949	elem_2d	923	elem_2d	01020000A06A08000002000000408651151F122941708A63E776AB58410000000000648140C0A2D600251229410A44827C73AB584100000060664E8140	\N	t
1742	overflow	OFL1742	12	1044	river	1015	elem_2d	01020000A06A0800000300000080C7A34CD814294160DBA1DB79AB5841000000205CE18040706DA24DD51429413F3F824480AB584148E17A14AE7B794020D60834CF142941F467DEEF80AB58410000000000000000	\N	t
1517	overflow	OFL1517	10	915	river	899	elem_2d	01020000A06A0800000300000093DB46E8BB1029412EFAE0356FAB584100000060666A8140A0E2F585BB102941538C63DF71AB58413096FC824E68814090475C6CBF102941E0818C6F72AB58410000000000000000	\N	t
1743	overflow	OFL1743	12	1045	river	1009	elem_2d	01020000A06A08000003000000E48032109D142941DD19DEF376AB584100000080EBDF8040801365C398142941279C11477EAB58410AD7A3703D065140E8ADFE9C96142941CCEDC9017FAB58410000000000000000	\N	t
1518	mesh_2d	LPAV1518	11	918	elem_2d	916	elem_2d	01020000A06A0800000200000050E03210EE112941C04282EC76AB5841000000208563814050A12839F2112941FADA1B967AAB5841000000400A678140	\N	t
1525	mesh_2d	LPAV1525	11	950	elem_2d	917	elem_2d	01020000A06A0800000200000010DC09B414122941B6F7A0E17FAB5841000000C01E7F81402036709A35122941E67C8CB37FAB5841000000608F648140	\N	t
1744	overflow	OFL1744	12	1046	river	998	elem_2d	01020000A06A0800000300000058555AC35814294169ECF2A175AB5841000000C01EEB804090F15A6C4F142941AB7E8CFB7AAB5841CCCCCCCCCC47754090DDAC244E14294118C66DAA7BAB58410000000000000000	\N	t
1519	mesh_2d	LPAV1519	11	919	elem_2d	916	elem_2d	01020000A06A08000002000000504F47BE0D12294172DB1B5679AB584100000040E164814050A12839F2112941FADA1B967AAB5841000000400A678140	\N	t
1745	overflow	OFL1745	12	1047	river	992	elem_2d	01020000A06A080000030000002E4EC1CA451529415746334782AB584100000080C2BF8040185BCBE9EA142941A3D11B7E93AB58417B14AE4761018140240B13ABE5142941384259A493AB58410000000000000000	\N	t
1520	mesh_2d	LPAV1520	11	924	elem_2d	916	elem_2d	01020000A06A08000002000000D03AEB2ED7112941B269DE4B7CAB584100000060666E814050A12839F2112941FADA1B967AAB5841000000400A678140	\N	t
1746	overflow	OFL1746	12	16	river	989	elem_2d	01020000A06A0800000300000077A04E515C152941A9C134A684AB5841713D0AD7A3BE8040D0B15A6CF9142941684B307496AB5841666666EED10481409810EA6EF2142941F9BB6DA696AB58410000000000000000	\N	t
1747	overflow	OFL1747	12	1048	river	985	elem_2d	01020000A06A08000003000000F5CAB40E7C14294171A1FAB975AB5841000000C0CCE280408802E01774142941690D4FA17CAB58418FC2F5285C896940E4741D826F142941145F074C7DAB58410000000000000000	\N	t
1521	mesh_2d	LPAV1521	11	924	elem_2d	922	elem_2d	01020000A06A08000002000000D03AEB2ED7112941B269DE4B7CAB584100000060666E8140C0B9FF5CCC112941EC22FDE079AB5841000000C0CC608140	\N	t
1526	mesh_2d	LPAV1526	11	950	elem_2d	930	elem_2d	01020000A06A0800000200000010DC09B414122941B6F7A0E17FAB5841000000C01E7F81401074CCA9FD1129418A2BD4347EAB584100000080EB798140	\N	t
1522	mesh_2d	LPAV1522	11	949	elem_2d	919	elem_2d	01020000A06A08000002000000408651151F122941708A63E776AB58410000000000648140504F47BE0D12294172DB1B5679AB584100000040E1648140	\N	t
1523	mesh_2d	LPAV1523	11	949	elem_2d	920	elem_2d	01020000A06A08000002000000408651151F122941708A63E776AB5841000000000064814080E3B77B37122941DA04782578AB58410000000029648140	\N	t
1527	mesh_2d	LPAV1527	11	951	elem_2d	944	elem_2d	01020000A06A08000002000000D0C4FFDCAE112941B68663D780AB5841000000400A77814090E084C8B6112941F8F7A0317FAB584100000000D76F8140	\N	t
1528	mesh_2d	LPAV1528	11	951	elem_2d	946	elem_2d	01020000A06A08000002000000D0C4FFDCAE112941B68663D780AB5841000000400A7781405003E157C7112941C4AFBF9680AB5841000000803D768140	\N	t
1748	overflow	OFL1748	12	1049	river	980	elem_2d	01020000A06A080000030000009A2972A537142941469E30E573AB584100000020AEE78040708446BE2A142941EDEFC95579AB5841AE47E1DAFAEB7F40505BEA2E2B1429415A933A6F7AAB58410000000000000000	\N	t
1529	mesh_2d	LPAV1529	11	952	elem_2d	931	elem_2d	01020000A06A08000002000000007328B96D1229411EE5F2D97AAB5841000000C0CC6081404049F5855412294142D1443279AB584100000060B8628140	\N	t
1537	mesh_2d	LPAV1537	11	929	elem_2d	924	elem_2d	01020000A06A08000002000000A073F585E31129413A0C4FC97FAB5841000000C0F57E8140D03AEB2ED7112941B269DE4B7CAB584100000060666E8140	\N	t
1530	mesh_2d	LPAV1530	11	952	elem_2d	933	elem_2d	01020000A06A08000002000000007328B96D1229411EE5F2D97AAB5841000000C0CC608140601D709A771229413EDC1B3677AB5841000000A070458140	\N	t
1531	mesh_2d	LPAV1531	11	926	elem_2d	920	elem_2d	01020000A06A0800000200000090DFB7FB41122941FAFBA08174AB5841000000E05148814080E3B77B37122941DA04782578AB58410000000029648140	\N	t
1532	mesh_2d	LPAV1532	11	926	elem_2d	923	elem_2d	01020000A06A0800000200000090DFB7FB41122941FAFBA08174AB5841000000E051488140C0A2D600251229410A44827C73AB584100000060664E8140	\N	t
1538	mesh_2d	LPAV1538	11	930	elem_2d	916	elem_2d	01020000A06A080000020000001074CCA9FD1129418A2BD4347EAB584100000080EB79814050A12839F2112941FADA1B967AAB5841000000400A678140	\N	t
1533	mesh_2d	LPAV1533	11	926	elem_2d	925	elem_2d	01020000A06A0800000200000090DFB7FB41122941FAFBA08174AB5841000000E05148814000EA65C35C12294138BE965A75AB584100000060B8468140	\N	t
1534	mesh_2d	LPAV1534	11	927	elem_2d	918	elem_2d	01020000A06A08000002000000F03BEB2ED4112941E075B51F77AB5841000000403363814050E03210EE112941C04282EC76AB58410000002085638140	\N	t
1535	mesh_2d	LPAV1535	11	927	elem_2d	921	elem_2d	01020000A06A08000002000000F03BEB2ED4112941E075B51F77AB5841000000403363814080C1D600D3112941D8104F7973AB5841000000A099518140	\N	t
1536	mesh_2d	LPAV1536	11	927	elem_2d	922	elem_2d	01020000A06A08000002000000F03BEB2ED4112941E075B51F77AB58410000004033638140C0B9FF5CCC112941EC22FDE079AB5841000000C0CC608140	\N	t
1539	mesh_2d	LPAV1539	11	930	elem_2d	929	elem_2d	01020000A06A080000020000001074CCA9FD1129418A2BD4347EAB584100000080EB798140A073F585E31129413A0C4FC97FAB5841000000C0F57E8140	\N	t
1540	mesh_2d	LPAV1540	11	931	elem_2d	920	elem_2d	01020000A06A080000020000004049F5855412294142D1443279AB584100000060B862814080E3B77B37122941DA04782578AB58410000000029648140	\N	t
1541	mesh_2d	LPAV1541	11	931	elem_2d	925	elem_2d	01020000A06A080000020000004049F5855412294142D1443279AB584100000060B862814000EA65C35C12294138BE965A75AB584100000060B8468140	\N	t
1548	mesh_2d	LPAV1548	11	939	elem_2d	917	elem_2d	01020000A06A0800000200000090F9654333122941547E8CE37BAB5841000000E0A36481402036709A35122941E67C8CB37FAB5841000000608F648140	\N	t
1542	mesh_2d	LPAV1542	11	932	elem_2d	931	elem_2d	01020000A06A0800000200000060A784484F122941A2923A5B7CAB5841000000803D6481404049F5855412294142D1443279AB584100000060B8628140	\N	t
1543	mesh_2d	LPAV1543	11	933	elem_2d	925	elem_2d	01020000A06A08000002000000601D709A771229413EDC1B3677AB5841000000A07045814000EA65C35C12294138BE965A75AB584100000060B8468140	\N	t
1544	mesh_2d	LPAV1544	11	935	elem_2d	928	elem_2d	01020000A06A080000020000003008701AB0122941BAB0BF067EAB584100000040E1528140506AD680BB12294100DB1B867AAB58410000008014448140	\N	t
1549	mesh_2d	LPAV1549	11	939	elem_2d	920	elem_2d	01020000A06A0800000200000090F9654333122941547E8CE37BAB5841000000E0A364814080E3B77B37122941DA04782578AB58410000000029648140	\N	t
1545	mesh_2d	LPAV1545	11	937	elem_2d	921	elem_2d	01020000A06A08000002000000F044EB2EBC11294186BE968A74AB5841000000000066814080C1D600D3112941D8104F7973AB5841000000A099518140	\N	t
1546	mesh_2d	LPAV1546	11	938	elem_2d	918	elem_2d	01020000A06A0800000200000040B6D600F11129419AD344F272AB5841000000803D4E814050E03210EE112941C04282EC76AB58410000002085638140	\N	t
1547	mesh_2d	LPAV1547	11	938	elem_2d	921	elem_2d	01020000A06A0800000200000040B6D600F11129419AD344F272AB5841000000803D4E814080C1D600D3112941D8104F7973AB5841000000A099518140	\N	t
1550	mesh_2d	LPAV1550	11	939	elem_2d	932	elem_2d	01020000A06A0800000200000090F9654333122941547E8CE37BAB5841000000E0A364814060A784484F122941A2923A5B7CAB5841000000803D648140	\N	t
1551	mesh_2d	LPAV1551	11	940	elem_2d	934	elem_2d	01020000A06A08000002000000309DA34DAB1129412022FD007CAB5841000000A047698140C02C66C3AA1129411E5630E479AB5841000000C0CC688140	\N	t
1552	mesh_2d	LPAV1552	11	940	elem_2d	936	elem_2d	01020000A06A08000002000000309DA34DAB1129412022FD007CAB5841000000A047698140A00DE1D7AB1129411A4082FC7DAB5841000000C0CC708140	\N	t
1560	mesh_2d	LPAV1560	11	957	elem_2d	937	elem_2d	01020000A06A080000020000001007E157BD112941C6C76D2E77AB58410000004033638140F044EB2EBC11294186BE968A74AB58410000000000668140	\N	t
1553	mesh_2d	LPAV1553	11	941	elem_2d	934	elem_2d	01020000A06A08000002000000B0D0D680AA112941400F4FB977AB584100000000D7658140C02C66C3AA1129411E5630E479AB5841000000C0CC688140	\N	t
1554	mesh_2d	LPAV1554	11	942	elem_2d	917	elem_2d	01020000A06A08000002000000E0E18E1F571229417C1626BD7FAB5841000000205C6381402036709A35122941E67C8CB37FAB5841000000608F648140	\N	t
1555	mesh_2d	LPAV1555	11	942	elem_2d	932	elem_2d	01020000A06A08000002000000E0E18E1F571229417C1626BD7FAB5841000000205C63814060A784484F122941A2923A5B7CAB5841000000803D648140	\N	t
1561	mesh_2d	LPAV1561	11	957	elem_2d	941	elem_2d	01020000A06A080000020000001007E157BD112941C6C76D2E77AB58410000004033638140B0D0D680AA112941400F4FB977AB584100000000D7658140	\N	t
1556	mesh_2d	LPAV1556	11	943	elem_2d	923	elem_2d	01020000A06A08000002000000D00766430D122941D05830B472AB584100000080144C8140C0A2D600251229410A44827C73AB584100000060664E8140	\N	t
1557	mesh_2d	LPAV1557	11	943	elem_2d	938	elem_2d	01020000A06A08000002000000D00766430D122941D05830B472AB584100000080144C814040B6D600F11129419AD344F272AB5841000000803D4E8140	\N	t
1558	mesh_2d	LPAV1558	11	944	elem_2d	936	elem_2d	01020000A06A0800000200000090E084C8B6112941F8F7A0317FAB584100000000D76F8140A00DE1D7AB1129411A4082FC7DAB5841000000C0CC708140	\N	t
1559	mesh_2d	LPAV1559	11	957	elem_2d	927	elem_2d	01020000A06A080000020000001007E157BD112941C6C76D2E77AB58410000004033638140F03BEB2ED4112941E075B51F77AB58410000004033638140	\N	t
1562	mesh_2d	LPAV1562	11	958	elem_2d	948	elem_2d	01020000A06A08000002000000F06A28398312294194EEC9ED7CAB5841000000C0CC508140B0BEE0577E122941649B114F80AB5841000000E0A3708140	\N	t
1563	mesh_2d	LPAV1563	11	958	elem_2d	952	elem_2d	01020000A06A08000002000000F06A28398312294194EEC9ED7CAB5841000000C0CC508140007328B96D1229411EE5F2D97AAB5841000000C0CC608140	\N	t
1571	mesh_2d	LPAV1571	11	961	elem_2d	960	elem_2d	01020000A06A08000002000000804FC252BB112941582DD46479AB584100000000295E8140504FC2D2BB1129414874B55F7BAB5841000000A0705B8140	\N	t
1564	mesh_2d	LPAV1564	11	959	elem_2d	935	elem_2d	01020000A06A08000002000000A04C7A719D122941E0C56D3E7CAB5841000000E07A4C81403008701AB0122941BAB0BF067EAB584100000040E1528140	\N	t
1565	mesh_2d	LPAV1565	11	960	elem_2d	922	elem_2d	01020000A06A08000002000000504FC2D2BB1129414874B55F7BAB5841000000A0705B8140C0B9FF5CCC112941EC22FDE079AB5841000000C0CC608140	\N	t
1566	mesh_2d	LPAV1566	11	960	elem_2d	940	elem_2d	01020000A06A08000002000000504FC2D2BB1129414874B55F7BAB5841000000A0705B8140309DA34DAB1129412022FD007CAB5841000000A047698140	\N	t
1572	mesh_2d	LPAV1572	11	962	elem_2d	933	elem_2d	01020000A06A080000020000003049A34D8B12294154DB1BA679AB584100000060B8448140601D709A771229413EDC1B3677AB5841000000A070458140	\N	t
1567	mesh_2d	LPAV1567	11	960	elem_2d	953	elem_2d	01020000A06A08000002000000504FC2D2BB1129414874B55F7BAB5841000000A0705B81408095A3CDBF112941CA7D8C537DAB5841000000608F628140	\N	t
1568	mesh_2d	LPAV1568	11	961	elem_2d	922	elem_2d	01020000A06A08000002000000804FC252BB112941582DD46479AB584100000000295E8140C0B9FF5CCC112941EC22FDE079AB5841000000C0CC608140	\N	t
1569	mesh_2d	LPAV1569	11	961	elem_2d	934	elem_2d	01020000A06A08000002000000804FC252BB112941582DD46479AB584100000000295E8140C02C66C3AA1129411E5630E479AB5841000000C0CC688140	\N	t
1570	mesh_2d	LPAV1570	11	961	elem_2d	957	elem_2d	01020000A06A08000002000000804FC252BB112941582DD46479AB584100000000295E81401007E157BD112941C6C76D2E77AB58410000004033638140	\N	t
1573	mesh_2d	LPAV1573	11	962	elem_2d	958	elem_2d	01020000A06A080000020000003049A34D8B12294154DB1BA679AB584100000060B8448140F06A28398312294194EEC9ED7CAB5841000000C0CC508140	\N	t
1574	mesh_2d	LPAV1574	11	962	elem_2d	959	elem_2d	01020000A06A080000020000003049A34D8B12294154DB1BA679AB584100000060B8448140A04C7A719D122941E0C56D3E7CAB5841000000E07A4C8140	\N	t
1582	mesh_2d	LPAV1582	11	965	elem_2d	928	elem_2d	01020000A06A0800000200000000F0EAAE9E122941026BDECB78AB5841000000E0A3448140506AD680BB12294100DB1B867AAB58410000008014448140	\N	t
1575	mesh_2d	LPAV1575	11	963	elem_2d	919	elem_2d	01020000A06A0800000200000080EFB77B171229410A4B59207CAB5841000000E0516E8140504F47BE0D12294172DB1B5679AB584100000040E1648140	\N	t
1576	mesh_2d	LPAV1576	11	963	elem_2d	930	elem_2d	01020000A06A0800000200000080EFB77B171229410A4B59207CAB5841000000E0516E81401074CCA9FD1129418A2BD4347EAB584100000080EB798140	\N	t
1577	mesh_2d	LPAV1577	11	963	elem_2d	939	elem_2d	01020000A06A0800000200000080EFB77B171229410A4B59207CAB5841000000E0516E814090F9654333122941547E8CE37BAB5841000000E0A3648140	\N	t
1583	mesh_2d	LPAV1583	11	965	elem_2d	959	elem_2d	01020000A06A0800000200000000F0EAAE9E122941026BDECB78AB5841000000E0A3448140A04C7A719D122941E0C56D3E7CAB5841000000E07A4C8140	\N	t
1578	mesh_2d	LPAV1578	11	963	elem_2d	950	elem_2d	01020000A06A0800000200000080EFB77B171229410A4B59207CAB5841000000E0516E814010DC09B414122941B6F7A0E17FAB5841000000C01E7F8140	\N	t
1579	mesh_2d	LPAV1579	11	964	elem_2d	958	elem_2d	01020000A06A08000002000000801C473E95122941BE913ABB7EAB5841000000A047578140F06A28398312294194EEC9ED7CAB5841000000C0CC508140	\N	t
1580	mesh_2d	LPAV1580	11	964	elem_2d	959	elem_2d	01020000A06A08000002000000801C473E95122941BE913ABB7EAB5841000000A047578140A04C7A719D122941E0C56D3E7CAB5841000000E07A4C8140	\N	t
1581	mesh_2d	LPAV1581	11	964	elem_2d	955	elem_2d	01020000A06A08000002000000801C473E95122941BE913ABB7EAB5841000000A04757814090507AF192122941EECD441282AB58410000004033718140	\N	t
1584	mesh_2d	LPAV1584	11	965	elem_2d	962	elem_2d	01020000A06A0800000200000000F0EAAE9E122941026BDECB78AB5841000000E0A34481403049A34D8B12294154DB1BA679AB584100000060B8448140	\N	t
1585	mesh_2d	LPAV1585	11	966	elem_2d	935	elem_2d	01020000A06A08000002000000E0878448A31229414A7C8C5381AB584100000060B86481403008701AB0122941BAB0BF067EAB584100000040E1528140	\N	t
1586	mesh_2d	LPAV1586	11	966	elem_2d	964	elem_2d	01020000A06A08000002000000E0878448A31229414A7C8C5381AB584100000060B8648140801C473E95122941BE913ABB7EAB5841000000A047578140	\N	t
1595	mesh_2d	LPAV1595	11	947	elem_2d	941	elem_2d	01020000A06A0800000200000080B25115A911294188FBA0B175AB5841000000E0A3648140B0D0D680AA112941400F4FB977AB584100000000D7658140	\N	t
1587	mesh_2d	LPAV1587	11	966	elem_2d	955	elem_2d	01020000A06A08000002000000E0878448A31229414A7C8C5381AB584100000060B864814090507AF192122941EECD441282AB58410000004033718140	\N	t
1588	mesh_2d	LPAV1588	11	967	elem_2d	937	elem_2d	01020000A06A080000020000004092CC29AD11294166FCA06173AB584100000040E1568140F044EB2EBC11294186BE968A74AB58410000000000668140	\N	t
1589	mesh_2d	LPAV1589	11	967	elem_2d	947	elem_2d	01020000A06A080000020000004092CC29AD11294166FCA06173AB584100000040E156814080B25115A911294188FBA0B175AB5841000000E0A3648140	\N	t
1590	mesh_2d	LPAV1590	11	945	elem_2d	923	elem_2d	01020000A06A08000002000000108728393812294112D444B271AB584100000080C24D8140C0A2D600251229410A44827C73AB584100000060664E8140	\N	t
1591	mesh_2d	LPAV1591	11	945	elem_2d	926	elem_2d	01020000A06A08000002000000108728393812294112D444B271AB584100000080C24D814090DFB7FB41122941FAFBA08174AB5841000000E051488140	\N	t
1592	mesh_2d	LPAV1592	11	946	elem_2d	929	elem_2d	01020000A06A080000020000005003E157C7112941C4AFBF9680AB5841000000803D768140A073F585E31129413A0C4FC97FAB5841000000C0F57E8140	\N	t
1593	mesh_2d	LPAV1593	11	946	elem_2d	944	elem_2d	01020000A06A080000020000005003E157C7112941C4AFBF9680AB5841000000803D76814090E084C8B6112941F8F7A0317FAB584100000000D76F8140	\N	t
1594	mesh_2d	LPAV1594	11	947	elem_2d	937	elem_2d	01020000A06A0800000200000080B25115A911294188FBA0B175AB5841000000E0A3648140F044EB2EBC11294186BE968A74AB58410000000000668140	\N	t
1596	mesh_2d	LPAV1596	11	953	elem_2d	924	elem_2d	01020000A06A080000020000008095A3CDBF112941CA7D8C537DAB5841000000608F628140D03AEB2ED7112941B269DE4B7CAB584100000060666E8140	\N	t
1597	mesh_2d	LPAV1597	11	953	elem_2d	936	elem_2d	01020000A06A080000020000008095A3CDBF112941CA7D8C537DAB5841000000608F628140A00DE1D7AB1129411A4082FC7DAB5841000000C0CC708140	\N	t
1598	mesh_2d	LPAV1598	11	953	elem_2d	944	elem_2d	01020000A06A080000020000008095A3CDBF112941CA7D8C537DAB5841000000608F62814090E084C8B6112941F8F7A0317FAB584100000000D76F8140	\N	t
1599	mesh_2d	LPAV1599	11	954	elem_2d	932	elem_2d	01020000A06A08000002000000A093ADA468122941B88763277EAB5841000000208561814060A784484F122941A2923A5B7CAB5841000000803D648140	\N	t
1600	mesh_2d	LPAV1600	11	954	elem_2d	942	elem_2d	01020000A06A08000002000000A093ADA468122941B88763277EAB58410000002085618140E0E18E1F571229417C1626BD7FAB5841000000205C638140	\N	t
1601	mesh_2d	LPAV1601	11	954	elem_2d	948	elem_2d	01020000A06A08000002000000A093ADA468122941B88763277EAB58410000002085618140B0BEE0577E122941649B114F80AB5841000000E0A3708140	\N	t
1602	mesh_2d	LPAV1602	11	954	elem_2d	952	elem_2d	01020000A06A08000002000000A093ADA468122941B88763277EAB58410000002085618140007328B96D1229411EE5F2D97AAB5841000000C0CC608140	\N	t
1603	mesh_2d	LPAV1603	11	955	elem_2d	948	elem_2d	01020000A06A0800000200000090507AF192122941EECD441282AB58410000004033718140B0BEE0577E122941649B114F80AB5841000000E0A3708140	\N	t
1604	mesh_2d	LPAV1604	11	956	elem_2d	918	elem_2d	01020000A06A0800000200000060CC5BEC0712294120C86D3E76AB584100000080EB63814050E03210EE112941C04282EC76AB58410000002085638140	\N	t
1605	mesh_2d	LPAV1605	11	956	elem_2d	919	elem_2d	01020000A06A0800000200000060CC5BEC0712294120C86D3E76AB584100000080EB638140504F47BE0D12294172DB1B5679AB584100000040E1648140	\N	t
1606	mesh_2d	LPAV1606	11	956	elem_2d	943	elem_2d	01020000A06A0800000200000060CC5BEC0712294120C86D3E76AB584100000080EB638140D00766430D122941D05830B472AB584100000080144C8140	\N	t
1607	mesh_2d	LPAV1607	11	956	elem_2d	949	elem_2d	01020000A06A0800000200000060CC5BEC0712294120C86D3E76AB584100000080EB638140408651151F122941708A63E776AB58410000000000648140	\N	t
1608	overflow	OFL1608	11	968	river	921	elem_2d	01020000A06A0800000300000094624E2DD311294109AB32ED6CAB5841000000400A418140A05B709AD1112941D3C96DB671AB5841F6285C8FC24C8140908EA34DD2112941566DDE9772AB58410000000000000000	\N	t
1609	overflow	OFL1609	11	969	river	925	elem_2d	01020000A06A0800000300000092B4BED563122941BAF0308271AB5841000000A0992D8140401B99F66112294105963A5373AB5841B81E854B61458140A082FF5C5F1229411EAAE85674AB58410000000000000000	\N	t
1610	overflow	OFL1610	11	970	river	928	elem_2d	01020000A06A0800000300000052096B3CE8122941299836F966AB5841000000C01E1B8140B03F7AF1BF122941E1E5F2D178AB5841C3F528D423418140005528B9BD122941706007AC79AB58410000000000000000	\N	t
1611	overflow	OFL1611	11	971	river	933	elem_2d	01020000A06A0800000300000033FE90F982122941567D8A2B71AB584100000080EB298140209684487D122941661A264D75AB58410AD7A36866468140C0597A717A12294152FBA04176AB58410000000000000000	\N	t
1612	overflow	OFL1612	11	934	elem_2d	875	elem_2d	01020000A06A08000002000000C02C66C3AA1129411E5630E479AB5841000000C0CC688140B051EB2E9A1129411037ABC87AAB584100000080147C8140	\N	t
1613	overflow	OFL1613	11	936	elem_2d	857	elem_2d	01020000A06A08000002000000A00DE1D7AB1129411A4082FC7DAB5841000000C0CC70814020E0ADA49C112941B4BA96BA7EAB5841000000A099878140	\N	t
1614	overflow	OFL1614	11	972	river	938	elem_2d	01020000A06A0800000300000088659C5FF4112941522E1C026CAB5841000000C01E39814018F3E097F21129418ADE1B1671AB58410000000000498140ACD45BCCF11129411259300472AB58410000000000000000	\N	t
1615	overflow	OFL1615	11	940	elem_2d	890	elem_2d	01020000A06A08000002000000309DA34DAB1129412022FD007CAB5841000000A0476981408047140B9A1129416C923AEB7CAB584100000000D7838140	\N	t
1616	overflow	OFL1616	11	941	elem_2d	866	elem_2d	01020000A06A08000002000000B0D0D680AA112941400F4FB977AB584100000000D765814020298F1F9911294192F0C99D77AB584100000000D7738140	\N	t
1617	overflow	OFL1617	11	973	river	943	elem_2d	01020000A06A08000003000000B63F6B19121229417C9AC5DF6CAB584100000000D73F814060D33290101229410E6EDEAB70AB58417A14AE47E1478140986DCCE90E1229416F6307B071AB58410000000000000000	\N	t
1618	overflow	OFL1618	11	974	river	965	elem_2d	01020000A06A08000003000000300AC49DA8122941A685196970AB58410000000000248140A86BFF9C9C12294183E6F22177AB58411F85EB3133448140D42DF5A59D122941C2A8E8F677AB58410000000000000000	\N	t
1619	overflow	OFL1619	11	967	elem_2d	874	elem_2d	01020000A06A080000020000004092CC29AD11294166FCA06173AB584100000040E156814030C4283995112941B26CDE4B74AB584100000080C25B8140	\N	t
1620	overflow	OFL1620	11	975	river	967	elem_2d	01020000A06A08000003000000CD46BFA1B11129414D6F83056FAB584100000040E14A814028C4FF9CB01129411CB5BF5672AB5841000000A099558140342B66E3AE112941C15830DC72AB58410000000000000000	\N	t
1621	overflow	OFL1621	11	976	river	945	elem_2d	01020000A06A0800000300000057FB329A2C122941C06AB53E6EAB584100000060B830814078FC65832B12294155A1117770AB5841333333F3A84B8140C44147DE31122941B43AAB1471AB58410000000000000000	\N	t
1622	overflow	OFL1622	11	977	river	945	elem_2d	01020000A06A08000003000000F7FEC0AE47122941D22E1FDB6FAB5841000000803D38814060A0ADA446122941A4114F5971AB5841D7A370DD23448140B813EB6E3F122941DBF2C98571AB58410000000000000000	\N	t
1623	overflow	OFL1623	11	947	elem_2d	831	elem_2d	01020000A06A0800000200000080B25115A911294188FBA0B175AB5841000000E0A364814070841E629B112941F2E6F2F975AB5841000000608F6A8140	\N	t
1624	overflow	OFL1624	11	951	elem_2d	837	elem_2d	01020000A06A08000002000000D0C4FFDCAE112941B68663D780AB5841000000400A778140A06399F6A01129414AEDC95D80AB5841000000A0477F8140	\N	t
\.


--
-- Name: _link_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('_link_id_seq', 1936, true);


--
-- Data for Name: _manhole_hydrology_node; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _manhole_hydrology_node (id, node_type, area, z_ground) FROM stdin;
\.


--
-- Data for Name: _manhole_node; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _manhole_node (id, node_type, area, z_ground) FROM stdin;
1050	manhole	1	560.030029
1051	manhole	1	559.099976
1052	manhole	1	554.969971
1054	manhole	1	553.309998
1053	manhole	1	552.599976
\.


--
-- Data for Name: _marker_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _marker_singularity (id, singularity_type, comment) FROM stdin;
\.


--
-- Data for Name: _mesh_2d_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _mesh_2d_link (id, link_type, z_invert, lateral_contraction_coef, border) FROM stdin;
1373	mesh_2d	557.590027	1	01020000A06A08000002000000F068EB2E5C1129419EC86DEE74AB5841000000C0F57C814020B3A3CD70112941788B632774AB584100000060B86C8140
1374	mesh_2d	561.130005	1	01020000A06A080000020000004080B87B95102941788863277CAB584100000060B88A8140C06BE1D7B0102941440E4F597AAB5841000000C01E8B8140
1375	mesh_2d	560.659973	1	01020000A06A0800000200000060CC51156411294168B1BF367CAB58410000002085858140204866C3611129412CEDC9AD80AB5841000000C0F59A8140
1376	mesh_2d	559.52002	1	01020000A06A0800000200000030B0F50542112941788863277CAB5841000000E0A37C814060CC51156411294168B1BF367CAB58410000002085858140
1377	mesh_2d	562.080017	1	01020000A06A0800000200000060B966C3331029415E0578C576AB5841000000608F908140507B5C6C351029411E4C594079AB5841000000A099938140
1378	mesh_2d	562.049988	1	01020000A06A08000002000000E0C066C31F1029418C6107B876AB584100000040E190814060B966C3331029415E0578C576AB5841000000608F908140
1379	mesh_2d	561.169983	1	01020000A06A0800000200000030E51E6299102941507F8C4379AB584100000080C28981404080B87B95102941788863277CAB584100000060B88A8140
1380	mesh_2d	561.22998	1	01020000A06A080000020000004080B87B95102941788863277CAB584100000060B88A81403005CDA97A102941F08863E77AAB584100000080148E8140
1381	mesh_2d	561.059998	1	01020000A06A080000020000004033005D8810294154C76D5E78AB5841000000A04789814030E51E6299102941507F8C4379AB584100000080C2898140
1382	mesh_2d	557.349976	1	01020000A06A08000002000000E067C2527A112941DAF0C9DD76AB584100000020857F814020B3A3CD70112941788B632774AB584100000060B86C8140
1383	mesh_2d	559.460022	1	01020000A06A08000002000000D095CCA9A3112941CA20FD907FAB584100000020AE7B8140E06599F69A1129419E72B5CF7FAB584100000020AE898140
1384	mesh_2d	561.559998	1	01020000A06A08000002000000E077140B191129410C35AB2880AB5841000000A070AB8140D00EAE24201129411274B5EF7BAB5841000000400A8D8140
1385	mesh_2d	559.559998	1	01020000A06A0800000200000030B0F50542112941788863277CAB5841000000E0A37C8140C0B0F585401129410C35AB2880AB5841000000C01EA18140
1386	mesh_2d	557.419983	1	01020000A06A0800000200000090115295AB1029419AC96D4E72AB584100000080146C81405037AE24B410294116F1C93D76AB58410000004033818140
1387	mesh_2d	560.150024	1	01020000A06A080000020000005037AE24B410294116F1C93D76AB5841000000403381814070C69976991029415A2ED4B476AB5841000000C0CC848140
1388	mesh_2d	561.159973	1	01020000A06A08000002000000106F85C83A1029417439AB6874AB5841000000A0478F8140708D0AB43B1029419A3AAB5871AB5841000000E0518A8140
1389	mesh_2d	560.080017	1	01020000A06A0800000200000050FE1E6256102941185930F471AB5841000000E05182814090E299764E102941706CDEFB74AB5841000000C0F5908140
1390	mesh_2d	562.440002	1	01020000A06A08000002000000602C521564102941B640825C7CAB5841000000208593814030E299764F102941D69B111F7FAB58410000000000B08140
1391	mesh_2d	561.690002	1	01020000A06A0800000200000060061FE240102941C45F07787BAB58410000004033958140007833905910294172DB1B5679AB584100000020858D8140
1392	mesh_2d	561.690002	1	01020000A06A08000002000000007833905910294172DB1B5679AB584100000020858D8140602C521564102941B640825C7CAB58410000002085938140
1393	mesh_2d	562.390015	1	01020000A06A0800000200000030E299764F102941D69B111F7FAB58410000000000B0814060061FE240102941C45F07787BAB58410000004033958140
1394	mesh_2d	559.840027	1	01020000A06A08000002000000B099EB2EDA1029415823FDC078AB5841000000C01E8781405037AE24B410294116F1C93D76AB58410000004033818140
1395	mesh_2d	559.26001	1	01020000A06A080000020000005037AE24B410294116F1C93D76AB584100000040338181401032AE24C210294150B4BF7674AB584100000000007A8140
1396	mesh_2d	561.47998	1	01020000A06A08000002000000D00EAE24201129411274B5EF7BAB5841000000400A8D8140504EE157FF1029410ABC962A7BAB5841000000400A8F8140
1397	mesh_2d	560.130005	1	01020000A06A08000002000000504EE157FF1029410ABC962A7BAB5841000000400A8F81407092C2D208112941E689635778AB584100000020AE818140
1398	mesh_2d	556.960022	1	01020000A06A08000002000000D0E4AD2490112941EABD962A76AB5841000000A0707B8140F0A0A34DA1112941422ED4F476AB5841000000E07A668140
1399	mesh_2d	557.619995	1	01020000A06A08000002000000503A3DE7A1112941E46ADE1B79AB5841000000C0F56C8140B0403DE79011294124BD963A78AB5841000000205C7F8140
1400	mesh_2d	558.190002	1	01020000A06A08000002000000809A473E451129414C9E118F78AB584100000080C2798140A091709A411129410A5830C474AB5841000000E051728140
1401	mesh_2d	558.289978	1	01020000A06A08000002000000A091709A411129410A5830C474AB5841000000E051728140F068EB2E5C1129419EC86DEE74AB5841000000C0F57C8140
1402	mesh_2d	559.200012	1	01020000A06A08000002000000903D8F9F62112941844C593078AB5841000000E0A3808140809A473E451129414C9E118F78AB584100000080C2798140
1403	mesh_2d	559.559998	1	01020000A06A08000002000000F068EB2E5C1129419EC86DEE74AB5841000000C0F57C8140903D8F9F62112941844C593078AB5841000000E0A3808140
1404	mesh_2d	560.48999	1	01020000A06A08000002000000C06BE1D7B0102941440E4F597AAB5841000000C01E8B8140B099EB2EDA1029415823FDC078AB5841000000C01E878140
1405	mesh_2d	561.429993	1	01020000A06A0800000200000060E3F585B91029417669DEEB7CAB5841000000E07A968140C06BE1D7B0102941440E4F597AAB5841000000C01E8B8140
1406	mesh_2d	561.679993	1	01020000A06A0800000200000000F199F6271029410E114FE972AB5841000000C0CC8E8140106F85C83A1029417439AB6874AB5841000000A0478F8140
1407	mesh_2d	561.880005	1	01020000A06A08000002000000507B5C6C351029411E4C594079AB5841000000A099938140109EB8FB451029412AB3BF8677AB5841000000A0478F8140
1408	mesh_2d	560.97998	1	01020000A06A08000002000000C06BE1D7B0102941440E4F597AAB5841000000C01E8B814030E51E6299102941507F8C4379AB584100000080C2898140
1409	mesh_2d	560.099976	1	01020000A06A080000020000005037AE24B410294116F1C93D76AB58410000004033818140C06BE1D7B0102941440E4F597AAB5841000000C01E8B8140
1410	mesh_2d	560.599976	1	01020000A06A0800000200000030E51E6299102941507F8C4379AB584100000080C289814070C69976991029415A2ED4B476AB5841000000C0CC848140
1411	mesh_2d	560.059998	1	01020000A06A08000002000000903D8F9F62112941844C593078AB5841000000E0A380814060CC51156411294168B1BF367CAB58410000002085858140
1412	mesh_2d	556.150024	1	01020000A06A08000002000000F068EB2E5C1129419EC86DEE74AB5841000000C0F57C81404077C25251112941606DDE7B72AB5841000000205C638140
1413	mesh_2d	554.619995	1	01020000A06A080000020000004077C25251112941606DDE7B72AB5841000000205C63814000AECC2963112941CCDE1B6670AB5841000000C0F5548140
1414	mesh_2d	560.659973	1	01020000A06A0800000200000070C69976991029415A2ED4B476AB5841000000C0CC8481404033005D8810294154C76D5E78AB5841000000A047898140
1415	mesh_2d	558.539978	1	01020000A06A08000002000000C02729B98B1029411E6DDE2B73AB5841000000205C75814070C69976991029415A2ED4B476AB5841000000C0CC848140
1416	mesh_2d	560.080017	1	01020000A06A08000002000000A0045CEC71112941FAD044F279AB5841000000C01E838140903D8F9F62112941844C593078AB5841000000E0A3808140
1417	mesh_2d	559.919983	1	01020000A06A08000002000000903D8F9F62112941844C593078AB5841000000E0A3808140E067C2527A112941DAF0C9DD76AB584100000020857F8140
1418	mesh_2d	561.909973	1	01020000A06A08000002000000109EB8FB451029412AB3BF8677AB5841000000A0478F814060B966C3331029415E0578C576AB5841000000608F908140
1419	mesh_2d	561.73999	1	01020000A06A0800000200000060B966C3331029415E0578C576AB5841000000608F908140106F85C83A1029417439AB6874AB5841000000A0478F8140
1420	mesh_2d	561.859985	1	01020000A06A08000002000000106F85C83A1029417439AB6874AB5841000000A0478F814090E299764E102941706CDEFB74AB5841000000C0F5908140
1421	mesh_2d	561.650024	1	01020000A06A08000002000000007833905910294172DB1B5679AB584100000020858D8140109EB8FB451029412AB3BF8677AB5841000000A0478F8140
1422	mesh_2d	561.630005	1	01020000A06A0800000200000090E299764E102941706CDEFB74AB5841000000C0F590814000A766C364102941D2B3BFC675AB5841000000400A8D8140
1423	mesh_2d	561.830017	1	01020000A06A08000002000000109EB8FB451029412AB3BF8677AB5841000000A0478F814090E299764E102941706CDEFB74AB5841000000C0F5908140
1424	mesh_2d	554.570007	1	01020000A06A0800000200000050110AB4861129415CAAE8B273AB5841000000608F5C814080268F1FA0112941FEB4BFA672AB5841000000400A558140
1425	mesh_2d	555.679993	1	01020000A06A08000002000000E0BF28B9A0112941A0F1C9CD74AB5841000000A0995D8140D0E4AD2490112941EABD962A76AB5841000000A0707B8140
1426	mesh_2d	555.52002	1	01020000A06A08000002000000D0E4AD2490112941EABD962A76AB5841000000A0707B814050110AB4861129415CAAE8B273AB5841000000608F5C8140
1427	mesh_2d	557.880005	1	01020000A06A08000002000000E054EBAE91112941929D117F7AAB5841000000E051828140503A3DE7A1112941E46ADE1B79AB5841000000C0F56C8140
1428	mesh_2d	561.52002	1	01020000A06A08000002000000F0F41E626F102941D2E5F2F978AB5841000000E0A38C8140007833905910294172DB1B5679AB584100000020858D8140
1429	mesh_2d	561.590027	1	01020000A06A08000002000000007833905910294172DB1B5679AB584100000020858D814000A766C364102941D2B3BFC675AB5841000000400A8D8140
1430	mesh_2d	558.190002	1	01020000A06A08000002000000A091709A411129410A5830C474AB5841000000E051728140E0B01EE2241129412E8A639777AB5841000000E051768140
1431	mesh_2d	558.98999	1	01020000A06A0800000200000080A199F6FB1029419E61078876AB584100000060B87C81403047E157121129413225FDD073AB584100000000D7778140
1432	mesh_2d	559.51001	1	01020000A06A080000020000007092C2D208112941E689635778AB584100000020AE81814080A199F6FB1029419E61078876AB584100000060B87C8140
1433	mesh_2d	558.77002	1	01020000A06A08000002000000E0B01EE2241129412E8A639777AB5841000000E0517681407092C2D208112941E689635778AB584100000020AE818140
1434	mesh_2d	558.789978	1	01020000A06A080000020000003047E157121129413225FDD073AB584100000000D7778140E0B01EE2241129412E8A639777AB5841000000E051768140
1435	mesh_2d	558.72998	1	01020000A06A08000002000000E0B01EE2241129412E8A639777AB5841000000E051768140809A473E451129414C9E118F78AB584100000080C2798140
1436	mesh_2d	559.169983	1	01020000A06A08000002000000809A473E451129414C9E118F78AB584100000080C279814030B0F50542112941788863277CAB5841000000E0A37C8140
1437	mesh_2d	559.570007	1	01020000A06A0800000200000030B0F50542112941788863277CAB5841000000E0A37C8140D00EAE24201129411274B5EF7BAB5841000000400A8D8140
1438	mesh_2d	558.77002	1	01020000A06A08000002000000D00EAE24201129411274B5EF7BAB5841000000400A8D8140E0B01EE2241129412E8A639777AB5841000000E051768140
1439	mesh_2d	555.97998	1	01020000A06A0800000200000070683DE726112941FA30D4B46FAB5841000000A070618140803CE1D72E1129413663074872AB5841000000608F6C8140
1440	mesh_2d	557.349976	1	01020000A06A08000002000000803CE1D72E1129413663074872AB5841000000608F6C81403047E157121129413225FDD073AB584100000000D7778140
1441	mesh_2d	556.419983	1	01020000A06A080000020000004077C25251112941606DDE7B72AB5841000000205C638140A091709A411129410A5830C474AB5841000000E051728140
1442	mesh_2d	554.369995	1	01020000A06A08000002000000C071EBAE4411294138DF1B466FAB584100000080145481404077C25251112941606DDE7B72AB5841000000205C638140
1443	mesh_2d	555.210022	1	01020000A06A08000002000000803CE1D72E1129413663074872AB5841000000608F6C8140C071EBAE4411294138DF1B466FAB58410000008014548140
1444	mesh_2d	557.679993	1	01020000A06A08000002000000A091709A411129410A5830C474AB5841000000E051728140803CE1D72E1129413663074872AB5841000000608F6C8140
1445	mesh_2d	558.75	1	01020000A06A0800000200000000A766C364102941D2B3BFC675AB5841000000400A8D8140E0B6140B71102941A04E599072AB58410000004033758140
1446	mesh_2d	561.280029	1	01020000A06A08000002000000A0D299F678102941C64282DC76AB5841000000608F8A814000A766C364102941D2B3BFC675AB5841000000400A8D8140
1447	mesh_2d	558.22998	1	01020000A06A08000002000000C02729B98B1029411E6DDE2B73AB5841000000205C758140A0D299F678102941C64282DC76AB5841000000608F8A8140
1448	mesh_2d	562.400024	1	01020000A06A08000002000000000BCD296B1029413A8763777FAB5841000000C0CCB28140602C521564102941B640825C7CAB58410000002085938140
1449	mesh_2d	561.75	1	01020000A06A08000002000000602C521564102941B640825C7CAB584100000020859381403005CDA97A102941F08863E77AAB584100000080148E8140
1450	mesh_2d	561.640015	1	01020000A06A080000020000003005CDA97A102941F08863E77AAB584100000080148E8140B0A63DE7801029410AE4F2B97DAB5841000000C0CC928140
1451	mesh_2d	562.349976	1	01020000A06A08000002000000B0A63DE7801029410AE4F2B97DAB5841000000C0CC928140000BCD296B1029413A8763777FAB5841000000C0CCB28140
1452	mesh_2d	560.27002	1	01020000A06A0800000200000020CB28B9821129412C7F8CA379AB58410000006066828140E054EBAE91112941929D117F7AAB5841000000E051828140
1453	mesh_2d	560.369995	1	01020000A06A08000002000000E054EBAE91112941929D117F7AAB5841000000E05182814040E4ADA4911129410CF9A0517CAB5841000000C0F5848140
1454	mesh_2d	561.570007	1	01020000A06A080000020000003005CDA97A102941F08863E77AAB584100000080148E8140F0F41E626F102941D2E5F2F978AB5841000000E0A38C8140
1455	mesh_2d	561.340027	1	01020000A06A08000002000000F0F41E626F102941D2E5F2F978AB5841000000E0A38C8140A0D299F678102941C64282DC76AB5841000000608F8A8140
1456	mesh_2d	561.130005	1	01020000A06A080000020000004033005D8810294154C76D5E78AB5841000000A0478981403005CDA97A102941F08863E77AAB584100000080148E8140
1457	mesh_2d	561.159973	1	01020000A06A08000002000000A0D299F678102941C64282DC76AB5841000000608F8A81404033005D8810294154C76D5E78AB5841000000A047898140
1458	mesh_2d	559.070007	1	01020000A06A08000002000000E067C2527A112941DAF0C9DD76AB584100000020857F8140D0E4AD2490112941EABD962A76AB5841000000A0707B8140
1459	mesh_2d	559.919983	1	01020000A06A08000002000000B0403DE79011294124BD963A78AB5841000000205C7F814020CB28B9821129412C7F8CA379AB58410000006066828140
1460	mesh_2d	559.429993	1	01020000A06A08000002000000D0E4AD2490112941EABD962A76AB5841000000A0707B8140B0403DE79011294124BD963A78AB5841000000205C7F8140
1461	mesh_2d	559.960022	1	01020000A06A0800000200000020CB28B9821129412C7F8CA379AB58410000006066828140E067C2527A112941DAF0C9DD76AB584100000020857F8140
1462	mesh_2d	563.98999	1	01020000A06A0800000200000010B18F9F2E1029413C0378757CAB5841000000608FA08140907D5C6C2F102941B4BA96BA7EAB584100000040E1B28140
1463	mesh_2d	561.27002	1	01020000A06A08000002000000B0A63DE7801029410AE4F2B97DAB5841000000C0CC9281404080B87B95102941788863277CAB584100000060B88A8140
1464	mesh_2d	562.869995	1	01020000A06A0800000200000020EC1EE286102941A84959D07FAB584100000020AEAF8140B0A63DE7801029410AE4F2B97DAB5841000000C0CC928140
1465	mesh_2d	561.25	1	01020000A06A080000020000004080B87B95102941788863277CAB584100000060B88A8140F0145295A21029410C35AB2880AB5841000000A099B58140
1466	mesh_2d	562.809998	1	01020000A06A08000002000000F0145295A21029410C35AB2880AB5841000000A099B5814060E3F585B91029417669DEEB7CAB5841000000E07A968140
1467	mesh_2d	562.549988	1	01020000A06A0800000200000060E3F585B91029417669DEEB7CAB5841000000E07A96814080F97A71D010294144BB963A7DAB5841000000A070958140
1468	mesh_2d	562.679993	1	01020000A06A0800000200000080F97A71D010294144BB963A7DAB5841000000A070958140D04D3310CA1029410C35AB2880AB5841000000E051B08140
1469	mesh_2d	558.73999	1	01020000A06A08000002000000C0B45115A311294128E4F2697DAB584100000000007281401055EB2E91112941FCC46D9E7EAB5841000000608F8A8140
1470	mesh_2d	558.539978	1	01020000A06A0800000200000040E4ADA4911129410CF9A0517CAB5841000000C0F5848140601BB87BA211294186A7E8427BAB584100000060B8728140
1471	mesh_2d	562.210022	1	01020000A06A08000002000000507B5C6C351029411E4C594079AB5841000000A09993814060061FE240102941C45F07787BAB58410000004033958140
1472	mesh_2d	562.299988	1	01020000A06A080000020000001049529517102941148963877AAB58410000000000948140507B5C6C351029411E4C594079AB5841000000A099938140
1473	mesh_2d	562.390015	1	01020000A06A0800000200000060061FE240102941C45F07787BAB5841000000403395814010B18F9F2E1029413C0378757CAB5841000000608FA08140
1474	mesh_2d	562.570007	1	01020000A06A0800000200000010B18F9F2E1029413C0378757CAB5841000000608FA081401049529517102941148963877AAB58410000000000948140
1475	mesh_2d	559.5	1	01020000A06A08000002000000B099EB2EDA1029415823FDC078AB5841000000C01E87814080CE1EE2D51029419EBE964A74AB584100000080147C8140
1476	mesh_2d	559.169983	1	01020000A06A0800000200000060BA473EF010294120A011AF73AB5841000000205C79814080A199F6FB1029419E61078876AB584100000060B87C8140
1477	mesh_2d	559.539978	1	01020000A06A0800000200000080A199F6FB1029419E61078876AB584100000060B87C8140B099EB2EDA1029415823FDC078AB5841000000C01E878140
1478	mesh_2d	558.969971	1	01020000A06A0800000200000080CE1EE2D51029419EBE964A74AB584100000080147C814060BA473EF010294120A011AF73AB5841000000205C798140
1479	mesh_2d	561	1	01020000A06A0800000200000080F97A71D010294144BB963A7DAB5841000000A070958140B099EB2EDA1029415823FDC078AB5841000000C01E878140
1480	mesh_2d	562.669983	1	01020000A06A08000002000000003F3390F11029410C35AB2880AB5841000000803DA8814080F97A71D010294144BB963A7DAB5841000000A070958140
1481	mesh_2d	561.669983	1	01020000A06A08000002000000504EE157FF1029410ABC962A7BAB5841000000400A8F8140003F3390F11029410C35AB2880AB5841000000803DA88140
1482	mesh_2d	560.890015	1	01020000A06A08000002000000B099EB2EDA1029415823FDC078AB5841000000C01E878140504EE157FF1029410ABC962A7BAB5841000000400A8F8140
1483	mesh_2d	554.830017	1	01020000A06A0800000200000040EAADA4811129416AB5BF8671AB5841000000E0A356814050110AB4861129415CAAE8B273AB5841000000608F5C8140
1484	mesh_2d	554.590027	1	01020000A06A0800000200000020B3A3CD70112941788B632774AB584100000060B86C814000AECC2963112941CCDE1B6670AB5841000000C0F5548140
1485	mesh_2d	555.559998	1	01020000A06A0800000200000050110AB4861129415CAAE8B273AB5841000000608F5C814020B3A3CD70112941788B632774AB584100000060B86C8140
1486	mesh_2d	558.909973	1	01020000A06A080000020000003047E157121129413225FDD073AB584100000000D777814060BA473EF010294120A011AF73AB5841000000205C798140
1487	mesh_2d	556.140015	1	01020000A06A0800000200000060BA473EF010294120A011AF73AB5841000000205C798140809DC252EB10294188ABE89270AB584100000000295E8140
1488	mesh_2d	555.77002	1	01020000A06A08000002000000D0A6701A09112941C659302470AB584100000060B86081403047E157121129413225FDD073AB584100000000D7778140
1489	mesh_2d	560.880005	1	01020000A06A08000002000000E06599F69A1129419E72B5CF7FAB584100000020AE898140302F6643A41129416C5D07B881AB58410000002085958140
1490	mesh_2d	561.210022	1	01020000A06A080000020000001055EB2E91112941FCC46D9E7EAB5841000000608F8A8140E06599F69A1129419E72B5CF7FAB584100000020AE898140
1491	mesh_2d	560.380005	1	01020000A06A0800000200000060CC51156411294168B1BF367CAB58410000002085858140A0045CEC71112941FAD044F279AB5841000000C01E838140
1492	mesh_2d	560.609985	1	01020000A06A08000002000000D051148B7E112941864082DC7CAB5841000000801488814060CC51156411294168B1BF367CAB58410000002085858140
1493	mesh_2d	560.26001	1	01020000A06A08000002000000A0045CEC71112941FAD044F279AB5841000000C01E83814020CB28B9821129412C7F8CA379AB58410000006066828140
1494	mesh_2d	560.23999	1	01020000A06A0800000200000020CB28B9821129412C7F8CA379AB58410000006066828140D051148B7E112941864082DC7CAB58410000008014888140
1495	mesh_2d	561.25	1	01020000A06A08000002000000D097F505831129414CA5E83281AB58410000000029968140D051148B7E112941864082DC7CAB58410000008014888140
1496	mesh_2d	560.650024	1	01020000A06A08000002000000D051148B7E112941864082DC7CAB5841000000801488814040E4ADA4911129410CF9A0517CAB5841000000C0F5848140
1497	mesh_2d	560.619995	1	01020000A06A0800000200000040E4ADA4911129410CF9A0517CAB5841000000C0F58481401055EB2E91112941FCC46D9E7EAB5841000000608F8A8140
1498	mesh_2d	561.369995	1	01020000A06A080000020000001055EB2E91112941FCC46D9E7EAB5841000000608F8A8140D097F505831129414CA5E83281AB58410000000029968140
1499	mesh_2d	557.51001	1	01020000A06A080000020000001032AE24C210294150B4BF7674AB584100000000007A814090115295AB1029419AC96D4E72AB584100000080146C8140
1500	mesh_2d	559.190002	1	01020000A06A0800000200000080CE1EE2D51029419EBE964A74AB584100000080147C81401032AE24C210294150B4BF7674AB584100000000007A8140
1501	mesh_2d	556.710022	1	01020000A06A08000002000000B0B39976CB1029410C4F597071AB5841000000C0CC66814080CE1EE2D51029419EBE964A74AB584100000080147C8140
1518	mesh_2d	556.429993	1	01020000A06A08000002000000C0BBADA4FD1129414242823C78AB5841000000E07A648140F0D184C8DD112941447F8C6379AB58410000008014648140
1519	mesh_2d	556.549988	1	01020000A06A08000002000000E0E1093405122941FA69DE8B7BAB584100000080EB6B8140C0BBADA4FD1129414242823C78AB5841000000E07A648140
1520	mesh_2d	556.469971	1	01020000A06A08000002000000F0D184C8DD112941447F8C6379AB58410000008014648140C08F7A71EA1129411C5F07387DAB5841000000C0CC748140
1521	mesh_2d	555.51001	1	01020000A06A0800000200000020741EE2C6112941CAE4F2B97BAB584100000080145C8140F0D184C8DD112941447F8C6379AB58410000008014648140
1522	mesh_2d	556.539978	1	01020000A06A0800000200000030E1E0572212294164B2BF9679AB5841000000E0A36481405006664311122941EAD1447277AB584100000060B8648140
1523	mesh_2d	555.789978	1	01020000A06A0800000200000080DCE0D72E1229416A76B5AF75AB5841000000205C5F814030E1E0572212294164B2BF9679AB5841000000E0A3648140
1524	mesh_2d	555.669983	1	01020000A06A0800000200000040FA8E1F1612294122DD1BD674AB5841000000A09965814080DCE0D72E1229416A76B5AF75AB5841000000205C5F8140
1525	mesh_2d	556.599976	1	01020000A06A080000020000004065CC29251229413AEEC9DD7DAB584100000040336581402014140B231229415C01787581AB5841000000803D7C8140
1526	mesh_2d	559.25	1	01020000A06A08000002000000C0BBADA4FD11294164AFBF9681AB5841000000606680814090B5AD240E122941CE5E07087EAB584100000000007A8140
1527	mesh_2d	558.539978	1	01020000A06A08000002000000D095CCA9A3112941CA20FD907FAB584100000020AE7B8140C0FE0934B81129411AE3F23980AB5841000000A047738140
1528	mesh_2d	558.409973	1	01020000A06A08000002000000C0FE0934B81129411AE3F23980AB5841000000A047738140206D473EBE1129416C5D07B881AB5841000000205C898140
1529	mesh_2d	555.77002	1	01020000A06A08000002000000F0AB5B6C5E12294168A7E8927BAB58410000008014628140C094ADA465122941E4DB1B2678AB584100000000295E8140
1530	mesh_2d	554.179993	1	01020000A06A08000002000000C094ADA465122941E4DB1B2678AB584100000000295E8140501B701A7D122941C44B59307AAB584100000060B84C8140
1531	mesh_2d	554.640015	1	01020000A06A0800000200000070B35B6C4A122941FA9E11BF76AB5841000000205C5B814080DCE0D72E1229416A76B5AF75AB5841000000205C5F8140
1532	mesh_2d	553.219971	1	01020000A06A0800000200000080DCE0D72E1229416A76B5AF75AB5841000000205C5F814040DAE0D734122941D28B633773AB5841000000803D4A8140
1533	mesh_2d	551.780029	1	01020000A06A08000002000000D05DA34D5412294112DE1B5672AB5841000000803D3E814070B35B6C4A122941FA9E11BF76AB5841000000205C5B8140
1534	mesh_2d	556.359985	1	01020000A06A0800000200000020937A71E1112941E657302475AB584100000080EB638140F0D184C8DD112941447F8C6379AB58410000008014648140
1535	mesh_2d	556	1	01020000A06A08000002000000B0178F9FC71129417A4D59A075AB5841000000801464814020937A71E1112941E657302475AB584100000080EB638140
1536	mesh_2d	556.200012	1	01020000A06A08000002000000F0D184C8DD112941447F8C6379AB5841000000801464814080F90934C61129410EE6F25978AB5841000000803D628140
1537	mesh_2d	557.849976	1	01020000A06A08000002000000A05E709AC9112941C268DECB7EAB5841000000C0CC6E8140C08F7A71EA1129411C5F07387DAB5841000000C0CC748140
1538	mesh_2d	557.48999	1	01020000A06A08000002000000C08F7A71EA1129411C5F07387DAB5841000000C0CC748140E0E1093405122941FA69DE8B7BAB584100000080EB6B8140
1539	mesh_2d	558.630005	1	01020000A06A08000002000000C0BBADA4FD11294164AFBF9681AB58410000006066808140C08F7A71EA1129411C5F07387DAB5841000000C0CC748140
1540	mesh_2d	555.710022	1	01020000A06A08000002000000802699F643122941E47E8C637AAB5841000000205C63814070B35B6C4A122941FA9E11BF76AB5841000000205C5B8140
1541	mesh_2d	555.080017	1	01020000A06A0800000200000070B35B6C4A122941FA9E11BF76AB5841000000205C5B8140C094ADA465122941E4DB1B2678AB584100000000295E8140
1542	mesh_2d	556.26001	1	01020000A06A08000002000000802699F643122941E47E8C637AAB5841000000205C638140F0AB5B6C5E12294168A7E8927BAB58410000008014628140
1543	mesh_2d	552.72998	1	01020000A06A08000002000000C094ADA465122941E4DB1B2678AB584100000000295E8140B0D88E9F6F122941F84D595074AB5841000000E0A3468140
1544	mesh_2d	552.880005	1	01020000A06A08000002000000805C28B9A9122941B6B1BF667BAB584100000080C2478140F0AFB7FBC01229416617264D7DAB5841000000C01E498140
1545	mesh_2d	554.390015	1	01020000A06A08000002000000D061701AC11129413AB5BF0672AB58410000000029568140B0178F9FC71129417A4D59A075AB58410000008014648140
1546	mesh_2d	556.169983	1	01020000A06A08000002000000204199F6FC1129411A2FD4B474AB5841000000400A63814020937A71E1112941E657302475AB584100000080EB638140
1547	mesh_2d	553.97998	1	01020000A06A0800000200000020937A71E1112941E657302475AB584100000080EB6381407055701AE21129416CDE1B6671AB5841000000C0F5508140
1548	mesh_2d	556.530029	1	01020000A06A0800000200000060F365C343122941F40C4FD97DAB5841000000E0A36481404065CC29251229413AEEC9DD7DAB58410000004033658140
1549	mesh_2d	556.419983	1	01020000A06A0800000200000030E1E0572212294164B2BF9679AB5841000000E0A3648140802699F643122941E47E8C637AAB5841000000205C638140
1550	mesh_2d	556.419983	1	01020000A06A08000002000000802699F643122941E47E8C637AAB5841000000205C63814060F365C343122941F40C4FD97DAB5841000000E0A3648140
1551	mesh_2d	556.950012	1	01020000A06A08000002000000601BB87BA211294186A7E8427BAB584100000060B872814010EC5B6CB3112941088963A77AAB5841000000608F668140
1552	mesh_2d	556.080017	1	01020000A06A08000002000000E0F53290B411294134DA1BA67CAB5841000000A099638140C0B45115A311294128E4F2697DAB58410000000000728140
1553	mesh_2d	556.669983	1	01020000A06A08000002000000B05C9976B31129415675B58F78AB5841000000205C658140503A3DE7A1112941E46ADE1B79AB5841000000C0F56C8140
1554	mesh_2d	556.5	1	01020000A06A08000002000000004DF5854A1229415453305481AB5841000000801478814060F365C343122941F40C4FD97DAB5841000000E0A3648140
1555	mesh_2d	556.400024	1	01020000A06A0800000200000060F365C343122941F40C4FD97DAB5841000000E0A3648140B0FF138B59122941C6B0BFE67DAB5841000000C0CC628140
1556	mesh_2d	553.719971	1	01020000A06A080000020000000016140B1E12294174FDA09170AB584100000080EB51814040FA8E1F1612294122DD1BD674AB5841000000A099658140
1557	mesh_2d	553.570007	1	01020000A06A08000002000000204199F6FC1129411A2FD4B474AB5841000000400A638140C090511503122941A8DE1BC670AB584100000060664E8140
1558	mesh_2d	557.409973	1	01020000A06A08000002000000D095CCA9A3112941CA20FD907FAB584100000020AE7B8140D0323DE7B5112941E0E3F2297EAB5841000000A0476B8140
1559	mesh_2d	556.320007	1	01020000A06A08000002000000B0178F9FC71129417A4D59A075AB5841000000801464814080F90934C61129410EE6F25978AB5841000000803D628140
1560	mesh_2d	556.419983	1	01020000A06A0800000200000080CDD600B3112941D4E6F24976AB5841000000C01E658140B0178F9FC71129417A4D59A075AB58410000008014648140
1561	mesh_2d	556.549988	1	01020000A06A08000002000000B05C9976B31129415675B58F78AB5841000000205C65814080CDD600B3112941D4E6F24976AB5841000000C01E658140
1562	mesh_2d	555.27002	1	01020000A06A08000002000000A09B5BEC891229417468DE9B7FAB58410000006066628140D0EA3CE775122941CC2BD4847DAB5841000000C01E5F8140
1563	mesh_2d	553.650024	1	01020000A06A08000002000000D0EA3CE775122941CC2BD4847DAB5841000000C01E5F8140501B701A7D122941C44B59307AAB584100000060B84C8140
1564	mesh_2d	552.940002	1	01020000A06A08000002000000805C28B9A9122941B6B1BF667BAB584100000080C2478140505F2839A2122941DA68DE8B7EAB5841000000205C558140
1565	mesh_2d	555.369995	1	01020000A06A08000002000000D0DC84C8C011294164BC963A7AAB584100000040335B814020741EE2C6112941CAE4F2B97BAB584100000080145C8140
1566	mesh_2d	555.880005	1	01020000A06A08000002000000E0F53290B411294134DA1BA67CAB5841000000A09963814010EC5B6CB3112941088963A77AAB5841000000608F668140
1567	mesh_2d	555.400024	1	01020000A06A0800000200000020741EE2C6112941CAE4F2B97BAB584100000080145C8140E0F53290B411294134DA1BA67CAB5841000000A099638140
1568	mesh_2d	555.409973	1	01020000A06A0800000200000080F90934C61129410EE6F25978AB5841000000803D628140D0DC84C8C011294164BC963A7AAB584100000040335B8140
1569	mesh_2d	556.719971	1	01020000A06A0800000200000010EC5B6CB3112941088963A77AAB5841000000608F668140B05C9976B31129415675B58F78AB5841000000205C658140
1570	mesh_2d	555.950012	1	01020000A06A08000002000000B05C9976B31129415675B58F78AB5841000000205C65814080F90934C61129410EE6F25978AB5841000000803D628140
1571	mesh_2d	555.400024	1	01020000A06A08000002000000D0DC84C8C011294164BC963A7AAB584100000040335B814010EC5B6CB3112941088963A77AAB5841000000608F668140
1572	mesh_2d	552.23999	1	01020000A06A08000002000000501B701A7D122941C44B59307AAB584100000060B84C814090537AF18A122941D4E6F24976AB584100000080EB458140
1573	mesh_2d	553.070007	1	01020000A06A0800000200000090281E6290122941B040826C7CAB5841000000608F4C8140501B701A7D122941C44B59307AAB584100000060B84C8140
1574	mesh_2d	552.820007	1	01020000A06A08000002000000B04E7AF197122941D6D044527AAB5841000000608F46814090281E6290122941B040826C7CAB5841000000608F4C8140
1575	mesh_2d	556.559998	1	01020000A06A08000002000000E0E1093405122941FA69DE8B7BAB584100000080EB6B814030E1E0572212294164B2BF9679AB5841000000E0A3648140
1576	mesh_2d	557.650024	1	01020000A06A0800000200000090B5AD240E122941CE5E07087EAB584100000000007A8140E0E1093405122941FA69DE8B7BAB584100000080EB6B8140
1577	mesh_2d	556.5	1	01020000A06A0800000200000030E1E0572212294164B2BF9679AB5841000000E0A36481404065CC29251229413AEEC9DD7DAB58410000004033658140
1578	mesh_2d	556.599976	1	01020000A06A080000020000004065CC29251229413AEEC9DD7DAB5841000000403365814090B5AD240E122941CE5E07087EAB584100000000007A8140
1579	mesh_2d	553.570007	1	01020000A06A08000002000000A09B5BEC891229417468DE9B7FAB5841000000606662814090281E6290122941B040826C7CAB5841000000608F4C8140
1580	mesh_2d	553.409973	1	01020000A06A0800000200000090281E6290122941B040826C7CAB5841000000608F4C8140505F2839A2122941DA68DE8B7EAB5841000000205C558140
1581	mesh_2d	556.409973	1	01020000A06A08000002000000106DFFDC98122941A40178B580AB584100000080EB698140A09B5BEC891229417468DE9B7FAB58410000006066628140
1582	mesh_2d	552.450012	1	01020000A06A08000002000000C0838448AE12294132E6F2F977AB584100000080EB438140805C28B9A9122941B6B1BF667BAB584100000080C2478140
1583	mesh_2d	552.840027	1	01020000A06A08000002000000805C28B9A9122941B6B1BF667BAB584100000080C2478140B04E7AF197122941D6D044527AAB5841000000608F468140
1584	mesh_2d	552.469971	1	01020000A06A08000002000000B04E7AF197122941D6D044527AAB5841000000608F46814090537AF18A122941D4E6F24976AB584100000080EB458140
1585	mesh_2d	554.669983	1	01020000A06A08000002000000505F2839A2122941DA68DE8B7EAB5841000000205C558140F0ABE057B01229413C4959F080AB584100000000005C8140
1586	mesh_2d	554.890015	1	01020000A06A08000002000000106DFFDC98122941A40178B580AB584100000080EB698140505F2839A2122941DA68DE8B7EAB5841000000205C558140
1587	mesh_2d	557.25	1	01020000A06A08000002000000406028B99F122941127B8C9384AB5841000000608F868140106DFFDC98122941A40178B580AB584100000080EB698140
1588	mesh_2d	554.570007	1	01020000A06A08000002000000D061701AC11129413AB5BF0672AB58410000000029568140F00BE157B01129416C104F9974AB584100000080145C8140
1589	mesh_2d	555.409973	1	01020000A06A08000002000000F00BE157B01129416C104F9974AB584100000080145C8140E0BF28B9A0112941A0F1C9CD74AB5841000000A0995D8140
1590	mesh_2d	553.27002	1	01020000A06A0800000200000040DAE0D734122941D28B633773AB5841000000803D4A81400016140B1E12294174FDA09170AB584100000080EB518140
1591	mesh_2d	551.780029	1	01020000A06A08000002000000D05DA34D5412294112DE1B5672AB5841000000803D3E814040DAE0D734122941D28B633773AB5841000000803D4A8140
1592	mesh_2d	557.809998	1	01020000A06A08000002000000A05E709AC9112941C268DECB7EAB5841000000C0CC6E814010AB2839D81129416C5D07B881AB5841000000400A8F8140
1593	mesh_2d	557.549988	1	01020000A06A08000002000000C0FE0934B81129411AE3F23980AB5841000000A047738140A05E709AC9112941C268DECB7EAB5841000000C0CC6E8140
1594	mesh_2d	555.51001	1	01020000A06A08000002000000F00BE157B01129416C104F9974AB584100000080145C814080CDD600B3112941D4E6F24976AB5841000000C01E658140
1595	mesh_2d	556.640015	1	01020000A06A0800000200000080CDD600B3112941D4E6F24976AB5841000000C01E658140F0A0A34DA1112941422ED4F476AB5841000000E07A668140
1596	mesh_2d	555.51001	1	01020000A06A0800000200000020741EE2C6112941CAE4F2B97BAB584100000080145C8140A05E709AC9112941C268DECB7EAB5841000000C0CC6E8140
1597	mesh_2d	556.450012	1	01020000A06A08000002000000D0323DE7B5112941E0E3F2297EAB5841000000A0476B8140E0F53290B411294134DA1BA67CAB5841000000A099638140
1598	mesh_2d	557.070007	1	01020000A06A08000002000000A05E709AC9112941C268DECB7EAB5841000000C0CC6E8140D0323DE7B5112941E0E3F2297EAB5841000000A0476B8140
1599	mesh_2d	556.26001	1	01020000A06A08000002000000B0FF138B59122941C6B0BFE67DAB5841000000C0CC628140F0AB5B6C5E12294168A7E8927BAB58410000008014628140
1600	mesh_2d	556.200012	1	01020000A06A08000002000000E085D600721229414CA5E83281AB584100000080C2818140B0FF138B59122941C6B0BFE67DAB5841000000C0CC628140
1601	mesh_2d	555.830017	1	01020000A06A08000002000000D0EA3CE775122941CC2BD4847DAB5841000000C01E5F8140E085D600721229414CA5E83281AB584100000080C2818140
1602	mesh_2d	555.869995	1	01020000A06A08000002000000F0AB5B6C5E12294168A7E8927BAB58410000008014628140D0EA3CE775122941CC2BD4847DAB5841000000C01E5F8140
1603	mesh_2d	556.799988	1	01020000A06A080000020000001073FFDC88122941B47B8CE382AB584100000080EB8B8140A09B5BEC891229417468DE9B7FAB58410000006066628140
1604	mesh_2d	556.349976	1	01020000A06A08000002000000C0BBADA4FD1129414242823C78AB5841000000E07A648140204199F6FC1129411A2FD4B474AB5841000000400A638140
1605	mesh_2d	556.530029	1	01020000A06A080000020000005006664311122941EAD1447277AB584100000060B8648140C0BBADA4FD1129414242823C78AB5841000000E07A648140
1606	mesh_2d	556.349976	1	01020000A06A08000002000000204199F6FC1129411A2FD4B474AB5841000000400A63814040FA8E1F1612294122DD1BD674AB5841000000A099658140
1607	mesh_2d	556.47998	1	01020000A06A0800000200000040FA8E1F1612294122DD1BD674AB5841000000A0996581405006664311122941EAD1447277AB584100000060B8648140
1625	mesh_2d	546.289978	1	01020000A06A080000020000002002ADA4EC13294158EEC98D7DAB5841000000C0CC1281409082C1D2DD132941DE35ABF87DAB5841000000C0F5148140
1626	mesh_2d	546.549988	1	01020000A06A080000020000009082C1D2DD132941DE35ABF87DAB5841000000C0F5148140F071138BD31329419C8563C783AB5841000000C0CC1C8140
1627	mesh_2d	546.820007	1	01020000A06A08000002000000F071138BD31329419C8563C783AB5841000000C0CC1C814060AEF485F11329414E5D070882AB5841000000E051168140
1628	mesh_2d	546.22998	1	01020000A06A0800000200000060AEF485F11329414E5D070882AB5841000000E0511681402002ADA4EC13294158EEC98D7DAB5841000000C0CC128140
1629	mesh_2d	546.02002	1	01020000A06A08000002000000502EE057FF13294158EEC98D7DAB584100000000291081402002ADA4EC13294158EEC98D7DAB5841000000C0CC128140
1630	mesh_2d	545.98999	1	01020000A06A0800000200000060AEF485F11329414E5D070882AB5841000000E051168140C09B1DE2071429415672B58F80AB584100000020850F8140
1631	mesh_2d	545.919983	1	01020000A06A08000002000000C09B1DE2071429415672B58F80AB584100000020850F8140502EE057FF13294158EEC98D7DAB58410000000029108140
1632	mesh_2d	544.650024	1	01020000A06A08000002000000E04D3C67181429418EA8E88278AB5841000000803D068140304265431C1429419ECF44927DAB5841000000C0CC0A8140
1633	mesh_2d	544.429993	1	01020000A06A08000002000000304265431C1429419ECF44927DAB5841000000C0CC0A8140802A8E1F401429411069DEFB7DAB5841000000A070038140
1634	mesh_2d	544.169983	1	01020000A06A08000002000000802A8E1F401429411069DEFB7DAB5841000000A07003814000BB50153D1429414C37AB287AAB5841000000C0CC068140
1635	mesh_2d	547.419983	1	01020000A06A0800000200000060448E1FFB132941F646590087AB584100000020851B8140509E46BEE51329411027D4248AAB584100000020AE318140
1636	mesh_2d	547.450012	1	01020000A06A0800000200000000BCA2CD0314294162ABBF468CAB5841000000205C2D814090FC83C81614294142CB443289AB5841000000A0991B8140
1637	mesh_2d	547.369995	1	01020000A06A0800000200000090FC83C81614294142CB443289AB5841000000A0991B814060448E1FFB132941F646590087AB584100000020851B8140
1638	mesh_2d	547.590027	1	01020000A06A08000002000000F071138BD31329419C8563C783AB5841000000C0CC1C8140F0628E9FA913294176F5A0E185AB5841000000A0702D8140
1639	mesh_2d	548.059998	1	01020000A06A08000002000000A080EAAEC7132941C8798C0388AB5841000000C0CC2E814020653C67DA1329414E1EFD3086AB58410000000029208140
1640	mesh_2d	547.51001	1	01020000A06A0800000200000020653C67DA1329414E1EFD3086AB58410000000029208140F071138BD31329419C8563C783AB5841000000C0CC1C8140
1641	mesh_2d	547.330017	1	01020000A06A0800000200000090B179F13A1429412EB696CA8AAB5841000000C0CC1A8140B0D9FEDC21142941B42FAB688EAB584100000060B8308140
1642	mesh_2d	548.219971	1	01020000A06A0800000200000060F75AEC3F14294106B4968A90AB5841000000608F328140B088F40556142941CED31BB68DAB584100000020AE218140
1643	mesh_2d	547.330017	1	01020000A06A08000002000000B088F40556142941CED31BB68DAB584100000020AE21814090B179F13A1429412EB696CA8AAB5841000000C0CC1A8140
1644	mesh_2d	547.109985	1	01020000A06A08000002000000B088F40556142941CED31BB68DAB584100000020AE2181408082F4856614294110C06DBE8BAB584100000040E1188140
1645	mesh_2d	546.659973	1	01020000A06A080000020000008082F4856614294110C06DBE8BAB584100000040E11881407015B7FB5C142941BADFF23989AB5841000000A047158140
1646	mesh_2d	546.619995	1	01020000A06A080000020000007015B7FB5C142941BADFF23989AB5841000000A04715814090B179F13A1429412EB696CA8AAB5841000000C0CC1A8140
1647	mesh_2d	544.119995	1	01020000A06A0800000200000070C5FEDC57142941ECE3F2097EAB5841000000C0F500814070F43110631429415C72B57F80AB584100000040E1028140
1648	mesh_2d	543.599976	1	01020000A06A0800000200000070F43110631429415C72B57F80AB584100000040E1028140F0DC5A6C86142941C85430747DAB58410000000029FE8040
1649	mesh_2d	543.900024	1	01020000A06A08000002000000202865C3611429410AC66DCE7BAB584100000080EB03814070C5FEDC57142941ECE3F2097EAB5841000000C0F5008140
1650	mesh_2d	543.22998	1	01020000A06A0800000200000010D33110BC142941AC98118F87AB5841000000C01EFD8040901A138BBC142941BCB896FA83AB584100000000D7F98040
1651	mesh_2d	543.210022	1	01020000A06A08000002000000901A138BBC142941BCB896FA83AB584100000000D7F98040C0C68348A61429419E47594085AB5841000000E051FC8040
1652	mesh_2d	543.539978	1	01020000A06A08000002000000C0C68348A61429419E47594085AB5841000000E051FC804070E308B4AB1429419413267D87AB584100000080EBFD8040
1653	mesh_2d	543.400024	1	01020000A06A0800000200000070E308B4AB1429419413267D87AB584100000080EBFD804010D33110BC142941AC98118F87AB5841000000C01EFD8040
1654	mesh_2d	545.280029	1	01020000A06A08000002000000C09B1DE2071429415672B58F80AB584100000020850F8140304265431C1429419ECF44927DAB5841000000C0CC0A8140
1655	mesh_2d	545.280029	1	01020000A06A08000002000000304265431C1429419ECF44927DAB5841000000C0CC0A814060E4FE5C0514294102933A5B7BAB5841000000400A0F8140
1656	mesh_2d	545.890015	1	01020000A06A0800000200000060E4FE5C0514294102933A5B7BAB5841000000400A0F8140502EE057FF13294158EEC98D7DAB58410000000029108140
1657	mesh_2d	545.219971	1	01020000A06A08000002000000D0453CE72D142941FC8563C782AB584100000080EB0981405094F405371429419E5B078886AB5841000000803D108140
1658	mesh_2d	544.77002	1	01020000A06A08000002000000C00809344814294198903ACB81AB58410000000029068140D0453CE72D142941FC8563C782AB584100000080EB098140
1659	mesh_2d	544.75	1	01020000A06A0800000200000080D1D50053142941AA5130C485AB5841000000803D0C8140C00809344814294198903ACB81AB58410000000029068140
1660	mesh_2d	545.52002	1	01020000A06A080000020000005094F405371429419E5B078886AB5841000000803D10814080D1D50053142941AA5130C485AB5841000000803D0C8140
1661	mesh_2d	544.320007	1	01020000A06A08000002000000A024C152D81429410EA8BF2695AB584100000000D7038140103F46BEE314294190CF1B0699AB58410000006066048140
1662	mesh_2d	543.369995	1	01020000A06A080000020000006058CB29F214294148044FF994AB5841000000C0F5FA8040A024C152D81429410EA8BF2695AB584100000000D7038140
1663	mesh_2d	545.849976	1	01020000A06A08000002000000703A8E9F151429415ED71B3684AB584100000080C2118140C09B1DE2071429415672B58F80AB584100000020850F8140
1664	mesh_2d	546.210022	1	01020000A06A0800000200000060448E1FFB132941F646590087AB584100000020851B8140703A8E9F151429415ED71B3684AB584100000080C2118140
1665	mesh_2d	546.780029	1	01020000A06A0800000200000060AEF485F11329414E5D070882AB5841000000E05116814060448E1FFB132941F646590087AB584100000020851B8140
1666	mesh_2d	544.47998	1	01020000A06A08000002000000401165C39E142941C664DE6B89AB5841000000C0F5048140F07CCBA990142941F0C16DBE86AB58410000004033058140
1667	mesh_2d	545.710022	1	01020000A06A0800000200000030E25A6C78142941B2A2E82288AB5841000000E0510E814030FDDF5782142941EEE8C9FD8BAB5841000000A070118140
1668	mesh_2d	544.619995	1	01020000A06A08000002000000F07CCBA990142941F0C16DBE86AB5841000000403305814030E25A6C78142941B2A2E82288AB5841000000E0510E8140
1669	mesh_2d	544.619995	1	01020000A06A0800000200000030FDDF5782142941EEE8C9FD8BAB5841000000A070118140401165C39E142941C664DE6B89AB5841000000C0F5048140
1670	mesh_2d	543.169983	1	01020000A06A08000002000000D05DCBA9E3142941FE9EE80292AB584100000000D7F98040C05AF485D01429413061DEFB92AB584100000080C2038140
1671	mesh_2d	544.359985	1	01020000A06A08000002000000C05AF485D01429413061DEFB92AB584100000080C2038140A024C152D81429410EA8BF2695AB584100000000D7038140
1672	mesh_2d	545.359985	1	01020000A06A0800000200000060E70834A1142941F8BC6DFE93AB5841000000E07A1081405071CBA9AF142941EE7E639795AB584100000040E10A8140
1673	mesh_2d	545.190002	1	01020000A06A080000020000005071CBA9AF142941EE7E639795AB584100000040E10A8140F0E5DF57C01429415860DE3B95AB584100000000000A8140
1674	mesh_2d	545.219971	1	01020000A06A08000002000000F0E5DF57C01429415860DE3B95AB584100000000000A8140A00865C3B51429412C768CA391AB5841000000A0990B8140
1675	mesh_2d	545.320007	1	01020000A06A08000002000000A00865C3B51429412C768CA391AB5841000000A0990B814060E70834A1142941F8BC6DFE93AB5841000000E07A108140
1676	mesh_2d	543.450012	1	01020000A06A080000020000009079CBA9991429417A0B4FC981AB5841000000A099FB8040B02D138B891429416C0A4F9984AB5841000000A047038140
1677	mesh_2d	543.419983	1	01020000A06A08000002000000C0C68348A61429419E47594085AB5841000000E051FC80409079CBA9991429417A0B4FC981AB5841000000A099FB8040
1678	mesh_2d	543.539978	1	01020000A06A08000002000000F07CCBA990142941F0C16DBE86AB58410000004033058140C0C68348A61429419E47594085AB5841000000E051FC8040
1679	mesh_2d	544.409973	1	01020000A06A08000002000000B02D138B891429416C0A4F9984AB5841000000A047038140F07CCBA990142941F0C16DBE86AB58410000004033058140
1680	mesh_2d	545.919983	1	01020000A06A0800000200000080631DE29D142941CEE7C9FD8EAB584100000080EB0F8140E0C6ACA48A1429410CC844C291AB5841000000A047158140
1681	mesh_2d	545.280029	1	01020000A06A08000002000000A00865C3B51429412C768CA391AB5841000000A0990B814080631DE29D142941CEE7C9FD8EAB584100000080EB0F8140
1682	mesh_2d	546.109985	1	01020000A06A08000002000000E0C6ACA48A1429410CC844C291AB5841000000A04715814060E70834A1142941F8BC6DFE93AB5841000000E07A108140
1683	mesh_2d	544.780029	1	01020000A06A0800000200000060B5CB29FA132941DA75B52F77AB584100000020850781402002ADA4EC13294158EEC98D7DAB5841000000C0CC128140
1684	mesh_2d	544.780029	1	01020000A06A0800000200000060E4FE5C0514294102933A5B7BAB5841000000400A0F814060B5CB29FA132941DA75B52F77AB58410000002085078140
1685	mesh_2d	546.659973	1	01020000A06A08000002000000E0C6ACA48A1429410CC844C291AB5841000000A047158140C032130B7C142941A0E5C9CD94AB584100000000D71F8140
1686	mesh_2d	546.150024	1	01020000A06A08000002000000204898F694142941489DE89296AB5841000000A0701D814060E70834A1142941F8BC6DFE93AB5841000000E07A108140
1687	mesh_2d	544.179993	1	01020000A06A08000002000000802A8E1F401429411069DEFB7DAB5841000000A07003814070C5FEDC57142941ECE3F2097EAB5841000000C0F5008140
1688	mesh_2d	545.97998	1	01020000A06A080000020000005094F405371429419E5B078886AB5841000000803D1081403021E057221429416A8463F786AB584100000080C2138140
1689	mesh_2d	547.299988	1	01020000A06A0800000200000090FC83C81614294142CB443289AB5841000000A0991B814090B179F13A1429412EB696CA8AAB5841000000C0CC1A8140
1690	mesh_2d	546.419983	1	01020000A06A080000020000003021E057221429416A8463F786AB584100000080C213814090FC83C81614294142CB443289AB5841000000A0991B8140
1691	mesh_2d	546.030029	1	01020000A06A0800000200000090B179F13A1429412EB696CA8AAB5841000000C0CC1A81405094F405371429419E5B078886AB5841000000803D108140
1692	mesh_2d	545.969971	1	01020000A06A0800000200000030FDDF5782142941EEE8C9FD8BAB5841000000A07011814080631DE29D142941CEE7C9FD8EAB584100000080EB0F8140
1693	mesh_2d	545.140015	1	01020000A06A0800000200000080631DE29D142941CEE7C9FD8EAB584100000080EB0F81402050463EB6142941BE63DE2B8CAB5841000000C01E098140
1694	mesh_2d	544.409973	1	01020000A06A080000020000002050463EB6142941BE63DE2B8CAB5841000000C01E098140401165C39E142941C664DE6B89AB5841000000C0F5048140
1695	mesh_2d	543.719971	1	01020000A06A0800000200000070E308B4AB1429419413267D87AB584100000080EBFD8040401165C39E142941C664DE6B89AB5841000000C0F5048140
1696	mesh_2d	543.650024	1	01020000A06A080000020000002050463EB6142941BE63DE2B8CAB5841000000C01E09814010D33110BC142941AC98118F87AB5841000000C01EFD8040
1697	mesh_2d	544.140015	1	01020000A06A08000002000000A094A2CD6C142941F0A3E8D284AB5841000000A09909814000B4FE5C861429419E0B4F6981AB5841000000E0A3008140
1698	mesh_2d	543.47998	1	01020000A06A0800000200000000B4FE5C861429419E0B4F6981AB5841000000E0A3008140F0DC5A6C86142941C85430747DAB58410000000029FE8040
1699	mesh_2d	544.359985	1	01020000A06A0800000200000070F43110631429415C72B57F80AB584100000040E1028140A094A2CD6C142941F0A3E8D284AB5841000000A099098140
1700	mesh_2d	545.539978	1	01020000A06A080000020000007015B7FB5C142941BADFF23989AB5841000000A04715814080D1D50053142941AA5130C485AB5841000000803D0C8140
1701	mesh_2d	545.200012	1	01020000A06A0800000200000080D1D50053142941AA5130C485AB5841000000803D0C8140A094A2CD6C142941F0A3E8D284AB5841000000A099098140
1702	mesh_2d	544.340027	1	01020000A06A0800000200000070F43110631429415C72B57F80AB584100000040E1028140C00809344814294198903ACB81AB58410000000029068140
1703	mesh_2d	544.409973	1	01020000A06A08000002000000B02D138B891429416C0A4F9984AB5841000000A047038140A094A2CD6C142941F0A3E8D284AB5841000000A099098140
1704	mesh_2d	545.179993	1	01020000A06A08000002000000A094A2CD6C142941F0A3E8D284AB5841000000A09909814030E25A6C78142941B2A2E82288AB5841000000E0510E8140
1705	mesh_2d	547.460022	1	01020000A06A0800000200000020653C67DA1329414E1EFD3086AB5841000000002920814060448E1FFB132941F646590087AB584100000020851B8140
1706	mesh_2d	544.140015	1	01020000A06A080000020000002050463EB6142941BE63DE2B8CAB5841000000C01E098140307B79F1CB142941A426D4448BAB5841000000C01E018140
1707	mesh_2d	544	1	01020000A06A08000002000000307B79F1CB142941A426D4448BAB5841000000C01E018140103E6F1ACB142941C8FE775588AB5841000000803D008140
1708	mesh_2d	543.640015	1	01020000A06A08000002000000103E6F1ACB142941C8FE775588AB5841000000803D00814010D33110BC142941AC98118F87AB5841000000C01EFD8040
1709	mesh_2d	546.109985	1	01020000A06A080000020000003021E057221429416A8463F786AB584100000080C2138140703A8E9F151429415ED71B3684AB584100000080C2118140
1710	mesh_2d	543.320007	1	01020000A06A080000020000009079CBA9991429417A0B4FC981AB5841000000A099FB8040104A6F1AAB14294186E3F2197FAB5841000000C01EFD8040
1711	mesh_2d	543.549988	1	01020000A06A0800000200000000B4FE5C861429419E0B4F6981AB5841000000E0A30081409079CBA9991429417A0B4FC981AB5841000000A099FB8040
1712	mesh_2d	544.599976	1	01020000A06A08000002000000F0E5DF57C01429415860DE3B95AB584100000000000A8140A024C152D81429410EA8BF2695AB584100000000D7038140
1713	mesh_2d	544.450012	1	01020000A06A08000002000000C05AF485D01429413061DEFB92AB584100000080C2038140A00865C3B51429412C768CA391AB5841000000A0990B8140
1714	mesh_2d	544.869995	1	01020000A06A08000002000000F0A6D580C4142941F417FD2097AB58410000006066088140F0E5DF57C01429415860DE3B95AB584100000000000A8140
1715	mesh_2d	544.97998	1	01020000A06A08000002000000805D1DE2AD142941FA2BAB5898AB58410000004033138140F0A6D580C4142941F417FD2097AB58410000006066088140
1716	mesh_2d	545.359985	1	01020000A06A080000020000005071CBA9AF142941EE7E639795AB584100000040E10A8140805D1DE2AD142941FA2BAB5898AB58410000004033138140
1717	mesh_2d	545.76001	1	01020000A06A0800000200000030E25A6C78142941B2A2E82288AB5841000000E0510E81407015B7FB5C142941BADFF23989AB5841000000A047158140
1718	mesh_2d	546.289978	1	01020000A06A080000020000008082F4856614294110C06DBE8BAB584100000040E118814030FDDF5782142941EEE8C9FD8BAB5841000000A070118140
1719	mesh_2d	544.570007	1	01020000A06A08000002000000E0C55AECC31429417AE7C9DD8FAB584100000020AE0581402050463EB6142941BE63DE2B8CAB5841000000C01E098140
1720	mesh_2d	544.609985	1	01020000A06A08000002000000A00865C3B51429412C768CA391AB5841000000A0990B8140E0C55AECC31429417AE7C9DD8FAB584100000020AE058140
1721	mesh_2d	543.080017	1	01020000A06A08000002000000C049463EC7142941AC20FDE07FAB584100000020AE018140806ECB29B7142941AC3E82CC81AB5841000000C0F5F88040
1722	mesh_2d	541.469971	1	01020000A06A08000002000000901A138BBC142941BCB896FA83AB584100000000D7F980402091FE5CE3142941D25D07A880AB584100000080C2EB8040
1723	mesh_2d	543.119995	1	01020000A06A08000002000000806ECB29B7142941AC3E82CC81AB5841000000C0F5F88040901A138BBC142941BCB896FA83AB584100000000D7F98040
1724	mesh_2d	547.789978	1	01020000A06A080000020000005093A24D70142941BE1AFDB08FAB5841000000C0CC1E81401015B7FB5D1429414E61DEAB92AB5841000000803D2E8140
1725	mesh_2d	546.640015	1	01020000A06A08000002000000E0C6ACA48A1429410CC844C291AB5841000000A0471581405093A24D70142941BE1AFDB08FAB5841000000C0CC1E8140
1726	mesh_2d	543.109985	1	01020000A06A08000002000000806ECB29B7142941AC3E82CC81AB5841000000C0F5F880409079CBA9991429417A0B4FC981AB5841000000A099FB8040
1727	mesh_2d	546.179993	1	01020000A06A0800000200000030FDDF5782142941EEE8C9FD8BAB5841000000A0701181405093A24D70142941BE1AFDB08FAB5841000000C0CC1E8140
1728	mesh_2d	547.679993	1	01020000A06A080000020000005093A24D70142941BE1AFDB08FAB5841000000C0CC1E8140B088F40556142941CED31BB68DAB584100000020AE218140
1729	mesh_2d	543.169983	1	01020000A06A08000002000000D05DCBA9E3142941FE9EE80292AB584100000000D7F98040E0C55AECC31429417AE7C9DD8FAB584100000020AE058140
1730	mesh_2d	544.940002	1	01020000A06A08000002000000304265431C1429419ECF44927DAB5841000000C0CC0A8140D0453CE72D142941FC8563C782AB584100000080EB098140
1731	mesh_2d	544.429993	1	01020000A06A08000002000000C00809344814294198903ACB81AB58410000000029068140802A8E1F401429411069DEFB7DAB5841000000A070038140
1732	mesh_2d	544.549988	1	01020000A06A08000002000000103F46BEE314294190CF1B0699AB58410000006066048140F0A6D580C4142941F417FD2097AB58410000006066088140
1733	mesh_2d	545.27002	1	01020000A06A08000002000000703A8E9F151429415ED71B3684AB584100000080C2118140D0453CE72D142941FC8563C782AB584100000080EB098140
1734	mesh_2d	541.450012	1	01020000A06A080000020000002091FE5CE3142941D25D07A880AB584100000080C2EB8040103E6F1ACB142941C8FE775588AB5841000000803D008140
1735	mesh_2d	543.109985	1	01020000A06A08000002000000D05DCBA9E3142941FE9EE80292AB584100000000D7F98040307B79F1CB142941A426D4448BAB5841000000C01E018140
1736	mesh_2d	544.719971	1	01020000A06A08000002000000E01C5BECDB132941264382DC75AB5841000000C01E0781409082C1D2DD132941DE35ABF87DAB5841000000C0F5148140
\.


--
-- Data for Name: _model_connect_bc_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _model_connect_bc_singularity (id, singularity_type, cascade_mode, zq_array) FROM stdin;
\.


--
-- Data for Name: _network_overflow_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _network_overflow_link (id, link_type, z_overflow, area) FROM stdin;
1755	network_overflow	552.590027	1
1754	network_overflow	553.799988	1
\.


--
-- Data for Name: _node; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _node (id, node_type, name, geom, generated, configuration, validity) FROM stdin;
1	river	NODR1	01010000A06A080000EDD79DBE3F0E29414AB54F5B8BAB58415C8FC2F528CE8140	\N	\N	t
2	river	NODR2	01010000A06A080000D6A71C53FF1429415CE6748D7BAB58415C8FC2F528CA8040	\N	\N	t
14	river	NODR14	01010000A06A0800007366BB3AB2142941A774B0DA77AB58415C8FC2F528E08040	\N	\N	t
3	river	NODR3	01010000A06A0800009148DB50A6142941EFACDD2C39AB5841EC51B81E85018140	\N	\N	t
4	river	NODR4	01010000A06A080000CAA8327C0D1529411366DA4679AB58416666666666C48040	\N	\N	t
15	river	NODR15	01010000A06A080000FE2B2B2DE0142941969526415AAB584148E17A14AEE38040	\N	\N	t
5	river	NODR5	01010000A06A080000425BCE8D1315294115A930CE7BAB58410000000000C28040	\N	\N	t
6	river	NODR6	01010000A06A08000050711C30541629418D62B97594AB5841F6285C8FC2B18040	\N	\N	t
16	river	NODR16	01010000A06A08000077A04E515C152941A9C134A684AB5841713D0AD7A3BE8040	\N	\N	t
7	river	NODR7	01010000A06A080000DD9909FEDC1329412F17F17D73AB5841B81E85EB51F48040	\N	\N	t
8	river	NODR8	01010000A06A0800009E79390C211429410F7F4D6E72AB5841A4703D0AD7F18040	\N	\N	t
9	river	NODR9	01010000A06A08000040FA26BD061029416154523B70AB58418FC2F5285C838140	\N	\N	t
10	river	NODR10	01010000A06A080000B779E3643E1029413D27BDCB6AAB58418FC2F5285C818140	\N	\N	t
11	river	NODR11	01010000A06A08000022DFA5A409122941B70BCD6F6CAB584133333333333F8140	\N	\N	t
1063	catchment	BC1063	01010000A06A080000E36F7B4A550D29418880436EB2AB58410000000000168240	\N	\N	t
12	river	NODR12	01010000A06A080000A75D4CCB38122941210725E06EAB5841B81E85EB51328140	\N	\N	t
23	river	NODR23	01010000A06A08000078431AF521122941C269C1B16DAB584152B81E85EB378140	\N	\N	t
1050	manhole	NODA1050	01010000A06A080000936FB6F9A613294147ACC51394AB58410AD7A3703D808140	\N	\N	t
24	river	NODR24	01010000A06A080000FA980F50CE1429414F40135579AB58415C8FC2F528E48040	\N	\N	t
13	river	NODR13	01010000A06A080000016E16AF4B132941CF3120F160AB58415C8FC2F528108140	\N	\N	t
1064	catchment	BC1064	01010000A06A080000A2D3F31E9F132941CB9C2E4319AB5841D7A3703D0A478140	\N	\N	t
1051	manhole	NODA1051	01010000A06A080000D4F36E7C2D1329410F971C138BAB5841CDCCCCCCCC788140	\N	\N	t
1062	catchment	BC1062	01010000A06A080000EAEC647844132941A9F6E96ABBAB5841A4703D0AD7ED8240	\N	\N	t
1052	manhole	NODA1052	01010000A06A0800000FB8AEE8BE122941AA9A209E82AB5841F6285C8FC2578140	\N	\N	t
1054	manhole	NODA1054	01010000A06A0800009A09863B33122941020EA12873AB584114AE47E17A4A8140	\N	\N	t
1053	manhole	NODA1053	01010000A06A080000EBFD46138912294196218E5778AB5841CDCCCCCCCC448140	\N	\N	t
1039	elem_2d	PAV1039	01010000A06A080000C08F27B9CB142941465C07C884AB58410000008014008140	12	\N	t
1038	elem_2d	PAV1038	01010000A06A08000030DCDF57DA14294170ACBF7689AB5841000000803DF68040	12	\N	t
1037	elem_2d	PAV1037	01010000A06A08000070D708B4CB142941B05807588EAB5841000000608F028140	12	\N	t
1036	elem_2d	PAV1036	01010000A06A0800000042E0D7CA13294130B0BF767FAB584100000000291A8140	12	\N	t
1035	elem_2d	PAV1035	01010000A06A080000804B8E1FE813294108FAA0B179AB584100000040E1128140	12	\N	t
1034	elem_2d	PAV1034	01010000A06A080000E0F031906C142941D825D4648DAB584100000040E11A8140	12	\N	t
1033	elem_2d	PAV1033	01010000A06A08000010BE27395014294164913AAB7FAB5841000000A047038140	12	\N	t
1032	elem_2d	PAV1032	01010000A06A080000C04FC1526514294114CC440287AB584100000020AE0F8140	12	\N	t
1031	elem_2d	PAV1031	01010000A06A080000A08BA2CD84142941BED71B3683AB5841000000A070038140	12	\N	t
1030	elem_2d	PAV1030	01010000A06A08000040483C67271429412E5C070885AB584100000020AE0F8140	12	\N	t
1029	elem_2d	PAV1029	01010000A06A080000C0BD79711A142941567C8C3381AB5841000000608F0A8140	12	\N	t
1028	elem_2d	PAV1028	01010000A06A0800006052463EB01429410A913A9B80AB58410000008014FA8040	12	\N	t
1027	elem_2d	PAV1027	01010000A06A080000C06EA2CDD1142941504B30B496AB58410000004033058140	12	\N	t
1026	elem_2d	PAV1026	01010000A06A080000801432900D142941D04B59107AAB5841000000803D0C8140	12	\N	t
1025	elem_2d	PAV1025	01010000A06A080000E00165C3C7142941D25407A898AB584100000040E10A8140	12	\N	t
1024	elem_2d	PAV1024	01010000A06A080000B06C138BE11329416A1326ED87AB58410000000029228140	12	\N	t
1023	elem_2d	PAV1023	01010000A06A080000E02CB77B1E14294116C06DAE8BAB5841000000803D208140	12	\N	t
1022	elem_2d	PAV1022	01010000A06A080000E0766F9A33142941280C4FF97FAB5841000000205C058140	12	\N	t
1021	elem_2d	PAV1021	01010000A06A080000209AFE5CCB142941F66BB58F91AB584100000060B8048140	12	\N	t
1020	elem_2d	PAV1020	01010000A06A080000209CA2CD581429415210262D90AB5841000000400A258140	12	\N	t
1019	elem_2d	PAV1019	01010000A06A080000B080CBA9861429416E5807088FAB5841000000E051168140	12	\N	t
1018	elem_2d	PAV1018	01010000A06A080000B0C483C8AB142941967B8C3383AB5841000000A070F98040	12	\N	t
1017	elem_2d	PAV1017	01010000A06A080000700CB7FB74142941B46BB53F92AB5841000000A070218140	12	\N	t
823	elem_2d	PAV823	01010000A06A0800004023B87B8D1129419A4E59A072AB5841000000608F568140	10	\N	t
1016	elem_2d	PAV1016	01010000A06A080000C0DD31909F1429416632AB3887AB584100000060B8008140	12	\N	t
824	elem_2d	PAV824	01010000A06A080000109B3DE79F10294104B2BF967AAB5841000000E0A38A8140	10	\N	t
1015	elem_2d	PAV1015	01010000A06A080000D03E6F1AC9142941AA903A9B81AB5841000000E0A3008140	12	\N	t
825	elem_2d	PAV825	01010000A06A08000060B9EBAE8510294126A7E8427CAB584100000060668C8140	10	\N	t
1014	elem_2d	PAV1014	01010000A06A08000090FF8D9FB214294196B4960A8FAB584100000080C20B8140	12	\N	t
826	elem_2d	PAV826	01010000A06A080000C07533905F1029414221FD507EAB5841000000E051A48140	10	\N	t
827	elem_2d	PAV827	01010000A06A08000020C9F585FF102941A21A26AD74AB5841000000E0517A8140	10	\N	t
822	elem_2d	PAV822	01010000A06A080000009F473E391129417E38ABF876AB58410000000000768140	10	\N	t
828	elem_2d	PAV828	01010000A06A080000401429B9BF10294194A8E87278AB58410000004033858140	10	\N	t
829	elem_2d	PAV829	01010000A06A080000C01C0A3468112941A2EFC91D7AAB5841000000608F828140	10	\N	t
830	elem_2d	PAV830	01010000A06A080000C0BB99F6B510294152DD1B5674AB5841000000C0F57A8140	10	\N	t
1013	elem_2d	PAV1013	01010000A06A08000090741D62701429415ADFF2398AAB5841000000A070138140	12	\N	t
848	elem_2d	PAV848	01010000A06A080000D0961E626A112941BE8A631776AB584100000020AE7D8140	10	\N	t
1012	elem_2d	PAV1012	01010000A06A080000D07FA24DA414294162C6443296AB58410000000029148140	12	\N	t
849	elem_2d	PAV849	01010000A06A080000401D29B9A7102941841726FD7CAB5841000000E07A948140	10	\N	t
1011	elem_2d	PAV1011	01010000A06A080000C0DE0834B8142941887E63A796AB584100000080140A8140	12	\N	t
850	elem_2d	PAV850	01010000A06A08000060DC99F65E102941C4E7F2C973AB5841000000E0A3828140	10	\N	t
1010	elem_2d	PAV1010	01010000A06A080000A09127B9C614294110BD6DBE93AB5841000000E051088140	12	\N	t
851	elem_2d	PAV851	01010000A06A08000020583D6752112941FC3F824C7EAB5841000000C0F5868140	10	\N	t
1009	elem_2d	PAV1009	01010000A06A0800005048987694142941723F82BC7FAB5841000000E051FC8040	12	\N	t
852	elem_2d	PAV852	01010000A06A080000B02EA44D271029413A19266D78AB5841000000205C918140	10	\N	t
1008	elem_2d	PAV1008	01010000A06A080000601D093411142941CC3C82CC86AB5841000000C01E178140	12	\N	t
853	elem_2d	PAV853	01010000A06A08000020207B71691029411A8963777AAB5841000000E0518E8140	10	\N	t
1007	elem_2d	PAV1007	01010000A06A080000A0416F9AC1142941BEC06DEE89AB5841000000608F028140	12	\N	t
854	elem_2d	PAV854	01010000A06A080000601D52158C102941FA5530447AAB584100000000D7898140	10	\N	t
1006	elem_2d	PAV1006	01010000A06A08000040EFFE5CE8132941F2CC44B284AB5841000000A0701B8140	12	\N	t
855	elem_2d	PAV855	01010000A06A08000040D6FF5C80112941586CDE3B75AB58410000008014748140	10	\N	t
1005	elem_2d	PAV1005	01010000A06A080000206F1DE27E142941E6E0F21986AB5841000000A070098140	12	\N	t
856	elem_2d	PAV856	01010000A06A080000C01EAEA4F5102941D037ABC878AB584100000080EB858140	10	\N	t
1004	elem_2d	PAV1004	01010000A06A08000050C4FEDC5A1429416E1FFD3083AB584100000040E1068140	12	\N	t
857	elem_2d	PAV857	01010000A06A08000020E0ADA49C112941B4BA96BA7EAB5841000000A099878140	10	\N	t
1003	elem_2d	PAV1003	01010000A06A080000B0316543481429412AB7962A88AB5841000000E0A3128140	12	\N	t
858	elem_2d	PAV858	01010000A06A08000030C03DE73C102941F48763877DAB584100000080EBA38140	10	\N	t
1002	elem_2d	PAV1002	01010000A06A080000C0D8834876142941B834AB0881AB5841000000C01E018140	12	\N	t
859	elem_2d	PAV859	01010000A06A08000050320AB42E112941AEB0BF267EAB58410000008014928140	10	\N	t
1001	elem_2d	PAV1001	01010000A06A080000B0008E9FAF1429415827D46489AB5841000000803D008140	12	\N	t
860	elem_2d	PAV860	01010000A06A08000060670A34A11029417CE7F28974AB584100000080147C8140	10	\N	t
1000	elem_2d	PAV1000	01010000A06A080000204598F69C14294196074F298CAB5841000000A0700D8140	12	\N	t
861	elem_2d	PAV861	01010000A06A080000500EF6054710294106E8F21973AB5841000000608F868140	10	\N	t
999	elem_2d	PAV999	01010000A06A08000000ADA2CD2B142941566FB58F88AB5841000000E0A3168140	12	\N	t
862	elem_2d	PAV862	01010000A06A080000D009F60553102941B0BB961A7CAB584100000000D7938140	10	\N	t
998	elem_2d	PAV998	01010000A06A08000090C9FEDC4C142941840D4F597CAB5841000000C0CC008140	12	\N	t
863	elem_2d	PAV863	01010000A06A080000406999F691112941D641825C79AB58410000008014808140	10	\N	t
997	elem_2d	PAV997	01010000A06A08000020691DE28E14294112F0A04194AB5841000000A099198140	12	\N	t
864	elem_2d	PAV864	01010000A06A080000D0570AB4CA102941309F112F76AB5841000000E07A7E8140	10	\N	t
996	elem_2d	PAV996	01010000A06A080000C0BFA2CDF9132941D28863377BAB584100000060B8108140	12	\N	t
865	elem_2d	PAV865	01010000A06A08000090F428391411294142DB1BD679AB584100000080EB858140	10	\N	t
995	elem_2d	PAV995	01010000A06A080000B08B79F19F142941F66BB58F91AB5841000000C0CC108140	12	\N	t
866	elem_2d	PAV866	01010000A06A08000020298F1F9911294192F0C99D77AB584100000000D7738140	10	\N	t
994	elem_2d	PAV994	01010000A06A08000050F5DF5797142941187B8C8384AB584100000060B8FE8040	12	\N	t
867	elem_2d	PAV867	01010000A06A08000020448F1F5111294174DC1BA676AB584100000000D77B8140	10	\N	t
993	elem_2d	PAV993	01010000A06A080000F08479F1B114294130F0A0F193AB5841000000A0470D8140	12	\N	t
868	elem_2d	PAV868	01010000A06A080000D097EB2EDF102941CAD3447272AB5841000000E07A6E8140	10	\N	t
992	elem_2d	PAV992	01010000A06A08000030BB5A6CE0142941CEB296CA93AB584100000020AEFF8040	12	\N	t
869	elem_2d	PAV869	01010000A06A08000040FD7A71C61029416074B51F7BAB5841000000A0478D8140	10	\N	t
831	elem_2d	PAV831	01010000A06A08000070841E629B112941F2E6F2F975AB5841000000608F6A8140	10	\N	t
832	elem_2d	PAV832	01010000A06A080000E014005DD9102941C6C46D2E7FAB584100000080149E8140	10	\N	t
833	elem_2d	PAV833	01010000A06A08000010DD47BE931029415819261D78AB584100000020AE878140	10	\N	t
834	elem_2d	PAV834	01010000A06A080000F0250AB44F1129410677B50F74AB5841000000C01E6D8140	10	\N	t
835	elem_2d	PAV835	01010000A06A080000701A331053112941CC6307B870AB584100000020AE578140	10	\N	t
836	elem_2d	PAV836	01010000A06A0800000045B87B33112941266EDE6B70AB584100000080C2638140	10	\N	t
837	elem_2d	PAV837	01010000A06A080000A06399F6A01129414AEDC95D80AB5841000000A0477F8140	10	\N	t
838	elem_2d	PAV838	01010000A06A080000804CAEA47B102941F64959007FAB5841000000A099A98140	10	\N	t
839	elem_2d	PAV839	01010000A06A080000F0E1C2D234102941B4818CE372AB5841000000A0998B8140	10	\N	t
840	elem_2d	PAV840	01010000A06A080000D0081F623A102941340F4FD977AB5841000000A070918140	10	\N	t
841	elem_2d	PAV841	01010000A06A080000807E5CEC2C102941FA05782575AB584100000020AE8F8140	10	\N	t
842	elem_2d	PAV842	01010000A06A08000000C7140B46102941F89D116F79AB5841000000A0478F8140	10	\N	t
843	elem_2d	PAV843	01010000A06A08000060C199F6A61029418A4C592078AB584100000020AE858140	10	\N	t
844	elem_2d	PAV844	01010000A06A08000080D128B9711129411CF8A0D17EAB58410000002085878140	10	\N	t
845	elem_2d	PAV845	01010000A06A08000000BEA3CD53112941EA7E8C537AAB5841000000C01E7D8140	10	\N	t
846	elem_2d	PAV846	01010000A06A080000609A1EE260112941366DDEEB72AB584100000060665E8140	10	\N	t
847	elem_2d	PAV847	01010000A06A080000B00200DD09112941C6B0BFE67DAB5841000000C01E998140	10	\N	t
870	elem_2d	PAV870	01010000A06A08000030700AB489102941046CDE1B76AB5841000000608F888140	10	\N	t
871	elem_2d	PAV871	01010000A06A080000C0DAFF5C741129412442828C78AB5841000000C0F5808140	10	\N	t
872	elem_2d	PAV872	01010000A06A080000B0BE3DE7401029410C953AEB75AB5841000000E07A908140	10	\N	t
873	elem_2d	PAV873	01010000A06A08000080F4473E551029411AD244F276AB584100000000008E8140	10	\N	t
991	elem_2d	PAV991	01010000A06A08000030FADF578A142941F212262D89AB584100000000D7098140	12	\N	t
990	elem_2d	PAV990	01010000A06A080000700E5B6C0214294168A4E89283AB5841000000608F148140	12	\N	t
901	river	NODR901	01010000A06A0800009D1D6A87221029416F50A8876DAB58410000008014768140	10	\N	t
989	elem_2d	PAV989	01010000A06A080000606F7971EB1429418A2CABD896AB58410000002085FF8040	12	\N	t
988	elem_2d	PAV988	01010000A06A080000C00B093440142941900A4F3984AB584100000060660A8140	12	\N	t
902	river	NODR902	01010000A06A080000E047D10564102941E24A13A96EAB58410000002085758140	10	\N	t
987	elem_2d	PAV987	01010000A06A0800000034B77B0B142941A45430D47DAB5841000000E0510E8140	12	\N	t
986	elem_2d	PAV986	01010000A06A080000A0E00834B3142941C0ADBFF685AB58410000002085FB8040	12	\N	t
903	river	NODR903	01010000A06A080000E29DACD918102941BE808C786EAB5841000000C01E7B8140	10	\N	t
985	elem_2d	PAV985	01010000A06A08000040E75AEC6A142941C0B0BFF67DAB5841000000205CFF8040	12	\N	t
984	elem_2d	PAV984	01010000A06A08000080DBACA453142941BCABBF568BAB584100000040331B8140	12	\N	t
904	river	NODR904	01010000A06A08000047132C7D4910294111FDDF1E6BAB584100000080EB7F8140	10	\N	t
983	elem_2d	PAV983	01010000A06A080000205FC1523C1429414ABF6DCE8DAB5841000000C01E238140	12	\N	t
982	elem_2d	PAV982	01010000A06A080000A0E75015C61329410C8F3AEB85AB5841000000C0CC228140	12	\N	t
905	river	NODR905	01010000A06A08000038799F8B0E102941FDA706796FAB584100000060B8828140	10	\N	t
981	elem_2d	PAV981	01010000A06A080000B0B3CBA9FE132941725A07A889AB5841000000E07A208140	12	\N	t
980	elem_2d	PAV980	01010000A06A08000030328E9F2B142941C836AB887BAB5841000000E0A3068140	12	\N	t
906	river	NODR906	01010000A06A080000497C756E91112941B2A9EA2B70AB58410000002085518140	10	\N	t
979	elem_2d	PAV979	01010000A06A08000050CA79F1F8132941520C4F897FAB5841000000803D128140	12	\N	t
978	elem_2d	PAV978	01010000A06A08000050FBD580E31329416420FDA080AB5841000000E051168140	12	\N	t
907	river	NODR907	01010000A06A080000BB77E08C5411294111C62A476DAB5841000000803D5C8140	10	\N	t
1040	river	NODR1040	01010000A06A080000C40766C3BB142941C04D255B78AB5841000000400AE58040	12	\N	t
908	river	NODR908	01010000A06A08000095281E77361129415FCC11816CAB5841000000E0A3688140	10	\N	t
874	elem_2d	PAV874	01010000A06A08000030C4283995112941B26CDE4B74AB584100000080C25B8140	10	\N	t
875	elem_2d	PAV875	01010000A06A080000B051EB2E9A1129411037ABC87AAB584100000080147C8140	10	\N	t
876	elem_2d	PAV876	01010000A06A080000A0D899F668102941A09E11AF77AB5841000000400A8D8140	10	\N	t
877	elem_2d	PAV877	01010000A06A080000B071148B2911294138B4BFB674AB5841000000608F748140	10	\N	t
878	elem_2d	PAV878	01010000A06A080000E0EB5115101129414AD2447276AB5841000000C0F57C8140	10	\N	t
879	elem_2d	PAV879	01010000A06A080000506E148B32112941BC9D110F7AAB584100000060B87E8140	10	\N	t
880	elem_2d	PAV880	01010000A06A080000A0BEF5851B112941748C638771AB5841000000E051688140	10	\N	t
881	elem_2d	PAV881	01010000A06A0800001073EB2E41112941F2AAE82272AB5841000000400A678140	10	\N	t
882	elem_2d	PAV882	01010000A06A080000D0BEEB2E77102941CC9F118F74AB5841000000E0517E8140	10	\N	t
883	elem_2d	PAV883	01010000A06A08000000FEF58572102941AA4A59207DAB5841000000C0F5948140	10	\N	t
884	elem_2d	PAV884	01010000A06A080000A062C252881129410C6ADE5B7BAB584100000060B8848140	10	\N	t
885	elem_2d	PAV885	01010000A06A080000A0750A347B102941026BDECB78AB584100000040338B8140	10	\N	t
886	elem_2d	PAV886	01010000A06A080000403A66C386112941DCA8E8B277AB584100000000D77F8140	10	\N	t
887	elem_2d	PAV887	01010000A06A0800008031A4CD1F102941F25430047DAB5841000000C01EA78140	10	\N	t
888	elem_2d	PAV888	01010000A06A080000A04E8548911029413621FD707EAB584100000060B8988140	10	\N	t
889	elem_2d	PAV889	01010000A06A080000008666C3BC102941D23F82BC7EAB584100000060B89C8140	10	\N	t
890	elem_2d	PAV890	01010000A06A0800008047140B9A1129416C923AEB7CAB584100000000D7838140	10	\N	t
891	elem_2d	PAV891	01010000A06A080000B09CE1572E102941501826DD7AAB584100000080EB958140	10	\N	t
892	elem_2d	PAV892	01010000A06A080000301AD780E6102941985730F475AB584100000020AE7B8140	10	\N	t
893	elem_2d	PAV893	01010000A06A080000F0C71E62E7102941480378557CAB584100000040E1928140	10	\N	t
894	elem_2d	PAV894	01010000A06A080000E021E1D775112941F82FD46472AB5841000000400A5F8140	10	\N	t
895	elem_2d	PAV895	01010000A06A080000706D6643FE1029410C59301472AB584100000060666C8140	10	\N	t
896	elem_2d	PAV896	01010000A06A08000010F85B6C9311294102E3F27980AB5841000000C01E918140	10	\N	t
897	elem_2d	PAV897	01010000A06A08000070BB7AF1751129412618264D7BAB58410000004033858140	10	\N	t
898	elem_2d	PAV898	01010000A06A080000E0BE511588112941C035AB487EAB584100000060B88C8140	10	\N	t
899	elem_2d	PAV899	01010000A06A08000080ACC252C31029416C77B5FF72AB584100000040E1728140	10	\N	t
1041	river	NODR1041	01010000A06A0800009B1EF969F51329415E7D45F872AB5841000000400AF78040	12	\N	t
900	river	NODR900	01010000A06A08000096E40A9931102941DA42790D6CAB5841000000A0997B8140	10	\N	t
909	river	NODR909	01010000A06A0800009E25CCDDDB102941478903256EAB584100000040E1768140	10	\N	t
910	river	NODR910	01010000A06A08000076EED6A618112941894D41C46CAB5841000000C0F56A8140	10	\N	t
1042	river	NODR1042	01010000A06A080000C13FFDABA9132941BE98E27970AB5841000000E07A108140	12	\N	t
911	river	NODR911	01010000A06A0800005F1D44F5721129418F1F77DF6EAB584100000060665A8140	10	\N	t
1043	river	NODR1043	01010000A06A08000091B44F8D1814294138D4A55772AB5841000000C0F5EA8040	12	\N	t
1044	river	NODR1044	01010000A06A08000080C7A34CD814294160DBA1DB79AB5841000000205CE18040	12	\N	t
1045	river	NODR1045	01010000A06A080000E48032109D142941DD19DEF376AB584100000080EBDF8040	12	\N	t
1046	river	NODR1046	01010000A06A08000058555AC35814294169ECF2A175AB5841000000C01EEB8040	12	\N	t
912	river	NODR912	01010000A06A080000854C8AD2FA102941342FBF136DAB5841000000E051768140	10	\N	t
913	river	NODR913	01010000A06A080000459A1EFC9B1029416501130C70AB5841000000400A6D8140	10	\N	t
1047	river	NODR1047	01010000A06A0800002E4EC1CA451529415746334782AB584100000080C2BF8040	12	\N	t
914	river	NODR914	01010000A06A080000595007AD7E1029416501130C70AB584100000040E1748140	10	\N	t
915	river	NODR915	01010000A06A08000093DB46E8BB1029412EFAE0356FAB584100000060666A8140	10	\N	t
1048	river	NODR1048	01010000A06A080000F5CAB40E7C14294171A1FAB975AB5841000000C0CCE28040	12	\N	t
1049	river	NODR1049	01010000A06A0800009A2972A537142941469E30E573AB584100000020AEE78040	12	\N	t
916	elem_2d	PAV916	01010000A06A08000050A12839F2112941FADA1B967AAB5841000000400A678140	11	\N	t
917	elem_2d	PAV917	01010000A06A0800002036709A35122941E67C8CB37FAB5841000000608F648140	11	\N	t
918	elem_2d	PAV918	01010000A06A08000050E03210EE112941C04282EC76AB58410000002085638140	11	\N	t
919	elem_2d	PAV919	01010000A06A080000504F47BE0D12294172DB1B5679AB584100000040E1648140	11	\N	t
920	elem_2d	PAV920	01010000A06A08000080E3B77B37122941DA04782578AB58410000000029648140	11	\N	t
921	elem_2d	PAV921	01010000A06A08000080C1D600D3112941D8104F7973AB5841000000A099518140	11	\N	t
922	elem_2d	PAV922	01010000A06A080000C0B9FF5CCC112941EC22FDE079AB5841000000C0CC608140	11	\N	t
923	elem_2d	PAV923	01010000A06A080000C0A2D600251229410A44827C73AB584100000060664E8140	11	\N	t
924	elem_2d	PAV924	01010000A06A080000D03AEB2ED7112941B269DE4B7CAB584100000060666E8140	11	\N	t
949	elem_2d	PAV949	01010000A06A080000408651151F122941708A63E776AB58410000000000648140	11	\N	t
950	elem_2d	PAV950	01010000A06A08000010DC09B414122941B6F7A0E17FAB5841000000C01E7F8140	11	\N	t
951	elem_2d	PAV951	01010000A06A080000D0C4FFDCAE112941B68663D780AB5841000000400A778140	11	\N	t
952	elem_2d	PAV952	01010000A06A080000007328B96D1229411EE5F2D97AAB5841000000C0CC608140	11	\N	t
925	elem_2d	PAV925	01010000A06A08000000EA65C35C12294138BE965A75AB584100000060B8468140	11	\N	t
926	elem_2d	PAV926	01010000A06A08000090DFB7FB41122941FAFBA08174AB5841000000E051488140	11	\N	t
927	elem_2d	PAV927	01010000A06A080000F03BEB2ED4112941E075B51F77AB58410000004033638140	11	\N	t
928	elem_2d	PAV928	01010000A06A080000506AD680BB12294100DB1B867AAB58410000008014448140	11	\N	t
929	elem_2d	PAV929	01010000A06A080000A073F585E31129413A0C4FC97FAB5841000000C0F57E8140	11	\N	t
930	elem_2d	PAV930	01010000A06A0800001074CCA9FD1129418A2BD4347EAB584100000080EB798140	11	\N	t
931	elem_2d	PAV931	01010000A06A0800004049F5855412294142D1443279AB584100000060B8628140	11	\N	t
932	elem_2d	PAV932	01010000A06A08000060A784484F122941A2923A5B7CAB5841000000803D648140	11	\N	t
933	elem_2d	PAV933	01010000A06A080000601D709A771229413EDC1B3677AB5841000000A070458140	11	\N	t
934	elem_2d	PAV934	01010000A06A080000C02C66C3AA1129411E5630E479AB5841000000C0CC688140	11	\N	t
935	elem_2d	PAV935	01010000A06A0800003008701AB0122941BAB0BF067EAB584100000040E1528140	11	\N	t
936	elem_2d	PAV936	01010000A06A080000A00DE1D7AB1129411A4082FC7DAB5841000000C0CC708140	11	\N	t
937	elem_2d	PAV937	01010000A06A080000F044EB2EBC11294186BE968A74AB58410000000000668140	11	\N	t
938	elem_2d	PAV938	01010000A06A08000040B6D600F11129419AD344F272AB5841000000803D4E8140	11	\N	t
939	elem_2d	PAV939	01010000A06A08000090F9654333122941547E8CE37BAB5841000000E0A3648140	11	\N	t
940	elem_2d	PAV940	01010000A06A080000309DA34DAB1129412022FD007CAB5841000000A047698140	11	\N	t
941	elem_2d	PAV941	01010000A06A080000B0D0D680AA112941400F4FB977AB584100000000D7658140	11	\N	t
942	elem_2d	PAV942	01010000A06A080000E0E18E1F571229417C1626BD7FAB5841000000205C638140	11	\N	t
943	elem_2d	PAV943	01010000A06A080000D00766430D122941D05830B472AB584100000080144C8140	11	\N	t
944	elem_2d	PAV944	01010000A06A08000090E084C8B6112941F8F7A0317FAB584100000000D76F8140	11	\N	t
957	elem_2d	PAV957	01010000A06A0800001007E157BD112941C6C76D2E77AB58410000004033638140	11	\N	t
958	elem_2d	PAV958	01010000A06A080000F06A28398312294194EEC9ED7CAB5841000000C0CC508140	11	\N	t
959	elem_2d	PAV959	01010000A06A080000A04C7A719D122941E0C56D3E7CAB5841000000E07A4C8140	11	\N	t
960	elem_2d	PAV960	01010000A06A080000504FC2D2BB1129414874B55F7BAB5841000000A0705B8140	11	\N	t
961	elem_2d	PAV961	01010000A06A080000804FC252BB112941582DD46479AB584100000000295E8140	11	\N	t
962	elem_2d	PAV962	01010000A06A0800003049A34D8B12294154DB1BA679AB584100000060B8448140	11	\N	t
963	elem_2d	PAV963	01010000A06A08000080EFB77B171229410A4B59207CAB5841000000E0516E8140	11	\N	t
964	elem_2d	PAV964	01010000A06A080000801C473E95122941BE913ABB7EAB5841000000A047578140	11	\N	t
965	elem_2d	PAV965	01010000A06A08000000F0EAAE9E122941026BDECB78AB5841000000E0A3448140	11	\N	t
966	elem_2d	PAV966	01010000A06A080000E0878448A31229414A7C8C5381AB584100000060B8648140	11	\N	t
967	elem_2d	PAV967	01010000A06A0800004092CC29AD11294166FCA06173AB584100000040E1568140	11	\N	t
945	elem_2d	PAV945	01010000A06A080000108728393812294112D444B271AB584100000080C24D8140	11	\N	t
946	elem_2d	PAV946	01010000A06A0800005003E157C7112941C4AFBF9680AB5841000000803D768140	11	\N	t
947	elem_2d	PAV947	01010000A06A08000080B25115A911294188FBA0B175AB5841000000E0A3648140	11	\N	t
948	elem_2d	PAV948	01010000A06A080000B0BEE0577E122941649B114F80AB5841000000E0A3708140	11	\N	t
953	elem_2d	PAV953	01010000A06A0800008095A3CDBF112941CA7D8C537DAB5841000000608F628140	11	\N	t
954	elem_2d	PAV954	01010000A06A080000A093ADA468122941B88763277EAB58410000002085618140	11	\N	t
955	elem_2d	PAV955	01010000A06A08000090507AF192122941EECD441282AB58410000004033718140	11	\N	t
956	elem_2d	PAV956	01010000A06A08000060CC5BEC0712294120C86D3E76AB584100000080EB638140	11	\N	t
968	river	NODR968	01010000A06A08000094624E2DD311294109AB32ED6CAB5841000000400A418140	11	\N	t
969	river	NODR969	01010000A06A08000092B4BED563122941BAF0308271AB5841000000A0992D8140	11	\N	t
970	river	NODR970	01010000A06A08000052096B3CE8122941299836F966AB5841000000C01E1B8140	11	\N	t
971	river	NODR971	01010000A06A08000033FE90F982122941567D8A2B71AB584100000080EB298140	11	\N	t
972	river	NODR972	01010000A06A08000088659C5FF4112941522E1C026CAB5841000000C01E398140	11	\N	t
973	river	NODR973	01010000A06A080000B63F6B19121229417C9AC5DF6CAB584100000000D73F8140	11	\N	t
974	river	NODR974	01010000A06A080000300AC49DA8122941A685196970AB58410000000000248140	11	\N	t
975	river	NODR975	01010000A06A080000CD46BFA1B11129414D6F83056FAB584100000040E14A8140	11	\N	t
976	river	NODR976	01010000A06A08000057FB329A2C122941C06AB53E6EAB584100000060B8308140	11	\N	t
977	river	NODR977	01010000A06A080000F7FEC0AE47122941D22E1FDB6FAB5841000000803D388140	11	\N	t
1098	river	NODR1098	01010000A06A080000AEA5B255160F29416861A71487AB5841000000008087C340	14	\N	t
1099	river	NODR1099	01010000A06A0800005B6AD0BE370F294127754FD97AAB5841000000008087C340	14	\N	t
1100	river	NODR1100	01010000A06A0800007965A3B1AA0E2941C68B054689AB5841000000008087C340	14	\N	t
1101	river	NODR1101	01010000A06A0800008ADC3012600F29411048867176AB5841000000008087C340	14	\N	t
1102	river	NODR1102	01010000A06A080000198812B5DD0E29414B4A8A3488AB5841000000008087C340	14	\N	t
1103	river	NODR1103	01010000A06A08000069ED08177E0E2941166951428AAB5841000000008087C340	14	\N	t
1104	river	NODR1104	01010000A06A080000C45B31DF8B0F2941FA46F7F373AB5841000000008087C340	14	\N	t
1105	river	NODR1105	01010000A06A080000B05E0CBE500E2941D49DB30E8BAB5841000000008087C340	14	\N	t
1106	river	NODR1106	01010000A06A08000036342A29BB0F2941442D83D474AB5841000000008087C340	14	\N	t
\.


--
-- Name: _node_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('_node_id_seq', 1163, true);


--
-- Data for Name: _overflow_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _overflow_link (id, link_type, z_crest1, width1, z_crest2, width2, cc, lateral_contraction_coef, break_mode, z_break, t_break, z_invert, width_breach, grp, dt_fracw_array, border) FROM stdin;
1507	overflow	562.789978	0	564.679993	15.7722187	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000F0181F620F10294192D91B567EAB5841F6285C1F85B781401049529517102941148963877AAB58410000000000948140
1508	overflow	554.280029	0	554.589355	15.8904877	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A0800000200000040EAADA4811129416AB5BF8671AB5841295C8FC2F552814080268F1FA0112941FEB4BFA672AB58416666666666528140
1509	overflow	555.530029	0	555.530029	15.8904877	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000C071EBAE4411294138DF1B466FAB5841AE47E17A1454814000AECC2963112941CCDE1B6670AB5841000000C0F5548140
1510	overflow	557.080017	0	557.080017	14.9901628	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A0800000200000070683DE726112941FA30D4B46FAB58413D0AD7A370578140C071EBAE4411294138DF1B466FAB5841D7A3707D14548140
1511	overflow	558.859985	0	558.859985	16.3014259	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000B0B39976CB1029410C4F597071AB5841073A6DA0D3648140809DC252EB10294188ABE89270AB58415C8FC2F5285E8140
1512	overflow	557.369995	0	557.369995	15.0012531	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000D0A6701A09112941C659302470AB584166666696C25D814070683DE726112941FA30D4B46FAB58413D0AD7A370578140
1513	overflow	555.299988	0	555.299988	15.893322	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A0800000200000000AECC2963112941CCDE1B6670AB5841EB51B81E8553814040EAADA4811129416AB5BF8671AB5841295C8FC2F5528140
1514	overflow	558.789978	0	558.789978	14.9901628	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000809DC252EB10294188ABE89270AB58415C8FC2F5285E8140D0A6701A09112941C659302470AB5841CDCCCCCCCC5A8140
1515	overflow	557.940002	0	558.454712	16.3014259	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000C02729B98B1029411E6DDE2B73AB58415C8FC2F52872814090115295AB1029419AC96D4E72AB584100000080146C8140
1516	overflow	558.609985	0	559.147156	13.5595169	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000E0B6140B71102941A04E599072AB5841E8B481EEB4778140C02729B98B1029411E6DDE2B73AB5841AE47E18AC2738140
1517	overflow	557.299988	0	557.596497	16.3133221	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A0800000200000090115295AB1029419AC96D4E72AB584158F28B65C96B8140B0B39976CB1029410C4F597071AB5841073A6DA0D3648140
1738	overflow	544.900024	0	545.109985	16.0219975	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000E01C5BECDB132941264382DC75AB58419A99999999F7804060B5CB29FA132941DA75B52F77AB5841A4703D9A19078140
1502	overflow	561.309998	0	561.544983	11.6877928	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A0800000200000000F199F6271029410E114FE972AB58410AD7A3703D8E8140708D0AB43B1029419A3AAB5871AB58415C8FC2E5518A8140
1503	overflow	561.919983	0	562.081848	15.7722187	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000E0C066C31F1029418C6107B876AB5841000000A07096814000F199F6271029410E114FE972AB58410AD7A3703D8E8140
1504	overflow	559.330017	0	559.837158	13.5514755	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A0800000200000050FE1E6256102941185930F471AB58414444444444828140E0B6140B71102941A04E599072AB5841D069039D367A8140
1505	overflow	562.159973	0	562.347473	15.7792807	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A080000020000001049529517102941148963877AAB5841F6285C8FC2A98140E0C066C31F1029418C6107B876AB584100000000009C8140
1506	overflow	560.450012	0	560.833557	13.5595169	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000708D0AB43B1029419A3AAB5871AB5841B81E85EB518A814050FE1E6256102941185930F471AB5841000000E051828140
1609	overflow	552.109985	0	552.746887	15.7849197	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000D05DA34D5412294112DE1B5672AB584190C2F5E87A438140B0D88E9F6F122941F84D595074AB5841E17A14AE47478140
1610	overflow	552.369995	0	552.503174	18.9060326	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000C0838448AE12294132E6F2F977AB5841EC51B81E85428140A0FB6F9AD112294190E5F2A979AB58419A999989C23F8140
1611	overflow	552.76001	0	552.831909	15.779911	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000B0D88E9F6F122941F84D595074AB5841703D0AC7F546814090537AF18A122941D4E6F24976AB5841A4703D0AD7458140
1612	overflow	559.51001	0	559.51001	8.61488247	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000601BB87BA211294186A7E8427BAB58415A2D6FBCBC758140503A3DE7A1112941E46ADE1B79AB58419F5D406C2A6F8140
1613	overflow	560.950012	0	560.950012	8.61488247	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000D095CCA9A3112941CA20FD907FAB58416C2A0D49CE838140C0B45115A311294128E4F2697DAB5841C6577CC557828140
1614	overflow	553.940002	0	554.040588	16.6784325	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A080000020000007055701AE21129416CDE1B6671AB58417B14AE47E14A8140C090511503122941A8DE1BC670AB584185EB51B81E478140
1615	overflow	560.47998	0	560.47998	8.61522484	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000C0B45115A311294128E4F2697DAB5841E32BBEE22B7A8140601BB87BA211294186A7E8427BAB5841B35ADE18C1788140
1616	overflow	558.47998	0	558.47998	8.61522484	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000503A3DE7A1112941E46ADE1B79AB5841D02E2016106E8140F0A0A34DA1112941422ED4F476AB58418C60A2BF93658140
1617	overflow	553.929993	0	554.068542	13.5049171	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000C090511503122941A8DE1BC670AB584185EB51B81E4781400016140B1E12294174FDA09170AB5841703D0AD7A3488140
1618	overflow	552.5	0	552.776855	18.9153748	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A0800000200000090537AF18A122941D4E6F24976AB584152B81E45E1458140C0838448AE12294132E6F2F977AB5841EC51B81E85428140
1619	overflow	555.469971	0	555.469971	8.61522484	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000E0BF28B9A0112941A0F1C9CD74AB5841BC318259CB5C814080268F1FA0112941FEB4BFA672AB5841000000400A558140
1620	overflow	553.789978	0	554.553528	16.6784325	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A0800000200000080268F1FA0112941FEB4BFA672AB5841000000400A558140D061701AC11129413AB5BF0672AB58410000000029568140
1621	overflow	553.380005	0	553.92926	13.4955473	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A080000020000000016140B1E12294174FDA09170AB5841B81E85AB474D8140F0E2B7FB381229413645825C70AB5841AE47E13A0A4A8140
1622	overflow	551.570007	0	552.415649	15.779911	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000F0E2B7FB381229413645825C70AB5841AE47E13A0A4A8140D05DA34D5412294112DE1B5672AB5841000000803D3E8140
1623	overflow	557.320007	0	557.320007	8.61488247	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000F0A0A34DA1112941422ED4F476AB58414630D14F07668140E0BF28B9A0112941A0F1C9CD74AB584179630413FD5B8140
1624	overflow	559.909973	0	560.536682	8.61522484	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000302F6643A41129416C5D07B881AB5841F6285C1F85958140D095CCA9A3112941CA20FD907FAB5841D9541A72EE8B8140
1608	overflow	554.140015	0	554.359009	16.6898003	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000D061701AC11129413AB5BF0672AB5841703D0AD7A34E81407055701AE21129416CDE1B6671AB58417B14AE47E14A8140
1737	overflow	543.640015	0	544.064697	14.4096146	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000104A6F1AAB14294186E3F2197FAB58410000000000000000C049463EC7142941AC20FDE07FAB584185EB51B81EFD7040
1739	overflow	545.820007	0	547.526367	68.8387146	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000F0628E9FA913294176F5A0E185AB5841000000A0702D8140E01C5BECDB132941264382DC75AB5841CDCCCC2C5CFF8040
1740	overflow	541.469971	0	542.474121	69.4201584	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A080000020000002091FE5CE3142941D25D07A880AB5841C2F5289C70F48040D05DCBA9E3142941FE9EE80292AB5841A4703D0AD7F98040
1741	overflow	544.849976	0	545.355286	16.0219975	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A0800000200000060B5CB29FA132941DA75B52F77AB584148E17A14AE068140E04D3C67181429418EA8E88278AB58410AD7A3703D068140
1742	overflow	541.5	0	542.700684	14.3998508	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000C049463EC7142941AC20FDE07FAB584185EB51B81EFD70402091FE5CE3142941D25D07A880AB584185EB51B81EFD8040
1743	overflow	543.549988	0	543.676514	19.488039	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000F0DC5A6C86142941C85430747DAB58410AD7A3703D066140104A6F1AAB14294186E3F2197FAB58410000000000000000
1744	overflow	544.52002	0	544.661987	19.488039	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A0800000200000000BB50153D1429414C37AB287AAB58418FC2F5285C897940202865C3611429410AC66DCE7BAB58410AD7A3703D067140
1745	overflow	543.73999	0	543.885681	13.8919039	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000D05DCBA9E3142941FE9EE80292AB5841A4703D0AD7F980406058CB29F214294148044FF994AB584152B81E85EB088140
1746	overflow	543.659973	0	543.765015	13.8885994	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A080000020000006058CB29F214294148044FF994AB584152B81E85EB088140400BEAAE00152941889211EF97AB58417B14AE57B8008140
1747	overflow	543.849976	0	544.124023	19.4786282	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000202865C3611429410AC66DCE7BAB58410AD7A3703D067140F0DC5A6C86142941C85430747DAB58410AD7A3703D066140
1748	overflow	544.859985	0	545.119995	19.488039	0.600000024	1	none	\N	\N	\N	\N	1	\N	01020000A06A08000002000000E04D3C67181429418EA8E88278AB58410AD7A3703D06814000BB50153D1429414C37AB287AAB584148E17AD47ACB7D40
\.


--
-- Data for Name: _param_headloss_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _param_headloss_singularity (id, singularity_type, q_dz_array) FROM stdin;
\.


--
-- Data for Name: _pipe_branch_marker_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _pipe_branch_marker_singularity (id, singularity_type, pk0_km, dx) FROM stdin;
\.


--
-- Data for Name: _pipe_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _pipe_link (id, link_type, comment, z_invert_up, z_invert_down, cross_section_type, h_sable, branch, rk, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom) FROM stdin;
1751	pipe		557.599976	553.400024	circular	0	1	1	50	\N	\N	\N	\N	\N
1752	pipe		553.400024	551.099976	circular	0	1	50	1	\N	\N	\N	\N	\N
1753	pipe		551.099976	550.5	circular	0	1	50	1	\N	\N	\N	\N	\N
1750	pipe		558.5	557.599976	circular	0	1	50	1	\N	\N	\N	\N	\N
\.


--
-- Data for Name: _porous_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _porous_link (id, link_type, z_invert, width, transmitivity, border) FROM stdin;
\.


--
-- Data for Name: _pump_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _pump_link (id, link_type, npump, zregul_array, hq_array) FROM stdin;
\.


--
-- Data for Name: _qq_split_hydrology_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _qq_split_hydrology_singularity (id, singularity_type, qq_array, downstream, downstream_type, split1, split1_type, split2, split2_type) FROM stdin;
\.


--
-- Data for Name: _regul_gate_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _regul_gate_link (id, link_type, z_invert, z_ceiling, width, cc, action_gate_type, z_invert_stop, z_ceiling_stop, v_max_cms, dt_regul_hr, mode_regul, z_control_node, z_pid_array, z_tz_array, q_z_crit, q_tq_array, nr_z_gate) FROM stdin;
\.


--
-- Data for Name: _regul_sluice_gate_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _regul_sluice_gate_singularity (id, singularity_type, z_invert, z_ceiling, width, cc, action_gate_type, z_invert_stop, z_ceiling_stop, v_max_cms, dt_regul_hr, mode_regul, z_control_node, z_pid_array, z_tz_array, q_z_crit, q_tq_array, nr_z_gate) FROM stdin;
\.


--
-- Data for Name: _reservoir_rs_hydrology_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _reservoir_rs_hydrology_singularity (id, singularity_type, drainage, drainage_type, overflow, overflow_type, q_drainage, z_ini, zs_array, treatment_mode, treatment_param) FROM stdin;
\.


--
-- Data for Name: _reservoir_rsp_hydrology_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _reservoir_rsp_hydrology_singularity (id, singularity_type, drainage, drainage_type, overflow, overflow_type, z_ini, zr_sr_qf_qs_array, treatment_mode, treatment_param) FROM stdin;
\.


--
-- Data for Name: _river_cross_section_pl1d; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _river_cross_section_pl1d (id, name, profile, generated) FROM stdin;
\.


--
-- Data for Name: _river_cross_section_profile; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _river_cross_section_profile (id, name, z_invert_up, z_invert_down, type_cross_section_up, type_cross_section_down, up_rk, up_rk_maj, up_sinuosity, up_circular_diameter, up_ovoid_height, up_ovoid_top_diameter, up_ovoid_invert_diameter, up_cp_geom, up_op_geom, up_vcs_geom, up_vcs_topo_geom, down_rk, down_rk_maj, down_sinuosity, down_circular_diameter, down_ovoid_height, down_ovoid_top_diameter, down_ovoid_invert_diameter, down_cp_geom, down_op_geom, down_vcs_geom, down_vcs_topo_geom, validity) FROM stdin;
4	CP4	537.070007	\N	valley	\N	25	12	1	\N	\N	\N	\N	\N	\N	20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
5	CP5	\N	536.179993	\N	valley	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25	12	1	\N	\N	\N	\N	\N	\N	21	\N	t
1	CP1	\N	569.659973	\N	valley	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25	12	1	\N	\N	\N	\N	\N	\N	3	\N	t
7	CP7	542.52002	542.52002	valley	pipe	25	12	1	\N	\N	\N	\N	\N	\N	4	\N	35	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	t
8	CP8	541.549988	541.549988	pipe	valley	35	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	25	12	1	\N	\N	\N	\N	\N	\N	6	\N	t
9	CP9	559.859985	\N	valley	\N	25	12	1	\N	\N	\N	\N	\N	\N	7	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
10	CP10	558.619995	\N	valley	\N	25	12	1	\N	\N	\N	\N	\N	\N	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
11	CP11	550.909973	\N	valley	\N	25	12	1	\N	\N	\N	\N	\N	\N	9	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
12	CP12	550.119995	\N	valley	\N	25	12	1	\N	\N	\N	\N	\N	\N	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
13	CP13	545.859985	\N	valley	\N	25	12	1	\N	\N	\N	\N	\N	\N	11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
14	CP14	539.950012	\N	valley	\N	25	12	1	\N	\N	\N	\N	\N	\N	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
2	CP2	537.210022	\N	valley	\N	25	12	1	\N	\N	\N	\N	\N	\N	13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
16	CP16	\N	536.109985	\N	valley	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25	12	1	\N	\N	\N	\N	\N	\N	15	\N	t
6	CP6	534.25	\N	valley	\N	25	12	1	\N	\N	\N	\N	\N	\N	16	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
3	CP3	\N	544.169983	\N	valley	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25	12	1	\N	\N	\N	\N	\N	\N	18	\N	t
15	CP15	540.409973	\N	valley	\N	25	12	1	\N	\N	\N	\N	\N	\N	19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
\.


--
-- Data for Name: _river_node; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _river_node (id, node_type, reach, z_ground, area) FROM stdin;
1	river	1	569.77002	1
2	river	1	537.27002	1
3	river	2	544.190002	1
4	river	2	536.549988	1
5	river	3	536.25	1
6	river	3	534.219971	1
7	river	1	542.539978	1
8	river	1	542.22998	1
9	river	1	560.419983	1
10	river	1	560.169983	1
11	river	1	551.900024	1
12	river	1	550.289978	1
13	river	1	546.02002	1
14	river	1	540.02002	1
15	river	2	540.460022	1
16	river	3	535.830017	1
23	river	1	550.98999	1
24	river	1	540.52002	1
900	river	1	559.450012	1
901	river	1	558.76001	1
902	river	1	558.690002	1
903	river	1	559.390015	1
904	river	1	559.98999	1
905	river	1	560.340027	1
906	river	1	554.190002	1
907	river	1	555.530029	1
908	river	1	557.080017	1
909	river	1	558.859985	1
910	river	1	557.369995	1
911	river	1	555.299988	1
912	river	1	558.789978	1
913	river	1	557.630005	1
914	river	1	558.609985	1
915	river	1	557.299988	1
968	river	1	552.130005	1
969	river	1	549.700012	1
970	river	1	547.390015	1
971	river	1	549.23999	1
972	river	1	551.140015	1
973	river	1	551.97998	1
974	river	1	548.5	1
975	river	1	553.359985	1
976	river	1	550.090027	1
977	river	1	551.030029	1
1040	river	1	540.630005	1
1041	river	1	542.880005	1
1042	river	1	546.059998	1
1043	river	1	541.369995	1
1044	river	1	540.169983	1
1045	river	1	539.98999	1
1046	river	1	541.390015	1
1047	river	3	535.969971	1
1048	river	1	540.349976	1
1049	river	1	540.960022	1
1098	river	1	9999	1
1099	river	1	9999	1
1100	river	1	9999	1
1101	river	1	9999	1
1102	river	1	9999	1
1103	river	1	9999	1
1104	river	1	9999	1
1105	river	1	9999	1
1106	river	1	9999	1
\.


--
-- Data for Name: _routing_hydrology_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _routing_hydrology_link (id, link_type, cross_section, length, slope, hydrograph) FROM stdin;
1758	routing_hydrology	1	0.100000001	0.00999999978	13
1759	routing_hydrology	1	0.100000001	0.00999999978	11
1760	routing_hydrology	1	0.100000001	0.00999999978	12
\.


--
-- Data for Name: _singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _singularity (id, singularity_type, name, node, node_type, configuration, validity) FROM stdin;
13	hydrograph_bc	HY13	1050	manhole	\N	t
11	hydrograph_bc	HY11	1	river	\N	t
8	bridge_headloss	BRDG8	23	river	\N	t
12	hydrograph_bc	HY12	3	river	\N	t
9	bridge_headloss	BRDG9	24	river	\N	t
10	strickler_bc	RK10	6	river	\N	t
\.


--
-- Name: _singularity_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('_singularity_id_seq', 13, true);


--
-- Data for Name: _station_node; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _station_node (id, node_type, area, z_invert, station) FROM stdin;
\.


--
-- Data for Name: _storage_node; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _storage_node (id, node_type, zs_array, zini, contour) FROM stdin;
\.


--
-- Data for Name: _street_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _street_link (id, link_type, width, length, rk) FROM stdin;
\.


--
-- Data for Name: _strickler_bc_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _strickler_bc_singularity (id, singularity_type, slope, k, width) FROM stdin;
10	strickler_bc	0.0299999993	25	20
\.


--
-- Data for Name: _strickler_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _strickler_link (id, link_type, z_crest1, width1, length, rk, z_crest2, width2, border) FROM stdin;
\.


--
-- Data for Name: _tank_bc_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _tank_bc_singularity (id, singularity_type, zs_array, zini) FROM stdin;
\.


--
-- Data for Name: _tz_bc_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _tz_bc_singularity (id, singularity_type, tz_array, cyclic, external_file_data) FROM stdin;
\.


--
-- Data for Name: _weir_bc_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _weir_bc_singularity (id, singularity_type, z_weir, width, cc) FROM stdin;
\.


--
-- Data for Name: _weir_link; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _weir_link (id, link_type, z_invert, width, cc, z_weir, v_max_cms) FROM stdin;
\.


--
-- Data for Name: _zq_bc_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _zq_bc_singularity (id, singularity_type, zq_array) FROM stdin;
\.


--
-- Data for Name: _zq_split_hydrology_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _zq_split_hydrology_singularity (id, singularity_type, downstream, downstream_type, split1, split1_type, split2, split2_type, downstream_law, downstream_param, split1_law, split1_param, split2_law, split2_param) FROM stdin;
\.


--
-- Data for Name: _zregul_weir_singularity; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY _zregul_weir_singularity (id, singularity_type, z_invert, z_regul, width, cc, mode_regul, reoxy_law, reoxy_param) FROM stdin;
\.


--
-- Data for Name: bank; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY bank (id, name, geom) FROM stdin;
\.


--
-- Name: bank_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('bank_id_seq', 1, false);


--
-- Data for Name: branch; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY branch (id, name, pk0_km, dx, geom) FROM stdin;
1	BRANCH_1	0	50	01020000A06A08000005000000936FB6F9A613294147ACC51394AB58410AD7A3703D808140D4F36E7C2D1329410F971C138BAB5841CDCCCCCCCC7881400FB8AEE8BE122941AA9A209E82AB5841F6285C8FC2578140EBFD46138912294196218E5778AB5841CDCCCCCCCC4481409A09863B33122941020EA12873AB584114AE47E17A4A8140
\.


--
-- Data for Name: catchment; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY catchment (id, name, geom) FROM stdin;
3	CATCHMENT_41	01030000206A080000010000000A000000AE026415DB142941DAECE0A49BAB5841DB06081A7B142941B4409146B5AB5841921E7AA4C9132941565B625FCFAB5841EAAA30B5AD12294195E0B113E6AB5841D3BBE98BEB11294163E76AC7F6AB58419DA005BDA8112941EF5B7515F8AB5841220B4A20111229411AD612ABB8AB58418005B7D85112294178BB41929EAB5841512E0D0FB612294160347F8379AB5841AE026415DB142941DAECE0A49BAB5841
2	CATCHMENT_40	01030000206A08000001000000600000007E0D26ADBDE728411C5D9B8149AC5841FDC47ECCA8E72841E241B7B206AC5841314F14DAC1E72841C0DE3EB9D7AB584166D9A9E7DAE7284183B6FB389CAB5841BE6560F8BEE628414081590768AB58410148057C01E428411811825C40AB5841A91C389822E12841633E456E12AB5841442AAAFF43E12841FA291A53E0AA58419472260B81DF28417D5D83629AAA584157A456F8C0DE28410091EC7154AA5841C4DA1E9646DF2841CF8267540AAA5841C9FCBB189ADF284100EB5FDFCFA95841033A3D6A30E0284176040B8C98A95841B1C15BCACEDD2841126CF06035A958416E40A07391DC28412C8B2E55BDA8584157054025C6DA28416AF8A67119A8584121EA5B5683DA28419A609FFCDEA75841E83D29C616DA2841F7B47F229BA758417F29FEAAE4D92841C0999B5358A75841B4B393B8FDD928415785703826A75841250CF9D8D6DA2841C891BC33F6A65841D7C37CCD99DC284157703267CBA658411CD686E500DE2841C6EB2FA171A658418FBF3AC703DF2841E8A81B081CA6584148BBF8C06DE128419DD5CB63C5A55841F6E12DF406E3284108D5B8AD9CA558419A33DA60CFE32841BC16A7DAA0A5584110AEDC03FCE4284102C859FCA3A55841B49E9F43BFE92841EC7E9F6566A55841061781E320EC2841633ED7A4B3A5584155FE13C258EE284196DDAA8327A658415ABFC717A7F228417EA92EBE44A65841F98DEDD416F7284145A388C05CA65841BACDE5D327FA28419AFF47067BA65841CF77F760C9FB284150D284F4A8A658410ED71535B3FC2841AB26651A65A65841F5A97DF8B8FE2841149503A312A658416DB5CE5C0F0029416ADC8417D6A5584180CE9128870129418F2ABF3FAAA5584117BA660D5501294144FDFB2DD8A55841EA730B05E301294159B56703ECA55841F54894CBB30229414E2CF86124A65841F126F748600229412662AD4981A658419709F276520329416FA4AE2CAEA65841DFAC4A50E3042941379E082FC6A65841A0EC424FF40729417EE00912F3A658415657B287340A2941B46A9F1F0CA75841919433D9CA0A294198BA126A5AA75841D0F351ADB40B29416AE368A0BEA75841A63E45666C0C2941863982E8F4A75841FA4775C7F70E2941B64707063FA85841028CAFCC9E0F2941CC90C19C7CA858417253C62B4E10294135A5ECB7AEA858411736C1594011294117641141D3A85841B1B2E4FF37112941929F5970EFA85841207AFB5EE7112941F5A625DA28A95841262DE7A2641229410F6CF06035A958411122ECE8BD142941AD0AB18980A9584135C323BF83172941BD460C6FC5A95841AB3D2662B0182941DFA98468F4A95841E70BF674701929418355A44238AA5841EEBEE1B8ED1929419B2FAD9A9FAA5841873B055FE51929415393384AF7AA5841CD462BEE6319294191F983421AAB5841DD9756DBA4142941E5D9329869AB58418A2659C42B1429417B87FF0450AB58414D5889B16B132941211EE10D39AB584107467F99041229417F0310F51EAB5841B9EF3A7CF60F2941C5B4C21622AB5841DB600DBE970E29411E8D924C0FAB584161C46D98170D29418DAE1C193AAB58414A5FD03D130C294121CD053048AB5841E30531F0430B2941B38DA50238AB5841C77C35F2AD092941E6BC08F62EAB584163B6A5864609294134D64CE34CAB58417CE33DC3400729411695716C71AB5841D04D5751D10529417FA99C87A3AB584156B1B72B51042941FAE4E4B6BFAB5841DD141806D1022941A20C1581D2AB58411BD51F07C0FF284196F2561EE1AB5841D00F2AABDBFD284129621B13E0AB58416048134C2CFD28411219617CA2AB58418D8E6E549EFC2841BF4DF0F7ADAB5841DA459C9EB1FA2841A7197432CBAB584187CDBAFE4FF82841347CD975D1AB584146DD4D693CF72841BBD1DF07DFAB5841632C83288AF52841E441B7B206AC584120ABC7D14CF428417942CA682FAC58413F8B4B52C4F22841F5ECC3D621AC5841242E4E81A5F028413D2FC5B94EAC58411CEA137CFEEF2841E456F58361AC584170542D0A8FEE2841649F9C6476AC5841EE7353DF67EC2841FD1BC00A6EAC584170B5163794EA2841F70E615975AC5841FA3A149467E928410A367E6D5FAC58417E0D26ADBDE728411C5D9B8149AC5841
\.


--
-- Name: catchment_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('catchment_id_seq', 4, true);


--
-- Data for Name: closed_parametric_geometry; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY closed_parametric_geometry (id, name, zbmin_array) FROM stdin;
1	CP_8	{{0,3},{0.5,4},{2,4},{2.5,3.5},{2.75,2},{3,0}}
\.


--
-- Name: closed_parametric_geometry_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('closed_parametric_geometry_id_seq', 1, true);


--
-- Data for Name: configuration; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY configuration (id, name, description) FROM stdin;
1	default	\N
\.


--
-- Name: configuration_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('configuration_id_seq', 1, true);


--
-- Data for Name: coverage; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY coverage (id, domain_type, geom) FROM stdin;
1	2d	01030000A06A0800000100000024000000302F6643A41129416C5D07B881AB5841EC51B81E85958140D095CCA9A3112941CA20FD907FAB5841D9541A72EE8B8140C0B45115A311294128E4F2697DAB5841C6577CC557828140601BB87BA211294186A7E8427BAB5841B35ADE18C1788140503A3DE7A1112941E46ADE1B79AB58419F5D406C2A6F8140F0A0A34DA1112941422ED4F476AB58418C60A2BF93658140E0BF28B9A0112941A0F1C9CD74AB584179630413FD5B814080268F1FA0112941FEB4BFA672AB5841666666666652814040EAADA4811129416AB5BF8671AB5841295C8FC2F552814000AECC2963112941CCDE1B6670AB5841EB51B81E85538140C071EBAE4411294138DF1B466FAB5841AE47E17A1454814070683DE726112941FA30D4B46FAB58413D0AD7A370578140D0A6701A09112941C659302470AB5841CDCCCCCCCC5A8140809DC252EB10294188ABE89270AB58415C8FC2F5285E8140B0B39976CB1029410C4F597071AB5841073A6DA0D364814090115295AB1029419AC96D4E72AB5841B1E4174B7E6B8140C02729B98B1029411E6DDE2B73AB58415C8FC2F528728140E0B6140B71102941A04E599072AB5841D069039D367A814050FE1E6256102941185930F471AB58414444444444828140708D0AB43B1029419A3AAB5871AB5841B81E85EB518A814000F199F6271029410E114FE972AB58410AD7A3703D8E8140E0C066C31F1029418C6107B876AB584100000000009C81401049529517102941148963877AAB5841F6285C8FC2A98140F0181F620F10294192D91B567EAB5841EC51B81E85B78140907D5C6C2F102941B4BA96BA7EAB5841F6285C8FC2B3814030E299764F102941D69B111F7FAB58410000000000B08140000BCD296B1029413A8763777FAB58417777777777B1814020EC1EE286102941A84959D07FAB5841EFEEEEEEEEB28140F0145295A21029410C35AB2880AB58416666666666B48140D04D3310CA1029410C35AB2880AB584152B81E856BAE8140003F3390F11029410C35AB2880AB58413D0AD7A370A88140E077140B191129410C35AB2880AB5841285C8FC275A28140C0B0F585401129410C35AB2880AB584114AE47E17A9C8140204866C3611129412CEDC9AD80AB58415C8FC2F5289A8140D097F505831129414CA5E83281AB5841A4703D0AD7978140302F6643A41129416C5D07B881AB5841EC51B81E85958140
2	2d	01030000A06A080000010000001D00000080268F1FA0112941FEB4BFA672AB58416666666666528140E0BF28B9A0112941A0F1C9CD74AB584179630413FD5B8140F0A0A34DA1112941422ED4F476AB58418C60A2BF93658140503A3DE7A1112941E46ADE1B79AB58419F5D406C2A6F8140601BB87BA211294186A7E8427BAB5841B35ADE18C1788140C0B45115A311294128E4F2697DAB5841C6577CC557828140D095CCA9A3112941CA20FD907FAB5841D9541A72EE8B8140302F6643A41129416C5D07B881AB5841EC51B81E85958140206D473EBE1129416C5D07B881AB5841E27A14AE4792814010AB2839D81129416C5D07B881AB5841D7A3703D0A8F8140C0BBADA4FD11294164AFBF9681AB5841703D0AD7A38581402014140B231229415C01787581AB58410AD7A3703D7C8140004DF5854A1229415453305481AB584100000000007F8140E085D600721229414CA5E83281AB5841F6285C8FC28181401073FFDC88122941B47B8CE382AB58415C8FC2F528848140406028B99F122941127B8C9384AB5841C3F5285C8F868140F0ABE057B01229413C4959F080AB5841935F2CF9C56E8140F0AFB7FBC01229416617264D7DAB584163C92F96FC568140A0FB6F9AD112294190E5F2A979AB584133333333333F8140C0838448AE12294132E6F2F977AB5841EC51B81E8542814090537AF18A122941D4E6F24976AB5841A4703D0AD7458140B0D88E9F6F122941F84D595074AB5841E17A14AE47478140D05DA34D5412294112DE1B5672AB58411F85EB51B8488140F0E2B7FB381229413645825C70AB58415C8FC2F5284A81400016140B1E12294174FDA09170AB5841703D0AD7A3488140C090511503122941A8DE1BC670AB584185EB51B81E4781407055701AE21129416CDE1B6671AB58417B14AE47E14A8140D061701AC11129413AB5BF0672AB5841703D0AD7A34E814080268F1FA0112941FEB4BFA672AB58416666666666528140
8	2d	01030000A06A0800000100000019000000E01C5BECDB132941264382DC75AB58419A99999999F78040F0628E9FA913294176F5A0E185AB5841E17A14AE472B8140A080EAAEC7132941C8798C0388AB5841B40B884DA5298140509E46BEE51329411027D4248AAB5841869CFBEC0228814000BCA2CD0314294162ABBF468CAB5841592D6F8C60268140B0D9FEDC21142941B42FAB688EAB58412CBEE22BBE24814060F75AEC3F14294106B4968A90AB5841FF4E56CB1B2381401015B7FB5D1429414E61DEAB92AB5841D1DFC96A79218140C032130B7C142941A0E5C9CD94AB5841A4703D0AD71F8140204898F694142941489DE89296AB584186EB51B81ED57640805D1DE2AD142941FA2BAB5898AB584186EB51B81ED56640E072A2CDC6142941A2E3C91D9AAB58410000000000000000103F46BEE314294190CF1B0699AB5841F6285C8FC2077140400BEAAE00152941889211EF97AB5841F6285C8FC20781406058CB29F214294148044FF994AB584152B81E85EB088140D05DCBA9E3142941FE9EE80292AB5841A4703D0AD7F980402091FE5CE3142941D25D07A880AB584185EB51B81EFD8040C049463EC7142941AC20FDE07FAB584185EB51B81EFD7040104A6F1AAB14294186E3F2197FAB58410000000000000000F0DC5A6C86142941C85430747DAB58410AD7A3703D066140202865C3611429410AC66DCE7BAB58410AD7A3703D06714000BB50153D1429414C37AB287AAB58418FC2F5285C897940E04D3C67181429418EA8E88278AB58410AD7A3703D06814060B5CB29FA132941DA75B52F77AB584148E17A14AE068140E01C5BECDB132941264382DC75AB58419A99999999F78040
3	reach	01030000A06A0800000100000015000000D0E8CDA91B0E2941E834AB8880AB58413D0AD7A3701B824010FA52953F0E2941A41226FD89AB58418FC2F5285CD58140A00B015D470E29413830AB088DAB584185EB51B81ECF8140F0C7F605580E29412493114F96AB5841713D0AD7A3E4814080663E67810E2941F4748CE394AB5841EC51B81E85E78140100586C8AA0E2941BA7F637793AB58416666666666EA8140A0A3CD29D40E29418A61DE0B92AB5841E17A14AE47ED8140F07148BE060F294174D31BA68EAB58413E0AD7A370EF81404040C352390F29415E4559408BAB58419A99999999F18140C064F685600F29412065DE7B88AB5841B81E85EB51EC8140408929B9870F2941EC5B07B885AB5841D7A3703D0AE78140205BA4CDB00F2941EE5C070883AB5841E27A14AE47DA814050E53DE7D90F2941F05D075880AB5841EC51B81E85CD8140F0181F620F10294192D91B567EAB5841EC51B81E85B78140F0181F620F102941966DDEEB71AB58413333333333878140300A711A00102941D84582AC6EAB5841295C8FC2F58C8140209A3390FE0F2941AE5859C057AB5841713D0AD7A3DA814070FE3DE7960F2941A82DFD405DAB584152B81E85EBFD814010EA0AB4440F29413490638767AB58411F85EB51B802824060075DECBF0E2941EE0F4FE975AB58413D0AD7A3700B8240D0E8CDA91B0E2941E834AB8880AB58413D0AD7A3701B8240
4	reach	01030000A06A080000010000001C000000C090511503122941A8DE1BC670AB584185EB51B81E478140005E1EE20112294188A111EF6FAB584185EB51B81E3F8140F065F5050812294160E11B8669AB584185EB51B81E3F8140D06ECCA90B122941E0D16D3E5CAB58410000000000618140B077A34D0F12294160C2BFF64EAB58417B14AE47E18281406011E1D7A111294124F4F2C952AB5841A4703D0AD788814010AB1E6234112941F2FCC99D56AB5841CDCCCCCCCC8E8140D0C547BED1102941486C07185AAB5841F6285C8FC2A3814080EC1EE285102941D280B5EF59AB584185EB51B81EB38140304529393D102941AE5859C057AB5841CDCCCCCCCCC28140604529B93C102941E632D4946AAB58417B14AE47E1888140708D0AB43B102941DEC0964A6EAB58417B14AE47E1888140708D0AB43B1029419A3AAB5871AB5841B81E85EB518A814050FE1E6256102941185930F471AB58414444444444828140E0B6140B71102941A04E599072AB5841D069039D367A8140C02729B98B1029411E6DDE2B73AB58415C8FC2F52872814090115295AB1029419AC96D4E72AB5841B1E4174B7E6B8140B0B39976CB1029410C4F597071AB5841073A6DA0D3648140809DC252EB10294188ABE89270AB58415C8FC2F5285E8140D0A6701A09112941C659302470AB5841CDCCCCCCCC5A814070683DE726112941FA30D4B46FAB58413D0AD7A370578140C071EBAE4411294138DF1B466FAB5841AE47E17A1454814000AECC2963112941CCDE1B6670AB5841EB51B81E8553814040EAADA4811129416AB5BF8671AB5841295C8FC2F552814080268F1FA0112941FEB4BFA672AB58416666666666528140D061701AC11129413AB5BF0672AB5841703D0AD7A34E81407055701AE21129416CDE1B6671AB58417B14AE47E14A8140C090511503122941A8DE1BC670AB584185EB51B81E478140
5	reach	01030000A06A080000010000000B000000B077A34D0F12294160C2BFF64EAB58417B14AE47E1828140D06ECCA90B122941E0D16D3E5CAB58410000000000618140F065F5050812294160E11B8669AB584185EB51B81E3F8140005E1EE20112294188A111EF6FAB584185EB51B81E3F8140C090511503122941A8DE1BC670AB584185EB51B81E4781400016140B1E12294174FDA09170AB5841703D0AD7A3488140F0E2B7FB381229413645825C70AB58415C8FC2F5284A81407034701A3A12294100848CC36CAB58416666666666448140B097D68042122941B437D4C45DAB58413D0AD7A370628140F0FA3CE74A1229415E1478C54EAB584114AE47E17A808140B077A34D0F12294160C2BFF64EAB58417B14AE47E1828140
6	reach	01030000A06A0800000100000010000000A0FB6F9AD112294190E5F2A979AB584133333333333F8140400DF585F4122941CE36AB787BAB58419A999999993B8140C081E0D720132941F04F59106FAB58411F85EB51B82B814040F6CB294D1329411C40ABA862AB5841A4703D0AD71B814040F6CB294D13294130C6961A60AB5841A4703D0AD71B8140D02CD6805F132941A62FFDF057AB5841AE47E17A14788140E0930934D512294102A23A5B53AB5841E17A14AE477C8140F0FA3CE74A1229415E1478C54EAB584114AE47E17A808140B097D68042122941B437D4C45DAB58413D0AD7A3706281407034701A3A12294100848CC36CAB58416666666666448140F0E2B7FB381229413645825C70AB58415C8FC2F5284A8140D05DA34D5412294112DE1B5672AB58411F85EB51B8488140B0D88E9F6F122941F84D595074AB5841E17A14AE4747814090537AF18A122941D4E6F24976AB5841A4703D0AD7458140C0838448AE12294132E6F2F977AB5841EC51B81E85428140A0FB6F9AD112294190E5F2A979AB584133333333333F8140
7	reach	01030000A06A080000010000000B000000400DF585F4122941CE36AB787BAB58419A999999993B8140F0628E9FA913294176F5A0E185AB5841E17A14AE472B8140E01C5BECDB132941264382DC75AB58419A99999999F78040B03AE057DE1329410CDE1B6672AB58411F85EB51B8FE8040100B5B6C0B142941069B3AFB65AB5841EC51B81E85598140F09B9876B513294156E51BF65EAB5841CDCCCCCCCC688140D02CD6805F132941A62FFDF057AB5841AE47E17A1478814040F6CB294D13294130C6961A60AB5841A4703D0AD71B814040F6CB294D1329411C40ABA862AB5841A4703D0AD71B8140C081E0D720132941F04F59106FAB58411F85EB51B82B8140400DF585F4122941CE36AB787BAB58419A999999993B8140
9	reach	01030000A06A0800000100000009000000100B5B6C0B142941069B3AFB65AB5841EC51B81E85598140B03AE057DE1329410CDE1B6672AB58411F85EB51B8FE8040E01C5BECDB132941264382DC75AB58419A99999999F7804060B5CB29FA132941DA75B52F77AB584148E17A14AE068140E04D3C67181429418EA8E88278AB58410AD7A3703D068140901709B42014294124F2C96D73AB584185EB51B81E03814000D8FE5C261429410426FDA071AB584185EB51B81E038140B03E3CE740142941425C308469AB5841A4703D0AD7338140100B5B6C0B142941069B3AFB65AB5841EC51B81E85598140
11	reach	01030000A06A08000001000000090000002091FE5CE3142941D25D07A880AB584185EB51B81EFD8040E0AF8348E314294160F9A0717BAB58416666666666E88040603D463EE8142941242ED44477AB58410000000000DA8040B0992739B11429411A1E266D6BAB5841666666666624814070CC5A6CB2142941D24282BC76AB58418FC2F5285CF980409070CBA9B11429415EA8E80279AB58418FC2F5285CF98040104A6F1AAB14294186E3F2197FAB58410000000000000000C049463EC7142941AC20FDE07FAB584185EB51B81EFD70402091FE5CE3142941D25D07A880AB584185EB51B81EFD8040
12	reach	01030000A06A080000010000000C000000B03E3CE740142941425C308469AB5841A4703D0AD733814000D8FE5C261429410426FDA071AB584185EB51B81E038140901709B42014294124F2C96D73AB584185EB51B81E038140E04D3C67181429418EA8E88278AB58410AD7A3703D06814000BB50153D1429414C37AB287AAB58418FC2F5285C897940202865C3611429410AC66DCE7BAB58410AD7A3703D067140F0DC5A6C86142941C85430747DAB58410AD7A3703D066140104A6F1AAB14294186E3F2197FAB584100000000000000009070CBA9B11429415EA8E80279AB58418FC2F5285CF9804070CC5A6CB2142941D24282BC76AB58418FC2F5285CF98040B0992739B11429411A1E266D6BAB58416666666666248140B03E3CE740142941425C308469AB5841A4703D0AD7338140
13	reach	01030000A06A080000010000000C000000209A3390FE0F2941AE5859C057AB5841713D0AD7A3DA8140300A711A00102941D84582AC6EAB5841295C8FC2F58C8140F0181F620F102941966DDEEB71AB58413333333333878140F0181F620F10294192D91B567EAB5841EC51B81E85B781401049529517102941148963877AAB5841F6285C8FC2A98140E0C066C31F1029418C6107B876AB584100000000009C814000F199F6271029410E114FE972AB58410AD7A3703D8E8140708D0AB43B1029419A3AAB5871AB5841B81E85EB518A8140708D0AB43B102941DEC0964A6EAB58417B14AE47E1888140604529B93C102941E632D4946AAB58417B14AE47E1888140304529393D102941AE5859C057AB5841CDCCCCCCCCC28140209A3390FE0F2941AE5859C057AB5841713D0AD7A3DA8140
14	reach	01030000A06A080000010000000F000000307CCBA992142941B07607583EAB58419A999999994F814010D63110B41429412EE5441244AB584148E17A14AE3D814010D33110BC142941C65B59804FAB5841CDCCCCCCCC388140F01A138BBB14294172FBC99D5AAB5841A4703D0AD7258140505BF405CF1429417876DE3B5AAB5841CDCCCCCCCCFE804050E1B6FBE714294134BEBF165AAB5841CDCCCCCCCCFE8040705A50953E152941441078B559AB584133333333334F8140606150152C1529418A1278A553AB58410AD7A3703D3A8140F043CBA9281529410C7CDE5B4BAB58410AD7A3703D4C8140A091ACA418152941904DABC83EAB58410000000000428140D096D580EF1429414C988CA336AB58410AD7A3703D5481404079A2CDB51429418CCCBFD633AB5841A4703D0AD7518140207BA2CDB0142941AE50AB7836AB5841D7A3703D0A1D8140404D6F9AA2142941B86359503AAB5841D7A3703D0A1D8140307CCBA992142941B07607583EAB58419A999999994F8140
15	reach	01030000A06A080000010000000B000000F01A138BBB14294172FBC99D5AAB5841A4703D0AD7258140B0992739B11429411A1E266D6BAB58416666666666248140603D463EE8142941242ED44477AB58410000000000DA8040D0B15A6CF9142941648A630777AB5841EC51B81E85D18040D005EA2E0F1529414824FD4076AB5841EC51B81E85D18040D0D33BE75D152941B6828C3370AB58410000000000048140F037CBA948152941CC5C301468AB5841AE47E17A140E8140705A50953E152941441078B559AB584133333333334F814050E1B6FBE714294134BEBF165AAB5841CDCCCCCCCCFE8040505BF405CF1429417876DE3B5AAB5841CDCCCCCCCCFE8040F01A138BBB14294172FBC99D5AAB5841A4703D0AD7258140
16	reach	01030000A06A080000010000000A000000703EF4051C1529419074B59F7AAB5841AE47E17A14D880406098ACA406152941DE2BD4547DAB5841C3F5285C8FE28040D05DCBA9E3142941FE9EE80292AB5841A4703D0AD7F980406058CB29F214294148044FF994AB584152B81E85EB088140400BEAAE00152941889211EF97AB5841F6285C8FC2078140E0ECE9AE51152941DEC16DEE86AB58416666666666DE804030DB128B6515294130ECC94D83AB58418FC2F5285CD7804080775AEC94152941C8BB96DA7BAB5841CDCCCCCCCCEC8040D0D33BE75D152941B6828C3370AB58410000000000048140703EF4051C1529419074B59F7AAB5841AE47E17A14D88040
18	reach	01030000A06A080000010000000C00000080775AEC94152941C8BB96DA7BAB5841CDCCCCCCCCEC804030DB128B6515294130ECC94D83AB58418FC2F5285CD78040E0ECE9AE51152941DEC16DEE86AB58416666666666DE8040400BEAAE00152941889211EF97AB5841F6285C8FC2078140C03EA2CD511529412AB86DCEA0AB58416666666666F4804030C4E92EBE152941004E07D8AAAB58411F85EB51B8E0804080D2453E05162941DCA3BF56A0AB58411F85EB51B8D18040D0E0A14D4C162941AE22D4D495AB58411F85EB51B8C28040D012D5804F1629412C94118F93AB58411F85EB51B8C280406017ACA45E16294126DDF21990AB5841713D0AD7A3088140C0623190E715294184B7963A87AB58413D0AD7A370DF804080775AEC94152941C8BB96DA7BAB5841CDCCCCCCCCEC8040
10	reach_end	01030000A06A08000001000000080000002091FE5CE3142941D25D07A880AB584185EB51B81EFD8040D05DCBA9E3142941FE9EE80292AB5841A4703D0AD7F980406098ACA406152941DE2BD4547DAB5841C3F5285C8FE28040703EF4051C1529419074B59F7AAB5841AE47E17A14D88040D08DD580071529412AE5F2B97AAB584114AE47E17AC68040603D463EE8142941242ED44477AB58410000000000DA8040E0AF8348E314294160F9A0717BAB58416666666666E880402091FE5CE3142941D25D07A880AB584185EB51B81EFD8040
17	reach_end	01030000A06A0800000100000007000000703EF4051C1529419074B59F7AAB5841AE47E17A14D88040D0D33BE75D152941B6828C3370AB58410000000000048140D005EA2E0F1529414824FD4076AB5841EC51B81E85D18040D0B15A6CF9142941648A630777AB5841EC51B81E85D18040603D463EE8142941242ED44477AB58410000000000DA8040D08DD580071529412AE5F2B97AAB584114AE47E17AC68040703EF4051C1529419074B59F7AAB5841AE47E17A14D88040
\.


--
-- Name: coverage_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('coverage_id_seq', 18, true);


--
-- Data for Name: coverage_marker; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY coverage_marker (id, name, comment, geom) FROM stdin;
\.


--
-- Name: coverage_marker_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('coverage_marker_id_seq', 1, false);


--
-- Data for Name: domain_2d; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY domain_2d (id, name, geom, comment) FROM stdin;
\.


--
-- Name: domain_2d_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('domain_2d_id_seq', 1, false);


--
-- Data for Name: domain_2d_marker; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY domain_2d_marker (id, name, domain_2d, geom, comment) FROM stdin;
\.


--
-- Name: domain_2d_marker_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('domain_2d_marker_id_seq', 1, false);


--
-- Data for Name: generation_step; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY generation_step (id, name, description) FROM stdin;
1	GEN_STEP_25	\N
2	GEN_STEP_26	\N
3	GEN_STEP_27	\N
4	GEN_STEP_28	\N
5	GEN_STEP_29	\N
6	GEN_STEP_30	\N
7	GEN_STEP_31	\N
8	GEN_STEP_32	\N
9	GEN_STEP_33	\N
10	GEN_STEP_34	\N
11	GEN_STEP_35	\N
12	GEN_STEP_37	\N
13	GEN_STEP_38	\N
14	GEN_STEP_47	\N
15	GEN_STEP_48	\N
\.


--
-- Name: generation_step_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('generation_step_id_seq', 15, true);


--
-- Data for Name: metadata; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY metadata (id, version, creation_date, configuration) FROM stdin;
1	1.0.0	2017-09-25 00:00:00	1
\.


--
-- Data for Name: open_parametric_geometry; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY open_parametric_geometry (id, name, zbmin_array) FROM stdin;
\.


--
-- Name: open_parametric_geometry_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('open_parametric_geometry_id_seq', 1, false);


--
-- Data for Name: reach; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY reach (id, name, pk0_km, dx, geom) FROM stdin;
1	REACH_1	0	50	01020000A06A08000031000000EDD79DBE3F0E29414AB54F5B8BAB58415C8FC2F528CE8140EDD79DBE3F0E29414AB54F5B8BAB58415C8FC2F528CE81403C4CFB8E870E2941C993A4178AAB58419A99999999BF81400C3A213CC30E2941EB909BB388AB584185EB51B81EB78140DE7536ECFF0E2941B77A4E9087AB5841E17A14AE47B38140C5C551411D0F2941F7E978EE86AB5841713D0AD7A3BE8140BA14576D2C0F2941B9C2BBE282AB5841EC51B81E85B1814037FFAF8A360F2941F836FD0B7BAB5841295C8FC2F59C8140A16472C2490F29413563D1E277AB584148E17A14AEA1814017D68D17670F2941E4DA50FD75AB5841295C8FC2F5928140FF25A96C840F2941F73B14D773AB58417B14AE47E18E81405D8C81CDA50F29415BEB8B5874AB58410AD7A3703D94814051DB86F9B40F2941234A7B5B75AB58411F85EB51B8868140BF2A173ACB0F2941E197FA7573AB584133333333338B814032005489E60F29417DE882F472AB5841AE47E17A148281401766A1D500102941827346CE70AB5841D7A3703D0A87814006D8473F25102941A9FB00446DAB584114AE47E17A768140ECD9739141102941EDF5EE7B6AAB584185EB51B81E898140E0DA89BA4F102941200C3C9F6BAB584100000000007A8140D3DB9FE35D1029417F30F0466EAB58417B14AE47E16C8140BADDCB357A1029416501130C70AB5841D7A3703D0A758140A94F729F9E1029416501130C70AB58417B14AE47E16A814009522906C21029419CA223096FAB584133333333336B8140D8F15FB0FC102941FE0E45036DAB5841AE47E17A147681402766BD80441129413D7E6F616CAB5841E17A14AE476781408B5242F06A1129412A1DAC876EAB584100000000005A8140EE3EC75F91112941C1E2702C70AB5841713D0AD7A35281404E417EC6B411294140C1C5E86EAB58418FC2F5285C4B81402D7C7D4DE3112941CBF8F7DF6BAB58410AD7A3703D40814017B666AB0312294184BBB3206CAB5841C3F5285C8F3A8140E7559D553E122941F98381296FAB5841EC51B81E85338140D28F86B35E1229419FE5799071AB5841B81E85EB513681401F68057EA4122941827346CE70AB5841713D0AD7A32A8140080610D6C2122941698CD6E56DAB5841295C8FC2F5228140666CE836E41229418603217367AB584114AE47E17A1C8140C9586DA60A132941E42CECE562AB5841D7A3703D0A298140AA7D3A363C13294146990DE060AB5841C3F5285C8F12814092CD558B5913294194A46B0061AB5841AE47E17A140C81408A0629C06B132941C22FF54964AB58415C8FC2F5280E8140EA08E0268F132941200C3C9F6BAB584166666666660A8140D00A0C79AB132941827346CE70AB584114AE47E17A10814028469614E11329419B5AB6B673AB58417B14AE47E1F280408F1CE98C0A14294160764F3272AB58410AD7A3703DF28040FF0758D32214294119390B7372AB58418FC2F5285CEB8040DCF4675750142941DD0C379C75AB5841AE47E17A14EA8040BB7D56E17F14294139EE94BC75AB5841F6285C8FC2E180400D33346EB0142941D88173C277AB584148E17A14AEDF80406AA67B15EA1429413F7441CB7AAB5841713D0AD7A3CE8040D6A71C53FF1429415CE6748D7BAB58415C8FC2F528CA8040
2	REACH_2	0	50	01020000A06A0800000B0000009148DB50A6142941EFACDD2C39AB5841EC51B81E8501814073BB97E3D8142941315F5E123BAB5841B81E85EB51FE80404DBED95E031529412849D75E3FAB5841E17A14AE47F780404C70EA5B02152941DA38627346AB5841E17A14AE47F980405DA7913EF8142941C0046E6D4FAB58410AD7A3703DEC8040F0A5F000E314294151F701E657AB5841EC51B81E85E38040EDBB22F8DF142941543A586D5AAB584148E17A14AEE380405721E52FF31429412C6519DA69AB5841B81E85EB51F08040D40B3E4DFD1429412592E8AD70AB5841CDCCCCCCCCDE80404F5AB86405152941DD0C379C75AB58413333333333C98040CAA8327C0D1529411366DA4679AB58416666666666C48040
3	REACH_3	0	50	01020000A06A08000007000000425BCE8D1315294115A930CE7BAB58410000000000C280402F3196F1351529417042219C80AB58411F85EB51B8C08040FD82DD986F1529414CFDBCAD86AB5841E17A14AE47BD804058A8353DA81529412F8672208DAB5841C3F5285C8FBA8040300F99B2D01529414F3BFC6990AB5841C3F5285C8FB6804005357C6B1016294168226C5293AB5841F6285C8FC2B3804050711C30541629418D62B97594AB5841F6285C8FC2B18040
\.


--
-- Name: reach_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('reach_id_seq', 3, true);


--
-- Data for Name: station; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY station (id, name, geom) FROM stdin;
\.


--
-- Name: station_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('station_id_seq', 1, false);


--
-- Data for Name: street; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY street (id, name, width, elem_length, rk, geom) FROM stdin;
\.


--
-- Name: street_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('street_id_seq', 1, false);


--
-- Data for Name: urban_flood_risk_area; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY urban_flood_risk_area (id, name, geom) FROM stdin;
\.


--
-- Name: urban_flood_risk_area_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('urban_flood_risk_area_id_seq', 1, false);


--
-- Data for Name: valley_cross_section_geometry; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY valley_cross_section_geometry (id, name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb, transect, t_ignore_pt, t_discret, t_distance_pt) FROM stdin;
13	VA_17	{{537.210022,0.100000001},{537.599976,6.65999985},{538.789978,12.6199999},{539.02002,15.0699997},{539.25,16.1399994},{539.48999,16.8700008}}	{{539.75,0},{541.26001,5.63999987},{541.719971,47.5299988},{543.22998,90.3000031}}	{{539.48999,0.00999999978},{539.48999,0.479999989},{539.48999,0.980000019},{539.48999,0.980000019}}	1	0	0	0	0	10	f	150	10
15	VA_19	{{536.109985,0},{536.299988,4.1500001},{538.219971,12.6199999},{539.210022,16.1399994},{539.460022,16.5699997},{539.599976,18.5599995}}	{{539.98999,0},{541.130005,4.92000008},{541.640015,38.5800018},{543.22998,82.7900009}}	{{539.599976,0.00999999978},{540.349976,11.6000004},{540.609985,28.6299992},{544.5,51.6699982}}	1	0	0	0	0	12	f	150	10
16	VA_20	{{534.25,0.850000024},{534.359985,2.23000002},{534.75,4.05000019},{535.780029,6.42999983},{535.960022,7.82999992},{536.340027,9.21000004}}	{{536.340027,0},{537.340027,73.4000015},{538.679993,105.779999},{540.090027,110.080002}}	{{537.150024,0.0199999996},{540.109985,9.63000011},{542.570007,11.3900003},{545.080017,15.7700005}}	1	0	0	0	0	13	f	150	10
3	VA_4	{{569.659973,3.67000008},{569.710022,7.26999998},{569.809998,7.9000001},{570.090027,11.3000002},{570.109985,11.9399996},{570.179993,12.7700005}}	{{570.179993,0},{570.380005,14.7399998},{571.859985,21.4799995},{572.580017,38.0400009}}	{{570.340027,0.0199999996},{574.119995,8},{575.210022,16.6200008},{579.429993,41.8600006}}	1	0	0	0	0	1	f	150	10
4	VA_7	{{542.52002,1.91999996},{542.570007,2.86999989},{542.619995,4.05000019},{542.950012,6.73999977},{543.429993,9.76000023},{544.059998,13.8900003}}	{{544.890015,0},{546.23999,4.01999998},{547.460022,41.7000008},{549.679993,68.8700027}}	{{544.059998,0.00999999978},{549.590027,39.0699997},{555.049988,49.9799995},{555.190002,54.5299988}}	1	0	0	0	0	2	f	150	10
6	VA_10	{{541.549988,0.0500000007},{542.030029,2.25},{543.119995,4.69999981},{543.140015,5.13000011},{544.059998,7.5999999},{544.390015,7.71999979}}	{{544.390015,0},{544.390015,6.36999989},{544.549988,19.9200001},{544.780029,20.7700005}}	{{545.400024,0.0199999996},{548.27002,5.90999985},{549.469971,27.8700008},{550.47998,35.0499992}}	1	0	0	0	0	3	f	150	10
7	VA_11	{{559.859985,1.00999999},{559.909973,2.21000004},{560.23999,4.3499999},{560.609985,7.5999999},{560.640015,8.75},{561.210022,12.3000002}}	{{561.210022,0},{562.179993,2.08999991},{563.690002,41.4000015},{566.940002,49.6800003}}	{{561.710022,2.76999998},{566.080017,46.5099983},{566.630005,56.9300003},{571.330017,94.4499969}}	1	0	0	0	0	4	f	150	10
8	VA_12	{{558.619995,0.129999995},{558.73999,4.55000019},{559.070007,7.30000019},{560.890015,12.9300003},{560.900024,13.6400003},{561.109985,14.2399998}}	{{561.109985,0},{561.299988,9.76000023},{561.349976,10.6400003},{561.390015,12.2399998}}	{{562.909973,0.610000014},{565.109985,16.8799992},{568.25,63.0200005},{568.450012,75.9100037}}	1	0	0	0	0	5	f	150	10
9	VA_13	{{550.909973,1.03999996},{551.359985,4.40999985},{551.890015,13.04},{552.340027,16.2199993},{553.030029,25.5599995},{553.109985,25.8099995}}	{{553.109985,0},{553.340027,0.870000005},{553.340027,1.73000002},{553.799988,3.43000007}}	{{553.440002,0.0199999996},{555.72998,17.1100006},{557.280029,59.7099991},{560.359985,106.300003}}	1	0	0	0	0	6	f	150	10
10	VA_14	{{550.119995,0},{550.409973,1.78999996},{550.97998,4.0999999},{551.27002,6.17000008},{552.549988,12.4899998},{552.940002,14.3900003}}	{{553.23999,0},{553.23999,0.0299999993},{553.409973,0.529999971},{553.409973,1.02999997}}	{{552.940002,0.00999999978},{554.409973,36.6800003},{557.780029,72.3099976},{560.059998,120.239998}}	1	0	0	0	0	7	f	150	10
11	VA_15	{{545.859985,0},{545.950012,1.02999997},{545.98999,3.9000001},{546.090027,5.03000021},{547.219971,8.32999992},{547.349976,9.47000027}}	{{547.349976,0},{549.919983,42.6899986},{550.450012,100.669998},{551.450012,108.720001}}	{{549.25,0.75},{549.25,3.72000003},{557.289978,19.6800003},{559.01001,34.6399994}}	1	0	0	0	0	8	f	150	10
12	VA_16	{{539.950012,0.50999999},{540.179993,4.0999999},{540.51001,5.23999977},{541.070007,6.53999996},{541.109985,7.09000015},{541.869995,8.36999989}}	{{541.869995,0},{541.869995,2.1099999},{543.619995,5.23999977},{544.210022,24.6000004}}	{{543.609985,0.74000001},{546.070007,28.1399994},{547.039978,29.6599998},{548.440002,45.9599991}}	1	0	0	0	0	9	f	150	10
18	VA_22	{{544.169983,1.94000006},{544.369995,6.67999983},{545.609985,11.8100004},{545.940002,12.54},{546.190002,14.25},{547.25,16.9200001}}	{{547.25,0},{547.609985,5.92000008},{549.900024,12.5200005},{553.950012,18}}	{{547.73999,0.00999999978},{548.159973,2.4000001},{552.380005,8.40999985},{554.22998,10.8199997}}	1	0	0	0	0	14	f	150	10
19	VA_23	{{540.409973,1.82000005},{540.460022,3.57999992},{541.599976,7.34000015},{541.659973,8.68999958},{543.789978,10.8100004},{543.849976,11.6300001}}	{{543.849976,0},{547.940002,4.73999977},{548.659973,6.88000011},{548.72998,9.88000011}}	{{545.22998,0.860000014},{545.530029,15.29},{553.72998,41.1100006},{553.900024,44.1699982}}	1	0	0	0	0	16	f	150	10
20	VA_24	{{537.070007,1.75999999},{537.150024,2.56999993},{537.340027,3.6500001},{537.460022,6.05999994},{537.820007,8.38000011},{538.190002,10.0699997}}	{{538.190002,0},{538.919983,2.43000007},{539.179993,8.22999954},{539.25,8.65999985}}	{{538.900024,1.24000001},{540.369995,8.26000023},{541.590027,35.1100006},{544.5,47.4300003}}	1	0	0	0	0	17	f	150	10
21	VA_36	{{536.179993,1.02999997},{536.309998,4.40999985},{537.030029,6.57000017},{537.190002,8.31999969},{538.619995,13.5299997},{539.109985,15.3199997}}	{{539.460022,0},{541.130005,5.96000004},{541.640015,40.8199997},{543.22998,84.25}}	{{539.109985,0.00999999978},{540.380005,14.7200003},{540.599976,29.8700008},{544.5,53.1500015}}	1	0	0	0	0	12	f	150	10
\.


--
-- Name: valley_cross_section_geometry_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('valley_cross_section_geometry_id_seq', 21, true);


--
-- Data for Name: valley_cross_section_topo_geometry; Type: TABLE DATA; Schema: model1; Owner: thierry.lepelletier
--

COPY valley_cross_section_topo_geometry (id, name, xz_array) FROM stdin;
\.


--
-- Name: valley_cross_section_topo_geometry_id_seq; Type: SEQUENCE SET; Schema: model1; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('valley_cross_section_topo_geometry_id_seq', 1, false);


SET search_path = project, pg_catalog;

--
-- Data for Name: _caquot_rainfall; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY _caquot_rainfall (id, rainfall_type, montana) FROM stdin;
\.


--
-- Data for Name: _double_triangular_rainfall; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY _double_triangular_rainfall (id, rainfall_type, montana_total, montana_peak, total_duration_mn, peak_duration_mn, peak_time_mn) FROM stdin;
\.


--
-- Data for Name: _gage_rainfall; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY _gage_rainfall (id, rainfall_type, cbv_grid_connect_file, interpolation) FROM stdin;
2	gage	\N	\N
\.


--
-- Data for Name: _intensity_curve_rainfall; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY _intensity_curve_rainfall (id, rainfall_type, t_mn_intensity_mmh) FROM stdin;
\.


--
-- Data for Name: _radar_rainfall; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY _radar_rainfall (id, rainfall_type, radar_file, radar_grid) FROM stdin;
\.


--
-- Data for Name: _rainfall; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY _rainfall (id, rainfall_type, name, comment, validity) FROM stdin;
2	gage	Pluie1	\N	f
\.


--
-- Name: _rainfall_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('_rainfall_id_seq', 2, true);


--
-- Data for Name: _single_triangular_rainfall; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY _single_triangular_rainfall (id, rainfall_type, montana, total_duration_mn, peak_time_mn) FROM stdin;
\.


--
-- Data for Name: bc_tz_data; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY bc_tz_data (id, filename) FROM stdin;
\.


--
-- Name: bc_tz_data_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('bc_tz_data_id_seq', 1, false);


--
-- Data for Name: c_affin_param; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY c_affin_param (id, scenario, model, type_domain, element) FROM stdin;
\.


--
-- Name: c_affin_param_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('c_affin_param_id_seq', 1, false);


--
-- Data for Name: coef_montana; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY coef_montana (id, name, comment, return_period_yr, coef_a, coef_b) FROM stdin;
\.


--
-- Name: coef_montana_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('coef_montana_id_seq', 1, false);


--
-- Data for Name: dem; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY dem (id, priority, source) FROM stdin;
\.


--
-- Name: dem_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('dem_id_seq', 1, true);


--
-- Data for Name: dry_inflow_hourly_modulation; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY dry_inflow_hourly_modulation (id, name, comment, hv_array) FROM stdin;
\.


--
-- Name: dry_inflow_hourly_modulation_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('dry_inflow_hourly_modulation_id_seq', 1, false);


--
-- Data for Name: dry_inflow_scenario; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY dry_inflow_scenario (id, name, comment) FROM stdin;
\.


--
-- Name: dry_inflow_scenario_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('dry_inflow_scenario_id_seq', 1, false);


--
-- Data for Name: dry_inflow_scenario_sector_setting; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY dry_inflow_scenario_sector_setting (dry_inflow_scenario, sector, vol_curve, volume_sewage_m3day, coef_volume_sewage_m3day, volume_clear_water_m3day, coef_volume_clear_water_m3day) FROM stdin;
\.


--
-- Data for Name: dry_inflow_sector; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY dry_inflow_sector (id, name, comment, geom) FROM stdin;
\.


--
-- Name: dry_inflow_sector_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('dry_inflow_sector_id_seq', 1, false);


--
-- Data for Name: gage_rainfall_data; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY gage_rainfall_data (rainfall, rain_gage, is_active, t_mn_hcum_mm) FROM stdin;
2	1	f	{{0,0},{30,14},{60,35},{80,42},{110,48},{140,50},{180,50}}
\.


--
-- Data for Name: grp; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY grp (id, name) FROM stdin;
\.


--
-- Name: grp_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('grp_id_seq', 1, false);


--
-- Data for Name: grp_model; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY grp_model (grp, model) FROM stdin;
\.


--
-- Data for Name: hydrograph_pollution; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY hydrograph_pollution (id, dry_flow_mes_mgl, dry_flow_dbo5_mgl, dry_flow_dco_mgl, dry_flow_ntk_mgl, runoff_mes_mgl, runoff_flow_dbo5_mgl, runoff_flow_dco_mgl, runoff_flow_ntk_mgl) FROM stdin;
\.


--
-- Name: hydrograph_pollution_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('hydrograph_pollution_id_seq', 1, false);


--
-- Data for Name: hydrograph_quality; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY hydrograph_quality (id, dry_flow_dbo5_mgl, dry_flow_nh4_mgl, dry_flow_no3_mgl, dry_flow_o2_mgl, dry_flow_ad_mgl, dry_flow_mes_mgl, runoff_flow_dbo5_mgl, runoff_flow_nh4_mgl, runoff_flow_no3_mgl, runoff_flow_ad_mgl, runoff_flow_mes_mgl, dbou_dbo5_ratio, dbo5_dcant_fraction, mes_decant_fraction) FROM stdin;
\.


--
-- Name: hydrograph_quality_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('hydrograph_quality_id_seq', 1, false);


--
-- Data for Name: hydrograph_variable_dry_flow; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY hydrograph_variable_dry_flow (id, sector, distrib_coef, lag_time_hr, dry_inflow_hourly_modulation) FROM stdin;
\.


--
-- Name: hydrograph_variable_dry_flow_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('hydrograph_variable_dry_flow_id_seq', 1, false);


--
-- Data for Name: inflow_rerouting; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY inflow_rerouting (id, param_scenario, model, hydrograph_from, hydrograph_to, option, value) FROM stdin;
\.


--
-- Name: inflow_rerouting_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('inflow_rerouting_id_seq', 1, false);


--
-- Data for Name: land_use; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY land_use (id, geom, land_use) FROM stdin;
\.


--
-- Name: land_use_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('land_use_id_seq', 1, false);


--
-- Data for Name: mixed; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY mixed (ord, scenario, grp) FROM stdin;
\.


--
-- Data for Name: model_config; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY model_config (id, name, config, cascade_order, comment) FROM stdin;
1	model1	1	0	\N
\.


--
-- Name: model_config_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('model_config_id_seq', 1, true);


--
-- Data for Name: output_option; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY output_option (id, scenario, model, type_element, element) FROM stdin;
\.


--
-- Name: output_option_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('output_option_id_seq', 1, false);


--
-- Data for Name: param_external_hydrograph; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY param_external_hydrograph (id, param_scenario, external_file) FROM stdin;
1	3	C:/Travail/_Projet_Hydra/_demo/Hydrol/App.txt
2	2	C:/Users/Thierry.Lepelletier/.hydra/pdemo/Data/App.txt
\.


--
-- Name: param_external_hydrograph_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('param_external_hydrograph_id_seq', 2, true);


--
-- Data for Name: param_hydrograph; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY param_hydrograph (id, param_scenario, model, hydrograph_from, hydrograph_to) FROM stdin;
\.


--
-- Name: param_hydrograph_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('param_hydrograph_id_seq', 1, false);


--
-- Data for Name: param_measure; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY param_measure (id, param_scenario, measure_file) FROM stdin;
\.


--
-- Name: param_measure_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('param_measure_id_seq', 1, false);


--
-- Data for Name: param_regulation; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY param_regulation (id, param_scenario, control_file) FROM stdin;
\.


--
-- Name: param_regulation_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('param_regulation_id_seq', 1, false);


--
-- Data for Name: param_scenario_group; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY param_scenario_group (scenario, grp) FROM stdin;
\.


--
-- Data for Name: points_xyz; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY points_xyz (id, name, source, comment, geom, z_ground) FROM stdin;
\.


--
-- Name: points_xyz_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('points_xyz_id_seq', 1, false);


--
-- Data for Name: pollution_land_accumulation; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY pollution_land_accumulation (id, sds_stockk_gha, cds_stock_kgha) FROM stdin;
\.


--
-- Name: pollution_land_accumulation_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('pollution_land_accumulation_id_seq', 1, false);


--
-- Data for Name: radar_grid; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY radar_grid (id, name, comment, x0, y0, dx, dy, nx, ny) FROM stdin;
\.


--
-- Name: radar_grid_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('radar_grid_id_seq', 1, false);


--
-- Data for Name: rain_gage; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY rain_gage (id, name, comment, geom) FROM stdin;
1	Pluvio_1		01010000A06A080000DE1FEFBDD611294149A297CBC4AB5841000000008087C340
\.


--
-- Name: rain_gage_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('rain_gage_id_seq', 1, true);


--
-- Data for Name: scenario; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY scenario (id, name, comment, comput_mode, dry_inflow, rainfall, dt_hydrol_mn, soil_moisture_coef, runoff_adjust_coef, option_dim_hydrol_network, date0, tfin_hr, tini_hydrau_hr, dtmin_hydrau_hr, dtmax_hydrau_hr, dzmin_hydrau, dzmax_hydrau, flag_save, tsave_hr, flag_rstart, flag_gfx_control, scenario_rstart, flag_hydrology_rstart, scenario_hydrology_rstart, trstart_hr, graphic_control, model_connect_settings, tbegin_output_hr, tend_output_hr, dt_output_hr, c_affin_param, t_affin_start_hr, t_affin_min_surf_ddomains, strickler_param, option_file, option_file_path, output_option, transport_type, iflag_mes, iflag_dbo5, iflag_dco, iflag_ntk, quality_type, diffusion, dispersion_coef, longit_diffusion_coef, lateral_diff_coef, water_temperature_c, min_o2_mgl, kdbo5_j_minus_1, koxygen_j_minus_1, knr_j_minus_1, kn_j_minus_1, bdf_mgl, o2sat_mgl, iflag_ad1, iflag_ad2, iflag_ad3, iflag_ad4, ad1_decay_rate_jminus_one, ad2_decay_rate_jminus_one, ad3_decay_rate_jminus_one, ad4_decay_rate_jminus_one, sst_mgl, sediment_file) FROM stdin;
3	scn2		hydrology_and_hydraulics	\N	\N	5	0	1	f	2000-01-01 12:00:00	12	12	0.00499999989	0.100000001	0.0500000007	0.100000001	f	0	f	f	\N	f	\N	0	f	cascade	0	12	0.100000001	f	0	0	f	f		f	no_transport	f	f	f	f	physico_chemical	f	20	41	0.449999988	20	1.5	0.200000003	0.449999988	0.0900000036	0.0350000001	1.5	9.06999969	f	f	f	f	0	0	0	0	0
2	scn1		hydrology_and_hydraulics	\N	2	5	0	1	f	2000-01-01 12:00:00	12	12	0.00499999989	0.100000001	0.0500000007	0.100000001	f	0	f	f	\N	f	\N	0	f	cascade	0	12	0.100000001	f	0	0	f	f		f	no_transport	f	f	f	f	physico_chemical	f	20	41	0.449999988	20	1.5	0.200000003	0.449999988	0.0900000036	0.0350000001	1.5	9.06999969	f	f	f	f	0	0	0	0	0
\.


--
-- Data for Name: scenario_group; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY scenario_group (id, name) FROM stdin;
\.


--
-- Name: scenario_group_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('scenario_group_id_seq', 1, false);


--
-- Name: scenario_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('scenario_id_seq', 3, true);


--
-- Data for Name: strickler_param; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY strickler_param (id, scenario, model, type_domain, element, rkmin, rkmaj, pk1, pk2) FROM stdin;
\.


--
-- Name: strickler_param_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('strickler_param_id_seq', 1, false);


--
-- Name: unique_name_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('unique_name_seq', 48, true);


--
-- Data for Name: wind_gage; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY wind_gage (id, name, comment, geom) FROM stdin;
\.


--
-- Name: wind_gage_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('wind_gage_id_seq', 1, false);


--
-- Data for Name: wind_scenario; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY wind_scenario (id, name, grid_connect_file, interpolation) FROM stdin;
\.


--
-- Data for Name: wind_scenario_anemometer_data; Type: TABLE DATA; Schema: project; Owner: thierry.lepelletier
--

COPY wind_scenario_anemometer_data (wind_gage, wind_scenario, hwd_array) FROM stdin;
\.


--
-- Name: wind_scenario_id_seq; Type: SEQUENCE SET; Schema: project; Owner: thierry.lepelletier
--

SELECT pg_catalog.setval('wind_scenario_id_seq', 1, false);


SET search_path = public, pg_catalog;

--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: thierry.lepelletier
--

COPY spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


SET search_path = model1, pg_catalog;

--
-- Name: _borda_headloss_link _borda_headloss_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _borda_headloss_link
    ADD CONSTRAINT _borda_headloss_link_id_key UNIQUE (id);


--
-- Name: _borda_headloss_singularity _borda_headloss_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _borda_headloss_singularity
    ADD CONSTRAINT _borda_headloss_singularity_id_key UNIQUE (id);


--
-- Name: _bradley_headloss_singularity _bradley_headloss_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _bradley_headloss_singularity
    ADD CONSTRAINT _bradley_headloss_singularity_id_key UNIQUE (id);


--
-- Name: _bridge_headloss_singularity _bridge_headloss_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _bridge_headloss_singularity
    ADD CONSTRAINT _bridge_headloss_singularity_id_key UNIQUE (id);


--
-- Name: _catchment_node _catchment_node_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _catchment_node
    ADD CONSTRAINT _catchment_node_id_key UNIQUE (id);


--
-- Name: _connector_hydrology_link _connector_hydrology_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _connector_hydrology_link
    ADD CONSTRAINT _connector_hydrology_link_id_key UNIQUE (id);


--
-- Name: _connector_link _connector_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _connector_link
    ADD CONSTRAINT _connector_link_id_key UNIQUE (id);


--
-- Name: _constant_inflow_bc_singularity _constant_inflow_bc_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _constant_inflow_bc_singularity
    ADD CONSTRAINT _constant_inflow_bc_singularity_id_key UNIQUE (id);


--
-- Name: _constrain _constrain_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _constrain
    ADD CONSTRAINT _constrain_name_key UNIQUE (name);


--
-- Name: _constrain _constrain_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _constrain
    ADD CONSTRAINT _constrain_pkey PRIMARY KEY (id);


--
-- Name: _crossroad_node _crossroad_node_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _crossroad_node
    ADD CONSTRAINT _crossroad_node_id_key UNIQUE (id);


--
-- Name: _deriv_pump_link _deriv_pump_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _deriv_pump_link
    ADD CONSTRAINT _deriv_pump_link_id_key UNIQUE (id);


--
-- Name: _elem_2d_node _elem_2d_node_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _elem_2d_node
    ADD CONSTRAINT _elem_2d_node_id_key UNIQUE (id);


--
-- Name: _froude_bc_singularity _froude_bc_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _froude_bc_singularity
    ADD CONSTRAINT _froude_bc_singularity_id_key UNIQUE (id);


--
-- Name: _fuse_spillway_link _fuse_spillway_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _fuse_spillway_link
    ADD CONSTRAINT _fuse_spillway_link_id_key UNIQUE (id);


--
-- Name: _gate_link _gate_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _gate_link
    ADD CONSTRAINT _gate_link_id_key UNIQUE (id);


--
-- Name: _gate_singularity _gate_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _gate_singularity
    ADD CONSTRAINT _gate_singularity_id_key UNIQUE (id);


--
-- Name: _hydraulic_cut_singularity _hydraulic_cut_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _hydraulic_cut_singularity
    ADD CONSTRAINT _hydraulic_cut_singularity_id_key UNIQUE (id);


--
-- Name: _hydrograph_bc_singularity _hydrograph_bc_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _hydrograph_bc_singularity
    ADD CONSTRAINT _hydrograph_bc_singularity_id_key UNIQUE (id);


--
-- Name: _hydrology_bc_singularity _hydrology_bc_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _hydrology_bc_singularity
    ADD CONSTRAINT _hydrology_bc_singularity_id_key UNIQUE (id);


--
-- Name: _link _link_id_link_type_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _link
    ADD CONSTRAINT _link_id_link_type_key UNIQUE (id, link_type);


--
-- Name: _link _link_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _link
    ADD CONSTRAINT _link_name_key UNIQUE (name);


--
-- Name: _link _link_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _link
    ADD CONSTRAINT _link_pkey PRIMARY KEY (id);


--
-- Name: _manhole_hydrology_node _manhole_hydrology_node_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _manhole_hydrology_node
    ADD CONSTRAINT _manhole_hydrology_node_id_key UNIQUE (id);


--
-- Name: _manhole_node _manhole_node_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _manhole_node
    ADD CONSTRAINT _manhole_node_id_key UNIQUE (id);


--
-- Name: _marker_singularity _marker_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _marker_singularity
    ADD CONSTRAINT _marker_singularity_id_key UNIQUE (id);


--
-- Name: _mesh_2d_link _mesh_2d_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _mesh_2d_link
    ADD CONSTRAINT _mesh_2d_link_id_key UNIQUE (id);


--
-- Name: _model_connect_bc_singularity _model_connect_bc_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _model_connect_bc_singularity
    ADD CONSTRAINT _model_connect_bc_singularity_id_key UNIQUE (id);


--
-- Name: _network_overflow_link _network_overflow_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _network_overflow_link
    ADD CONSTRAINT _network_overflow_link_id_key UNIQUE (id);


--
-- Name: _node _node_geom_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _node
    ADD CONSTRAINT _node_geom_key UNIQUE (geom);


--
-- Name: _node _node_id_node_type_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _node
    ADD CONSTRAINT _node_id_node_type_key UNIQUE (id, node_type);


--
-- Name: _node _node_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _node
    ADD CONSTRAINT _node_name_key UNIQUE (name);


--
-- Name: _node _node_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _node
    ADD CONSTRAINT _node_pkey PRIMARY KEY (id);


--
-- Name: _overflow_link _overflow_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _overflow_link
    ADD CONSTRAINT _overflow_link_id_key UNIQUE (id);


--
-- Name: _param_headloss_singularity _param_headloss_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _param_headloss_singularity
    ADD CONSTRAINT _param_headloss_singularity_id_key UNIQUE (id);


--
-- Name: _pipe_branch_marker_singularity _pipe_branch_marker_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _pipe_branch_marker_singularity
    ADD CONSTRAINT _pipe_branch_marker_singularity_id_key UNIQUE (id);


--
-- Name: _pipe_link _pipe_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _pipe_link
    ADD CONSTRAINT _pipe_link_id_key UNIQUE (id);


--
-- Name: _porous_link _porous_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _porous_link
    ADD CONSTRAINT _porous_link_id_key UNIQUE (id);


--
-- Name: _pump_link _pump_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _pump_link
    ADD CONSTRAINT _pump_link_id_key UNIQUE (id);


--
-- Name: _qq_split_hydrology_singularity _qq_split_hydrology_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _qq_split_hydrology_singularity
    ADD CONSTRAINT _qq_split_hydrology_singularity_id_key UNIQUE (id);


--
-- Name: _regul_gate_link _regul_gate_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _regul_gate_link
    ADD CONSTRAINT _regul_gate_link_id_key UNIQUE (id);


--
-- Name: _regul_sluice_gate_singularity _regul_sluice_gate_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _regul_sluice_gate_singularity
    ADD CONSTRAINT _regul_sluice_gate_singularity_id_key UNIQUE (id);


--
-- Name: _reservoir_rs_hydrology_singularity _reservoir_rs_hydrology_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _reservoir_rs_hydrology_singularity
    ADD CONSTRAINT _reservoir_rs_hydrology_singularity_id_key UNIQUE (id);


--
-- Name: _reservoir_rsp_hydrology_singularity _reservoir_rsp_hydrology_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _reservoir_rsp_hydrology_singularity
    ADD CONSTRAINT _reservoir_rsp_hydrology_singularity_id_key UNIQUE (id);


--
-- Name: _river_cross_section_pl1d _river_cross_section_pl1d_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_pl1d
    ADD CONSTRAINT _river_cross_section_pl1d_id_key UNIQUE (id);


--
-- Name: _river_cross_section_pl1d _river_cross_section_pl1d_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_pl1d
    ADD CONSTRAINT _river_cross_section_pl1d_name_key UNIQUE (name);


--
-- Name: _river_cross_section_profile _river_cross_section_profile_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_profile
    ADD CONSTRAINT _river_cross_section_profile_id_key UNIQUE (id);


--
-- Name: _river_cross_section_profile _river_cross_section_profile_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_profile
    ADD CONSTRAINT _river_cross_section_profile_name_key UNIQUE (name);


--
-- Name: _river_node _river_node_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_node
    ADD CONSTRAINT _river_node_id_key UNIQUE (id);


--
-- Name: _routing_hydrology_link _routing_hydrology_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _routing_hydrology_link
    ADD CONSTRAINT _routing_hydrology_link_id_key UNIQUE (id);


--
-- Name: _singularity _singularity_id_singularity_type_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _singularity
    ADD CONSTRAINT _singularity_id_singularity_type_key UNIQUE (id, singularity_type);


--
-- Name: _singularity _singularity_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _singularity
    ADD CONSTRAINT _singularity_name_key UNIQUE (name);


--
-- Name: _singularity _singularity_node_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _singularity
    ADD CONSTRAINT _singularity_node_key UNIQUE (node);


--
-- Name: _singularity _singularity_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _singularity
    ADD CONSTRAINT _singularity_pkey PRIMARY KEY (id);


--
-- Name: _station_node _station_node_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _station_node
    ADD CONSTRAINT _station_node_id_key UNIQUE (id);


--
-- Name: _storage_node _storage_node_contour_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _storage_node
    ADD CONSTRAINT _storage_node_contour_key UNIQUE (contour);


--
-- Name: _storage_node _storage_node_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _storage_node
    ADD CONSTRAINT _storage_node_id_key UNIQUE (id);


--
-- Name: _street_link _street_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _street_link
    ADD CONSTRAINT _street_link_id_key UNIQUE (id);


--
-- Name: _strickler_bc_singularity _strickler_bc_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _strickler_bc_singularity
    ADD CONSTRAINT _strickler_bc_singularity_id_key UNIQUE (id);


--
-- Name: _strickler_link _strickler_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _strickler_link
    ADD CONSTRAINT _strickler_link_id_key UNIQUE (id);


--
-- Name: _tank_bc_singularity _tank_bc_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _tank_bc_singularity
    ADD CONSTRAINT _tank_bc_singularity_id_key UNIQUE (id);


--
-- Name: _tz_bc_singularity _tz_bc_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _tz_bc_singularity
    ADD CONSTRAINT _tz_bc_singularity_id_key UNIQUE (id);


--
-- Name: _weir_bc_singularity _weir_bc_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _weir_bc_singularity
    ADD CONSTRAINT _weir_bc_singularity_id_key UNIQUE (id);


--
-- Name: _weir_link _weir_link_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _weir_link
    ADD CONSTRAINT _weir_link_id_key UNIQUE (id);


--
-- Name: _zq_bc_singularity _zq_bc_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _zq_bc_singularity
    ADD CONSTRAINT _zq_bc_singularity_id_key UNIQUE (id);


--
-- Name: _zq_split_hydrology_singularity _zq_split_hydrology_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _zq_split_hydrology_singularity
    ADD CONSTRAINT _zq_split_hydrology_singularity_id_key UNIQUE (id);


--
-- Name: _zregul_weir_singularity _zregul_weir_singularity_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _zregul_weir_singularity
    ADD CONSTRAINT _zregul_weir_singularity_id_key UNIQUE (id);


--
-- Name: bank bank_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY bank
    ADD CONSTRAINT bank_name_key UNIQUE (name);


--
-- Name: bank bank_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY bank
    ADD CONSTRAINT bank_pkey PRIMARY KEY (id);


--
-- Name: branch branch_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY branch
    ADD CONSTRAINT branch_name_key UNIQUE (name);


--
-- Name: branch branch_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY branch
    ADD CONSTRAINT branch_pkey PRIMARY KEY (id);


--
-- Name: catchment catchment_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY catchment
    ADD CONSTRAINT catchment_name_key UNIQUE (name);


--
-- Name: catchment catchment_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY catchment
    ADD CONSTRAINT catchment_pkey PRIMARY KEY (id);


--
-- Name: closed_parametric_geometry closed_parametric_geometry_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY closed_parametric_geometry
    ADD CONSTRAINT closed_parametric_geometry_name_key UNIQUE (name);


--
-- Name: closed_parametric_geometry closed_parametric_geometry_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY closed_parametric_geometry
    ADD CONSTRAINT closed_parametric_geometry_pkey PRIMARY KEY (id);


--
-- Name: configuration configuration_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY configuration
    ADD CONSTRAINT configuration_name_key UNIQUE (name);


--
-- Name: configuration configuration_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY configuration
    ADD CONSTRAINT configuration_pkey PRIMARY KEY (id);


--
-- Name: coverage_marker coverage_marker_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY coverage_marker
    ADD CONSTRAINT coverage_marker_name_key UNIQUE (name);


--
-- Name: coverage_marker coverage_marker_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY coverage_marker
    ADD CONSTRAINT coverage_marker_pkey PRIMARY KEY (id);


--
-- Name: coverage coverage_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY coverage
    ADD CONSTRAINT coverage_pkey PRIMARY KEY (id);


--
-- Name: domain_2d_marker domain_2d_marker_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY domain_2d_marker
    ADD CONSTRAINT domain_2d_marker_name_key UNIQUE (name);


--
-- Name: domain_2d_marker domain_2d_marker_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY domain_2d_marker
    ADD CONSTRAINT domain_2d_marker_pkey PRIMARY KEY (id);


--
-- Name: domain_2d domain_2d_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY domain_2d
    ADD CONSTRAINT domain_2d_name_key UNIQUE (name);


--
-- Name: domain_2d domain_2d_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY domain_2d
    ADD CONSTRAINT domain_2d_pkey PRIMARY KEY (id);


--
-- Name: generation_step generation_step_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY generation_step
    ADD CONSTRAINT generation_step_name_key UNIQUE (name);


--
-- Name: generation_step generation_step_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY generation_step
    ADD CONSTRAINT generation_step_pkey PRIMARY KEY (id);


--
-- Name: metadata metadata_id_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY metadata
    ADD CONSTRAINT metadata_id_key UNIQUE (id);


--
-- Name: open_parametric_geometry open_parametric_geometry_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY open_parametric_geometry
    ADD CONSTRAINT open_parametric_geometry_name_key UNIQUE (name);


--
-- Name: open_parametric_geometry open_parametric_geometry_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY open_parametric_geometry
    ADD CONSTRAINT open_parametric_geometry_pkey PRIMARY KEY (id);


--
-- Name: reach reach_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY reach
    ADD CONSTRAINT reach_name_key UNIQUE (name);


--
-- Name: reach reach_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY reach
    ADD CONSTRAINT reach_pkey PRIMARY KEY (id);


--
-- Name: station station_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY station
    ADD CONSTRAINT station_name_key UNIQUE (name);


--
-- Name: station station_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY station
    ADD CONSTRAINT station_pkey PRIMARY KEY (id);


--
-- Name: street street_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY street
    ADD CONSTRAINT street_name_key UNIQUE (name);


--
-- Name: street street_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY street
    ADD CONSTRAINT street_pkey PRIMARY KEY (id);


--
-- Name: urban_flood_risk_area urban_flood_risk_area_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY urban_flood_risk_area
    ADD CONSTRAINT urban_flood_risk_area_name_key UNIQUE (name);


--
-- Name: urban_flood_risk_area urban_flood_risk_area_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY urban_flood_risk_area
    ADD CONSTRAINT urban_flood_risk_area_pkey PRIMARY KEY (id);


--
-- Name: valley_cross_section_geometry valley_cross_section_geometry_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY valley_cross_section_geometry
    ADD CONSTRAINT valley_cross_section_geometry_name_key UNIQUE (name);


--
-- Name: valley_cross_section_geometry valley_cross_section_geometry_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY valley_cross_section_geometry
    ADD CONSTRAINT valley_cross_section_geometry_pkey PRIMARY KEY (id);


--
-- Name: valley_cross_section_topo_geometry valley_cross_section_topo_geometry_name_key; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY valley_cross_section_topo_geometry
    ADD CONSTRAINT valley_cross_section_topo_geometry_name_key UNIQUE (name);


--
-- Name: valley_cross_section_topo_geometry valley_cross_section_topo_geometry_pkey; Type: CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY valley_cross_section_topo_geometry
    ADD CONSTRAINT valley_cross_section_topo_geometry_pkey PRIMARY KEY (id);


SET search_path = project, pg_catalog;

--
-- Name: _caquot_rainfall _caquot_rainfall_id_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _caquot_rainfall
    ADD CONSTRAINT _caquot_rainfall_id_key UNIQUE (id);


--
-- Name: _double_triangular_rainfall _double_triangular_rainfall_id_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _double_triangular_rainfall
    ADD CONSTRAINT _double_triangular_rainfall_id_key UNIQUE (id);


--
-- Name: _gage_rainfall _gage_rainfall_id_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _gage_rainfall
    ADD CONSTRAINT _gage_rainfall_id_key UNIQUE (id);


--
-- Name: _intensity_curve_rainfall _intensity_curve_rainfall_id_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _intensity_curve_rainfall
    ADD CONSTRAINT _intensity_curve_rainfall_id_key UNIQUE (id);


--
-- Name: _radar_rainfall _radar_rainfall_id_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _radar_rainfall
    ADD CONSTRAINT _radar_rainfall_id_key UNIQUE (id);


--
-- Name: _rainfall _rainfall_id_rainfall_type_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _rainfall
    ADD CONSTRAINT _rainfall_id_rainfall_type_key UNIQUE (id, rainfall_type);


--
-- Name: _rainfall _rainfall_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _rainfall
    ADD CONSTRAINT _rainfall_pkey PRIMARY KEY (id);


--
-- Name: _single_triangular_rainfall _single_triangular_rainfall_id_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _single_triangular_rainfall
    ADD CONSTRAINT _single_triangular_rainfall_id_key UNIQUE (id);


--
-- Name: bc_tz_data bc_tz_data_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY bc_tz_data
    ADD CONSTRAINT bc_tz_data_pkey PRIMARY KEY (id);


--
-- Name: c_affin_param c_affin_param_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY c_affin_param
    ADD CONSTRAINT c_affin_param_pkey PRIMARY KEY (id);


--
-- Name: c_affin_param c_affin_param_scenario_model_type_domain_element_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY c_affin_param
    ADD CONSTRAINT c_affin_param_scenario_model_type_domain_element_key UNIQUE (scenario, model, type_domain, element);


--
-- Name: coef_montana coef_montana_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY coef_montana
    ADD CONSTRAINT coef_montana_pkey PRIMARY KEY (id);


--
-- Name: dem dem_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dem
    ADD CONSTRAINT dem_pkey PRIMARY KEY (id);


--
-- Name: dem dem_source_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dem
    ADD CONSTRAINT dem_source_key UNIQUE (source);


--
-- Name: dry_inflow_hourly_modulation dry_inflow_hourly_modulation_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dry_inflow_hourly_modulation
    ADD CONSTRAINT dry_inflow_hourly_modulation_pkey PRIMARY KEY (id);


--
-- Name: dry_inflow_scenario dry_inflow_scenario_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dry_inflow_scenario
    ADD CONSTRAINT dry_inflow_scenario_pkey PRIMARY KEY (id);


--
-- Name: dry_inflow_scenario_sector_setting dry_inflow_scenario_sector_setti_dry_inflow_scenario_sector_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dry_inflow_scenario_sector_setting
    ADD CONSTRAINT dry_inflow_scenario_sector_setti_dry_inflow_scenario_sector_key UNIQUE (dry_inflow_scenario, sector);


--
-- Name: dry_inflow_sector dry_inflow_sector_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dry_inflow_sector
    ADD CONSTRAINT dry_inflow_sector_pkey PRIMARY KEY (id);


--
-- Name: grp_model grp_model_grp_model_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY grp_model
    ADD CONSTRAINT grp_model_grp_model_key UNIQUE (grp, model);


--
-- Name: grp grp_name_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY grp
    ADD CONSTRAINT grp_name_key UNIQUE (name);


--
-- Name: grp grp_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY grp
    ADD CONSTRAINT grp_pkey PRIMARY KEY (id);


--
-- Name: hydrograph_pollution hydrograph_pollution_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY hydrograph_pollution
    ADD CONSTRAINT hydrograph_pollution_pkey PRIMARY KEY (id);


--
-- Name: hydrograph_quality hydrograph_quality_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY hydrograph_quality
    ADD CONSTRAINT hydrograph_quality_pkey PRIMARY KEY (id);


--
-- Name: hydrograph_variable_dry_flow hydrograph_variable_dry_flow_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY hydrograph_variable_dry_flow
    ADD CONSTRAINT hydrograph_variable_dry_flow_pkey PRIMARY KEY (id);


--
-- Name: inflow_rerouting inflow_rerouting_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY inflow_rerouting
    ADD CONSTRAINT inflow_rerouting_pkey PRIMARY KEY (id);


--
-- Name: land_use land_use_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY land_use
    ADD CONSTRAINT land_use_pkey PRIMARY KEY (id);


--
-- Name: mixed mixed_scenario_grp_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY mixed
    ADD CONSTRAINT mixed_scenario_grp_key UNIQUE (scenario, grp);


--
-- Name: mixed mixed_scenario_ord_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY mixed
    ADD CONSTRAINT mixed_scenario_ord_key UNIQUE (scenario, ord);


--
-- Name: model_config model_config_name_config_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY model_config
    ADD CONSTRAINT model_config_name_config_key UNIQUE (name, config);


--
-- Name: model_config model_config_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY model_config
    ADD CONSTRAINT model_config_pkey PRIMARY KEY (id);


--
-- Name: output_option output_option_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY output_option
    ADD CONSTRAINT output_option_pkey PRIMARY KEY (id);


--
-- Name: param_external_hydrograph param_external_hydrograph_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_external_hydrograph
    ADD CONSTRAINT param_external_hydrograph_pkey PRIMARY KEY (id);


--
-- Name: param_hydrograph param_hydrograph_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_hydrograph
    ADD CONSTRAINT param_hydrograph_pkey PRIMARY KEY (id);


--
-- Name: param_measure param_measure_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_measure
    ADD CONSTRAINT param_measure_pkey PRIMARY KEY (id);


--
-- Name: param_regulation param_regulation_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_regulation
    ADD CONSTRAINT param_regulation_pkey PRIMARY KEY (id);


--
-- Name: param_scenario_group param_scenario_group_scenario_grp_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_scenario_group
    ADD CONSTRAINT param_scenario_group_scenario_grp_key UNIQUE (scenario, grp);


--
-- Name: gage_rainfall_data pk_id; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY gage_rainfall_data
    ADD CONSTRAINT pk_id PRIMARY KEY (rainfall, rain_gage);


--
-- Name: points_xyz points_xyz_name_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY points_xyz
    ADD CONSTRAINT points_xyz_name_key UNIQUE (name);


--
-- Name: points_xyz points_xyz_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY points_xyz
    ADD CONSTRAINT points_xyz_pkey PRIMARY KEY (id);


--
-- Name: pollution_land_accumulation pollution_land_accumulation_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY pollution_land_accumulation
    ADD CONSTRAINT pollution_land_accumulation_pkey PRIMARY KEY (id);


--
-- Name: radar_grid radar_grid_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY radar_grid
    ADD CONSTRAINT radar_grid_pkey PRIMARY KEY (id);


--
-- Name: rain_gage rain_gage_name_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY rain_gage
    ADD CONSTRAINT rain_gage_name_key UNIQUE (name);


--
-- Name: rain_gage rain_gage_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY rain_gage
    ADD CONSTRAINT rain_gage_pkey PRIMARY KEY (id);


--
-- Name: scenario_group scenario_group_name_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY scenario_group
    ADD CONSTRAINT scenario_group_name_key UNIQUE (name);


--
-- Name: scenario_group scenario_group_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY scenario_group
    ADD CONSTRAINT scenario_group_pkey PRIMARY KEY (id);


--
-- Name: scenario scenario_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY scenario
    ADD CONSTRAINT scenario_pkey PRIMARY KEY (id);


--
-- Name: strickler_param strickler_param_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY strickler_param
    ADD CONSTRAINT strickler_param_pkey PRIMARY KEY (id);


--
-- Name: strickler_param strickler_param_scenario_model_type_domain_element_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY strickler_param
    ADD CONSTRAINT strickler_param_scenario_model_type_domain_element_key UNIQUE (scenario, model, type_domain, element);


--
-- Name: wind_gage wind_gage_name_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY wind_gage
    ADD CONSTRAINT wind_gage_name_key UNIQUE (name);


--
-- Name: wind_gage wind_gage_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY wind_gage
    ADD CONSTRAINT wind_gage_pkey PRIMARY KEY (id);


--
-- Name: wind_scenario_anemometer_data wind_scenario_anemometer_data_wind_gage_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY wind_scenario_anemometer_data
    ADD CONSTRAINT wind_scenario_anemometer_data_wind_gage_key UNIQUE (wind_gage);


--
-- Name: wind_scenario_anemometer_data wind_scenario_anemometer_data_wind_gage_wind_scenario_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY wind_scenario_anemometer_data
    ADD CONSTRAINT wind_scenario_anemometer_data_wind_gage_wind_scenario_key UNIQUE (wind_gage, wind_scenario);


--
-- Name: wind_scenario_anemometer_data wind_scenario_anemometer_data_wind_scenario_key; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY wind_scenario_anemometer_data
    ADD CONSTRAINT wind_scenario_anemometer_data_wind_scenario_key UNIQUE (wind_scenario);


--
-- Name: wind_scenario wind_scenario_pkey; Type: CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY wind_scenario
    ADD CONSTRAINT wind_scenario_pkey PRIMARY KEY (id);


SET search_path = model1, pg_catalog;

--
-- Name: model1_constrain_discretized_geom_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_constrain_discretized_geom_idx ON _constrain USING gist (discretized);


--
-- Name: model1_constrain_geom_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_constrain_geom_idx ON _constrain USING gist (geom);


--
-- Name: model1_coverage_geom_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_coverage_geom_idx ON coverage USING gist (geom);


--
-- Name: model1_elem_2d_node_contour_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_elem_2d_node_contour_idx ON _elem_2d_node USING gist (contour);


--
-- Name: model1_link_down_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_link_down_idx ON _link USING btree (down);


--
-- Name: model1_link_generated_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_link_generated_idx ON _link USING btree (generated);


--
-- Name: model1_link_geom_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_link_geom_idx ON _link USING gist (geom);


--
-- Name: model1_link_name_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_link_name_idx ON _link USING btree (name);


--
-- Name: model1_link_up_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_link_up_idx ON _link USING btree (up);


--
-- Name: model1_node_generated_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_node_generated_idx ON _node USING btree (generated);


--
-- Name: model1_node_geom_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_node_geom_idx ON _node USING gist (geom);


--
-- Name: model1_node_name_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_node_name_idx ON _node USING btree (name);


--
-- Name: model1_reach_geom_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_reach_geom_idx ON reach USING gist (geom);


--
-- Name: model1_singularity_name_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_singularity_name_idx ON _singularity USING btree (name);


--
-- Name: model1_street_geom_idx; Type: INDEX; Schema: model1; Owner: thierry.lepelletier
--

CREATE INDEX model1_street_geom_idx ON street USING gist (geom);


--
-- Name: results_surface _RETURN; Type: RULE; Schema: model1; Owner: thierry.lepelletier
--

CREATE RULE "_RETURN" AS
    ON SELECT TO results_surface DO INSTEAD  WITH dump AS (
         SELECT public.st_collect(n.geom) AS points,
            s.geom AS envelope,
            s.id
           FROM coverage s,
            crossroad_node n
          WHERE ((s.domain_type = 'street'::public.hydra_coverage_type) AND public.st_intersects(s.geom, n.geom))
          GROUP BY s.id
        ), voronoi AS (
         SELECT public.st_intersection(public.st_setsrid((public.st_dump(public.st_voronoipolygons(dump.points, (0)::double precision, dump.envelope))).geom, 2154), dump.envelope) AS geom
           FROM dump
        )
 SELECT e.id,
    e.name,
    e.contour AS geom
   FROM elem_2d_node e
UNION
 SELECT n.id,
    n.name,
    c.geom
   FROM (storage_node n
     JOIN coverage c ON ((n.contour = c.id)))
UNION
 SELECT n.id,
    n.name,
    voronoi.geom
   FROM voronoi,
    crossroad_node n
  WHERE public.st_intersects(voronoi.geom, n.geom)
UNION
 SELECT n.id,
    n.name,
    c.geom
   FROM crossroad_node n,
    coverage c
  WHERE ((c.domain_type = 'crossroad'::public.hydra_coverage_type) AND public.st_intersects(n.geom, c.geom))
  ORDER BY 1;


--
-- Name: borda_headloss_link model1_borda_headloss_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_borda_headloss_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON borda_headloss_link FOR EACH ROW EXECUTE PROCEDURE borda_headloss_link_fct();


--
-- Name: borda_headloss_singularity model1_borda_headloss_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_borda_headloss_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON borda_headloss_singularity FOR EACH ROW EXECUTE PROCEDURE borda_headloss_singularity_fct();


--
-- Name: bradley_headloss_singularity model1_bradley_headloss_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_bradley_headloss_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON bradley_headloss_singularity FOR EACH ROW EXECUTE PROCEDURE bradley_headloss_singularity_fct();


--
-- Name: branch model1_branch_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_branch_trig AFTER INSERT OR DELETE OR UPDATE ON branch FOR EACH ROW EXECUTE PROCEDURE branch_after_fct();


--
-- Name: bridge_headloss_singularity model1_bridge_headloss_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_bridge_headloss_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON bridge_headloss_singularity FOR EACH ROW EXECUTE PROCEDURE bridge_headloss_singularity_fct();


--
-- Name: catchment model1_catchment_after_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_catchment_after_trig AFTER INSERT OR UPDATE ON catchment FOR EACH ROW EXECUTE PROCEDURE catchment_after_fct();


--
-- Name: catchment_node model1_catchment_node_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_catchment_node_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON catchment_node FOR EACH ROW EXECUTE PROCEDURE catchment_node_fct();


--
-- Name: connector_hydrology_link model1_connector_hydrology_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_connector_hydrology_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON connector_hydrology_link FOR EACH ROW EXECUTE PROCEDURE connector_hydrology_link_fct();


--
-- Name: connector_link model1_connector_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_connector_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON connector_link FOR EACH ROW EXECUTE PROCEDURE connector_link_fct();


--
-- Name: constant_inflow_bc_singularity model1_constant_inflow_bc_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_constant_inflow_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON constant_inflow_bc_singularity FOR EACH ROW EXECUTE PROCEDURE constant_inflow_bc_singularity_fct();


--
-- Name: _constrain model1_constrain_after_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_constrain_after_trig AFTER INSERT OR DELETE OR UPDATE OF geom, elem_length ON _constrain FOR EACH STATEMENT EXECUTE PROCEDURE constrain_after_fct();


--
-- Name: constrain model1_constrain_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_constrain_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON constrain FOR EACH ROW EXECUTE PROCEDURE constrain_fct();


--
-- Name: coverage model1_coverage_after_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_coverage_after_trig AFTER INSERT OR UPDATE OF geom ON coverage FOR EACH ROW EXECUTE PROCEDURE coverage_after_fct();


--
-- Name: crossroad_node model1_crossroad_node_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_crossroad_node_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON crossroad_node FOR EACH ROW EXECUTE PROCEDURE crossroad_node_fct();


--
-- Name: deriv_pump_link model1_deriv_pump_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_deriv_pump_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON deriv_pump_link FOR EACH ROW EXECUTE PROCEDURE deriv_pump_link_fct();


--
-- Name: elem_2d_node model1_elem_2d_node_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_elem_2d_node_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON elem_2d_node FOR EACH ROW EXECUTE PROCEDURE elem_2d_node_fct();


--
-- Name: froude_bc_singularity model1_froude_bc_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_froude_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON froude_bc_singularity FOR EACH ROW EXECUTE PROCEDURE froude_bc_singularity_fct();


--
-- Name: fuse_spillway_link model1_fuse_spillway_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_fuse_spillway_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON fuse_spillway_link FOR EACH ROW EXECUTE PROCEDURE fuse_spillway_link_fct();


--
-- Name: gate_link model1_gate_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_gate_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON gate_link FOR EACH ROW EXECUTE PROCEDURE gate_link_fct();


--
-- Name: gate_singularity model1_gate_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_gate_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON gate_singularity FOR EACH ROW EXECUTE PROCEDURE gate_singularity_fct();


--
-- Name: hydraulic_cut_singularity model1_hydraulic_cut_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_hydraulic_cut_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON hydraulic_cut_singularity FOR EACH ROW EXECUTE PROCEDURE hydraulic_cut_singularity_fct();


--
-- Name: hydrograph_bc_singularity model1_hydrograph_bc_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_hydrograph_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON hydrograph_bc_singularity FOR EACH ROW EXECUTE PROCEDURE hydrograph_bc_singularity_fct();


--
-- Name: hydrology_bc_singularity model1_hydrology_bc_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_hydrology_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON hydrology_bc_singularity FOR EACH ROW EXECUTE PROCEDURE hydrology_bc_singularity_fct();


--
-- Name: _link model1_link_after_delete_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_link_after_delete_trig AFTER DELETE ON _link FOR EACH ROW EXECUTE PROCEDURE _link_after_delete_fct();


--
-- Name: manhole_hydrology_node model1_manhole_hydrology_node_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_manhole_hydrology_node_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON manhole_hydrology_node FOR EACH ROW EXECUTE PROCEDURE manhole_hydrology_node_fct();


--
-- Name: manhole_node model1_manhole_node_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_manhole_node_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON manhole_node FOR EACH ROW EXECUTE PROCEDURE manhole_node_fct();


--
-- Name: marker_singularity model1_marker_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_marker_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON marker_singularity FOR EACH ROW EXECUTE PROCEDURE marker_singularity_fct();


--
-- Name: mesh_2d_link model1_mesh_2d_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_mesh_2d_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON mesh_2d_link FOR EACH ROW EXECUTE PROCEDURE mesh_2d_link_fct();


--
-- Name: metadata model1_metadata_configuration_after_update_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_metadata_configuration_after_update_trig BEFORE UPDATE OF configuration ON metadata FOR EACH ROW EXECUTE PROCEDURE metadata_configuration_after_update_fct();


--
-- Name: coverage_marker model1_mkc_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_mkc_trig AFTER INSERT OR DELETE OR UPDATE OF geom ON coverage_marker FOR EACH ROW EXECUTE PROCEDURE mkc_after_fct();


--
-- Name: domain_2d_marker model1_mkd_after_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_mkd_after_trig AFTER INSERT OR DELETE OR UPDATE OF geom, domain_2d ON domain_2d_marker FOR EACH ROW EXECUTE PROCEDURE mkd_after_fct();


--
-- Name: model_connect_bc_singularity model1_model_connect_bc_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_model_connect_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON model_connect_bc_singularity FOR EACH ROW EXECUTE PROCEDURE model_connect_bc_singularity_fct();


--
-- Name: network_overflow_link model1_network_overflow_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_network_overflow_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON network_overflow_link FOR EACH ROW EXECUTE PROCEDURE network_overflow_link_fct();


--
-- Name: _node model1_node_update_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_node_update_trig AFTER UPDATE OF geom ON _node FOR EACH ROW EXECUTE PROCEDURE node_update_fct();


--
-- Name: overflow_link model1_overflow_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_overflow_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON overflow_link FOR EACH ROW EXECUTE PROCEDURE overflow_link_fct();


--
-- Name: param_headloss_singularity model1_param_headloss_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_param_headloss_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON param_headloss_singularity FOR EACH ROW EXECUTE PROCEDURE param_headloss_singularity_fct();


--
-- Name: pipe_branch_marker_singularity model1_pipe_branch_marker_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_pipe_branch_marker_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON pipe_branch_marker_singularity FOR EACH ROW EXECUTE PROCEDURE pipe_branch_marker_singularity_fct();


--
-- Name: pipe_link model1_pipe_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_pipe_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON pipe_link FOR EACH ROW EXECUTE PROCEDURE pipe_link_fct();


--
-- Name: porous_link model1_porous_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_porous_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON porous_link FOR EACH ROW EXECUTE PROCEDURE porous_link_fct();


--
-- Name: pump_link model1_pump_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_pump_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON pump_link FOR EACH ROW EXECUTE PROCEDURE pump_link_fct();


--
-- Name: qq_split_hydrology_singularity model1_qq_split_hydrology_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_qq_split_hydrology_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON qq_split_hydrology_singularity FOR EACH ROW EXECUTE PROCEDURE qq_split_hydrology_singularity_fct();


--
-- Name: reach model1_reach_after_insert_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_reach_after_insert_trig AFTER INSERT ON reach FOR EACH ROW EXECUTE PROCEDURE reach_after_insert_fct();


--
-- Name: reach model1_reach_after_update_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_reach_after_update_trig AFTER UPDATE ON reach FOR EACH ROW EXECUTE PROCEDURE reach_after_update_fct();


--
-- Name: reach model1_reach_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_reach_trig AFTER INSERT OR DELETE OR UPDATE ON reach FOR EACH ROW EXECUTE PROCEDURE reach_after_fct();


--
-- Name: regul_gate_link model1_regul_gate_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_regul_gate_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON regul_gate_link FOR EACH ROW EXECUTE PROCEDURE regul_gate_link_fct();


--
-- Name: regul_sluice_gate_singularity model1_regul_sluice_gate_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_regul_sluice_gate_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON regul_sluice_gate_singularity FOR EACH ROW EXECUTE PROCEDURE regul_sluice_gate_singularity_fct();


--
-- Name: reservoir_rs_hydrology_singularity model1_reservoir_rs_hydrology_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_reservoir_rs_hydrology_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON reservoir_rs_hydrology_singularity FOR EACH ROW EXECUTE PROCEDURE reservoir_rs_hydrology_singularity_fct();


--
-- Name: reservoir_rsp_hydrology_singularity model1_reservoir_rsp_hydrology_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_reservoir_rsp_hydrology_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON reservoir_rsp_hydrology_singularity FOR EACH ROW EXECUTE PROCEDURE reservoir_rsp_hydrology_singularity_fct();


--
-- Name: river_cross_section_pl1d model1_river_cross_section_pl1d_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_river_cross_section_pl1d_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON river_cross_section_pl1d FOR EACH ROW EXECUTE PROCEDURE river_cross_section_pl1d_fct();


--
-- Name: river_cross_section_profile model1_river_cross_section_profile_del_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_river_cross_section_profile_del_trig INSTEAD OF DELETE ON river_cross_section_profile FOR EACH ROW EXECUTE PROCEDURE river_cross_section_profile_del_fct();


--
-- Name: river_cross_section_profile model1_river_cross_section_profile_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_river_cross_section_profile_trig INSTEAD OF INSERT OR UPDATE ON river_cross_section_profile FOR EACH ROW EXECUTE PROCEDURE river_cross_section_profile_fct();


--
-- Name: river_node model1_river_node_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_river_node_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON river_node FOR EACH ROW EXECUTE PROCEDURE river_node_fct();


--
-- Name: routing_hydrology_link model1_routing_hydrology_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_routing_hydrology_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON routing_hydrology_link FOR EACH ROW EXECUTE PROCEDURE routing_hydrology_link_fct();


--
-- Name: station_node model1_station_node_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_station_node_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON station_node FOR EACH ROW EXECUTE PROCEDURE station_node_fct();


--
-- Name: station model1_station_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_station_trig AFTER INSERT OR DELETE OR UPDATE ON station FOR EACH ROW EXECUTE PROCEDURE station_after_fct();


--
-- Name: storage_node model1_storage_node_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_storage_node_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON storage_node FOR EACH ROW EXECUTE PROCEDURE storage_node_fct();


--
-- Name: street model1_street_after_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_street_after_trig AFTER INSERT OR UPDATE OF width, geom ON street FOR EACH ROW EXECUTE PROCEDURE street_after_fct();


--
-- Name: street_link model1_street_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_street_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON street_link FOR EACH ROW EXECUTE PROCEDURE street_link_fct();


--
-- Name: strickler_bc_singularity model1_strickler_bc_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_strickler_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON strickler_bc_singularity FOR EACH ROW EXECUTE PROCEDURE strickler_bc_singularity_fct();


--
-- Name: strickler_link model1_strickler_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_strickler_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON strickler_link FOR EACH ROW EXECUTE PROCEDURE strickler_link_fct();


--
-- Name: tank_bc_singularity model1_tank_bc_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_tank_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON tank_bc_singularity FOR EACH ROW EXECUTE PROCEDURE tank_bc_singularity_fct();


--
-- Name: tz_bc_singularity model1_tz_bc_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_tz_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON tz_bc_singularity FOR EACH ROW EXECUTE PROCEDURE tz_bc_singularity_fct();


--
-- Name: weir_bc_singularity model1_weir_bc_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_weir_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON weir_bc_singularity FOR EACH ROW EXECUTE PROCEDURE weir_bc_singularity_fct();


--
-- Name: weir_link model1_weir_link_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_weir_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON weir_link FOR EACH ROW EXECUTE PROCEDURE weir_link_fct();


--
-- Name: zq_bc_singularity model1_zq_bc_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_zq_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON zq_bc_singularity FOR EACH ROW EXECUTE PROCEDURE zq_bc_singularity_fct();


--
-- Name: zq_split_hydrology_singularity model1_zq_split_hydrology_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_zq_split_hydrology_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON zq_split_hydrology_singularity FOR EACH ROW EXECUTE PROCEDURE zq_split_hydrology_singularity_fct();


--
-- Name: zregul_weir_singularity model1_zregul_weir_singularity_trig; Type: TRIGGER; Schema: model1; Owner: thierry.lepelletier
--

CREATE TRIGGER model1_zregul_weir_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON zregul_weir_singularity FOR EACH ROW EXECUTE PROCEDURE zregul_weir_singularity_fct();


SET search_path = project, pg_catalog;

--
-- Name: model model_intead_trig; Type: TRIGGER; Schema: project; Owner: thierry.lepelletier
--

CREATE TRIGGER model_intead_trig INSTEAD OF INSERT ON model FOR EACH ROW EXECUTE PROCEDURE model_instead_fct();


--
-- Name: caquot_rainfall project_caquot_rainfall_trig; Type: TRIGGER; Schema: project; Owner: thierry.lepelletier
--

CREATE TRIGGER project_caquot_rainfall_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON caquot_rainfall FOR EACH ROW EXECUTE PROCEDURE caquot_rainfall_fct();


--
-- Name: double_triangular_rainfall project_double_triangular_rainfall_trig; Type: TRIGGER; Schema: project; Owner: thierry.lepelletier
--

CREATE TRIGGER project_double_triangular_rainfall_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON double_triangular_rainfall FOR EACH ROW EXECUTE PROCEDURE double_triangular_rainfall_fct();


--
-- Name: gage_rainfall project_gage_rainfall_trig; Type: TRIGGER; Schema: project; Owner: thierry.lepelletier
--

CREATE TRIGGER project_gage_rainfall_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON gage_rainfall FOR EACH ROW EXECUTE PROCEDURE gage_rainfall_fct();


--
-- Name: intensity_curve_rainfall project_intensity_curve_rainfall_trig; Type: TRIGGER; Schema: project; Owner: thierry.lepelletier
--

CREATE TRIGGER project_intensity_curve_rainfall_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON intensity_curve_rainfall FOR EACH ROW EXECUTE PROCEDURE intensity_curve_rainfall_fct();


--
-- Name: radar_rainfall project_radar_rainfall_trig; Type: TRIGGER; Schema: project; Owner: thierry.lepelletier
--

CREATE TRIGGER project_radar_rainfall_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON radar_rainfall FOR EACH ROW EXECUTE PROCEDURE radar_rainfall_fct();


--
-- Name: single_triangular_rainfall project_single_triangular_rainfall_trig; Type: TRIGGER; Schema: project; Owner: thierry.lepelletier
--

CREATE TRIGGER project_single_triangular_rainfall_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON single_triangular_rainfall FOR EACH ROW EXECUTE PROCEDURE single_triangular_rainfall_fct();


SET search_path = model1, pg_catalog;

--
-- Name: _borda_headloss_link _borda_headloss_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _borda_headloss_link
    ADD CONSTRAINT _borda_headloss_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _borda_headloss_singularity _borda_headloss_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _borda_headloss_singularity
    ADD CONSTRAINT _borda_headloss_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _bradley_headloss_singularity _bradley_headloss_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _bradley_headloss_singularity
    ADD CONSTRAINT _bradley_headloss_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _bridge_headloss_singularity _bridge_headloss_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _bridge_headloss_singularity
    ADD CONSTRAINT _bridge_headloss_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _catchment_node _catchment_node_contour_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _catchment_node
    ADD CONSTRAINT _catchment_node_contour_fkey FOREIGN KEY (contour) REFERENCES catchment(id) ON DELETE SET NULL;


--
-- Name: _catchment_node _catchment_node_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _catchment_node
    ADD CONSTRAINT _catchment_node_id_fkey FOREIGN KEY (id, node_type) REFERENCES _node(id, node_type) ON DELETE CASCADE;


--
-- Name: _connector_hydrology_link _connector_hydrology_link_hydrograph_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _connector_hydrology_link
    ADD CONSTRAINT _connector_hydrology_link_hydrograph_fkey FOREIGN KEY (hydrograph) REFERENCES _hydrograph_bc_singularity(id);


--
-- Name: _connector_hydrology_link _connector_hydrology_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _connector_hydrology_link
    ADD CONSTRAINT _connector_hydrology_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _connector_link _connector_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _connector_link
    ADD CONSTRAINT _connector_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _constant_inflow_bc_singularity _constant_inflow_bc_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _constant_inflow_bc_singularity
    ADD CONSTRAINT _constant_inflow_bc_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _crossroad_node _crossroad_node_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _crossroad_node
    ADD CONSTRAINT _crossroad_node_id_fkey FOREIGN KEY (id, node_type) REFERENCES _node(id, node_type) ON DELETE CASCADE;


--
-- Name: _deriv_pump_link _deriv_pump_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _deriv_pump_link
    ADD CONSTRAINT _deriv_pump_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _elem_2d_node _elem_2d_node_domain_2d_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _elem_2d_node
    ADD CONSTRAINT _elem_2d_node_domain_2d_fkey FOREIGN KEY (domain_2d) REFERENCES domain_2d(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _elem_2d_node _elem_2d_node_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _elem_2d_node
    ADD CONSTRAINT _elem_2d_node_id_fkey FOREIGN KEY (id, node_type) REFERENCES _node(id, node_type) ON DELETE CASCADE;


--
-- Name: _froude_bc_singularity _froude_bc_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _froude_bc_singularity
    ADD CONSTRAINT _froude_bc_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _fuse_spillway_link _fuse_spillway_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _fuse_spillway_link
    ADD CONSTRAINT _fuse_spillway_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _gate_link _gate_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _gate_link
    ADD CONSTRAINT _gate_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _gate_singularity _gate_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _gate_singularity
    ADD CONSTRAINT _gate_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _hydraulic_cut_singularity _hydraulic_cut_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _hydraulic_cut_singularity
    ADD CONSTRAINT _hydraulic_cut_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _hydrograph_bc_singularity _hydrograph_bc_singularity_hourly_modulation_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _hydrograph_bc_singularity
    ADD CONSTRAINT _hydrograph_bc_singularity_hourly_modulation_fkey FOREIGN KEY (hourly_modulation) REFERENCES project.dry_inflow_hourly_modulation(id);


--
-- Name: _hydrograph_bc_singularity _hydrograph_bc_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _hydrograph_bc_singularity
    ADD CONSTRAINT _hydrograph_bc_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _hydrograph_bc_singularity _hydrograph_bc_singularity_sector_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _hydrograph_bc_singularity
    ADD CONSTRAINT _hydrograph_bc_singularity_sector_fkey FOREIGN KEY (sector) REFERENCES project.dry_inflow_sector(id) ON DELETE SET NULL;


--
-- Name: _hydrology_bc_singularity _hydrology_bc_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _hydrology_bc_singularity
    ADD CONSTRAINT _hydrology_bc_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _link _link_down_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _link
    ADD CONSTRAINT _link_down_fkey FOREIGN KEY (down, down_type) REFERENCES _node(id, node_type) ON DELETE SET NULL;


--
-- Name: _link _link_generated_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _link
    ADD CONSTRAINT _link_generated_fkey FOREIGN KEY (generated) REFERENCES generation_step(id) ON DELETE CASCADE;


--
-- Name: _link _link_up_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _link
    ADD CONSTRAINT _link_up_fkey FOREIGN KEY (up, up_type) REFERENCES _node(id, node_type) ON DELETE SET NULL;


--
-- Name: _manhole_hydrology_node _manhole_hydrology_node_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _manhole_hydrology_node
    ADD CONSTRAINT _manhole_hydrology_node_id_fkey FOREIGN KEY (id, node_type) REFERENCES _node(id, node_type) ON DELETE CASCADE;


--
-- Name: _manhole_node _manhole_node_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _manhole_node
    ADD CONSTRAINT _manhole_node_id_fkey FOREIGN KEY (id, node_type) REFERENCES _node(id, node_type) ON DELETE CASCADE;


--
-- Name: _marker_singularity _marker_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _marker_singularity
    ADD CONSTRAINT _marker_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _mesh_2d_link _mesh_2d_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _mesh_2d_link
    ADD CONSTRAINT _mesh_2d_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _model_connect_bc_singularity _model_connect_bc_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _model_connect_bc_singularity
    ADD CONSTRAINT _model_connect_bc_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _network_overflow_link _network_overflow_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _network_overflow_link
    ADD CONSTRAINT _network_overflow_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _node _node_generated_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _node
    ADD CONSTRAINT _node_generated_fkey FOREIGN KEY (generated) REFERENCES generation_step(id) ON DELETE CASCADE;


--
-- Name: _overflow_link _overflow_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _overflow_link
    ADD CONSTRAINT _overflow_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _param_headloss_singularity _param_headloss_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _param_headloss_singularity
    ADD CONSTRAINT _param_headloss_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _pipe_branch_marker_singularity _pipe_branch_marker_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _pipe_branch_marker_singularity
    ADD CONSTRAINT _pipe_branch_marker_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _pipe_link _pipe_link_branch_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _pipe_link
    ADD CONSTRAINT _pipe_link_branch_fkey FOREIGN KEY (branch) REFERENCES branch(id) ON DELETE SET NULL;


--
-- Name: _pipe_link _pipe_link_cp_geom_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _pipe_link
    ADD CONSTRAINT _pipe_link_cp_geom_fkey FOREIGN KEY (cp_geom) REFERENCES closed_parametric_geometry(id);


--
-- Name: _pipe_link _pipe_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _pipe_link
    ADD CONSTRAINT _pipe_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _pipe_link _pipe_link_op_geom_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _pipe_link
    ADD CONSTRAINT _pipe_link_op_geom_fkey FOREIGN KEY (op_geom) REFERENCES open_parametric_geometry(id);


--
-- Name: _porous_link _porous_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _porous_link
    ADD CONSTRAINT _porous_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _pump_link _pump_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _pump_link
    ADD CONSTRAINT _pump_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _qq_split_hydrology_singularity _qq_split_hydrology_singularity_downstream_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _qq_split_hydrology_singularity
    ADD CONSTRAINT _qq_split_hydrology_singularity_downstream_fkey FOREIGN KEY (downstream, downstream_type) REFERENCES _link(id, link_type);


--
-- Name: _qq_split_hydrology_singularity _qq_split_hydrology_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _qq_split_hydrology_singularity
    ADD CONSTRAINT _qq_split_hydrology_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _qq_split_hydrology_singularity _qq_split_hydrology_singularity_split1_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _qq_split_hydrology_singularity
    ADD CONSTRAINT _qq_split_hydrology_singularity_split1_fkey FOREIGN KEY (split1, split1_type) REFERENCES _link(id, link_type);


--
-- Name: _qq_split_hydrology_singularity _qq_split_hydrology_singularity_split2_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _qq_split_hydrology_singularity
    ADD CONSTRAINT _qq_split_hydrology_singularity_split2_fkey FOREIGN KEY (split2, split2_type) REFERENCES _link(id, link_type);


--
-- Name: _regul_gate_link _regul_gate_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _regul_gate_link
    ADD CONSTRAINT _regul_gate_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _regul_gate_link _regul_gate_link_z_control_node_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _regul_gate_link
    ADD CONSTRAINT _regul_gate_link_z_control_node_fkey FOREIGN KEY (z_control_node) REFERENCES _node(id) ON DELETE SET NULL;


--
-- Name: _regul_sluice_gate_singularity _regul_sluice_gate_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _regul_sluice_gate_singularity
    ADD CONSTRAINT _regul_sluice_gate_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _regul_sluice_gate_singularity _regul_sluice_gate_singularity_z_control_node_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _regul_sluice_gate_singularity
    ADD CONSTRAINT _regul_sluice_gate_singularity_z_control_node_fkey FOREIGN KEY (z_control_node) REFERENCES _node(id) ON DELETE SET NULL;


--
-- Name: _reservoir_rs_hydrology_singularity _reservoir_rs_hydrology_singularity_drainage_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _reservoir_rs_hydrology_singularity
    ADD CONSTRAINT _reservoir_rs_hydrology_singularity_drainage_fkey FOREIGN KEY (drainage, drainage_type) REFERENCES _link(id, link_type);


--
-- Name: _reservoir_rs_hydrology_singularity _reservoir_rs_hydrology_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _reservoir_rs_hydrology_singularity
    ADD CONSTRAINT _reservoir_rs_hydrology_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _reservoir_rs_hydrology_singularity _reservoir_rs_hydrology_singularity_overflow_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _reservoir_rs_hydrology_singularity
    ADD CONSTRAINT _reservoir_rs_hydrology_singularity_overflow_fkey FOREIGN KEY (overflow, overflow_type) REFERENCES _link(id, link_type);


--
-- Name: _reservoir_rsp_hydrology_singularity _reservoir_rsp_hydrology_singularity_drainage_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _reservoir_rsp_hydrology_singularity
    ADD CONSTRAINT _reservoir_rsp_hydrology_singularity_drainage_fkey FOREIGN KEY (drainage, drainage_type) REFERENCES _link(id, link_type);


--
-- Name: _reservoir_rsp_hydrology_singularity _reservoir_rsp_hydrology_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _reservoir_rsp_hydrology_singularity
    ADD CONSTRAINT _reservoir_rsp_hydrology_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _reservoir_rsp_hydrology_singularity _reservoir_rsp_hydrology_singularity_overflow_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _reservoir_rsp_hydrology_singularity
    ADD CONSTRAINT _reservoir_rsp_hydrology_singularity_overflow_fkey FOREIGN KEY (overflow, overflow_type) REFERENCES _link(id, link_type);


--
-- Name: _river_cross_section_pl1d _river_cross_section_pl1d_generated_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_pl1d
    ADD CONSTRAINT _river_cross_section_pl1d_generated_fkey FOREIGN KEY (generated) REFERENCES generation_step(id) ON DELETE CASCADE;


--
-- Name: _river_cross_section_pl1d _river_cross_section_pl1d_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_pl1d
    ADD CONSTRAINT _river_cross_section_pl1d_id_fkey FOREIGN KEY (id) REFERENCES _river_node(id);


--
-- Name: _river_cross_section_profile _river_cross_section_profile_down_cp_geom_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_profile
    ADD CONSTRAINT _river_cross_section_profile_down_cp_geom_fkey FOREIGN KEY (down_cp_geom) REFERENCES closed_parametric_geometry(id);


--
-- Name: _river_cross_section_profile _river_cross_section_profile_down_op_geom_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_profile
    ADD CONSTRAINT _river_cross_section_profile_down_op_geom_fkey FOREIGN KEY (down_op_geom) REFERENCES open_parametric_geometry(id);


--
-- Name: _river_cross_section_profile _river_cross_section_profile_down_vcs_geom_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_profile
    ADD CONSTRAINT _river_cross_section_profile_down_vcs_geom_fkey FOREIGN KEY (down_vcs_geom) REFERENCES valley_cross_section_geometry(id);


--
-- Name: _river_cross_section_profile _river_cross_section_profile_down_vcs_topo_geom_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_profile
    ADD CONSTRAINT _river_cross_section_profile_down_vcs_topo_geom_fkey FOREIGN KEY (down_vcs_topo_geom) REFERENCES valley_cross_section_topo_geometry(id);


--
-- Name: _river_cross_section_profile _river_cross_section_profile_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_profile
    ADD CONSTRAINT _river_cross_section_profile_id_fkey FOREIGN KEY (id) REFERENCES _river_node(id);


--
-- Name: _river_cross_section_profile _river_cross_section_profile_up_cp_geom_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_profile
    ADD CONSTRAINT _river_cross_section_profile_up_cp_geom_fkey FOREIGN KEY (up_cp_geom) REFERENCES closed_parametric_geometry(id);


--
-- Name: _river_cross_section_profile _river_cross_section_profile_up_op_geom_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_profile
    ADD CONSTRAINT _river_cross_section_profile_up_op_geom_fkey FOREIGN KEY (up_op_geom) REFERENCES open_parametric_geometry(id);


--
-- Name: _river_cross_section_profile _river_cross_section_profile_up_vcs_geom_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_profile
    ADD CONSTRAINT _river_cross_section_profile_up_vcs_geom_fkey FOREIGN KEY (up_vcs_geom) REFERENCES valley_cross_section_geometry(id);


--
-- Name: _river_cross_section_profile _river_cross_section_profile_up_vcs_topo_geom_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_cross_section_profile
    ADD CONSTRAINT _river_cross_section_profile_up_vcs_topo_geom_fkey FOREIGN KEY (up_vcs_topo_geom) REFERENCES valley_cross_section_topo_geometry(id);


--
-- Name: _river_node _river_node_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_node
    ADD CONSTRAINT _river_node_id_fkey FOREIGN KEY (id, node_type) REFERENCES _node(id, node_type) ON DELETE CASCADE;


--
-- Name: _river_node _river_node_reach_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _river_node
    ADD CONSTRAINT _river_node_reach_fkey FOREIGN KEY (reach) REFERENCES reach(id);


--
-- Name: _routing_hydrology_link _routing_hydrology_link_hydrograph_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _routing_hydrology_link
    ADD CONSTRAINT _routing_hydrology_link_hydrograph_fkey FOREIGN KEY (hydrograph) REFERENCES _hydrograph_bc_singularity(id);


--
-- Name: _routing_hydrology_link _routing_hydrology_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _routing_hydrology_link
    ADD CONSTRAINT _routing_hydrology_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _singularity _singularity_node_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _singularity
    ADD CONSTRAINT _singularity_node_fkey FOREIGN KEY (node, node_type) REFERENCES _node(id, node_type);


--
-- Name: _station_node _station_node_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _station_node
    ADD CONSTRAINT _station_node_id_fkey FOREIGN KEY (id, node_type) REFERENCES _node(id, node_type) ON DELETE CASCADE;


--
-- Name: _station_node _station_node_station_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _station_node
    ADD CONSTRAINT _station_node_station_fkey FOREIGN KEY (station) REFERENCES station(id);


--
-- Name: _storage_node _storage_node_contour_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _storage_node
    ADD CONSTRAINT _storage_node_contour_fkey FOREIGN KEY (contour) REFERENCES coverage(id) ON DELETE SET NULL;


--
-- Name: _storage_node _storage_node_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _storage_node
    ADD CONSTRAINT _storage_node_id_fkey FOREIGN KEY (id, node_type) REFERENCES _node(id, node_type) ON DELETE CASCADE;


--
-- Name: _street_link _street_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _street_link
    ADD CONSTRAINT _street_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _strickler_bc_singularity _strickler_bc_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _strickler_bc_singularity
    ADD CONSTRAINT _strickler_bc_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _strickler_link _strickler_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _strickler_link
    ADD CONSTRAINT _strickler_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _tank_bc_singularity _tank_bc_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _tank_bc_singularity
    ADD CONSTRAINT _tank_bc_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _tz_bc_singularity _tz_bc_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _tz_bc_singularity
    ADD CONSTRAINT _tz_bc_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _weir_bc_singularity _weir_bc_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _weir_bc_singularity
    ADD CONSTRAINT _weir_bc_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _weir_link _weir_link_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _weir_link
    ADD CONSTRAINT _weir_link_id_fkey FOREIGN KEY (id, link_type) REFERENCES _link(id, link_type) ON DELETE CASCADE;


--
-- Name: _zq_bc_singularity _zq_bc_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _zq_bc_singularity
    ADD CONSTRAINT _zq_bc_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _zq_split_hydrology_singularity _zq_split_hydrology_singularity_downstream_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _zq_split_hydrology_singularity
    ADD CONSTRAINT _zq_split_hydrology_singularity_downstream_fkey FOREIGN KEY (downstream, downstream_type) REFERENCES _link(id, link_type);


--
-- Name: _zq_split_hydrology_singularity _zq_split_hydrology_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _zq_split_hydrology_singularity
    ADD CONSTRAINT _zq_split_hydrology_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: _zq_split_hydrology_singularity _zq_split_hydrology_singularity_split1_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _zq_split_hydrology_singularity
    ADD CONSTRAINT _zq_split_hydrology_singularity_split1_fkey FOREIGN KEY (split1, split1_type) REFERENCES _link(id, link_type);


--
-- Name: _zq_split_hydrology_singularity _zq_split_hydrology_singularity_split2_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _zq_split_hydrology_singularity
    ADD CONSTRAINT _zq_split_hydrology_singularity_split2_fkey FOREIGN KEY (split2, split2_type) REFERENCES _link(id, link_type);


--
-- Name: _zregul_weir_singularity _zregul_weir_singularity_id_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _zregul_weir_singularity
    ADD CONSTRAINT _zregul_weir_singularity_id_fkey FOREIGN KEY (id, singularity_type) REFERENCES _singularity(id, singularity_type);


--
-- Name: domain_2d_marker domain_2d_marker_domain_2d_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY domain_2d_marker
    ADD CONSTRAINT domain_2d_marker_domain_2d_fkey FOREIGN KEY (domain_2d) REFERENCES domain_2d(id);


--
-- Name: metadata metadata_configuration_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY metadata
    ADD CONSTRAINT metadata_configuration_fkey FOREIGN KEY (configuration) REFERENCES configuration(id);


--
-- Name: valley_cross_section_geometry valley_cross_section_geometry_transect_fkey; Type: FK CONSTRAINT; Schema: model1; Owner: thierry.lepelletier
--

ALTER TABLE ONLY valley_cross_section_geometry
    ADD CONSTRAINT valley_cross_section_geometry_transect_fkey FOREIGN KEY (transect) REFERENCES _constrain(id);


SET search_path = project, pg_catalog;

--
-- Name: _caquot_rainfall _caquot_rainfall_id_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _caquot_rainfall
    ADD CONSTRAINT _caquot_rainfall_id_fkey FOREIGN KEY (id, rainfall_type) REFERENCES _rainfall(id, rainfall_type);


--
-- Name: _caquot_rainfall _caquot_rainfall_montana_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _caquot_rainfall
    ADD CONSTRAINT _caquot_rainfall_montana_fkey FOREIGN KEY (montana) REFERENCES coef_montana(id);


--
-- Name: _double_triangular_rainfall _double_triangular_rainfall_id_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _double_triangular_rainfall
    ADD CONSTRAINT _double_triangular_rainfall_id_fkey FOREIGN KEY (id, rainfall_type) REFERENCES _rainfall(id, rainfall_type);


--
-- Name: _double_triangular_rainfall _double_triangular_rainfall_montana_peak_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _double_triangular_rainfall
    ADD CONSTRAINT _double_triangular_rainfall_montana_peak_fkey FOREIGN KEY (montana_peak) REFERENCES coef_montana(id);


--
-- Name: _double_triangular_rainfall _double_triangular_rainfall_montana_total_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _double_triangular_rainfall
    ADD CONSTRAINT _double_triangular_rainfall_montana_total_fkey FOREIGN KEY (montana_total) REFERENCES coef_montana(id);


--
-- Name: _gage_rainfall _gage_rainfall_id_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _gage_rainfall
    ADD CONSTRAINT _gage_rainfall_id_fkey FOREIGN KEY (id, rainfall_type) REFERENCES _rainfall(id, rainfall_type);


--
-- Name: _intensity_curve_rainfall _intensity_curve_rainfall_id_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _intensity_curve_rainfall
    ADD CONSTRAINT _intensity_curve_rainfall_id_fkey FOREIGN KEY (id, rainfall_type) REFERENCES _rainfall(id, rainfall_type);


--
-- Name: _radar_rainfall _radar_rainfall_id_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _radar_rainfall
    ADD CONSTRAINT _radar_rainfall_id_fkey FOREIGN KEY (id, rainfall_type) REFERENCES _rainfall(id, rainfall_type);


--
-- Name: _radar_rainfall _radar_rainfall_radar_grid_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _radar_rainfall
    ADD CONSTRAINT _radar_rainfall_radar_grid_fkey FOREIGN KEY (radar_grid) REFERENCES radar_grid(id);


--
-- Name: _single_triangular_rainfall _single_triangular_rainfall_id_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _single_triangular_rainfall
    ADD CONSTRAINT _single_triangular_rainfall_id_fkey FOREIGN KEY (id, rainfall_type) REFERENCES _rainfall(id, rainfall_type);


--
-- Name: _single_triangular_rainfall _single_triangular_rainfall_montana_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY _single_triangular_rainfall
    ADD CONSTRAINT _single_triangular_rainfall_montana_fkey FOREIGN KEY (montana) REFERENCES coef_montana(id);


--
-- Name: c_affin_param c_affin_param_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY c_affin_param
    ADD CONSTRAINT c_affin_param_scenario_fkey FOREIGN KEY (scenario) REFERENCES scenario(id) ON DELETE CASCADE;


--
-- Name: dry_inflow_scenario_sector_setting dry_inflow_scenario_sector_setting_dry_inflow_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dry_inflow_scenario_sector_setting
    ADD CONSTRAINT dry_inflow_scenario_sector_setting_dry_inflow_scenario_fkey FOREIGN KEY (dry_inflow_scenario) REFERENCES dry_inflow_scenario(id) ON DELETE CASCADE;


--
-- Name: dry_inflow_scenario_sector_setting dry_inflow_scenario_sector_setting_sector_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dry_inflow_scenario_sector_setting
    ADD CONSTRAINT dry_inflow_scenario_sector_setting_sector_fkey FOREIGN KEY (sector) REFERENCES dry_inflow_sector(id) ON DELETE CASCADE;


--
-- Name: dry_inflow_scenario_sector_setting dry_inflow_scenario_sector_setting_vol_curve_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY dry_inflow_scenario_sector_setting
    ADD CONSTRAINT dry_inflow_scenario_sector_setting_vol_curve_fkey FOREIGN KEY (vol_curve) REFERENCES dry_inflow_hourly_modulation(id);


--
-- Name: gage_rainfall_data gage_rainfall_data_rain_gage_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY gage_rainfall_data
    ADD CONSTRAINT gage_rainfall_data_rain_gage_fkey FOREIGN KEY (rain_gage) REFERENCES rain_gage(id) ON DELETE CASCADE;


--
-- Name: gage_rainfall_data gage_rainfall_data_rainfall_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY gage_rainfall_data
    ADD CONSTRAINT gage_rainfall_data_rainfall_fkey FOREIGN KEY (rainfall) REFERENCES _gage_rainfall(id) ON DELETE CASCADE;


--
-- Name: grp_model grp_model_grp_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY grp_model
    ADD CONSTRAINT grp_model_grp_fkey FOREIGN KEY (grp) REFERENCES grp(id) ON DELETE CASCADE;


--
-- Name: grp_model grp_model_model_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY grp_model
    ADD CONSTRAINT grp_model_model_fkey FOREIGN KEY (model) REFERENCES model_config(id);


--
-- Name: hydrograph_variable_dry_flow hydrograph_variable_dry_flow_dry_inflow_hourly_modulation_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY hydrograph_variable_dry_flow
    ADD CONSTRAINT hydrograph_variable_dry_flow_dry_inflow_hourly_modulation_fkey FOREIGN KEY (dry_inflow_hourly_modulation) REFERENCES dry_inflow_hourly_modulation(id);


--
-- Name: hydrograph_variable_dry_flow hydrograph_variable_dry_flow_sector_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY hydrograph_variable_dry_flow
    ADD CONSTRAINT hydrograph_variable_dry_flow_sector_fkey FOREIGN KEY (sector) REFERENCES dry_inflow_sector(id);


--
-- Name: inflow_rerouting inflow_rerouting_param_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY inflow_rerouting
    ADD CONSTRAINT inflow_rerouting_param_scenario_fkey FOREIGN KEY (param_scenario) REFERENCES scenario(id) ON DELETE CASCADE;


--
-- Name: mixed mixed_grp_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY mixed
    ADD CONSTRAINT mixed_grp_fkey FOREIGN KEY (grp) REFERENCES grp(id);


--
-- Name: mixed mixed_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY mixed
    ADD CONSTRAINT mixed_scenario_fkey FOREIGN KEY (scenario) REFERENCES scenario(id) ON DELETE CASCADE;


--
-- Name: output_option output_option_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY output_option
    ADD CONSTRAINT output_option_scenario_fkey FOREIGN KEY (scenario) REFERENCES scenario(id) ON DELETE CASCADE;


--
-- Name: param_external_hydrograph param_external_hydrograph_param_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_external_hydrograph
    ADD CONSTRAINT param_external_hydrograph_param_scenario_fkey FOREIGN KEY (param_scenario) REFERENCES scenario(id) ON DELETE CASCADE;


--
-- Name: param_hydrograph param_hydrograph_param_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_hydrograph
    ADD CONSTRAINT param_hydrograph_param_scenario_fkey FOREIGN KEY (param_scenario) REFERENCES scenario(id) ON DELETE CASCADE;


--
-- Name: param_measure param_measure_param_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_measure
    ADD CONSTRAINT param_measure_param_scenario_fkey FOREIGN KEY (param_scenario) REFERENCES scenario(id) ON DELETE CASCADE;


--
-- Name: param_regulation param_regulation_param_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_regulation
    ADD CONSTRAINT param_regulation_param_scenario_fkey FOREIGN KEY (param_scenario) REFERENCES scenario(id) ON DELETE CASCADE;


--
-- Name: param_scenario_group param_scenario_group_grp_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_scenario_group
    ADD CONSTRAINT param_scenario_group_grp_fkey FOREIGN KEY (grp) REFERENCES scenario_group(id) ON DELETE CASCADE;


--
-- Name: param_scenario_group param_scenario_group_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY param_scenario_group
    ADD CONSTRAINT param_scenario_group_scenario_fkey FOREIGN KEY (scenario) REFERENCES scenario(id) ON DELETE CASCADE;


--
-- Name: scenario scenario_dry_inflow_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY scenario
    ADD CONSTRAINT scenario_dry_inflow_fkey FOREIGN KEY (dry_inflow) REFERENCES dry_inflow_scenario(id);


--
-- Name: scenario scenario_rainfall_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY scenario
    ADD CONSTRAINT scenario_rainfall_fkey FOREIGN KEY (rainfall) REFERENCES _rainfall(id);


--
-- Name: scenario scenario_scenario_hydrology_rstart_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY scenario
    ADD CONSTRAINT scenario_scenario_hydrology_rstart_fkey FOREIGN KEY (scenario_hydrology_rstart) REFERENCES scenario(id);


--
-- Name: scenario scenario_scenario_rstart_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY scenario
    ADD CONSTRAINT scenario_scenario_rstart_fkey FOREIGN KEY (scenario_rstart) REFERENCES scenario(id);


--
-- Name: strickler_param strickler_param_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY strickler_param
    ADD CONSTRAINT strickler_param_scenario_fkey FOREIGN KEY (scenario) REFERENCES scenario(id) ON DELETE CASCADE;


--
-- Name: wind_scenario_anemometer_data wind_scenario_anemometer_data_wind_gage_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY wind_scenario_anemometer_data
    ADD CONSTRAINT wind_scenario_anemometer_data_wind_gage_fkey FOREIGN KEY (wind_gage) REFERENCES wind_gage(id);


--
-- Name: wind_scenario_anemometer_data wind_scenario_anemometer_data_wind_scenario_fkey; Type: FK CONSTRAINT; Schema: project; Owner: thierry.lepelletier
--

ALTER TABLE ONLY wind_scenario_anemometer_data
    ADD CONSTRAINT wind_scenario_anemometer_data_wind_scenario_fkey FOREIGN KEY (wind_scenario) REFERENCES wind_scenario(id);


--
-- PostgreSQL database dump complete
--
