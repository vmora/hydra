# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run hydra calculation

USAGE

   python -m hydra.kernel.hydra_kernel <calculation_directory>

OPTIONS

   -h, --help
        print this help
"""

import os
import sys
import getopt
from shutil import copyfile
import subprocess
from subprocess import Popen, PIPE
from PyQt4.QtGui import QApplication
from PyQt4.QtCore import QCoreApplication, QTranslator
from hydra.gui.text_info import TextInfo
import datetime

def execute(program, dest_dir, parent):
    if parent is not None:
        return gui_execute(program)

    logfilename = "{}_{}".format(program, ".exe.log")
    errfilename = "{}_{}".format(program, ".err.log")
    with open(logfilename, "w") as logfile:
        with open(errfilename, "w") as errfile:
            subprocess.Popen(program,
                    stderr=errfile,
                    stdout=logfile).communicate()
    copyfile(logfilename, os.path.join(dest_dir, os.path.basename(logfilename)))
    copyfile(errfilename, os.path.join(dest_dir, os.path.basename(errfilename)))
    os.remove(logfilename)
    os.remove(errfilename)
    return 0

def gui_execute(program):
    subprocess.Popen(program,
                    stderr=None,
                    stdout=None).communicate()

    return 0

def get_error(fname):
    err = 0
    with open(fname) as f:
        content = f.readlines()
        err = int(content[0])
    return err

def readfile(fname):
    content=""
    with open(fname) as f:
        content = f.readlines()
    return content

def log_info(info, info_form):
    if info_form is not None:
        if isinstance(info, list):
            for textinfo in info:
                info_form.textInfos.append(textinfo)
        else:
            info_form.textInfos.append(info)
            info_form.repaint()

def get_status(project_dir, calc_mode):
    def get_filedate(file_name):
        try:
            mtime = os.path.getmtime(file_name)
        except OSError:
            mtime = 0
        return datetime.datetime.fromtimestamp(mtime).strftime("%d/%m/%Y %H:%M:%S")

    cur_dir = os.getcwd()
    calc_dir = os.path.join(project_dir, "travail")
    hyd_dir = os.path.join(project_dir, "hydraulique")
    if not os.path.isdir(calc_dir):
        return -1, "Not calculated", "Never"
    os.chdir(calc_dir)

    if not os.path.exists("hydrol.err"):
        os.chdir(cur_dir)
        return -1, "Not calculated", "Never"

    if not calc_mode=="hydraulics":
        i=get_error("hydrol.err")
        if (i!=0):
            os.chdir(cur_dir)
            return i, "hydrol error", get_filedate("hydrol.err")

    if not calc_mode=="hydrology":
        i=get_error("hydragen.err")
        if (i!=0):
            os.chdir(cur_dir)
            return i, "hydragen error", get_filedate("hydragen.err")

    os.chdir(hyd_dir)
    i=get_error("whydram24.err")
    date=get_filedate("whydram24.err")
    os.chdir(cur_dir)
    if (i!=0):
        return i, "whydram24 error", date
    return 0, "Calculated", date

def run(project_dir, flag_gfx_control, calc_mode, parent=None, keep_opened=True):
    if parent is not None:
        info_form = TextInfo(parent)
        info_form.show()
        info_form.textInfos.clear()
    else:
        info_form=None
    i=0
    cur_dir = os.getcwd()

    log_info("Calculation starting", info_form)
    kernel_dir = os.path.abspath(os.path.dirname(__file__))
    calc_dir = os.path.join(project_dir, "travail")
    hyd_dir = os.path.join(project_dir, "hydraulique")

    os.chdir(calc_dir)

    if not calc_mode=="hydraulics":
        execute(os.path.join(kernel_dir, "hydrol.exe"), calc_dir, parent)
        i=get_error("hydrol.err")
        logs=readfile("hydrol.log")
        log_info(logs, info_form)
        if (i==0):
            log_info("hydrol ok", info_form)
        else:
            log_info("hydrol error: %i"%i, info_form)
            with open("hydrol.log") as f:
                content = f.readlines()
                for line in content:
                    log_info(line, info_form)
            return
    if not calc_mode=="hydrology":
        if (i==0):
            execute(os.path.join(kernel_dir, "hydragen.exe"), calc_dir, parent)
            logs=readfile("hydragen.log")
            log_info(logs, info_form)
            i=get_error("hydragen.err")
            if (i==0):
                log_info("hydragen ok", info_form)

                os.chdir(hyd_dir)
                if flag_gfx_control:
                    execute(os.path.join(kernel_dir, "whydram24.exe"), calc_dir, parent)
                else:
                    execute(os.path.join(kernel_dir, "whydram24_dos.exe"), calc_dir, parent)

                i=get_error("whydram24.err")
                logs=readfile("whydram24.log")
                log_info(logs, info_form)

                if i==0:
                    log_info("whydram24 ok", info_form)
                else:
                    log_info("whydram24 error: %i"%i, info_form)
                    with open("whydram24.log") as f:
                        content = f.readlines()
                        for line in content:
                            log_info(line, info_form)
                    return
            else:
                log_info("hydragen error: %i"%i, info_form)
                return

    if i==0 and info_form and not keep_opened:
        info_form.close()

    os.chdir(cur_dir)

if __name__ == "__main__":
    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "h",
                ["help"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    directory_calculation = args[0]
    run(directory_calculation, "moda","scn_1")

