# coding=UTF-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run all tests

USAGE

    python -m hydra.simulation_solo_test.py [-kh] [model]

    if model is specified, only export this model and keep it


OPTIONS

    -k, --keep
        project and models, do export only

"""
from __future__ import absolute_import # important to read the doc !
import sip
for cls in [u'QDate', u'QDateTime', u'QString', u'QTextStream', u'QTime', u'QUrl', u'QVariant']:
    sip.setapi(cls, 2)

from hydra.project import Project
from hydra.database.import_model import import_model_csv
from hydra.database.export_calcul import ExportCalcul
from hydra.utility.timer import Timer
from hydra.utility.log import SilentLogger, LogManager
from PyQt4.QtGui import QApplication
from PyQt4.QtCore import QCoreApplication, QTranslator
import os, sys, re
import getopt
import difflib
from hydra.gui.scenario_manager import ScenarioManager
from hydra.gui.rain_manager import RainManager
from hydra.database.database import create_project, project_exists, remove_project

data_dir = os.path.join(os.path.dirname(__file__), '..', 'database', 'test_data', 'light_projects')
hydra_path = os.path.join(os.path.expanduser("~"), ".hydra")
project_name = 'simulation_test'

models = [os.path.join(data_dir, f)
        for f in os.listdir(data_dir) if f.endswith(".csv")]

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

only = args if len(args) else None
keep = True if "-k" in optlist or "--keep" in optlist or only else False

# Init gui tools
if project_exists(project_name):
    remove_project(project_name)

test_project = create_project(hydra_path, project_name, "2154")
log_manager = LogManager(SilentLogger(), "Hydra")
project = Project.load_project(project_name, log_manager)
project.save()
app = QApplication(sys.argv)

model_to_calc = ["reso", "riv"]
scenarios={
    "reso":{
        "rainfall":"hyetograph",
        "comput_mode":"hydrology_and_hydraulics",
        "texts":{
            "tfin_hr":"12"
        }
    },
    "riv":{
        "rainfall":"hyetograph",
        "comput_mode":"hydrology_and_hydraulics",
        "texts":{
            "tfin_hr":"12"
        }
    }
}

def setup_scenario(model_name, id_rainfall):
    ui_scenario = ScenarioManager(project)
    ui_scenario.new_scenario()
    last_index = ui_scenario.table_scenario.table.rowCount()-1
    ui_scenario.table_scenario.table.setCurrentCell(last_index, 0)
    scn_name = "scn_" + model_name
    print "-- scenario name:"+scn_name
    ui_scenario.table_scenario.table.item(last_index,1).setText(scn_name)
    id_scenario = float(ui_scenario.active_scn_id)
    print "--- id: " + str(id_scenario)
    ui_scenario.comput_settings.set_comput_mode(scenarios[model_name]["comput_mode"])
    print "--- comput mode: " + scenarios[model_name]["comput_mode"]
    for parameter in scenarios[model_name]["texts"]:
        getattr(ui_scenario.comput_settings, parameter).setText(scenarios[model_name]["texts"][parameter])
        print "--- " + parameter + ": " + scenarios[model_name]["texts"][parameter]
    ui_scenario.hydrology_settings.selected_rainfall=id_rainfall
    ui_scenario.comput_settings.dtmin_hydrau_hr.setValue(1)
    ui_scenario.save()
    return ui_scenario

def rainfall():
    print "-- " + scenarios[model_name]["rainfall"]
    if scenarios[model_name]["rainfall"]=="hyetograph":
        return hyetograph_rainfall()
    elif scenarios[model_name]["rainfall"]=="caquot":
        return caquot_rainfall()
    else:
        return None

def caquot_rainfall():
    ui_rain = RainManager(project)
    ui_rain.new_montana()
    last_index = ui_rain.table_montana.table.rowCount() -1
    ui_rain.table_montana.table.selectRow(last_index)
    id_montana = ui_rain.table_montana.table.item(last_index,0).text()
    ui_rain.new_caquot()
    last_index = ui_rain.table_caquot.table.rowCount() -1
    ui_rain.table_caquot.table.selectRow(last_index)
    ui_rain.table_caquot.table.item(last_index, 2).setText(id_montana)
    id_rainfall = ui_rain.table_montana.table.item(last_index,0).text()
    ui_rain.save()
    return id_rainfall

def hyetograph_rainfall():
    ui_rain = RainManager(project)
    ui_rain.new_hyetograph()
    last_index = ui_rain.table_hyetograph.table.rowCount() -1
    ui_rain.table_hyetograph.table.selectRow(last_index)
    id_rainfall = ui_rain.table_hyetograph.table.item(last_index,0).text()
    ui_rain.set_hyetograph_table_items([[0.0, 0.0], [60.0, 40.0], [120.0, 0.0]])
    ui_rain.save()
    return id_rainfall

def compare_results(model_name, scenario_name, id_scenario):
    scenario_dir = project.get_senario_path(id_scenario)
    test_output_work = os.path.join(scenario_dir, "travail")
    test_ref_dir = os.path.join(data_dir, 'res_ref', scenario_name)
    ndiff=0
    for outfile in os.listdir(test_ref_dir):
        if outfile[-4:] == '.li2': continue
        test_output, ref_output =  os.path.join(test_output_work, outfile), os.path.join(test_ref_dir, outfile)
        if open(test_output).read().replace('\n','').replace('\nt','') != open(ref_output).read().replace('\n','').replace('\nt',''):
            ref = [line for line in open(ref_output).readlines() if not re.match(r'^(=+ +)+=+$', line)]
            gen = [line for line in open(test_output).readlines() if not re.match(r'^(=+ +)+=+$', line)]
            diff = difflib.unified_diff(ref, gen, fromfile=ref_output, tofile=test_output)
            nodiff = True
            for line in diff:
                #print line.rstrip(), re.match(r'(=+ +)+', line)
                nodiff &= False
            if not nodiff:
                sys.stderr.write("found diff {} {}\n".format(ref_output, test_output))
                exit(1)

for model in models:
    model_name = os.path.split(model)[1][:-4]
    if model_name in model_to_calc:
        if only and model_name not in only:
            continue

        if model_name in project.get_models():
            project.delete_model(model_name)
        project.add_new_model(model_name)
        project.set_current_model(model_name)
        print "Import model: " + model_name
        import_model_csv(model, project, project.get_current_model(), model_name)
        print "- Setting up rainfall:"
        id_rainfall = rainfall()
        print "- Setting up scenario:"
        scenario_manager = setup_scenario(model_name, id_rainfall)

        print "- Run calculation"
        if os.name == 'posix': #@todo : remove once the code runs on linux
            project.delete_model(model_name)
            project.commit()
            continue

        scenario_manager.run(True)
        print "-- calc " + model_name +  " OK"
        id, name = project.get_current_scenario()
        # compare_results(model_name, name, id)
            # TO DO: Better check the results files
        print "-- results OK"

        project.delete_model(model_name)
        project.commit()

if not keep:
    remove_project(project_name)
else:
    project.commit()
print 'ok'

