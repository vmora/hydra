|routing_line| Routage hydrologique (*Hydrology routing*)
#########################################################

Définition
----------

Cet élément permet de définir un décalage temporel entre un hydrogramme ruisselé calculé à l’exutoire d’un BV et le point d’injection dans le réseau.

.. |routing_line| image:: routing_line.png


Données de l'élément
--------------------

.. image:: routing.png
..

En général le décalage est nul, ce qui est reflété par les valeurs ci-dessus renseignées par défaut.


Commentaires
-------------

Cet objet est de type « link ». Il sert de lien entre un **bassin versant hydrologique** et le **réseau hydraulique**.

Son extrémité amont est obligatoirement connectée à un objet |catchment_node_point| :doc:`bassin versant <catchment_node_point>` (*catchment*).

Son extrémité aval doit être connectée à un des deux objets suivants :
   * un |hydrology_node_point| :doc:`regard <hydrology_node_point>` (*hydrology node*)
   * un containeur hydraulique qui peut être de type quelconque (`manhole`, `river node`, `station node`, `storage`, `element 2D`, `crossroad`) mais qui doit impérativement porter un objet `hydrograph` faute de quoi l’objet routage ne peut pas être créé.


   .. |catchment_node_point| image:: catchment_node_point.png
   .. |hydrology_node_point| image:: hydrology_node_point.png


   
