Noeud de gestion (*Station node*)
################################################

Définition
-----------

Regard particulier représentant une chambre ou un local technique.

Données de l'élément
-------------------------

.. image:: station_node_ui.png
   :width: 250px

La seule donnée attributaire est la surface au sol de la chambre schématisée par le nœud de gestion.

Commentaires
------------------

Le nœud de gestion ne peut être créé qu’à l’intérieur d’une station de gestion. 

Les nœuds de gestion sont connectés entre eux par des objets *link*.

Il n’existe pas de cote de débordement associée au nœud de gestion.


