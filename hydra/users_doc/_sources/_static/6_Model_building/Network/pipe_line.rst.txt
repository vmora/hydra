|NET_P| Tronçon de collecteur (*Pipe*)
########################################

.. |NET_P| image:: pipe_line.png

Définition
-----------

Tronçon de collecteur d’assainissement

Données de l'élément
---------------------

a) Données communes à tous les types de tronçon

.. image:: pipe.png
   :width: 430px

Les données attributaires sont :
  * la cote amont (*Upstream elevation*)
  * la cote aval (*Downtream elevation*)
  * la hauteur moyenne d'ensablement (*Silting height*) supposée constante au cours d’une simulation
  * la longueur du collecteur en m (*Length*)

b) Données spécifiques à chaque type de collecteur

- **Section circulaire** `circular`

.. image:: pipe_circ.png
   :width: 410px

- **Section ovoïde** `ovoïd`
 
.. image:: pipe_ov.png
   :width: 410px

- **Section paramétrique fermée** `pipe`

.. image:: pipe_param.png
   :width: 430px

    Les sections géométriques sont définies sous forme tabulée de couples de valeurs hauteur-largeur. Le nombre de points est limité à 20.

    Ces  sections  sont stockées dans une bibliothèque . 

    Une section peut être partagée par autant de tronçons qu’on le désire.

- **Section paramétrique ouverte** `channel`

.. image:: pipe_param.png
   :width: 430px

    Les sections géométriques sont définies sous forme tabulée de couples de valeurs hauteur-largeur. Le nombre de points est limité à 20.

    Ces  sections  sont stockées dans une bibliothèque . 

    Une section peut être partagée par autant de tronçons qu’on le désire.

    
Pour  les collecteurs fermés (*circular*, *ovoid* et *pipe*) le collecteur est prolongé par une fente de faible largeur. La cote d’eau calculée est alors assimilée à une cote piézométrique. Les débordements sont gérés par les équations des regards.

Pour les collecteurs ouverts (channel) le canal est prolongé par des parois verticales fictives prolongeant la dernière largeur de la section.

Commentaires
--------------

L’objet **pipe** est de type *link*, il est connecté à ses deux extrémités :
  * soit avec deux nœuds hydrologqies,
  * soit avec deux regards de collecteurs.

Cet objet est donc utilisé à la fois pour les **arborescences hydrologiques** et pour le **réseau hydraulique d’assainissement**. 

Les équations mises en œuvre dans  chaque domaine sont néanmoins très différentes :
  * dans le cas de l’**arborescence hydrologique**, les écoulements sont décrits par de simples lois de routage : les conditions de remous aval sont négligées et les mises en charge sont décrites localement. Les équations correspondantes sont décrites dans le manuel d’analyse : « Modélisation hydrologique ».
  * dans le cas du **réseau hydraulique d’assainissement**, les écoulements le long de chaque tronçon sont régis par les équations de St Venant intégrées sur la section du collecteur. Les équations mises en œuvre sont décrites dans le manuel d’analyse : « Modélisation hydraulique ».



