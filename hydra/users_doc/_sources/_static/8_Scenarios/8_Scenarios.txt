Paramétrage d'un scénario de calcul
###################################

Gestionnaire de scenario
========================
Le menu principal |I1| **scenarios/Settings**  permet de créer de nouveaux scénarios  et définit tous les paramètres  du scénario nécessaires au bon déroulement d’une simulation :

    .. |I1| image:: ..\0_Hydra_icone.png
       :height: 25px
       :width: 25px
       :scale: 100%
       :align: middle
       
    .. image:: _Scenario_settings_ui.png
       :scale: 100%

L'ajout et la suppression de scénarios se font via les bouttons + et -.

Le bouton *clone scénario* permet de créer un nouveau scénario en dupliquant tous les paramètres d’un scénario déjà créé.

Le gestionnaire de scénario comprend six onglets détaillés ci-après.

Menu **Computation settings** (*Paramètres de calcul*)
======================================================
    .. image:: _Computation_settings_ui.png
       :scale: 100%

Les valeurs par défaut dont indiquées ci-dessus. Les différentes rubriques sont  définies comme suit :

____

**Computation mode**

Par défaut une simulation enchaîne les deux étapes de calcul hydrologique et de calcul hydraulique (option *Hydrologic and hydraulic*). Deux options complémentaires sont disponibles :
 
- Le **calcul hydrologique** (*Hydrology*) mobilise uniquement le domaine hydrologique d’un modèle et se conclue par le calcul des hydrogrammes d’apport dans le réseau hydraulique au niveau de chaque objet *hydrograph*. Il intègre donc le calcul des apports de temps sec associés aux secteurs d’apports.
- Le **calcul hydraulique** (*Hydraulic*) mobilise uniquement le réseau hydraulique ; les hydrogrammes d’injection au niveau de chaque objet *hydrograph*  sont lus dans un scénario antérieur qui a déjà tourné. Ce scénario est déclaré dans le sous menu *hydrology  settings*.

____

**Computation  starting date**

Date calendaire de démarrage effectif des calcul au temps relatif t=0. En réalité le calcul en régime transitoire démarre à un temps relatif négatif suffisant pour atteindre les conditions de régime établi au temps t=0.

____

**Computation duration**

Durée de la simulation :math:`t_{fin}` en **heures**, à compter du temps t=0.

____

**Hydrologic computation time step**

Pas de temps de calcul  dth dans l’étape de  calcul hydrologique. Le pas de temps est exprimé en **minutes**.

Il doit être réglé pour satisfaire la condition :math:`60*t_{fin}/dth<5000`

____

**Hydraulic computation time step**

Les 2 valeurs min et max contrôlent  la fourchette de variation du pas de temps de calcul. Le pas de temps min de 0.005h n’a en pratique pas besoin d’être modifié. Le pas de temps max doit en revanche être ajusté à la nature de la simulation :

- pour les simulations de **réseau d’assainissement** ou les **études courantologiques locales**, il conviendra de se limiter à 0.1h au maximum, la valeur plus faible de 0.02 heures étant recommandée pour minimiser les risques de diffusion numérique dans le cas de réseaux à fortes pentes.
- Pour les simulations **fluviales** de propagation de crues le pas de temps maximum peut être porté à 0.5h ou plus.

____

**Water surface variation between two time steps**
 
Ces paramètres règlent l’algorithme de pas de temps adaptatif appliqué par le programme de calcul :

- min : variation de hauteur d'eau minimale sur l'ensemble des noeuds de calcul permettant d'augmenter le pas de temps de calcul,
- max : variation de hauteur d'eau maximale sur l'ensemble des noeuds de calcul entrainant une réduction du pas de temps de calcul.

En pratique, les valeurs  par défaut proposées sont  correctement  ajustées et conviennent pour la quasi-totalité des simulations. Elles ne doivent donc pas être modifiées, saut cas très exceptionnel.

____

**Output time for hot start scenario**

Cette option permet de stocker l’état hydraulique d’un système à un instant  donné, pour une reprise des calculs à cet instant par un scénario ultérieur (*Initial conditions settings/Hot start scenario*).

Cette option est très utile dans le cas de simulation de longue durée, elle permet de scinder un calcul en plusieurs scénarios successifs tout en assurant une continuité parfaite des calculs entre 2 scénarios successifs.

____

**Initial conditions settings**

Deux options mutuellement exclusives :

- dans l’option **start from rest**, le démarrage d’un calcul se fait à froid, avec des conditions de débit nul dans toutes les branches et à travers toutes les liaisons. Il faut alors définir un temps tini (*Duration of initial conditions settings simulation (h)*), nécessaire à l’établissement du régime établi. Ce temps est calé sur le temps de propagation le long du système hydraulique modélisé.
- Si l’option **hot start scénario** est  activée, il faut préciser le scénario pour lequel une sauvegarde a été faite et indiquer le temps relatif de reprise dans le repère de temps du nouveau scénario.

____

**Computation output time interval**

Définition des **pas de temps de stockage des résultats** (*Output time step (h)*)  pour tous les types de sorties et de l’intervalle de temps pendant lequel les résultats sont stockés. 

Par défaut tmin = 0. et tmax= durée de la simulation.

____

**Graphic control during computation**

Cette option permet de contrôler graphiquement la bonne exécution des calculs en visualisant l’évolution temporelle des lignes et des débits le long des branches filaires.

Cet outil est intéressant en phase de réglage du modèle; il permet notamment de mettre en évidence certaines anomalies de cotes de fond ou de cotes de débordement, ou d’identifier les zones responsables d’un ralentissement des calculs et nécessitant généralement des corrections locales à apporter au modèle.


Menu **Hydrology settings** (*Paramètres hydrologiques*)
========================================================

    .. image:: _Hydrology_settings_ui.png
       :scale: 75%

____

**Rainfall scenario**

Sélection du scénario de pluie dans la bibliothèque définie dans le menu principal |I1| **hydrology/Rainfall**. 

Par défaut, aucun scénario de pluie n'est défini.
       
____

**Dry inflow scénario**

Sélection du scénario d’apport de temps sec dans la bibliothèque définie dans le menu principal |I1| **hydrology/dry flow scenario**. 

Par défaut, aucun scénario de temps sec n'est défini.

____

**Hydrographs from other scenario**

Scénario contenant les hydrogrammes d’apport utilisés lorsque l’option *hydraulic computation mode* est sélectionnée.

Cette option permet de s'affranchir des calculs hydrologiques (modèle pluie débit de grande ampleur par exemple) pour simuler des scénarios d'aménagement n'intégrant que des modifications structurelles du modèle via des configurations ou des modes de régulations distincts reprenant les entrants hydrologiques d'un scénario de référence.

____

**Hydrologic initial conditions**


Permet de 'forcer' les conditions initiales de sol; différentes options sont disponibles :

- *Soil moisture coefficient* : définit un coefficient initial d’humidité des sols en début de simulation. Ce paramètre est utilisé pour les lois de production de ruissellement associées aux lois de type SCS et Holtan.
- *Runoff coefficient variation* : défini une modulation du coefficient de ruissellement pour les lois de production avec coefficient de ruissellement constant.
- *hot start scenario* : les conditions initiales de sols sont lues dans un fichier généré en fin de simulation d'un scénario ayant déjà tourné. Cette option est utile dans les applications suivantes :
    - chroniques longues scindées en plusieurs scénarios,
    - prise en compte d'une période préparatoire sur une longue durée permettant de prendre en compte l'état de saturation des sols à l'arrivée d'un événement pluviométrique intense, ne nécessitant que des calculs hydrologiques.

____

**External file hydrographs**

Liste des fichiers d’hydrogrammes externes lus par le programme de calcul pour les objets *hydrographs* avec positionnement à *yes* de l’option *external file data*. Ces fichiers peuvent également contenir des courbes de marée et des courbes de vent.

Menu **Model ordering** (*pilotage des modèles*)
================================================

    .. image:: _Model_ordering_ui.png
       :scale: 75%

Ce menu concerne un projet composé de plusieurs modèles. Hydra propose **trois modes de pilotage** des modèles en phase calcul :

- **1.	Mode cascade :**
    Les modèles sont exécutés en séquence les uns après les autres, dans l’ordre affiché dans le tableau de gauche. On définit dans ce mode autant  de groupes que de modèles, avec un modèle par groupe.
- **2.	Mode global :**
    Les modèles sont assemblés dans un fichier de calcul unique, ce qui permet de calculer exactement les interactions aval-amont entre les modèles : tout se passe  pour les calculs comme si le projet était composé d’un seul modèle.
    
    On définit dans ce mode un seul groupe, tous les modèles sont déclarés dans ce groupe; l’ordre de déclaration des modèles dans le groupe est arbitraire.
    
- **3.	Mode mixte :**
    Les deux précédents modes sont panachés. On peut très bien dans ce mode ne simuler qu’une partie des modèles et les assembler comme on le souhaite par groupe, à condition bien sûr de respecter des règles topologique de cohérence amont-aval entre les groupes.

    
Menu **Regulation and configurations** (*régulation et configurations*)
=======================================================================
Régulation
**********

Hydra dispose d’un module de pilotage d’un certain nombre d’objets de modélisation hydraulique via un langage de programmation formelle. Les instructions sont définies dans un fichier ASCII, déclaré comme un fichier externe. Les règles détaillées de pilotage et la syntaxe associée sont décrites dans le document PDF intitulé : *fichiers de contrôle*.

Ce module complète et étend considérablement la gamme des fonctionnalités de régulation locale intégrées aux actionneurs mobiles.
On peut déclarer plusieurs fichiers de contrôle dans un même scénario.

Configurations
**************


Menu **Computation and output options** (*options de calcul et de sorties*)
===========================================================================

____

Option de calcul **affin**

Par défaut les calculs sont basés sur une résolution simplifiée  des équations de Barré de St Venant, c’est-à-dire ne tenant pas compte des termes convectifs dans les équations de bilan de quantité de mouvement. Cette simplification permet de limiter les temps de calculs, elle est parfaitement justifiée pour de nombreuses classes d’application. 

Elle s’avère néanmoins erronée dans quelques cas, tels que la courantologie 2D, les ondes de rupture de barrage ou les écoulements torrentiels impliquant des ressauts hydrauliques. Il est alors nécessaire d’affiner la formulation en activant l’option « affin » dans les domaines 2D ou les branches filaires qui le nécessitent. Le paramétrage de cette option est réalisé via l’écran de saisie suivant :

    .. image:: _Affin_option_ui.png
       :scale: 75%
       
Cet écran permet de déclarer les branches, les bief1D ou les domaine 2D que l’on souhaite traiter en mode *Affin*.

Il faut également définir le temps à partir duquel le calcul affin est déclenché. On peut également spécifier une surface minimum à affecter aux pavés pour éviter des pas de temps de calculs trop faibles.

____
       
Option **Strickler coefficient adjustment** (*ajustement du coefficient de Strickler*)

Cette option est généralement utilisée dans la phase de calage des lignes d’eau d’un modèle. Elle permet de modifier les coefficients de frottement dans un domaine 2D ou le long d’un bief de rivière sans devoir rééditer les données de chaque objet. Ces données sont lues dans la phase de calculs et se substituent aux coefficients définis dans la base de données. On peut ainsi tester plusieurs réglages afin de converger vers un calage satisfaisant sans affecter les paramètres de référence du modèle.

Le paramétrage de cette option est réalisé via l’écran de saisie suivant :

    .. image:: _Strickler_option_ui.png
       :scale: 75%

Cet écran permet de déclarer les **biefs 1D** (*Reach* de rivière) ou les **domaines 2D** sur lesquels on souhaite modifier les coefficients de frottement. Les paramètres à définir dépendent du sous domaine :

- Sous domaine 2D : le coefficient de  Strickler Kmin  s’applique à toutes les mailles du domaine.
- Reach : on définit les coefficients de strickler Kmin et Kmaj des lits mineurs et majeurs. Si les champs PK1 et PK2 ne sont pas renseignées ces coefficients s’appliquent à la totalité du bief. Dans le cas contraire ils s’appliquent au tronçon compris entre les pk PK1 et PK2.

____
       
**Additionnal output : z(t) et q(t)** (Options de sortie complémentaires : *z(t) et q(t)*)

Cette option permet de générer automatiquement en sortie de simulation des fichiers d’hydrogrammes et de limnigrammes en des points particuliers d’un modèle. Ces fichiers sont générés au format .csv.

Le paramétrage de cette option est réalisé via l’écran de saisie suivant :

    .. image:: _Zt_Qt_output_option_ui.png
       :scale: 75%
    
Une sorite de courbe peut être demandée au niveau d’une singularité, d’une condition à la limite ou d’un link.

Dans le cas du link ou de la singularité  la cote d’eau stockée est la cote au nœud amont.

____

Options diverses via un **fichier externe**

Les trois options ci-dessus ont été isolées car elles sont utilisées relativement fréquemment. D’autres options sont disponibles et peuvent être définies dans le fichier déclaré dans la fenêtre suivante :

    .. image:: _Additionnal_output_options_ui.png
       :scale: 75%

Une option déclarée dans ce ficher se compose d’un première ligne avec un mot clé précédé du signe « * ». les lignes suivantes précisent les paramètres attachés à l’option.
Dans ce fichier deux blocs d’options consécutives doivent être séparés par au moins une ligne blanche.

Le tableau ci-dessous récapitule les options reconnues par le programme de calcul. Cette liste n’est pas exhaustive et pourra être complétée en fonction des futurs besoins ressentis.

.. list-table::  
   :widths: 10 10 10
   :header-rows: 1 
   :stub-columns: 1 

   * - Mot clé
     - Définition
     - Paramètres de l’option

   * - 
     - 
     -

   * - 
     - 
     -     

Menu **Transport** (*transport solide et qualité*)
==================================================
Le transport de substances peut être simulé dans Hydra via 5 options mutuellement exclusives :

    .. image:: _Transport_options_ui.png
       :scale: 75%

- Option **Pollution** : Pollution generated and network transport (*génération de la pollution et transport dans les réseaux d'assainissement*)
- Option **Quality 1** : Physico chemical, 4 inter related parameters DBO, NH4, NO3, O2 (*Physico chimie, 4 paramètres interragissant entre eux DBO, NH4, NO3, O2*)
- Option **Quality 2** : Bacteriological, no chemical intercation between parameters (*Bactériologie, pas d'interraction entre les paramètres*)
- Option **Quality 3** : Suspended sediment transport (*transport de matières en suspension, MES*)
- Option **Hydro morphology** : Bedload (*transport sédimentaire*)
____

Option **Pollution**

Cette option permet de simuler la génération de pollution par ruissellement et sa propagation dans un réseau d’assainissement. 4 paramètres peuvent être simulés :

    .. image:: _Pollution_option_ui.png
       :scale: 75%

Ces paramètres n’interagissent pas entre eux.

Les stocks de pollution entrainés dans les réseaux par ruissellement sont définis dans la table *land pollution accumulation* du menu principal |I1| **scenarios/hydrology**.

____

Option qualité 1 : **physico chemical**

Cette option permet de simuler l’évolution de la qualité physico-chimique d’un milieu récepteur : lac ou cours d’eau à l’aide de 4 paramètres : O2, DBO, HH4 et NO3. Ces paramètres sont liés par des réactions d’échanges chimiques. Les constantes de réactions sont défies par défaut dans la grille de saisie suivante :

    .. image:: _Physico-chemical_option_ui.png
       :scale: 75%

On peut également simuler les effets de diffusion  turbulente et de dispersion qui se superposent aux phénomènes convectifs de transport et ont pour effet d’étirer le nuage de pollution.

____

Option quality 2 : **bactériological**

Cette option permet de simuler l’évolution de la qualité bactériologique d’un milieu récepteur : lac ou cours d’eau à l’aide de 4 paramètres  de contamination fécale dont les Eschéricia Ecoli et les Entéroques intestinaux :

    .. image:: _Bacteriological_option_ui.png
       :scale: 75%

A chaque paramètre on associe un taux de mortalité en :math:`jour^{-1}`.

Les paramètres n’interagissent pas entre eux.

On peut prendre en compte  les effets de dispersion comme pour l’option *quality 1*.  

____

Option quality 3 : **Suspended sediment transport**

Le transport de matières en suspension peut être simulé dans Hydra via 5 options mutuellement exclusives :

    .. image:: _Suspended_sediment_transport_ui.png
       :scale: 75%

Ce module a été développé pour analyser le devenir  des rejets polluants chargés de MES dans un milieu récepteur , afin de dresser la carte des matériaux polluants déposés après un événement pluvieux. 
       
____

Option **Hydro morphology** (*transport sédimentaire*)

Cette option active le module de calcul de transport hydro sédimentaire par charriage. Les paramètres de transport sont définis dans le fichier déclaré dans la fenêtre de saisie :

    .. image:: _Bedload_transport_ui.png
       :scale: 75%

Le format des données est détaillé dans le document PDF intitulé : *Données pour les calculs de transport hydro-sédimentaires*.

