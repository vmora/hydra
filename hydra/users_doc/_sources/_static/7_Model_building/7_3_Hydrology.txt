Hydrologie
==========
Introduction
------------
Dans le cas des réseaux des grandes agglomérations ou dans les secteurs amont des vallées alluviales, il est souvent inutile de modéliser finement la propagation des hydrogrammes des réseaux amont à l‘aide de lois de type Barré de Saint Venant. Des logiciels de calculs simplifiés, basés sur la méthode de l’onde diffusive, suffisent généralement. 

D’où l’intérêt, pour favoriser la rapidité des calculs et faciliter l’exploitation de grands modèles, de limiter la modélisation hydraulique aux secteurs aval, caractérisés généralement par de faibles pentes, de nombreuses singularités et d’effets aval, et d’alimenter ces réseaux par des modèles hydrologiques simplifiés.

Le **domaine hydrologique** répond à ces besoins, il assure une double fonction de:
    - génération d’hydrogrammes de ruissellement à partir de données pluviométriques,
    - propagation de ces hydrogrammes  le long d’un réseau secondaire composé de tronçons de routage et de singularités de type dérivation ou bassins.

Un sous réseau hydrologique génère des hydrogrammes à partir de la pluie, les propage à l’aide de lois de routage simplifiées, et modélise des ouvrages particuliers tels que des dérivations ou des bassins. Les hydrogrammes calculés aux exutoires aval d’un réseau hydrologique sont automatiquement injectés dans le réseau hydraulique ou rejetés vers l’extérieur.

Lors d’une simulation les calculs hydrologiques peuvent être exécutés indépendamment de la simulation hydraulique : les résultats de cette modélisation peuvent être donc exploités sans devoir exécuter la simulation  des sous modèles hydrauliques. 

Sur un plan algorithmique les calculs des réseaux hydrologiques  sont exécutés suivant une table d’opérations en procédant de l’amont vers l’aval dans chaque sous réseau. A chaque étape de calcul  un hydrogramme attaché à un objet de modélisation est calculé. Il résulte de cette procédure que les calculs hydrologiques sont très rapides mais la validité des résultats est conditionnée par l’absence d’influence hydraulique aval.

Schématisation d’un sous réseau hydrologique
--------------------------------------------
La modélisation d’un sous réseau hydrologique met un œuvre un ensemble d’objets de modélisation hydrologique, distinct des objets de modélisation hydraulique, pour  simuler :
    - Les apports de ruissellement à partir de la pluie,
    - Les apports de temps sec,
    - Les apports définis par des hydrogrammes externes,
    - Les routages le long de tronçons de collecteurs,
    - Les dérivations,
    - Les stockages dans les bassins.  

Le domaine hydrologique se compose d’arborescences  distinctes. Une arborescence se compose en amont de  nœuds de bassin versants (catchment) , connectés chacun à un regard( hydrology node) par l’intermédiaire d’un élément de routage (hydrology routing).

.. image:: Hydrology/_Hydrologic_domain.png
   :width: 550px
   :align: center

Les **objets de modélisation** associés au domaine hydrologique sont les suivants :    

    .. image:: ../6_Model_building/Hydrology/_Hydrology_button_ui.png
    ..


    
Le |HY_CC| `catchment contour` est une entité polygonale servant à **délimiter le contour du bassin versant**. Cet objet n’a pas d’attribut particulier et sert  au calcul des lames d’eaux moyennes précipitées sur chaque bassin versant (noeud de type `catchment`) en cas de pluie spatialement hétérogène.

Le |HY_C| :doc:`Catchment <Hydrology/catchment_node_point>` permet la **transformation pluie - débit**. Il correspond à un noeud auquel sont attachées les caractéristiques géométriques, les lois de production de pluie nette et de ruissellement et les paramètres associés.

Le **routage simplifié** des apports hydrologiques se fait via un réseau maillé pouvant contenir les éléments suivants :

-  des |HY_N| :doc:`regards <Hydrology/hydrology_node_point>`
-  des **liaisons binodales**, connectées à chaque extrémité à un regard :
    - |HY_P| :doc:`connecteurs <Hydrology/routing_line>`, permettant de propager un hydrogramme entre deux noeuds sans déphasage et sans déformation
    - **liaisons de dérivation**, posées sur des nœuds hydrologiques; elles doivent obligatoirement figurer à tous les points comportant des diffluences 
        - |HY_SQQ| :doc:`split QQ <Hydrology/qq_split_hydrology_point>`
        - |HY_SZQ| :doc:`split ZQ <Hydrology/zq_split_hydrology_point>`
    - des **colllecteurs**; ce sont les mêmes objets que ceux du réseaux hydraulique d’assainissement : |NET_P| :doc:`Pipe <Network/pipe_line>`
- des |HY_RS| :doc:`bassins de stockage <Hydrology/reservoir_rs_point>` et des |HY_RSP| :doc:`bassins de stockage paramétriques <Hydrology/reservoir_rsp_point>`
- des |HY_BC| :doc:`conditions limites <Hydrology/hydrology_bc_point>`
    
Les **exutoires aval** d’un sous réseau hydrologique sont obligatoirement matérialisés par un des objets suivants :

- Une **condition limite aval** (|HY_BC| :doc:`hydrology BC <Hydrology/hydrology_bc_point>`) qui doit être posé sur le regard aval : l’hydrogramme aval sort du réseau modélisé, dans le cas par exemple d’un rejet dans le milieu récepteur,
- Un élément de **routage hydrologique** (|HY_R| :doc:`hydrology routing <Hydrology/routing_line>`) permettant de **raccorder un réseau hydrologique à un réseau hydraulique**. Le nœud hydraulique peut être de type quelconque (`manhole`, `river node`, `station node`, `storage`, `element 2D`, `crossroad`) mais **doit porter un objet `hydrograph`**.


    .. |HY_C| image:: ../6_Model_building/Hydrology/catchment_node_point.png
    .. |HY_CC| image:: ../6_Model_building/Hydrology/catchment_contour_polygon.png
    .. |HY_N| image:: ../6_Model_building/Hydrology/hydrology_node_point.png
    .. |HY_R| image:: ../6_Model_building/Hydrology/routing_line.png
    .. |HY_SQQ| image:: ../6_Model_building/Hydrology/qq_split_hydrology_point.png
    .. |HY_SZQ| image:: ../6_Model_building/Hydrology/zq_split_hydrology_point.png
    .. |HY_P| image:: ../6_Model_building/Hydrology/connector_hydrology_line.png
    .. |HY_RS| image:: ../6_Model_building/Hydrology/reservoir_rs_point.png
    .. |HY_RSP| image:: ../6_Model_building/Hydrology/reservoir_rsp_point.png
    .. |HY_BC| image:: ../6_Model_building/Hydrology/hydrology_bc_point.png
    .. |NET_P| image:: ../6_Model_building/Network/pipe_line.png

Règles d'agencement des objets
------------------------------
Le schéma ci-après présente des exemples licites d’agencement d’arborescences hydrologiques

.. image:: Hydrology/_Hydrology_connecting_rules.png
   :Width: 600px

Principe de calcul
---------------------

L’arborescence hydrologique constitue un domaine à part : les calculs sont en effets réalisés dans une étape préliminaire selon une logique amont-aval : les hydrogrammes sont calculés de proche en proche en descendant le long de l’arborescence. 

Les hydrogrammes obtenus aux exutoires aval qui sont connectés au réseau hydraulique sont stockés temporairement. 

Ils sont ensuite utilisés comme input hydrologiques dans la phase suivante de calcul hydraulique qui met en œuvre tous les autres domaines.


Résultats fournis par un calcul hydrologique
---------------------------------------------

Les résultats de synthèse sont stockés dans un fichier au format .csv. Ce dernier fournit des bilans à chaque point de calcul.

Une option **dimensionnement** permet de calculer automatiquement les diamètre équivalents de collecteurs , ainsi que les volumes de bassins permettant de faire transiter une pluie donnée sans mise en charge et sans débordement. 

Cette option peut s’avérer très utile pour dimensionner un réseau pluvial de ZAC par exemple.
