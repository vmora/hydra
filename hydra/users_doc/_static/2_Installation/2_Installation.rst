Procédure d'installation et de mise à jour
##########################################

Le chapitre qui suit vise à décrire pas-à-pas la procédure d’installation sur un poste du plugin **hydra**. Cette procédure fait référence aux fichiers fournis dans le **package d'installation** que vous êtes invité à télécharger sur le site `hydra-software.net <http://hydra-software.net>`_ après avoir choisi le type de licence souhaité (STARTER, PRO LOCAL, PRO NETWORK).

Le plugin **hydra** s’ajoute à une installation existante de QGIS et s’appuie sur une base de données relationnelle (hébergée sous PostgreSQL). La procédure complète se déroule en 3 étapes :

ETAPE 1 : Installation de **QGIS**

ETAPE 2 : Installation de **PostGre** et de son extension spatiale **PostGis** via **Stackbuilder**

ETAPE 3 : Paramétrage et installation d'hydra et des différents composants nécessaires à son fonctionnement :

    - installation de la bibliothèque **GDAL**
    - installation du programme **GMSH**
    - Création d'un utilisateur PostGreSQL
    - Mise à jour des variables d'environnement
    - Chargement du plugin hydra dans QGIS

L'étape 3 est réalisée automatiquement par le setup fourni dans le zip d'installation. 

.. warning:: Afin que le setup fonctionne correctement, QGIS et PostGre doivent être installés dans leurs répertoires par défaut. Si ce n'est pas le cas, vous devrez réaliser une installation manuelle en suivant la :doc:`procédure d'installation avancée <../Installation_manuelle/Installation_manuelle>`. 

Une fois l'installation d'hydra finalisée, il vous faudra activer votre clé de licence : cette procédure est décrite au chapitre 2.3.

.. warning:: L'installation ne peut se faire que sur un poste **64 bits**.

.. warning:: La procédure d’installation est à exécuter **une seule fois**. La mise à jour ultérieure de l’application passe uniquement par la **mise à jour du plugin HYDRA** dans l’interface de QGIS.

Première installation
=====================

Installation de QGIS 
-----------------------

Installez **QGIS** sur votre poste : `https://www.qgis.org/fr/site/forusers/download.html <https://www.qgis.org/fr/site/forusers/download.html>`_ ou utilisez une version existante (Les versions 2.14 « Long term release repository » jusqu'à 2.18.12 sont supportées).


Installation de PostGreSQL et de son extension spatiale PostGis
---------------------------------------------------------------

**PostGreSQL**

Installez **PostgreSQL 9.6** : `https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows <https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows>`_
    
- A la demande du mot de passe pour le « superuser PostGre », notez le mot de passe fourni.
- Les autres paramètres sont à laisser par défaut.

.. warning:: Attention au port d’installation si plusieurs versions cohabitent sur le poste.

____

**StackBuilder**

A l’issue de l’installation de PostgreSQL, exécutez **StackBuilder** (installateur de compléments à PostgreSQL) :

.. image:: instal_PostGre_1.png
   :width: 380px
   :align:   center

Paramétrez l’installation des composants supplémentaires pour l’installation de PostgreSQL effectuée précédemment.

.. image:: instal_PostGre_2.png
   :width: 380px
   :align:   center

Lors du choix des applications à installer, sélectionnez :

- EDB Language Pack (Add-ons, tools and utilities)
- Postgis (Spatial extensions)

.. image:: instal_PostGre_3.png
   :width: 380px
   :align:   center

Laissez les paramètres par défaut pour l’installation de ces deux extensions.

Installation d'HYDRA 
-----------------------

Exécutez le fichier **setup_hydra.exe** qui permet l’installation du plugin hydra dans QGIS.

Après avoir sélectionné la langue et accepté la licence, le premier écran permet de configurer la connexion au serveur PostgreSQL installé en 3.2. Précisez le port d’installation (par défaut 5432) et le mot de passe choisi pour le « superuser postgres ».

.. image:: config_postgre_1.png
   :width: 400px
   :align:   center

Sur l’écran suivant, choisissez un mot de passe pour l’utilisateur local du serveur PostgreSQL qui sera créé (Cette page ne s’affichera pas si l’utilisateur est déjà existant).
   
.. image:: config_postgre_2.png
   :width: 400px
   :align:   center

Enfin, laissez cochée par défaut la case « Installer le pilote de protection HASP », qui installe un composant nécessaire lors de l’activation de votre clé de produit.

.. image:: config_postgre_3.png
   :width: 400px
   :align:   center

Validez et laissez le script s'exécuter.

Redémarrez le PC, puis ouvrez QGIS. Activez le plugin hydra via QGIS et le menu extensions > installer/gérer les extensions 

    .. image:: instal_hydra2.png
       :width: 500px
       :align:   center
   

Debugage - Problème d'installation 
-----------------------------------

En cas d’erreur, l’intégralité des composants utilisés ainsi que les logs de l’installation sont localisés dans ``C:\Program Files\Hydra``. Les logs (fichiers txt de ce répertoire) seront demandés pour toute sollicitation d’assistance.
 
Mise à jour du plugin
=====================

Procédure de mise à jour
-----------------------------

Pour mettre à jour hydra :

- Fermez QGIS,

- Supprimez le répertoire ``C:\Users\name\.qgis2\python\plugins\hydra``

- Dézippez le fichier **hydra.zip** dans ``C:\Users\name\.qgis2\python\plugins``,

Le plugin sera mis à jour à l'ouverture de QGIS.

Compatibilité projet / version plugin
-------------------------------------

Lors du travail avec le plugin HYDRA, la gestion des versions est effectuée à deux niveaux : on distingue la version du **plugin HYDRA**, qui trace la version du code de l’interface, et le **numéro de version du projet**, qui trace le format dans lequel se trouve la base de données liée au projet.

Versions du plugin
******************

Le numéro de version du plugin est accessible via le gestionnaire d’extension de QGIS. 

Que le plugin soit installé à partir d’un dépôt en ligne ou via une archive .zip, le **numéro de version** est écrit **en tête de la page de présentation du plugin** :

.. image:: version_hydra.png
   :width: 500px
   :align:   center

A chaque itération sur le code du plugin HYDRA, ce numéro de version est incrémenté de façon à refléter l’importance des évolutions apportées.

A chaque nouvelle version, un *changelog* résume également les principaux changements : nouvelles fonctionnalités et corrections de bugs.

Versions du projet
******************

Une mention est également présente dans le *changelog* en cas d’évolution de la structure du modèle de données (la base PostgreSQL) : *"Database version increased to … "*.

Ainsi, chaque version du plugin est conçue pour fonctionner avec des projets d’une structure précise. 

Le **numéro de version de la structure de données** (des projets) est mentionné en tête du **Gestionnaire de Projets** (Menu *Manage projects*) :


Activation et gestion de votre licence
==========================================

Un email contenant votre clé de licence vous est envoyé sous 24 à 48h ouvrées après votre inscription sur le site. 

Activation de la clé
----------------------
L’activation de la clé de licence se fait **après installation d’hydra**  via l'`interface de gestion de clé <http://licensing.hydra-software.setec.fr:8080/ems/customerLogin.html>`_ 

Deux méthodes sont possibles :
 
- **ONLINE ACTIVATION** (activation en ligne) :  cette méthode est à utiliser si vous êtes sur l'ordinateur où est installé hydra, et que vous êtes connecté à Internet.

- **OFFLINE ACTIVATION** (activation manuelle / hors ligne) : cette méthode est à utiliser si vous souhaitez générer la licence depuis un autre ordinateur que celui où est installé hydra. Dans ce cas, l'ordinateur qui génère la licence doit être connecté à Internet mais celui qui a hydra n'a pas besoin de l'être.

Online activation
********************

Rendez-vous sur l'interface de gestion de clé accessible à l'adresse suivante :

`http://licensing.hydra-software.setec.fr:8080/ems/customerLogin.html <http://licensing.hydra-software.setec.fr:8080/ems/customerLogin.html>`_

    .. image:: interface_client_EMS.png
       :width: 400px
       :align:   center

Entrez le numéro de clé de licence qui vous a été communiqué par mail et cliquez sur **Login**

Vous obtenez alors l'écran suivant :

    .. image:: ecran_EMS.png
       :width: 650px
       :align:   center

Cliquez sur **Online Activation**. 

Un message s'affiche alors pour vous avertir que la clé a bien été activée. Félicitations, vous êtes prêt à utiliser **hydra** !

.. warning:: si l'activation en ligne n'a pas fonctionné :

    - Fermez l' `interface de gestion de clé <http://licensing.hydra-software.setec.fr:8080/ems/customerLogin.html>`_
    - Rendez-vous sur le `Sentinel Admin Control Center <http://localhost:1947/>`_. 
    - Dans le menu de gauche, sélectionnez **Configuration**. Allez sur le dernier onglet "Network" 
    - Vérifiez les adresses URL qui se trouvent dans "EMS URL". Vous devez y voir l'adresse suivante : http://195.81.235.43:8080 . Si ce n'est pas le cas, ajoutez-la.

      .. image:: config_EMS.png
         :width: 700px
         :align:   center 

    - Ouvrez à nouveau l'`interface de gestion de clé <http://licensing.hydra-software.setec.fr:8080/ems/customerLogin.html>`_ et réessayez de faire l'activation en ligne.

.. warning:: si l'activation en ligne n'a pas fonctionné malgré la manipulation précédente, faites une **activation offline**.

Offline activation
********************

.. note:: Pour l'activation hors ligne, l'ordinateur qui génère la licence doit être connecté à Internet mais celui qui accueillera hydra n'a pas besoin de l'être.

.. warning:: Dans le cas d'une activation offline, pensez à bien prendre l'empreinte de la machine sur laquelle sera installé hydra.

**Sur l'ordinateur où est installé hydra :**

- Executez **RUS_DDYTE.exe**

    .. image:: RUS_2.png
       :width: 380px
       :align:   center

- Choisissez "Installation of new protection key" et cliquez sur **Collect Information** (en bas à gauche de la fenêtre) pour générer un fichier c2v. Ce fichier contient l'empreinte de la machine.

**Sur l'ordinateur connecté à Internet :**

- Rendez-vous sur l'interface de gestion de clé accessible à l'adresse suivante :

  `http://licensing.hydra-software.setec.fr:8080/ems/customerLogin.html <http://licensing.hydra-software.setec.fr:8080/ems/customerLogin.html>`_

    .. image:: interface_client_EMS.png
       :width: 400px
       :align:   center

- Entrez le numéro de clé de licence qui vous a été communiqué par mail et cliquez sur **Login**

- Vous obtenez alors l'écran suivant :

    .. image:: ecran_EMS.png
       :width: 650px
       :align:   center

- Cliquez sur **Offline Activation**

- Dans la fenêtre **Upload C2V**, téléchargez le fichier c2v que vous avez généré sur la machine où est installé hydra.  

    .. image:: RUS_3.png
       :width: 450px
       :align:   center

- Cliquez sur le bouton **Generate**

- Téléchargez le fichier V2C :

    .. image:: RUS_4.png
       :width: 600px
       :align:   center

**Sur l'ordinateur où est installé hydra :**

- Exécuez à nouveau **RUS_DDYTE.exe**

- Sélectionnez l'onglet **Apply License File**

- Dans "Update File", téléchargez le fichier V2C que vous avez générée sur l'interface de gestion de clé.

    .. image:: RUS_5.png
       :width: 450px
       :align:   center

- Cliquez sur **Apply update**. Un message s'affiche alors pour vous avertir que la clé a bien été activée. Félicitations, vous êtes prêt à utiliser **hydra** !

Contrôle de la clé
----------------------

Afin de vérifier que votre clé est bien activée, vous pouvez vous rendre sur le `Sentinel Admin Control Center <http://localhost:1947/>`_.

Dans le menu de gaucge, sélectionnez "Sentinel keys". Vérifiez alors que votre clé apparait bien dans la liste à droite. 


Transfert de la license
-------------------------

Vous pouvez utiliser transférer une license d'un ordinateur (ordinateur "source") à un autre (ordinateur "receveur"). 

Cette procédure se déroule en 3 étapes et nécessite :

- Une connexion Internet
- RTE installé sur les deux ordinateurs (cf. 2.3.1)
- RUS_DDYTE.exe sur les deux ordinateurs

**1. Collecte des informations sur l'ordinateur "receveur" :** 

- Exécutez RUS_DDYTE.exe sur l'ordinateur "receveur", ,

- Sélectionnez l'onglet **Transfer License**

- Suivez les instructions décrites dans "Step 1" pour collecter les informations de l'ordinateur receveur et enregistrez les dans un fichier. Assurez-vous que ce fichier est accessible depuis l'ordinateur "source" (ou copiez le).

    .. image:: transfer_1.png
       :width: 450px
       :align:   center

**2. Génération du fichier de transfert**

- Exécutez RUS_DDYTE.exe sur l'ordinateur "source"

- Sélectionnez l'onglet **Transfer License**

- Suivez les instructions décrites dans "Step 2" pour sélectionner la SL key à transférer, lire le fichier contenant les informations de l'ordinateur "receveur" et générer un fichier de transfert de licence (h2h). Assurez-vous que ce fichier est accessible depuis l'ordinateur "receveur".

- Une fois cette étape réalisée, la clé n'est plus disponible sur l'ordinateur "source".

    .. image:: transfer_2.png
       :width: 450px
       :align:   center

.. warning:: Assurez-vous de garder une copie du fichier de transfert jusqu'à la fin de la procédure de transfert. Si vous perdez ce fichier, vous perdez votre licence !

**3. Transfert de la licence**

- Sur l'ordinateur "receveur", ouvrez l'onglet "Apply License File" sur l'utilitaire RUS

- Dans "Update File", téléchargez le fichier de transfert de licence (h2h)

- Cliquez sur **Apply Update**. La clé est maintenant installée sur l'ordinateur "receveur".

    .. image:: transfer_3.png
       :width: 450px
       :align:   center


