Gestion des données hydrologiques et hydro climatiques
######################################################

Introduction
============
On distingue dans Hydra trois familles de données hydrologiques et hydro-climatiques :
- Les apports hydrologiques
- Les niveaux de marée
- Le vent

Les apports hydrologiques
-------------------------
Les apports hydrologiques résultent de la superposition de trois sources distinctes :
- le ruissellement sur les bassins versants  généré par les précipitations,
- les apports ponctuels définis explicitement par un hydrogramme,
- les apports de temps sec résultant de l’activité anthropique ou d’infiltration d’eau claires en provenance des nappes.

Les apports de ruissellement sont générés au niveau du réseau hydrologique via les bassins versants. Ces apports se rejettent généralement dans le réseau hydraulique par l’intermédiaire de l’objet de modélisation |BOUND_HY| :doc:`Hydrograph <../6_Model_building/Boundary_conditions/constant_inflow_bc_point>`.

Les  hydrogrammes d’apport  sont définis  directement dans les objets |BOUND_HY| :doc:`Hydrograph <../6_Model_building/Boundary_conditions/constant_inflow_bc_point>`.

Les apports de temps sont injectés dans le réseau hydraulique par l’intermédiaire de l’objet de modélisation |BOUND_HY| :doc:`Hydrograph <../6_Model_building/Boundary_conditions/constant_inflow_bc_point>`.

Cet agencement est illustré par l’exemple suivant :

    .. image:: _Hydrologic_input_ui.png
       :scale: 75%

L’hydrogramme Hy1 injecté dans le réseau hydraulique est formé par la superposition des hydrogramme ruisselés  en provenance des bassins BV1 et BV2 et d’un hydrogramme de temps sec générés à partir des données du secteurs SECT_1.

L’hydrogramme Hy2 est formé par la superposition de l’hydrogramme défini dans l’élément et  d’un hydrogramme de temps sec généré à partir des données du secteurs SECT_1.

L’hydrogramme Hy3 est formé par la superposition  d’un débit constant de temps sec et L’hydrogramme Hy2 est formé par la superposition.

.. note:: les hydrogrammes déclarés dans l’objet *hydrograph* peuvent être également définis dans un fichier externe déclaré dans le gestionnaire de scénario.

On dispose ainsi d’une grande flexibilité pour définir des hydrogrammes injectés dans le réseau hydraulique.

    .. |BOUND_HY| image:: ../6_Model_building/Boundary_conditions/hydrograph_bc_point.png

    
Les niveaux de marée
--------------------
Les courbes de marée sont définies via la condition à la limite « Z(t) BC » :

    .. image:: _Sea_level_ui.png
       :scale: 75%

Cette courbe peut être définie dans la fenêtre de l’objet ou via un fichier externe. Si l’option *cyclic* est cochée : la courbe est répétée cycliquement.

Le vent
-------
Les conditions de vent sont définies dans le fenêtre de paramétrage d’un scénario, sous forme de courbes temporelles de vitesse et de direction de vent à chacun des anémomètres définis dans le modèle.  Les caractéristiques du vent sont appliquées en tout point  et à tout instant par interpolation des valeurs définies aux stations anémométriques. 

Cette fonction de forçage est appliqués aux mailles d’un domaine 2D et le long d’un domaine filaire.

Le menu principal |I1| **hydrology** permet de paramétrer les apports de temps sec en relation avec les secteurs d’apports, ainsi que la pluviométrie.

    .. |I1| image:: ..\0_Hydra_icone.png
       :height: 25px
       :width: 25px
       :scale: 100%
       :align: middle
    
Apports de temps sec
====================

Le secteur d’apport
-------------------

Le secteur d’apport est délimité par un polygone à l’aide du bouton |DRY_INFLOW_SECT| *Dry inflow sector*.C’est une entité définie au niveau « projet », elle peut englober des objets de modèles différents.

Les données du secteur sont accessibles via le menu hydrology :

    .. image:: _Dry_inflow_sector_ui.png
       :scale: 75%

L’application liste automatiquement tous les objets |BOUND_HY| :doc:`Hydrograph <../6_Model_building/Boundary_conditions/constant_inflow_bc_point>` trouvés à l’intérieur du contour du secteur. Chaque objet *hydrograph*  associé à un secteur est renseigné par :

- le ratio de volume de temps sec du secteur affecté à l’élément ( distribution coefficient),
- le temps de transfert entre l’hydrogramme injecté dans l’élément et l’hydrogramme global défini à l’exutoire aval du secteur ( lag-time) . En général lag-time =0 sauf pour les réseaux de grandes étendues.


    .. |DRY_INFLOW_SECT| image:: dry_inflow_sector_polygon.png

Principe de calcul de l’hydrogramme total d’apport généré par un secteur    
------------------------------------------------------------------------
Un scénario d’apport de temps sec est défini via le menu principal |I1| **hydrology**

    .. image:: _Dry_inflow_sector_ui.png
       :scale: 75%

Il faut tout d’abord renseigner des courbes de modulation des apports EU au cours de la journée.
Chaque scénario est défini par les paramètres suivants :

- La courbe de modulation horaire des apports EU stricts.
- Le volume Veu  de référence des apports d’eau usées strictes (VSm3d) en m3/j,
- Un coefficient d’ajustement des apports EU
- Le volume Vecpp de référence d’eaux claires ( Vcw m3d) en m3/j.
Le débit instantané est calculé comme suit :
L’hydrogramme d’apport de temps sec en chaque point d’injection est construit à partir des courbes volumétriques adimensionnelles comme suit (en m3/s):

.. math::

   Q(t)=V_{eu}*\frac{df(t)}{dt}+V_{Ecpp}*\frac{1}{86400}

Pluviométrie
============
Les données pluviométriques sont définies via le gestionnaire de pluies, puis appelées dans le gestionnaire de scénarios. Différents types de pluies peuvent être définies :



- :doc:`Pluies de projets <project_rainfall>`
- :doc:`Pluies définies par des hyétogrammes synthétiques <hyetograph_rainfall>`
- :doc:`Pluies définies à partir de données pluviographiques <real_hyetograph_data_rainfall>`
- :doc:`Pluies radar <radar_rainfall>`


Vent
====


Marée
=====

















