Gestion des projets
###################

Notions de projets et de modèles
================================

Un modèle correspond à la description, par le biais d'objets de modélisation spécifiques, du territoire à modéliser.
Un projet appelle un ou plusieurs modèles et contient les paramètres des scénarios de calcul et les données hydrologiques associées. Le pilotage des calculs se fait au niveau du projet.


Les données associées à chaque projet et chaqe modèle sont structurées en tables dans la base de données sous jacente. A chaque projet correspond une base de données, structurée en différents schémas :
    - un schéma par modèle,
    - un schéma project regroupant les tables de gestion du projet, communes à tous les modèles hydrauliques du projet. Ce sont des tables géralement alphanumériques décrivant des données non géographiques telles que :
        - la pluviométrie,
        - le paramétrage des scénarios de calcul,
        - les paramètres d’exploitation de résultats, ...
        
les schémas ci-dessous illustrent les correspondance entre un système hydraulique et la base de données sous-jacente :

    .. image:: 4_Project_scheme_BD.png
       :scale: 50%

Gestion des projets
===================
La gestion des projets se fait via l'icone hydra du menu Principal |I|\Gestionnaire de projet.

    .. |I| image:: ..\0_Hydra_icone.png
       :height: 25px
       :width: 25px
       :scale: 100%
       :align: middle

Le gestionnaire de projet permet d'accéder aux fonctionnalités suivantes :
    - création d'un nouveau projet,
    - ouverture d'un projet existant,
    - import / export d'un projet,
    - mise à jour d'un projet existant,
    - copie d'un projet,
    - suppression d'un projet
     
Les fonctionnalités de création et d'ouverture d'un projet existant sont directement accessibles depuis l'icone hydra du menu Principal.


Création d'un projet
--------------------
Lors de la création d'un nouveau projet, l'utilisateur défini un système de projection, qui sera appliqué à l'ensemble des modèles constituant le projet.

L'application crée les tables associées au projet dans la base de données et charge les vues graphiques associées :
    - Rain gage,
    - Dry inflow sector,
    - Anemometer.

A ce stade, aucun modèle n'est associé au projet. Ils sont créés dans un second temps.

Un répertoire portant le nom du projet est cré dans `C:\Users\name\.hydra` ainsi qu'un fichier projet.qgs qui sera appelé lors de l'ouverture d'un projet.

Le fichier projet.qgs est mis à jour lors de l'enregistrement du projet sous QGIS; les mises en forme spécifiques ainsi que tables complémentaires appelées par l'utilisateur sont ainsi enregistrées et chargées à la prochaine ouverture du projet.


Ouverture d'un projet existant
------------------------------
L'ouverture d'un projet entraine le chargement de l'ensemble des tables associées et des outils de création et d'exploitation des modèles dans l'interface.

.. warning:: Ne pas ouvrir un projet via le fichier projet.qgs. Ceci entrainerait uniquement le chargement des tables, sans accès aux outils hydra.


Import / export d'un projet
---------------------------
Les fonctionnalités d'import / export permettent de sauvegarder et d'échanger un projet via un fichier sql unique, contenant la structure et le contenu de la base de données associée.

Lors de l'export, l'utilisateur sélectionne le répertoire dans lequel le fichier projet.sql sera créé.

Lors de l'import, l'utilisateur sélectionne le fichier *.sql à importer, et renseigne le nom du projet à créer, qui peut différer de celui du fichier importé.


Mise à jour d'un projet existant
--------------------------------
La mise à jour du plugin peut nécessiter la modification de la structure de la base de données sous-jacente. Les projets présentant une structure de base de données non compatible avec la version du plugin installée sont indiqués en rouge dans le gestionnaire de projet, et ne peuvent être ouverts.

La fonction de mise à jour permet de mettre à jour la structure de la base de données de ces projets afin de les rendre compatibles avec le plugin installé.

.. note:: La mise à jour de la base de données est irréversible; si besoin, exporter préalablement le modèle pour en conserver une sauvegarde dans la version antérieure.


Copie d'un projet existant
--------------------------


Création d'un modèle
====================
Les fonctions de création de modèles sont disponibles lorsqu'un projet est ouvert:

- création d'un nouveau projet vierge; l'application crée le schéma correspondant dans la base de données et charge les tables acssociées dans l'interface. 10 modèles distincts peuvent être créés dans un même projet.
- import d'un modèle au format csv, généré par Hydracity.

Les différents outils hydra de construction et d'exploitation des modèles sont chargés lors de la création du premier modèle associé au projet ouvert.





