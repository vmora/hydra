var DOCUMENTATION_OPTIONS = {
    URL_ROOT: '',
    VERSION: 'both',
    LANGUAGE: 'french',
    COLLAPSE_INDEX: false,
    FILE_SUFFIX: '.html',
    HAS_SOURCE: true,
    SOURCELINK_SUFFIX: '.txt'
};