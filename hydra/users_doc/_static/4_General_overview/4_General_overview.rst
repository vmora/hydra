Présentation générale
#####################


.. image:: 3_GUI_overview.png
   :width: 650px
   :align: center

Barre de menu
=============
...

Barre d’outils généraux
=======================
Cette barre d’outils, positionnée sous le menu, permet un accès direct aux principales fonctionnalités :
    - Edition, suppression d'objets de modélisation,
    - Choix du scénario actif,
    - Lancement des calculs pour le scénario actif,
    - Outils généraux.

Objets de modélisation
=======================
Les domaines de modélisation
----------------------------
Le schéma suivant récapitule illustre la diversité des entités de modélisation mobilisables au sein d’un même modèle.

.. image:: 3_Modellisation_domains.png
   :width: 665px
   :align: center
       


Gestionnaire de couches
=======================

Espace de travail
=================

