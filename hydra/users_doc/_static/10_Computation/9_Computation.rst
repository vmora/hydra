Execution d'un calcul de simulation
########################################

Lancement d'un scénario de calcul
====================================
Le lancement d'un scénario de calcul peut se faire

- via le :doc:`gestionnaire de scénarios <../8_Scenarios/8_Scenarios>` : sélectionner le scénario, et cliquer sur `run`
- via le bouton *run* de la barre d'outils après avoir sélecctionné le scénario actif via le menu déroulant :

   .. image:: run_scenario.png
      :width: 230px

Etapes de calcul
=====================

Les opérations suivantes sont réalisées en séquence :

1.	Création du sous répertoire xxx dans le répertoire du projet, xxx désignant le nom du scénario ( 24 caractères au maximum).

2.	Création de trois sous répertoires :  /travail ,  /hydrol et / hydraulique  sous le répertoire xxx.

3.	Extraction de la base du projet de toutes les données nécessaires à l’exécution des calculs. Ces données sont converties en fichiers et stockées dans le répertoire « travail », à l’exception du fichier *scenario.nom* qui est copié dans le répertoire du scénario. Ce fichier contient le nom des modèles à exécuter ainsi que les modes de connexion entre modèles. 

4.	Exécution de  l’étape de calcul hydrologique à l’aide du programme exécutable **hydrol.exe**. Les fichiers produits sont stockés dans le répertoire /hydrol. La simulation s’arrête là si le mode de calcul sélectionné est *hydrology*.

5.	Exécution du programme utilitaire **hydragen.exe**. Cet utilitaire met en forme les fichiers générés et les complètent pour assurer une compatibilité parfaite avec les fichiers traités par le programme de calcul hydraulique. C’est notamment dans cet étape que sont ajoutés tous les nœuds de calcul intermédiaires le long des branches filaires, ainsi que les éléments de modélisation visant à assurer les continuités des écoulements au droit des ruptures géométriques. Les fichiers générés par cet utilitaire sont copiés dans le répertoire /hydraulique.

6.	Exécution du programme de calcul **whydram24_dos.exe**. les fichiers de résultats produits par cet exe sont stockés dans le répertoire /hydraulique.

A noter que l’exécution des calculs ne pourra démarrer que si toutes les invalidités de données détectées par l’IHM ont été résolues. 

Autrement dit aucun symbole |ERREUR| ne doit subsister dans le plan de travail du modèle pour permettre le démarrage des calculs.


.. |ERREUR| image:: erreur.png

Répertorisation
=====================

L’arbre suivant récapitule l’organisation des fichiers créés dans l’étape d’exécution des calculs :

.. image:: repertorisation.png
   :width: 460px

Contrôle du bon déroulement des opérations
=================================================

Deux fichiers sont générés à chaque étape de calcul :

-	le fichier d’extension .err  retourne la valeur **0** si le traitement s’est déroulé normalement et si aucune anomalie bloquante n’a été détectée. Il retourne la valeur **1** dans le cas contraire.
-	le fichier d’extension .log contient la nature du message d’erreur détecté par le programme 

En cas d’erreur détectée par un des programmes de calculs la séquence de calcul est interrompue et le message d’erreur contenu dans le fichier .log est affiché dans une fenêtre   de l’IHM. 


Suivi des calculs hydrauliques
=====================================

Les calculs hydrauliques sont gérés par des algorithmes de résolution numérique des équations et nécessitent des moyens de suivi et de contrôle spécifiques. Les précisions données ci-après ne sont pas nécessaires à la bonne exécution des calculs mais elles facilitent la compréhension de la séquence de déroulement des calculs, ainsi que la recherche des solutions en cas d’anomalie de calcul.

Génération des conditions initiales
----------------------------------------

En mode de calcul avec **démarrage à froid**, le calcul démarre avec toutes les liaisons fermées. Les hauteurs d’eau affectées à chaque nœud sont par défaut nulles. 

En pratique une hauteur minimum de 20cm a été introduite le long des branches filaires pour accélérer l’établissement du régime permanent dans les temps négatifs. Les cotes d’eau initiales sont également recalculées pour tenir compte des effets de rehaussement engendrés par des singularités telles que des déversoirs le long d’une branche. Aucun débit fictif n’est introduit. 

Les débits d’apports physiques sont ensuite progressivement injectés dans le modèle pour mettre en eau le modèle. Si une branche n’est alimentée par aucun débit le niveau d’eau initial va rapidement décroitre et la hauteur d’eau sera nulle partout.

Cette méthode d’établissement des conditions de régime établi explique la raison pour laquelle le régime transitoire doit démarrer à un temps négatif, afin d’éviter de polluer les résultats au temps positifs tant qu’on n’a pas atteint un régime permanent.


Suivi du déroulement des calculs à l’écran
---------------------------------------------

Le suivi des calculs de simulation hydraulique est contrôlé par des messages écrits dans une fenêtre DOS :

.. image:: computation.png
   :width: 560px
   :align: center

Cette fenêtre déroulante affiche à chaque pas de temps de calcul le numéro du nœud pour lequel le plus grand saut de variation de cote d’eau entre deux pas de temps est détecté, ainsi que la valeur dz de la variation de cote correspondante. Cette valeur de saut conditionne le réglage automatique du pas de temps de calcul qui est également affiché sur la fenêtre ci-dessus.

Le fichier .out
---------------------------------------------

Un certain nombre d’informations sont stockées dans le fichier **hydraulique/xxx.out**. Ce fichier contient des messages d’avertissements (warnings) sur des anomalies résiduelles non bloquantes détectées sur le jeu de données et sur la façon dont elles ont été résolues. Il contient également :

-	La liste des nœuds de calculs et des singularités rangées par branche, ainsi que les cotes initiales affectées au nœud de calcul avant le démarrage du calcul transitoire,
-	la copie des informations de contrôle affichées sur la fenêtre DOS , plus des informations complémentaires sur l’évolution des pas de temps.

La consultation de ce fichier après calcul permet d’identifier certaines anomalies traduites en particulier par des pas de temps anormalement faibles et imputables à des nœuds que l’on peut parfaitement identifier en inspectant ce fichier. 

A noter que les numéros de nœuds de calcul principaux utilisés par le programme **whydram_dos** sont très exactement les id des noeux consultables dans les éditeurs généraux des couches de type *node*. On peut donc très facilement accéder dans l’IHM à l’endroit du modèle qui présente une difficulté de calcul afin d’identifier la nature du problème et de faciliter sa résolution.

Suivi graphique des calculs
-----------------------------------

Le programme de simulation hydraulique dispose d’un module d’imagerie dynamique de suivi des calculs pendant leur exécution. 

Ce module est activé avec l’option *graphic control during computation* dans le gestionnaire de scénarios. Il permet de visualiser à l’écran l’évolution des lignes d’eau et des débits le long des branches filaires et donc de contrôler graphiquement le bon déroulement des calculs.

.. image:: visu.png
   :width: 620px
   :align: center

Cet utilitaire comporte un menu permettant de sélectionner les branches, de régler les échelles, d’ajuster les intervalles de rafraichissement entre autres. 

On peut à tout moment interrompre le calcul et accéder au menu en tapant sur la touche Echap n’importe où sur le graphe.

Pour sélectionner les branches, il faut cliquer sur le menu branche ; la fenêtre se saisie suivante apparait :

.. image:: select_branche.png
   :width: 470px
   :align: center


Il faut sélectionner le modèle, puis la liste des branches à afficher, séparées entre elles par un caractère blanc. Une branche est définie par son nom précédé du signe « * ».

Cet utilitaire se révèle très utile en phase de mise au point du modèle. A noter que les temps de  calculs sont plus longs dans ce mode qu’en mode DOS en raison des temps d’affichage graphiques.

Fichiers résultats
=====================================

Les fichiers résultats sont regroupés par modèle, même dans le cas d’un calcul multi-modèles en mode global. 

Le tableau suivant récapitule les fichiers résultats produits par défaut pour chaque modèle simulé :

.. image:: tableau.png
   :width: 470px
   :align: center

Tous ces fichiers sont exploités par les utilitaires d’exploitation des résultats, qu’ils soient intégrés à l’IHM ou externes. Plus spécifiquement :

-	Les fichiers .csv sont exploités par l’utilitaire d’analyse thématique. Ils sont également consultables pour des traitements spécifiques sous Excel.
-	Les fichiers binaires .W13, .W14 et .W15 sont lus par les utilitaires de visualisation des courbes x(t), le fichier .W15 est exploité en plus par les utilitaires de traitement cartographique.
-	Les fichiers .W16 servent à tracer des profils en long détaillés le long des branches filaires en prenant en compte l’ensemble des points de calcul et pas seulement les nœuds utilisateurs définis dans l’IHM.
