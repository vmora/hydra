|POROUS_LINK| Liaison poreuse (*Porous link*)
################################################

.. |POROUS_LINK| image:: porous_line.png

Définition
-----------

Fuites à travers des digues perméables.

Données de l'élément
-------------------------

.. image:: porous_line_ui.png
   :width: 450px

Equations
-----------------

.. image:: porous_line_eq1.png
   :width: 420px
   
   
.. image:: porous_line_eq2.png
   :width: 130px

Commentaires
-----------------

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

Il est principalement utilisé pour raccorder latéralement des biefs 1D parallèles de vallée.

Cette liaison peut être générée automatiquement le long d’une ligne de contrainte via les outils de maillage : |MESH| |MESH2|

.. |MESH| image:: ../River_free_surface_flow/mesh_button.png

.. |MESH2| image:: ../River_free_surface_flow/mesh_button_2.png

