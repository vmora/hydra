|OVERFLOW_LINK| Surverse (*Overflow link*)
############################################

.. |OVERFLOW_LINK| image:: overflow_line.png

Définition
-----------

Franchissement d’un obstacle linéaire dans ou le long du lit majeur : berge, talus, remblais.

Données de l'élément
-------------------------

.. image:: overflow_line_ui.png
   :width: 450px

Cet objet est défini par un assemblage de **deux déversants** parallèles dont les caractéristiques (largeur *Width*, cote de seuil *Z crest*) sont déduites de la géométrie de l’obstacle.

Le paramètre *weir coefficient* est le **coefficient de seuil** classique,

Le paramètre *latéral contraction coefficient* contrôle la **largeur effective de la lame déversante**. Il est fonction de la nature physique de la liaison.

**Paramètres de rupture de berge**

Il est possible de simuler une rupture de berge avec cet objet en activant l’option *break mode*. 

2 modes de rupture sont possibles :

- Début de rupture commandée par le dépassement d’un niveau d’eau :

.. image:: overflow_line_rupt1.png
   :width: 470px

- Début de rupture commandé par un temps :

.. image:: overflow_line_rupt2.png
   :width: 470px

Le paramètre *Group number* détermine le groupe de brèches qui se forment simultanément, selon la logique suivante : dès qu’une brèche est détectée sur une liaison d’un groupe donné, les brèches sont déclenchées simultanément sur toutes le liaisons du groupe.

Les autre paramètres de rupture sont :

- La largeur totale de la brèche en fin de rupture (qui doit rester inférieure à la largeur de la liaison)
- La courbe temporelle d’élargissement de la brèche à partir de l’instant de la rupture.

Commentaires
-----------------

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

Son utilisation couvre la modélisation d’un grande variété d’obstacles linéaires, en particulier :

- Le raccordement entre berges d’un lit mineur de cours d’eau et un lit majeur modélisé comme un domaine 2D. Le coefficient de contraction latérale est dans ce cas généralement limité à la valeur .2 pour tenir compte de la nature physique des mécanisme d’échange de débit latéral entre le cours d’eau et le domaine 2D en cas de débordement.
- Le franchissement d’un talus dans un lit majeur,
- Le raccordement entre une rue et l’îlot adjacent en tenant compte de l’effet de blocage des habitations riveraines en bordure de rue.

Cette liaison est généralement générée automatiquement le long d’une ligne de contrainte via les outils de maillage : |MESH| |MESH2|

.. |MESH| image:: ../River_free_surface_flow/mesh_button.png

.. |MESH2| image:: ../River_free_surface_flow/mesh_button_2.png
