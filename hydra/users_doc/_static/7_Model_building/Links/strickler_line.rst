|STRICK_LINK| Liaison frottement (*Strickler link*)
#######################################################


.. |STRICK_LINK| image:: strickler_line.png

Définition
-----------

Liaison par frottement sur le fond

Données d'entrée
-----------------

.. image:: strickler_ui.png
   :width: 480px


Equations
-----------------

Le débit d’échange entre les éléments cide et cidr est donné par les équations :

.. image:: strickler_line_eq1.png
   :width: 230px

Commentaires
-----------------

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

Il est principalement utilisé pour raccorder latéralement des biefs 1D parallèles de vallée.

Cette liaison peut être générée automatiquement le long d’une ligne de contrainte via les outils de maillage : |MESH| |MESH2|

.. |MESH| image:: ../River_free_surface_flow/mesh_button.png

.. |MESH2| image:: ../River_free_surface_flow/mesh_button_2.png

