|WEIR_LINK| Seuil déversant (*Weir link*)
#################################################

.. |WEIR_LINK| image:: Weir_link_button.png


Définition
-----------

Seuil latéral


Données et schéma de définition
----------------------------------

.. image:: weir_link.png
   :width: 440px

Le seuil déversant est défini par :

- une cote radier (*Z invert*), 
- une cote de seuil fixe (*Z weir*),
- une largeur de seuil (*Width*)
- un coefficient de seuil. 

La cote de seuil ZS est mobile. Elle est ajustée pour satisfaire ZA = ZN (cote de régulation)

- La singularité DE intègre plusieurs régimes d’écoulement décrits ci-après.
- Si ZN <= ZS, le seuil est considéré comme fixe. Dans le cas contraire, il est régulé de façon à maintenir le niveau amont égal à ZN, du moins tant que les conditions aval le permettent.
- Attention à la définition du paramètre µD pour le module DE. µD intègre le coefficient 2/3 qui apparaît généralement explicitement dans les formules de déversoirs.


Equations
-------------

La formulation décrivant les écoulements sur le seuil est très générale et s’applique à une classe étendue de singularités regroupant les vannes et les déversoirs. 

Les équations appliquées tiennent compte de la géométrie des ouvrages de part et d’autre du seuil. 

Elles sont détaillées dans le manuel d’analyse : : « Modélisation hydraulique ».


Commentaires
---------------

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

