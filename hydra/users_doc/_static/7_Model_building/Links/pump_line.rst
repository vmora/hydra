|PUMP_LINK| Maillage par pompage (*Pump link*)
###################################################

.. |PUMP_LINK| image:: pump_line.png

Définition
-----------

Station de pompage


Données de l'élément
----------------------------------

.. image:: pump_line_ui.png
   :width: 540px

La station de pompage peut contenir jusqu’à 10 pompes en parallèle.

Les séquences de marche / arrêt de chaque pompe sont réglées par un niveau d’arrêt et un niveau bas de redémarrage. 

Une courbe caractéristique Q(H) est définie pour chaque pompe (10 points maximum)

Commentaires
---------------

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

