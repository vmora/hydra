|BORDA_LINK| Perte de charge type Borda (*Borda headloss*)
###################################################################

.. |BORDA_LINK| image:: borda_headloss_line.png

Définition
-----------

Perte de charge de type Borda.

Cet objet propose un panel de 20 lois différentes de pertes de charge selon la géométrie de la singularité.

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

La perte de charge est exprimée pour chaque loi sous la forme générale :

.. image:: ../Structures/borda_headloss_eq1.png
   :width: 100px

où :

.. image:: ../Structures/borda_headloss_eq2.png
   :width: 350px

dE étant la **perte de charge** de part et d’autre de la singularité

et u la **vitesse de référence** (en général vitesse dans le section amont ou aval de la singularité selon le type de loi sélectionnée)

L’objet *Borda link* partage des lois communes avec l’objet :doc:`Borda singularity <../Structures/borda_headloss_point>`. Il propose des lois supplémentaires en raison de la plus grande diversité de configurations d’accroche des extrémités de la liaison.

Données générales à toutes les lois
----------------------------------------

.. image:: borda_headloss_line_schema.png
   :width: 600px

Il faut préciser la nature de l’objet sur laquelle s’accroche chaque extrémité, car cela conditionne le calcul des termes cinétiques dans l’expression de la charge hydraulique.

Trois types de  choix sont offerts :

1. *Computed* : le nœud extrémité est un container pour laquelle une section géométrique est définie. Le terme cinétique est donné par la vitesse calculée le long du tronçon amont du nœud (pour l’extrémité amont du link) ou aval du nœud (pour l’extrémité aval du link).

2. *Circulaire* : le nœud extrémité est connecté à un tronçon de section circulaire supposé en charge. Il faut alors fournir la section de la canalisation.

3. *Canal* : le nœud extrémité est connecté à un canal de section rectangulaire. Il faut dans ce cas définir la cote radier du canal et sa largeur.

Données et équations spécifiques à chaque loi
----------------------------------------------------

Loi 1 : dE=kQ² 
**************

.. image:: borda_headloss_line_eq1.png
   :width: 370px

Pour cette loi uniquement, la loi de perte de charge est exprimée en négligeant les termes cinétiques attachés aux sections amont et aval.

Le coefficient K est rentré dans l’unité : m/(m3/s)²

Loi 2 : dE=(K/2g)Q²/S²
**************************

.. image:: borda_headloss_line_eq2.png
   :width: 370px

Il faut définir :

- La référence à la section S : tronçon amont ou aval,
- Le coefficient k  ( sans dimension)

Loi 3 : Sharp edge outlet (piquage sortant dans un réservoir)
***************************************************************

.. image:: borda_headloss_line_eq3.png
   :width: 520px

Loi 4 : Sharp edge inlet (piquage entrant depuis un réservoir)
***************************************************************

.. image:: borda_headloss_line_eq4.png
   :width: 530px

Loi 5 : Bifurcation – séparation de courants
**************************************************

.. image:: borda_headloss_line_eq5.png
   :width: 530px

Loi 6 : Jonction – réunion de courants
**************************************************

.. image:: borda_headloss_line_eq6.png
   :width: 530px

Loi 9 : Sharp geometry transition
**************************************************

.. image:: borda_headloss_line_eq7.png
   :width: 350px

.. image:: borda_headloss_line_eq8.png
   :width: 540px

Loi 10 : Contraction with transition
***************************************

.. image:: borda_headloss_line_eq9.png
   :width: 490px

.. image:: borda_headloss_line_eq10.png
   :width: 540px

Loi 11 : Enlargment with transition
*****************************************

.. image:: borda_headloss_line_eq11.png
   :width: 490px

.. image:: borda_headloss_line_eq12.png
   :width: 540px

Loi 12 : Circular bend
***************************

.. image:: borda_headloss_line_eq13.png
   :width: 490px

.. image:: borda_headloss_line_eq14.png
   :width: 540px

Loi 13 : Sharp angle bend
****************************

.. image:: borda_headloss_line_eq15.png
   :width: 490px

.. image:: borda_headloss_line_eq16.png
   :width: 540px

Loi 14 : Screen normal to flow
************************************

.. image:: borda_headloss_line_eq17.png
   :width: 490px

.. image:: borda_headloss_line_eq18.png
   :width: 580px

Loi 15 : Screen oblique to flow
************************************

.. image:: borda_headloss_line_eq19.png
   :width: 490px

.. image:: borda_headloss_line_eq20.png
   :width: 580px

Loi 16 : Friction
******************

.. image:: borda_headloss_line_eq21.png
   :width: 490px

.. image:: borda_headloss_line_eq22.png
   :width: 560px

Loi 17 : Parametric headloss
*********************************

.. image:: borda_headloss_line_eq23.png
   :width: 490px

.. image:: borda_headloss_line_eq24.png
   :width: 560px

Loi 18 : Sharp bend rectangular
*********************************

.. image:: borda_headloss_line_eq25.png
   :width: 490px

.. image:: borda_headloss_line_eq26.png
   :width: 560px

.. image:: borda_headloss_line_eq27.png
   :width: 560px

.. image:: borda_headloss_line_eq28.png
   :width: 560px

Loi 19 : T shaped bifurcation
*********************************

.. image:: borda_headloss_line_eq29.png
   :width: 560px

Loi 20 : T shaped junction
*********************************

.. image:: borda_headloss_line_eq30.png
   :width: 560px

