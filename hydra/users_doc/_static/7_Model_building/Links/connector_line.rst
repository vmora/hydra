|CON_LINK| Connecteur (*Connector link*)
########################################


.. |CON_LINK| image:: connector_line.png

Définition
-----------

Connection simple entre deux containeurs.

Données d'entrée
-----------------

.. image:: connector.png
   :width: 250px

Aucune donnée attributaire pour cet objet

Commentaires
-----------------

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

L’objet *connector* est d’une utilisation très large. Il sert le plus souvent à raccorder des domaines entre eux, en l’absence de singularités physiques.

Cette liaison peut être générée automatiquement le long d’une ligne de contrainte via les outils de maillage : |MESH| |MESH2|

.. |MESH| image:: ../River_free_surface_flow/mesh_button.png

.. |MESH2| image:: ../River_free_surface_flow/mesh_button_2.png

Ce mode de génération est utilisé notamment pour définir une condition à la limite le long d’un frontière dans un domaine 2D.

