|L_REGUL_GATE| Vanne régulée (*Regulated gate link*)
###########################################################

.. |L_REGUL_GATE| image:: regul_gate_line.png

Définition
-----------

Vanne régulée en cote ou en débit.

Données de l'élément
-----------------------

Cet objet à exactement les mêmes caractéristiques que la singularité **vanne régulée** (|STRUC_RG| :doc:`Regulated gate <../Structures/regul_sluice_gate_point>`), il est régit par le mêmes équations et les mêmes principes de régulation.

Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.


    .. |STRUC_RG| image:: ../Structures/regul_sluice_gate_point.png