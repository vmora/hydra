|DERIVATED_PUMP_LINK| Dérivation par pompage (*Derivated pump link*)
##########################################################################

.. |DERIVATED_PUMP_LINK| image:: deriv_pump_line.png

Définition
-----------

Ouvrage de dérivation selon une loi q(z)


Données de l'élément
----------------------------------

.. image:: deriv_pump_line_schema.png
   :width: 450px

On doit définir :

- Le débit capable de la dérivation,
- Les courbe tabulée débit dérivé – cote d’eau au nœud amont de la liason.

Le débit dérivé est exprimé en fraction du débit capable, sa valeur est comprise entre 0 et 1.

La courbe de dérivation doit être monotone et croissante. Elle ne doit pas dépasser 10 points.


Commentaires
---------------

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

