|PT_MARKER| Marqueur de noeud (*Point marker*)
##################################################

.. |PT_MARKER| image:: marker_point_point.png

Définition
-----------

Marqueur de noeud

Données de l'élément
-----------------------

.. image:: marker_point.png
   :width: 240px

Cet objet n’a pas de données attributaires.

Commentaires
---------------

Cet objet est de type *singularité*. Il ne peut être posé que sur un des deux containers suivants :

- :doc:`Manhole <../Network/manhole_point>`

- :doc:`Riber node <../River_free_surface_flow/river_node>`

Il permet de "tagger" visuellement des nœuds dans un modèle, il est également utilisé dans certaines options de calculs définies dans le paramétrage d’un scénario, telle que l’imposition d’un hydrogramme le long d’un tronçon courant.
