|BORDA| Perte de charge Borda (* Borda headloss*)
###################################################

.. |BORDA| image:: borda_headloss_point.png

Définition
-----------

Perte de charge de type Borda.

Cet objet propose un panel de 13 lois différentes de pertes de charge selon la géométrie de la singularité :

.. image:: borda_headloss.png
   :width: 490px

La perte de charge est exprimée pour chaque loi sous la forme générale :

.. image:: borda_headloss_eq1.png
   :width: 100px

où :

.. image:: borda_headloss_eq2.png
   :width: 350px

dE étant la **perte de charge** de part et d’autre de la singularité

et u la **vitesse de référence** (en général vitesse dans le section amont ou aval de la singularité selon le type de loi sélectionnée)

La singularité Borda est obligatoirement posée sur un :doc:`regard <../Network/manhole_point>` ou un :doc:`noeud de bief filaire <../River_free_surface_flow/river_node>`, et donc à la limite entre les sections amont et aval de collecteurs de géométries différentes. 

Les termes E1 et E2 intègrent donc bien les vitesses d’écoulement qui peuvent être différentes de part et d’autre de la singularité :


Données et équations spécifiques à chaque loi
-------------------------------------------------

Loi 1 : dE=kQ² 
**************

.. image:: borda_headloss_eq3.png
   :width: 370px

Pour cette loi uniquement, la loi de perte de charge est exprimée en négligeant les termes cinétiques attachés aux sections amont et aval.

Le coefficient K est rentré dans l’unité : m/(m3/s)²

Loi 2 : dE=(K/2g)Q²/S²
**************************

.. image:: borda_headloss_eq4.png
   :width: 370px

Il faut définir :

- La référence à la section S : tronçon amont ou aval,
- Le coefficient k  ( sans dimension)


Loi 9 : Sharp geometry transition
************************************

.. image:: borda_headloss_eq5.png
   :width: 370px

.. image:: borda_headloss_eq6.png
   :width: 530px

Loi 10 : Contraction with transition
*************************************

.. image:: borda_headloss_eq7.png
   :width: 450px

.. image:: borda_headloss_eq8.png
   :width: 530px


Loi 11 : Enlargment with transition
************************************

.. image:: borda_headloss_eq9.png
   :width: 450px

.. image:: borda_headloss_eq10.png
   :width: 530px


Loi 12 : Circular bend
************************

.. image:: borda_headloss_eq11.png
   :width: 450px

.. image:: borda_headloss_eq12.png
   :width: 530px

Loi 13 : Sharp angle bend
******************************

.. image:: borda_headloss_eq13.png
   :width: 450px

.. image:: borda_headloss_eq14.png
   :width: 530px


Loi 14 : Screen normal to flow
*********************************

.. image:: borda_headloss_eq15.png
   :width: 450px

.. image:: borda_headloss_eq16.png
   :width: 530px

Loi 15 : Screen oblique to flow
**********************************

.. image:: borda_headloss_eq17.png
   :width: 450px

.. image:: borda_headloss_eq18.png
   :width: 530px

Loi 16 : Friction
******************************

.. image:: borda_headloss_eq19.png
   :width: 450px

.. image:: borda_headloss_eq20.png
   :width: 530px

Loi 17 : Parametric headloss
******************************

.. image:: borda_headloss_eq21.png
   :width: 450px

.. image:: borda_headloss_eq22.png
   :width: 530px

Loi 18 : Sharp bend rectangular
*********************************

.. image:: borda_headloss_eq23.png
   :width: 450px

.. image:: borda_headloss_eq24.png
   :width: 530px

.. image:: borda_headloss_eq25.png
   :width: 530px

.. image:: borda_headloss_eq27.png
   :width: 530px
