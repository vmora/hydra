|param_dh| Perte de charge paramétrique (*Parametric headloss*)
#####################################################################

.. |param_dh| image:: param_headloss_point.png

Définition
-----------

Loi paramétrique de variation de cote d’eau en fonction du débit.

Données de l'élément
-----------------------

.. image:: param_headloss.png
   :width: 470px

Conditions à respecter :

- Le nombre de points doit être au plus égal à 10.
- La courbe dh(Q) doit être monotone et strictement croissante.
- dz(1)=Q(1)=0 obligatoirement.

Commentaires
---------------------

Cet objet est de type *singularité*. Il ne peut être posé que sur un des deux containers suivants :

- :doc:`Manhole <../Network/manhole_point>`

- :doc:`Riber node <../River_free_surface_flow/river_node>`

