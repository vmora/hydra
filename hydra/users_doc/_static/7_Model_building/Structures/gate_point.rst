Vanne manuelle (*S_gate*)
##############################

Définition
-----------

Vanne manuelle

Données et schéma de définition
---------------------------------

.. image:: s_gate.png
   :width: 490px

La vanne est définie par :

- la **géométrie** du cadre (*Z ceiling*, *Z invert*, *Width*) 
- la position de la vanne (*Z gate*) qui détermine la **section d’ouverture**,
- et par un **coefficient de vanne** (*Gate coefficient*).

La vanne peut être déplacée selon deux modes :

- déplacement **vers le haut** (*Upward opening*) : la section d’ouverture augmente quand on lève la vanne,
- déplacement **vers la bas** (*Downward opening*) : la section d’ouverture augmente quand on abaisse la vanne.

La position de la vanne (*Z gate*) peut être modifiée durant une simulation via le module externe de contrôle et régulation. Le paramètre  *V max* règle dans ce cas la vitesse maximum de déplacement du seuil de la vanne.

Equations
-------------

La formulation décrivant les écoulements à travers la vanne est très générale et s’applique à une classe étendue de singularités regroupant les vannes et les déversoirs. 

Les équations appliquées tiennent compte de la géométrie des ouvrages de part et d’autre de la vanne. 

Elles sont détaillées dans le manuel d’analyse : : « Modélisation hydraulique ».

Commentaires
---------------------

Cet objet est de type *singularité*. Il ne peut être posé que sur un des deux containers suivants :

- :doc:`Manhole <../Network/manhole_point>`

- :doc:`Riber node <../River_free_surface_flow/river_node>`

