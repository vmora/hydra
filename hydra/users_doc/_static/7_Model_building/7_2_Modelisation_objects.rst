Les objets de modélisation
==========================
La figure suivante présente l’ensemble des objets disponibles pour construire un modèle, ces objets sont regroupés par famille « métier » :

.. image:: 6_Model_objects.png
   :width: 480px
   :align: center

Chaque objet de modélisation fait l’objet d’une fiche précisant :
    - la fonction de l’objet
    - les équations mises en œuvre,
    - les règles  et les restrictions éventuelles d’utilisation

On précise ci-après les règles générales de construction d’un modèle en raisonnant par domaine.
Les mots en italiques désignent les appellations en anglais des différents termes qui figurent dans l’IHM.