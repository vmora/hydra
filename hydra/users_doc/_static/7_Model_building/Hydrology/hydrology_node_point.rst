|HY_N| Regard ou noeud hydrologique (*hydrology node*)
######################################################

Définition
-----------
Regard d’une arborescence hydrologique

    .. |HY_N| image:: hydrology_node_point.png


Données de l'élément
---------------------

.. image:: hydrology-node.png
..

Les données attributaires sont :
	* Ground elevation : la cote du tampon
	* Area (m²) : la section du regard exprimée en m²


Commentaires
--------------

Cet objet ne peut être connecté qu’aux links suivants :
	* |HY_P| :doc:`connecteur hydrologique <connector_hydrology_line>` (*hydrology connector*) 
	* |HY_R| :doc:`routage hydrologique <routing-line>` (*hydrology routing*)
	* des pipes

Il ne peut être occupé que par une des singularités suivantes :
	* |HY_SQQ| :doc:`dérivation QQ <qq_split_hydrology_point>` (*split QQ*)
	* |HY_SZQ| :doc:`dérivation ZQ <zq_split_hydrology_point>` (*split ZQ*)
	* |HY_RS| :doc:`bassin de stockage <reservoir_rs_point>` (*reservoir*)
	* |HY_RSP| :doc:`bassin de stockage paramétrique <reservoir_rsp_point>` (*parametric reservoir*)


  .. |HY_SQQ| image:: qq_split_hydrology_point.png
  .. |HY_SZQ| image:: zq_split_hydrology_point.png
  .. |HY_RS| image:: reservoir_rs_point.png
  .. |HY_RSP| image:: reservoir_rsp_point.png
  .. |HY_R| image:: routing_line.png
  .. |HY_P| image:: connector_hydrology_line.png