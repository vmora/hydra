|HY_RSP| Bassin de stockage paramétrique (*Parametric reservoir*)
####################################################################

.. |HY_RSP| image:: reservoir_rsp_point.png

Définition
-----------

Bassin de retenue équipé d’une surverse S et d’un ouvrage de vidange F à **débit variable** en fonction du niveau d’eau dans le bassin.

.. image:: reservoir_rsp_fonct.png
   :width: 300px
   :align: center

   
Données de l'élément
---------------------

.. image:: reservoir_rsp.png
   :width: 550px
   :align: center
..


Pour sélectionner la branche connectée à la vidange ou la branche connectée à la surverse :
  * Cliquez sur le bouton *selection*
  * Puis cliquez dans le réseau sur le tronçon à sélectionner

La géométrie du réservoir est définie par une courbe tabulée S(z).

Il faut de plus définir :
  * la cote d'eau initiale dans le bassin (*Z initial*)
  * la courbe tabulée Qf(z) du débit de vidange (*drainage discharge*)
  * la courbe tabulée Qs(z) du débit de surverse (*overflow discharge*) 

Si l’option de calcul **pollution** est activée dans la fenêtre de paramétrage du scénario, le bouton *pollution* sert à définir les taux d’abattement ou les concentrations résiduelles des différents paramètres de pollution dans le bassin.

Equations
--------------

La variation du niveau d'eau h dans le bassin est calculée au moyen de l'équation de continuité liant la surface au miroir et les débits entrant et sortant.

.. image:: reservoir_rsp_equations.png
   :width: 260px

Commentaires
--------------

L’objet *reservoir* doit être posé obligatoirement sur un nœud hydrologique.
