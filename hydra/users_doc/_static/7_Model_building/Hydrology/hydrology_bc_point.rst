|HY_BC| Condition limite hydrologique (*Hydrology boundary condition*)
########################################################################

.. |HY_BC| image:: hydrology_bc_point.png

Définition
-----------

Exutoire aval d’une arborescence hydrologique.

Données de l'élément
---------------------

.. image:: hydrology_bc.png
   :width: 250px

Cet objet  ne contient aucune donnée attributaire.

Commentaires
--------------

Cet objet est de type **singularité**. Il est posé obligatoirement sur le nœud hydrologique aval d’une arborescence hydrologique. 

Le débit sortant est perdu pour le réseau.