|HY_SQQ| Discharge split singularity (*split_QQ*)
########################################

.. |HY_SQQ| image:: qq_split_hydrology_point.png

Définition
-----------

Calcul de répartition de débit entre une branche aval et une ou deux branches dérivées selon une loi « split QQ » :

.. image:: splitQQ-fonct.png
   :width: 300px
   :align: center


Données de l'élément
---------------------
.. image:: splitQQ.png
   :width: 430px
   :align: center
..

Pour sélectionner la branche aval ou  une branche dérivée :
  * Cliquez sur le bouton *selection*
  * Puis cliquez dans le réseau sur le tronçon à sélectionner

Les lois de dérivation sont définies sous forme de courbes tabulées Qd1(Qam) et Qd2(Qam). L’hydrogramme aval est déduit par différence : Qav= Qam-Qd1-Qd2.

Chaque courbe de débit doit être strictement croissante.

Commentaires
--------------

L’objet *discharge split singularity* doit être posé obligatoirement sur un nœud hydrologique.
