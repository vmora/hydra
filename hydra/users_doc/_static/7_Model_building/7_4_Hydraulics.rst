Réseaux d'assainissement
========================
Introduction
--------------
Le système d’assainissement urbain ne présente pas de difficulté de représentation topologique car il est parfaitement structuré par le réseau de collecteurs qui le composent. La difficulté principale vient de l’existence de nombreuses singularités interagissant au sein d’une même entité fonctionnelle de gestion : usine de pompage, ouvrage de stockage, station de maillage des flux…

Pour tenir compte de cette réalité, la schématisation d’un réseau d’assainissement distingue trois groupes d’entités, illustrés dans la figure ci-dessous :
    - Le domaine filaire, détaillé dans le présent chapitre,
    - Les stations de gestion, voir :ref:`stations`,
    - Les liaisons latérales voir :ref:`links`.

Modélisation des collecteurs
----------------------------
Les objets de modélisation associés au domaine assainissement sont les suivants :    

    .. image:: ../6_Model_building/Network/_Network_button_ui.png
    ..

Le domaine d’assainissement est structuré par des tronçons de collecteurs ( |NET_P| :doc:`Pipe <Network/pipe_line>`) connectés à chaque extrémité  à un regard ( |NET_M| :doc:`Manhole <Network/manhole_point>`) :

    .. |NET_P| image:: ../6_Model_building/Network/pipe_line.png
    .. |NET_M| image:: ../6_Model_building/Network/manhole_point.png

    .. image:: ../6_Model_building/Network/_Network_pipes_nodes.png
       :scale: 75%

Un regard est défini par sa section et la cote TN. Il est créé avec le bouton |NET_M| :doc:`Manhole <Network/manhole_point>`; une cote lui est affetcée autolmatiquement à partir du MNT chargé le cas échéant.

Un tronçon de collecteur est défini par les cotes radier amont et aval et sa géométrie. Il est créé via le bouton |NET_P| :doc:`Pipe <Network/pipe_line>` puis sélection des regards amont et aval.

La géométrie d’un collecteur peut être de 4 types différents :

    .. image:: ../6_Model_building/Network/_Network_sections.png
       :scale: 75%

Les règles d’agencement des branches de collecteurs
----------------------------------------------------
Les branches de collecteurs sont découpées automatiquement et de façon dynamique au fur et à mesure de l’enrichissement du modèle. Les branches ainsi générées et leur délimitation peuvent être consultées dans la couche « branch » du gestionnaire de couches.

Il n’est donc pas nécessaire de marquer une branche. Toutefois la singularité branch |NET_BM| :doc:`Marker <Network/marker_branch_point>` permet à l’utilisateur de donner à une branche le nom de son choix et de **redéfinir le pas de discrétisation des calculs**; par défaut ce pas est égal à 50m.

    .. |NET_BM| image:: ../6_Model_building/Network/marker_branch_point.png


Dans l’exemple ci-dessous l’agencement du réseau génère 5 branches délimitées comme suit :

    .. image:: ../6_Model_building/Network/_Network_branches.png
       :scale: 50%
    
Les règles d’agencement des branches sont illustrées par le schéma suivant  :

    .. image:: ../6_Model_building/Network/_Network_connecting_rules.png
       :scale: 50%

La règle peut être exprimée comme suit : de part et d’autre d’un nœud il faut respecter la condition : m=1 ou n=1, où m est le nombre de branches amont du nœud et n le nombre de branches aval du nœud.

Gestion des débordements
------------------------
Les débordements sont générés lorsque le niveau d’eau dans un regard atteint la chaussée (cote TN). Le débit de débordement est calculé selon une loi d’orifice définie par une section d’orifice paramétrable. Les volume débordé est considéré comme perdu par le modèle, sauf si le regard est connecté à un réseau de surface par une liaison de type |LINK_NO| :doc:`Network overflow <Links/network_overflow_line>`. Ce point est détaillé au § :ref:`links` .

    .. |LINK_NO| image:: ../6_Model_building/Link/network_overflow_line.png

    
Les ouvrages et liaisons
------------------------
Les regards peuvent porter des :ref:`structures` et être connectés entre eux par des :ref:`links` .



Rivières et écoulements à surface libre
=======================================
Introduction
------------
Les objets de modélisation et outils de construction associés au domaine rivières et écoulements à surface libre sont les suivants :    

    .. image:: ../6_Model_building/River_free_surface_flow/_River_button_ui.png
    ..

Hydra propose différents schémas de modélisation des écoulements de rivière, et de façon plus générale à surface libre (lit majeur, secteurs urbains soumis aux débordements de réseaux, ...), adaptés à l’échelle de travail et aux systèmes étudiés, tout en permettant leur couplage :
    - **filaire**, ou unidimensionnel (1D) : cours d’eau (lits mineur et / ou majeur, cannaux, tronçons couverts), représentés par des sections géométriques agencées le long d’un axe préférentiel d’écoulement ; les vitesses d’écoulement sont imposées le long de cet axe,
    - **bidimensionnel (2D)** : le terrain naturel et / ou la bathymétrie sont schématisés par un maillage triangulaire ou quadrangulaire, chaque maille étant caractérisée par sa surface et la cote moyenne du terrain naturel,
    - **casiers** : zones de stockage présentant de faibles vitesses d’écoulement et de faibles variations de hauteurs d’eau, caractérisés par une loi de remplissage cote / volume,
    - **rues** : axes potentiels privilégiés des écoulements dans un environnement urbain dense et/ou régulier, assimilées à un schéma filaire,    

Ces différents schémas et les objets de modélisation associés sont présentés dans les chapitres suivants. Ils peuvent être connectés entre eux par des :ref:`links`.


Dans le cas de modèle surfaciques comportant plusieurs milliers de mailles et de liaisons il est illusoire de tenter de construire et de renseigner manuellement chaque objet de modélisation. 
Hydra dispose pour cela d’un **outil de génération automatique** très performant permettant, à partir du tracé par l’utilisateur de simples polylignes, des lignes de contrainte |RIV_CST| `Constrain`, de générer automatiquement les maillages 2D ainsi que les liaisons connectant ces différents domaines.

Ces outils sont présentés dans le chapitre suivant.

    .. |RIV_CST| image:: ../6_Model_building/River_free_surface_flow/constrain_line.png

.. _conceptualisation:
 
Conceptualisation du territoire à modéliser pour la génération du maillage 2D, de liaisons en lot et la cartographie avancée des zones inondables
-------------------------------------------------------------------------------------------------------------------------------------------------
Objectifs
*********
La construction des modèles, notamment pour les écoulements de surface, s’appuie sur des méthodes et moyens de conceptualisation permettant de passer d’un schéma de modélisation très général défini par l’utilisateur à la génération automatisée des objets de modélisation associés à cette représentation.

Cette méthodologie permet au modélisateur de se focaliser sur la conceptualisation du schéma de modélisation, découlant directement de sa compréhension du fonctionnement physique du système, et d’automatiser la génération des objets de modélisation associés.

Les écoulements de surface sont avant tout contraints par des marqueurs physiques du terrain naturel, que le modélisateur se doit d’identifier afin de définir les schémas de modélisation les plus adaptés au contexte géographique et aux attendus associés à la modélisation mise en œuvre :
    - structures du système alluvial au sens hydro géomorphologique : lit mineur, délimité par ses berges, lit moyen accueillant les crues fréquentes, lit majeur qui accueille les crues rares à exceptionnelles
    - Obstacles aux écoulements ( remblais, digues, …) et ruptures de pente,  
    - Axes préférentiels d’écoulement,  
    - Fossés de drainage et de ressuyage des plaines alluviales,  
    - Occupation du sol : végétation, bâti, …
    
La construction des modèles passe ainsi par une première étape préalable de délimitation de l’ensemble de ces espaces ou marqueurs, via des **lignes de contraintes** |RIV_CST| `Constrain`.

Lignes de contraintes et coverages
**********************************
Les **lignes de contraintes** sont matérialisées par des polylignes créées par l'utilisateur, via le bouton |RIV_CST|; leurs fonctions sont multiples :
    - délimitation des **frontières extérieures des différents domaines de modélisation**. La juxtaposition de lignes de contraintes contiguës se refermant sur elles-mêmes permet de délimiter des **coverage**, surfaces polygonales auxquelles sont affectés des schémas de modélisation donnés : `mesh (2D)`, `reach (1D)`, `storage`, `street`. Ces polygones sont créés dynamiquement par l'application lors de l'enregistrement des lignes de contraintes. Les modalités de création de ces domaines et des entités associées sont détaillées dans les chapitres suivants :
        - 1d (§ :ref:`1d_domain`),
        - 2d et génération du maillage 2d (§ :ref:`mesh`),
        - casiers (§ :ref:`storage`),
        - rues (§ :ref:`streets`),
    - **automatisation de la génération de liaisons** entre domaines (§ :ref:`links_multi`),
    - généreration des **analyses cartographiques** des réusltats de calcul.


    .. |RIV_CST| image:: ../6_Model_building/River_free_surface_flow/constrain_line.png
   
   
   
Pour chacune de ces lignes de contraintes, l’utilisateur défini :
    - une **fonction** (type) :
        - **boundary**,
        - **flood plain transect**  permettant de définir l’**orientation de la vallée** et de **générer des sections de vallée** pour les modèles filaires,
        - le **type de liaisons** hydrauliques associées le cas échéant, et leurs paramètres non géométriques : 2D, overflow,  porous.   
    - une **taille objectif d’éléments de modélisation** (mailles 2D pour les domaines 2D, largeurs de liaisons d’échange entre deux domaines filaires, …).

    .. image:: ../6_Model_building\River_free_surface_flow/_Constrain_ui.png
    ..    

.. note:: une étoile jaune ( |RIV_CST| `Unclosed constrain`) matérialise les sommets des lignes de contraintes proches mais non fusionnées afin d'alerter l'utilisateur sur :
    - la présence de sommets de deux lignes de contraintes très proches mais non fusionnées, ne permettant pas la création d'un coverage,
    - la proximité de deux sommets d'une même ligne de contrainte susceptible d'entrainer la création de mailles de très petite dimension.


.. |RIV_UNCLOSED_CST| image:: ../6_Model_building/River_free_surface_flow/unclosed_constrain.png
    
Domaine filaire (ou 1D)
-----------------------
Les objets de modélisation
**************************
Les objets de modélisation associés au domaine 1D sont les suivants :    

    .. image:: ../6_Model_building\River_free_surface_flow/_River_1D_button_ui.png
    ..

Principes de modélisation
*************************
Le bief  ( |RIV_REACH| :doc:`Reach <River_free_surface_flow/reach_line>`) constitue l’ossature de ce domaine : c’est une polyligne représentant l’axe du cours d’eau. Cet objet doit être tracé avant tous les autres. Deux nœuds extrémités ( |RIV_RNODE| :doc:`River node <River_free_surface_flow/river_node_point>`) sont automatiquement générés  lors de la création du bief.  

Après sélection du bouton |RIV_REACH| :doc:`Reach <River_free_surface_flow/reach_line>`, l'utilisateur dessine la polyligne matérialisant le bief de l'amont vers l'aval.

Les données attributaires d’un bief sont :
    - son nom
    - le PK origine du bief ( 0. Par défaut)
    - le pas de discrétisation ( 50m par défaut).

On peut ensuite poser des nœuds ( |RIV_RNODE| :doc:`River node <River_free_surface_flow/river_node_point>`) intermédiaires  le long du bief; ces nœuds servent de support aux profils de sections, aux singularités et aux liaisons latérales.

La géométrie d’un bief est définie par les objets |RIV_CS| :doc:`Cross section <River_free_surface_flow/river_cross_section_point>` posés sur les nœuds.


Les extrémités des biefs :
    - portent des :ref:`boundaries`,
    - ou sont connectées à un ou plusieurs autres biefs par des :ref:`links`.

    .. |RIV_REACH| image:: ../6_Model_building/River_free_surface_flow/reach_line.png
    .. |RIV_RNODE| image:: ../6_Model_building/River_free_surface_flow/river_node_point.png
    .. |RIV_CS| image:: ../6_Model_building/River_free_surface_flow/river_cross_section_point.png

    
Géométries de sections et règles d'agencement
**********************************************
La géométrie d’une branche filaire est renseignée au niveau des objets |RIV_CS| :doc:`Cross section <River_free_surface_flow/river_cross_section_point>`, posés sur des noeuds de rivière |RIV_RNODE| :doc:`River node <River_free_surface_flow/river_node_point>`. Pour chacun de ces objets on peut définir une section amont et une section aval, afin de prendre en compte les discontinuités de géométries.

    .. image:: ../6_Model_building/River_free_surface_flow/_Cross_section_ui.png
    ..

**Cinq types différents de sections géométriques sont disponibles :**
    - les **4 sections de collecteurs** définies pour les branches d’assainissement, |NET_P| :doc:`tronçons de collecteur <Network/pipe_line>`  
        - **Circular** : collecteur circulaire, caractérisé par son diamètre
        - **Ovoïd** : collecteur ovoïde, caratérisé par ses diamètres bas et haut et par sa hauteur
        - **sections paramétriques**, définies par une courbe tabulée hauteur / largeur :
            - **Pipe** : collecteur paramétrique fermé
            - **Channel** : canal paramétrique ouvert   
        Une **cote radier** (*zf* upstream elevation et *zf* downstream elevation) et un **coefficient de strickler** sont affectés à chaque section.
        
        Pour les **sections paramétriques**, l'onglet **geométry** permet de sélectionner une géométrie existante ou d'en créer une nouvelle qui sera enregistrée dans la bibliothèque de sections.
    - la section **valley** à lits composés. Elle peut être construite manuellement, ou **générée à partir du MNT et/ou de semis de points bathymétriques** (cf. :doc:`Valley cross section <River_free_surface_flow/Cross_section/Valley_cross_section>`).
        Sont définis :
            - deux **coefficients de strickler** : un pour le lit mineur, et un pour le lit majeur (affecté aux rives gauche et droite),
            - un **coefficient de sinuosité**, correspondant au ratio entre la longueur développée du lit mineur et la longueur de la vallée jusqu'à la section suivante pour laquelle un coefficient de sinuosité est défini (compris entre 0 et 1).
                .. note:: En l'absence de coefficient de Strickler et de coefficient de sinuosité (alors définis à 0), les trois valeurs de la section amont seront automatiquement affectées.
            - Une **géométrie** : l'onglet **geométry** permet de sélectionner une géométrie existante ou d'en créer une nouvelle qui sera enregistrée dans la bibliothèque de sections,
            - Une **cote radier** du lit mineur (*zf* upstream elevation et *zf* downstream elevation). Les cotes section sont recalées en ajoutant la grandeur zf - zfmin à chaque cotes de section, où zfmin est la cote de fond du lit mineur définie dans la géométrie. 
    
La **bibliothèque de sections** est également accessible via le bouton |TB_SEC| `Geometries library` de la barre d'outils hydra.
    
    .. |TB_SEC| image:: ../Tool_bar/geom.svg
        

**Un bief peut ainsi être défini géométriquement comme suit :**

    .. image:: ../6_Model_building/River_free_surface_flow/_Reach_sections_building_rules.png
       :scale: 75%

.. note::
    - Une *geometry downstream* doit être nécessairement définie sur le **nœud amont** du bief
    - Une *geometry upstream* doit être nécessairement définie sur le **nœud aval** du bief
    - Si un seul profil est défini sur un point courant, il peut être posé indifféremment en amont (*upstream*) ou en aval (*downstream).
       
.. _1d_domain:

Délimitation du domaine surfacique 1D
*************************************
La délimitation du domaine surfacique 1D (coverage de type `reach`)est nécessaire pour la génération automatique de liaisons vers d'autres domaines et les traitements cartographiques des résultats de calcul. Elle se fait via les lignes de contrainte |RIV_CST| :doc:`Constrain <River_free_surface_flow/constrain_line>`.

Ce domaine correspond à l'emprise du territoire couverte par une modélisation de type 1D à surface libre, par le biais de sections de type *vallée*. Il est délimité par :
    - deux lignes de contrainte de type flood plain transect définissant les limites amont et aval du domaine,
    - des lignes de contrainte délimitant les frontières latérales du domaine. L'utilisateur précise le type de liaisons associées à ces frontières le cas échéant (cf. :ref:`links`; par défaut, des liaisons |LINK_O| :doc:`Overflow link <Links/overflow_line>` seront générées) 

 
Un coverage est créé lorsque plusieurs lignes de contraintes juxtaposées délimitent un polygone. Ce coverage est de type **Reach** (1D) si il intersecte un bief portant des sections **vallée**.

Des flood plain transect intermédiaires peuvent être ajoutés :
    - pour générer des géométries de sections,
    - pour préciser l'orientation de la vallée. Ces lignes de contraintes complémentaires permettent de définir l'axe d'interpolation des cotes calculées sur le lit mineur en lit majeur pour le traitement cartographique des résultats de calcul ainsi que l'orientation des liaisons vers les domaines connexes le cas échéant. 

    .. image:: ../6_Model_building/River_free_surface_flow/_1D_coverage_building.png
       :scale: 75%


 .. warning::
     Les lignes de contraintes doivent ête fusionnées les unes aux autres sur leurs extrémités.
     
     Une attention particulière doit être apportée à la définition des Flood plain transect aux **extrémités des biefs et des tronçons de type vallée**. Sur l'exemple ci-dessous, à la transition entre un tronçon de type vallée et un tronçon de type collecteur circulaire, les contraintes devant délimiter le domaine 1D doivent être positionnées à l'extérieur des biefs de type vallée : en aval de la dernière section vallée et en amont de la dernière section couverte.

    .. image:: ../6_Model_building/River_free_surface_flow/_1D_coverage_rules.png
       :scale: 75%

.. _storage:

Casiers
-------
Le casier est une zone d’expansion du lit majeur caractérisée par des vitesses d’écoulement généralement faibles et dont le contour s’appuie sur la topographie naturelle ou sur des obstacles artificiels à l’écoulement des eaux. Le casier est délimité par un contour polygonal de forme quelconque et caractérisé par une courbe de remplissage cote / surface.

Le **contour** du casier est défini par une ou plusieurs **lignes de contrainte** |RIV_CST| :doc:`Constrain <River_free_surface_flow/constrain_line>`. Un **coverage** est créé, de type 2D par défaut. L'insertion d'un marqueur de casier |RIV_STORAGE| affecte un type casier au coverage.

    .. image:: ../6_Model_building/River_free_surface_flow/_Storage_building.png
       :scale: 100%

La loi de remplissage cote / surface du casier est calculée à partir du MNT à l'aide du bouton |RIV_STORAGE_FILLING_CURVE|.

    .. image:: ../6_Model_building/River_free_surface_flow/_Storage_ui.png
       :scale: 75%
  

    .. |RIV_STORAGE| image:: ../6_Model_building/River_free_surface_flow/Storage_marker.png
    .. |RIV_STORAGE_FILLING_CURVE| image:: ../6_Model_building/River_free_surface_flow/_Generate_storage_filling_curve_button_ui.png

    
Domaine 2D
----------

.. _mesh: 

Création du maillage
********************
L’ossature d‘un sous domaine 2D est la maille à 3 ou 4 nœuds (mesh element), catactérisée par :
    - sa surface
    - une cote moyenne de fond,
    - une cote basse de fond, calculée à partir des données fournies par les liaisons connectées à la maille.

La génération du maillage s'appuie sur les **lignes de contrainte** |RIV_CST| :doc:`Constrain <River_free_surface_flow/constrain_line>`. La juxtaposition de lignes de contrainte définit le contour d'un **coverage** qui est de type 2D s'il n'intersecte pas de `bief 1D` de type vallée, de `rue` (street) ou de `marqueurs casiers ou null`.

Les paramètres associés à la ligne de contrainte sont :
    - La **largeur d'élément** (`element length`), qui correspond à la **largeur de la maille** attendue le long de la ligne de contrainte,
    - Le **type de liaison** associé à la ligne de contrainte, pour la génération automatique des liaisons entre les éléments situés de part et d'autre de cette ligne de contrainte.

 .. warning:: Le maillage s'appuie également sur les **sommets des lignes de contrainte**; leur espacement doit donc être cohérent avec la taille de mailles souhaitée et la longueur d'élément affectée à la ligne de contrainte pour ne pas créer de trop fortes distorsions dans le maillage. 

Le maillage est généré via le bouton |RIV_MESH|, puis un clic dans le coverage considéré. Lors de cette étape, sont également créées :
    - les liaisons entre mailles 2d situées à l'intéieur du coverage,
    - les liaisons vers les autres domaines s'appuyant sur des lignes de contrainte communes (cf. :ref:`links`).

 .. note:: Des lignes de contraintes peuvent être ajoutées à l'intérieur d'un coverage; le maillage s'appuiera alors sur ces lignes suivant le même principe que pour les frontières extérieures du coverage. Les liaisons entre les éléments siués de part et d'autre de ces lignes de contrainte seront également générées lors du maillage.

Le maillage peut être ajusté, par modification des lignes de contrainte ou de leurs caractéristiques. Le maillage existant doit être préalablement supprimé via le bouton |RIV_MESH_GARBAGE| puis sélection du coverage à démailler. Toutes les liaisons générées automatiquement avec mailles supprimées sont également supprimées.

    .. image:: ../6_Model_building/River_free_surface_flow/_Mesh_2d.png
       :scale: 75%
       
Les outils suivants permettent de générer le maillage en lot :
    - |RIV_MESH_REGEN_UNMESHED| : maillage des coverage non maillés,
    - |RIV_MESH_REGEN_ALL| : maillage de l'ensemble des coverage 2d, y compris ceux déjà maillés le cas échéant.


    .. |RIV_MESH| image:: ../6_Model_building/River_free_surface_flow/mesh.svg
    .. |RIV_MESH_GARBAGE| image:: ../6_Model_building/River_free_surface_flow/delete_mesh.svg
    .. |RIV_MESH_REGEN_ALL| image:: ../6_Model_building/River_free_surface_flow/mesh_regen.svg
    .. |RIV_MESH_REGEN_UNMESHED| image:: ../6_Model_building/River_free_surface_flow/mesh_unmeshed.svg

    
Affectation d'un nom de domaine
*******************************
Un nom de domaine 2D peut ête affecté aux mailles via le bouton |RIV_DOM2D| , permettant de préciser les modalités d’algorithmes de calculs dans ce domaine.

    .. |RIV_DOM2D| image:: ../6_Model_building/River_free_surface_flow/2D_domain_marker_point.png

.. _streets:

Rues
----
Les objets de modélisation associés au domaine rue sont les suivants :    

    .. image:: ../6_Model_building/River_free_surface_flow/_Street_button_ui.png
    ..

    - Les **carrefours** : |RIV_CR| :doc:`Crossroad <River_free_surface_flow/crossroad_point>` : carrefours, définis par leur surface d'emprise,
    - Les **tronçons de rue** : |RIV_STREET_SEGMENT| :doc:`Street segment <River_free_surface_flow/street_segment_line>` : tronçons de rue, définis par une largeur moyenne, un décaissé et un coefficient de strickler,

Les objets |RIV_STREET| **street** sont utilisés pour **automatiser** la construction des éléments associés au domaine *street*, et ne sont pas exploités pour le calcul.    
    
Ce domaine se présente comme un réseau maillé de tronçons de rues connectés à un carrefour à chaque extrémité, permettant de modéliser des débordements le long de rues.

Création manuelle des carrefours et rues
****************************************
Les carrefours sont créés via le bouton |RIV_CR| :doc:`Crossroad <River_free_surface_flow/crossroad_point>`. Les tronçons de rue sont ensuite créés via le bouton |RIV_STREET_SEGMENT| :doc:`Street segment <River_free_surface_flow/street_segment_line>`, en sélectionnant le carrefour amont puis le carrefour aval.


Automatisation de la création des objets carrefour et rue, délimitation du domaine rue
**************************************************************************************
L'objet |RIV_STREET| *rue* permet d'**automatiser la création des carrefours et des tronçons de rue** le long d'une voirie de largeur homogène. Après sélection du bouton |RIV_STREET|, l'utilisateur positionne la polyligne matérialisant la rue puis lui affecte une largeur et un coefficient de strickler.

Après validation, l'application crée :

- des carrefours |RIV_CR| :doc:`Crossroad <River_free_surface_flow/crossroad_point>` aux sommets de la polyligne`*street*,
- des tronçons de rue |RIV_STREET_SEGMENT| :doc:`Street segment <River_free_surface_flow/street_segment_line>`, reliant les |RIV_CR| :doc:`Crossroad <River_free_surface_flow/crossroad_point>`,
- une ligne de contrainte autour de la rue, délimitant un coverage; un type *street* est affecté au coverage, qui contient au moins un objet *street*.

    .. image:: ../6_Model_building/River_free_surface_flow/_Street_coverage_generation.png
       :scale: 75% 

    .. |RIV_STREET| image:: ../6_Model_building/River_free_surface_flow/street_line.png
    .. |RIV_STREET_SEGMENT| image:: ../6_Model_building/River_free_surface_flow/street_line.png
    .. |RIV_CR| image:: ../6_Model_building/River_free_surface_flow/crossroad_point.png

Lorsque plusieurs street s'intersectant sont créées, l'application effectue un découpage des lignes de contraintes créées autour de chaque street. Des coverage de type **crossroad** sont créés aux intersections entre les éléments street; ils contiennent un unique noeud crossroad. Une reprise manuelle des lignes de contrainte ainsi générées s'avère généralement nécessaire pour ajuster les limites de coverages.    

    .. image:: ../6_Model_building/River_free_surface_flow/_Street_coverage_generation_2.png
       :scale: 75% 
       
.. _stations: 

Stations de gestion
===================
La station de gestion est utilisée pour modéliser des usines ou des ouvrages électro mécaniques; elle est matérialisée par un contour polygonal |STN_CONTOUR| entourant des chambres |STN_NODE| `station node` connectées entre elles par des liaisons latérales (:ref:`links`).
    - le contour est créé via le bouton |STN_CONTOUR| :doc:`Station gestion <River_free_surface_flow/Station/station_polygon>`,
    - les chambres sont crées dans l'emprise d'un contour de station de gestion via le bouton |STN_NODE| :doc:`Station node <River_free_surface_flow/Station/station_node_point>`
    
Un contour polygonal de station ne peut communiquer avec le réseau externe que par l’intermédiaire de liaisons latérales : aucun collecteur ne peut traverser un contour de station.

    .. image:: ../6_Model_building/Station/_station_example.png
       :scale: 20%


    .. |STN_NODE| image:: ../6_Model_building/Station/station_node_point.png
    .. |STN_CONTOUR| image:: ../6_Model_building/Station/station_polygon.png

   
.. _structures: 

Ouvrages
========
Les ouvrages (ou singularités) sont posés sur des noeuds : |RIV_RNODE| :doc:`River node <River_free_surface_flow/river_node_point>` et |NET_M| :doc:`Manhole <Network/manhole_point>`, via les boutons associés. Ce sont en pratiques des ouvrages ou des singularités géométriques qui créent une perte de charge le long d’une branche ou d’un bief.
   
.. warning:: 
    - On ne peut poser qu’une seule singularité sur un node,
    - Il est interdit de poser une singularité sur un nœud extrémité de branche.
    
Les ouvrages disponibles sont les suivants : 

    .. image:: ../6_Model_building/Structures/_Strucutres_button_ui.png
    ..

- 1. |STRUC_G| :doc:`Gate <Structures/gate_point>` : **vanne**
- 2. |STRUC_W| :doc:`Weir <Structures/zregul_weir_point>` : **déversoir frontal** (seuil), intègre une loi de régulation exprimée par la tenue d’une cote d’eau constante en amont immédiat du déversoir tant que celui-ci dispose d’une pelle suffisante.
- 3. |STRUC_B| :doc:`Borda <Structures/borda_headloss_point>` : **perte de charge de type Borda** :math:`KV^2/2g`, offre en réalité une dizaine de lois différentes de pertes de charge. C’est donc un objet multifonctions, les lois de pertes de charge reposant pour beaucoup sur les abaques expérimentales tirées du memento d’Idelsik.
- 4. |STRUC_PH| :doc:`parametric headloss <Structures/param_headloss_point>` : perte de charge paramétrique, exprime une loi tabulés dz(Q).
- 5. |STRUC_BRAD| :doc:`Bradley <Structures/bradley_headloss_point>` : **ouvrage de franchissement**, ne s'appliquant qu'aux noeuds de rivière. Il reprend la formulation Bradley et s’applique à un ouvrage de franchissement de cours d’eau en prenant en compte l’effet d’obstruction des piles et des culées; une loi d'orifice est appliquée lorsque l'ouvrage se met en charge. 
- 6. |STRUC_BRID| :doc:`Bridge <Structures/bridge_headloss_point>` : **ouvrage de franchissement**, ne s'appliquant qu'aux noeuds de rivière. Il décrit précisément la courbe d’ouverture b(z) d’un ouvrage de franchissement au-dessus et permet de calculer précisément les pertes de charge tenant compte de la géométriequeconque de l’ouverture.
- 7. |STRUC_RG| :doc:`Regulated gate <Structures/regul_sluice_gate_point>` : **vannage régulé**
    La vanne régulée et la vanne simple (1 |STRUC_G|) ont des caractéristiques géométriques identiques. La vanne régulée autorise une régulation en cote ou en débit, on peut également lui imposer un fonctionnement manuel ; on se retrouve alors dans le cas de la vanne manuelle. On aurait très bien pu supprimer la vanne manuelle de la liste des objets puisque son fonctionnement est couvert par celui de la vanne régulée. Les deux objets sont néanmoins conservés pour bien distinguer un ouvrages statique d’un organe actif régulable.  
- 8. |STRUC_HC| :doc:`Hydraulic cut <Structures/hydraulic_cut_point>` : **coupure hydraulique**, permet de décrite sous forme tabulée z(q) une loi de déversement d’un ouvrage en régime dénoyé. Cet objet a été conservé dans cette version Hydra pour des raisons de compatibilité avec des modèles antérieurs  mais son intérêt  pratique demeure limité désormais.
- 9. |STRUC_MP| :doc:`Point marker <Structures/marker_point_point>` : **marqueur de point**, permet de « tagger » un noeud pour générer des sorties ponctuelles et pour couvrir également des fonctionnalités optionnelles intéressantes, comme celle d’imposer un débit à un nœud de calcul quelconque

   
    .. |STRUC_G| image:: ../6_Model_building/Structures/gate_point.png
    .. |STRUC_W| image:: ../6_Model_building/Structures/zregul_weir_point.png
    .. |STRUC_B| image:: ../6_Model_building/Structures/borda_headloss_point.png
    .. |STRUC_PH| image:: ../6_Model_building/Structures/param_headloss_point.png
    .. |STRUC_BRAD| image:: ../6_Model_building/Structures/bradley_headloss_point.png
    .. |STRUC_BRID| image:: ../6_Model_building/Structures/bridge_headloss_point.png
    .. |STRUC_RG| image:: ../6_Model_building/Structures/regul_sluice_gate_point.png
    .. |STRUC_HC| image:: ../6_Model_building/Structures/hydraulic_cut_point.png
    .. |STRUC_MP| image:: ../6_Model_building/Structures/marker_point_point.png



.. note::
    - Pour l’ensemble de ces objets, les lois perte de charge dE(Q) sont exprimées par rapport à la charge totale ( :math:`z+v^2/2g`) et non par rapport à la cote : les termes cinétiques sont bien intégrés dans les formulations.
    - Pour les calculs, ces singularités sont transformées en interne en liaisons binodales alignées le long du bief ou de la branche :
    .. image:: ../6_Model_building/Structures/_Structures_computation_decomposition.png
      :scale: 75%

.. _links:
      
Liaisons
========
Introduction
------------
Les liaisons permettent de connecter les différents objets de modélisation entre eux, soit n’importe quel noeud de n’importe quel domaine à l’exception des nœuds hydrologiques :
    - les manhole |NET_M|,
    - les river node |RIV_RNODE|,
    - Les station node |STN_NODE|,
    - Les  mesh element 
    - Les storage |RIV_STORAGE|,
    - les crossroads.

Des restrictions sont appliquées sur les règles de connexions de certaines liaisons, détaillées ci-après.  
.. note:: Les liaisons peuvent être créées avant la création des noeuds qui les porteront; elles devront cependant toutes être raccordées à un noeud lors de la génération du calcul.

    Cette relaxation de la contrainte de connectivité des liaisons permet :
        - de positionner des liaisons et de renseigner leurs caractéristiques avant que le schéma de modélisation soit totalement défini (lors du recensement des ouvrages sur le terrain ou de la préparation des levés topographiques par exemple),
        - de conserver leurs caractéristiques si le noeud qui les porte devait être supprimé (actualisation du maillage 2D, modification de l'agancement de carrefours, ...).

Liaisons disponibles
--------------------
Les liaisons disponibles sont les suivantes : 
    
- 1. |LINK_G| :doc:`Gate link <Links/gate_line>` : **vanne**
- 2. |LINK_RG| :doc:`Regulated gate link <Links/regul_gate_line>` : **vannage régulé**
- 3. |LINK_G| :doc:`Weir link <Links/weir_line>` : **seuil déversant**
    Les vannes et seuils déversants (1,2 et 3) ont les même caractéristiques que les singularités correspondantes, à l'exception des seuils : la liaison corrrespond à un seuil fixe alors que la singularité est régulée.
- 4. |LINK_PUMP| :doc:`Pump link <Links/pump_line>` : **maillage par pompage**, permet de modéliser de façon très complète le fonctionnement d’une station de pompage, avec plusieurs pompes en parallèle et une régulation de chaque pompe de type marche-arrêt en fonction d’un niveau bas et d’un niveau haut.
- 5. |LINK_DPUMP| :doc:`Deriv pump link <Links/deriv_pump_line>` : **dérivation par pompage**, loi de pompage simplifiée sous la forme d’un courbe tabulée qpomp(z)
- 6. |LINK_B| :doc:`Borda link <Links/borda_headloss_line>` : **perte de charge de type Borda** :math:`KV^2/2g`, similaire à la singularité Borda (elle intègre plus de lois de pertes de charge, 20 au total).
- 7. |LINK_C| :doc:`Connector link <Links/connector_line>` : **connecteur**, liaison de raccord très utilisée car elle permet de **propager des flux sans perte de charge** ni déphasage.Elle sert par exemple à raccorder un réseau à des chambres de station de gestion, ou des branches filaires entre elles.
- 8. |LINK_KS| :doc:`Strickler link <Links/strickler_line>` : **liaison frottement**, modélise une perte de charge linéaire; son usage tend à se limiter compte des fonctions similaires assurées par d’autres liaisons, mais elles s’avère utile dans certains cas de raccordement latéral entre deux domaines distincts.
- 9. |LINK_P| :doc:`Porous link <Links/porous_line>` : **liaison poreuse**, sert à modéliser des fuites à travers une digue perméable.
- 10. |LINK_O| :doc:`Overflow link <Links/overflow_line>` : **surverse**, composée de deux seuils parallèles matérialisant deux niveaux différents de surverse le long d’un même frontière. Elle est notamment utilisée pour connecter :
    - un cours d’eau à un domaine 2D,
    - une rue à un domaine 2D,
    - les mailles d’un domaine 2D au franchissement d’une discontinuité topographique.
    Cet objet dispose par ailleurs d’un **module de rupture** paramétrable, permettant de modéliser des ruptures de berges ou de digues.
- 11. |LINK_NO| :doc:`Network overflow link <Links/network_overflow_line>` : **surverse depuis un réseau**, obligatoirement connectée à un nœud de type manhole à l’extrémité amont et à un carrefour ou une maille 2D à son extrémité aval. Elle est utilisée pour simuler les échanges de débit entre un regard d’assainissement et un réseau de surface en cas d’inondation de la surface ou de débordement par le regard. Les différents cas de fonctionnement sont illustrés ci-dessous.
    .. image:: ../6_Model_building/Links/_Network_overflow_scheme.png
      :scale: 50%
- 12. |LINK_2d| :doc:`Mesh 2d link <Links/mesh_2d_line>` : **liaison 2d**, connecte les mailles d’un domaine 2D entre elles. Ces liaisons sont caractérisées par une cote z invert calculée le long de l’arête commune aux deux mailles. La cote basse d’un maille (zs2) est déduite via la relation :

    zs2 (i) =  min ( zsij) j=1,4
    
    .. image:: ../6_Model_building/Links/_Mesh2d_link_scheme.png
      :scale: 50%
      
    La cote zs2 est toujours inférieure à la cote de fond moyenne de la maille i  ; elle sert à modéliser des pénétrations lentes en provenance des mailles adjacentes, lorsque ces mailles sont inondées et que leur niveau d’inondation est supérieure à la cotre zs2. On a ainsi un rendu cartographique plus réaliste des inondations possibles des points bas d’une maille.

    .. |LINK_G| image:: ../6_Model_building/Links/gate_line.png
    .. |LINK_RG| image:: ../6_Model_building/Links/regul_gate_line.png
    .. |LINK_W| image:: ../6_Model_building/Links/weir_line.png
    .. |LINK_PUMP| image:: ../6_Model_building/Links/pump_line.png
    .. |LINK_DPUMP| image:: ../6_Model_building/Links/deriv_pump_line.png
    .. |LINK_B| image:: ../6_Model_building/Links/borda_headloss_line.png
    .. |LINK_C| image:: ../6_Model_building/Links/connector_line.png
    .. |LINK_KS| image:: ../6_Model_building/Links/strickler_line.png
    .. |LINK_P| image:: ../6_Model_building/Links/porous_line.png
    .. |LINK_O| image:: ../6_Model_building/Links/overflow_line.png
    .. |LINK_NO| image:: ../6_Model_building/Links/network_overflow_line.png
    .. |LINK_2d| image:: ../6_Model_building/Links/mesh_2d_line.png

Deux types de liaisons peuvent être distingués :
    - Les liaisons 1 à 6, qui modélisent des ouvrages ponctuels dont les caractéristiques ne peuvent être déduites de la seule topographie : ouvrages de décharge, stations de relevage, ... et doivent être **créées manuellement**,
    - Les liaisons 7 à 12, qui peuvent être **générées automatiquement** : liaisons dont les caractéristiques sont uniquement liées à la topographie et à la géométrie des objets auxquels elles sont rattachées : liaisons de déversement sur des digues, berges ou infrastructures en remblai de grande longueur (cf. :ref:`links_multi`); ces liaisons peuvent également être créées manuellement.

    
Création de liaisons individuelles
----------------------------------
Les liaisons sont créées par sélection de l'outil associé à la liaison considérée, puis sélection des objets amont et aval.

    .. image:: ../6_Model_building/Links/_Links_button_ui.png
    ..
    
Des sommets complémentaires peuvent être ajoutés sur la polyligne matérilaisant la liaison, via les outils d'édition topologique de QGIS après avoir rendu la couche correspondante modifiable dans le gestionnaire de couches.


.. _links_multi:

Création des liaisons en lot
----------------------------
La génération des liaisons en lot passe par la définition de **coverage**, polygones délimitant les frontières des différents domaines (et sous domaines) de modélisation entre eux.

Les coverage sont définis par juxtaposition de lignes de contrainte contiguës et fusionnées à leurs extrémités (cf. :ref`conceptualisation`). Deux domaines frontaliers s'appuient sur une ou plusieur ligne(s) de contrainte commune(s).

Les méthodes de construction des covrages sont décrites dans les chapitre spécifiques à chcun d'entre eux : :ref:`1d_domain`, :ref:`storage`, :ref:`mesh`, :ref:`streets`

    .. image:: ../6_Model_building/Links/_demo_links_1.png
       :scale: 75%

Liaisons associées à un maillage 2D
***********************************
Les liaisons internes à un coverage 2d sont générées avec le maillage |RIV_MESH|.

Les lignes de contrainte peuvent être communes à deux domaines de même type ou de type distinct. Lors du maillage d'un coverage, les liaisons vers les éléments des domaines frontaliers sont également créées automatiquement.

Autres laisons entre coverage
*****************************

L'outil |CST_LINKS| permet de générer des liaisons entre les coverage autres que mesh (2d).

Après sélection de l'outil |CST_LINKS|, sélectionner le coverage amont puis le coverage aval; les liaisons sont créées par l'application.


.. |CST_LINKS| image:: ../6_Model_building\River_free_surface_flow/constrain_links.svg

Autres laisons
**************

- Liaisons de **débordement de réseaux** vers les rues et les mailles 2d
    L'outil NOF permet de générer les liaisons Network overflow, entre les regards des réseaux d'assainissement et les domaines de surface mesh (2d) et street.

    L'activation de l'outil regénère l'ensemble des liaisons du modèle :
        - vers les **mailles 2d** contenant les regards : une liaison  de type network overflow est créée entre le manhole et la maille correspondante,
        - vers les **coverage street** contenant les regards : une liaison  de type network overflow est créée entre le manhole et le carrefour (crossroad) le plus proche.
- Liaisons entre un **maillage 2d** et un **reach de type channel**: cette fonctionnalité permet d'immerger un bief 1d dans le maillage 2D sans que celui-ci s'appuie sur les berges du cours d'eau. Elle est notamment utilisée pour modéliser des fossés de faible largeur au regard de la taille des mailles 2d afin de représenter précisément le ressuyage du lit majeur.
    Ces liaisons sont générées lors du maillage. Une liaison est générée depuis le reach portant des sections de type `channel` vers chacune des mailles 2d qu'il intersecte.
    
    .. image:: ../6_Model_building/Links/_Channel_mesh_links.png
      :scale: 50%
- Liaisons entre un **maillage 2d** et une condition limite.
    Ces liaisons, de type `connector`, sont générées via l'outil |CST_LINKS|, puis sélection d'un noeud de station de gestion ( |STN_NODE| `station node`) puis de la ligne de contrainte frontière, qui doit être de type **boudary**.La ligne de contrainte doit être de type « boundary ».
    
    .. image:: ../6_Model_building/Links/_Station_node_meh_link.png
      :scale: 75%

.. |CST_LINKS| image:: ../6_Model_building\River_free_surface_flow/constrain_links.svg

Synthèse
********
L'image ci-dessous présente les éléments générés à partir des différents outils disponibles pour l'exemple présenté en début de chapitre.

    .. image:: ../6_Model_building/Links/_demo_links_2.png
    ..
    
Le tableau ci-dessous synthétise les liaisons inter-domaines pouvant être générées ainsi que l'outil associé.

.. list-table::  
   :widths: 10 10 10 10 10 10 10 10
   :header-rows: 1 
   :stub-columns: 1 

   * -  
     - Mesh 
     - 1d
     - Street
     - Storage
     - Network
     - Channel
     - Station
   * - Mesh
     - |RIV_MESH|
     - |RIV_MESH|
     - |RIV_MESH|
     - |RIV_MESH|
     - NOF
     - |RIV_MESH|
     - |RIV_MESH|
   * - 1d
     - |RIV_MESH|
     - |CST_LINKS|
     - |CST_LINKS|
     - |CST_LINKS|
     - `-`
     - `-`
     - `-`
   * - Street
     - |RIV_MESH|
     - |CST_LINKS|
     - |CST_LINKS|
     - |CST_LINKS|
     - NOF
     - `-`
     - `-`
   * - Storage
     - |RIV_MESH|
     - |CST_LINKS|
     - |CST_LINKS|
     - |CST_LINKS|
     - `-`
     - `-`
     - `-`
   * - Network
     - NOF
     - `-`
     - NOF
     - `-`
     - `-`
     - `-`
     - `-`
   * - Channel
     - |CST_LINKS|
     - `-`
     - `-`
     - `-`
     - `-`
     - `-`
     - `-`
   * - Station
     - |RIV_MESH|
     - `-`
     - `-`
     - `-`
     - `-`
     - `-`
     - `-`




.. _boundaries:

Conditions aux limites
======================
Définition
**********
Les conditions aux limites (*Boundary conditions*) sont des liaisons uninodales, d’une utilisation très générale comme pour les links. Ce  sont des liaisons connectées à n’importe quel noeud à leur extrémité amont et à un nœud  fictif externe à leur extrémité aval :

- Si le flux est positif, il sort du modèle et il est donc perdu.
- S’il est négatif , c’est un apport qui vient enrichir le modèle.

Chacune de ces liisons est attachée à un noeud, et est donc posée sur celui-ci comme pour le cas  d’une singularité .

Si une liaison uninodale est posée sur le nœud aval d’un branche d'assainissement (*branch*) ou d’un bief de rivière (*reach*), elle est assimilée à une **condition limite aval**. Mais cette notion de liaison uninodale recouvre une signification et un emploi beaucoup plus général puisqu’elle s’étend à l’ensemble des noeuds (containeurs) du modèle, à l’exception toutefois des nœuds hydrologiques.

.. warning:: Un nœud ne peut accueillir au plus qu’une seule liaison uninodale ou une seule singularité.

Objets de modélisation
**********************
Les conditions limites disponibles sont les suivantes : 

    .. image:: ../6_Model_building/Boundary_conditions/_Boundaries_button_ui.png
    ..

- 1. |BOUND_CST_INFLOW| :doc:`Constant inflow <Boundary_conditions/constant_inflow_bc_point>` : **débit constant**, permet d’injecter un débit constant en tout point du modèle.
- 2. |BOUND_HY| :doc:`Hydrograph <Boundary_conditions/constant_inflow_bc_point>` : **hydrogramme**, élément de modélisation composite qui fournit tous les **apports au modèle hydraulique**:
    - Hydrogrammes :
        - Hydrogramme imposé
        - Hydrogramme externe, défini par une **courbe Q(t)** appelée dans le gestionnaire de scénario ou **hydrogrammes issus des apports d'un réseau hydrologique** (via une ou plusieurs liaisons *Hydrolgy routing* connectées au noeud portant l'objet Hydrogramme),
    - Apports variables de **temps sec** en relation avec les concepts de secteurs d’apports décrits au §xxx.
- 3. |BOUND_FR| :doc:`Froude <Boundary_conditions/froude_bc_point>` : condition aval de type **chute (Froude)**, modélise les déversements libre en aval de branche ou de bief.
- 4. |BOUND_RK| :doc:`Strickler <Boundary_conditions/strickler_bc_point>` : condition aval de type **strickler**, modélise un écoulement uniforme  en aval de branche ou de bief.
- 5. |BOUND_ZQ| :doc:`Z(Q) <Boundary_conditions/zq_bc_point>` : **loi z(q)**,  modélise un écoulement  de type courbe de tarage en aval de branche ou de bief.
- 6. |BOUND_Zt| :doc:`Z(t) <Boundary_conditions/tz_bc_point>` : **loi z(t)**, modélise un niveau d’eau fixé à un nœud ; ce peut être une courbe de marée par exemple.
- 7. |BOUND_W| :doc:`Weir <Boundary_conditions/weir_bc_point>` : **seuil**, modélise une loi de surverse de type seuil.
- 8. |BOUND_T| :doc:`Tank <Boundary_conditions/tank_bc_point>` : **bassin**, généralement utilisée pour modéliser des bassins de retenue dans des :ref:`stations`. On peut également imposer une cote initiale sur le nœud connecté au bassin.
- 9. |BOUND_CONNECT| :doc:`Model connection <Boundary_conditions/connect_model_bc_point>` : **connection de modèles**, sert à connecter hydrauliquement deux modèles différents d’un même projet. On distingue deux modes de fonctionnement selon les options sélectionnées dans le gestionnaire de scénario :
    - 1. Mode global :
        Les deux modèles sont simulés simultanément et forment donc une seule entité de calcul. Le programme va dans ce cas chercher à appareiller les noms des objets *Model connection*. S’il trouve deux noms identiques dans deux modèles différents, il va alors générer une liaison de type connector entre les deux noms portant ces objets :
         
         .. image:: ../6_Model_building/Boundary_conditions/_connect_model_global_scheme.png
         ..
         
    - 2. Mode cascade :
        Dans ce mode, le modèle 1 est exécuté d’abord, puis le modèle 2. L’hydrogramme sortant du modèle 1 est stocké puis réinjecté dans le modèle 2 lors de la seconde simulation.
        
        Il faut pour cela  définir une condition à la limite de type z(q) dans l’élément *Model connection*du modèle 1 (la courbe z(q) doit être définie par l’utilisateur) et une condition à la  limite de type hydrograph dans l’élément *Model connection* du modèle 2. 


    .. |BOUND_CST_INFLOW| image:: ../6_Model_building/Boundary_conditions/constant_inflow_bc_point.png
    .. |BOUND_HY| image:: ../6_Model_building/Boundary_conditions/hydrograph_bc_point.png
    .. |BOUND_FR| image:: ../6_Model_building/Boundary_conditions/froude_bc_point.png
    .. |BOUND_RK| image:: ../6_Model_building/Boundary_conditions/strickler_bc_point.png
    .. |BOUND_ZQ| image:: ../6_Model_building/Boundary_conditions/zq_bc_point.png
    .. |BOUND_Zt| image:: ../6_Model_building/Boundary_conditions/tz_bc_point.png
    .. |BOUND_W| image:: ../6_Model_building/Boundary_conditions/weir_bc_point.png
    .. |BOUND_T| image:: ../6_Model_building/Boundary_conditions/tank_bc_point.png
    .. |BOUND_CONNECT| image:: ../6_Model_building/Boundary_conditions/connect_model_bc_point.png
    
    
    