Rues (*street*) et carrefours (*crossroad*)
##################################################

Définition
-----------

Les  objets *crossroad* et *street segments* sont concus pour modéliser les écoulements préférentiels le long des rues et à travers les carrefours en cas de débordement en zone urbanisée.

Données des éléments
---------------------

.. image:: crossroad_street.png
   :width: 490px

Le **carrefour** est un container défini par :

- *Ground elevation* = la cote du terrain naturel, renseignée par l’IHM s’il existe un MNT sous-jacent
- *Area* = la surface calculée par l'IHM.
- *h* = le décaissé h du carrefour par rapport au terrain naturel.

Le **segment de rue** est de type *link*. Chaque extrémité joint obligatoirement un container de type **crossroad**. Cet objet est défini par :

- *Width* = une largeur moyenne,
- *Length* = une longueur, calculée par l'IHM.
- *Strickler coefficient* = le coefficient de frottement.

Equations
-------------

Les **carrefours** sont gérés par les mêmes équations que les :doc:`casiers <storage>`.

Les **segments de rue** sont quant à eux gérés par les même équations que les :doc:`tronçons de collecteur <../Network/pipe_line>` avec une section géométrique **rectangulaire**.

Méthode de construction
------------------------

Les deux objets ci-dessus peuvent être construits manuellement et créant d’abord les carrefours, puis les segment de rues les joignant. 

Il est cependant beaucoup plus efficace d’utiliser les outils de génération automatique disponibles dans l’IHM. On procède comme suit :

**ETAPE 1 : Création des rues à l’aide de l’outil** |STREET_LINE|

.. |STREET_LINE| image:: street_line.png

Cet outil permet de tracer une polyligne représentant l’axe d’une rue. Cette opération génère automatiquement les objets suivants :

.. image:: street_const1.png
   :width: 490px

**ETAPE 2 : Densification du réseau de rues**

Il est possible de densifier le réseau de rues en venant appuyer une extrémité de rue sur un point courant d’une rue existante (ce point doit être différent d’un sommet). L’IHM génère un coverage de type crossroad à chaque point d’intersection de rue :

.. image:: street_const2.png
   :width: 550px

**ETAPE 3 : Création d'îlots entre les rues**

La génération de lignes de containtes le long de chaque bordure de rue permet de définir des coverage 2D  qui représentent les ilots  urbains entre les rues. 

Chaque coverage est ensuite maillé. 

En outre, les liaisons internes au coverage des liaisons supplémentaires sont générées entre la rue et l’îlot. Des carrefours intermédiaires sont au besoin ajoutées automatiquement par le programme et les tronçons de rue sont redécoupés pour satisfaite les longueurs de discrétisation définies le long des lignes de contraintes :

.. image:: street_const3.png
