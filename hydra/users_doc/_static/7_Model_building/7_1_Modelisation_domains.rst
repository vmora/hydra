Les domaines de modélisation
============================
Présentation générale
---------------------
Le développement urbain rend de plus en plus complexe la gestion de l’eau et des risques associés. Les métropoles littorales peuvent ainsi être soumises à l’interaction de différents aléas : submersion marine, débordement de cours d’eau, saturation des réseaux d’assainissement pluvial en cas d’événement pluvieux intense entrainant des débordements de surface.

Hydra permet de représenter ces différents phénomènes en un même modèle afin de mettre à disposition de l’ingénieur des outils de compréhension du fonctionnement du système et de lui permettre de proposer et dimensionner des solutions techniques adaptées.

La modélisation numérique est la représentation d'un système physique par une schématisation permettant sa description, sur laquelle sont résolues les équations propres aux phénomènes étudiés. La modélisation revêt ainsi nécessairement un caractère simplificateur de la complexité du système : simplification des caractéristiques géométriques, et simplification des lois physiques associées.

Se pose alors la question des méthodes de schématisation de ces différents systèmes hydrauliques. Alors que les systèmes maritimes peuvent être modélisés par un maillage représentant la bathymétrie (dont la résolution est dépendante de l’échelle de travail), la modélisation des systèmes fluviaux et d’assainissement nécessite de prendre en compte le fonctionnement des singularités que constituent les ouvrages hydrauliques ; l’échelle 
de travail et les enjeux associés à la modélisation mise en œuvre peuvent par ailleurs amener le modélisateur à simplifier ou affiner la schématisation du système.

Hydra propose ainsi différents schémas de modélisation, adaptés à l’échelle de travail et aux systèmes étudiés, tout en permettant leur couplage :
    
- **filaire**, ou unidimensionnel (1D) :
    - réseaux d'assainissement, composés de tronçons auqxquels sont affectés une géométrie, connectés à des regards,
    - cours d’eau (lits mineur et / ou majeur), représentés par des sections géométriques agencées le long d’un axe préférentiel d’écoulement ; les vitesses d’écoulement sont imposées le long de cet axe,
- **bidimensionnel** (2D) : le terrain naturel et / ou la bathymétrie sont schématisés par un maillage triangulaire ou quadrangulaire, chaque maille étant caractérisée par sa surface et la cote moyenne du terrain naturel,
- **casiers** : zones de stockage présentant de faibles vitesses d’écoulement et de faibles variations de hauteurs d’eau, caractérisés par une loi de remplissage cote / volume,
- **rues** : axes potentiels privilégiés des écoulements dans un environnement urbain dense et/ou régulier, assimilées à un schéma filaire,
- **stations de gestion** : unité fonctionnelle composée de chambres interconnectées, permettant une description fine des ouvrages hydrauliques complexes,
- **hydrologie** : bassins versants et réseaux de routage simplifiés, permettant le calcul des apports de ruissellement.

La connexion de ces différents domaines de modélisation (et plus généralement des différents objets de modélisation) est assurée par des liaisons, de natures différentes selon la topographie ou le type d’ouvrage le cas échéant.

Le schéma ci-dessous présente l’ensemble des domaines de schématisation disponibles ainsi que les liaisons hydrauliques permettant leur connexion.
 
.. image:: Modelisation_domains.png
   :width: 650px
   :align: center
    
*Schéma de synthèse des domaines de schématisation disponibles et des liaisons hydrauliques permettant leur connexion*

Une **arborescence hydrologique** se distingue des autres blocs par l’ordre dans lesquels les calculs sont réalisés : les hydrogrammes sont tout d’abord calculés de proche en proche le long de chaque arborescence hydrologique dans l’étape de génération des fichiers de calcul hydraulique. Ils sont ensuite stockés aux exutoires avals, puis injectés à chaque pas de temps de  calcul dans le modèle hydraulique formé par tous les autres blocs fonctionnels.

Le **réseau hydraulique d’assainissement** et l’**arborescence hydrologique** partagent des objets communs de modélisation notamment les tronçons, mais ils diffèrent par les singularités qui sont spécifiques à chaque type de réseau. Les deux types de réseaux ne sont donc pas interchangeables, des adaptations sont nécessaires pour convertir une arborescence hydrologique en un réseau hydraulique et vice versa.

Dans le cas de schématisations 2D, la représentation des ouvrages et discontinuités par des lois d’écoulement précises permettent de conserver un maillage relativement lâche et d’optimiser les temps de calcul.

Hydra propose **deux modes de résolution des équations de Barré de St Venant**, l’un complet et l’autre simplifié (cf paper 1), pouvant être affecté à tout ou partie des différents domaines constituant un modèle donné :

- le **mode complet** prend en compte la totalité des termes des équations de St Venant. Celles-ci mettent en œuvre trois variables indépendantes pour chaque nœud de calcul  d’un domaine 2D ou deux dans un domaine 1D du fait du couplage des équations via les termes convectifs : cote d’eau et vitesses d’écoulement. 
- le **mode simplifié** néglige les termes convectifs, ce qui revient à ne considérer qu’une variable indépendantes à chaque nœud de calcul, à savoir la cote d’eau. Il en résulte des gains appéciables en temps de calcul.

La souplesse d’assemblage de ces différents schémas de modélisation permet de définir différents niveaux de précision suivant les objectifs attendus pour un modèle donné.

Ces schémas de modélisation permettent de couvrir l’ensemble des problématiques associées à la gestion des eaux de surface :

- Hydrologie via des modules de transformation pluie – débit et de routage simplifié,
- Fluviaux,
- Assainissement urbain,
- Maritime et estuariens.

Conceptualisation
-----------------
Les briques décrivant chaque bloc fonctionnel et les interactions entre blocs sont classées formellement en trois grandes entités :

- Le «node » (ou containeur),  un objet de stockage correspondant à un nœud de calcul. Huit types de containeurs sont reconnus dans Hydra :
    - Le bassin versant.
    - Le regard d’une arborescence hydrologique
    - Le regard d’un réseau d’assainissement,
    - Le nœud d’un bief fluvial,,
    - La chambre dans une station de gestion,
    - Le casier,
    - La maille 2D (ou le pavé),
    - Le carrefour,
- La « singularité », entité nodale « posée » sur un containeur. Elle assure des fonctions différentes selon le type :
    - Perte de charge singulière le long d’une branche filaire,
    - Conditions à la limite aval,
    - Liaison latérale uninodale de type Q=f(z).
- La liaison latérale (ou « link »), connectant deux containeurs ; il véhicule les flux d’échanges entre les containeurs.

Certaines de ces entités peuvent être regroupées en collections. On distingue ainsi :
    - La branche de collecteur,
    - Le bief de vallée (ou reach),
    - La station de gestion,
    - Les domaine 2D.

Toutes ces entités servent à définir un modèle hydraulique. Plusieurs modèles peuvent co-exister au sein d’un projet. Les connexions entre modèle sont assurées par des singularités particulières nommées « model connexion ».  
Les principes d’agencement entre ces différentes entités seront gérés dans l’IHM par des règles de cohérence (ou règles d’intégrité), dont la plupart sont intégrées dans l’architecture du modèle conceptuel de données ci-après.

Un modèle hydraulique est décrit dans *PostGre* par un schéma composé de :
    - 72 tables
    - 58 vues

Les tables sont regroupées en trois grandes entités : les « nodes », les « singularities » et les « links ».
Chaque entité se compose d’une table abstraite décrivant les caractéristiques topologiques de l’objet de modélisation et de tables dérivées contenant chacune les attributs spécifiques à chaque objet.
Le diagramme suivant décrit l’ossature du MCD :
 
.. image:: 6_MCD.png
   :width: 480px
   :align: center
    
A chaque entité correspond une table abstraite et autant de tables dérivées que d’objets de modélisation appartenant à cette entité.
De nombreuses autres tables viennent compléter ce MCD. Les caractéristiques de chacune peuvent être consultées dans le gestionnaire pgAdmin.
Les contraintes d’intégrité reliant les tables entre elles, ainsi que les fonctions « triggers »qui sont déclenchées lors d’opérations de création ou de mises à jour de données, contrôlent la validité des différentes opération de création et de mise à jour et  assurent  ainsi la cohérence topologique du modèle à chaque étape de son évolution.