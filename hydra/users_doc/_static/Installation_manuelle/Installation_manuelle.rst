Procédure d'installation avancée d'hydra
##########################################

Installation de QGIS et mise à jour des librairies Python de QGIS
-----------------------------------------------------------------

____

**Installation de QGIS**

Si QGIS n'est pas installé ou si vous souhaitez utilisez hydra avec une autre version de QGIS que celle installée, installez **QGIS** sur le poste : `https://www.qgis.org/fr/site/forusers/download.html <https://www.qgis.org/fr/site/forusers/download.html>`_

_____

**Mise à jour des librairies Python 2.7**

Afin de disposer également des **librairies Python 2.7** utilisées par le plugin HYDRA, ézippez **Lib 2.7.rar** dans le répertoire ``C:/program files/QGIS/Apps/Python27/Lib``.

Cela ajoute 3 répertoires dans /Lib correspondant aux librairies Python 2.7 suivantes :

- rtree 0.8.3
- alabaster
- sphinx

Installation de PostGreSQL et de son extension spatiale PostGis
---------------------------------------------------------------

____

**Installation de PostGreSQL**

Installez **PostgreSQL 9.6** : `https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows <https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows>`_
    
- A la demande du mot de passe pour le « superuser PostGre », notez le mot de passe fourni.
- Les autres paramètres sont à laisser par défaut.

.. warning:: Attention au port d’installation si plusieurs versions cohabitent sur le poste.

____

**Installation de l'extension Postgis via StackBuilder**

A l’issue de l’installation de PostgreSQL, exécutez **StackBuilder** (installateur de compléments à PostgreSQL) :

.. image:: instal_PostGre_1.png
   :width: 380px
   :align:   center

Paramétrez l’installation des composants supplémentaires pour l’installation de PostgreSQL effectuée précédemment.

.. image:: instal_PostGre_2.png
   :width: 380px
   :align:   center

Lors du choix des applications à installer, sélectionnez :

- EDB Language Pack (Add-ons, tools and utilities)
- Postgis (Spatial extensions)

.. image:: instal_PostGre_3.png
   :width: 380px
   :align:   center

Laissez les paramètres par défaut pour l’installation de ces deux extensions.

____

**Mise à jour des librairies Python 3.3**

Afin de disposer également des **librairies Python 3.3** utilisées par le plugin HYDRA, dézippez **Lib 3.3.rar** dans le répertoire ``C:\edb\languagepack-9.6\x64\Python-3.3\Lib``.
 
Cela ajoute 2 répertoires dans /Lib correspondant aux librairies Python 3.3 suivantes :

- numpy 1.11.3
- Shapely 1.6.1


Bibliothèques et programmes complémentaires
-------------------------------------------
GDAL
****
Exécutez **gdal-201-1800-x64-core.msi** (Option *Complete*).

Exécutez **GDAL-2.1.2.win-amd64-py3.3.msi** (*All Users*, *On local hard drive* avec le chemin ``C:\edb\languagepack-9.6\x64\Python-3.3`` en paramètre).

GMSH
****
Dézippez **gmsh-2.12.0-Windows.rar** dans ``C:\Program Files``.


Paramétrage
-----------  
Création d'un utilisateur PostGreSQL
************************************
Il faut à présent créer un utilisateur pour pouvoir accéder à la base de données. Pour cela, il existe **2 possibilités** :

- Créer un utilisateur via un administrateur de bases de données PostgreSQL (pgAdmin). Cette option est recommandée si vous passez par un serveur de bases de données centralisé.
- Exécuter le script **pg_pass_conf.bat**
	- Attention, si PostgreSQL a été installé sur un port différent de 5432, éditez ce fichier et modifiez l'adresse du port.
	- Choisissez un mot de passe (il s’agira du mot de passe lié à votre compte utilisateur pour PostgreSQL). Entrez le mot de passe choisi pour l’utilisateur « PostGre » (cf 2.1.2). Attention, ce second mot de passe se rentre sans retour visuel !).
	
Dans le répertoire ``C:\Users\name\AppData\Roaming\postgresql``, un fichier **pgpass.conf** a été créé, contenant la ligne :

   ``127.0.0.1:*:*:user.name:password``

Mode service PostgreSQL (Pour accès aux données TOPO par GDAL)
**************************************************************
Pour que la base de données puisse accéder aux données des fichiers terrain, passez le service PostgreSQL en mode **Compte Système Local** :

.. image:: instal_mode_service1.png
   :width: 320px
   :align:   center

.. image:: instal_mode_service2.png
   :width: 650px
   :align:   center

Puis redémarrez le service :

.. image:: instal_mode_service3.png
   :width: 500px
   :align:   center

Il est possible, suite à cette manipulation, d'avoir une fenêtre détection des services interactifs qui s'ouvre intempestivement sur le poste. Ce service (**Détection des services interactifs**) peut être arrêté dans le gestionnaire de services sans danger.

Variables d'environnement
*************************
- Si vous disposez des droits d'administration :
  Exécutez **hydra_registery_edit.bat** puis redémarrez le PC.

  Cela permet de créer les variables d’environnement système suivantes (si ces variables sont existantes, les chemins leur sont ajoutés) :

  .. image:: instal_tab1.png
     :width: 600px
     :align:   center

- Si vous ne disposez pas des droits d'administration, créez manuellemement les variables d'environnement ci-dessus depuis la session utilisateur.

**Redémarrez le PC pour que ces modifications soient prises en compte.**
        
.. note :: contrôles à réaliser en cas d'erreurs lors de l'appel du plugin.

    - après exécution de la commande « python », vérifiez que l’on peut appeler les librairies suivantes :
        .. image:: instal_cmd1.png
           :width: 470px
           :align:   center
  
        .. image:: instal_cmd2.png
            :width: 470px
            :align:   center
    - l’appel de **gmsh** en ligne de commande doit produire le résultat suivant :
        .. image:: instal_GMSH.png
           :width: 470px
           :align:   center

Vérification des droits d'administration
****************************************
Pour le déploiement de certaines fonctionnalités, lors du démarrage du plugin, il est nécessaire que python puisse écrire dans le répertoire ``C:\Program Files\PostgreSQL\9.6\share\extension``.

-> Vérifiez les droits d’administration sur ce répertoire.


Appel du plugin dans QGIS
--------------------------
Le chargement du plugin **hydra** est réalisé dans l’interface de QGIS.

Pour charger le plugin :

- Si QGIS est ouvert sur votre poste, fermez-le,

- Dézippez le fichier **hydra.zip** dans ``C:\Users\name\.qgis2\python\plugins``,

- Ouvrez QGIS et activez le plugin hydra via QGIS et le menu extensions > installer/gérer les extensions 

    .. image:: instal_hydra2.png
       :width: 500px
       :align:   center
   
.. note :: Dans le cas d'une installation d'**hydra** sur un réseau, si vous souhaitez paramétrer son dépôt, ouvrez QGIS et rendez-vous dans le menu extensions > installer/gérer les extensions :

    .. image:: instal_hydra.png
       :width: 500px
       :align:   center
   
    .. image:: instal_hydra2.png
       :width: 500px
       :align:   center



