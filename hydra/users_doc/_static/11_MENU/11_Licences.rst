Procédure d'installation et de mise à jour
##########################################

Le chapitre qui suit vise à décrire pas-à-pas la procédure d’installation sur un poste du plugin **hydra**.

Le plugin **hydra** s’ajoute à une installation existante de QGIS, et s’appuie sur une base de données relationnelle (hébergée sous PostgreSQL). La procédure complète se déroule donc en 3 étapes :

- Installation des différents composants :
    - installation de QGIS (version > 2.14) et mise à jour des librairies Python 2.7,
    - installation de PostGre et de son extension spatiale PostGis via *Stackbuilder*, mise à jour des librairies Python 3.3,
    - installation de la bibliothèque GDAL
    - installation du programme GMSH

- Paramétrage :
    - Création d'un utilisateur PostGreSQL
    - Mise à jour des variables d'environnement,

.. warning:: L'installation ne peut se faire que sur un poste **64 bits**.

.. warning:: La procédure d’installation est à exécuter **une seule fois**. La mise à jour ultérieure de l’application passe uniquement par la **mise à jour du plugin HYDRA** dans l’interface de QGIS.

Cette notice fait référence aux fichiers fournis dans le **package d'installation**.

Première installation
=====================
Installation de QGIS et mise à jour des librairies Python de QGIS
-----------------------------------------------------------------

____

**Installation de QGIS**

Si QGIS n'est pas installé ou si vous souhaitez utilisez hydra avec une autre version de QGIS que celle installée, installez **QGIS** sur le poste : `https://www.qgis.org/fr/site/forusers/download.html <https://www.qgis.org/fr/site/forusers/download.html>`_

_____

**Mise à jour des librairies Python 2.7**

Afin de disposer également des **librairies Python 2.7** utilisées par le plugin HYDRA, dézippez **Lib_python-2.7.rar** dans le répertoire *apps\\Python27* du répertoire d'installation de QGIS (par défaut C:\\Program Files\\QGIS 2.xx\\apps\\Python27).

Le répertoire *Lib* existant sera écrasé (on prendra soin de le renommer préalablement *Lib.old* par exemple, en cas d’erreur).

Il est également possible de fusionner les deux répertoires de bibliothèques **Python 2.7** si des bibliothèques personnalisées sont installées.

Installation de PostGreSQL et de son extension spatiale PostGis
---------------------------------------------------------------

____

**Installation de PostGreSQL**

Installez **PostgreSQL 9.6** : `https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows <https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows>`_
    
- A la demande du mot de passe pour le « superuser postgres », notez le mot de passe fourni.
- Les autres paramètres sont à laisser par défaut.

.. warning:: Attention au port d’installation si plusieurs versions cohabitent sur le poste.

____

**Installation de l'extension Postgis via *StackBuilder* **

A l’issue de l’installation de PostgreSQL, exécutez **StackBuilder** (installateur de compléments à PostgreSQL) :

.. image:: instal_PostGre_1.png
   :width: 380px
   :align:   center

Paramétrez l’installation des composants supplémentaires pour l’installation de PostgreSQL effectuée précédemment.

.. image:: instal_PostGre_2.png
   :width: 380px
   :align:   center

Lors du choix des applications à installer, sélectionnez :

- EDB Language Pack (Add-ons, tools and utilities)
- Postgis (Spatial extensions)

.. image:: instal_PostGre_3.png
   :width: 380px
   :align:   center

Laissez les paramètres par défaut pour l’installation de ces deux extensions.

____

**Mise à jour des librairies Python 3.3**

Afin de disposer également des **librairies Python 3.3** utilisées par le plugin HYDRA, dézippez **Lib_python-3.3.rar** dans le répertoire *C:\edb\languagepack-9.6\x64\Python-3.3\Lib* (que l’on pourra prendre soin de renommer *Lib.old* par exemple, en cas de besoin).


Bibliothèques et programmes complémentaires
-------------------------------------------
GDAL
****
Exécutez **gdal-201-1800-x64-core.msi** (Option “Complete”).


GMSH
****
Dézippez **gmsh-2.12.0-Windows.rar** dans *C:\\Program Files*.


Paramétrage
-----------  
Création d'un utilisateur PostGreSQL
************************************
Il faut à présent créer un utilisateur pour pouvoir accéder à la base de données. Pour cela, il existe **2 possibilités** :

- Créer un utilisateur via un administrateur de bases de données PostgreSQL (pgAdmin). Cette option est recommandée si vous passez par un serveur de bases de données centralisé.
- Exécuter le script **pg_pass_conf.bat**
	- |WARN| Attention, si PostgreSQL a été installé sur un port différent de 5432, éditez ce fichier et modifier l'adresse du port.
	- Choisissez un mot de passe (il s’agira du mot de passe lié à votre compte utilisateur pour PostgreSQL). Entrer le mot de passe choisi pour l’utilisateur « PostGre » (cf 2.2.1). Attention, ce second mot de passe se rentre sans retour visuel !).
	
Dans le répertoire *C:\\Users\\name\\AppData\\Roaming\\postgresql*, un fichier **pgpass.conf** a été créé, contenant la ligne :

*127.0.0.1:*:*:user.name:password*

Mode service PostgreSQL (Pour accès aux données TOPO par GDAL)
**************************************************************
Pour que la base de données puisse accéder aux données des fichiers terrain, passez le service PostgreSQL en mode **Compte Système Local** :

.. image:: instal_mode_service1.png
   :width: 320px
   :align:   center

.. image:: instal_mode_service2.png
   :width: 650px
   :align:   center

Puis redémarrez le service :

.. image:: instal_mode_service3.png
   :width: 500px
   :align:   center

Variables d'environnement
*************************
- Si l'utilisateur dispose des droits d'administration :
    Exécutez **hydra_registery_edit.bat** puis redémarrez le PC.

    Cela permet de créer les variables d’environnement système suivantes (si existantes, les chemins leur sont ajoutées) :

    .. image:: instal_tab1.png
       :width: 600px
       :align:   center

    - Dans le cas contraire, créez manuellmement les variables d'environnement ci-dessus depuis la session utilisateur.

**Redémarrer le PC pour que ces modifications soient prises en compte.**
        
.. note : contrôles à réaliser en cas d'erreurs lors de l'appel du plugin.
    - après exécution de la commande « python », vérifiez que l’on peut appeler les librairies suivantes :
        .. image:: instal_cmd1.png
           :width: 470px
           :align:   center
  
        .. image:: instal_cmd2.png
            :width: 470px
            :align:   center
    - l’appel de **gmsh** en ligne de commande doit produire le résultat suivant :
        .. image:: instal_GMSH.png
           :width: 470px
           :align:   center

Vérification des droits d'administration
****************************************
Pour le déploiement de certaines fonctionnalités, lors du démarrage du plugin, il est nécessaire que python puisse écrire dans le répertoire *C:\Program Files\PostgreSQL\9.6\share\extension*.

-> Vérifiez les droits d’administration sur ce répertoire.


Appel du plugin dans QGIS
--------------------------
Le chargement du plugin HYDRA** est réalisée dans l’interface de QGIS.

Pour cela, ouvrez QGIS et rendez-vous dans le menu extensions > installer/gérer les extensions, paramétrez le dépôt du plugin HYDRA :

.. image:: instal_hydra.png
   :width: 500px
   :align:   center
   
.. image:: instal_hydra2.png
   :width: 500px
   :align:   center

Dans le cas où l’utilisateur n’a pas accès au dépôt centralisé :
- fermer QGIS le cas échéant,
- extraire le fichier **hydra.zip** dans C:\\Users\\name\\.qgis2\\python\\plugins,
- activer le plugin hydra via QGIS et le menu extensions > installer/gérer les extensions

    .. image:: instal_hydra2.png
       :width: 500px
       :align:   center
   
Mise à jour du plugin
=====================
Mise à jour
-----------
Si le dépôt du plugin hydra a été paramétré et est accessible, la mise à jour du plugin est réalisée via le menu extensions > installer/gérer les extensions > Tout mettre à jour

    .. image:: instal_hydra2.png
       :width: 500px
       :align:   center

Dans le cas où l’utilisateur n’a pas accès au dépôt centralisé :
- fermer QGIS le cas échéant,
- supprimer le répertoire C:\\Users\\name\\.qgis2\\python\\plugins\\hydra
- extraire le fichier **hydra.zip** dans C:\\Users\\name\\.qgis2\\python\\plugins,

Le plugin sera mis à jour à l'ouverture de QGIS.


Compatibilité projet / version plugin
-------------------------------------

Lors du travail avec le plugin HYDRA, la gestion des versions est effectuée à deux niveaux : on distingue la version du **plugin HYDRA**, qui trace la version du code de l’interface, et le **numéro de version du projet**, qui trace le format dans lequel se trouve la base de données liée au projet.

Versions du plugin
******************

Le numéro de version du plugin est accessible via le gestionnaire d’extension de QGIS. 

Que le plugin soit installé à partir d’un dépôt en ligne ou via une archive .zip, le **numéro de version** est écrit **en tête de la page de présentation du plugin** :

.. image:: version_hydra.png
   :width: 500px
   :align:   center

A chaque itération sur le code du plugin HYDRA, ce numéro de version est incrémenté de façon à refléter l’importance des évolutions apportées.

A chaque nouvelle version, un *changelog* résume également les principaux changements : nouvelles fonctionnalités et corrections de bugs.

Versions du projet
******************

Une mention est également présente dans le *changelog* en cas d’évolution de la structure du modèle de données (la base PostgreSQL) : *"Database version increased to … "*.

Ainsi, chaque version du plugin est conçue pour fonctionner avec des projets d’une structure précise. 

Le **numéro de version de la structure de données** (des projets) est mentionné en tête du **Gestionnaire de Projets** (Menu *Manage projects*) :



