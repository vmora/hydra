Gestion des données topographiques et bathymétriques
####################################################


Hydra permet l'exploitataion de données topographiques et bathymétriques en formats :
    - raster (GeoTif),
    - vecteurs : semis de points (bathymétriques notamment) (shape).

La gestion des données topographiques se fait via l'icone hydra du menu Principal |I1| et le menu Terrain.

    .. |I1| image:: ..\0_Hydra_icone.png
       :height: 25px
       :width: 25px
       :scale: 100%
       :align: middle

.. image:: 5_Manage_terrain_data_ui.PNG
   :width: 480px
   :align: center
    
Données raster
==============
Hydra exploite des données raster au format vrt appelant des dalles au format GeoTif. Ces fichiers doivent être définis dans le même système de projetction que le projet.

L'outil |I2| permet la transformation de fichiers ARCGIS ASCII *.asc en fichiers Geotif et vrt associés est proposé.

    .. |I2| image:: 5_vrt.png
       :height: 25px
       :width: 25px
       :scale: 100%
       :align: middle

Après sélection de l'outil VRT, l'utilisateur sélectionne les fichiers asc en entrée; les fichiers GeoTif et vrt sont enregistrés dans le sous répertoire Terrain du workspace associé au projet. Ces fichiers peuvent être déplacés manuellement; il faut alors modifier le chemin d'accès dans le gestionnaire de terrain.

Plusieurs raster peuvent être appelés, un ordre de priorité est alors fixé par l'utilisateur à chacun d'entre eux en cas de superposition; il correspond à l'ordre d'affichage des différentes sources dans le gestionnaire de données topographiques.

Les raster appelées sont chargés dans le gestionnaire de couches, et une analyse thématique avec ombrage est automatiquement appliquée; celle-ci peut être modifiée par l'utilisateur via le gestionnaire de couches.

.. image:: 5_Raster.png
   :width: 620px
   :align: center
       
Données vectorielles
====================
Hydra peut également prendre en compte des données vectorielles sous forme de semis de points (x, y, z) pour certaines fonctionnalités (génération de profils en travers de cours d'eau notamment).

Les données sont importées et stockées dans la base de données, au niveau du projet, dans la table terrain points (gestionnaire de couches Project\Terrain points). Les données peuvent être directement copiées depuis les fichiers source vers cette table après l'avoir rendue modifiable.

















