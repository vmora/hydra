|S_REG_GATE| Vanne régulée (*Regulated sluice gate*)
##########################################################

.. |S_REG_GATE| image:: regul_sluice_gate_point.png

Définition
-----------

Vanne régulée en cote ou en débit

Données et schéma de définition
---------------------------------

1. Données générales
*********************

.. image:: regul_sluice_gate.png
   :width: 470px

Ces données sont les mêmes que  pour une :doc:`vanne manuelle <sluice_gate>`.

2. Données de régulation
****************************

3 modes de régulation sont proposés :

a. Régulation en cote
........................

.. image:: regul_sluice_gate_z.png
   :width: 490px

Il faut sélectionner le nœud de contrôle pour lequel la régulation en côte est appliquée en cliquant sur le bouton *Select node* puis sur le nœud souhaité dans le modèle.

   **Algorithme de régulation :**

   La variation du débit de consigne à travers la vanne est donnée par :

   .. image:: regul_sluice_gate_eq1.png
      :width: 270px

   où e = écart entre la cote au point de consigne et la cote de consigne

   Le débit de consigne à travers la vanne est transformé en cote d’ouverture de vanne en appliquant les équations du module QMV avec (z1, z2) figés.

   La consigne est rafraîchie au pas de temps donné par dt-reg.

   Si le débit à travers la vanne devient négatif, alors la vanne se ferme automatiquement.
  

   **Ajustement des paramètres de régulation**
  
   Les valeurs adoptées en première approximation sont fournies ci-après pour les quelques configurations types les plus souvent rencontrées en pratique. 
  
   Sur les schémas qui suivent, le point de consigne correspond au point c.
   
   - CONFIGURATION 1 : Point de consigne localisé à l’aval du régulateur (vanne au point R)
   
     .. image:: regul_sluice_gate_z1.png
        :width: 470px
   
     Dans cette configuration, une augmentation du débit q produit une augmentation de la cote en C. On suppose par ailleurs que la cote en C est fortement dépendante du débit en ce point.
   
     On peut choisir les paramètres comme suit :
   
     .. image:: regul_sluice_gate_eq2.png
        :width: 185px
   
     Avec : 
   
     S = surface au miroir traduisant la capacité d’emmagasinement du réseau entre les points R et C

     .. image:: regul_sluice_gate_eq3.png
        :width: 155px

     Exemple pour un collecteur, lorsque le point C est situé à proximité immédiate de R  :

        - K2 = K3 = 0
        - K1 = - LV

          Où L est la largeur au miroir et V la vitesse moyenne de l’écoulement
   
   - CONFIGURATION 2 : Point de consigne situé à l’amont du régulateur (vanne au point R)

     .. image:: regul_sluice_gate_z2.png
        :width: 470px
   
     On suppose dans ce cas que le débit q est directement influencé par la cote en C.
     
     .. image:: regul_sluice_gate_eq4.png
        :width: 150px

   - CONFIGURATION 3 : Actionneur situé en dérivation du point de consigne

     .. image:: regul_sluice_gate_z3.png
        :width: 300px
   
     Les lignes du coefficient sont les mêmes que pour la configuration 2.

     S’il existe une forte dépendance entre le débit Qc et la cote Zc, il faut modifier l’expression pour K1.

     .. image:: regul_sluice_gate_eq5.png
        :width: 330px
      
   Les expressions définies ci-dessus ne donnent que des ordres de grandeur. 
     
   Il faut ensuite affiner les valeurs empiriquement par essais successifs afin d’obtenir la qualité de régulation désirée.
   
   *Remarques* : 
   
   Les temps de la courbe consigne doivent être strictement croissant.
   
   En cas d’inversion de débit, la régulation est interrompue et la vanne se ferme.
   
b. Régulation en débit
..........................

     .. image:: regul_sluice_gate_Q.png
        :width: 490px
  
  **Algorithme de régulation :**
  
   La courbe de consigne est le débit de consigne à travers la vanne ; cette consigne peut être réactualisée à chaque pas de temps :
  
  .. image:: regul_sluice_gate_Q2.png
     :width: 490px

   Le point de consigne est fixé au droit de la vanne et ne peut être modifié par l’utilisateur.
  
   La cote critique est la cote d’eau que l’on s’interdit de dépasser en amont de la vanne. Lorsque cette cote est atteinte la consigne de débit ne peut plus être respectée et la vanne est progressivement ouverte.
  
   La régulation est rafraîchie tous les pas de temps ; le débit de consigne à travers la vanne est transformé en cote d’ouverture de vanne en appliquant les équations du module QMV avec (Z1, Z2) figés.
  
   En cas d’inversion de débit, la régulation est interrompue et la vanne se ferme.
   
c. Pas de régulation
..........................

     .. image:: regul_sluice_gate_noregul.png
        :width: 470px
   
    La sélection de  ce mode équivaut à sélectionner l’objet :doc:`S_gate <sluice_gate>`. 
    
    Les fonctionnalité de l’objet *s_regulated_gate* couvrent donc celles de l’objet *s_gate*. Cependant l’objet *s_gate* est conservé pour des raisons de compatibilité avec le code de calcul.