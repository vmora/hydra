|BRIDGE| Bridge headloss
##############################

.. |BRIDGE| image:: bridge_headloss_point.png

Définition
-----------

Perte de charge de type **Borda** au franchissement d’un pont.

Données de l'élément
-----------------------

.. image:: bridge_headloss.png
   :width: 420px

La section d’ouverture du pont est donnée par une courbe tabule b(Z).

Equations
--------------

.. image:: bridge_headloss_eq1.png
   :width: 210px


.. image:: bridge_headloss_eq2.png
   :width: 320px

E1 et E1 sont les charges hydrauliques de part et d’autre du pont.

C est un coefficient de contraction qui est fonction du rapport SB/S2. Il varie entre 0.6 (si SB/S2<0.6) et 1 (si SB/S2=1.)

Les sections Sb et S2 sont recalculées à chaque pas de temps en fonction des cotes d’eau Z1 et Z2.

Si Sz1 > Z road,  le programme active une loi de déversement au-dessus de la route.

Cette formulation est plus générale que la formulation :doc:`Bradley <bradley_headloss_point>` car elle prend en compte des sections d’ouverture de géométrie quelconque.

Commentaires
---------------

Cet objet est de type *singularité*. Il ne peut être posé que sur un container de type **noeud de rivière** (|RIV_RNODE| :doc:`River node <../River_free_surface_flow/river_node_point>`).

.. |RIV_RNODE| image:: ../River_free_surface_flow/river_node_point.png

