|hydraulic_cut| Coupure hydraulique (*Hydraulic cut*)
############################################

.. |hydraulic_cut| image:: hydraulic_cut_point.png

Définition
-----------

Coupure hydraulique

Données de l'élément
-----------------------

.. image:: hydraulic_cut.png
   :width: 440px

Conditions à respecter :

- Le nombre de points de la courbe Z(Q) doit être au plus égal à 10.
- La courbe z(Q) doit être monotone et strictement croissante.

Equations
---------------

.. image:: hydraulic_cut_eq1.png
   :width: 240px

Za= Z(Q) si Z2 < Z(Q)
Za=Zc si Z2<Z(Q)

Commentaires
---------------------

Cet objet est de type *singularité*. Il ne peut être posé que sur un des deux containers suivants :

- **regard** (|NET_M| :doc:`Manhole <../Network/manhole_point>`)

- **noeud de rivière** (|RIV_RNODE| :doc:`River node <../River_free_surface_flow/river_node_point>`).

.. |RIV_RNODE| image:: ../River_free_surface_flow/river_node_point.png
.. |NET_M| image:: ../Network/manhole_point.png

En pratique, l’utilisation de cet objet reste très limitée car des coupures automatiques sont générées par défaut par le programme de calcul à chaque point de discontinuité de géométrie. 

Ces coupures permettent de gérer les écoulements dénoyés en cas de rupture de cote altimétrique entre deux tronçons.