|BRADLEY| Perte de charge Bradley (*Bradley headloss*)
##########################################################

.. |BRADLEY| image:: bradley_headloss_point.png

Définition
-----------

Perte de charge calculée selon la formule de Bradley au passage d’un pont.

Données de l'élément
-----------------------

.. image:: bradley_headloss.png
   :width: 520px

.. image:: bradley_headloss_schema.png
   :width: 320px

Equations
---------------

Les équations régissant la perte de charge au franchissement du pont sont basées sur la formulation de Bradley. 

Lorsque la cote d’eau amont dépasse Zv (*Z ceiling*), les équations de Bradley sont remplacées par des équations de type orifice si Z1 > Zv+0.5m et des équations interpolées entre Bradley et la loi d’orifice si  Zv < Z1 < Zv+0.5m.

Les distances Dg et Dd sont négatives si la culée rentre dans le lit mineur.

Commentaires
---------------------

Cet objet est de type *singularité*. Il ne peut être posé que sur un container de type **noeud de rivière** (|RIV_RNODE| :doc:`River node <../River_free_surface_flow/river_node_point>`).

.. |RIV_RNODE| image:: ../River_free_surface_flow/river_node_point.png