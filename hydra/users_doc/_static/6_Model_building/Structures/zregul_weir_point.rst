|S_WEIR| Déversoir frontal (*Weir*)
############################################

.. |S_WEIR| image:: zregul_weir_point.png

Définition
-----------

Seuil frontal régulé

Données et schéma de définition
---------------------------------

.. image:: regulated_weir.png
   :width: 470px

Le seuil déversant est défini par :

- une cote de régulation égale à la cote *Z regul* du  seuil mobile lorsque le débit est nul ou négatif.
- une cote basse de seuil fixe (*Z invert*)
- une largeur (*Width*), 
- un coefficient de seuil (*Weir coefficient*)

Lorsque le débit est positif (sens amont vers aval), le seuil mobile se règle à chaque pas de temps pour maintenir la cote d’eau à la valeur *Z regul* en amont immédiat du seuil. Ce niveau d’eau est maintenu tant que le seuil mobile reste supérieur à la cote *Z invert*. Pour les forts débits, le seuil mobile est complètement couché et la loi de seuil est réglée par rapport à la cote *Z invert*.

Il est également possible d’imposer un mode de régulation consistant à régler la largeur du seuil pour maintenir la cote d’eau *Z regul*. La largeur du seuil est dans ce cas variable et varie entre 0 et la valeur *Witdth*.

La cote de régulation *Z regul* peut être modifiée durant une simulation via le **module externe de contrôle et régulation**. On peut également figer le seuil mobile à une cote donnée.

Le bouton **quality** permet de définir les paramètres de réoxygénation de la chute d’eau créée par le seuil lorsque l’option de calcul *physico-chemical* est sélectionnée dans le gestionnaire de scénarios.

Equations
---------------

La formulation décrivant les écoulements sur le seuil est très générale et s’applique à une classe étendue de singularités regroupant les vannes et les déversoirs. 

Les équations appliquées tiennent compte de la géométrie des ouvrages de part et d’autre du seuil. 

Elles sont détaillées dans le manuel d’analyse : : « Modélisation hydraulique ».

Commentaires
---------------

Cet objet est de type *singularité*. Il ne peut être posé que sur un des deux containeurs suivants :

- **regard** (|NET_M| :doc:`Manhole <../Network/manhole_point>`)

- **noeud de rivière** (|RIV_RNODE| :doc:`River node <../River_free_surface_flow/river_node_point>`).

.. |RIV_RNODE| image:: ../River_free_surface_flow/river_node_point.png
.. |NET_M| image:: ../Network/manhole_point.png