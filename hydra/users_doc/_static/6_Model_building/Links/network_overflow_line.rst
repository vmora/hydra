|NET_OVERFLOW_LINK| Surverse depuis un réseau (*Network overflow link*)
#######################################################################

.. |NET_OVERFLOW_LINK| image:: network_overflow_line.png

Définition
-----------

Liaison de surverse entre un regard de collecteur et un conteneur de surface : carrefour ou maille 2D.

Données de l'élément
-------------------------

.. image:: network_overflow_ui.png
   :width: 260px

Les données attributaires sont :

- La cote de surverse, calée au niveau du tampon ou de la bouche avaloir du regard d’assainissement,
- La surface de l’orifice de passage  de l’écoulement au niveau de regard.

Equations et configurations d'écoulement
-----------------------------------------------

Les équation d’échange sont gérées par une loi  symétrique  d’orifice selon les niveaux d’eau respectifs de part et d’autre de la bouche d’égout :

Cas du régime noyé 
**********************

.. image:: network_overflow_eq1.png
   :width: 130px

où :

- S = area

- H = z1-z2

Cas du régime dénoyé 
**********************

.. image:: network_overflow_eq1.png
   :width: 130px

où :

- H = z1-zoverflow

Les 4 régimes possibles d’écoulement sont récapitulés ci-dessous :

.. image:: network_overflow_sch1.png
   :width: 550px

Commentaires
------------------

Cet objet est de type *link*.

Son extrémité amont est nécessairement connecté à un regard ( |NET_M| :doc:`Manhole <../Network/manhole_point>`).

    .. |NET_M| image:: ../Network/manhole_point.png

Son extrémité aval est en général connecté à un des deux containeurs suivants :

- une maille 2D,
- un carrefour.

En pratique les liaisons *network overflow* sont générées automatiquement via l’outil de maillage |MESH_NET|

.. |MESH_NET| image:: outil_maillage.png

Cet outil  scanne l’ensemble des objets *manhole node* et scrute la présence d’un *coverage 2D* ou d’un coverage *street* au-dessus du node. Si c’est le cas il génère la liaison *network overflow*. 

Dans le cas du coverage *street*, il ajoute au besoin des carrefours pour connecter au plus près le *manhole node* à la surface.

