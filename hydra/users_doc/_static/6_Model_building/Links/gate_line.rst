|L_GATE| Vanne manuelle (*Gate link*)
######################################

.. |L_GATE| image:: gate_line.png

Définition
-----------

Vanne manuelle

Données de l'élément
-----------------------

Cet objet à exactement les mêmes caractéristiques que la singularité :doc:`S_gate <../Structures/sluice_gate>` et il est régit par le mêmes équations.

Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.
