|NET_M| Regard (*Manhole*)
#####################################

.. |NET_M| image:: manhole_point.png

Définition
-----------

Regard d’un réseau d’assainissement

Données de l'élément
---------------------

.. image:: manhole.png
   :width: 470px
   :align: center

Les données attributaires sont :
  * la cote du tampon (*Ground elevation*)
  * la section du regard en m² (*Area*)

Equations
--------------

.. image:: manhole_equations.png
   :width: 150px

Avec :
  * qi  = débit du tronçon connecté au regard,
  * qd = débit de débordement si z > zground. Ce débit est exprimé par une loi d’orifice  avec une section d’orifice égale à 0.5 m². 
  
Le débit débordé est considéré comme perdu par le modèle. 
Si le regard est connecté à un réseau de surface via une liaison binodale de type **surverse depuis un réseau** (|LINK_NO| :doc:`Network overflow link <../Links/network_overflow_line>`) le débit déversé n’est pas perdu mais échangé avec le réseau de surface.

.. |LINK_NO| image:: ../Links/network_overflow_line.png

Commentaires
--------------

Les **tronçons de collecteur** ( |NET_P| :doc:`Pipe <pipe_line>`) ne peuvent être connectés qu’à cet objet.

Le regard ne peut être occupé que par **une singularité** de type *structures ans headlosses*

.. |NET_P| image:: pipe_line.png