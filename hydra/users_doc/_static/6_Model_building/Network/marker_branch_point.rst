|NET_BM| Marqueur de branche (*Branch marker*)
#################################################

.. |NET_BM| image:: marker_branch_point.png

Définition
-----------

Marqueur de branche. 

La branche est une succession de **tronçons de collecteurs** (|NET_P| :doc:`Pipe <pipe_line>`) délimités à ses extrémités par une confluence, une défluence, un début ou une fin de tronçon.

.. |NET_P| image:: pipe_line.png


Données de l'élément
---------------------

.. image:: marker_branch.png
   :width: 230px

- pk0 = pk du début de la branche

- dx = pas d’espace entre deux nœuds de calcul

Commentaires
--------------

Les branches de collecteurs sont découpées automatiquement et de façon dynamique au fur et à mesure de l’enrichissement du modèle. Les branches ainsi générées et leur délimitation peuvent être consultées dans la couche « branch » du gestionnaire de couches.

Il n’est donc pas nécessaire de marquer une branche. Toutefois la singularité branch |NET_BM| :doc:`Marker <Network/marker_branch_point>` permet à l’utilisateur de donner à une branche le nom de son choix et de **redéfinir le pas de discrétisation des calculs**; par défaut ce pas est égal à 50m.


Pour imposer un nom de branche utilisateur il suffit de poser le **marqueur de branche** |NET_BM| sur n’importe quel **regard** ( |NET_M| :doc:`Manhole <manhole_point>`) faisant partie de la branche.

.. |NET_M| image:: manhole_point.png
