Sous-domaine 2D (*2D domain*)
########################################

Il est utile dans certaines applications d’affecter un numéro de groupe commun à un ensemble de mailles 2D. 

On affecte à ce numéro de groupe des propriétés particulières associées à des options de calculs via le gestionnaire de scénarios.

On dispose pour cela de deux outils :

- le bouton |MANAGE_2D| (*manage 2D domain*) qui permet de créer un numéro de domaine 2D.

.. |MANAGE_2D| image:: manage_2D_point.png

.. image:: manage_2D.png
   :width: 390px

- le bouton |2D_MARKER| (*2D domain marker*) qui permet d’affecter un numéro de domaine 2D à un ensemble de mailles 2D générées via un coverage 2D.

.. image:: 2D_domain_marker.png
   :width: 250px

Ce marker est placé à l’intérieur du coverage 2D avant maillage. Après maillage du coverage, chaque maille 2D reçoit comme attribut le numéro du sous-domaine inscrit dans le marker :

.. image:: 2D_domain_marker-fonct.png
 
.. |2D_MARKER| image:: 2D_domain_marker_point.png


