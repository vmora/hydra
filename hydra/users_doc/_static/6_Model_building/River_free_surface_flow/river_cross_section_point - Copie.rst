Profil en travers (*Cross section*)
###################################

Définition
-----------

Définition d’une géométrie  sur un :doc:`river node <river_node>`.

Données de l'élément
---------------------

.. image:: cross_section.png

Un objet **cross section** doit obligatoirement être défini sur les nœuds extrémités amont et aval du bief.

Une géométrie ponctuelle est définie en posant une singularité **cross section** sur un **river node**.

- L’utilisateur peut renseigner une géométrie différente en amont et en aval du nœud.

- Une *geometry downstream* doit être nécessairement définie sur le **nœud amont** du bief

- Une *geometry upstream* doit être nécessairement définie sur le **nœud aval** du bief

Si un seul profil est défini sur un point courant, il peut être posé indifféremment en amont (*upstream*) ou en aval (*downstream).

**Types de sections**
*********************

Cinq types de sections peuvent être définies :

- Les sections *circular*, *ovoid*, *pipe* et *channel* que l’on retrouve dans les :doc:`tronçons de collecteur <../Network/pipe_line>` le long d’une branche d’assainissement.

- La section *valley* propre au bief de cours d’eau. Cette dernière est définie par :

  + Le coefficient de Strickler du lit majeur
  + Les coefficient de Strickler du lit mineur
  + Le coefficient de sinuosité : rapport ente la longueur développée du lit mineur et la longueur du lit majeur.
  + Une géométrie de profil en travers qui est stockée dans une bibliothèque de profils en travers.
  Ces paramètres peuvent ne pas être renseignés si le profil est défini sur un nœud courant. Il faut alors attribuer la valeurs 0 à chaque paramètre. 
  
  Le programme sélectionne dans ce cas les valeurs du triplet renseigne sur le premier profil trouvé en amont sur le bief.
  
  Ces paramètres doivent être obligatoirement renseignés dur le nœud amont du bief
  
Un profil en travers de valley est découpé en 3 lits : lit majeur rive gauche, lit mineur et lit majeur rive droite :

.. image:: cross_section_valley.png

.. image:: cross_section_table.png

Pour définir une section en travers de *valley*  il faut cliquer sur le bouton *Geometry* :

.. image:: cross_section_geometry.png
   :width: 460px
   :align: center

- Il faut respecter : 
   .. image:: cross_section_eq1.png
      :width: 350px

- Les cotes et les largeurs de sections doivent être rentrées dans un ordre strictement croissant de valeurs. Entre 2 cotes, les largeurs sont interpolées linéairement.

- Il faut respecter :
   .. image:: cross_section_eq2.png
      :width: 80px

- Si µ1 < 0, le programme considère que l’échange latéral de débit entre le lit majeur actif et le lit d’expansion se fait instantanément. Autrement dit, les cotes d’eau dans le lit (mineur + majeur actif) et dans le lit d’expansion sont maintenues à tout instant.

- Le long d’une même branche, les paramètres µ1 doivent avoir le même signe.

- La rugosité du lit entre (BMIN(N1) ; ZMIN(N1)) et (BMAJ(1) ; ZMAJ(1)) est égale à celle du lit mineur.

**Altimétrie**
*****************

Les cotes renseignées dans la géométrie ci-dessus sont des cotes relatives, l’altimétrie de chaque section est calée par rapport à la cote radier du lit mineur : zf = upstream elevation / downstream elevation.

Les cotes absolues de la section sont recalées en ajoutant la grandeur zf - zfmin à chaque cotes de section, où zfmin est la cote relative de fond du lit mineur dans le tableau *river bed geometry*.

**Précisions sur les murettes**
**********************************

Les murettes (lorsqu’elles existent), sont automatiquement positionnées au droit du premier point du lit majeur.

- Si ZMG < ZMAJ-RG(1), la protection est ignorée en rive gauche

- Si ZMD < ZMAJ-RD(1), la protection est ignorée en rive droite

- Si BMAJ(1) ≠ 0 (en rive gauche ou en rive droite) la murette est en retrait. On considère alors que le lit mineur comprend la largeur BMAJ-RG(1) + BMAJ-RD(1).

**Génération d’une géométrie de section de vallée à partir du MNT et/ou d’un semis de points**
************************************************************************************************

A compléter