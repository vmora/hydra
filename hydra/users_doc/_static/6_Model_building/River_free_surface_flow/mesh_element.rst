|RIV_MESH_ELT| Maille 2D (*Mesh element*)
###################################################

.. |RIV_MESH_ELT| image:: pave_polygon.png

Définition
-----------

La maille 2D (*mesh element*) peut en première approche être assimilée à un casier de forme particulière. En réalité c’est une maille d’un domaine bidimensionnel, connectée aux mailles adjacentes par l’intermédiaire de liaisons spécifiques : les liaisons de type *mesh link*. 

Le couple (mesh element, mesh link) interagit pour résoudre les équations de St Venant à deux dimensions. 

Données de l'élément
---------------------

.. image:: mesh_element.png
   :width: 270px

La maille 2D est un container défini géométriquement par un contour polygonal de 3 ou 4 sommets.

Les données attributaires sont :

- *Z bottom* = la cote de fond moyenne
- *Strickler coefficient* = le coefficient de frottement.
- *Area* = la surface (calculée) en m²

Méthode de construction
------------------------

Cet objet peut être construit manuellement à l’aide du bouton *mesh element*. Il est cependant bien préférable de le générer automatiquement via le bouton de :doc:`ligne de contrainte <constrain_line>` |CONSTRAIN_LINE|.

.. |CONSTRAIN_LINE| image:: constrain_line.png

La fermeture d’un contour polygonal crée par défaut un « coverage » de type 2D :

.. image:: mesh_element_image.png
   :width: 320px

La sélection du bouton *create mesh* et un clic souris à l’intérieur du coverage génère le maillage suivant, dont la densité est contrôlée par les attributs des **lignes de contraintes** :

.. image:: mesh_element_image2.png
   :width: 320px

Cette opération permet de générer non seulement **les mailles 2D** et leurs attributs mais également les liaisons *mesh link* connectant les mailles entre elles, ainsi que leurs **attributs géométriques et altimétriques**.

Les **attributs altimétriques** ne sont toutefois générés que si la **couche MNT** sous-jacente est présente dans le modèle.

