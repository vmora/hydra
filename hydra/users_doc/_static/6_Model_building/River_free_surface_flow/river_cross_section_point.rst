Profil en travers (*Cross section*)
###################################

Définition
-----------

Les objets |RIV_CS| :doc:`Cross section <river_cross_section_point>`, posés sur des noeuds de rivière |RIV_RNODE| :doc:`River node <river_node_point>`, permettent de définir la géométrie d'une branche filaire. Pour chacun de ces objets on peut définir une section amont et une section aval, afin de prendre en compte les **discontinuités de géométries**.

    .. |RIV_RNODE| image:: river_node_point.png
    .. |RIV_CS| image:: river_cross_section_point.png
    
Données de l'élément
---------------------

.. image:: _cross_section_ui.png


Cinq types différents de sections géométriques sont disponibles :
    - les **4 sections de collecteurs** définies pour les branches d’assainissement, |NET_P| :doc:`tronçons de collecteur <../Network/pipe_line>`  
        - **Circular** : collecteur circulaire, caractérisé par son diamètre
        - **Ovoïd** : collecteur ovoïde, caratérisé par ses diamètres bas et haut et par sa hauteur
        - **sections paramétriques**, définies par une courbe tabulée hauteur / largeur :
            - **Pipe** : collecteur paramétrique fermé
            - **Channel** : canal paramétrique ouvert   
        Une **cote radier** (*zf* upstream elevation et *zf* downstream elevation) et un **coefficient de strickler** sont affectés à chaque section.
        
        Pour les **sections paramétriques**, l'onglet **geométry** permet de sélectionner une géométrie existante ou d'en créer une nouvelle qui sera enregistrée dans la bibliothèque de sections.
    - la section **valley** à lits composés. Elle peut être construite manuellement, ou **générée à partir du MNT et/ou de semis de points bathymétriques** (cf. :doc:`Géométries de vallées <Cross_section/Valley_cross_section>`).
        Sont définis :
            - deux **coefficients de strickler** : un pour le lit mineur, et un pour le lit majeur (affecté aux rives gauche et droite),
            - un **coefficient de sinuosité**, correspondant au ratio entre la longueur développée du lit mineur et la longueur de la vallée jusqu'à la section suivante pour laquelle un coefficient de sinuosité est défini (compris entre 0 et 1).
                .. note:: En l'absence de coefficient de Strickler et de coefficient de sinuosité (alors définis à 0), les trois valeurs de la section amont seront automatiquement affectées.
            - Une **géométrie** : l'onglet **geométry** permet de sélectionner une géométrie existante ou d'en créer une nouvelle qui sera enregistrée dans la bibliothèque de sections,
            - Une **cote radier** du lit mineur (*zf* upstream elevation et *zf* downstream elevation). Les cotes section sont recalées en ajoutant la grandeur zf - zfmin à chaque cotes de section, où zfmin est la cote de fond du lit mineur définie dans la géométrie.            

    .. |NET_P| image:: ../Network/pipe_line.png
    
La **bibliothèque de sections** est également accessible via le bouton |TB_SEC| `Geometries library` de la barre d'outils hydra.
    
    .. |TB_SEC| image:: ../../Tool_bar/geom.svg
        

Règles d'agencement
-------------------

- Une *geometry downstream* doit être nécessairement définie sur le **nœud amont** du bief

- Une *geometry upstream* doit être nécessairement définie sur le **nœud aval** du bief

- Si un seul profil est défini sur un point courant, il peut être posé indifféremment en amont (*upstream*) ou en aval (*downstream).
  

Un bief peut ainsi être défini géométriquement comme suit :

    .. image:: _Reach_sections_building_rules.png
       :scale: 75%
