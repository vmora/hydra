Reach
##############


Définition
-----------

Bief filaire de cours d’eau/ vallée inondable

Données de l'élément
---------------------

.. image:: reach.png
   :width: 230px
 
Le bief filaire de cours d’eau est décrit par une **polyligne** représentant l’axe du cours d’eau. 

C’est la première entité à créer dans une schématisation 1D d’écoulement à surface libre.

Le bief est caractérisé par :

- pk0 = le pk du noeud amont 

- dx = pas d’espace entre deux nœuds de calcul (50 m par défaut)

