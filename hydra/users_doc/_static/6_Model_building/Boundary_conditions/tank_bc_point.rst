|TANK_BC| Tank BC
#############################


.. |TANK_BC| image:: tank_BC_button.png

Définition
-----------

Réservoir

Données de l'élément
------------------------

.. image:: tank_BC_ui.png
   :width: 450px

La courbe S(z) est définie sous forme tabulée. 

Nombre Np de points =  20 au maximum.

On peut spécifier une cote initiale de remplissage du bassin.

A noter que cet élément ne contrôle pas les dépassements de cote d’eau :

- Si z > z(np)  S = S(np)
- Si z < z(1) :  S = S(1).

Le  bassin peut ainsi se vider en-deçà de la cote de fond. C’est à l’utilisateur de correctement définir des cotes de seuil de liaisons connectés au bassin pour garantir que la cote dans le bassin reste toujours supérieure à z(1).

   
Commentaires
-----------------

L’objet *Tank BC* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

Il est le plus souvent utilisé pour modéliser les bassins de retention à l’intérieur d’une :doc:`station de gestion <../Stations/station_gestion>`.
