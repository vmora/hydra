|WEIR_BC| Weir BC
#############################


.. |WEIR_BC| image:: weir_BC_button.png

Définition
-----------

Prélèvement d’un débit selon une loi de seuil dénoyé.

Données de l'élément
------------------------

.. image:: weir_BC_ui.png
   :width: 400px

Commentaires
-----------------

L’objet *Weir BC* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.
