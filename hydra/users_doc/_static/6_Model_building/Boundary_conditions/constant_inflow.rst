|Q_BC| Constant inflow
#########################


.. |Q_BC| image:: constant_inflow.png

Définition
-----------

Injection d’un débit constant.

Données de l'élément
------------------------

.. image:: constant_inflow_ui.png
   :width: 250px

Commentaires
-----------------

L’objet *constant inflow* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

Son utilisation reste assez limitée en pratique car ses fonctionnalités sont englobées dans celles de l’objet :doc:`hydrograph <hydrograph>`, beaucoup plus riches.