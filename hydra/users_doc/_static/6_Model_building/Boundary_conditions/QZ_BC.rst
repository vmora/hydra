|QZ_BC| Q(Z) BC
#############################


.. |QZ_BC| image:: QZ_BC_button.png

Définition
-----------

Prélèvement d’un débit en fonction de la cote d’eau du containeur.

Données de l'élément
------------------------

.. image:: QZ_BC_ui.png
   :width: 450px

La courbe q(z) doit être strictement croissante et il faut imposer Q(1)=0. 10 points au maximum.

Commentaires
-----------------

L’objet *Q(Z) BC* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

Si le container est un nœud de branche filaire cet objet est généralement posé sur le nœud aval de la branche.
