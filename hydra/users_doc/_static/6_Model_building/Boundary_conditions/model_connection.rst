|MODEL_CONNECT| Model connection
#####################################


.. |MODEL_CONNECT| image:: model_connection.png

Définition
-----------

Objet de raccordement hydraulique entre deux modèles d’un même projet.

Données de l'élément
------------------------

.. image:: model_connection_ui.png
   :width: 450px

Deux options sont possibles dans le mode de raccordement en cascade (c’est dire quand les modèles sont exécutés les uns après les autres) 

1.	Downstream conditionz(q)

L’objet se trouve sur un nœud du modèle amont. Il faut dans ce cas définir une condition aval de type z(q), comme illustré ci-dessus.

2.	Hydrograph

L’objet se trouve sur un nœud du modèle aval. Aucune donnée attributaire n’est nécessaire dans ce cas. Cet objet est transformé en objet :doc:`hydrograph <hydrograph>` lors de la phase exécution des calculs, il reçoit alors l’hydrogramme stocké dans l’objet *modele connection* du modèle amont.

   
Commentaires
-----------------

L’objet *model connection* est traité comme une liaison uninodale lorsque l’on est en mode de calcul *cascade*. Il peut être posé sur n’importe quel container, à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.
