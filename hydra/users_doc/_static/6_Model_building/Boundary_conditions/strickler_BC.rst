|STRICKLER_BC| Strickler BC
#############################


.. |STRICKLER_BC| image:: strickler_button.png

Définition
-----------

Condition à la limite aval  de type écoulement en régime uniforme.

Données de l'élément
------------------------

.. image:: strickler_BC_ui.png
   :width: 300px

Equations
-------------

.. image:: strickler_BC_eq1.png
   :width: 200px


S : surface mouillée = bxH

R : rayon hydraulique = H

m : pente

H : hauteur d’eau . Z - Zfond

   
Commentaires
-----------------

L’objet *Strickler BC* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

Si le container est un nœud de branche filaire cet objet est généralement posé sur le nœud aval de la branche.
