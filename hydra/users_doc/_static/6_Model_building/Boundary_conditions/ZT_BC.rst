|ZT_BC| Z(T) BC
#############################


.. |ZT_BC| image:: ZT_BC_button.png

Définition
-----------

Imposition d’une cote variable dans le temps.

Données de l'élément
------------------------

.. image:: ZT_BC_ui.png
   :width: 450px

La courbe z(t) peut être définie dans l’élément ou dans un fichier externe déclaré dans le paramétrage du scénario actif. Le nombre de points np doit être au plus égal à 20 si la courbe z(t) est définie dans l’élément.

Quand l’option *cyclic* est activée, le signal affiché se répète dans les temps inférieurs à t(1) et supérieurs à t(np)  avec une période égale à t(np )-t(1).


Commentaires
-----------------

L’objet *Z(t) BC* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets :doc:`catchment <../Hydrology/catchment_node_point>` et :doc:`hydrology node <../Hydrology/hydrology_node_point>`.

Si le container est un nœud de branche filaire cet objet est généralement posé sur le nœud aval de la branche.
