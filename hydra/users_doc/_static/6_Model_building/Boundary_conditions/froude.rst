|FROUDE| Froude BC
#########################


.. |FROUDE| image:: froude_button.png

Définition
-----------

Condition à la limite aval  de type écoulement critique : Fr=1.

Données de l'élément
------------------------

.. image:: froude_ui.png
   :width: 250px

Aucune donnée attributaire.

Equations
-------------

.. image:: froude_eq1.png
   :width: 100px

Avec :

- S = section mouillée en C
- L = largeur au miroir


Commentaires
-----------------

L’objet *Froude BC* est une **liaison uninodale**. Il est utilisé comme condition limite aval d’une branche filaire.

Il ne peut être posé que sur un containeur de type :doc:`regard <../Network/manhole_point>` ou :doc:`river node <../River_free_surface_flow/river_node>`.

