|HY_SZQ| Geometric split singularity (*split_ZQ*)
####################################################

.. |HY_SZQ| image:: zq_split_hydrology_point.png

Définition
-----------

Calcul de répartition de débit entre une branche aval et une ou deux branches dérivées selon une loi « split QZ »

.. image:: splitQZ-fonct.png
   :width: 300px
   :align: center

Données de l'élément
---------------------
.. image:: splitQZ.png
   :width: 400px
   :align: center
..

Pour sélectionner la branche aval ou  une branche dérivée :
  * Cliquez sur le bouton *selection*
  * Puis cliquez dans le réseau sur le tronçon à sélectionner

Le débit dans la branche aval et dans chaque branche dérivée est calculé à partir de la sélection d’un des 4 types de lois suivantes :

.. image:: splitQZ-lois.png
..

La relation entre le débit amont et le débit aval dans chaque branche est calculée géométriquement comme suit :

.. image:: splitQZ-lois2.png
   :width: 460px

..

Commentaires
--------------

L’objet *geometric split singularity* doit être posé obligatoirement sur un nœud hydrologique.

