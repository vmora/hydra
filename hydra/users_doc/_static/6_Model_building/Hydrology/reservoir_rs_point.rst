|HY_RS| Bassin de stockage (*Reservoir*)
#######################################

.. |HY_RS| image:: reservoir_rs_point.png

Définition
-----------

Bassin de retenue équipé d’une surverse S et d’un ouvrage de vidange F à **débit constant**.

.. image:: reservoir_rs_fonct.png
   :width: 300px
   :align: center

   
Données de l'élément
---------------------

.. image:: reservoir_rs.png
   :width: 400px
   :align: center
..


Pour sélectionner la branche connectée à la vidange ou la branche connectée à la surverse :
  * Cliquez sur le bouton *selection*
  * Puis cliquez dans le réseau sur le tronçon à sélectionner

La géométrie du réservoir est définie par une courbe tabulée S(z).

Il faut de plus définir :
  * la cote d'eau initiale dans le bassin (*Z initial*)
  * le débit de fuite en m^3/s (*drainage discharge*) 

Si l’option de calcul **pollution** est activée dans la fenêtre de paramétrage du scénario, le bouton *pollution* sert à définir les taux d’abattement ou les concentrations résiduelles des différents paramètres de pollution dans le bassin.

Equations
--------------

La variation du niveau d'eau h dans le bassin est calculée au moyen de l'équation de continuité liant la surface au miroir et les débits entrant et sortant.

.. image:: reservoir_rs_equations.png
   :width: 250px

Avec :
  * S  = surface au miroir correspondant au niveau d'eau h dans le bassin,
  * hd = niveau d'eau dans le bassin correspondant à la cote de surverse
  * L = 10 m (valeur par défaut)

Commentaires
--------------

L’objet *reservoir* doit être posé obligatoirement sur un nœud hydrologique.