Bassin versant (*Catchment*)
############################

Définition
----------
Délimitation d’un  bassin versant et calcul d’un hydrogramme de ruissellement à partir de données pluviométriques.

Le **contour de bassin versant** |catchment_contour_polygon| `catchment contour` est une entité polygonale, elle  ne contient aucune donnée attributaire, c’est une donnée facultative.

Le **bassin versant** est représenté par le symbole |catchment_node_point|. Il contient toutes les données attributaires.

.. |catchment_node_point| image:: catchment_node_point.png
.. |catchment_contour_polygon| image:: catchment_contour_polygon.png


Données de l'élément
--------------------

.. image:: catchment.png
..
   
Il faut renseigner 4 types de données :
   
**1. Données générales du bassin versant**
   
	* Area (ha) : surface en hectares
	* Length (m) : longueur en mètres
	* Average slope (m/m) : pente moyenne
	* Impervious soil coefficient : coefficient d'imperméabilisation

	Le nom du contour de BV entourant l’objet BV est indiqué s’il existe à côté de "contour"

**2. Fonction de production de la pluie nette** (*Net flow production function*)
     
	Cinq  modes de calculs de la pluie nette sont proposés :

	* Coefficient de ruissellement constant
	* Méthode d’Horner
	* Méthode SCS
	* Méthode Holtan
	* Formulation alternative pour les sols perméables
	
	Les deux premiers modes de calculs s’appliquent à des bassins versants urbains ou à dominante imperméable, les trois autres s’appliquent à des sols perméables caractérisés par une forte infiltration dans le sous-sol.
   
**3. Fonction de production du ruissellement à partir du calcul de la pluie nette** (*Runoff production function*)

	Deux modes de calculs sont proposés :

	* La méthode du **réservoir linéaire** basée sur l’application d’une formule de temps de concentration : **Desbordes** (pour les bassins versants urbains), **Passini** ou **Giandotti** (pour les bassins versants ruraux)
	* La méthode de **l’hydrogramme unitaire** de type **Socose**. Il faut dans ce cas renseigner le temps de concentration et un coefficient de forme pour l’hydrogramme unitaire.
   
**4. Données annexes**

	* Débit de temps sec permanent à l’exutoire du BV (=0 par défaut)
	* Ecrêtement du débit de pointe (pas d’écrètement par défaut)


Commentaires
-------------

Les différentes formulations et les règles de sélection des différents paramètres sont précisées dans le manuel d’analyse modélisation hydrologique.

L’objet *contour de BV* |catchment_contour_polygon| `catchment contour` doit être défini uniquement si la pluie sélectionnée nécessite le calcul d’une lame d’eau moyenne attachée au BV : pluie *radar* ou lame d’eau définie à partir de données de pluviographes.

