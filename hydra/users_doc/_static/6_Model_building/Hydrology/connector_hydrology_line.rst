|HY_P| Connecteur hydrologique (*hydrology connector*)
#########################################################

  .. |HY_P| image:: connector_hydrology_line.png


Définition
-----------

Le connecteur hydrologique sert à **propager un hydrogramme** entre deux noeuds de calcul **sans déformation** ni **déphasage**.


Données de l'élément
---------------------
.. image:: hydrology-connector.png
..

Cet objet  ne contient aucune donnée attributaire.

Commentaires
--------------

Cet objet est de type *link*.

Son extrémité aval doit être connectée à un des deux objets suivants :
  * |HY_N| :doc:`un regard <hydrology_node_point>` (*hydrology node*),
  * un containeur hydraulique. Il est obligatoire dans ce cas que le container soit occupé par la singularité « hydrograph », faute de quoi l’objet connecteur ne peut pas être créé

    .. |HY_N| image:: hydrology_node_point.png