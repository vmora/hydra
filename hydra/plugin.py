# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
QGIS plugin
"""

import os.path
import traceback
import json
import resources_rc
import re
from subprocess import Popen, PIPE
from qgis.gui import QgsCollapsibleGroupBox
from qgis.core import QGis, QgsMapLayerRegistry, QgsProject, QgsPluginLayerRegistry, QgsCoordinateReferenceSystem
from PyQt4.QtCore import QSettings, QTranslator, QCoreApplication, QObject, Qt, QSize
from PyQt4.QtGui import QToolBar, QMenu, QAction, QIcon, QPixmap, QComboBox, QToolButton, QFileDialog, QGroupBox, QInputDialog, QLabel, QDockWidget, QMessageBox, QApplication, QPushButton
from hydra.project import Project
from hydra.gui import new_project_dialog as npdialog
from hydra.gui import new_model_dialog as nmdialog
from hydra.gui import project_manager_dialog as pjmanagerdialog
from hydra.gui.settings import HydraSettings
from hydra.gui.project_tree import ProjectTree
from hydra.gui.model_dock import ModelDock
from hydra.gui.long_profil_dock import LongProfilDock
from hydra.gui.time_control import TimeControl
from hydra.gui.toolbar import ToolBar
from hydra.gui.results_selector import ResultsSelector
from hydra.utility.log import LogManager
from hydra.utility.dll import deploy_dll
from hydra.utility.map_point_tool import create_polygon, create_line, create_point, Snapper, create_segment
from hydra.utility.qgis_utilities import QGisUIManager, QGisLogger
from hydra.utility.tables_properties import TablesProperties
from hydra.utility.settings_properties import SettingsProperties
from hydra.advanced_tools.import_shp.import_shapefile import ImportShapefile
from hydra.database import database as dbhydra
from hydra.versions.dump_restore import srid_from_dump, version_from_dump
from hydra.versions.dump_restore import import_model_in_project

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

_plugin_dir = os.path.dirname(__file__)
_hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")
_layer_icons_dir = os.path.join(_plugin_dir, "ressources", "layer_icons")
_log_file = os.path.join(_hydra_dir, 'hydra.log')
_desktop_dir = os.path.join(os.path.expanduser("~"), "Desktop")

class Hydra(QObject):
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        QObject.__init__(self)
        self.__iface = iface

        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            _plugin_dir,
            'i18n',
            '{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.__translator = QTranslator()
            self.__translator.load(locale_path)
            QCoreApplication.installTranslator(self.__translator)

        self.__project_menu = None
        self.__model_menu = None
        self.__hydro_menu = None
        self.__scenario_menu = None

        self.__toolbar = None
        self.__project_tree = None
        self.__model_dock = None
        self.__long_profil_dock = None
        self.__longi_profile_interface = None
        self.__time_control_profil_dock = None

        self.__currentproject = None

        self.__show_w15 = False

        self.__ui_manager = QGisUIManager(self.__iface)
        self.__log_manager = LogManager(QGisLogger(self.__iface), tr("Hydra"))

        # create hydra directory
        if not os.path.isdir(_hydra_dir):
            os.makedirs(_hydra_dir)

        # check settings file
        SettingsProperties.check_settings_file()

        # reset log file
        open(_log_file, 'w').close()

        deploy_dll(_plugin_dir, self.__log_manager)

        QgsProject.instance().readProject.connect(self.project_loaded)

    def project_loaded(self, dom):
        project_path = QgsProject.instance().readPath("./")
        if ".hydra/" in project_path:
            project_name = project_path.split(".hydra/")[1].split('/')[0]
            self.__currentproject = Project.load_project(project_name, self.__log_manager, self.__ui_manager)
            self.__currentproject.open_in_ui(True)
            self.__remove_dependant_menus()
            self.__refresh_ui_menus()

    def initGui(self):
        """create the menu entries and toolbar icons inside the QGIS GUI"""

        # cleanup the qgis interface (remove toolbars and menus)
        # for tool_bar in self.__iface.mainWindow().findChildren(QToolBar):
            # tool_bar.hide()
        # self.__iface.mainWindow().menuBar().clear()

        self.__iface.mainWindow().menuBar().addMenu(self.project_menu())
        self.__currentproject = None

    def __create_dependant_menus(self):
        for action in self.__project_menu.actions():
            if action.text() == tr("&Terrain"):
                for menu in [self.model_menu(), self.hydro_menu(), self.scenario_menu()]:
                    self.__project_menu.insertMenu(action, menu)

        self.__iface.mainWindow().addToolBar(self.toolbar())

        self.__iface.mainWindow().addDockWidget(Qt.LeftDockWidgetArea, self.project_tree())
        self.__iface.mainWindow().tabifyDockWidget(self.__iface.mainWindow().findChildren(QDockWidget, "Layers")[0], self.project_tree())

        self.__iface.mainWindow().addDockWidget(Qt.LeftDockWidgetArea, self.model_dock())
        self.__iface.mainWindow().tabifyDockWidget(self.__iface.mainWindow().findChildren(QDockWidget, "Layers")[0], self.model_dock())

        self.__iface.mainWindow().addDockWidget(Qt.LeftDockWidgetArea, self.long_profil_dock())
        self.__iface.mainWindow().tabifyDockWidget(self.__iface.mainWindow().findChildren(QDockWidget, "Layers")[0], self.long_profil_dock())

    def __refresh_ui_menus(self):
        if self.__currentproject!=None:
            if not self.__model_menu:
                self.__create_dependant_menus()
            else:
                self.__refresh_models_menu()
                if self.__project_tree:
                    self.__project_tree.refresh_content(self.__currentproject)
                if self.__toolbar:
                    self.__toolbar.refresh_content(self.__currentproject)
        else:
            self.__remove_dependant_menus()

    def project_menu(self):
        """returns the project menu, creates it if needed"""
        if not self.__project_menu:
            self.__project_menu = QMenu(tr("&Hydra"))
            self.__project_menu.setIcon(QIcon(os.path.join(_plugin_dir, 'ressources', 'images', 'hydra_logo.png')))
            self.__project_menu.addAction(tr("&New project")).triggered.connect(self.__project_new)
            self.__open_menu = self.__project_menu.addMenu(tr("&Open project"))
            self.__open_menu.aboutToShow.connect(self.__update_project_list)
            self.__project_menu.addAction(tr("Manage &projects")).triggered.connect(self.__project_manage)
            self.__project_menu.addSeparator()
            self.__project_menu.addAction(tr("&Terrain")).triggered.connect(self.__terrain_manager)
            self.__project_menu.addAction(tr("&Configurations")).triggered.connect(self.__config_manager)


            self.__advanced_tools_menu = self.__project_menu.addMenu(tr("&Advanced tools"))
            self.__advanced_tools_menu.addAction(tr("&Extract")).triggered.connect(self.__extract)
            self.__advanced_tools_menu.addAction(tr("&Anim-Eau")).triggered.connect(self.__animeau)
            self.__advanced_tools_menu.addAction(tr("&Crgeng")).triggered.connect(self.__crgeng)
            self.__advanced_tools_menu.addSeparator()
            self.__advanced_tools_menu.addAction(tr("&Import shapefile to database")).triggered.connect(self.__import_shapefile)
            self.__advanced_tools_menu.addAction(tr("&Import data from database")).triggered.connect(self.__import_work_table)

            self.__project_menu.addAction(tr("&Reload project layers")).triggered.connect(self.__project_layers_reload)
            self.__project_menu.addSeparator()
            self.__project_menu.addAction(tr("Documentation")).triggered.connect(self.__doc)
            self.__project_menu.addAction(tr("Settings")).triggered.connect(self.__settings)
            self.__project_menu.addAction(tr("About Hydra")).triggered.connect(self.__about)
        return self.__project_menu

    def __terrain_manager(self):
        from hydra.gui.terrain_manager import TerrainManager
        if self.__currentproject != None:
            terrain_manager = TerrainManager(self.__currentproject, self.__iface.mainWindow())
            terrain_manager.exec_()
            Project.reload_terrain_layers(self.__currentproject)
            self.__currentproject.save()

    def __config_manager(self):
        from hydra.gui.config_manager import ConfigManager
        if self.__currentproject != None:
            config_manager = ConfigManager(self.__currentproject, self.__iface.mainWindow())
            config_manager.exec_()
            self.__refresh_layers()
            self.__refresh_ui_menus()

    def __extract(self):
        from hydra.advanced_tools.extract import Extract
        if self.__currentproject != None:
            extract = Extract(self.__currentproject, self.__iface.mainWindow())
            extract.exec_()

    def __animeau(self):
        from hydra.advanced_tools.animeau import AnimEau
        if self.__currentproject != None:
            animeau = AnimEau(self.__currentproject, self.__iface.mainWindow())
            animeau.exec_()

    def __crgeng(self):
        from hydra.advanced_tools.crgeng import Crgeng
        if self.__currentproject != None:
            crgeng = Crgeng(self.__currentproject, self.__iface.mainWindow())
            crgeng.exec_()

    def __set_current_model(self):
        if self.__currentproject.get_current_model() is None\
                or self.sender().text() != self.__currentproject.get_current_model().name:
            self.__currentproject.set_current_model(self.sender().text())
            for act in self.__model_menu.findChildren(QAction):
                ft = act.font()
                ft.setBold(act == self.sender())
                act.setFont(ft)
        self.__refresh_ui_menus()

    def model_menu(self):
        """returns the model menu, creates it if needed"""
        if not self.__model_menu:
            self.__model_menu = QMenu(tr("&Models"))
            self.__models_menu = self.__model_menu.addMenu(tr("&Set current"))
            models = self.__currentproject.get_models()
            for model in models:
                action = self.__models_menu.addAction(model)
                action.triggered.connect(self.__set_current_model)
                if self.__currentproject.get_current_model() is None \
                        or self.__currentproject.get_current_model().name == model:
                    self.__currentproject.set_current_model(model)
                    ft = action.font()
                    ft.setBold(True)
                    action.setFont(ft)
            self.__model_menu.addAction(tr("&Add model")).triggered.connect(self.__model_new)
            self.__model_menu.addAction(tr("&Delete current model")).triggered.connect(self.__model_delete)
            self.__model_menu.addAction(tr("&Import model (sql)")).triggered.connect(self.__import_model_sql)
            self.__model_menu.addAction(tr("&Import model (csv)")).triggered.connect(self.__import_model_csv)
            self.__model_menu.addAction(tr("&Reload current model")).triggered.connect(self.__model_layers_reload)
        return self.__model_menu

    def __refresh_models_menu(self):
        self.__models_menu.clear()
        if self.__currentproject != None:
            models = self.__currentproject.get_models()
            for model in models:
                action = self.__models_menu.addAction(model)
                action.triggered.connect(self.__set_current_model)
                if self.__currentproject.get_current_model() is None \
                        or self.__currentproject.get_current_model().name == model:
                    self.__currentproject.set_current_model(model)
                    ft = action.font()
                    ft.setBold(True)
                    action.setFont(ft)

    def hydro_menu(self):
        """returns the hydrology menu, creates it if needed"""
        if not self.__hydro_menu:
            self.__hydro_menu = QMenu(tr("&Hydrology"))
            self.__hydro_menu.addAction(tr("&Dryflow sector")).triggered.connect(self.__dry_inflow_sectors_settings)
            self.__hydro_menu.addAction(tr("Dryflow &scenario")).triggered.connect(self.__dry_inflow_manager)
            self.__hydro_menu.addAction(tr("&Rain scenario")).triggered.connect(self.__rain_scenario)
        return self.__hydro_menu

    def scenario_menu(self):
        """returns the scenario menu, creates it if needed"""
        if not self.__scenario_menu:
            self.__scenario_menu = QMenu(tr("&Scenarios"))
            self.__scenario_menu.addAction(tr("&Settings")).triggered.connect(self.__scenario_manage)
            self.__scenario_menu.addAction(tr("&Grouping")).triggered.connect(self.__scenario_grouping)
            self.__scenario_menu.addAction(tr("&Import data (csv)")).triggered.connect(self.__import_project_csv)
        return self.__scenario_menu

    def project_tree(self):
        """returns the project_tree, creates it if needed"""
        if not self.__project_tree:
            self.__project_tree = ProjectTree(tr("Project items"), self.__currentproject, self.__refresh_ui_menus)
        return self.__project_tree

    def model_dock(self):
        """returns the modelisation dock, creates it if needed"""
        if not self.__model_dock:
            self.__model_dock = ModelDock(tr("Model elements"), self.__currentproject, self.__iface, self.__refresh_layers)
        return self.__model_dock

    def long_profil_dock(self):
        """returns the long profil dock, creates it if needed"""
        # flag test proto
        if not self.__time_control_profil_dock:
            self.__time_control_profil_dock=TimeControl(blocker=None)
        if not self.__long_profil_dock:
            self.__long_profil_dock = LongProfilDock(tr("Longitudinal profile"), self.__currentproject, self.__iface,self.__time_control_profil_dock)
        return self.__long_profil_dock

    def toolbar(self):
        """returns the toolbar, creates it if needed"""
        if not self.__toolbar:
            self.__toolbar = ToolBar(tr("Hydra toolbar"), self.__currentproject, self.__iface,
                                        self.__refresh_layers,
                                        self.__refresh_ui_menus)
        return self.__toolbar

    def __remove_dependant_menus(self):
        self.__remove_model_menus()

        if self.__model_menu:
            self.__model_menu.setParent(None)
            self.__model_menu = None
        if self.__hydro_menu:
            self.__hydro_menu.setParent(None)
            self.__hydro_menu = None
        if self.__scenario_menu:
            self.__scenario_menu.setParent(None)
            self.__scenario_menu = None

        if self.__toolbar:
            self.__iface.mainWindow().removeToolBar(self.__toolbar)
            self.__toolbar = None

    def __remove_model_menus(self):
        if self.project_tree:
            self.__iface.mainWindow().removeDockWidget(self.__project_tree)
            self.__project_tree = None

        if self.model_dock:
            self.__iface.mainWindow().removeDockWidget(self.__model_dock)
            self.__model_dock = None

        if self.__time_control_profil_dock:
            self.__time_control_profil_dock = None

        if self.long_profil_dock:
            self.__iface.mainWindow().removeDockWidget(self.__long_profil_dock)
            self.__long_profil_dock = None

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        self.__remove_dependant_menus()
        self.__project_menu.setParent(None)
        self.__project_menu = None
        QgsProject.instance().readProject.disconnect(self.project_loaded)
        self.__iface.newProject()

    def __refresh_layers(self, names=None):
        for layer in self.__iface.mapCanvas().layers():
            if (names is None) or (layer.name() in names) or (layer.name() == TablesProperties.get_properties()['invalid']['name']) or (layer.name() == TablesProperties.get_properties()['configured_current']['name']):
                layer.triggerRepaint()

    def __model_new(self):
        if self.__currentproject != None:
            new_model_dialog = nmdialog.NewModelDialog(self.__currentproject)
            new_model_dialog.exec_()
            self.__currentproject.save()
            self.__refresh_ui_menus()
        else:
            self.__log_manager.warning(tr("You need a new a project before adding a new model."))

    def __model_delete(self):
        if self.__currentproject is not None and self.__currentproject.get_current_model() is not None:
            confirm = QMessageBox(QMessageBox.Question, tr('Delete model'), tr('This will delete model {}. Proceed?').format(self.__currentproject.get_current_model().name), QMessageBox.Ok | QMessageBox.Cancel).exec_()
            if confirm == QMessageBox.Ok:
                self.__currentproject.delete_model(self.__currentproject.get_current_model().name)
                self.__currentproject.set_current_model(self.__currentproject.get_models()[0] if self.__currentproject.get_models() else None)
                # Bug where model layer of active model are unloaded after deletion of another model
                if self.__currentproject.get_current_model():
                    self.__currentproject.reload_model_layers(self.__currentproject.get_current_model().name)
                self.__currentproject.save()
        else:
            self.__log_manager.notice(tr("No model to delete found."))

    def __import_shapefile(self):
        if self.__currentproject is not None :
            title = tr('Import Shapefile')
            filter = tr('Shapefile (*.shp)')
            filepath = QFileDialog.getOpenFileName(None, title, _desktop_dir, filter)
            if filepath :
                text, ok = QInputDialog.getText(None, tr('Import shp data'),tr('Enter a name for the table (in schema "work")'))
                if ok:
                    postgrepath, pg = SettingsProperties.get_path('postgre'), SettingsProperties.get_postgre_settings()
                    pg_shp2pgsql = os.path.join(postgrepath, 'bin', 'shp2pgsql')
                    psql = os.path.join(postgrepath, 'bin', 'psql')
                    filepath = os.path.abspath(filepath)
                    argslist = [pg_shp2pgsql,'-I','-s',str(self.__currentproject.srid),'-W','latin1',filepath,'work.'+str(text),'|',psql,'-U',pg['user'],'-d',str(self.__currentproject.name)]

                    install_env = os.environ.copy()
                    install_env['PGPASSWORD'] = SettingsProperties.get_postgre_settings()['password']
                    process = Popen(argslist, stdin=PIPE, stdout=PIPE, stderr=PIPE, env=install_env, shell=True)
                    output, error = process.communicate()
                    exit_code = process.returncode

                    QMessageBox(QMessageBox.Information,tr('info'),tr("Data Imported into workspace as {} table".format(text)),QMessageBox.Ok).exec_()
        else:
            self.__log_manager.warning(tr("You need an open project to import shapefile."))

    def __import_work_table(self):
        if self.__currentproject is not None:
            content = self.__currentproject.execute("SELECT table_name FROM information_schema.tables WHERE table_schema='work' and table_name IN (SELECT f_table_name FROM public.geometry_columns)").fetchall()
            contentwork = [string for (string,) in content]
            tablename, ok = QInputDialog.getItem(None , "Select data from schema work", "Select the table to add", contentwork, 0, False)
            if ok and tablename :
                ImportShapefile(self.__currentproject,tablename, None).exec_()
        else:
            self.__log_manager.warning(tr("You need an open project to import shapefile."))

    def __project_layers_reload(self):
        if self.__currentproject != None:
            Project.reload_project_layers(self.__currentproject)
            Project.reload_terrain_layers(self.__currentproject)
            Project.reload_rain_layers(self.__currentproject)
            self.__currentproject.save()
            self.__refresh_ui_menus()
        else:
            self.__log_manager.warning(tr("You need an open project to reload its layers."))

    def __model_layers_reload(self):
        if self.__currentproject != None and Project.get_current_model(self.__currentproject) != None:
            current_model = Project.get_current_model(self.__currentproject).name
            Project.reload_model_layers(self.__currentproject, Project.get_current_model(self.__currentproject).name)
            Project.set_current_model(self.__currentproject, current_model)
            self.__currentproject.save()
            self.__refresh_ui_menus()
        else:
            self.__log_manager.warning(tr("You need a project and a model to reload a model."))

    def __project_new(self):
        new_project_dialog = npdialog.NewProjectDialog(self.__iface.mainWindow())
        if new_project_dialog.exec_():
            name, srid, workspace = new_project_dialog.get_result()
            if name != '':
                self.__currentproject = Project.create_new_project(workspace,
                        name, srid, self.__log_manager, self.__ui_manager)
                self.__currentproject.open_in_ui()
                self.__currentproject.save()
                self.__remove_dependant_menus()
                target_crs = QgsCoordinateReferenceSystem()
                target_crs.createFromSrid(srid)
                self.__iface.mapCanvas().setDestinationCrs(target_crs)
                self.__refresh_ui_menus()

    def __project_manage(self):
        project_manager_dialog = pjmanagerdialog.ProjectManagerDialog(self.__log_manager, self.__ui_manager,
                    self.__currentproject.name if self.__currentproject is not None else None, self.__iface.mainWindow())
        project_manager_dialog.exec_()
        if project_manager_dialog.new_current_project!=None \
                and self.__currentproject!=project_manager_dialog.new_current_project:
            if self.__currentproject != None:
                self.__currentproject.close()
            self.__currentproject=project_manager_dialog.new_current_project
            self.__currentproject.open_in_ui()
            target_crs = QgsCoordinateReferenceSystem()
            target_crs.createFromSrid(self.__currentproject.srid)
            self.__iface.mapCanvas().setDestinationCrs(target_crs)
            self.__remove_dependant_menus()
        if project_manager_dialog.qgis_current_project_deleted:
            self.__currentproject.close()
            self.__remove_dependant_menus()
            self.__currentproject=None
        self.__refresh_ui_menus()

    def __doc(self):
        os.system(os.path.abspath(os.path.join(_plugin_dir, "users_doc", "index.html")))

    def __settings(self):
        if self.__currentproject != None:
            if self.__toolbar:
                self.__toolbar.drop_results()

        hydra_settings = HydraSettings(project=self.__currentproject)
        hydra_settings.exec_()

        self.__refresh_layers((TablesProperties.get_properties()['coverage']['name'], TablesProperties.get_properties()['branch']['name'], TablesProperties.get_properties()['street_link']['name']))
        deploy_dll(_plugin_dir, self.__log_manager)

    def __about(self):
        about_file = os.path.join(_plugin_dir, 'license_short.txt')
        with open(about_file, 'r') as f:
            about_text = f.read()
        about_pic = QPixmap(os.path.join(_plugin_dir, 'ressources', 'images', 'hydra_logo.png')).scaledToHeight(32, Qt.SmoothTransformation)
        about_box = QMessageBox()
        about_box.setWindowTitle(tr('About Hydra'))
        about_box.setIconPixmap(about_pic)
        about_box.setTextFormat(Qt.RichText)
        about_box.setText(about_text)
        about_box.exec_()

    def __dry_inflow_sectors_settings(self):
        from hydra.gui.dry_inflow_sector_settings import DryInflowSectorSettings
        if self.__currentproject != None:
            dry_inflow = DryInflowSectorSettings(self.__currentproject)
            dry_inflow.exec_()
        self.__refresh_ui_menus()

    def __dry_inflow_manager(self):
        from hydra.gui.dry_inflow_manager import DryInflowManager
        if self.__currentproject != None:
            dry_inflow = DryInflowManager(self.__currentproject)
            dry_inflow.exec_()
        self.__refresh_ui_menus()

    def __rain_scenario(self):
        from hydra.gui.rain_manager import RainManager
        if self.__currentproject != None:
            rain = RainManager(self.__currentproject)
            rain.exec_()
            Project.reload_rain_layers(self.__currentproject)
            self.__currentproject.save()
        self.__refresh_ui_menus()

    def __scenario_manage(self):
        from hydra.gui.scenario_manager import ScenarioManager
        if self.__currentproject != None:
            if self.__toolbar:
                self.__toolbar.drop_results()
            scenario = ScenarioManager(self.__currentproject)
            current_scn = self.__currentproject.get_current_scenario()
            if current_scn:
                scenario.select_scenario_by_name(current_scn[1])
            scenario.exec_()
        self.__refresh_ui_menus()

    def __scenario_grouping(self):
        from hydra.gui.scenario_grouping import ScenarioGrouping
        if self.__currentproject != None:
            dialog = ScenarioGrouping(self.__currentproject)
            dialog.exec_()

    def __import_model_sql(self):
        from PyQt4.QtGui import QFileDialog
        title = tr('Import hydra dump file')
        filter = tr('Csv files (*.sql)')
        file_name = QFileDialog.getOpenFileName(None, title, _desktop_dir, filter)
        if file_name:
            models=[]
            with open(file_name, 'r') as fil:
                for line in fil:
                    match = re.search(r"CREATE SCHEMA (\w+);", line)
                    if match:
                        name = match.group(1)
                        if name not in ['hydra', 'project', 'public']:
                            models.append(name)

            model=None
            if len(models)==1:
                model=models[0]
            elif len(models)>1:
                m, ok = QInputDialog.getItem(None, "Model selection", "Select the model you want to add to project {}".format(self.__currentproject.name), models, 0, False)
                if ok and m:
                    model=m
            if model:
                confirm = QMessageBox(QMessageBox.Question, tr('Import model'), tr('This will import model {} from file {} into project {}. Proceed?'.format(model, os.path.basename(file_name), self.__currentproject.name)), QMessageBox.Ok | QMessageBox.Cancel).exec_()
                if confirm == QMessageBox.Ok:
                    file_srid = srid_from_dump(file_name)
                    file_version = version_from_dump(file_name)
                    if file_srid != '' and file_version != '':
                        if file_srid != self.__currentproject.srid:
                            self.__log_manager.warning("File {} and project {} SRIDs do not match".format(file_name, self.__currentproject.name))
                            return
                        if file_version != self.__currentproject.get_version():
                            self.__log_manager.warning("File {} and project {} versions do not match".format(file_name, self.__currentproject.name))
                            return
                        self.__currentproject.add_new_model(model)
                        QApplication.setOverrideCursor(Qt.WaitCursor)
                        QApplication.processEvents()
                        exit_code, output, error = import_model_in_project(self.__currentproject.name, model, file_name)
                        QApplication.restoreOverrideCursor()
                        if exit_code == 0:
                            self.__currentproject.set_current_model(model)
                            QMessageBox.information(None, tr('Import successfull'),
                                        tr('Model {} successfully exported into {}.').format(model, self.__currentproject.name), QMessageBox.Ok)
                        else:
                            self.__currentproject.delete_model(model)
                            QMessageBox.critical(None, tr('Import error'),
                                        tr('Error importing model {} into {}. See hydra.log file.').format(model, self.__currentproject.name), QMessageBox.Ok)
                            self.__log_manager.error('SQL model import error', '{e}\n{o}'.format(e=error, o=output))
                        self.__refresh_layers()
                        self.__refresh_ui_menus()
                    else:
                        self.__log_manager.notice("File {} is not a valid hydra sql dump file".format(file_name))


    def __import_model_csv(self):
        from PyQt4.QtGui import QFileDialog
        from PyQt4.QtCore import QCoreApplication
        from hydra.database.import_model import import_model_csv
        from hydra.gui.text_info import TextInfo
        title = tr('Import csv file')
        filter = tr('Csv files (*.csv)')
        fileName = QFileDialog.getOpenFileName(None, title, _desktop_dir, filter)
        if fileName:
            model_name = os.path.split(fileName)[1][:-4]
            try:
                def log_info(info, info_form):
                    if info_form is not None:
                        info_form.textInfos.append(info)
                        QApplication.processEvents()

                info_form = TextInfo(self.__iface.mainWindow())
                info_form.show()
                info_form.textInfos.clear()
                QApplication.setOverrideCursor(Qt.WaitCursor)
                QApplication.processEvents()
                if model_name in self.__currentproject.get_models():
                    self.__currentproject.delete_model(model_name)
                log_info("Creating model...", info_form)
                self.__currentproject.add_new_model(model_name)
                self.__currentproject.set_current_model(model_name)
                log_info("Import model...", info_form)
                import_model_csv(fileName, self.__currentproject, self.__currentproject.get_current_model(), model_name, self.__log_manager, info_form)
                log_info("Save imported model...", info_form)
                self.__currentproject.commit()
                self.__refresh_layers()
                self.__refresh_ui_menus()
                if info_form:
                    info_form.close()
                self.__log_manager.notice("{f} imported in model {m}".format(f=fileName,m=model_name))
                QApplication.restoreOverrideCursor()
                QMessageBox(QMessageBox.Information, tr('Import model'), tr('Importation of model {} finished.'.format(model_name)), QMessageBox.Ok).exec_()
            except Exception as e:
                self.__currentproject.rollback()
                QApplication.restoreOverrideCursor()
                traceback.print_exc()
                raise e

    def __import_project_csv(self):
        from PyQt4.QtGui import QFileDialog
        from PyQt4.QtCore import QCoreApplication
        from hydra.database.import_project import import_project_csv
        from hydra.gui.text_info import TextInfo
        title = tr('Import csv file')
        filter = tr('Csv files (*.csv)')
        fileName = QFileDialog.getOpenFileName(None, title, _desktop_dir, filter)
        if fileName:
            try:
                info_form = TextInfo(self.__iface.mainWindow())
                info_form.show()
                info_form.textInfos.clear()
                QApplication.setOverrideCursor(Qt.WaitCursor)
                QApplication.processEvents()
                import_project_csv(fileName, self.__currentproject, self.__log_manager, info_form)
                self.__currentproject.commit()
                self.__refresh_ui_menus()
                if info_form:
                    info_form.close()
                self.__log_manager.notice("{f} imported as project data".format(f=fileName))
                QApplication.restoreOverrideCursor()
                QMessageBox(QMessageBox.Information, tr('Import project data'), tr('Importation of datas from file {} finished.'.format(fileName)), QMessageBox.Ok).exec_()
            except Exception as e:
                self.__currentproject.rollback()
                QApplication.restoreOverrideCursor()
                traceback.print_exc()
                raise e

    def __open_project(self):
        project_name = self.sender().text()
        workspace = dbhydra.get_project_metadata(project_name)[2]
        project_dir = os.path.join(workspace, project_name)

        if not os.path.isdir(project_dir):
            msgBox = QMessageBox()
            msgBox.setWindowTitle(tr('Project directory not found'))
            msgBox.setIcon(QMessageBox.Question)
            msgBox.setText(tr('Directory "{p}" not found in {ws}. Select another workspace (directory containing the project folder) or create a new folder for project {p} in {ws}?').format(p=project_name, ws=workspace))
            msgBox.addButton(QPushButton('Select workspace'), QMessageBox.YesRole)
            msgBox.addButton(QPushButton('Create folder'), QMessageBox.NoRole)
            ret = msgBox.exec_()
            if ret == 0: #YesRole
                dirName = QFileDialog.getExistingDirectory(None, tr('Select directory'), workspace)
                if dirName:
                    dbhydra.change_project_workspace(project_name, dirName)

        self.__currentproject = Project.load_project(project_name, self.__log_manager, self.__ui_manager)
        self.__currentproject.open_in_ui()
        target_crs = QgsCoordinateReferenceSystem()
        target_crs.createFromSrid(self.__currentproject.srid)
        self.__iface.mapCanvas().setDestinationCrs(target_crs)
        self.__remove_dependant_menus()
        self.__refresh_ui_menus()

    def __update_project_list(self):
        self.__open_menu.clear()
        for project in sorted(dbhydra.get_projects_list()):
            act = self.__open_menu.addAction(project)
            act.triggered.connect(self.__open_project)
            if self.__currentproject and self.__currentproject.name == project:
                ft = act.font()
                ft.setBold(True)
                act.setFont(ft)