# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os.path
from subprocess import Popen, PIPE
from qgis.gui import QgsCollapsibleGroupBox
from hydra.utility.settings_properties import SettingsProperties

def deploy_to_postgres(plugin_dir, pg_dir, log_manager):
    if os.path.isdir(SettingsProperties.get_path('postgre')):
        if os.path.isfile(os.path.join(SettingsProperties.get_path('python'), 'pythonw.exe')):
            install_args = [os.path.join(SettingsProperties.get_path('python'), 'pythonw.exe'),
                            os.path.join(plugin_dir, 'server', 'install.py'),
                            pg_dir]
            install_env = os.environ.copy()
            install_env['PYTHONHOME'] = str(SettingsProperties.get_path('python'))
            install_process = Popen(install_args, stdin=PIPE, stdout=PIPE, stderr=PIPE, env=install_env)
            out, err = install_process.communicate()
            if not err:
                log_manager.notice('Postgre extension hydra installed in {}.'.format(pg_dir))
            elif "PermissionError" in err:
                #PermissionError: [Errno 13] Permission denied:
                log_manager.warning('Make sure you have the administrator rights to directory: {}.'.format(pg_dir))
            else:
                log_manager.error('Error while installing PostGRE hydra extension. Check "user\.hydra\hydra.log" file.',
                                        """postgre hydra extension install\ndirectory: {}\ncommand: {}\nenvironment: {}\nstandard output: {}\nstandard error: {}
                                        """.format(pg_dir, install_args, install_env, out, err))
    else:
        log_manager.error('Error in Postgre path', 'Error with path to PostgreSQL repository in setting')

def deploy_to_python3(plugin_dir, log_manager):
    if os.path.isdir(SettingsProperties.get_path('python')):
        py_args = [os.path.join(SettingsProperties.get_path('python'), 'pythonw.exe'),
                        os.path.join(plugin_dir, 'server', 'setup.py'),
                        'install']
        py_env = os.environ.copy()
        py_env['PYTHONHOME'] = str(SettingsProperties.get_path('python'))
        py_process = Popen(py_args, stdin=PIPE, stdout=PIPE, stderr=PIPE, env=py_env, cwd=os.path.join(plugin_dir, 'server'))
        py_out, py_err = py_process.communicate()
        py_code = py_process.returncode
        if 'Finished processing dependencies' in py_out:
            log_manager.notice('Python 3 extension hydra installed in {}.'.format(py_env['PYTHONHOME']))
        elif "PermissionError" in py_err:
            #PermissionError: [Errno 13] Permission denied:
            log_manager.warning('Make sure you have the administrator rights to directory: {}.'.format(os.path.join(py_env['PYTHONHOME'], 'Lib')))
        else:
            log_manager.error('Error while installing Python3 hydra extension. Check "user\.hydra\hydra.log" file.',
                                    """python3 hydra extension install\ncommand: {}\nenvironment: {}\nstandard output: {}\nstandard error: {}
                                    """.format(py_args, py_env, py_out, py_err))
    else:
        log_manager.error('Error in Python3 path', 'Error with path to Python 3 repository in setting')

def deploy_dll(plugin_dir, log_manager):
    if os.name != 'posix' and SettingsProperties.local():
        # hydra dll for postgre
        pg_dir = os.path.join(SettingsProperties.get_path('postgre'), 'share', 'extension')
        deploy_to_postgres(plugin_dir, pg_dir, log_manager)

        # hydra dll for python
        deploy_to_python3(plugin_dir, log_manager)

if __name__ == "__main__":

    from hydra.utility.log import LogManager, ConsoleLogger

    log = LogManager(ConsoleLogger(), "Hydra")
    plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

    deploy_dll(plugin_dir, log)
