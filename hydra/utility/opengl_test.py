# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import numpy
import time
import math
import os
#import tripy this is buggy
from shapely import wkt
from itertools import chain, islice
from math import log, ceil
from qgis.core import *
from PyQt4.QtOpenGL import QGLPixelBuffer, QGLFormat, QGL
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from OpenGL.GL import *
from OpenGL.arrays import vbo
from OpenGL.GL import shaders
import seidel

dir = os.path.dirname(__file__)

def roundUpSize(size):
    """return size roudup to the nearest power of 2"""
    return QSize(pow(2, ceil(log(size.width())/log(2))),
                 pow(2, ceil(log(size.height())/log(2))))


def chunks(iterable, size=10):
    """Yield successive n-sized chunks from l."""
    iterator = iter(iterable)
    for first in iterator:
        yield list(chain([first], islice(iterator, size - 1)))

if __name__ == "__main__":
    import sys
    from qgis.core import *

    """
    ogl_test.py image_destination
    """

    app = QgsApplication(sys.argv, True)
    QgsApplication.setPrefixPath('/usr/local', True)
    QgsApplication.initQgis()

    if len(sys.argv) == 2:
        dest = sys.argv[1]
    else:
        dest = os.path.join(dir, 'img_test.jpg')

    fmt = QGLFormat()
    fmt.setAlpha(True)
    size = QSize(512, 512)
    pixBuf = QGLPixelBuffer(size, fmt)
    assert pixBuf.format().alpha()
    pixBuf.makeCurrent()
    pixBuf.bindToDynamicTexture(pixBuf.generateDynamicTexture())

    glClearColor(0., 0., 0., 0.)
    glDisableClientState(GL_VERTEX_ARRAY)
    glDisable(GL_TEXTURE_1D)
    glShadeModel(GL_FLAT)
    glClear(GL_COLOR_BUFFER_BIT)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glColor4f(1., 0., 0, 1.)

    glBegin(GL_TRIANGLES)
    glVertex3f(-.5, -.5, 0)
    glVertex3f(.5, -.5, 0)
    glVertex3f(-.5, .5, 0)
    glEnd()

    pixBuf.doneCurrent()
    pixBuf.toImage().save(dest)
    print 'Written to {}'.format(dest)

    exit(0)