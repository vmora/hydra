# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""get rid of useless test projects

USAGE
    hydra.utility.project_cleaner [-t, -h]
        deletes all prjects ending with "_test"

OPTIONS
    -t, --template
        also delete template_project

    -h, --help
        print this help

    -a, --all
        delete all databases

"""

from __future__ import absolute_import # important to read the doc !
import os
import sys
import getopt
import string
import psycopg2
from psycopg2.extras import LoggingConnection
import logging
import shutil
from ..utility.settings_properties import SettingsProperties
from ..database import database

current_dir = os.path.dirname(__file__)
hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")
db = SettingsProperties.get_db()
pg = SettingsProperties.get_postgre_settings()

def get_directories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

def remove_project(project_name):
    con = psycopg2.connect(database="postgres", host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
    con.set_isolation_level(0)
    cur = con.cursor()

    working_dir = hydra_dir
    if project_name in database.get_projects_list():
        try:
            con_project = psycopg2.connect(database=project_name, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
            cur_project = con_project.cursor()
            cur_project.execute("""select workspace from hydra.metadata""")
            working_dir, = cur_project.fetchone()
            cur_project.close()
            # close all remaining opened connection to this database if any
            cur.execute("select pg_terminate_backend(pg_stat_activity.pid) \
                        from pg_stat_activity \
                        where pg_stat_activity.datname='"+project_name+"';")
            cur.execute("drop database %s"%(project_name))
            print 'Database "', project_name, '" dropped'
        except:
            print "Cannot drop DB {}".format(project_name)
    # if directory still exist, remove it
    if os.path.exists(os.path.join(working_dir, project_name)):
        shutil.rmtree(os.path.join(working_dir, project_name))
        print 'Directory "', os.path.join(working_dir, project_name), '" deleted'
    con.commit()

def clean(all=False):
    for project in database.get_projects_list():
        if (project[-5:] == '_test' or all):
            remove_project(project)
    for directory in get_directories(hydra_dir):
        if (directory[-5:] == '_test' or directory not in database.get_projects_list() or all):
            remove_project(directory)

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hta",
            ["help", "template", "all"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if "-t" in optlist or "--template" in optlist:
    remove_project('template_project')
    clean()

if "-a" in optlist or "--all" in optlist:
    valid = {"yes": True, "y": True, "ye": True,"no": False, "n": False, '':False}
    sys.stdout.write("Are you sure you want to delete all databases on server {host}, port {port} ? [y/N]".format(host=db['host'], port=db['port']))
    choice = raw_input().lower()
    if choice in valid:
        if valid[choice]:
            clean(all=True)
    else:
        sys.stdout.write("Answer not clear.\n")


