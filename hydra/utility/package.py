# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
packaging script for the hydra project

USAGE
    package.py [-h, -i, -u] [directory],

OPTIONS
    -h, --help
        print this help

    -i, --install [directory]
        install the package in the .qgis2 directory, if directory is ommited,
        install in the QGis plugin directory
        deploys dll (postgresql and python3)

    -u, --uninstall
        uninstall (remove) the package from .qgis2 directory

    -d, --deploy
        deploy the package to qgis repository directory
        possibility to add version: python -m hydra.utility.package -d 1.0

    -s, --setup
        compiles a new setup binary

    -k, --kernel
        lightweigth zip (for mail) that excludes the kernel directory and the setup directory

"""

import os
import zipfile
import re
import shutil
import getpass
import time
import json
import subprocess
from hydra.utility.log import LogManager, ConsoleLogger

log_manager = LogManager(ConsoleLogger(), "Hydra")

qgis_plugin_dir = os.path.abspath(os.path.join(os.path.expanduser("~"), ".qgis2", "python", "plugins"))
hydra_dir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
inno_setup_dir = os.path.abspath("C:\Program Files (x86)\Inno Setup 5")
repository_dir = os.path.abspath("L:/Logiciels/Hydra/repository/")

meta_file = os.path.join(hydra_dir, "metadata.txt")
changelog_file = os.path.join(hydra_dir, "..", "CHANGELOG")

zipname = "hydra"
zipext = ".zip"

def run_tests():
    out, err = subprocess.Popen(["python", "-m", "hydra.test"]).communicate()
    if err:
        log_manager.error("Tests failed.", err)
        exit(1)

def deploy(zip_filename, new_version):
    file_name = repository_dir + zipname + "_" + str(new_version) + zipext
    __drop(file_name)
    shutil.copyfile(zip_filename,file_name)
    log_manager.notice("Copied ZIP in {}".format(file_name))
    xmlfile = os.path.join(repository_dir, "hydra.xml")
    with open(xmlfile, 'r') as file:
        data = file.readlines()
    data[3]= "  <pyqgis_plugin name='hydra' version='" + str(new_version) + "'>\n"
    data[5]= "    <version>" + str(new_version) + "</version>\n"
    data[10]= "    <download_url>file:///" + file_name +  "</download_url>\n"
    data[11]= "    <uploaded_by>" + getpass.getuser() + "</uploaded_by>\n"
    data[12]= "    <create_date>" + time.strftime("%x") + "</create_date>\n"
    with open(xmlfile, 'w') as file:
        file.writelines( data )
    log_manager.notice("Updated XML file in {}".format(repository_dir))
    log_manager.notice("Deployed version {} in {}".format(new_version,repository_dir))

def compile_setup(zip_filename):
    setup_dir = os.path.abspath(os.path.join(os.path.dirname(hydra_dir), 'setup'))
    target_zip_file = os.path.join(setup_dir, 'hydra_setup', zipname+zipext)
    __drop(target_zip_file)
    target_bin_name = 'setup_hydra_'+__plugin_version()
    __drop(os.path.abspath(os.path.join(setup_dir, target_bin_name+'.exe')))
    shutil.copyfile(zip_filename,target_zip_file)
    out, err = subprocess.Popen([os.path.join(inno_setup_dir, 'iscc.exe'), os.path.join(setup_dir,'setup_hydra.iss'), '/Qp', '/F'+target_bin_name]).communicate()
    if err :
        log_manager.error("Compiling failed.", err)
    log_manager.notice("Setup compiled in {}.".format(os.path.abspath(os.path.join(setup_dir, target_bin_name+'.exe'))))

def update_meta(newversion):
    with open(meta_file, 'r') as file:
        data = file.readlines()
    if newversion is None:
        newversion = float(data[14].split('=')[1]) + 0.01
    data[14]= "version="+str(newversion)+"\n"
    data[31]= "  "+str(newversion)+"\n"
    with open(meta_file, 'w') as file:
        file.writelines( data )
    log_manager.notice("Metadata file updated to {}".format(data[14].rstrip().replace("="," ")))
    return newversion

def update_changelog():
    with open(meta_file, 'r') as file:
        data = file.readlines()
    with open(changelog_file, 'w') as file:
        file.writelines(data[31:-1])
    log_manager.notice("Changelog for version {} updated".format(data[31].strip()))

def uninstall(install_dir):
    target_dir = os.path.join(install_dir, "hydra")
    __drop(target_dir)
    log_manager.notice("Removed plugin from {}".format(install_dir))

def install(install_dir, zip_filename):
    uninstall(install_dir)
    with zipfile.ZipFile(zip_filename, "r") as z:
        z.extractall(install_dir)
    log_manager.notice("Installed plugin in {}".format(install_dir))

def zip_(zip_filename, kernel=True):
    """the zip file doesn't include tests, demos or docs"""
    with zipfile.ZipFile(zip_filename, 'w') as package:
        for root, dirs, files in os.walk(hydra_dir):
            users_doc = os.path.realpath(root).startswith(os.path.realpath(os.path.join(hydra_dir, 'users_doc')))
            if (not re.match(r".*(test_data|docs|build|.*.egg-info).*", root) and (kernel or not re.match(r".*(kernel|setup).*", root))) or users_doc:
                for file_ in files:
                    if (re.match(r".*\.(plu|py|txt|ui|json|sql|png|svg|qml|exe|hvc|dll|bat|control|html|js|css|gif)$", file_) \
                            and not re.match(r".*(_test|_guitest|_demo|_solotest)\.py", file_) and not re.match(r".*(_test)\.sql", file_) \
                            and not re.match(r"(requirements.txt)", file_) and not re.match(r"(package.py|project_cleaner.py|test.py|icons_generator.py)", file_)) \
                            or users_doc:
                        fake_root = root.replace(hydra_dir, "hydra")
                        package.write(os.path.join(root, file_),
                                      os.path.join(fake_root, file_))
    log_manager.notice("Compressed zip file {}".format(zip_filename))

def __drop(name):
    if os.path.isfile(name):
        os.remove(name)
        log_manager.notice("Del file {}".format(name))
    if os.path.isdir(name):
        shutil.rmtree(name)
        log_manager.notice("Del folder {}".format(name))

def __plugin_version():
    with open(meta_file, 'r') as file:
        data = file.readlines()
    return data[14].split('=')[1].rstrip()

if __name__ == "__main__":

    import getopt
    import sys
    from hydra.utility.dll import deploy_dll

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "hiudsk",
                ["help", "install", "uninstall", "deploy", "setup", "kernel"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    if "-d" in optlist or "--deploy" in optlist:
        if len(args) == 1:
            new_version = args[0]
        else:
            new_version = None
        run_tests()
        new_version = update_meta(new_version)
        update_changelog()

    if "-s" in optlist or "--setup" in optlist:
        run_tests()

    kernel = not ("-k" in optlist or "--kernel" in optlist)

    zip_filename = os.path.join(os.path.dirname(os.path.dirname(__file__)), zipname+zipext)
    zip_(zip_filename, kernel)

    if "-u" in optlist or "--uninstall" in optlist or "-i" in optlist or "--install" in optlist:
        install_dir = qgis_plugin_dir if len(args)==0 else args[0]

    if "-u" in optlist or "--uninstall" in optlist:
        uninstall(install_dir)

    if "-i" in optlist or "--install" in optlist:
        install(install_dir, zip_filename)
        deploy_dll(plugin_dir, log_manager)

    if "-d" in optlist or "--deploy" in optlist:
        deploy(zip_filename, new_version)

    if "-s" in optlist or "--setup" in optlist:
        compile_setup(zip_filename)