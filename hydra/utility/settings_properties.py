# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import json
import os

_hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")
_settings_file = os.path.join(_hydra_dir, "hydra.config")

class ConnexionError(Exception):
    pass

class SettingsProperties():
    __settings_dic = None

    @staticmethod
    def get_snap():
        if SettingsProperties.__settings_dic is None:
            with open (_settings_file) as fi:
                SettingsProperties.__settings_dic = json.load(fi)
        return SettingsProperties.__settings_dic['snap']

    @staticmethod
    def get_db(param=None):
        if SettingsProperties.local() == False:
            return SettingsProperties.get_server_db(param)
        else:
            return SettingsProperties.get_local_db(param)

    @staticmethod
    def local():
        if SettingsProperties.__settings_dic is None:
            with open (_settings_file) as fi:
                SettingsProperties.__settings_dic = json.load(fi)
        if SettingsProperties.__settings_dic['local'] == 1:
            return True
        else:
            return False

    @staticmethod
    def get_server_db(param=None):
        if SettingsProperties.__settings_dic is None:
            with open (_settings_file) as fi:
                SettingsProperties.__settings_dic = json.load(fi)
        if param:
            return SettingsProperties.__settings_dic['server_db'][param]
        else:
            return SettingsProperties.__settings_dic['server_db']

    @staticmethod
    def get_local_db(param=None):
        if SettingsProperties.__settings_dic is None:
            with open (_settings_file) as fi:
                SettingsProperties.__settings_dic = json.load(fi)
        if param == 'host':
            return '127.0.0.1'
        elif param:
            return SettingsProperties.__settings_dic['local_db'][param]
        else:
            local_dict = SettingsProperties.__settings_dic['local_db']
            local_dict['host'] = '127.0.0.1'
            return local_dict

    @staticmethod
    def get_path(app=None):
        if SettingsProperties.__settings_dic is None:
            with open (_settings_file) as fi:
                SettingsProperties.__settings_dic = json.load(fi)
        if app:
            return SettingsProperties.__settings_dic['path'][app]
        else:
            return SettingsProperties.__settings_dic['path']

    @staticmethod
    def get_postgre_settings():
        pg_pass_conf = os.path.join(os.getenv('APPDATA'), 'postgresql', 'pgpass.conf') \
                if os.name != 'posix' else\
                os.path.join(os.getenv('HOME'), '.pgpass')

        with open(pg_pass_conf) as fi:
            for line in fi:
                spl = line.rstrip().split(':')
                if  len(spl) == 5 \
                        and spl[0] in (SettingsProperties.get_db('host'), '*') \
                        and spl[1] in (str(SettingsProperties.get_db('port')), '*'):
                    return {'user':spl[-2], 'password':spl[-1]}

        raise ConnexionError("No connexion login found in file {} for host: {} and port: {}.".format(pg_pass_conf, SettingsProperties.get_db('host'), SettingsProperties.get_db('port')))

    @staticmethod
    def get_timestep():
        if SettingsProperties.__settings_dic is None:
            with open (_settings_file) as fi:
                SettingsProperties.__settings_dic = json.load(fi)
        return SettingsProperties.__settings_dic['timestep']

    @staticmethod
    def get_velocity_style(param=None):
        if SettingsProperties.__settings_dic is None:
            with open (_settings_file) as fi:
                SettingsProperties.__settings_dic = json.load(fi)
        if param:
            return SettingsProperties.__settings_dic['velocity_arrows'][param]
        else:
            return SettingsProperties.__settings_dic['velocity_arrows']

    @staticmethod
    def write_settings_file(snap, db_server_port, db_server_host, local, db_local_port, path_postgre, path_python, timestep, velocity_scale, velocity_classes, velocity_colors):
        dic = SettingsProperties.__write_settings_dic(snap, db_server_port, db_server_host, local, db_local_port, path_postgre, path_python, timestep, velocity_scale, velocity_classes, velocity_colors)
        with open (_settings_file, "w") as fo:
            json.dump(dic, fo, indent=4)
        SettingsProperties.__reset_settings()

    @staticmethod
    def check_settings_file():
        """
        complete settings file with default values or creates it if needed
        to be called at the start of plugin only (no need to reset)
        """
        default_settings = SettingsProperties.__write_settings_dic()
        if not os.path.isfile(_settings_file):
            file_settings = default_settings
        else:
            with open (_settings_file) as fi:
                try:
                    file_settings = json.load(fi)
                    for key1 in default_settings.keys():
                        if key1 not in file_settings.keys():
                            file_settings[key1] = default_settings[key1]
                        elif type(default_settings[key1])==type({}):
                            for key2 in default_settings[key1].keys():
                                if key2 not in file_settings[key1].keys():
                                    file_settings[key1][key2] = default_settings[key1][key2]
                except:
                    file_settings = default_settings
        with open (_settings_file, "w") as fo:
            json.dump(file_settings, fo, indent=4)

    @staticmethod
    def __write_settings_dic(snap=20,
                                db_server_port=5432, db_server_host='192.168.16.211',
                                local=True, db_local_port=5432,
                                path_postgre='C:\\Program Files\\PostgreSQL\\9.6',
                                path_python='C:\\edb\\languagepack-9.6\\x64\\Python-3.3',
                                timestep=1,
                                velocity_scale=25,
                                velocity_classes=[0, 0.25, 0.5, 1, 2],
                                velocity_colors=[[255, 255, 178, 255],
                                                 [254, 183,  81, 255],
                                                 [245,  86,  41, 255],
                                                 [189,   0,  36, 255],
                                                 [139,   0,  36, 255]]):

        """generated a settings dict with possibly default values"""
        loc = 1 if local else 0
        dict ={'local': loc,
                    'server_db': {
                        'port': db_server_port,
                        'host': db_server_host
                        },
                    'local_db': {
                        'port': db_local_port
                        },
                    'path': {
                        'postgre': path_postgre,
                        'python': path_python
                        },
                    'snap': snap,
                    'timestep': timestep,
                    'velocity_arrows': {
                        'scale': velocity_scale,
                        'classes': velocity_classes,
                        'colors': velocity_colors
                        }
                    }
        return dict

    @staticmethod
    def __reset_settings():
        SettingsProperties.__settings_dic = None

