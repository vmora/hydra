# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from settings_properties import SettingsProperties
import os

SettingsProperties.check_settings_file()
assert float(SettingsProperties.get_snap())
assert isinstance(SettingsProperties.get_db('host'), basestring)
assert int(SettingsProperties.get_db('port'))
assert SettingsProperties.local() in (True, False)
assert isinstance(SettingsProperties.get_path('postgre'), basestring)
assert isinstance(SettingsProperties.get_path('python'), basestring)
if os.name !='posix':
    user, password = SettingsProperties.get_postgre_settings()
    assert isinstance(user, basestring)
    assert isinstance(password, basestring)
assert float(SettingsProperties.get_velocity_style('scale'))
velocity_classes = SettingsProperties.get_velocity_style('classes')
for k in velocity_classes:
    try:
        float(k)
    except:
        raise
velocity_colors = SettingsProperties.get_velocity_style('colors')
for color in velocity_colors:
    assert len(color) == 4
    for i in color:
        try:
            float(i)
        except:
            raise
        assert 0 <= i <= 255

print 'ok'


