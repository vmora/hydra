# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
check that there are not space ending lines

fix problems with option -f
"""

import os
import re
import sys

hydra_dir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
for root, dirs, files in os.walk(hydra_dir):
    for file_ in files:
        if re.match(r".*.(py|sql|json)$", file_) and not re.match(r".*docs.build.*", root):
            f = os.path.join(root, file_)
            lines = open(f).readlines()
            err = False
            for i, line in enumerate(lines):
                if re.match(r'^.* +$', line):
                    sys.stderr.write("%s:%d error: line ends with whitespace(s)\n"%(f, i+1))
                    err = True
            if len(sys.argv)==2 and sys.argv[1] == '-f' and err:
                open(f, 'w').write("\n".join([l.rstrip() for l in lines]))


print 'ok'
