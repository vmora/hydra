# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import numpy
import time
import math
import os
#import tripy this is buggy
from shapely import wkt
from itertools import chain, islice
from math import log, ceil
from qgis.core import *
from PyQt4.QtOpenGL import QGLPixelBuffer, QGLFormat, QGL
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from OpenGL.GL import *
from OpenGL.arrays import vbo
from OpenGL.GL import shaders
from hydra.project import Project
from hydra.utility.log import ConsoleLogger
from hydra.utility.empty_ui_manager import EmptyUIManager
from hydra.utility.opengl_layer import OpenGlLayerType, OpenGlLayer
from hydra.utility.result_decoder import W14Result, W15Result
from hydra.utility.settings_properties import SettingsProperties
import seidel


def roundUpSize(size):
    """return size roudup to the nearest power of 2"""
    return QSize(pow(2, ceil(log(size.width())/log(2))),
                 pow(2, ceil(log(size.height())/log(2))))


def chunks(iterable, size=10):
    """Yield successive n-sized chunks from l."""
    iterator = iter(iterable)
    for first in iterator:
        yield list(chain([first], islice(iterator, size - 1)))

class ShaderError(Exception):
    pass

class MyLayerLegendNode(QgsLayerTreeModelLegendNode):
    def __init__(self, nodeLayer, parent, text, color):
        QgsLayerTreeModelLegendNode.__init__(self, nodeLayer, parent)
        self.__text = text
        self.__color = color

    def data(self, role):
        if role == Qt.DisplayRole or role == Qt.EditRole:
            return self.__text
        elif role  == Qt.DecorationRole:
            im = QImage(16, 16, QImage.Format_ARGB32)
            im.fill(self.__color)
            return im
        else:
            return None

class MyLayerLegend(QgsDefaultPluginLayerLegend):
    def __init__(self, layer):
        QgsDefaultPluginLayerLegend.__init__(self, layer)
        self.nodes = []

    def createLayerTreeModelLegendNodes(self, nodeLayer):
        self.nodes = [
                MyLayerLegendNode(nodeLayer, self, "0.01 - 0.2 m", QColor.fromRgbF(0.77647059,  0.85882353,  0.9372549)),
                MyLayerLegendNode(nodeLayer, self, "0.2 - 0.5 m", QColor.fromRgbF(0.61960784,  0.79215686,  0.88235294)),
                MyLayerLegendNode(nodeLayer, self, "0.5 - 1.0 m", QColor.fromRgbF(0.41960784,  0.68235294,  0.83921569)),
                MyLayerLegendNode(nodeLayer, self, "1.0 - 1.5 m", QColor.fromRgbF(0.25882353,  0.57254902,  0.77647059)),
                MyLayerLegendNode(nodeLayer, self, "1.5 - 2.0 m", QColor.fromRgbF(0.12941176,  0.44313725,  0.70980392)),
                MyLayerLegendNode(nodeLayer, self, "> 2.0 m", QColor.fromRgbF(0.03137255,  0.31764706,  0.61176471))
                ]
        return self.nodes

class MyLayer(OpenGlLayer):

    LAYER_TYPE="my_layer"

    def __init__(self, project, w14, w15, name=None):
        OpenGlLayer.__init__(self, MyLayer.LAYER_TYPE, name)

        self.__layerLegend = MyLayerLegend(self)
        self.setLegend(self.__layerLegend)

        self.__pixBuf = None
        self.__recompileShader = False
        self.__level_shaders = None
        self.__velocity_shaders = None

        model = project.get_current_model().name

        srid, ext = project.execute("""
            select max(st_srid(geom)), st_extent(geom) from {model}.coverage
            """.format(model=model)).fetchone()

        ext = [float(c) for p in ext.replace('BOX(', '').replace(')', '').split(',') for c in p.split()]

        self.setExtent(QgsRectangle(ext[0], ext[1], ext[2], ext[3]))
        self.__offset = numpy.array([.5*(ext[0]+ext[2]), .5*(ext[1]+ext[3])])
        crs = QgsCoordinateReferenceSystem("epsg:{}".format(srid))
        self.setCrs(crs)
        self.__destCRS = crs

        self.__w15 = w15
        self.__w14 = w14
        self.__step = 0

        vtx = []
        triangles = []
        lines = []
        points = []
        vtx_idx = [] # gives each vtx an index for lookup in the resluts
        centers = []
        centers_idx = []
        idx = 0

        # temporary table to speedup things
        #project.execute("""
        #    create temporary table temp_open_reach as select * from {model}.open_reach
        #    """.format(model=model))

        for names in chunks(enumerate(self.__w15.names), 10000): # the chunk size increases the lenght of the query (values list)
            res = project.execute("""
                select st_force2d(contour), zb, idx, st_x(geom), st_y(geom)
                from {model}.elem_2d_node as n
                join (values {values}) as w(idx, name) on n.name=w.name
                order by w.idx
                """.format(
                    model=model,
                    values=",".join(["({}, '{}')".format(i, n) for i, n in names]))).fetchall()

            for rec in res:
                geom = wkt.loads((project.execute("""select wkb_loads('{}')""".format(rec[0])).fetchone())[0])
                vtx += [(c[0], c[1], rec[1]) for c in geom.exterior.coords]
                triangles += [(len(vtx)-3, len(vtx)-2, len(vtx)-1)]\
                        if len(geom.exterior.coords) == 3 else \
                        [(len(vtx)-4, len(vtx)-3, len(vtx)-2), (len(vtx)-4, len(vtx)-2, len(vtx)-1)]
                vtx_idx += list([rec[2]]*len(geom.exterior.coords))
                centers.append((rec[3], rec[4]))
                centers_idx.append(rec[2])

            res = project.execute("""
                select st_force2d(c.geom), zs_array[1][1], idx
                from {model}.storage_node as n
                join (values {values}) as w(idx, name) on n.name=w.name
                join {model}.coverage as c on st_intersects(n.geom, c.geom)
                order by w.idx
                """.format(
                    model=model,
                    values=",".join(["({}, '{}')".format(i, n) for i, n in names]))).fetchall()

            for rec in res:
                geom = wkt.loads((project.execute("""select wkb_loads('{}')""".format(rec[0])).fetchone())[0])
                assert geom.is_valid
                exterior = numpy.array(geom.exterior.coords)
                if tuple(exterior[-1]) == tuple(exterior[0]):
                    exterior = exterior[:-1]
                triangulator = seidel.Triangulator(exterior)
                tris = triangulator.triangles()
                for tri in tris:
                    vtx += [(c[0], c[1], rec[1]) for c in tri]
                    vtx_idx += list([rec[2]]*3)
                    triangles.append((len(vtx)-3, len(vtx)-2, len(vtx)-1))

            res = project.execute("""
                select st_x(u.geom), st_y(u.geom), u.z_ground, st_x(d.geom), st_y(d.geom), d.z_ground, w.idx, x.idx
                from {model}.street_link as l
                join {model}.crossroad_node as u on u.id = l.up
                join {model}.crossroad_node as d on d.id = l.down
                join (values {values}) as w(idx, name) on u.name=w.name
                join (values {values}) as x(idx, name) on d.name=x.name
                order by w.idx
                """.format(
                    model=model,
                    values=",".join(["({}, '{}')".format(i, n) for i, n in names]))).fetchall()

            for rec in res:
                vtx += [(rec[0], rec[1], rec[2]), (rec[3], rec[4], rec[5])]
                lines += [len(vtx)-2, len(vtx)-1]
                vtx_idx += [rec[6], rec[7]]


            #cur.execute("""
            #    with trans as (
            #        select n.name, {model}.interpolate_transect_at(n.geom) as geom
            #        from {model}.river_node as n
            #        join temp_open_reach as r on r.reach=n.reach
            #    )
            #    select t.geom, {model}.profile_from_transect(t.geom, 't'::boolean), w.idx
            #    from trans as t
            #    join (values {values}) as w(idx, name) on t.name=w.name
            #    where t.geom is not null and st_geometrytype(t.geom)='ST_LineString'
            #    order by idx
            #    """.format(
            #        model=model,
            #        values=",".join(["({}, '{}')".format(i, n) for i, n in names])))


        self.__vtx = numpy.require(vtx, numpy.float64, 'F')
        self.__origVtx = self.__vtx
        self.__vtx[:, :2] -= self.__offset
        self.__vtx = self.__vtx.astype(numpy.float32)
        self.__triangles = numpy.require(triangles, numpy.int32, 'F')
        self.__vtxIdx = numpy.require(vtx_idx, numpy.int32, 'F')

        self.__centers = numpy.require(centers, numpy.float64, 'F')
        self.__origCenters = self.__centers
        self.__centers -= self.__offset
        self.__centers = self.__centers.astype(numpy.float32)
        self.__centersIdx = numpy.require(centers_idx, numpy.int32, 'F')

        self.__lines = numpy.require(lines, numpy.int32, 'F')


    def __recompileNeeded(self):
        self.__recompileShader = True

    def __compileShaders(self):
        # Polygons for water level
        level_vertex_shader = shaders.compileShader("""
            attribute vec3 position;
            attribute float water_altitude;
            varying float water_level;
            void main()
            {
                water_level = water_altitude - position.z;
                gl_Position = gl_ModelViewProjectionMatrix * vec4(position.xy, 0, 1.);
            }
            """, GL_VERTEX_SHADER)

        level_fragment_shader = shaders.compileShader("""
            varying float water_level;
            void main()
            {
                gl_FragColor = vec4(0., 0., 0., 1.);

                if (water_level < 0.01)
                    gl_FragColor = vec4(0., 0., 0., 0.);
                else if (water_level >= 0.01 && water_level < 0.2)
                    gl_FragColor = vec4(0.77647059,  0.85882353,  0.9372549, 1.);
                else if (water_level >= 0.2 && water_level < 0.5)
                    gl_FragColor = vec4(0.61960784,  0.79215686,  0.88235294, 1.);
                else if (water_level >= 0.5 && water_level < 1.0)
                    gl_FragColor = vec4(0.41960784,  0.68235294,  0.83921569, 1.);
                else if (water_level >= 1.0 && water_level < 1.5)
                    gl_FragColor = vec4(0.25882353,  0.57254902,  0.77647059, 1.);
                else if (water_level >= 1.5 && water_level < 2.0)
                    gl_FragColor = vec4(0.12941176,  0.44313725,  0.70980392, 1.);
                else if (water_level >= 2.0)
                    gl_FragColor = vec4(0.03137255,  0.31764706,  0.61176471, 1.);
            }
            """, GL_FRAGMENT_SHADER)

        self.__level_shaders = shaders.compileProgram(level_vertex_shader, level_fragment_shader)
        self.altitude_position_location = 0
        self.water_altitude_location = 1
        glBindAttribLocation(self.__level_shaders, self.altitude_position_location, 'position')
        glBindAttribLocation(self.__level_shaders, self.water_altitude_location, 'water_altitude')


        # Arrows for water velocity
        velocity_vertex_shader = shaders.compileShader("""
            attribute vec3 position;
            attribute float water_flow;
            varying float water_velocity;
            void main()
            {
                water_velocity = water_flow;
                gl_Position = gl_ModelViewProjectionMatrix * vec4(position.xy, 0, 1.);
            }
            """, GL_VERTEX_SHADER)

        velocity_fragment_shader = shaders.compileShader("""
            varying float water_velocity;
            void main()
            {
                gl_FragColor = vec4(0, 0, 0, 0);

                %s
            }
            """ %(self.__velocity_shader()), GL_FRAGMENT_SHADER)

        self.__velocity_shaders = shaders.compileProgram(velocity_vertex_shader, velocity_fragment_shader)
        self.velocity_position_location = 0
        self.water_velocity_location = 1
        glBindAttribLocation(self.__velocity_shaders, self.velocity_position_location, 'position')
        glBindAttribLocation(self.__velocity_shaders, self.water_velocity_location, 'water_flow')

        self.__recompileShader = False
        self.__scale = float(SettingsProperties.get_velocity_style('scale'))

    def __velocity_shader(self):
        velocity_classes = SettingsProperties.get_velocity_style('classes')
        velocity_colors = SettingsProperties.get_velocity_style('colors')

        if len(velocity_classes) != len(velocity_colors):
            raise ShaderError("Number of classes and colors do not match.")

        if len(velocity_classes) == 0:
            raise ShaderError("No color class found.")

        normalized_colors = [[str(float(x)/255.) for x in rgb] for rgb in velocity_colors]

        classes = []
        for i in range(len(velocity_classes)):
            if i == 0:
                f = """if (water_velocity >= {} && water_velocity < {})
                            gl_FragColor = vec4({});""".format(velocity_classes[i], velocity_classes[i+1], ', '.join(normalized_colors[i]))
            elif i+1 < len(velocity_classes):
                f = """else if (water_velocity >= {} && water_velocity < {})
                            gl_FragColor = vec4({});""".format(velocity_classes[i], velocity_classes[i+1], ', '.join(normalized_colors[i]))
            else:
                f = """else if (water_velocity >= {})
                            gl_FragColor = vec4({});""".format(velocity_classes[i], ', '.join(normalized_colors[i]))
            classes.append(f)

        shaders_classes = '\n'.join(classes)
        return shaders_classes


    def __resize(self, roundupImageSize):
        # QGLPixelBuffer size must be power of 2
        assert roundupImageSize == roundUpSize(roundupImageSize)

        # force alpha format, it should be the default,
        # but isn't all the time (uninitialized)
        #fmt = QGLFormat(QGL.SampleBuffers)
        fmt = QGLFormat()
        fmt.setAlpha(True)

        self.__pixBuf = QGLPixelBuffer(roundupImageSize, fmt)
        assert self.__pixBuf.format().alpha()
        self.__pixBuf.makeCurrent()
        self.__pixBuf.bindToDynamicTexture(self.__pixBuf.generateDynamicTexture())
        self.__compileShaders()
        #glEnable(GL_MULTISAMPLE)
        self.__pixBuf.doneCurrent()

    def image(self, rendererContext, size=None):

        transform = rendererContext.coordinateTransform()
        ext = rendererContext.extent()
        mapToPixel = rendererContext.mapToPixel()

        size = QSize((ext.xMaximum()-ext.xMinimum())/mapToPixel.mapUnitsPerPixel(),
                     (ext.yMaximum()-ext.yMinimum())/mapToPixel.mapUnitsPerPixel()) \
                             if abs(mapToPixel.mapRotation()) < .01 else size

        if transform:
            ext = transform.transform(ext)
            if transform.destCRS() != self.__destCRS:
                self.__destCRS = transform.destCRS()
                def transf(x):
                    p = transform.transform(x[0], x[1])
                    return [p.x(), p.y(), x[2]]
                vtx = numpy.array(self.__origVtx)
                vtx = numpy.apply_along_axis(transf, 1, vtx)
                vtx[:, :2] -= self.__offset
                self.__vtx = vtx.astype(numpy.float32)
                def transf2d(x):
                    p = transform.transform(x[0], x[1])
                    return [p.x(), p.y()]
                vtx = numpy.array(self.__origCenters)
                vtx = numpy.apply_along_axis(transf2d, 1, vtx)
                vtx[:, :2] -= self.__offset
                self.__centers = vtx.astype(numpy.float32)
                self.__recompileNeeded()

        return self.__image(
                size,
                (.5*(ext.xMinimum() + ext.xMaximum()),
                 .5*(ext.yMinimum() + ext.yMaximum())),
                (mapToPixel.mapUnitsPerPixel(),
                 mapToPixel.mapUnitsPerPixel()),
                 mapToPixel.mapRotation())

    def __image(self, imageSize, center, mapUnitsPerPixel, rotation=0):

        start = time.time()
        roundupSz = roundUpSize(imageSize)

        if not self.__pixBuf \
                or roundupSz.width() != self.__pixBuf.size().width() \
                or roundupSz.height() != self.__pixBuf.size().height():
            # we need to call the main thread for a change of the
            # pixel buffer and wait for the change to happen
            self.__resize(roundupSz)

        self.__pixBuf.makeCurrent()


        if self.__recompileShader:
            self.__compileShaders()

        #self.__vbo.bind()
        glClearColor(0., 0., 0., 0.)
        glDisableClientState(GL_VERTEX_ARRAY)
        glEnableVertexAttribArray(self.altitude_position_location)
        glEnableVertexAttribArray(self.water_altitude_location)
        glDisable(GL_TEXTURE_1D)

        glShadeModel(GL_FLAT)

        glClear(GL_COLOR_BUFFER_BIT)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        # scale
        glScalef(2./(roundupSz.width()*mapUnitsPerPixel[0]),
                 2./(roundupSz.height()*mapUnitsPerPixel[1]),
                 1)
        # rotate
        glRotatef(-rotation, 0, 0, 1)

        # translate
        glTranslatef(-center[0]+self.__offset[0],
                     -center[1]+self.__offset[1],
                     0)

        # Draw water-level polygons for elem_2d
        glUseProgram(self.__level_shaders)

        start_read = time.time()
        results = self.__w15.read(self.__step) #numpy.linspace(0, 1, len(self.__w15.names))
        water_altitude = results[self.__vtxIdx[:], 1]
        #print "fetch in %.3f sec"%(time.time() - start_read)
        start_render = time.time()
        glVertexAttribPointer(self.altitude_position_location, 3, GL_FLOAT, False, 0, self.__vtx)
        glVertexAttribPointer(self.water_altitude_location, 1, GL_FLOAT, False, 0, water_altitude)
        glDrawElementsui(GL_TRIANGLES, self.__triangles)

        # Draw water_level on street (same shader)
        glLineWidth(5)
        glDrawElementsui(GL_LINES, self.__lines)

        glUseProgram(0)

        # Draw water_level on street (same shader)
        glLineWidth(5)
        glDrawElementsui(GL_LINES, self.__lines)

        # Draw water-speed arrows for elem_2d
        glUseProgram(self.__velocity_shaders)
        glLineWidth(2.5)
        glDisableVertexAttribArray(self.altitude_position_location)
        glDisableVertexAttribArray(self.water_altitude_location)
        glEnableVertexAttribArray(self.velocity_position_location)
        glEnableVertexAttribArray(self.water_velocity_location)

        glPointSize(1)
        glColor4f(0.,0.,0.,1.)
        vtx = numpy.empty((self.__centers.shape[0]*6, 2), dtype=numpy.float32)
        water_speed = results[self.__centersIdx[:], 2:4]

        water_flow_six_repeat = numpy.repeat([numpy.linalg.norm(v) for v in water_speed], 6)

        #water_flow_six_repeat[:] = 0.5
        #water_flow_six_repeat[:16] = 1

        arrow = water_speed*self.__scale
        theta = math.pi/8
        rot = numpy.array([
            [math.cos(theta), math.sin(theta)],
            [-math.sin(theta), math.cos(theta)]])
        vtx[0::6,:] = self.__centers
        vtx[1::6,:] = self.__centers + arrow
        vtx[2::6,:] = vtx[1::6,:]
        vtx[3::6,:] = vtx[1::6,:] - .1*arrow.dot(rot)
        vtx[4::6,:] = vtx[1::6,:]
        vtx[5::6,:] = vtx[1::6,:] - .1*arrow.dot(rot.T)

        # print 'Timestep: ', self.__step
        # print 'Water speed: ', water_speed
        # print 'Water flow: ', water_flow_six_repeat
        # print 'VTX: ', vtx

        glVertexAttribPointer(self.velocity_position_location, 2, GL_FLOAT, False, 0, vtx)
        glVertexAttribPointer(self.water_velocity_location, 1, GL_FLOAT, False, 0, water_flow_six_repeat)
        # /!\ Remove lines drawing for arrows as arrow length/color classes behaves weirdly
        glDrawArrays(GL_LINES, 0, len(vtx));

        #print "render in %.3f sec"%(time.time() - start_render)

        img = self.__pixBuf.toImage()

        self.__pixBuf.doneCurrent()

        #print "image in %.3f sec"%(time.time() - start)
        return img.copy( .5*(roundupSz.width()-imageSize.width()),
                         .5*(roundupSz.height()-imageSize.height()),
                         imageSize.width(), imageSize.height())

    def setArrowScale(self, scale):
        self.__scale = scale

    def setStep(self, idx):
        self.__step = idx
        self.triggerRepaint()

    def steps(self):
        return range(self.__w15.nb_steps)

class MyLayerType(OpenGlLayerType):
    def __init__(self, type_=None):
        OpenGlLayerType.__init__(self, type_ or MyLayer.LAYER_TYPE)
        #self.__dlg = None

    def createLayer(self):
        return MyLayer()

    def showLayerProperties(self, layer):
        #self.__dlg = PropertyDialog(layer)
        return False

if __name__ == "__main__":
    import sys
    from qgis.core import *

    """
    mylayer.py [project_name w14file w15file image_destination]
    """

    app = QgsApplication(sys.argv, True)
    QgsApplication.setPrefixPath('/usr/local', True)
    QgsApplication.initQgis()

    project_name = sys.argv[1]
    project = Project(ConsoleLogger(), EmptyUIManager(), project_name)
    project.set_current_model('model1')

    w14 = W14Result(sys.argv[2])
    w15 = W15Result(sys.argv[3])

    dest = sys.argv[4]

    layer = MyLayer(project, w14, w15, 'test_layer')

    class DummyMapToPixel(object):
        def mapUnitsPerPixel(self):
            return 10
        def mapRotation(self):
            return 0

    class DummyContext(object):
        def mapToPixel(self):
            return DummyMapToPixel()
        def extent(self):
            return layer.extent()
        def coordinateTransform(self):
            return None

    for step in [20,21]: #layer.steps():#range(5): #
        layer.setStep(step)
        img_file = os.path.join(dest, 'test%03d.png'%(step))
        layer.image(DummyContext()).save(img_file)
        print 'Written ', img_file

    exit(0)