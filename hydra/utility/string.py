# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
string ulility modules
"""
from __future__ import absolute_import # important to read the doc !
import string
import unicodedata
import re

def normalized_name(name, max_length=24):
    """removes accents, spaces and caps"""
    name = name.replace(u" ", u"_").lower()
    filter_ = string.ascii_letters+"_"+string.digits
    return ''.join(x for x in unicodedata.normalize('NFKD', name) \
            if x in filter_)[:max_length]

def normalized_caps_name(name, max_length=24):
    """removes accents and spaces"""
    name = name.replace(u" ", u"_")
    filter_ = string.ascii_letters+"_"+string.digits
    return ''.join(x for x in unicodedata.normalize('NFKD', name) \
            if x in filter_)[:max_length]

def normalized_model_name(model_name):
    """removes accents and spaces and limit length"""
    max_length=16
    return normalized_name(model_name, max_length)

def get_str(value):
    return str(value) if value is not None else ''

def isfloat(value):
    try:
        float(value)
        return True
    except:
        return False

def isint(value):
    try:
        int(value)
        return True
    except:
        return False

def get_sql_float(value):
    """Return None if null or not a float"""
    if isint(value):
        return int(value)
    elif isfloat(value):
        return float(value)
    else:
        return None

def get_sql_forced_float(value):
    """Return 0 if null or not a float"""
    return float(value) if isfloat(value) else 0

def list_to_sql_array(py_list):
    return str(map(str, py_list)).replace('[', '{').replace(']', '}').replace('\'', '') if py_list else None