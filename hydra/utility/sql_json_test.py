# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if layers loaded actually exist on the database

USAGE

   python -m hydra.utility.sql_json_test

"""

from __future__ import absolute_import # important to read the doc !
assert __name__ == "__main__"
import hydra.utility.sql_json as sql_json

params = dict()
params['gameson'] = dict()
params['r15'] = dict()
params['gameson']['pollution_coef'] = "10.52"
params['gameson']['waterfall_coef'] = "0.0"
params['gameson']['adjust_coef'] = "1"
params['r15']['reoxygen_coef'] = "'TEST'"
params['r15']['temperature_correct_coef'] = "15"

str_json = sql_json.pack(params)
assert str_json == '{"r15": {"reoxygen_coef": null, "temperature_correct_coef": 15}, "gameson": {"waterfall_coef": 0.0, "adjust_coef": 1, "pollution_coef": 10.52}}'
unpacked = sql_json.unpack(str_json)
assert unpacked['r15']['reoxygen_coef'] == ''

print "ok"