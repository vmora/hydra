# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
smart access to hydra binary files

HydraResult implement __getitem__(self, keys) operator
If keys arg is a string or a list of string then access is made
by element name and return a ResultFromElem object.
If keys arg is a int or a list of int or a slice then access is made
by time step number and return a ResultFromTimeStep object.

ResultFromElem and ResultFromTimeStep also implement __getitem__(self, keys) operator
ResultFromElem accept slice, int and list of int to indicates the desired time steps of the selected elements

ResultFromTimeStep accept string, list of string, slice, int and list of int to indicates the desired elements for the selected time steps

Results are return in the following format: [time, [list of 8 floats]]

USAGE

    results = HydraResult(file_path)

    Access by time steps:
        results[0]["NOD_1"]                 --> return the result of "NOD_1" at the first time step
        results[0][["NOD_1", "NOD_2"]]      --> return the result of "NOD_1" and "NOD_2" for the first time step
        results[0:3][["NOD_1", "NOD_2"]]    --> return the result of "NOD_1" and "NOD_2" for the first 3 time steps
        results[[0,5]][["NOD_1", "NOD_2"]]  --> return the result of "NOD_1" and "NOD_2" for the first time step 0 and 5
        results[0][:]                       --> return the result of all elements for the first time step
        results[:][:]                       --> return the result of all elements for all time steps

    Access by elements:
        results["NOD_1"][:]                 --> return the result of all time steps for the element "NOD_1"
        results[["NOD_1", "NOD_2"]][:]      --> return the result of all time steps for the element "NOD_1" and "NOD_2"

"""

import struct
import os
import numpy
import collections

class HydraResult:
    @staticmethod
    def read_binary(file_stream, time_step, i_elem, nb_elem):
        start = (nb_elem+1)*32 + time_step*(nb_elem+1)*32
        file_stream.seek(start)
        time = numpy.frombuffer(file_stream.read(32), dtype=numpy.float32)[0]
        file_stream.seek(32*(i_elem), 1)
        results = numpy.frombuffer(file_stream.read(32), dtype=numpy.float32)
        return [time, results]

    class ResultFromElem():
        def __init__(self, file_stream, i_elem, nb_step, nb_elem):
            self.__file = file_stream
            self.__nb_step = nb_step
            self.__i_elem = i_elem
            self.__nb_elem = nb_elem

        def __getitem__(self, keys):
            if isinstance(keys, slice):
                start = 0 if keys.start<0 else keys.start if keys.start<self.__nb_step else self.__nb_step
                stop = 0 if keys.stop<0 else keys.stop if keys.stop<self.__nb_step else self.__nb_step
                iter_values = range(start, stop)
            elif isinstance(keys, int):
                start = 0 if keys<0 else keys if keys<self.__nb_step else self.__nb_step
                stop = start+1
                iter_values = range(start, stop)
            elif isinstance(keys, list):
                iter_values = keys
            else:
                raise Exception("Key type exception in getitem operator.")

            values = list()
            if isinstance(self.__i_elem, list):
                for elem in self.__i_elem:
                    for i in iter_values:
                        values.append(HydraResult.read_binary(self.__file, i, elem, self.__nb_elem))
            else:
                for i in iter_values:
                    values.append(HydraResult.read_binary(self.__file, i, self.__i_elem, self.__nb_elem))

            return values

    class ResultFromTimeStep():
        def __init__(self, file_stream, i_step, nb_step, names_elem):
            self.__file = file_stream
            self.__nb_step = nb_step
            self.__i_step = i_step
            self.__names_elem = names_elem

        def __getitem__(self, keys):
            iter_values = list()
            if isinstance(keys, str):
                iter_values.append(int(self.__names_elem.index(keys.upper())))
            elif isinstance(keys, int):
                iter_values = [keys]
            elif isinstance(keys, list):
                for key in keys:
                    if isinstance(key, int):
                        iter_values.append(key)
                    else:
                        iter_values.append(int(self.__names_elem.index(key.upper())))
            elif isinstance(keys, slice):
                nb_elem = len(self.__names_elem)
                start = 0 if keys.start<0 else keys.start if keys.start<nb_elem else nb_elem
                stop = 0 if keys.stop<0 else keys.stop if keys.stop<nb_elem else nb_elem
                iter_values = range(start, stop)
            else:
                raise Exception("Key type exception in getitem operator.")

            values = list()
            if isinstance(self.__i_step, list):
                for step in self.__i_step:
                    for i_elem in iter_values:
                        values.append(HydraResult.read_binary(self.__file, step, i_elem, len(self.__names_elem)))
            elif isinstance(self.__i_step, slice):
                istep_st = 0 if self.__i_step.start<0 else self.__i_step.start if self.__i_step.start<self.__nb_step else self.__nb_step
                istep_ed = 0 if self.__i_step.stop<0 else self.__i_step.stop if self.__i_step.stop<self.__nb_step else self.__nb_step
                for istep in range(istep_st, istep_ed):
                    for i_elem in iter_values:
                        values.append(HydraResult.read_binary(self.__file, istep, i_elem, len(self.__names_elem)))
            else:
                for i_elem in iter_values:
                    values.append(HydraResult.read_binary(self.__file, self.__i_step, i_elem, len(self.__names_elem)))

            return values

    def __len__(self):
        return self.__nb_step

    def __init__(self, file_path):
        self.__file = open(file_path, 'rb')

    def __del__(self):
        if self.__file:
            self.__file.close()

    def __getitem__(self, keys):
        self.__file.seek(0)
        nb_step, nb_elem, nb_link = numpy.frombuffer(self.__file.read(32), dtype=numpy.int32)[:3]
        names = list()
        for i in range(nb_elem + nb_link):
            names.append(''.join(struct.unpack('s'*24, self.__file.read(24))).strip().upper())
            self.__file.seek(8, 1)

        if isinstance(keys, slice) or isinstance(keys, int): # from time step
            return HydraResult.ResultFromTimeStep(self.__file, keys, nb_step, names)
        elif isinstance(keys, str): # from elem name
            i_elem = int(names.index(keys.upper()))
            return HydraResult.ResultFromElem(self.__file, i_elem, nb_step, nb_elem + nb_link)
        elif isinstance(keys, list):
            if len(keys)>0:
                if isinstance(keys[0], int): # from time step
                    return HydraResult.ResultFromTimeStep(self.__file, keys, nb_step, names)
                elif isinstance(keys[0], str): # from elem name
                    indices = list()
                    for name in keys:
                        indices.append(int(names.index(name.upper())))
                    return HydraResult.ResultFromElem(self.__file, indices, nb_step, nb_elem + nb_link)
                else:
                    raise Exception("Key type exception in getitem operator.")
        else:
            raise Exception("Key type exception in getitem operator.")
        return None

    def __setitem__(self, key, item):
        raise Exception("Set item is not allowed")

class W14Result(object):
    def __init__(self, filename):
        f = open(filename, 'rb')
        f.seek(0)
        nb_step, nb_umps, nb_lias = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
        value_map = []
        names = []
        type_ids = []
        for i in range(nb_umps+nb_lias):
            name = f.read(24).decode('utf-8').rstrip()
            itype, id_ =  numpy.frombuffer(f.read(8), dtype=numpy.int32)[:2]
            names.append(name)

        self.__file = f
        self.__nb_step = nb_step
        self.__nb_lias = nb_umps+nb_lias
        self.__names = names

    def __getattr__(self, name):
        if "names" == name:
            return self.__names
        elif "nb_steps" == name:
            return self.__nb_step
        raise AttributeError

    def __del(self):
        self.__file.close()

    def read(self, step):
        self.__file.seek(32*((1 + self.__nb_lias)*(1 + step)+1))
        return numpy.frombuffer(self.__file.read(32*self.__nb_lias), dtype=numpy.float32).reshape((self.__nb_lias, -1))

class W15Result(object):
    def __init__(self, filename):
        f = open(filename, 'rb')

        f.seek(0)
        nb_step, nb_nod, nb_lias = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
        name_idx = {}
        value_map = []
        names = []
        type_ids = []
        for i in range(nb_nod+nb_lias):
            name = f.read(24).decode('utf-8').rstrip()
            name_idx[name] = i
            itype, id_ =  numpy.frombuffer(f.read(8), dtype=numpy.int32)[:2]
            type_ids.append(itype)
            if itype > 0 and itype < 7:
                value_map.append({
                        1: ['q_up', 'z_up', 'v_min', 'deb'],
                        2: ['q_up', 'z_up', 'z_down', 'deb_over_qmaj', 'hdepot', 'qs'],
                        3: ['q_up'],
                        4: ['z', 'vol', '', '', 'hdepot', 'qs'],
                        5: ['q_up', 'z_up', 'z_down', 'zs_over_zb', 'hdepot', 'qs'],
                        6: ['z', 'vol', '', '', 'hdepot', 'qs'],
                        7: ['z', 'vol', 'u_x', 'u_y', 'hdepot', 'qs'],
                        }[itype])
            else:
                value_map.append([])
            names.append(name)

        self.__name_idx = name_idx
        self.__file = f
        self.__nb_step = nb_step
        self.__nb_nod = nb_nod+nb_lias
        self.__value_map = value_map
        self.__names = names
        self.__type_ids = type_ids

    def __getattr__(self, name):
        if "names" == name:
            return self.__names
        elif "nb_steps" == name:
            return self.__nb_step
        raise AttributeError

    def __del(self):
        self.__file.close()

    def read(self, step):
        self.__file.seek(32*((1 + self.__nb_nod)*(1 + step)+1))
        return numpy.frombuffer(self.__file.read(32*self.__nb_nod), dtype=numpy.float32).reshape((self.__nb_nod, -1))

    def get_step_times(self):
        times = []
        for i in range(self.__nb_step):
            self.__file.seek(32*((1 + self.__nb_nod) + i*(self.__nb_nod+1)))
            times.append(float(numpy.frombuffer(self.__file.read(32), dtype=numpy.float32)[0]))
        return times

class W16Result:
    class W16TimeResult():
        def __init__(self, parent, ielem):
            self.__parent = parent
            self.ielem = ielem

        def __getitem__(self, keys):
            if isinstance(keys, slice):
                start = 0 if keys.start<0 else keys.start if keys.start<self.__parent.nstep+2 else self.__parent.nstep
                stop = 0 if keys.stop<0 else keys.stop if keys.stop<self.__parent.nstep+2 else self.__parent.nstep
                iter_values = range(start, stop)
            elif isinstance(keys, int):
                start = 0 if keys<0 else keys if keys<self.__parent.nstep+2 else self.__parent.nstep
                stop = start+1
                iter_values = range(start, stop)
            else:
                raise Exception("Key type should be int or slice.")

            values = list()
            for i in iter_values:
                values.append(self.result_at_time(i))

            return values

        def result_at_time(self, time):
            result = list()
            start = self.__parent.nrec[self.ielem-1] if self.ielem>0 else 0
            end = self.__parent.nrec[self.ielem]
            self.__parent.file.seek(32*(1 + self.__parent.nb + self.__parent.ntot) + 32*time*(self.__parent.ntot+1))
            time = numpy.frombuffer(self.__parent.file.read(32), dtype=numpy.float32)[0]
            self.__parent.file.seek(32*start,1)
            for n in range(start, end):
                result.append([time, numpy.frombuffer(self.__parent.file.read(32), dtype=numpy.float32)])
            return result


    def __len__(self):
        return self.nb

    def __init__(self, file_path):
        self.file = open(file_path, 'rb')
        self.nstep = 0
        self.ntot = 0
        self.nb = 0
        self.names = []
        self.nrec = []
        self.read_header()

    def read_header(self):
        self.file.seek(0)
        self.nstep, self.ntot, self.nb = numpy.frombuffer(self.file.read(32), dtype=numpy.int32)[:3]
        self.names = []
        self.nrec = []
        for i in range(self.nb):
            name = ''.join(struct.unpack('s'*16, self.file.read(16))).strip().upper()
            nrec = struct.unpack('i'*4, self.file.read(16))[0]
            self.names.append(name)
            self.nrec.append(nrec)

        self.elem_geom = []
        for i in range(self.ntot):
            self.elem_geom.append(numpy.frombuffer(self.file.read(32), dtype=numpy.float32))

    def __del__(self):
        if self.file:
            self.file.close()

    def get_geom(self, name):
        indice = self.names.index(name.strip().upper())
        start = self.nrec[indice-1] if indice>0 else 0
        end = self.nrec[indice]
        return self.elem_geom[start:end]

    def get_step_times(self):
        times = []
        for i in range(self.nstep):
            self.file.seek(32*(1 + self.nb + self.ntot) + 32*i*(self.ntot+1))
            times.append(float(numpy.frombuffer(self.file.read(32), dtype=numpy.float32)[0]))
        return times

    def __getitem__(self, keys):
        if not (isinstance(keys, str) or isinstance(keys, unicode)):
            raise Exception("Key should be str elem name.")
        indice = self.names.index(keys.strip().upper())
        return W16Result.W16TimeResult(self, indice)

    def __setitem__(self, key, item):
        raise Exception("Set item is not allowed")

def get_results(elem_name, result_file_path):
    assert os.path.isfile(result_file_path)
    result = [[], [], [], [], []]
    with open(result_file_path, 'rb') as file:
        file.seek(0)
        data = struct.unpack('i'*8, file.read(32))
        nStep = data[0]
        nElem = data[1] + data[2]

        index = -1
        for i in range(0,nElem):
            name = ''.join(struct.unpack('s'*24, file.read(24))).strip()
            id = struct.unpack('i'*2, file.read(8))
            if (name.upper()==elem_name.strip().upper()):
                index = i
                file.seek(32*(nElem-index-1),1)
                break

        if index==-1:
            raise RuntimeError("Elem not found.")

        for i in range(0, nStep):
            rdata = file.read(32)
            date = struct.unpack('f'*8, rdata)[0]
            file.seek(32*index,1)
            rdata = file.read(32)

            data = struct.unpack('f'*8, rdata)
            result[0].append(date)
            result[1].append(data[0])
            result[2].append(data[1])
            result[3].append(data[2])
            result[4].append(data[3])
            result[5].append(data[4])
            result[6].append(data[5])
            result[7].append(data[6])
            result[8].append(data[7])

            file.seek(32*(nElem-index-1),1)

    return result

if __name__=='__main__':
    w16result = W16Result("C:/Users/vmw7_64/.hydra/ill/SCN_1/hydraulique/SCN_1_MRG3.w16")

    name = "MRKB_3"
    indice = w16result.names.index(name)
    start = w16result.nrec[indice-1] if indice>0 else 0
    end = w16result.nrec[indice]-1

    # print "Indice ", name, ": ", indice
    # print "start ; end: ", start, " ; ", end

    # elem_geom = w16result.get_geom(name)

    # for i in range(len(elem_geom)):
        # print "%f %f %f %f"%(elem_geom[i][0],elem_geom[i][1],elem_geom[i][2],elem_geom[i][3])

    # print "Longitudinal :"
    # pks = numpy.array([r[0] for r in elem_geom])
    # baseline = numpy.array([(r[0], r[1]) for r in elem_geom]) # pk, zf
    # overflow = numpy.array([(r[0], r[2]) for r in elem_geom]) # pk, zv/zd
    # print pks
    # print baseline
    # print overflow

    # exit(0)
    values_min = w16result[name][w16result.nstep+1][0]
    values_min = numpy.array([r[1] for r in values_min])
    values_mid = w16result[name][w16result.nstep/2][0]
    values_mid = numpy.array([r[1] for r in values_mid])
    values_max = w16result[name][w16result.nstep+2][0]
    values_max = numpy.array([r[1] for r in values_max])

    for j in range(len(values_min)):
        print "z,min = %f ; z,mid = %f ; z,max = %f"%(float(values_min[j][0]), float(values_mid[j][0]), float(values_max[j][0]))

    # test = open("C:/Users/vmw7_64/.hydra/fixes_test/SCN_3/hydraulique/SCN_3_RIV.w16", 'rb')
    # test.seek(0)
    # nstep, ntot, nb = numpy.frombuffer(test.read(32), dtype=numpy.int32)[:3]
    # print nstep, ntot, nb
    # for i in range(nb):
        # name = ''.join(struct.unpack('s'*16, test.read(16))).strip().upper()
        # nrec = struct.unpack('i'*4, test.read(16))[0]
        # print name, nrec
    # print "#"
    # for i in range(ntot):
        # r=numpy.frombuffer(test.read(32), dtype=numpy.float32)
        # print "%f %f %f %f"%(r[0],r[1],r[2],r[3])
    # print "#"
    # while r.any():
        # r=numpy.frombuffer(test.read(32), dtype=numpy.float32)
        # print "%f %f %f %f"%(r[0],r[1],r[2],r[3])

    exit(0)