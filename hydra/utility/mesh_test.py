# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run mesh tests

USAGE

   python -m hydra.mesh_test [-hk]

OPTIONS

   -h, --help
        print this help

   -k, --keep
        keep database after test

    -r, --reset
        reset template db

"""

assert __name__ == "__main__"

from hydra.project import Project
from hydra.database.import_model import import_model_csv
from hydra.database.database import TestProject, project_exists, remove_project
from hydra.utility.timer import Timer
import os, sys
import getopt


try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hkr",
            ["help", "keep", 'reset'])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

keep =  "-k" in optlist or "--keep" in optlist
reset = "-r" in optlist or "--reset" in optlist


if not project_exists("template_project") or reset:
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")


project_name='mesh_test'
test_project = TestProject(project_name, keep)
project = Project.load_project(project_name)
project.add_new_model('model')
data_dir = os.path.join(os.path.dirname(__file__), 'test_data')

project.execute("""
    create view datal as
    with jdata as (
        select '{}'::json as data
    )
    select
        row_number() over() as id,
        st_setsrid(st_geomfromgeojson((d->'geometry')::varchar), 2154) as geom,
        (d->'properties'->'type')::varchar as type
    from (select json_array_elements(data->'features') as d from jdata) as t
    """.format(open(os.path.join(data_dir, 'mesh_test_line.geojson')).read()))
project.execute("""
    create view datap as
    with jdata as (
        select '{}'::json as data
    )
    select
        row_number() over() as id,
        st_setsrid(st_geomfromgeojson((d->'geometry')::varchar), 2154) as geom,
        (d->'properties'->'type')::varchar as type
    from (select json_array_elements(data->'features') as d from jdata) as t
    """.format(open(os.path.join(data_dir, 'mesh_test_point.geojson')).read()))


statements = """
insert into model.reach(geom) select geom from datal where type='"reach"'
;;
insert into model.river_cross_section_profile(geom) select n.geom from model.river_node as n
;;
update model.river_node set z_ground=project.altitude(geom)
;;
update model.river_cross_section_profile as p set type_cross_section_down='valley' where exists (select 1 from model.reach as r where st_intersects(p.geom, st_startpoint(r.geom)))
;;
update model.river_cross_section_profile as p set type_cross_section_up='valley' where exists (select 1 from model.reach as r where st_intersects(p.geom, st_endpoint(r.geom)))
;;

insert into model.street(geom) select geom from datal where type='"street"'
;;
update model.street set width=60
;;

insert into model.constrain(geom, constrain_type, elem_length) select geom,  'flood_plain_transect', 300 from datal where type='"transect"'
;;
insert into model.constrain(geom, elem_length) select st_force3d(geom),  300 from datal where type='"constrain"'
;;
insert into model.constrain(geom, elem_length, constrain_type, link_attributes) select st_force3d(geom),  300, 'porous', '{"transmitivity": 0.3}'::json from datal where type='"porous"'
;;
insert into model.constrain(geom, elem_length, constrain_type) select st_force3d(geom),  300, 'strickler' from datal where type='"strickler"'
;;

insert into model.storage_node(geom, zs_array)
select st_setsrid(st_makepoint(st_x(st_centroid(c.geom)), st_y(st_centroid(c.geom)), 999), 2154), '{{0, 0}, {10,1000}}'
from model.coverage as c, datap as p where st_intersects(c.geom, p.geom) and p.type='"storage"'
;;

insert into model.coverage_marker(geom)
select st_setsrid(st_makepoint(st_x(p.geom), st_y(p.geom), 999), 2154)
from datap as p where p.type='"null"'
;;

insert into model.manhole_node(geom)
select st_setsrid(st_makepoint(st_x(p.geom), st_y(p.geom), 999), 2154)
from datap as p where p.type='"manhole"'
;;

update model.constrain set geom=geom where id = (select min(id) from model.constrain)
;;

create view debug as
with l as (
select model.interpolate_transect_at(geom) as geom
from (select (st_dumppoints(geom)).geom as geom from model.coverage where domain_type='reach' ) as t
)
select row_number() over() as id, geom, st_azimuth(st_startpoint(geom), st_endpoint(geom)) from l
;;

select model.mesh(id) from model.coverage where domain_type='2d'
;;

select model.create_links(c1.id, c2.id)
from model.coverage as c1, model.coverage as c2, model.constrain as c
where st_intersects(c1.geom, c2.geom)
and st_length(st_collectionextract(st_intersection(c1.geom, c2.geom), 2)) > 0
and st_length(st_collectionextract(st_intersection(c.discretized, st_intersection(c1.geom, c2.geom)), 2)) > 0
and st_length(st_collectionextract(st_intersection(c.discretized, st_intersection(c1.geom, c2.geom)), 2)) > 0
and (c.constrain_type is null or c.constrain_type!='flood_plain_transect')
and c1.domain_type in ('reach', 'street', 'storage')
and c2.domain_type in ('reach', 'street', 'storage')
and c1.id <> c2.id
;;

select model.create_network_overflow_links(c.id)
from model.coverage as c
where c.domain_type in ('2d', 'storage', 'street', 'crossroad')
;;

"""

for statement in statements.split(';;')[:-1]:
    project.execute(statement)

project.commit()

assert(project.execute("select count(1) from model.invalid").fetchone()[0] == 32)
assert(project.execute("select count(1) from model.elem_2d_node").fetchone()[0] == 335)
assert(project.execute("select count(1) from model.strickler_link").fetchone()[0] == 5)
assert(project.execute("select count(1) from model.overflow_link").fetchone()[0] == 195)
assert(project.execute("select count(1) from model.network_overflow_link").fetchone()[0] == 18)
assert(project.execute("select count(1) from model.mesh_2d_link").fetchone()[0] == 586)
assert(project.execute("select count(1) from model.porous_link").fetchone()[0] == 6)

print('ok')

