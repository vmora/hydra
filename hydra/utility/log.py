# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import sys
import os
import datetime
import codecs

_hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")
_log_file = os.path.join(_hydra_dir, 'hydra.log')

class ConsoleProgressDisplay:
    def __init__(self):
        if sys.stdout.isatty():
            sys.stdout.write("  0%")
            sys.stdout.flush()

    def set_ratio(self, ratio):
        if sys.stdout.isatty():
            sys.stdout.write("\b"*4+"% 3d%%"%(int(ratio*100)))
            sys.stdout.flush()

    def __del__(self):
        if sys.stdout.isatty():
            sys.stdout.write("\n")
        else:
            sys.stdout.write("100%\n")
        sys.stdout.flush()

class SilentLogger:
    def __init__(self):
        pass

    def error(self, title, message, error):
        raise Exception(message+'\n'+repr(error))

    def warning(self, title, message):
        pass

    def notice(self, title, message):
        pass

    def progress(self, title, message):
        sys.stdout.write("Progress: %s "%(message))
        return ConsoleProgressDisplay()

class ConsoleLogger:
    def __init__(self):
        pass

    def error(self, title, message, error):
        raise Exception(message+'\n'+repr(error))

    def warning(self, title, message):
        sys.stderr.write("Warning: " + message + "\n")

    def notice(self, title, message):
        sys.stdout.write("Notice: " + message + "\n")

    def progress(self, title, message):
        sys.stdout.write("Progress: %s "%(message))
        return ConsoleProgressDisplay()

class LogManager:
    def __init__(self, logger=ConsoleLogger(), app_name="Hydra"):
        self.__logger = logger
        self.__title = app_name

    def error(self, message, error=None):
        with codecs.open(_log_file, 'a') as file:
            file.write(datetime.datetime.now().strftime("%d/%m/%Y %H:%M") + ': ERROR\n' + message + '\n')
            if error:
                file.write(repr(error) + "\n")
            file.write('\n')
        self.__logger.error(self.__title, message, error)

    def warning(self, message):
        with codecs.open(_log_file, 'a') as file:
            file.write(datetime.datetime.now().strftime("%d/%m/%Y %H:%M") + ': WARNING\n' + message + "\n")
        self.__logger.warning(self.__title, message)

    def notice(self, message):
        with codecs.open(_log_file, 'a') as file:
            file.write(datetime.datetime.now().strftime("%d/%m/%Y %H:%M") + ': NOTICE\n' + message + "\n")
        self.__logger.notice(self.__title, message)

    def progress(self, message):
        return self.__logger.progress(self.__title, message)

