# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
QGisUIManager manage iface interactions with gqis
"""

from __future__ import unicode_literals
import os
import re
import json
import operator
from qgis.gui import QgsMapCanvas, QgsLayerTreeMapCanvasBridge, QgsMessageBar
from qgis.core.contextmanagers import qgisapp
from qgis.core import QgsProject,  QgsDataSourceURI, QgsVectorLayer, QgsMapLayerRegistry, QgsPluginLayerRegistry, QgsLayerTreeGroup, QgsRasterLayer, QgsLayerTreeLayer, QgsVectorJoinInfo
from PyQt4.QtCore import QFileInfo, QCoreApplication, QObject, Qt
from PyQt4.QtGui import QProgressBar
from hydra.utility.tables_properties import TablesProperties
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.mylayer import MyLayerType, MyLayer

_plugin_dir = os.path.dirname(os.path.dirname(__file__))
_qml_dir = os.path.join(_plugin_dir, "ressources", "qml")
_utility_dir = os.path.join(_plugin_dir, "utility")

csv_keys = json.load(open(os.path.join(os.path.dirname(__file__), "csv_data.json")))


def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

class QGisProgressDisplay():
    """QProgressBar wrapper to provide minimal interface"""
    def __init__(self):
        self.__widget = QProgressBar()
        self.__widget.setMaximum(100)
        self.__widget.setAlignment(Qt.AlignLeft|Qt.AlignVCenter)

    def widget(self):
        return self.__widget

    def set_ratio(self, ratio):
        self.__widget.setValue(int(ratio*100))

class QGisLogger:
    def __init__(self, iface):
        self.__iface = iface

    def error(self, title, message, error):
        self.__iface.messageBar().pushMessage(title, message+'\n'+repr(error), level=QgsMessageBar.CRITICAL)
        raise Exception(message+'\n'+repr(error))

    def warning(self, title, message):
        self.__iface.messageBar().pushMessage(title, message, level=QgsMessageBar.WARNING)

    def notice(self, title, message):
        pass
        # self.__iface.messageBar().pushMessage(title, message, level=QgsMessageBar.INFO, duration=3)

    def progress(self, title, message):
        progressMessageBar = self.__iface.messageBar().createMessage(message)
        progress = QGisProgressDisplay()
        progressMessageBar.layout().addWidget(progress.widget())
        self.__iface.messageBar().pushWidget(progressMessageBar, self.__iface.messageBar().INFO)
        return progress


class QGisUIManager:
    def __init__(self, iface):
        self.__iface = iface

    def new_project(self):
        self.__iface.newProject()

    def save_project(self, project_filename):
        project = QgsProject.instance()
        project.write(QFileInfo(project_filename))

    def open_project(self, project_filename):
        self.__iface.newProject()
        project = QgsProject.instance()
        project.read(QFileInfo(project_filename))
        #@todo: implémenter le nettoyage/initialisation des layers si des modèles on été supprimés/ajoutés

    def add_vector_layers(self, project_name, model_name, json_file, arrows=False):
        uri = QgsDataSourceURI()
        db = SettingsProperties.get_db()
        pg = SettingsProperties.get_postgre_settings()
        uri.setConnection(db['host'], str(db['port']), project_name, pg['user'], pg['password'])
        __currendir = os.path.dirname(__file__)
        layer_map = json.load(open(os.path.join(__currendir, json_file)))
        properties = TablesProperties.get_properties()
        self.__layermap_process(layer_map['objects'], properties,  uri, model_name, self.__add_layer_group(model_name, None), arrows)
        QgsProject.instance().write(QFileInfo(QgsProject.instance().fileName()))

    def add_terrain_layers(self, project_name, files):
        root = QgsProject.instance().layerTreeRoot()
        terrain_group = root.addGroup(tr('terrain'))
        for terrain in files:
            raster = QgsRasterLayer(terrain, re.sub(r".*/", "", re.sub(r".*\\", "" , terrain)))
            raster.loadNamedStyle(os.path.join(_qml_dir, 'hillshade.qml'))
            QgsMapLayerRegistry.instance().addMapLayer(raster, False)
            terrain_group.addLayer(raster)
        QgsProject.instance().write(QFileInfo(QgsProject.instance().fileName()))

    def add_rain_layers(self, project_name, files):
        root = QgsProject.instance().layerTreeRoot()
        rain_group = root.addGroup(tr('rain'))
        for rain in files:
            raster = QgsRasterLayer(rain, re.sub(r".*/", "", re.sub(r".*\\", "" , rain)))
            raster.loadNamedStyle(os.path.join(_qml_dir, 'radar_rain_vrt.qml'))
            QgsMapLayerRegistry.instance().addMapLayer(raster, False)
            rain_group.addLayer(raster)
        QgsProject.instance().write(QFileInfo(QgsProject.instance().fileName()))

    def add_csv_result_layers(self, project_path, project_name, model_name, scn_name):
        root = QgsProject.instance().layerTreeRoot()
        if scn_name and scn_name != '':
            if root.findGroup('res: '+scn_name):
                scn_group = root.findGroup('res: '+scn_name)
            else:
                scn_group = root.addGroup('res: '+scn_name)

            if model_name and model_name != '':
                if scn_group.findGroup('res: '+model_name):
                    self.remove_group_layers('res: '+model_name, scn_group)
                model_group = scn_group.addGroup('res: '+model_name)

                uri = QgsDataSourceURI()
                db = SettingsProperties.get_db()
                pg = SettingsProperties.get_postgre_settings()
                uri.setConnection(db['host'], str(db['port']), project_name, pg['user'], pg['password'])

                csv_group = model_group.addGroup(tr('CSV Files'))
                csv_group.setExpanded(False)
                # Load corresponding layer
                for type in sorted(csv_keys, key=lambda k: csv_keys[k]["rank"]):
                    csv_name = ('_').join([scn_name.upper(), model_name.upper(), csv_keys[type]['csv']])
                    file_uri = os.path.join(project_path, scn_name, csv_keys[type]['folder'], (csv_name+'.csv'))
                    if os.path.isfile(file_uri):
                        # Add vector layer
                        uri.setDataSource(str(model_name), str(csv_keys[type]['table']), str('geom'), "", str('id'))
                        self.__add_layer(uri.uri(), "{}_{}".format(type.title(), scn_name.upper()), os.path.join(_qml_dir, csv_keys[type]['style']), model_group)
                        # Add CSV as layer
                        csv_uri = """file:///{}?delimiter=;""".format(file_uri)
                        csv = QgsVectorLayer(csv_uri, type, 'delimitedtext')
                        QgsMapLayerRegistry.instance().addMapLayer(csv, False)
                        csv_group.addLayer(csv)
                        # Join to table
                        target_layer = self.__get_layer(model_name, csv_keys[type]['table'])
                        if target_layer is not None:
                            joinObject = QgsVectorJoinInfo()
                            joinObject.joinLayerId = csv.id()
                            joinObject.joinFieldName = csv_keys[type]['join_name']
                            joinObject.targetFieldName = csv_keys[type]['target_name']
                            joinObject.memoryCache = True
                            target_layer.addJoin(joinObject)
                QgsProject.instance().write(QFileInfo(QgsProject.instance().fileName()))

    def remove_group_layers(self, group_name, root = QgsProject.instance().layerTreeRoot()):
        group = root.findGroup(group_name)
        if group is not None:
            for child in group.children():
                if not isinstance(child, QgsLayerTreeGroup):
                    QgsMapLayerRegistry.instance().removeMapLayer(child.layerId())
                else:
                    self.remove_group_layers(child.name(), group)
            root.removeChildNode(group)
        QgsProject.instance().write(QFileInfo(QgsProject.instance().fileName()))

    def add_w15_layer(self, project, w14_result, w15_result):
        QgsPluginLayerRegistry.instance().addPluginLayerType(MyLayerType())

        layer = MyLayer(project, w14_result, w15_result, name="Water level - {}".format(project.get_current_scenario()[1]))

        QgsMapLayerRegistry.instance().addMapLayer(layer, False)
        QgsProject.instance().layerTreeRoot().addLayer(layer)
        QgsProject.instance().write(QFileInfo(QgsProject.instance().fileName()))
        return layer

    def remove_w15_layer(self, group=None):
        QgsPluginLayerRegistry.instance().removePluginLayerType(MyLayer.LAYER_TYPE)
        QgsProject.instance().write(QFileInfo(QgsProject.instance().fileName()))

    def set_model_layers_expanded(self, model_name, expanded=True, checked=True):
        root = QgsProject.instance().layerTreeRoot()
        for child in root.children():
            if isinstance(child, QgsLayerTreeGroup) and child.name() == model_name:
                child.setExpanded(expanded)
                transparency = 0 if expanded or model_name=='project' else 50
                self.__change_group_transparency(child, transparency)
                self.__change_group_visibility(child, checked)
        QgsProject.instance().write(QFileInfo(QgsProject.instance().fileName()))

    def __change_group_transparency(self, group, transparency):
        for child in group.children():
            if isinstance(child, QgsLayerTreeLayer):
                if child.layer().type() == 0: #QgsVectorLayer
                    child.layer().setLayerTransparency(int(transparency))
                else:
                    child.layer().renderer().setOpacity(transparency/100.0)
                child.layer().triggerRepaint()
            else:
                self.__change_group_transparency(child, transparency)

    def __change_group_visibility(self, group, visibility):
        for child in group.children():
            if isinstance(child, QgsLayerTreeLayer):
                if child.layer().type() == 0: #QgsVectorLayer
                    self.__iface.legendInterface().setLayerVisible(child.layer(), visibility)
                child.layer().triggerRepaint()
            else:
                self.__change_group_visibility(child, visibility)

    def group_project_exists(self):
        tree = QgsProject.instance().layerTreeRoot()
        for leaf in tree.children():
            if isinstance(leaf, QgsLayerTreeGroup) and leaf.name() == "project":
                return True

    def __layermap_process(self, items, properties, uri, model_name, current_group, arrows=False):
        for object in items:
            if object['type']=="layer":
                uri.setDataSource(str(model_name), str(object['table']), str(properties[object['table']]['geom']), "", str(properties[object['table']]['key']))
                if arrows and (properties[object['table']]['type'] == 'line' or properties[object['table']]['type'] == 'segment'):
                    self.__add_layer(uri.uri(), tr(properties[object['table']]['name']), properties[object['table']]['style'][0:-4]+'_oriented.qml',current_group)
                else:
                    self.__add_layer(uri.uri(), tr(properties[object['table']]['name']), properties[object['table']]['style'],current_group)
            if object['type']=="group":
                self.__layermap_process(object['objects'], properties, uri, model_name, self.__add_layer_group(object['name'], current_group), arrows)

    def __add_layer_group(self, group_name, parent):
        if parent==None:
            root = QgsProject.instance().layerTreeRoot()
            group = root.addGroup(tr(group_name))
        else:
            group = QgsLayerTreeGroup(tr(group_name))
            parent.addChildNode(group)
        return group

    def __add_layer(self, path, layer_name, layer_style, group):
        if group==None:
            layer = QgsVectorLayer(path, tr(layer_name), "postgres")
            if layer_style is not None:
                layer.loadNamedStyle(os.path.join(_qml_dir, layer_style))
            if not layer.isValid():
                raise RuntimeError("invalid postgres layer (uri: {})".format(path))
            QgsMapLayerRegistry.instance().addMapLayer(layer)
        else:
            layer = QgsVectorLayer(path, tr(layer_name), "postgres")
            if layer_style is not None:
                layer.loadNamedStyle(os.path.join(_qml_dir, layer_style))
            if not layer.isValid():
                raise RuntimeError("invalid postgres layer (uri: {})".format(path))
            QgsMapLayerRegistry.instance().addMapLayer(layer, False)
            group.addLayer(layer)

    def __get_layer(self, model_name, table_name):
        maplayer = QgsMapLayerRegistry.instance().mapLayers()
        target_source = """ table="{}"."{}" """.format(model_name, table_name)
        for name, layer in maplayer.iteritems():
            if target_source in layer.source():
                return layer
        return None