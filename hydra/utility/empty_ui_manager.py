# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
EmptyUIManager is an empty ui manager used for testing purposes
"""

class EmptyUIManager:
    def __init__(self):
        pass
    def new_project(self):
        pass
    def open_project(self, project_filename):
        pass
    def close_project(self, project_filename):
        pass
    def save_project(self, project_filename):
        pass
    def add_vector_layers(self, project_name, model_name, json_file):
        pass
    def add_raster_layer(self, raster_file):
        pass
    def remove_model_layers(self, model_name):
        pass
    def remove_group_layers(self, model_name):
        pass
    def set_model_layers_expanded(self, model_name, expanded):
        pass
    def group_project_exists(self):
        return True

