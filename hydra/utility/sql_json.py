# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import hydra.utility.string as string
import os
import json

def pack(dic):
    __check_dict_value(dic)
    return json.dumps(dic)

def unpack(param_json):
    param = json.loads(param_json)
    __recursive_dict_text(param)
    return param

def __check_dict_value(dic):
    for key, value in dic.iteritems():
        if isinstance(value, dict):
            __check_dict_value(value)
        elif isinstance(value, list):
            dic[key]=value
        elif isinstance(value, int):
            dic[key]=int(value)
        else:
            dic[key]=string.get_sql_float(value)

def __text_from_json(value):
    if string.isfloat(value):
        return str(value)
    else:
        return ''

def __recursive_dict_text(dic):
    for key, value in dic.iteritems():
        if isinstance(value, dict):
            __recursive_dict_text(value)
        else:
            dic[key]=__text_from_json(value)

instances = json.load(open(os.path.normpath(os.path.join(os.path.dirname(__file__), '..', 'server', 'hydra', 'hydra.instances.json'))))

def set_fields_to_json(dialog, param_name, param_json):
    param_dict = None
    if param_name in instances.keys():
        param_properties = instances[param_name]
        for type in param_properties.keys():
            for parameter in param_properties[type]['params']:
                # for each parameter, if default value in json + field with name in .ui, set to default
                if 'default' in param_properties[type]['params'][parameter].keys():
                    if hasattr(dialog, parameter):
                        try:
                            getattr(dialog, parameter).setText(string.get_str(param_properties[type]['params'][parameter]['default']))
                        except:
                            pass

                # then, if value in param_json defined (from previous writting) set to it
                if param_json is not None:
                    param_dict = unpack(param_json)
                    if type in param_dict.keys():
                        parameters = param_dict[type]
                        if parameter in parameters.keys():
                            if hasattr(dialog, parameter):
                                try:
                                    getattr(dialog, parameter).setText(string.get_str(parameters[parameter]))
                                except:
                                    pass
    return param_dict

def build_json_from_fields(dialog, param_name):
    dic = dict()
    if param_name in instances.keys():
        param_properties = instances[param_name]
        for type in param_properties.keys():
            dic[type] = dict()
            for parameter in param_properties[type]['params']:
                # for parameter, if exists a field in form with same name, puts his value in dic
                if hasattr(dialog, parameter):
                    try:
                        dic[type][parameter] = getattr(dialog, parameter).text()
                    except:
                        dic[type][parameter] = None
    dic_json = pack(dic)
    return dic_json

