# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if layers loaded actually exist on the database

USAGE

   python -m hydra.utility.layer_test

"""

from __future__ import absolute_import # important to read the doc !

assert __name__ == "__main__"

import psycopg2
from psycopg2.extras import LoggingConnection
import logging
from ..database.database import TestProject, create_model
import json
import sys
import os
from .settings_properties import SettingsProperties

def test_object(object_, cur):
    for key, value in object_.iteritems():
        if key == "objects":
            for obj in value:
                test_object(obj, cur)
        elif key == "type" and object_["type"] == "layer":
            try:
                cur.execute("select * from model.{}".format(object_["table"]))
            except Exception as e:
                raise RuntimeError("Table {} does not exist in model".format(object_["table"]))

layer_tree = json.load(open(os.path.join(os.path.dirname(__file__), "layers_model.json")))

project_name = "layers_test"
test_project = TestProject(project_name)

db = SettingsProperties.get_db()
pg = SettingsProperties.get_postgre_settings()
con = psycopg2.connect(database=project_name, host=db['host'], port=db['port'], user=pg['user'], password=pg['password'])
cur = con.cursor()
create_model(cur, "model", 2154)

test_object(layer_tree, cur)

print "ok"


