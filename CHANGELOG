  1.22
    - Updated computation kernel (17/07/2018) [Kernel is locked and crypted]
    - Updated CRGENG (V8 08/07/2018)
    - Updated EXTRACT (V9 16/07/2018)
    - Updated WANIMOLT (20/06/2018)
    - Updated WEXPDESS (16/07/2018)
    - Updated user interface for scenario manager
    - Updated longitudinal profile interface
    - Updated static results display to be separated per scenario and per model
    - Updated dry inflow sectors management
    - Updated external tools output
    - Optimized config handling during item creation and mesh/link generation
    - Optimized config switching behaviour
    - Added option to load external data into visu graph tool
    - Added better coordinates tracker in visu tool graph
    - Added possibility to change a project workspace at opening if project directory not found
    - Fixed bug with hot start/start from rest scenario option not exclusive
    - Fixed ZQ array display in ZQ split hydrology singularity interface
    - Fixed bug on montana coefficients and associated rains generation
    - Fixed bug when adding or deleting hydrographs rerouting
  1.21
    - Database version increased to 1.3: Use the 'update' button in the project manager
    - Updated computation kernel (21/06/2018) [Kernel is locked and crypted]
        + Updated bed load sediment transport formulation
        + Updated gate/weir formulation
        + Added features to control/regulation module
        + Added controls on miscellaneous input data  in order to issue proper messages and to avoid Fortran error messages
        + Fixed small hydrology input flow rates
    - Updated "update project" feature, will now directly update to latest version
    - Updated external files data storage to work relatively to workspace if applicable
    - Updated configuration feature: multiple configs are callable in the same scenario
    - Updated configured item display: no markers when in default
    - Updated toolbar: [model creation tools]|[model exploitation tools]
    - Updated "display results" to not display blue when H=0
    - Updated scenario manager GUI
    - Added tool to display a table with min/max values of results on given item
    - Added option to use a reference scenario for computation
    - Added hydrograph rerouting
    - Added menu "utility" with specific tools (Extract, Animo, Crgeng)
    - Added possibility to choose file name when exporting a project
    - Added tool to split constrain lines
    - Added graph to dry inflow modulation curve
    - Added tool to import shapefiles in 'work' database and to import data from 'work' to project (BETA)
    - Added configuration to river cross section profiles
    - Added option to river reach singularities to select full section discharge or river bed discharge for headloss computation
    - Sorted scenarios and configurations lists/boxes and project manager by name
    - Changed default value for computation starting date to 01/01/2000 - 00:00:00
    - Fixed bug with "TypeError: write() takes exactly 2 arguments (1 given)"
    - Fixed bug with clone scenario
  1.20
    - Database version increased to 1.2.1: Use the 'update' button in the project manager
    - Updated computation kernel (18/04/2018) [Kernel is locked and crypted]
        + Updated handling of computation and output options
        + Added an option for alternative formulation for gates and weirs
        + Fixed bug on overflow links
    - Updated users documentation
    - Updated configurations:
        + Can now properly remove configuration from a given object
        + Configured items forms only displays current configured items
        + Layer only displays current configured items (Must "Reload model layers" for this to work)
        + Fixed bug where creating an object with active configuration could result in a configurated object
    - Added "Reach" to option in Computation option Strickler
    - Fixed bug with scenario grouping (used to not be able to control scenario running order)
        From now on, scenarios will be run in alphabetical order
    - Fixed bug with included documentation
    - Fixed bug on catchment edition from version 1.2.0
    - Fixed bug on elem2D edition from version 1.2.0
    - Fixed display bug with "Database settings" page in settings
  1.06
    - Database version increased to 1.2.0: Use the 'update' button in the project manager
    - Updated table $model.catchment_node: all parameters in individual columns.
        /!\ WARNING: At update, only DEFAULT CONFIGURATION parameters will be saved. Other CONFIGURATIONS on CATCHMENT_NODES will be DROPPED!
    - Updated computation kernel (15/03/2018) [Kernel is locked and crypted]
        + Forced restriction on regulation flow changes for "gate" objects
        + Optimized computation methods for objects "bridge", "dh", "racc", "bradley", "weir"
        + Added option for external rain files access (usefull for long chronicles computation)
        + Added soil initial conditions reading, computed in a preliminary scenario
        + Added features to control and regulation module
        + New CRGENG (V8) adapted to hydra
    - Added option 'Define K' for catchment nodes runoff type
    - Added column 'unit_correction_coef' to radar_rain. Not editable via UI for the moment
    - Added back *branch export in geom.dat
    - Added icon to radar_rain animation button
    - Added hydrology rstart export in opsor.dat file
    - Fixed bug with unneeded configurations poping up on some items (storages)
    - Fixed bug with SRID detection at import of hydra .sql project file
    - Fixed bug preventing domain_2d_marker deletion
    - Fixed bug with hydrology pipes not exported
    - Fixed multiple bugs with radar rain (units, *PRAD export)
    - Fixed bug with gaged rainfall units label
    - Fixed bug at scenario cloning
    - Fixed bug at hydra startup with shapely dependency
  1.05
    - Database version increased to 1.1.2: Use the 'update' button in the project manager
    - Updated computation kernel (02/02/2018)
    - Added radar rain support:
        + Load .tif files into hydra rain manager to form a .vrt rain file
        + Display rain evolution with a timeslider
        + Compute rain hyetogram in catchments
    - Added option in settings to control the number of images/sec during animations
    - Added option in settings to control automatic updates and manually trigger for coverages, branches and street links generation
    - Updated borda singularities and links UI behaviour. May cause some errors at edition (clear "param" field of object to solve)
    - Updated error feedback during first export of objects at start of computation
    - Updated boundary condition link generation for better generation
    - Fixed bug with bad terrain data reading at link generation during mesh
    - Fixed bug in QCap computation in pipes
    - Fixed bug where manhole insert on pipe would mess geometries type
    - Removed '*branch' keyword export temporarily due to errors on a few databases
  1.04
    - Database version increased to 1.1.1: Use the 'update' button in the project manager
    - Added "*branch" keyword to geom.dat file at export with manholes coordinates along a wastewater branch
    - Added option to save graphs in visu_tool + saves comboboxes state when scenario or model changes
    - Updated valley geom handling, allowing user to select no constrain
    - Updated longitudinal profile tool
    - Updated project manager import script. Will now check beforehand if file data version is available among installed hydra extension versions
    - Fixed bug on export format of overflow_link with Z break mode
    - Fixed errors with corrupted projects in project_manager
    - Fixed bug with links at export: "warning: segment AB mal orienté pour la liaison ... dans le fichier L2D"
    - Fixed bug where configured singularities were not listed in general configuration tool
    - Fixed bug with configuration not working if more than 1 configuration exists and some objects are not configured
    - Fixed bug where after computation, displayed configuration was not coherent with indicated configuration in toolbar
    - Fixed bugs at pipe_link creation/edit that did not update manholes validity/branch generation
    - Fixed bug that could happen at singularity creation on river_node
  1.03
    - Database version increased to 1.1.0: Use the 'update' button in the project manager
    - Updated computation kernel (03/12/2017) [Kernel is locked and crypted]
        + GATE and WEIR:
            - New formulation (back to Hydracity's formulation).
            - Transitioning laws between flow modes physically more coherent.
        + BRIDGE: Enforce continuity of flow rate when changing between flow types.
        + TANK BC: Takes into account initial condition (Zini) at computation start.
        + Z(T) BC: Fixed bug in complex/global computation mode.
        + BORDA HEADLOSS: Updated derivative terms computation. Algorithm is now more reliable.
        + MANHOLE NODE: When attached to pipe with open (channel) section, outing hole is replaced by a very wide weir.
        + MODEL CONNECT: Fixed bug.
        + REGULATION and CONTROL: New functionalities
            - Command 'DATA': Variables initialization
            - Command 'NTERPN': Computes an mathematic expression in one instruction
            - Commands 'ELSEIF' and 'ELSE': Expand options available from commands 'IF' and 'ENDIF'
            - Action 'CLQ0' on object 'Constant Inflow': Allows waterflow modulation from control file
            - Regulation function '#PID': Allows pumps from 'Pump Derivation' object to be regulated with PID defined in control file
            - Update on meta-commands instructions
        + PARDISO SOLVER: Strengthened the algorithm so it keeps on running even when a problem is raised by Pardiso solver.
    - Added configuration functionality on nodes, singularities and links
    - Added dynamic display of water levels in 2D domains (Flow velocity arrows to come)
    - Added project creation date display and sorting in project manager
    - Added possibility to select 'null' as constrain line type once changed to something else
    - Added access to rain manager from scenario manager
    - Implemented model deletion and importation from .sql hydra file (may drop some model's layers, use "Reload current model" if it happens)
    - Fixed bug where QGIS kept handle on csv files
    - Fixed bug with Borda Headloss links
    - Topo module now uses temp directory for topo_in.dat file
  1.02
    - Database version increased to 1.0.1: Use the 'update' button in the project manager
    - Added option to choose a custom pipe length (only for pipe links)
    - Added network overflow generation (2D, street and storage coverages -> manholes)
    - Updated some errors feedback for easier understanding by user
    - Fixed bug with workspace when working on distant database
    - Fixed bug with no regulation option on regulated gates
    - Removed model layers auto handling by UI (slow when working on multiple models)
  1.01
    - Updated computation kernel (05/10/2017) [Kernel is locked and crypted]
    - Updated users doc (06/10/2017)
    - Fixed UI bug on bridge graph display
    - Fixed error at routing hydrology link import from CSV file
  1.0
    - WARNING: Computation kernel updated for a locked one. You need a key to run computation
    - Updated computation kernel (29/09/2017)
    - Added license headers to all .py and .sql files for external diffusion
    - Added license and contact information to Hydra > About menu
  0.77
    - Database version increased to 1.0.0: Use the 'update' button in the project manager
    - Added tool to display static results (CSV) automatically in UI
    - Added a log on computation kernel and topo (profile generation)
    - Added tool to split or merge river reach
    - Updated computation kernel (21/09/2017)
    - Reworked UI for cross sections
    - Removed downstream connectivity of pipe on station nodes
    - Fixed L2D.dat file generation
    - Fixed several bugs in street handling mechanics
    - Fixed buf in topo (profile generation)
  0.76
    - Added file *_link.dat export
    - Updated computation kernel (13/09/2017)
  0.75
    - Added log journal to export calcul script: activity logged in /user/.hydra/hydra.log file
    - Updated computation kernel (11/09/2017)
    - Fixed bug at profile generation from MNT and points
  0.74
    - Added ZQ split editor (small data chages, contact dev team if error around this object)
    - Added users documentation in HYDRA menu
    - Added _link.dat file export
    - Updated computation kernel
    - Fixed bug when generating valley cross section from points (topo.exe)
    - Fixed bug at crossroad coverage geometry export
    - Fixed bug when running an invalid project and using shortcut to validity manager
    - Fixed bug at plugin initialisation after first setup
    - Normalized projects name
  0.73
    - Database version increased to 0.0.29: Use the 'update' button in the project manager
    - New feature : domain 2D grouping for mesh elements
    - Revamped borda objects
    - Added transport and quality results to visualisation tool
    - Updated kernell following Domain 2D and Borda updates
    - Fixed bug at PTR river node change
    - Fixed bugs in profile tools
  0.72
    - OPT SOR export updated to match scenario manager
    - Copy project available
    - Find project SRID at import of dump file
    - Fixed bug when exporting geoms with shorter tables than required
  0.71
    - Database version increased to 0.0.28: Use the 'update' button in the project manager
    - Reworked project manager
    - Fixed broken profile generation from points
    - Automatic generation of links between domains extended
    - Updated computation kernell
  0.70
    - WARNING: Database version increased to 0.0.27
    - Updated river cross section tool
    - Patched link generation reach -> mesh (speed up)
    - Added possibility to set rk, rk maj and sinuosity to 0 on river profiles (kernell will then use the upstream one)
  0.69
    - WARNING: Database version increased to 0.0.26
    - Reworked terrain handling
    - Fix scenario manager
    - Add mass meshing tools
    - Updated pumps, pipes and tank BC
    - Added connectivity validity around nodes/pipes
    - Add zoom to validity manager
  0.68
    - WARNING: Database version increased to 0.0.25
    - Updated computation kernell
    - Updates on tables:
        + Gate singularity (width instead of area)
        + Regul gate singularity (z ceiling instead of h max)
        + Mesh 2D link (lateral contraction coef added)
        + Storages (Contour inherited from coverage)
    - Removed tables:
        + River to 2D link (=> Overflow),
        + Deriv qz link (=> Deriv pump),
        + Urban storage (=> Storage),
        + Crossroad to urban link (=> Overflow)
    - Menus overhaul
    - Overhaul of terrain manager
    - Added handling of points for profile computation
    - Display of invalidities reason in validity manager
    - Table TZ_BC: add possibility of external file data
    - Network overflow created
    - Better behaviour of crossroads, streets and street links
    - Possibility to select ESPG at project creation
    - Update on scenario manager
  0.67
    - Fixed bug at coverage export
    - Fixed bug on storages
    - Better GDAL handling (not necessary depending on plugin usage)
  0.66
    - Updated kernell
    - Modified overflow link generation with meshing tool
    - Allowed routing_hydrology_link to connect to storage at downstream point
    - Removed gdal_array use
  0.65
    - Fixed bugs around overflows
    - Fixed bugs around project_manager
    - Fixed inconsistencies in export and export carto
  0.64
    - WARNING: Database version increased to 0.0.24
    - Updated longitudinal profile tool
    - Updated kernel
    - Integration of meshlayer tool for dynamic 2D vicualisation
    - Implemented overflow link tool
    - Implemented scenario grouping
    - Add Qcap calculation as default for pipe_link
  0.63
    - WARNING: Database version increased to 0.0.22
    - Display cross_section info in pipe/river profile table
    - Updated validity conditions for nodes in stations or singularities on reach/branch ends
    - Allow to reload project layers
    - Display river node Z as info
    - Updated branch algorithms
  0.62
    - Improved longitudinal profile tool
  0.61
    - Wait cursor while performing certain time consuming actions
    - Copy paste in geometry arrays
    - Fixed bug on run calculation button
  0.60
    - Complete overhaul of meshing tools and mechanics (WARNING: existing DBs need updates for this to work)
    - Better autosetup of python DLL at plugin start
  0.59
    - Implemented quicker access to project items (see project tree widget)
    - Fixed bug with valley-typed river profiles
    - Implemented terrain manager (WARNING: existing DBs need updates for this to work)
    - Fixed wonky UI refreshing mechanics
    - Fixed bug in scenario manager with hydrol_time_step parameter
  0.58
    - Implemented search > zoom
    - Minor fix in .CSV file import
    - Updated settings for server connection
  0.57
    - Rework of project manager : export tool working again
    - Updated mesh scripts (server-side)
  0.56
    - New meshing tools
    - Minor improvements in UI
    - Implemented scenario cloning
    - Implemented model ordering
  0.55
    - Several minor UI changes
    - Fixed bug in sectors/HY_BC gestion
    - Implemented permeable soil option in catchment(netflow)
    - Implemented forms for borda_headlosses
    - Fixed graph UI in ZQ_BC form
  0.54
    - Implemented model connect form
    - Fixed bug in pump link validity
  0.53
    - Fixed bug when running calculation from toolbar
    - Fixed bug at visualisation
    - Fixed bug with workspace display in project manager
  0.52
    - Added a searching tool for objects
  0.51
    - Added scenario, rain,... importation from csv file
    - Reworked selet tool
    - Fixed bug in time control
    - Fixed bugs in scenario manager
  0.50
    - Auto setup of Python3 'hydra' Lib and PostgreSQL 'hydra' extension at plugin start
  0.49
    - Updated Database operating mode (WARNING: existing DBs need updates for this to work)
    - Added possibility of prolonging a reach
    - Fixed bug where layers exteriors to hydra plugin where lost on saving
    - General improvements on scenario manager
  0.48
    - Updated carto generation
    - Updated DB constraints on project tables
  0.47
    - Fixed blocking bug at project creation
  0.46
    - Implemented QQ split hydrology forms
    - Improved visu tool
  0.45
    - Fixed bug at settings opening
    - Added some specific node/links edition tools
    - Added finished popup at the end of a model import
  0.44
    - Started carto generation when calculating
    - Added hot start options in scenarios
    - Implemented cascade deletion if needed
  0.43
    - Updated kernel
    - Added possibility to customize database connection parameters in settings
    - Added Z/Q mode, min/max display and selection of multiple branches to profile display
    - Added orientation of links an reaches option
    - Fixed bug on calculation launch when more than 1 model in project
    - Added all graphics displays in forms
    - Implemented pump and deriv_pump forms
    - Updated dry inflow/hydrograph relation mecanics
    - Implemented access to geometry editor from pipe/river cross section
  0.42
    - WARNING: Database version increased to 0.0.18
    - Fixed profile display errors
    - Implemented ZUI forms
    - Minor fixes
  0.41
    - Implemented paste in Gage Rainfall table
  0.40
    - WARNING: Database version increased to 0.0.17
    - Implemented Bradley and Bridge edition
    - Regrouped regulated gates
    - Implemented small graphes display on geometries/rain editions
  0.39
    - WARNING: Database version increased to 0.0.15
    - Fixed CBV import
    - Improved current model/scenario management and display
    - Improved profile tool
    - Improved visualisation tool
  0.38
    - Write log file each session in /.hydra/hydra.log
    - Centralized time control UI in toolbar
    - Fixed import on cyclic in TZ BC
    - Fixed select tool bug
  0.37
    - WARNING: Database version increased to 0.0.14
    - Improved profile tool
    - Major updates on GUI (meshing tools relocated in modeling dock, modelling dock created on the left under layer manager)
  0.36
    - WARNING: Database version increased to 0.0.13
    - Auto creation of river node when placing singularity on river reach
    - Implemented workspace path for each project
    - Cleared UI, improved some forms/displays (rainfalls, geoms, scenarios)
    - Improved profile tool
  0.35
    - Fixed .CSV files import errors
    - Updated calculation Kernel
    - Fixed EditTool bug
  0.34
    - Catchment contour and node correctly displayed
    - Upgraded profile tool for sewage water branches
  0.33
    - Updated result reader with w14 file
    - Added forms : weirbc, tankbc, markerbranch, hydrauliccut, paramheadloss, porouslink, derivweirlink, derivqzlink
  0.32
    - Fixed errors during calculation kernel call
    - Added connector hydrology button
    - Improved visu tool (W15 results)
    - Added snap distance in settings
    - Added selection on control node for ZRegul gate link
    - Updated UI for more uniformity
    - Fixed bug in geometry cross section at import
  0.31
    - Updated calcul kernel
  0.3
    - Added ZRegul Gate Link and QRegul Gate Link forms
  0.29
    - Updated project manager (export/import/copy models)
    - Added profile tool
  0.28
    - Set inactive models opacity to 50%, collapsed inactive models on model switch
