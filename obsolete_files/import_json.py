# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
import database in json/geojson format

USAGE

   python -m hydra.database.import_json project_database file.[zip|geojson]
"""

from qgis.core import *

import json
import tempfile
import re
import os
from functools import partial
import traceback
import shutil
from ..utility.log import LogManager
from ..utility.sql_json import instances as INSTANCES
#from ..utility.timer import Timer
from zipfile import ZipFile

__currendir = os.path.dirname(__file__)

GEOM_COLS = dict({typ+"_"+t: inst['geom']
        for t in ['node', 'link', 'singularity']
        for typ, inst in INSTANCES[t+'_type'].iteritems()
        },
        **{
            'river_cross_section_profile': 'geom',
            'catchment': 'geom',
            'reach': 'geom',
            'station': 'geom',
            'domain_2d': 'geom'
            })


def import_zip(filename, project, log=LogManager()):
    tmpdir = tempfile.mkdtemp()
    ZipFile(filename).extractall(tmpdir)
    models = [o for o in os.listdir(tmpdir)
            if os.path.isdir(os.path.join(tmpdir, o))
            and o != 'project']
    # @todo import project files
    tables = [
            "configuration",
            "generation_step",
            "river_cross_section_geometry",
            "valley_cross_section_geometry",
            "closed_parametric_geometry",
            "open_parametric_geometry",
            "valley_cross_section_topo_geometry",
            "catchment",
            "reach",
            "station",
            "domain_2d",
            "spillway_group"
             ]+[subtyp+'_node' for subtyp in INSTANCES["node_type"].keys()
             ]+["river_cross_section_profile"
             ]+["hydrograph_bc_singularity"
             ]+[subtyp+'_link' for subtyp in INSTANCES["link_type"].keys()
             ]+[subtyp+'_cross_section' for subtyp in INSTANCES["cross_section_type"].keys()
             ]+[subtyp+'_singularity' for subtyp in INSTANCES["singularity_type"].keys()
                     if subtyp != "hydrograph_bc"]
    progress = log.progress("loading")
    tot = len(models)*len(tables)
    i = 0
    for model in models:
        if model not in project.get_models():
            project.add_new_model(model)
            project.commit()

        project.execute("alter table {schema}.reach disable trigger {schema}_reach_after_insert_trig".format(schema=model))
        project.execute("alter table {schema}._pipe_link disable trigger {schema}_pipe_after_insert_trig".format(schema=model))
        project.execute("alter table {schema}._river_cross_section_profile disable trigger {schema}_river_cross_section_profile_after_insert_trig".format(schema=model))
        project.commit()
        for t in tables:
            i += 1
            progress.set_ratio(float(i)/tot)
            f = os.path.join(tmpdir, model, t+'.geojson')
            if os.path.exists(f):
                import_json(f, project)

        project.execute("alter table {schema}.reach enable trigger {schema}_reach_after_insert_trig".format(schema=model))
        project.execute("alter table {schema}._pipe_link enable trigger {schema}_pipe_after_insert_trig".format(schema=model))
        project.execute("alter table {schema}._river_cross_section_profile enable trigger {schema}_river_cross_section_profile_after_insert_trig".format(schema=model))
        project.commit()

    shutil.rmtree(tmpdir)

def import_json(filename, project, log=LogManager()):
    """import one json file into project"""

    gdal_log, os.environ["CPL_LOG"] = tempfile.mkstemp()#@todo remove when gdal version > 2.0

    #print filename

    def ref_extract(schema, table, field):
        "returns integer pk referenced by name"

        def unref_(schema, table, field, value):
            if not value:
                return value
            sql = """
                   select id from {schema}.{table} where name='{name}'
                   """.format(
                       schema=schema,
                       table=table,
                       name=value
                       )
            res = project.execute(sql).fetchone()
            if not res:
                raise Exception("no element named '{}' in {}.{}".format(value, schema, table))
            return res[0]

        # add references that are difficult to parse ()
        if table in ("zq_split_hydrology_singularity", "qq_split_hydrology_singularity"):
            if field in ("downstream", "split1", "split2"):
                return partial(unref_, schema, '_link', field)
        elif table in ("routing_hydrology_link", "connector_hydrology_link"):
            if field == "hydrograph":
                return partial(unref_, schema, 'hydrograph_bc_singularity', field)
        elif table in ("reservoir_rsp_hydrology_singularity", "reservoir_rs_hydrology_singularity"):
            if field in ("drainage",  "overflow"):
                return partial(unref_, schema, '_link', field)
        elif table == "regul_sluice_gate_singularity":
            if field == "control_node":
                return partial(unref_, schema, '_node', field)

        m = re.match("(.*)_(node|link|singularity|cross_section)$", table)
        if m:
            subtyp, typ = m.group(1), m.group(2)
            if typ+'_type' in INSTANCES:
                if typ == "cross_section" and field == "id_pipe_link":
                    return partial(unref_, schema, '_link', field)
                if typ == "cross_section" and field in ("id_river_cross_section_profile_down", "id_river_cross_section_profile_up"):
                    return partial(unref_, schema, 'river_cross_section_profile', field)
                for c in INSTANCES[typ+'_type'][subtyp]['decl']:
                    spl = c.split()
                    if c[0] != ' ':
                        if spl[0]==field and 'references' in spl:
                            return partial(unref_, schema, spl[spl.index('references')+1].replace("(id)", "").split('.')[1], field)
        return lambda x: x

    assert filename[-8:] == '.geojson'
    dirname, table = os.path.split(filename[:-8])
    dirname, schema = os.path.split(dirname)

    if schema != 'project' and schema not in project.get_models():
        project.add_new_model(schema)
        project.commit()

    assert schema in project.get_models()

    has_geom = table in GEOM_COLS

    #timer = Timer()

    try:
        src = QgsVectorLayer(filename, table, 'ogr')
        dst_uri = "dbname='{dbname}' key='id' table=\"{schema}\".\"{table}\" {geom} sql=".format(
            dbname=project.name,
            schema=schema,
            table=table,
            geom='({})'.format(GEOM_COLS[table]) if has_geom else ''
            )
        dst = QgsVectorLayer(dst_uri, table, 'postgres')

        assert dst.isValid()
        assert src.isValid()

        src.selectAll()
        pasted = []
        src_fields = [field.name() for field in src.pendingFields()]
        dst_fields = [field.name() for field in dst.pendingFields()]
        extract = {field: ref_extract(schema, table, field) for field in src_fields}
        for feat in src.selectedFeatures():
            new_feat = QgsFeature()
            attr = [extract[name](feat[name]) if name in src_fields else dst.dataProvider().defaultValue(i) for i, name in enumerate(dst_fields)]
            new_feat.setAttributes(attr) # @todo: use default values
            if has_geom and feat.geometry():
                new_feat.setGeometry(feat.geometry())
            pasted.append(new_feat)
    except:
        traceback.print_exc()
        raise Exception("error encoutered in "+schema+"."+table)
    #print timer.reset('convert features')
    dst.startEditing()
    dst.beginEditCommand('import')
    ok, feats = dst.dataProvider().addFeatures(pasted)
    if not ok:
        log.warning("\n".join(dst.dataProvider().errors()))
        raise Exception("cannot commit changes to "+schema+"."+table)

    dst.endEditCommand()
    project.commit()
    #print timer.reset('commit')
    project.vacuum_analyse(schema+"."+table)
    #print timer.reset('vacuum')
    os.remove(os.environ["CPL_LOG"])


if __name__ == "__main__":
    from qgis.core import QgsApplication
    import sys
    from ..project import Project
    app = QgsApplication(sys.argv, False)
    QgsApplication.initQgis()
    project = Project.load_project(sys.argv[1])
    if sys.argv[2][-8:] == '.geojson':
        import_json(sys.argv[2], project)
    elif sys.argv[2][-4:] == '.zip' :
        import_zip(sys.argv[2], project)

