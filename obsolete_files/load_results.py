# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import numpy
import time
import sys
import os
from hydra.project import Project

if False:
    result_dir = sys.argv[1]

    # for a given project and a given scenario


    scenario = "results"
    project = Project.load_project('ill')
    project.execute("drop schema if exists {} cascade".format(scenario))
    project.execute("create schema {}".format(scenario))

    project.execute("""
        create table {scenario}.time_step(
            id serial primary key,
            time float
            )
            """.format(scenario=scenario, srid=project.srid))

    project.execute("""
        create table {scenario}.point_z_max(
            id serial primary key,
            z float,
            geom geometry(POINTZ, {srid})
            )
            """.format(scenario=scenario, srid=project.srid))

    project.execute("""
        create table {scenario}.surf_z_max(
            id serial primary key,
            z float,
            h float,
            geom geometry(POLYGONZ, {srid})
            )
            """.format(scenario=scenario, srid=project.srid))

    #@todo loop over models
    basename = os.path.join(result_dir, 'Q100-83a_ILL') # depend on model name and scenario_name
    model_name = 'ill'

    project.set_current_model(model_name)
    model = project.get_current_model()

    with open(basename+'.W15', 'rb') as f:
        nb_step, nb_node, nb_link = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
        #print nb_step, nb_node, nb_link
        nodes = [(f.read(24), f.read(8))[0].strip() for i in range(nb_node)]
        node_map = {k: v for k, v in model.execute("""
            select name, id from $model._node where node_type in ('river', 'manhole')
            """).fetchall()}
        elem_2d_map = {k: v for k, v in model.execute("""
            select name, id from $model._node where node_type='elem_2d'
            """).fetchall()}
        storage_map = {k: v for k, v in model.execute("""
            select name, id from $model._node where node_type='storage'
            """).fetchall()}
        times = []
        for i in range(nb_step):
            results = numpy.frombuffer(f.read(32*(nb_node+nb_link+1)), dtype=numpy.float32)
            times.append((float(results[0]),))

        node_values = []
        surf_values = []
        f.seek(32, 1) # time
        for i in range(nb_node):
            val = numpy.frombuffer(f.read(32), dtype=numpy.float32)
            if nodes[i] in node_map:
                node_values.append((float(val[1]), node_map[nodes[i]]))
            elif nodes[i] in elem_2d_map:
                surf_values.append((float(val[1]), float(val[1]), elem_2d_map[nodes[i]], float(val[1]), float(val[1]), elem_2d_map[nodes[i]]))
            elif nodes[i] in storage_map:
                surf_values.append((float(val[0]), float(val[0]), storage_map[nodes[i]], float(val[0]), float(val[0]), storage_map[nodes[i]]))

        project.executemany("""
            insert into {scenario}.time_step(time)
            values (%s)""".format(scenario=scenario), times)

        project.executemany("""
            insert into {scenario}.point_z_max(geom, z)
            select geom, %s from {model}._node
            where id=%s""".format(scenario=scenario, model=model_name),
            node_values)

        project.executemany("""
            insert into {scenario}.surf_z_max(geom, z, h)
            select contour, %s, %s-zb as h from {model}.elem_2d_node
            where id=%s
            union
            select contour, %s, %s-zs_array[1][1] as h from {model}.storage_node
            where id=%s
            """.format(scenario=scenario, model=model_name),
            surf_values)

        project.commit()










    #with open(basename+'.W14', 'rb') as f:
    #    nb_step, nb_sing, nb_link = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
    #    singularities = [(f.read(24), f.read(8))[0].strip() for i in range(nb_sing)]
    #    links = [(f.read(24), f.read(8))[0].strip() for i in range(nb_link)]
    #    # go to last record: max values
    #    f.seek(32*nb_step*(nb_sing+nb_link+1))
    #    #print numpy.frombuffer(f.read(32), dtype=numpy.float32)
    #
    #    #print numpy.frombuffer(f.read(32*(nb_sing+nb_link+1)), dtype=numpy.float32).reshape(-1, 8)
    #
