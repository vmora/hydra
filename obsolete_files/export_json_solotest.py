# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from qgis.core import *

import sys

from ..project import Project
from .database import TestProject
from .export_json import export_json, INSTANCES, columns, ref_to_name
from .import_json import import_zip, GEOM_COLS
from ..utility.tables_properties import TablesProperties
import json
import os
import re
from ..utility.timer import Timer

from import_model import import_model_csv
import tempfile

timer = Timer()
# initialize QGis for dataproviders
app = QgsApplication(sys.argv, False)
QgsApplication.initQgis()
print timer.reset('init qgis')


def run_test(db, zipfile):
    project = Project.load_project(db)
    print timer.reset('load project')
    export_json(zipfile, project)
    print timer.reset('export json')
    tp = TestProject('import_json_test', True)
    test_project = Project.load_project('import_json_test')
    print timer.reset('create test project')
    import_zip(zipfile, test_project)
    print timer.reset('import json')

    print "comparing reloaded project to original"
    for table, data in TablesProperties.get_properties().iteritems():
        if data["source"] == "project":
            pass
        elif table not in ('branch', 'coverage', 'close_point'):
            for model in project.get_models():
                cols = '*'
                m = re.match(r'(.*)_(node|link|singularity|cross_section)$', table)
                if m:
                    subtyp, typ = m.group(1), m.group(2)
                    instance = INSTANCES[typ+'_type'][subtyp]
                    cols = ", ".join([ref_to_name(c, model, table)
                            for c in
                                ['name', 'configuration']
                                + columns(instance, instance['geom'], subtyp)
                                +["st_astext({})".format(instance['geom'])]
                                + (['generated references $model.generation_step']
                                    if typ!='singularity' else [])
                                ])

                ref = project.execute("select {} from {}.{} order by name".format(cols, model, table)).fetchall()
                res = test_project.execute("select {} from {}.{} order by name".format(cols, model, table)).fetchall()
                for rf, rs in zip(ref, res):
                    if rf[1:] != rs[1:]:
                        print "<", rf[1:]
                        print ">", rs[1:]
                        raise Exception("found diff in {}.{}".format(model, table))

                invalidities = test_project.execute("""
                    select id, name, subtype::varchar||'_'||type::varchar, reason
                    from {}.invalid
                    """.format(model)).fetchall()
                if len(invalidities):
                    sys.stderr.write("warning: invalidities in {}:\n".format(model))
                    sys.stderr.write("\n".join(("\t"+"\t".join((str(j) for j in i)) for i in invalidities)))
                    sys.stderr.write("\n")

if len(sys.argv) == 3:
    run_test(sys.argv[1], sys.argv[2])
else:
    data_dir = os.path.join(os.path.dirname(__file__), 'test_data', 'light_projects')

    tp = TestProject('export_json_test')
    project = Project.load_project('export_json_test')
    project.add_new_model('reso')
    project.set_current_model('reso')
    import_model_csv(os.path.join(data_dir, 'reso.csv'), project, project.get_current_model(), 'reso')
    project.add_new_model('riv')
    project.set_current_model('riv')
    import_model_csv(os.path.join(data_dir, 'riv.csv'), project, project.get_current_model(), 'riv')

    run_test('export_json_test', tempfile.mkstemp('.zip')[1])



print 'ok'
