# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
Process hydra .z1, .z2 and tiff tiles

USAGE

    coverage tif 27572 tile01.asc tile02.asc ...

        create tif tiles in current directory

    coverage elev file.z1 file.z2 tile01.tif tole02.tif ...

        create tif obtained by the difference between elevation in result file and
        elevation in terrain tiles (.tif)

    coverage cover level.vrt file.z1

        create polygones for levels 0.25 0.5 1 and 2m
"""

import rtree
import getopt
import sys
import os
import numpy
import struct
from math import ceil, log
from itertools import chain
from subprocess import Popen, PIPE
from osgeo import gdal, osr, ogr
from shapely import wkb
from shapely.geometry import MultiPoint, Point, LineString, Polygon, MultiLineString
from shapely.ops import polygonize, linemerge
from xml.etree import ElementTree
from OpenGL.GLU import *
from OpenGL.GL import *
from OpenGL.GL import shaders
from OpenGL.GL.EXT.framebuffer_object import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtOpenGL import QGLPixelBuffer, QGLFormat, QGLContext
from hydra.utility.timer import Timer

LEVELS = [0.001, 0.25, 0.5, 1, 2]

def concat(x):
    ret = []
    for l in x:
        ret += l
    return ret

def run_(cmd):
    if os.name == 'posix':
        out, err = Popen(cmd).communicate()
    else:
        out, err = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True).communicate()
    if err:
        sys.stderr.write(err+'\n')
        raise Exception("error in {}".format(" ".join(cmd)))

if __name__=='__main__':

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "h",
                ["help"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    if 'tif' == args[0]:
        srid = int(args[1])
        for asc in args[2:]:
            run_(['gdalwarp']
                + ['-ot', 'Float32']
                + (['-s_srs', 'EPSG:%d'%(srid)] if srid else [])
                + ['-t_srs', 'EPSG:%d'%(srid),
                   asc,
                   '.'.join(os.path.split(asc)[1].split('.')[:-1])+'.tif'])

    elif 'elev' == args[0]:
        timer = Timer()
        assert(len(args)>=4)
        app = QApplication(sys.argv)
        fmt = QGLFormat()
        width, height = None, None

        with open(args[1], 'rb') as f:
            nb_nodes, nb_elem  = numpy.frombuffer(f.read(8), dtype=numpy.int32)
            vtx = numpy.array(numpy.frombuffer(f.read(8*3*nb_nodes), dtype=numpy.float64).reshape(-1,3), dtype=numpy.float32)
            tri = numpy.frombuffer(f.read(4*3*nb_elem), dtype=numpy.int32).reshape(-1,3) - 1

        #with open(args[2], 'rb') as f:
        #    nb_nodes, nb_th  = numpy.frombuffer(f.read(8), dtype=numpy.int32)
        #    date = nb_th/2 # - 1
        #    f.seek(date*4*(1+nb_nodes), 1)
        #    vtx[:,2] = numpy.frombuffer(f.read(4*(1+nb_nodes)), dtype=numpy.float32)[1:]

        #correction 20m
        #vtx[:,2] -= 38

        amin, amax = 100, 200 #numpy.min(vtx[:,2]), numpy.max(vtx[:,2])

        pixBuf = None

        level_files = []

        # get input raster
        for fil in args[3:]:
            if fil[-10:] == '.level.tif': # file previously generated
                continue
            mvtx = numpy.array(vtx)
            src_ds = gdal.Open(fil)
            inband = src_ds.GetRasterBand(1)
            nodata = inband.GetNoDataValue()

            altitudes = inband.ReadAsArray()

            if (width, height) != (src_ds.RasterXSize, src_ds.RasterYSize):
                width, height = src_ds.RasterXSize, src_ds.RasterYSize
                pixBuf = QGLPixelBuffer(QSize(width, height), fmt)
                pixBuf.makeCurrent()

                glDepthMask(True)
                glEnable(GL_DEPTH_TEST)

                glEnableClientState(GL_VERTEX_ARRAY)

                glMatrixMode(GL_PROJECTION)
                glLoadIdentity()
                glColor3f(1,0,0)

            gt = src_ds.GetGeoTransform()

            xmin, ymin, xmax, ymax = gt[0], gt[3]+height*gt[5], gt[0]+width*gt[1], gt[3]

            #print "bbox", xmin, ymin, xmax, ymax
            #in_ext = numpy.logical_and(
            #    numpy.logical_and(mvtx[:,0] >= xmin , mvtx[:,0] <= xmax),
            #    numpy.logical_and(mvtx[:,1] >= ymin, mvtx[:,1] <= ymax))
            #print numpy.min(mvtx[in_ext,2]), numpy.max(mvtx[in_ext,2])

            glMatrixMode(GL_MODELVIEW)
            glLoadIdentity()

            glScalef(2./(xmax-xmin), -2./(ymax-ymin), 2./(amax-amin))
            glTranslatef(-.5*(xmax+xmin), -.5*(ymax+ymin), -.5*(amax+amin))

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            glVertexPointerf(vtx)
            glDrawElementsui(GL_TRIANGLES, tri)

            level = glReadPixels(0, 0, width, height, GL_DEPTH_COMPONENT, GL_FLOAT)
            nodat_here = level > .9999999
            level = 2.0 * level - 1.0
            level *= .5*(amax-amin)
            level += .5*(amin+amax)
            #print "level in ", numpy.min(level[numpy.logical_not(nodat_here)]), numpy.max(level[numpy.logical_not(nodat_here)])

            level -= altitudes
            level[nodat_here] = -.1 #nodata
            level[level < 0.] = -.1
            #level[nodat_here] = nodata

            #print numpy.min(level), numpy.max(level), numpy.mean(level)
            if numpy.max(level) > 0:
                # write raster
                outfile = '.'.join(fil.split('.')[:-1])+'.level.tif'
                driver = gdal.GetDriverByName('GTiff')
                outRaster = driver.Create(outfile, width, height, 1, gdal.GDT_Float32)
                outRaster.SetProjection(src_ds.GetProjectionRef())
                outRaster.SetGeoTransform(gt)
                outband = outRaster.GetRasterBand(1)
                outband.SetNoDataValue(nodata)
                outband.WriteArray(level)
                outband.GetStatistics( True, True )
                outband.FlushCache()
                driver, outRaster, outband = None, None, None
                level_files.append(outfile)


        print timer.reset('tile generation')

        # build vrt and levels
        if os.path.exists('level.vrt'):
            os.remove('level.vrt')
        run_(['gdalbuildvrt', 'level.vrt'] +  level_files)
        print timer.reset('vrt creation')
        #run_(['gdal_contour', '-a', 'level'] + concat([['-fl', str(l)] for l in LEVELS]) + ['level.vrt', 'contour.shp'])
        open("blured.vrt", "w").write("""
<VRTDataset rasterXSize="13000" rasterYSize="20000">
  <SRS>EPSG:27572</SRS>
  <GeoTransform>  9.7196400000000000e+05,  2.0000000000000000e+00,  0.0000000000000000e+00,  2.3955790000000000e+06,  0.0000000000000000e+00, -2.0000000000000000e+00</GeoTransform>
  <VRTRasterBand dataType="Float32" band="1">
    <Metadata>
      <MDI key="STATISTICS_MAXIMUM">9.5065774917603</MDI>
      <MDI key="STATISTICS_MEAN">-0.066395050478341</MDI>
      <MDI key="STATISTICS_MINIMUM">-0.10000000149012</MDI>
      <MDI key="STATISTICS_STDDEV">0.1369556433065</MDI>
    </Metadata>
    <NoDataValue>-9999</NoDataValue>
    <KernelFilteredSource>
      <SourceFilename relativeToVRT="1">level.vrt</SourceFilename>
      <SourceBand>1</SourceBand>
      <SourceProperties RasterXSize="13000" RasterYSize="20000" DataType="Float32" BlockXSize="128" BlockYSize="128" />
      <Kernel normalized="1">
        <Size>5</Size>
        <Coefs>0.0036630037 0.014652015 0.025641026 0.014652015 0.0036630037 0.014652015 0.058608059 0.095238095 0.058608059 0.014652015 0.025641026 0.095238095 0.15018315 0.095238095 0.025641026 0.014652015 0.058608059 0.095238095 0.058608059 0.014652015 0.0036630037 0.014652015 0.025641026 0.014652015 0.0036630037 </Coefs>
      </Kernel>
    </KernelFilteredSource>
  </VRTRasterBand>
</VRTDataset>
""")


        run_(['gdal_contour', '-a', 'level'] + concat([['-fl', str(l)] for l in LEVELS]) + ['blured.vrt', 'blured_contour.shp'])
        print timer.reset('contours extraction')

    elif 'cover' == args[0]:
        timer = Timer()

        with open(args[2], 'rb') as f:
            nb_nodes, nb_elem  = numpy.frombuffer(f.read(8), dtype=numpy.int32)
            vtx = numpy.array(numpy.frombuffer(f.read(8*3*nb_nodes), dtype=numpy.float64).reshape(-1,3), dtype=numpy.float32)
            tri = numpy.frombuffer(f.read(4*3*nb_elem), dtype=numpy.int32).reshape(-1,3) - 1

        # create contour of the mesh
        edges = set()
        for t in tri:
            for n1, n2 in zip(list(t),list(t[1:])+list([t[0]])):
                edge = tuple(sorted([n1, n2]))
                if edge in edges:
                    edges.remove(edge)
                else:
                    edges.add(edge)

        borders = linemerge([LineString([Point(vtx[edge[0]][:2]), Point(vtx[edge[1]])][:2]) for edge in edges])
        if isinstance(borders, LineString):
            borders = [borders]

        print timer.reset('mesh border extraction')

        # find the grid in vrt
        tree = ElementTree.parse(args[1])

        gt = [float(x)
                for GeoTransform in tree.findall("GeoTransform")
                for x in GeoTransform.text.split(',')]
        print gt
        boxes = []
        for VRTRasterBand in tree.findall("VRTRasterBand"):
            for ComplexSource in VRTRasterBand.findall("ComplexSource"):
                for DstRect in ComplexSource.findall("DstRect"):
                    box =  [float(DstRect.get('xOff')),
                            float(DstRect.get('yOff')),
                            float(DstRect.get('xSize')),
                            float(DstRect.get('ySize'))]
                    boxes.append(Polygon([(gt[0]+box[0]*gt[1]         , gt[3]+box[1]*gt[5]),
                                          (gt[0]+(box[0]+box[2])*gt[1], gt[3]+box[1]*gt[5]),
                                          (gt[0]+(box[0]+box[2])*gt[1], gt[3]+(box[1]+box[3])*gt[5]),
                                          (gt[0]+box[0]*gt[1]         , gt[3]+(box[1]+box[3])*gt[5])]))

        print timer.reset('boxes creation')

        # tile contours dan generate polygons
        poly_file = 'poly.shp'
        driver = ogr.GetDriverByName('ESRI Shapefile')
        outDataSource = driver.CreateDataSource(poly_file)
        outLayer = outDataSource.CreateLayer("poly", geom_type=ogr.wkbPolygon)
        idField = ogr.FieldDefn("id", ogr.OFTInteger)
        outLayer.CreateField(idField)
        levelField = ogr.FieldDefn("level", ogr.OFTReal)
        outLayer.CreateField(levelField)
        classField = ogr.FieldDefn("class", ogr.OFTReal)
        outLayer.CreateField(classField)
        # .prj file
        open('poly.prj', 'w').write(open('blured_contour.prj').read())

        source = driver.Open('blured_contour.shp', 0)
        contours = MultiLineString([wkb.loads(feature.GetGeometryRef().ExportToWkb()) for feature in  source.GetLayer()] + [l for l in borders])

        level_src = gdal.Open('blured.vrt')
        level_gt = level_src.GetGeoTransform()
        level_rb = level_src.GetRasterBand(1)


        def level_class(level):
            ret = 0
            for c in [0]+LEVELS:
                if c > level:
                    break
                ret = c
            return ret

        #polygons = polygonize(contours)
        #print timer.reset('polygonization')

        #i = 0
        #for p in polygons:
        #    i += 1
        #    print i
        #    featureDefn = outLayer.GetLayerDefn()
        #    feature = ogr.Feature(featureDefn)
        #    feature.SetGeometry(ogr.CreateGeometryFromWkb(p.wkb))
        #    feature.SetField("id", i)
        #    representative = p.representative_point()
        #    px = int((representative.x - level_gt[0]) / level_gt[1])
        #    py = int((representative.y - level_gt[3]) / level_gt[5])
        #    level = numpy.frombuffer(level_rb.ReadRaster(px-1, py-1, 2, 2, buf_type=gdal.GDT_Float32), dtype=numpy.float32)
        #    level = float(numpy.mean(level))
        #    #print representative.x, representative.y, level, level_class(level)
        #    feature.SetField("level", level)
        #    feature.SetField("class", level_class(level))
        #    outLayer.CreateFeature(feature)
        #print timer.reset('writing file')
        #exit(0)


        for b, box in enumerate(boxes):
            sys.stdout.write('\r%.1f'%(100*float(b)/len(boxes)))
            sys.stdout.flush()

            box_contours = box.intersection(contours)
            print timer.reset('boxes/contour intersection')
            if isinstance(box_contours, LineString):
                box_contours = MultiLineString([box_contours])

            ends = [l.coords[0] for l in box_contours if not l.is_ring and Point(l.coords[0]).distance(box.exterior) < .001]\
                 + [l.coords[-1] for l in box_contours if not l.is_ring and Point(l.coords[-1]).distance(box.exterior) < .001] \
                 + list(box.exterior.coords)

            alpha_ends = [box.exterior.project(Point(e)) for e in ends]
            ordered_ends = [e for (a,e) in sorted(zip(alpha_ends, ends))]
            closing = [LineString([s, e]) for s, e in zip(ordered_ends, ordered_ends[1:]+ordered_ends[0:1])]

            print timer.reset('contour closing')
            polygons = polygonize(list(box_contours)+closing)
            print timer.reset('polygonization')

            for i, p in enumerate(polygons):
                featureDefn = outLayer.GetLayerDefn()
                feature = ogr.Feature(featureDefn)
                feature.SetGeometry(ogr.CreateGeometryFromWkb(p.wkb))
                feature.SetField("id", i+1)
                representative = p.representative_point()
                px = int((representative.x - level_gt[0]) / level_gt[1])
                py = int((representative.y - level_gt[3]) / level_gt[5])
                level = numpy.frombuffer(level_rb.ReadRaster(px-1, py-1, 3, 3, buf_type=gdal.GDT_Float32), dtype=numpy.float32)
                level = float(numpy.mean(level))
                #print representative.x, representative.y, level, level_class(level)
                feature.SetField("level", level)
                feature.SetField("class", level_class(level))
                outLayer.CreateFeature(feature)
        print "wrote", poly_file



    elif 'mesh' == args[0]:

        with open(args[1], 'rb') as f:
            nb_nodes, nb_elem  = numpy.frombuffer(f.read(8), dtype=numpy.int32)
            vtx = numpy.array(numpy.frombuffer(f.read(8*3*nb_nodes), dtype=numpy.float64).reshape(-1,3), dtype=numpy.float32)
            tri = numpy.frombuffer(f.read(4*3*nb_elem), dtype=numpy.int32).reshape(-1,3) - 1

        # create contour of the mesh
        edges = set()
        for t in tri:
            for n1, n2 in zip(list(t),list(t[1:])+list([t[0]])):
                edge = tuple(sorted([n1, n2]))
                if edge in edges:
                    edges.remove(edge)
                else:
                    edges.add(edge)

        borders = linemerge([LineString([Point(vtx[edge[0]][:2]), Point(vtx[edge[1]])][:2]) for edge in edges])

        polygons = [Polygon(borders)]


        driver = ogr.GetDriverByName('ESRI Shapefile')
        source = driver.Open('blured_contour.shp', 0)
        contours = MultiLineString([wkb.loads(feature.GetGeometryRef().ExportToWkb()) for feature in  source.GetLayer()])

        lines = contours
        points = []

        # first build a node map to have an id for each node
        node_map = {}
        current_id = 0
        tempdir = '/tmp' #mkdtemp()
        tmp_in_file = os.path.join(tempdir, 'tmp_mesh.geo')

        with open(tmp_in_file, "w") as geo:
            for polygon in polygons:
                surface_idx = []
                for ring in [polygon.exterior] + list(polygon.interiors):
                    ring_idx = []
                    for coord in ring.coords:
                        if coord in node_map:
                            ring_idx.append(node_map[coord])
                        else:
                            current_id += 1
                            ring_idx.append(current_id)
                            node_map[coord] = current_id
                            geo.write("Point(%d) = {%f, %f, 0, 999999};\n"%(current_id, coord[0], coord[1]))
                    loop_idx = []
                    for i, j in zip(ring_idx[:-1], ring_idx[1:]):
                        current_id += 1
                        geo.write("Line(%d) = {%d, %d};\n"%(current_id, i, j))
                        loop_idx.append(current_id)
                    current_id += 1
                    geo.write("Line Loop(%d) = {%s};\n"%(current_id, ", ".join((str(i) for i in loop_idx))))
                    surface_idx.append(current_id)
                current_id += 1
                geo.write("Plane Surface(%d) = {%s};\n"%(current_id, ", ".join((str(i) for i in surface_idx))))
                current_surface = current_id
                for line in lines:
                    if polygon.covers(line):
                        line_idx = []
                        for coord in line.coords:
                            if coord in node_map:
                                line_idx.append(node_map[coord])
                            else:
                                current_id += 1
                                line_idx.append(current_id)
                                node_map[coord] = current_id
                                geo.write("Point(%d) = {%f, %f, 0, 999999};\n"%(current_id, coord[0], coord[1]))
                        for i, j in zip(line_idx[:-1], line_idx[1:]):
                            current_id += 1
                            geo.write("Line(%d) = {%d, %d};\n"%(current_id, i, j))
                            geo.write("Line {%d} In Surface {%d};\n"%(current_id, current_surface))
                for point in points:
                    if polygon.covers(point):
                        if point.coords[0] in node_map:
                            geo.write("Point {%d} In Surface {%d};\n"%(node_map[point.coords[0]], current_surface))
                        else:
                            current_id += 1
                            node_map[point.coords[0]] = current_id
                            geo.write("Point(%d) = {%f, %f, 0, 999999};\n"%(current_id, point.coords[0][0], point.coords[0][1]))
                            geo.write("Point {%d} In Surface {%d};\n"%(current_id, current_surface))




    else:
       raise Exception("unknown command {}".format(args[0]))
