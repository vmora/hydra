# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
export database in json/geojson format

the output is a zip file containing on directory for the project and one directory for each model

USAGE

   python -m hydra.database.export_json project_database file.zip
"""


import json
import os
import string
from zipfile import ZipFile
from ..utility.sql_json import instances as INSTANCES

__currendir = os.path.dirname(__file__)

def ref_to_name(decl, schema, table):
    "returns sub-select for reference"
    spl = decl.split()
    # add references that are difficult to parse ()
    if table in ("zq_split_hydrology_singularity", "qq_split_hydrology_singularity"):
        if spl[0] == "downstream":
            spl = "downstream references $model._link".split()
        elif spl[0] == "split1":
            spl = "split1 references $model._link".split()
        elif spl[0] == "split2":
            spl = "split2 references $model._link".split()
    elif table in ("routing_hydrology_link", "connector_hydrology_link"):
        if spl[0] == "hydrograph":
            spl = "hydrograph references $model.hydrograph_bc_singularity".split()
    elif table in ("reservoir_rsp_hydrology_singularity", "reservoir_rs_hydrology_singularity"):
        if spl[0] == "drainage":
            spl = "drainage references $model._link".split()
        elif spl[0] == "overflow":
            spl = "overflow references $model._link".split()
    elif table == "regul_sluice_gate_singularity":
        if spl[0] == "control_node":
            spl = "control_node references $model._node".split()

    return """(select ref.name from {ref} as ref where ref.id={col}) as {col}""".format(
                col=spl[0],
                ref=string.Template(
                        spl[spl.index('references')+1].replace("(id)", "")
                        ).substitute({"model": schema})
                ) if 'references' in spl else spl[0]

def columns(instance, geom, subtyp):
    return [i for i in instance['decl']
            if i[0] != " "
            and i.split()[0]!=geom
            and (subtyp != 'pipe' or i.split()[0]!='branch')]

def export_json(filename, project, compact=True):
    """export project as json files
    compact is not human friendly"""

    zipf = ZipFile(filename, 'w')

    models = []
    for model in project.get_models():
        models += [
            [model, 'configuration', None, ['name', 'description']],
            [model, 'metadata', None, ['version', 'creation_date', 'configuration references $model.configuration']],
            [model, 'generation_step', None, ['name', 'description']],
            [model, 'river_cross_section_geometry', None, ['name', 'zbmin_array', 'zbtot_array', 'rlambda', 'rmu1', 'rmu2']],
            [model, 'valley_cross_section_geometry', None, ['name', 'zbmin_array', 'zbmaj_lbank_array', 'zbmaj_rbank_array', 'rlambda', 'rmu1', 'rmu2', 'zlevee_lb', 'zlevee_rb']],
            [model, 'closed_parametric_geometry', None, ['name', 'zbmin_array']],
            [model, 'open_parametric_geometry', None, ['name', 'zbmin_array']],
            [model, 'valley_cross_section_topo_geometry', None, ['name', 'xz_array']],
            [model, 'river_cross_section_profile', 'geom', ['name', 'z_invert_up', 'z_invert_down', 'type_cross_section_up', 'type_cross_section_down']],
            [model, 'catchment', 'geom', ['name']],
            [model, 'reach', 'geom', ['name', 'pk0_km', 'dx']],
            [model, 'station', 'geom', ['name']],
            [model, 'domain_2d', 'geom', ['name']],
            [model, 'spillway_group', None, ['name']]
            ]

        for typ in ['node', 'cross_section', 'link', 'singularity']:
            for subtyp, instance in INSTANCES[typ+'_type'].iteritems():
                geom = instance['geom']  if 'geom' in instance else None
                models.append([
                    model,
                    subtyp+'_'+typ,
                    geom,
                    columns(instance, geom, subtyp)
                    + (['generated references $model.generation_step']
                        if typ  in ('node', 'link') else [])
                    + (['id_pipe_link references $model._link',
                        'id_river_cross_section_profile_up references $model.river_cross_section_profile',
                        'id_river_cross_section_profile_down references $model.river_cross_section_profile',

                        ]
                        if typ == 'cross_section' else [])
                    + [ 'configuration', 'name']
                    ])

    for schema, table, geom, cols in [
            ['project', 'model_config',     None, ['name', 'config', 'comment']],
            ['project', 'grp',       None, ['name']],
            ['project', 'grp_model', None, ['grp project.grp', 'model_config references project.model_config']],
            ['project', 'mixed', None, ['ord', 'scenario references project.scenario', 'grp references project.grp']],
            ] + models:
        sql = """
            select hydra_to_json('{schema}.{table}', {geom}, {cols})
            """.format(
                schema=schema,
                table=table,
                geom="'{}'".format(geom) if geom else 'null',
                cols=", ".join(["'{}'".format(ref_to_name(c, schema, table)) for c in cols])
                )

        res, = project.execute(sql).fetchone()

        if res:
            data = json.loads(res)
            for i in range(len(data["features"])):
                for k, v in data["features"][i]["properties"].iteritems():
                    if type(v) == bool:
                        data["features"][i]["properties"][k] = 't' if v else 'f'
                    if type(v) == list:
                        data["features"][i]["properties"][k] = str(v).replace("[","{").replace("]","}")
                    #if type(v) == dict:
                    #    data["features"][i]["properties"][k] = json.dumps(v)

            if schema != 'project' and table == 'configuration':
                data["features"].pop(0) # default config

            if compact:
                zipf.writestr(schema+'/'+table+'.geojson', json.dumps(data))
            else:
                zipf.writestr(schema+'/'+table+'.geojson', json.dumps(data, indent=2, separators=(',', ': '))+'\n')


if __name__ == "__main__":
    from qgis.core import QgsApplication
    import sys
    from ..project import Project
    app = QgsApplication(sys.argv, False)
    QgsApplication.initQgis()
    project = Project.load_project(sys.argv[1])
    export_json(sys.argv[2], project, False)
