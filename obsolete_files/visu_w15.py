# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
from qgis.core import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from hydra.utility.settings_properties import SettingsProperties
from hydra.post_process.mylayer import MyLayerType, MyLayer

class VisuW15():
    def __init__(self, project, iface):
        self.__iface = iface
        self.timer = QTimer(None)
        self.layer = None
        self.timeSlider = None
        self.playButton = None
        self.spinBox = None

        self.project = project

    def layerWillBeRemoved(self, layerId):
        layer = QgsMapLayerRegistry.instance().mapLayer(layerId)
        if isinstance(layer, MyLayer):
            pass

    def layersAdded(self, layers):
        for layer in layers:
            if isinstance(layer, MyLayer):
                # stop animation is running
                self.timer.stop()

                # create slider to animate results
                self.timeSlider and self.timeSlider.setParent(None)
                self.timeSlider = QSlider(Qt.Horizontal)
                self.timeSlider.setMinimum(0)
                self.timeSlider.setMaximum(len(layer.steps())-1)
                self.timeSlider.valueChanged.connect(layer.setStep)
                self.__iface.addToolBarWidget(self.timeSlider)

                self.spinBox and self.spinBox.setParent(None)
                self.spinBox = QSpinBox()
                self.spinBox.setMinimum(0)
                self.spinBox.setMaximum(len(layer.steps())-1)
                self.__iface.addToolBarWidget(self.spinBox)
                self.timeSlider.valueChanged.connect(self.spinBox.setValue)

                # create play button
                self.playButton and self.playButton.setParent(None)
                self.playButton = QPushButton('play')
                self.playButton.setCheckable(True)
                self.playButton.clicked.connect(self.play)
                self.__iface.addToolBarWidget(self.playButton)

    def initGui(self):
        self.__openGlLayerType = MyLayerType()
        QgsPluginLayerRegistry.instance().addPluginLayerType(self.__openGlLayerType)

        QgsMapLayerRegistry.instance().layersAdded.connect(self.layersAdded)
        QgsMapLayerRegistry.instance().layerWillBeRemoved.connect(self.layerWillBeRemoved)

        # create open result button
        self.openBtn = QPushButton("Open results")
        self.openBtn.clicked.connect(self.openResults)
        self.openBtnAct = self.__iface.addToolBarWidget(self.openBtn)

    def openResults(self):
        fil = QFileDialog.getOpenFileName(None,
                u"Open results",
                self.project.get_workspace(),
                "Hydra result file (*.w15)")
        if not fil:
            return

        db = SettingsProperties.get_db()
        pg = SettingsProperties.get_postgre_settings()

        self.olayer = MyLayer(self.project, fil[:-3]+'w14', fil, name="Water level")
        QgsMapLayerRegistry.instance().addMapLayer(self.olayer)

        #QgsMapLayerRegistry.instance().addMapLayer(self.layer)

    def unload(self):
        self.timeSlider and self.timeSlider.setParent(None)
        self.playButton and self.playButton.setParent(None)
        self.spinBox and self.spinBox.setParent(None)
        self.__iface.removeToolBarIcon(self.openBtnAct)
        #QgsPluginLayerRegistry.instance().removePluginLayerType(MyLayer.LAYER_TYPE)

    def animate(self):
        if self.__iface.mapCanvas().isDrawing():
            return
        self.timeSlider.setValue(
                (self.timeSlider.value() + 1)
                % (self.timeSlider.maximum() + 1) )

    def play(self, checked):
        if checked:
            self.timer.stop()
            self.timer.timeout.connect(self.animate)
            self.timer.start(100.)
            self.playButton.setText('Pause')
        else:
            self.timer.stop()
            self.playButton.setText('Play')
